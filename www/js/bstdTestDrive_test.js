var bstdTestDrive = (new function() {
	var sThis = this,
		_modelsTempalte = '<div class="model">'
								+ '<div class="name">{#modelName#}</div>'
								+ '<div class="image"><img src="{#modelImage#}" alt="" /></div>'
								+ '<div class="submodels"></div>'
						+ '</div>',
		_submodelsTemplate = '<div class="submodelItem">'
								+ '<input id="id_{#submodelId#}" type="radio" name="modelId" value="{#submodelId#}"  />'
								+ '<label for="id_{#submodelId#}">{#modelName#}</label>'
							+ '</div>',
		_data,
		_currentDealers,
		_formObj,
		_dealerRCode,
		
		_predefinedDealers,
		_predefinedCities;
	
	var _loadData = function() {
		var _dealers = {},
			_cities = {},
			_dealersSubmodels = {},
			_i,
			_len,
			_cd,
			_curr,
			
			_models,
			_subModels,
			_tmp,
			_filters = {};
		
		if(_data) {
			bstdLTAPI.ga('UA-9238963-7', 1);
		
			//_predefinedDealers,
			//_predefinedCities;
			if(_predefinedCities) {
				_data.cities = bstdSystem.array.applyFilter(_data.cities, {"cityId": _predefinedCities}, true);
				_data.partners = bstdSystem.array.applyFilter(_data.partners, {"cityId": _predefinedCities}, true);
				_data.partnersModels = bstdSystem.array.applyFilter(_data.partnersModels, bstdSystem.array.valuesByFields(_data.partners, "partnerId", true), true);
																								//getArrayObjectsForFields: function(array, fieldsName, unique, uniqueFields)
				_data.submodels = bstdSystem.array.applyFilter(_data.submodels, bstdSystem.array.valuesByFields(_data.partnersModels, "submodelId", true), true);
				
				_data.models = bstdSystem.array.applyFilter(_data.models, bstdSystem.array.valuesByFields(_data.submodels, "modelId", true), true);
				
				
				$("#frmFields").append('<input type="hidden" name="type" value="' + _predefinedCities + '" />');
				
			}
			else if(_predefinedDealers) {
				_data.partners = bstdSystem.array.applyFilter(_data.partners, {"partnerId": _predefinedDealers}, true);
				
				_data.cities = bstdSystem.array.applyFilter(_data.cities, bstdSystem.array.valuesByFields(_data.partners, "cityId", true), true);
				
				_data.partnersModels = bstdSystem.array.applyFilter(_data.partnersModels, bstdSystem.array.valuesByFields(_data.partners, "partnerId", true), true);
																								//getArrayObjectsForFields: function(array, fieldsName, unique, uniqueFields)
				_data.submodels = bstdSystem.array.applyFilter(_data.submodels, bstdSystem.array.valuesByFields(_data.partnersModels, "submodelId", true), true);
				
				_data.models = bstdSystem.array.applyFilter(_data.models, bstdSystem.array.valuesByFields(_data.submodels, "modelId", true), true);
			}
			
			sThis.createModels(_data.models, _data.submodels);
			$("input[name=modelId]").on("change",
											function() {
												_changeSubmodel($(this).val());
											});
			if(!_predefinedCities || _predefinedCities instanceof Array && _predefinedCities.length > 1) {
				$("#cityId").on("change",
												function() {
													_changeCity($(this).val());
												});
			}
			else {
				$("#city").hide();
			}
			$("#frmFields")
				.on("succesfullSubmiting", function(e, data){
					if(typeof data.result == "object" && data.result.dealerRCode) {
						_dealerRCode = data.result.dealerRCode;
					}
					_formObj.disable();
					bstdAlert("Спасибо!<br />Ваша заявка на прохождение тест-драйва отправлена дилеру.<br />В ближайшее время представитель дилера свяжется с Вами.", "Запись на тест-драйв");
					//bstdLTAPI.ga.event('Click', isLexus && /estimate-promo.tmex/.test(location.params.ref) ? 'Submit_promo' : 'Submit', _activeDealers ? 'Dealer' : 'Main', undefined, false);
					
					bstdLTAPI.ga.customVar(1, "model", $('input[name="submodelId"]:checked').attr('value'), 3); 
					bstdLTAPI.ga.customVar(2, "dealer", _dealerRCode, 3);
				})
				.on("validateError", function() {
					bstdAlert("Не заполнено или заполнено с ошибками одно или несколько полей.", "Запись на тест-драйв");
				});
			_formObj = new bstdForm("#frmFields",
									bstdForm.prototype.MODE_ADD,
									null,
									{
										action: "http://content.lexus-russia.ru/z/testdrives/",
										errorType: 1
									});
			$("#agreement").on("change", function() {
					if($("#agreement").prop("checked")) {
						$("#submit").show();
					}
					else {
						$("#submit").hide();
					}
				});
			if(window != top && window["bstdFrames"] && typeof bstdFrames.setHeight == "function") {
				bstdFrames.setHeight(100);
			}
		}
	};

	var _changeCity = function(city) {
		bstdLTAPI.UI.createOptions("#partnerId",
									bstdSystem.array.applyFilter(_currentDealers,
										{
											cityId: city
										}),
									null,
									null,
									"partnerId",
									"partnerName");
	};
	
	var _changeSubmodel = function(submodel) {
	//function(array1, fieldName, concat, unique)
		var _dealersIds = bstdSystem.array.getArrayByField(bstdSystem.array.applyFilter2(_data.partnersModels,
																							{
																								submodelId: {
																									by: "or",
																									filter: submodel
																								}
																							}),
															"partnerId",
															null,
															true),
			_citiesIds,
			_cities;
			
			if(_dealersIds && _dealersIds.length) {
				_currentDealers = bstdSystem.array.applyFilter2(_data.partners,
																{
																	partnerId: {
																		by: "or",
																		filter: _dealersIds
																	}
																});
			}
			
			if(_data.cities.length == 1) {
				_changeCity(_predefinedCities);
			}
			else {
				if(_dealersIds && _dealersIds.length) {
					_currentDealers = bstdSystem.array.applyFilter2(_data.partners,
																	{
																		partnerId: {
																			by: "or",
																			filter: _dealersIds
																		}
																	});
					_citiesIds = bstdSystem.array.getArrayByField(_currentDealers,
																	"cityId",
																	null,
																	true);
					_cities = bstdSystem.array.applyFilter2(_data.cities,
																{
																	cityId: {
																		by: "or",
																		filter: _citiesIds
																	}
																});
					bstdLTAPI.UI.createOptions("#cityId", _cities, null, null, "cityId", "cityName");
					$("#partnerId").empty();
				}
		}
	};
	
	this.createModels = function(models, submodels) {
		var _i,
			_len,
			_curr,
			_len2,
			_i2,
			_item,
			_list = $("#modelsList");
		if(models instanceof Array) {
			if(_list.length) {
				_len = models.length;
				for(_i = 0; _i < _len; _i ++) {
					_curr = models[_i];
					_list.append(_item = $(bstdSystem.templater.getByTemplate(_modelsTempalte, _curr)));
					_item = $(".submodels", _item);
					sThis.createSubmodels(_item, bstdSystem.array.applyFilter(submodels, {modelId: _curr["modelId"]}));
				}
			}
		}
	};
	
	this.createSubmodels = function(ele, submodels) {
		var _i,
			_len,
			_item;
		ele = $(ele);
		if(submodels instanceof Array) {
			if(ele.length) {
				_len = submodels.length;
				for(_i = 0; _i < _len; _i ++) {
					ele.append($(bstdSystem.templater.getByTemplate(_submodelsTemplate, submodels[_i])));
				}
			}
		}
	};
	
	(function() {
		var _params = {
				actionId: 1,
				callback: function(data) {
						_data = data;
						bstdSystem.onAfterLoad(_loadData);
					}
			};
		if(location.params) {
			if(location.params.dealer) {
				_params["dealerId"] = _predefinedDealers = location.params.dealer;
			}
			else if(location.params.dealerId) {
				_params["dealerId"] = _predefinedDealers = location.params.dealerId;
			}
			if(location.params.cityId) {
				_params["cityId"] = _predefinedCities = location.params.cityId;
			}
		}
		
		bstdLTAPI.actions.info(_params);
	})();
});