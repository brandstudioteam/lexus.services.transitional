(function() {
	var _params = location.href.split("?"),
		_i,
		_tmp,
		_s,
		_str;
	location["params"] = {};
	if(_params.length > 1) {
		location["stringParams"] = _str = decodeURIComponent(_params[1].split("#")[0]);
		_params = location["stringParams"].split("&");
		if(_params) {
			for(_i = 0; _i < _params.length; _i++) {
				if(!_params[_i]) {
					continue;
				}
				_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
	            if((_s = _tmp[1].indexOf("[]")) != -1) {
	                _tmp[1] = _tmp[1].substr(0, _s);
	            }
	            if(_tmp[2] === undefined) {
	                _tmp[2] = null;
	            }
				if(_tmp[1] in location["params"]) {
					if(location["params"][_tmp[1]] instanceof Array) {
						location["params"][_tmp[1]].push(_tmp[2]);
					}
					else location["params"][_tmp[1]] = [location["params"][_tmp[1]], _tmp[2]];
				}
				else location["params"][_tmp[1]] = _tmp[2];
			}
		}
	}
	return location["params"];
})();