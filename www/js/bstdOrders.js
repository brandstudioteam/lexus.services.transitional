if(!window.bstdTestDriveCall) {
	bstdSystem.prepareInit({
	/*
		inclideScript: [
			["http://content.toyota.ru/js/jq/jquery.maskedinput.js"]
		],
*/
		messages: [
			["emptyModel", "Необходимо выбрать интересующую модель", 152],
			["emptyCity", "Необходимо выбрать город", 152],
			["emptyDealer", "Необходимо выбрать дилера", 152],
			["emptyFirstName", "Необходимо ввеcти имя", 152],
			["emptyLastName", "Необходимо ввеcти фамилию", 152],
			["emptyAge", "Необходимо ввеcти возраст", 152],
			["emptyPhone", "Необходимо ввеcти телефон", 152],
			["emptyEmail", "Необходимо ввеcти адрес электронной почты", 152],
			["emptyCurrentCarBrand", "Необходимо выбрать производителя, имеющегося у Вас в наличии автомобиля", 152],
			["emptyCurrentCar", "Необходимо выбрать модель, имеющегося у Вас в наличии автомобиля", 152]
		]
	});
}
var bstdTestDrive = (new function() {
	var sThis = this,
		_data,
		_currentDealers,
		//_formObj,
		_form,
		_modelObj,
		_cityObj,
		_dealerObj,
		_dealerRcode,
		_activeDealers = location.params["dealer"],
		_activeDealersCount = 0,
		_formObj,
		_setPhoneMaskCount = 0;
	if(location.host=='content.lexus-russia.ru' && typeof bstdSystem.ga == "function") {
		bstdSystem.ga('UA-51300333-1');
	}
	
	var _setPhoneMask = function() {
		$('[name=phone]').mask("+7 (999) 999-99-99");
		/*
	
		if($('#phone', _form).mask) {
			$('#phone', _form).mask("+7 (999) 999-99-99");
		}
		else {
			if(_setPhoneMaskCount == 10) {
				return;
			}
			setTimeout(_setPhoneMask, 100);
		}
		*/
	};
	if(!window.bstdTestDriveCall) {
		bstdSystem.loadScript("http://content.toyota.ru/js/jq/jquery.maskedinput.js",
							function() {
								bstdSystem.onAfterLoad(_setPhoneMask);
							}, null, "jquery.maskedinput");
	}

	if(_activeDealers) {
		if(_activeDealers instanceof Array) {
			switch(_activeDealers.length) {
				case 0:
					_activeDealers = null;
					break;
				case 1:
					_activeDealers = _activeDealers[0];
					_activeDealersCount = 1;
					break;
				default:
					_activeDealersCount = _activeDealers.length;
			}
		}
		else {
			_activeDealersCount = 1;
		}
	}

	var _loadData = function() {
		_form = $("form.preorder-form");
		//_setPhoneMask();
		if(_activeDealersCount) {
			$("[data-field=cityId]", _form).remove();
		}
		if(_activeDealersCount == 1) {
			$("[data-field=partnerId]", _form).remove();
			_form.append('<input type="hidden" name="partnerId" value="' + _activeDealers + '" />');
		}
		_form.on("succesfullSubmiting", function(e, data){
				if(typeof data.result == "object" && data.result.dealerRcode) {
					_dealerRcode = data.result.dealerRcode;
				}
				_formObj.disable();
				$("#SuccessWin").show();
				$("body").css({overflow: "hidden"});
				if(location.host=='content.lexus-russia.ru') {
					bstdSystem.ga.event('Click', 'submit', 'preorder-form', undefined, false);
				}
				else {
					ga('send', 'event', 'Click', 'submit', 'preorder-form');
				}
			});
			/*
			.on("validateError", function() {
				bstdAlert("Не заполнено или заполнено с ошибками одно или несколько полей.", "Запись на тест-драйв");
			});
			*/
		_formObj = new bstdForm(_form,
								bstdForm.prototype.MODE_ADD,
								null,
								{
									action: "http://content.lexus-russia.ru/z/orders/",
									errorType: 1
								});
		/*
		$("#agreement", _form).on("change", function() {
				if($("#agreement", _form).prop("checked")) {
					$("#submit", _form).show();
				}
				else {
					$("#submit", _form).hide();
				}
			});
		*/
		_modelObj	= _formObj.element("modelId");
		_modelObj.addItem(_activeDealers
							? bstdSystem.array.applyFilter(
														_data.submodels,
														bstdSystem.array.valuesByFields(
															bstdSystem.array.applyFilter2(
																		_data.partnersModels,
																		{
																			partnerId: {
																				by: "or",
																				filter: _activeDealers
																			}
																		}),
															"submodelId",
															true),
														1)
							: _data.submodels, {value: "submodelId", text: "modelName"});
		$("#modelId", _form).change(function(e, data) {
				if(data && data.selectValue) {
					_changeSubmodel(data.selectValue);
				}
			});
		
		
		var _changeSubmodel = function(submodel) {
			var _dealersIds,
				_citiesIds,
				_cities;
			
			if(!_activeDealersCount || _activeDealersCount > 1) {
				_dealersIds = bstdSystem.array.getArrayByField(bstdSystem.array.applyFilter2(_data.partnersModels,
																								{
																									submodelId: {
																										by: "or",
																										filter: submodel
																									}
																								}),
																"partnerId",
																null,
																true);
				if(_dealersIds && _dealersIds.length) {
					_currentDealers = bstdSystem.array.applyFilter2(_data.partners,
																	{
																		partnerId: {
																			by: "or",
																			filter: _dealersIds
																		}
																	});
					if(_activeDealersCount > 1) {
						bstdLTAPI.UI.createOptions($("#partnerId", _form),
													_currentDealers,
													null,
													null,
													"partnerId",
													"partnerName");
					}
					if(!_activeDealersCount) {
						_citiesIds = bstdSystem.array.getArrayByField(_currentDealers,
																		"cityId",
																		null,
																		true);
						_cities = bstdSystem.array.applyFilter2(_data.cities,
																{
																	cityId: {
																		by: "or",
																		filter: _citiesIds
																	}
																});
						_cityObj.empty();
						_cityObj.addItem(_cities, {value: "cityId", text: "cityName"});
						_dealerObj.empty();
					}
				}
			}
		};
		
		if(!_activeDealersCount) {
			_cityObj	= _formObj.element("cityId");
			$("#cityId", _form).change(function(e, data) {
					if(data && data.selectValue) {
						_changeCity(data.selectValue);
					}
				});
		}
		if(!_activeDealersCount || _activeDealersCount > 1) {
			_dealerObj	= _formObj.element("partnerId");
			
		}
		
		var _changeCity = function(city) {
			_dealerObj.empty();
			_dealerObj.addItem(bstdSystem.array.applyFilter(_currentDealers,
											{
												cityId: city
											}),
										{value: "partnerId", text: "partnerName"});
		};
		
		
		bstdLTAPI.cars.getManufacturers({callback: function(data) {
				_formObj.element("currentBrand").addItem(data, {value: "id", text: "title"});
			}});
		$("#currentBrand", _form).change(function(e, data) {
			if(data && data.selectValue) {
				bstdLTAPI.cars.getCars({manufacturerId: data.selectValue,
										callback: function(data) {
					_formObj.element("currentModel").empty();
					_formObj.element("currentModel").addItem(data, {value: "id", text: "title"});
				}});
			}
		});
		$("#agreementInfo", _form).click(function() {
			$("#AgreementWin").show();
			$("body").css({overflow: "hidden"});
		});
		$("#AgreementWin .close").click(function() {
			$("#AgreementWin").hide();
			$("body").css({overflow: "visible"});
		});
		$("#SuccessWin .close").click(function() {
			$("#SuccessWin").hide();
			$("body").css({overflow: "visible"});
		});
	};
	(function() {
		var _sendData = {
				actionId: 2,
				callback: function(data) {
										_data = data;
										bstdSystem.onAfterLoad(_loadData);
									}
			};
		if(_activeDealers) {
			_sendData["partnerId"] = _activeDealers;
		}
		bstdLTAPI.actions.info(_sendData);
	})();
});