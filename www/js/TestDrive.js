			$.ajaxSetup({
				headers: {
					"X_REQUESTED_WITH": "XMLHttpRequest"
				}
			});
            
			var _loadCities = function() {
				$.ajax(
       					{
       						url: 		"http://contacts.toyota.ru/z/",
       						type:		"post",
       						data:		{prq:$.toJSON({a:"p", b: "a", d: {"modelId": _currentModel}})},
       						dataType:	'json',
       						complete:	function(response) {
       								if(response && (response.response || response.responseText)) {
       									response = response.response || response.responseText;
       									if(typeof response == "string") {
       										response = $.parseJSON(response);
       									}
       								}
       							
       								if(response && response.result) {
       									response = response.result;
           								if(response.length > 1) {
           									items = '<option disabled selected></option>';
           									for(i = 0; i < response.length; i++) {
               									items += '<option value="' + response[i].cityId + '">' + response[i].cityName + '</option>';
               								}
           								}
           								else {
           									items = '<option value="' + response[0].cityId + '" selected="selected" disabled="disabled">' + response[0].cityName + '</option>';
           								}
           								$('#city').append(items);
           								$('#city').attr("disabled", false);
       								}
       							}
       					}
       				);
			};
			
            $(document).ready(function()
            {
				var _dealerRCode,
				 _form,
				 _activeDealers = location.params["dealer"];
				 
				if(!_activeDealers && location.params["_gaA"]) {
					bstdSystem.ga(location.params["_gaA"], null, location.parent ? location.parent.basicHost : location.params && location.params.ref ? location.params.ref.basicHost : location.basicHost);
				}
				else {
					//bstdSystem.ga(bstdSystem.isLexus ? 'UA-44690698-1' : 'UA-31211135-1');
				}
			
			
			
			
			
			
            	_isLoaded = true;

				var mask = function(hide) {
					if(hide == 1) {
						$("#testdrive").hide();
	                    $("#loadingInfo").show();
					}
					else if(hide == 2) {
						$("#testdrive").hide();
	                    $("#loadingInfo").hide();
					}
					else {
						$("#testdrive").show();
                    	$("#loadingInfo").hide();
					}
				};

				$("#personal_inf").bind("change", function() {
					if($("#personal_inf").attr("checked")) {
						$("#submit").show();
					}
					else {
						$("#submit").hide();
					}
				});
				
				var changeCaptcha = function() {
					$("#captchaImg").attr("src", $("#captchaImg").attr("src").split("?")[0] + "?r=" + Math.random());
					$("#captcha").val('');
				};
				
				var checkCaptcha = function() {
					if(!$("#captcha").val()) {
						alert('Введите код подтверждения');
						$("#captcha").focus();
						return false;
					}
					mask(1);
					
					$.ajax({
   						url: 		"http://contacts.toyota.ru/z/",
   						type:		"post",
   						data:		{prq:$.toJSON({a:"z", b: "a", d: {"captcha": $("#captcha").val()}})},
   						dataType:	'json',
   						complete:	function(response) {
   							if(response && response.response) {
								response = response.response;
								if(typeof response == "string") {
									response = $.parseJSON(response);
								}
							}
   							if(response.error) {
    							 alert('Не верный код подтверждения');
    							 changeCaptcha();
    							 mask();
    							 $("#captcha").focus();
    						 }
    						 else {
    							 if(validate() === false) {
    								 mask();
    							 }
    						 }
    					}
   					});
					return false;
				};
				
                $('#submit').click(checkCaptcha);
                $("#captchaImg").click(changeCaptcha);
				
                
                $('.text_field').val('');

                if(_partnerCheck == 3) {
                	$("#city").bind("change", function(){
                		$('#dealer').empty();
                		$.ajax(
           					{
           						url: 		"http://contacts.toyota.ru/z/",
           						type:		"post",
           						data:		{prq:$.toJSON({a:"p", b: "d", d: {"cityId": $("#city").val(), "modelId": _currentModel}})},
           						dataType:	'json',
           						complete:	function(response) {
           								if(response && (response.response || response.responseText)) {
           									response = response.response || response.responseText;
           									if(typeof response == "string") {
           										response = $.parseJSON(response);
           									}
           								}
           							
           								if(response && response.result) {
           									response = response.result;
	           								if(response.length > 1) {
	           									items = '<option disabled selected></option>';
	           									for(i = 0; i < response.length; i++) {
	               									items += '<option value="' + response[i].partnerId + '">' + response[i].partnerName + '</option>';
	               								}
	           								}
	           								else {
	           									items = '<option value="' + response[0].partnerId + '" selected="selected" disabled="disabled">' + response[0].partnerName + '</option>';
	           								}
	           								$('#dealer').append(items);
	           								$('#dealer').attr("disabled", false);
           								}
           							}
           					}
           				);
					});
                }
                
                
                if(_modelCheck == 1) {
                	$("#car").bind("change", function(){
                		_currentModel = $("#car").val();
                		
                		_currentModelName = $('#car option:selected').text();
                		
                		$("#car_image")[0].src=$("[data-ref-model-id=" + _currentModel + "]").attr("data-model-image");
                		
                		$("#car_image").css("visibility", "visible");
                		
                		$("#modelId").val(_currentModel);
                		$('#city').empty();
                		$('#city').attr("disabled", true);
                		$('#dealer').empty();
                		$('#dealer').attr("disabled", true);
                		_loadCities();
					});
                }
                else {
                	_loadCities();
                }
                
				var validate = function(e){
					var _val,
						_data = {};
					
                    _val = $('#lastName').val();
                    if(!_val) {
                    	alert('Укажите свою фамилию');
                    	$("#lastName").focus();
                        return false;
                    }
					else if(!/^([а-яё]+(-[а-яё])*)+$/i.test(_val)) {
						alert("Поле \"Фамилия\" содержит недопустимые символы. Вводится по-русски");
						$("#lastName").focus();
						 return false;
					}
					else {
						_data["lastName"] = _val;
					}
					
                    _val = $('#firstName').val();
                    if(!_val) {
                    	alert('Укажите свою фамилию');
                    	$("#firstName").focus();
                        return false;
                    }
					else if(!/^[а-яё]+$/i.test(_val)) {
						alert("Поле \"Имя\" содержит недопустимые символы. Вводится по-русски");
						$("#firstName").focus();
						 return false;
					}
					else {
						_data["firstName"] = _val;
					}


                    _val = $('#email').val();
                    if(!_val) {
                    	alert('Укажите адрес своей электронной почты');
                    	$("#email").focus();
                        return false;
                    }
					else if(!/^([\+\.\-_\w\d]+@([\.\-_\w\d]+\.[-_\w\d]+)+)$/i.test(_val)) {
						alert("Поле \"E-mail\" содержит недопустимые символы.");
						$("#email").focus();
						 return false;
					}
					else {
						_data["email"] = _val;
					}

                    _val = $('#phone').val();
                    if(!_val) {
                    	alert('Укажите свой телефон');
                    	$("#phone").focus();
                        return false;
                    }
					else if(!/^(((\+7)|8)?\s*-?(\d{10}|((\d{3}\s*[-.]?){2}|((\(?\s*\d{3}\s*\)?\s*[-.]?)(\d{3}\s*[-.]?)))((\d{2}\s*[-.]?){2}|\d{4})))$/.test(_val)) {
						alert("Поле \"Контактный телефон\" содержит недопустимые символы.");
						$("#phone").focus();
						 return false;
					}
					else {
						_data["phone"] = _val;
					}
                    
                    _val = $("#gender").val();
					if(_val)
						_data["gender"] = _val;
					else {
						alert("Необходимо выбрать пол");
						$("#gender").focus();
						return false;
					}
					
					_val = $("#age").val();
					if(!_val) {
						alert("Введите возраст");
						$("#age").focus();
						return false;
					}
					else if(!/\d{2}/.test(_val) || _val < 18 || _val > 99) {
						alert("Введено некорректное значение в поле \"Возраст\"");
						$("#age").focus();
						return false;
					}
					else {
						_data["age"] = _val;
					}

					if(_partnerCheck == 2) {
						_data["partnerId"] = _currentDealer;
					}
					else {
					 	_val = $("#dealer").val();
						if(_val)
							_data["partnerId"] = _val;
						else {
							if(_partnerCheck == 3) {
								if(!$("#city").val()) {
									alert('Выберите город, чтобы выбрать дилера');
									$("#city").focus();
									return false;
								}
							}
							alert("Необходимо выбрать дилера");
							$("#dealer").focus();
							return false;
						}
					}
					_data["modelId"] = _currentModel;
					
					_data["subscribe"] = $("#subscribe_1").attr("checked") ? 1 : 0;
					
					_val = $("#isOwner_1").attr("checked") ? 1 : $("#isOwner_2").attr("checked") ? 0 : null;
					if(_val === null) {
						alert("Укажите, являетесь ли Вы владельцем автомобиля Toyota");
						$("#isOwner_1").focus();
						return false;
					}
					
					$.ajax({
   						url: 		"http://contacts.toyota.ru/z/",
   						type:		"post",
   						data:		{prq:$.toJSON({a:"p", b: "l", d: _data})},
   						dataType:	'json',
   						complete:	function(response) {
   							if(response && response.response) {
								response = response.response;
								if(typeof response == "string") {
									response = $.parseJSON(response);
								}
							}
   							
   							if(response.error) {
   								if(typeof response.errors == "object") {
   									for(var i in response.errors) {
   										
   									}
   								}
   							}
   							else {
   								if(_partnerCheck == 3) {
								
									if(bstdSystem.ga.isCrossdomain) {
										bstdSystem.ga.linkPost($("#frmFields").get(0));
										bstdSystem.ga.event('Test-Drive', 'Submit', bstdSystem.getBrandName(), undefined, false); //_activeDealers ? 'Dealer' : bstdSystem.getBrandName()
										bstdSystem.ga.event('Test-Drive', 'model', _currentModelName, undefined, false);
										bstdSystem.ga.event('Test-Drive', 'dealerR', response.dealerRcode, undefined, false);
									}
									else {
										//bstdSystem.ga.event('Click', 'Submit', _activeDealers ? 'Dealer' : 'Main', undefined, false);
									}
                                }
   								$('#main').fadeOut(400, function() {mask(2); $('#final').fadeIn(400);});
   							}
						}
       				});
                };
            });
