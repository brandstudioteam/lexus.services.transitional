var bstdTestDrive = (new function() {
	var sThis = this,
		_modelsTempalte = '<div class="model">'
								+ '<div class="name">{#modelName#}</div>'
								+ '<div class="image"><img src="{#modelImage#}" alt="" /></div>'
								+ '<div class="submodels"></div>'
						+ '</div>',
		_submodelsTemplate = '<div class="submodelItem">'
								+ '<input id="id_{#submodelId#}" type="radio" name="modelId" value="{#submodelId#}"  />'
								+ '<label for="id_{#submodelId#}">{#modelName#}</label>'
							+ '</div>',
		_data,
		_currentDealers,
		_formObj,
		_dealerRcode,
		_activeDealers = location.params["dealer"];
	
	var _loadData = function() {
		var _dealers = {},
			_cities = {},
			_dealersSubmodels = {},
			_i,
			_len,
			_cd,
			_curr;
		
		if(_data) {
			if(!_activeDealers && location.params["_gaA"]) {
				bstdSystem.ga(location.params["_gaA"], null, "www.lexus.ru");//location.parent ? location.parent.basicHost : location.params && location.params.ref ? location.params.ref.basicHost : location.basicHost);
			}
			else {
				bstdSystem.ga('UA-9238963-7');
			}
		
			sThis.createModels(_data.models, _data.submodels);
			$("input[name=modelId]").on("change",
											function() {
												_changeSubmodel($(this).val());
											});
			$("#cityId").on("change",
											function() {
												_changeCity($(this).val());
											});
			$("#frmFields")
				.on("succesfullSubmiting", function(e, data){
					if(typeof data.result == "object" && data.result.dealerRcode) {
						_dealerRcode = data.result.dealerRcode;
					}
					_formObj.disable();
					bstdAlert("Спасибо!<br />Ваша заявка на прохождение тест-драйва отправлена дилеру.<br />В ближайшее время представитель дилера свяжется с Вами.", "Запись на тест-драйв");
					//setTimeout(function() {
						if(bstdSystem.ga.isCrossdomain) {
							bstdSystem.ga.linkPost($("#frmFields").get(0));
							bstdSystem.ga.event('Test-Drive', 'Submit', bstdSystem.getBrandName(), undefined, false); //_activeDealers ? 'Dealer' : bstdSystem.getBrandName()
							bstdSystem.ga.event('Test-Drive', 'model', $('input[name="modelId"]:checked').val(), undefined, false);
							bstdSystem.ga.event('Test-Drive', 'dealer', _dealerRcode, undefined, false);
						}
						else {
							bstdSystem.ga.event('Click', 'Submit', _activeDealers ? 'Dealer' : 'Main', undefined, false);
							bstdSystem.ga.event('Click', 'dealer', _dealerRcode, undefined, false);
							bstdSystem.ga.event('Click', 'model', $('input[name="modelId"]:checked').val(), undefined, false);
						}
					//}, 10);
				})
				.on("validateError", function() {
					bstdAlert("Не заполнено или заполнено с ошибками одно или несколько полей.", "Запись на тест-драйв");
				});
			_formObj = new bstdForm("#frmFields",
									bstdForm.prototype.MODE_ADD,
									null,
									{
										action: "http://content.lexus-russia.ru/z/testdrives/",
										errorType: 1
									});
			$("#agreement").on("change", function() {
					if($("#agreement").prop("checked")) {
						$("#submit").show();
					}
					else {
						$("#submit").hide();
					}
				});
			if(window != top && window["bstdFrames"] && typeof bstdFrames.setHeight == "function") {
				bstdFrames.setHeight(100);
			}
		}
	};

	var _changeCity = function(city) {
		_currentDealers
		bstdLTAPI.UI.createOptions("#partnerId",
									bstdSystem.array.applyFilter(_currentDealers,
										{
											cityId: city
										}),
									null,
									null,
									"partnerId",
									"partnerName");
	};
	
	var _changeSubmodel = function(submodel) {
	//function(array1, fieldName, concat, unique)
		var _dealersIds = bstdSystem.array.getArrayByField(bstdSystem.array.applyFilter2(_data.partnersModels,
																							{
																								submodelId: {
																									by: "or",
																									filter: submodel
																								}
																							}),
															"partnerId",
															null,
															true),
			_citiesIds,
			_cities;
			
			if(_dealersIds && _dealersIds.length) {
				_currentDealers = bstdSystem.array.applyFilter2(_data.partners,
																{
																	partnerId: {
																		by: "or",
																		filter: _dealersIds
																	}
																});
				_citiesIds = bstdSystem.array.getArrayByField(_currentDealers,
																"cityId",
																null,
																true);
				_cities = bstdSystem.array.applyFilter2(_data.cities,
															{
																cityId: {
																	by: "or",
																	filter: _citiesIds
																}
															});
				bstdLTAPI.UI.createOptions("#cityId", _cities, null, null, "cityId", "cityName");
				$("#partnerId").empty();
			}
														
	};
	
	this.createModels = function(models, submodels) {
		var _i,
			_len,
			_curr,
			_len2,
			_i2,
			_item,
			_list = $("#modelsList");
		if(models instanceof Array) {
			if(_list.length) {
				_len = models.length;
				for(_i = 0; _i < _len; _i ++) {
					_curr = models[_i];
					_list.append(_item = $(bstdSystem.templater.getByTemplate(_modelsTempalte, _curr)));
					_item = $(".submodels", _item);
					sThis.createSubmodels(_item, bstdSystem.array.applyFilter(submodels, {modelId: _curr["modelId"]}));
				}
			}
		}
	};
	
	this.createSubmodels = function(ele, submodels) {
		var _i,
			_len,
			_item;
		ele = $(ele);
		if(submodels instanceof Array) {
			if(ele.length) {
				_len = submodels.length;
				for(_i = 0; _i < _len; _i ++) {
					ele.append($(bstdSystem.templater.getByTemplate(_submodelsTemplate, submodels[_i])));
				}
			}
		}
	};
	
	bstdLTAPI.actions.info({
								actionId: 1,
								callback: function(data) {
										_data = data;
										bstdSystem.onAfterLoad(_loadData);
									}
							});
});