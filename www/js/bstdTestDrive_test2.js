var bstdTestDrive = (new function() {
	var sThis = this,
		_modelsTempalte = '<div class="model">'
								+ '<div class="name">{#modelName#}</div>'
								+ '<div class="image"><img src="{#modelImage#}" alt="" /></div>'
								+ '<div class="submodels"></div>'
						+ '</div>',
		_submodelsTemplate = '<div class="submodelItem">'
								+ '<input id="id_{#submodelId#}" type="radio" name="modelId" value="{#submodelId#}"  />'
								+ '<label for="id_{#submodelId#}">{#modelName#}</label>'
							+ '</div>',
		_data,
		_currentDealers,
		_formObj,
		_dealerRcode,
		_activeDealers = location.params["dealer"],
		_activeDealersCount = 0;
	
	if(_activeDealers) {
		if(_activeDealers instanceof Array) {
			switch(_activeDealers.length) {
				case 0:
					_activeDealers = null;
					break;
				case 1:
					_activeDealers = _activeDealers[0];
					_activeDealersCount = 1;
					break;
				default:
					_activeDealersCount = _activeDealers.length;
			}
		}
		else {
			_activeDealersCount = 1;
		}
	}
	var _loadData = function() {
		var _dealers = {},
			_cities = {},
			_dealersSubmodels = {},
			_i,
			_len,
			_cd,
			_curr,
			_models,
			_submodels;
		
		if(_data) {
			if(!_activeDealers && location.params["_gaA"]) {
				bstdSystem.ga(location.params["_gaA"], null, "www.lexus.ru");//location.parent ? location.parent.basicHost : location.params && location.params.ref ? location.params.ref.basicHost : location.basicHost);
			}
			else {
				bstdSystem.ga('UA-9238963-7');
			}

			_submodels = _activeDealers ? bstdSystem.array.applyFilter(
														_data.submodels,
														bstdSystem.array.valuesByFields(
															bstdSystem.array.applyFilter2(
																		_data.partnersModels,
																		{
																			partnerId: {
																				by: "or",
																				filter: _activeDealers
																			}
																		}),
															"submodelId",
															true),
														1) : _data.submodels;
			
			_models = _activeDealers ? bstdSystem.array.applyFilter(
														_data.models,
														bstdSystem.array.valuesByFields(
															_submodels,
															"modelId",
															true),
														1) : _data.models;

			sThis.createModels(_models, _submodels);
			$("input[name=modelId]").on("change",
										function() {
											_changeSubmodel($(this).val());
										});
			if(_activeDealersCount) {
				$("#city").remove();
				$("#frmFields").append('<input type="hidden" name="fds" value="1">');
				if(_activeDealersCount == 1) {
					$("#dealer").hide();
					$("#dealer").remove();
					$("#frmFields").append('<input type="hidden" name="partnerId" value="' + _activeDealers + '">');
				}
			}
			else {
				$("#cityId").on("change",
								function() {
									_changeCity($(this).val());
								});
			}
			$("#frmFields")
				.on("succesfullSubmiting", function(e, data){
					if(typeof data.result == "object" && data.result.dealerRcode) {
						_dealerRcode = data.result.dealerRcode;
					}
					_formObj.disable();
					bstdAlert("Спасибо!<br />Ваша заявка на прохождение тест-драйва отправлена дилеру.<br />В ближайшее время представитель дилера свяжется с Вами.", "Запись на тест-драйв");
					//setTimeout(function() {
						if(bstdSystem.ga.isCrossdomain) {
							bstdSystem.ga.linkPost($("#frmFields").get(0));
							bstdSystem.ga.event('Test-Drive', 'Submit', bstdSystem.getBrandName(), undefined, false); //_activeDealers ? 'Dealer' : bstdSystem.getBrandName()
							bstdSystem.ga.event('Test-Drive', 'model', $('input[name="modelId"]:checked').val(), undefined, false);
							bstdSystem.ga.event('Test-Drive', 'dealer', _dealerRcode, undefined, false);
						}
						else {
							bstdSystem.ga.event('Click', 'Submit', _activeDealers ? 'Dealer' : 'Main', undefined, false);
							bstdSystem.ga.event('Click', 'dealer', _dealerRcode, undefined, false);
							bstdSystem.ga.event('Click', 'model', $('input[name="modelId"]:checked').val(), undefined, false);
						}
					//}, 10);
				})
				.on("validateError", function() {
					bstdAlert("Не заполнено или заполнено с ошибками одно или несколько полей.", "Запись на тест-драйв");
				});
			_formObj = new bstdForm("#frmFields",
									bstdForm.prototype.MODE_ADD,
									null,
									{
										action: "http://content.lexus-russia.ru/z/testdrives/",
										errorType: 1
									});
			$("#agreement").on("change", function() {
					if($("#agreement").prop("checked")) {
						$("#submit").show();
					}
					else {
						$("#submit").hide();
					}
				});
			if(window != top && window["bstdFrames"] && typeof bstdFrames.setHeight == "function") {
				bstdFrames.setHeight(100);
			}
		}
	};

	var _changeCity = function(city) {
		bstdLTAPI.UI.createOptions("#partnerId",
									bstdSystem.array.applyFilter(_currentDealers,
										{
											cityId: city
										}),
									null,
									null,
									"partnerId",
									"partnerName");
	};
	
	var _changeSubmodel = function(submodel) {
		var _dealersIds,
			_citiesIds,
			_cities;
		
		if(!_activeDealersCount || _activeDealersCount > 1) {
			_dealersIds = bstdSystem.array.getArrayByField(bstdSystem.array.applyFilter2(_data.partnersModels,
																							{
																								submodelId: {
																									by: "or",
																									filter: submodel
																								}
																							}),
															"partnerId",
															null,
															true);
			if(_dealersIds && _dealersIds.length) {
				_currentDealers = bstdSystem.array.applyFilter2(_data.partners,
																{
																	partnerId: {
																		by: "or",
																		filter: _dealersIds
																	}
																});
				if(_activeDealersCount > 1) {
					bstdLTAPI.UI.createOptions("#partnerId",
												_currentDealers,
												null,
												null,
												"partnerId",
												"partnerName");
				}
				if(!_activeDealersCount) {
					_citiesIds = bstdSystem.array.getArrayByField(_currentDealers,
																	"cityId",
																	null,
																	true);
					_cities = bstdSystem.array.applyFilter2(_data.cities,
															{
																cityId: {
																	by: "or",
																	filter: _citiesIds
																}
															});
					bstdLTAPI.UI.createOptions("#cityId", _cities, null, null, "cityId", "cityName");
					$("#partnerId").empty();
				}
			}
		}
	};
	
	this.createModels = function(models, submodels) {
		var _i,
			_len,
			_curr,
			_len2,
			_i2,
			_item,
			_list = $("#modelsList");

		if(models instanceof Array) {
			if(_list.length) {
				_len = models.length;
				for(_i = 0; _i < _len; _i ++) {
					_curr = models[_i];
					_list.append(_item = $(bstdSystem.templater.getByTemplate(_modelsTempalte, _curr)));
					_item = $(".submodels", _item);
					sThis.createSubmodels(_item, bstdSystem.array.applyFilter(submodels, {modelId: _curr["modelId"]}));
				}
			}
		}
	};
	
	this.createSubmodels = function(ele, submodels) {
		var _i,
			_len,
			_item;
		ele = $(ele);
		if(submodels instanceof Array) {
			if(ele.length) {
				_len = submodels.length;
				for(_i = 0; _i < _len; _i ++) {
					ele.append($(bstdSystem.templater.getByTemplate(_submodelsTemplate, submodels[_i])));
				}
			}
		}
	};
	(function() {
		var _sendData = {
				actionId: 1,
				callback: function(data) {
										_data = data;
										bstdSystem.onAfterLoad(_loadData);
									}
			};
		if(_activeDealers) {
			_sendData["partnerId"] = _activeDealers;
		}
		bstdLTAPI.actions.info(_sendData);
	})();
});