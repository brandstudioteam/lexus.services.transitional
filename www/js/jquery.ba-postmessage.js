(function($){
  var interval_id,
    last_hash,
    cache_bust = 1,
    rm_callback,
    window = this,
    FALSE = !1,
    postMessage = 'postMessage',
    addEventListener = 'addEventListener',
    _opera,
    p_receiveMessage,
    has_postMessage;
	_opera = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
	_opera = !!(_opera && parseFloat(_opera[1]) < 9.7);
  	has_postMessage = window[postMessage] && !_opera;
  $[postMessage] = function( message, target_url, target ) {
    if ( !target_url ) { return; }
    message = typeof message === 'string' ? message : $.param( message );
    target = target || parent;
    if ( has_postMessage ) {
      target[postMessage]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );
    } else if ( target_url ) {
      target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
    }
  };
  $.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
    if ( has_postMessage ) {
      if ( callback ) {
        rm_callback && p_receiveMessage();
        rm_callback = function(e) {
        	if(e.originalEvent) {
        		e = e.originalEvent;
        	}
          if ( ( typeof source_origin === 'string' && source_origin != "*" && e.origin !== source_origin )
            || ( $.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
            return FALSE;
          }
          callback( e );
        };
      }
      $(window).bind('message', rm_callback);
    } else {
      interval_id && clearInterval( interval_id );
      interval_id = null;
      if ( callback ) {
        delay = typeof source_origin === 'number'
          ? source_origin
          : typeof delay === 'number'
            ? delay
            : 100;
        interval_id = setInterval(function(){
          var hash = document.location.hash,
            re = /^#?\d+&/;
          if ( hash !== last_hash && re.test( hash ) ) {
            last_hash = hash;
            callback({ data: hash.replace( re, '' ) });
          }
        }, delay );
      }
    }
  };
})(jQuery);