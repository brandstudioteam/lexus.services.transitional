$(document).ready(function(){

    $("body").on('click', function(){
        if($('.select-top:hover').length) return false;
        $(".selector-close").hide();
    })

    $("div.select-top").on('click', function(){
        if( $(this).hasClass('disabled') ) return false;
        $(this).parent().find('.error_container').html('');
        $(this).next().show();
        if( $(this).hasClass('big-popup') )
            $("div."+$(this).data('popup')).show();

        if( $(this).hasClass('model-car') )
            $("div.model-list-wrap").show();

        if( $(this).hasClass('dealers-top') && $("ul.dealers-list li").length > 3 )
            $("img.close-dealers").show();

    })

    $("div.big-popup-wrap li").on('click', function(){
        $("div[data-popup="+$(this).parents("div.big-popup-wrap").data('select')+"]").html($(this).html());
        $(this).parents("div.big-popup-wrap").hide();
    })

    $("ul.select-list, div.model-list-wrap").hover(function(){},function(){
//        $(this).hide();
    })

   /*  $("ul.slider").width( $("li.slide").width() * $("li.slide").length ); */

    /* $("div.slider-next").on('click',function(){
        if($(this).hasClass('disabled')) return false;
        var goto = ( $('ul.slider').data('current')+1 > $("li.slide").length-1 )?0:$('ul.slider').data('current')+1;
        $('ul.slider').data('current', goto);
        slider_animate($('ul.slider').data('current'));
    }) */

    /* $("div.slider-prev").on('click',function(){
        if($(this).hasClass('disabled')) return false;
        var goto = ( $('ul.slider').data('current')-1 < 0 )?$("li.slide").length-1:$('ul.slider').data('current')-1;
        $('ul.slider').data('current', goto);
        slider_animate($('ul.slider').data('current'));
    }) */

    $("li.slide input, li.slide textarea").focus(function(){
        $(this).parent().find('.error_container').html('');
        if( $(this).hasClass('send-button') ) return false;

        if( $("div.keyboard-wrap").css('display') == 'none' ){
            $("div#lexusLogo").fadeOut();
            $("div.keyboard-wrap").fadeIn();
        }

        if( ($(this).hasClass('mileage') || $(this).hasClass('year') || $(this).hasClass('to-number') ) && !$(this).hasClass('active-field') )
            $("li.change-number").eq(0).trigger('click');

        if( $(this).hasClass('to-english') && !$(this).hasClass('active-field') )
            $("li.lang").eq(0).trigger('click');

        if( $(this).hasClass('to-russian') && !$(this).hasClass('active-field') )
            $("li.lang").eq(1).trigger('click');

        if( !$(this).hasClass('active-field') ){
            $("li.slide input, li.slide textarea").removeClass('active-field');
            $(this).addClass('active-field');
        }

    })

    $("input.send-button").on('click', function(){
        setTimeout(function(){
            if( is_error() ){
                $("div.overall-error").html('Часть полей, обязательных для заполнения, не заполнена. Пожалуйста, вернитесь к заполнению формы').show();
            }else{
                $("div.overall-error").hide();
                $("div.preloader_send").css('height',$(document).height()).show();
            }
        },100)
    })

    $("img.close-success-popup").on('click', function(){
        $("div.success_popup").hide();
        window.location.reload();
    })

   /*  $('div.keyboard-wrap li.letter').click(function(){
        if( $(this).hasClass('empty-letter') ) return false;
        if( $(this).hasClass('clear') ){
            $("input:not(.send-button), textarea").val('');
            $("div.select-top").html('');
            $("div#modelsList div.submodelItem label").removeClass('active');
            $("div.checkbox").removeClass('active');
            $("div.checkbox").find("input[type=checkbox]").prop('checked', false);
            slider_animate(0);
            $('ul.slider').data('current', 0);
            return false;
        }
        $('.active-field').simulate("key-sequence", {sequence: (($(this).hasClass('uppercase'))?$(this).html().toUpperCase():$(this).html())});
    })

    $('div.keyboard-wrap li.delete').click(function(){
        $('.active-field').val($('.active-field').val().substr(0, $('.active-field').val().length-1));
    })

    $('div.keyboard-wrap li.caps-lock').click(function(){
        $('div.keyboard-wrap li.letter').toggleClass('uppercase');
    })

    $('div.keyboard-wrap li.lang, div.keyboard-wrap li.change-number').click(function(){
        $("ul.keyboard").hide();
        $("ul.keyboard-"+$(this).data('lang')).show();
    }) */

    $("div.checkbox").on('click', function(){
        $(this).parent().find("div.checkbox").removeClass('active');
        $(this).addClass('active');

        $(this).parent().find("input[type=checkbox]").prop('checked', $(this).data('value'));
    })

    $("div#cityId").on('click', function(){
        $("div.content-wrap-test-drive").addClass('city-show');
        $("img.close-city-popup").show();
        $("div#cityId ul.itemsContainer li").off('click').on('click', function(){
            $("div.content-wrap-test-drive").removeClass('city-show');
            $("img.close-city-popup").hide();
        })
    })

    $("img.close-city-popup").on('click', function(){
        $("div#cityId ul.itemsContainer").hide();
        $(this).hide();
    })

    $("#phone").mask("+7 (000) 000-00-00");

    /* $("div.wrap").css('minHeight',$(document).height()+'px'); */

    $("div.checkbox-lexus").on('click', function(){
        $("input[name="+$(this).data('radio')+"]").removeClass('active-field');
        $("input[name="+$(this).data('radio')+"]").eq($(this).index()).trigger('click').addClass('active-field');
    })

    change_car_image();

    $("#frmFields").bind("succesfullSubmiting", function(e, data){
        $("div.success_popup").show();
        $("div.preloader_send").hide();
        setTimeout(function(){
            window.location.reload();
        },6000);
    })
})

/* function slider_animate(slide){
    if( slide == $("li.slide").length - 1 )
        $("div.slider-next").addClass('disabled');
    else
        $("div.slider-next").removeClass('disabled');

    if( slide == 0 )
        $("div.slider-prev").addClass('disabled');
    else
        $("div.slider-prev").removeClass('disabled');

    $("ul.slider").stop().animate({marginLeft:'-'+(slide*$("li.slide").width())+'px'}, {duration: 1200, easing: "easeInOutExpo", complete: function(){

        if(slide != 0 && $("div.submodelItem input[type=radio]:checked").val() >= 20 && $("div.keyboard-wrap").css('display') == 'none')
            $("div#lexusLogo").fadeIn();


        if( slide != 0 && $("div.submodelItem input[type=radio]:checked").val() < 20)
            $("div.keyboard-wrap").fadeIn();
    }});
    if( slide == 0 ){
        $("div#lexusLogo").fadeOut();
        $("div.keyboard-wrap").fadeOut();
    }
} */

function is_error(){
    for( var i = 0; i < $("div.form-error:not(.overall-error)").length; i++){
        if($("div.form-error:not(.overall-error)").eq(i).html() != '') return true;
    }
    return false;
}

function change_car_image(){
    if( $("div#modelsList").html() == '' ){
        setTimeout(change_car_image, 300);
        return false;
    }
    $("div.model").eq(0).find('div.image').html('<img src="/img/forms/lexus-is.jpg" alt="">');
    $("div.model").eq(1).find('div.image').html('<img src="/img/forms/lexus-gs.jpg" alt="">');
    $("div.model").eq(2).find('div.image').html('<img src="/img/forms/lexus-es.jpg" alt="">');
    $("div.model").eq(3).find('div.image').html('<img src="/img/forms/lexus-ls.jpg" alt="">');
    $("div.model").eq(4).find('div.image').html('<img src="/img/forms/lexus-rx.jpg" alt="">');
    $("div.model").eq(5).find('div.image').html('<img src="/img/forms/lexus-lx.jpg" alt="">');
    $("div.model").eq(6).find('div.image').html('<img src="/img/forms/lexus-gx.jpg" alt="">');
    $("div.model").eq(7).find('div.image').html('<img src="/img/forms/lexus-ct.jpg" alt="">');
    $("div.model").eq(8).find('div.image').html('<img src="/img/forms/lexus-nx.jpg" alt="">');

    /* $("div.model").eq(8).find('div.submodelItem').eq(0).find('label').append('<span>  / доступен с декабря 2014</span>');
    $("div.model").eq(8).find('div.submodelItem').eq(1).find('label').append('<span>  / доступен с декабря 2014</span>'); */
    $("div.model").eq(8).find('div.submodelItem').eq(2).find('label').append('<span>  / доступен с марта 2015</span>');
    /* $("div.model").eq(8).find('div.submodelItem').eq(3).find('label').append('<span>  / доступен с октября 2014</span>'); */

    setTimeout(function(){
        $("div#modelsList").show();

        $("div#modelsList div.submodelItem label").on('click', function(){

            $("div#modelsList div.submodelItem label").removeClass('active');
            $(this).addClass('active');

            /* $('ul.slider').data('current', 1); */
            /* slider_animate(1); */

            $("div.select-city-selector").removeClass('disabled');

            $("select#partnerId, div.dealers-top, div.select-city-selector").html('');

            setTimeout(function(){
                $("div.select-city").html('<img src="/img/forms/close-big-popup-lexus.png" class="close-big-popup">');
                var cities_array = Array();
                for( var i = 0; i < $("select#cityId option").length; i++ ){
                    if( typeof $("select#cityId option").eq(i).attr('value') != 'undefined' )
                        cities_array.push({id:$("select#cityId option").eq(i).attr('value'), title:$("select#cityId option").eq(i).html()});
                }

                var cities_line = Math.ceil(cities_array.length/4);
                for(var i = 0; i < 4; i++){
                    $("div.select-city").append('<ul></ul>');
                    for( var j = 0; j < cities_line; j++ ){
                        if( typeof cities_array[j+cities_line*i] != 'undefined' )
                            $("div.select-city ul").eq(i).append('<li data-id="'+cities_array[j+cities_line*i].id+'">'+cities_array[j+cities_line*i].title+'</li>');
                    }
                }

                $("div.select-city ul li").on('click', function(){

                    $("div.dealers-top").removeClass('disabled');

                    $("select#partnerId, div.dealers-top").html('');
                    $("div.select-city-selector").html($(this).html());
                    $("div.select-city").hide();
                    $("select#cityId option").removeAttr("selected");
                    $("select#cityId option[value=" + $(this).data('id') +"]").attr("selected","selected") ;
                    $("select#cityId").trigger('change');
                    set_partner();
                })

                $("img.close-big-popup").off('click').on('click', function(){
                    $("div.big-popup-wrap").hide();
                })

            },1000)
        })

    },300)
}

function set_partner(){
    if( $("select#partnerId").html() == '' ){
        setTimeout(set_partner,100);
        return false;
    }
    $("ul.dealers-list").html('');
    for(var i = 0; i < $("select#partnerId option").length; i++){
        if( typeof $("select#partnerId option").eq(i).attr('value') != 'undefined' )
            $("ul.dealers-list").append('<li data-id="'+$("select#partnerId option").eq(i).attr('value')+'">'+$("select#partnerId option").eq(i).html()+'</li>');
    }

    if( $("select#partnerId option").length > 3 )
        $("ul.dealers-list").addClass('more');
    else
        $("ul.dealers-list").removeClass('more');

    $("ul.dealers-list li").off('click').on('click', function(){
        $(this).parent().prev().html($(this).html());
        $("ul.select-list").hide();
        $("select#partnerId option").removeAttr("selected");
        $("select#partnerId option[value=" + $(this).data('id') +"]").attr("selected","selected") ;
    })

    if( $("select#partnerId option").length > 1 )
        $("div.dealers-top").html('<span style="color:grey">Выберите дилера</span>');
    else
        $("ul.dealers-list li").eq(0).trigger('click');

}