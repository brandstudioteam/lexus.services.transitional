$(document).ready(function () {

$('.custom_select').on('click', function () {
    $(this).children('.dropdown_menu').toggle();
})

$('.custom_select .dropdown_menu').on('click', '.item', function () {
    var setValue = $(this).text();
    $(this).closest('.custom_select').children('input').val(setValue);
} )


$('label.checkbox').on('click', function () {
    $(this).toggleClass('checked');
})

})