if(!window[bstdSets]) {
	var bstdSets = {};
}
if(!bstdSets["tips"]) {
	bstdSets["tips"] = {};
}
if(!bstdSets.tips["elementId"]) {
	bstdSets.tips["elementId"] = "bstdTips";
}
if(!bstdSets.tips["defailtClass"]) {
	bstdSets.tips["defailtClass"] = "tips";
}



//bstdSets.tips["animateOpen"] = "show";
//bstdSets.tips["animateClose"] = "hide";
bstdSets.tips["animateOpenParams"] = 0;
bstdSets.tips["animateCloseParams"] = 0;
bstdSets.tips["position"] = "auto"; //auto (0) | top (1) | left (2) | bottom (3) | right (4) | bottom-left (5) | bottom-right (6) | top-left (7) | top-right (8)
bstdSets.tips["linking"] = "cursor"; //cursor (1) | element (2)
	
//bstdSets.tips["autoActivate"] = true; //true | false или эквивалентные им значения



/**
 * Элемент всплывающая подсказка (tips)
 * 
 * Настройки:
 * elementId - идентификатор DOM-элемента, используемого как контейнер подсказок
 * defailtClass - имя класса стиля. Используется при создании контейнера скриптом
 * 
 * Анимаци подсказок при открытии и закрытии.
 * animateOpen - имя плагина jQuery, производящего анимацию при открытии подсказки
 * animateOpenParams - параметры, передаваемые плагину в animateOpen
 * animateClose - имя плагина jQuery, производящего анимацию при закрытии подсказки
 * animateCloseParams - параметры, передаваемые плагину в animateClose
 * По умолчанию используется show и hide. В качестве параметров им передаются animateOpenParams и animateCloseParams соответственно
 * 
 * position - позация подсказки относительно элемента или курсора (см. linking). По умолчанию используется позиция снизу и слева
 * linking - привязка подсказки. Может принимать значения element и cursor. По умолчанию используется cursor.
 * 			При привязке подсказки к курсору, подсказка перемещается за курсором, пока курсор находится в границах элемента.
 * 			При привязке подсказки к элементу, подсказка существует статично, пока элемент находится в границах элемента.
 * 
 * 
 * autoActivate - автоматически активирует подсказки.
 * 
 * Для автоматической активации или дефктивации можно установить атрибут bstd_tips_set тега body документа со значениями
 * для декактивации:
 * 		none, 0, false, off, null или пустой строки;
 * для активации любое значение эквивалентное true.
 * 
 * Для автоматического отображения подсказок в соответствующих DOM-элементах необходимо установить атрибут tips, значение которого соответствует тексту подсказки.
 * 
 * Для установки подсказок из скрипта или добаления новых элементов, для которых необходимо устанвить подсказку, метод activate. Подсказки должны иметь атрибут tips.
 * Для индивидуальной установки подсказок из скрипта используется метод set
 * 
 * {@link} bstdSystem.js
 * 
 */
var bstdTips = new (
	/**
	 * @constructor
	 */
	function() {
		var thisObj = this;
		
		var /** @type {HTMLElement} HTML-элемент контейнера*/
			_tips,
			/** @type {HTMLElement} HTML-элемент для которого устанавливается подсказка*/
			_HTMLElement,
			/** @type {integer} Тип позиции подсказки относительно элемента*/
			_position,
			_isOpen = false,
			_autoActivate;
		
		var _getTips = function() {
			_tips = $(bstdSets.tips.elementId);
			if(!_tips.length) {
				_tips = $("<div id='" + bstdSets.tips.elementId + "' class='" + bstdSets.tips.defailtClass + "'>");
				$("body").append(_tips);
			}
			_tips.hide();
		}
		
		bstdSystem.onAfterLoad(function() {
			var _attr = $("body").attr("bstd_tips_set");
			_getTips();
			/*
			 * Проверяем установку автоматического подключения всплывающих подсказок
			 */
			if(_attr !== undefined) {
				bstdSets.tips.autoActivate = !(!_attr || /(none|0|false|off|null)/i.test(_attr));
			}
			else if(bstdSets.tips.autoActivate === undefined) {
				bstdSets.tips.autoActivate = false;
			}
			if(bstdSets.tips.autoActivate) {
				_autoActivate = true;
				thisObj.activate();
			}
		});
		
		/*
		 * Активируем подсказки 
		 */
		this.activate = function() {
			if(bstdSets.tips.autoActivate) {
				bstdSystem.onAfterLoad(function() {
					$("[tips]").each(function() {
						thisObj.set(this, $(this).attr("tips"));
					});
				});
			}
		}
		
		/**
		 * Открывает подсказку
		 * @param {Object} ev Объект события
		 * @param {HTMLElement} HTMLElement HTML-элемент, для которого устанавливается подсказка
		 * @param {string} text Текст посказки
		 * @param {integer} position Тип позиции подсказки относительно элемента
		 * @param {integer} timeOut Время существования подсказки после потери фокуса
		 */
		thisObj.open = function(ev, HTMLElement, text, position) {
			if(!HTMLElement || HTMLElement.nodeType != 1 || !text) {
				return false;
			}
			if(!_tips) {
				_getTips();
			}
			_tips.html(text); //TODO: дописать возможность установки заголовков, типов: подсказка, ошибка и т.п.
			if(_HTMLElement && _HTMLElement != HTMLElement) {
				thisObj.close();
			}
			_HTMLElement = $(HTMLElement);
			if(position) {
				_position = position;
			}
			_setPosition(ev);
			if(bstdSets.tips.linking === undefined || bstdSets.tips.linking == "cursor" || bstdSets.tips.linking == 1) {
				_HTMLElement.bind("mousemove", _move);
			}
			_HTMLElement.bind("mouseleave", thisObj.close);
			_tips.css("z-index", 20000);
			_isOpen = true;
			
			if(bstdSets.tips.animateOpen && typeof _tips[bstdSets.tips.animateOpen] == "function") {
				_tips[bstdSets.tips.animateOpen](bstdSets.tips.animateOpenParams);
			}
			else {
				if(bstdSets.tips.animateOpenParams) {
					_tips.show(bstdSets.tips.animateOpenParams);
				}
				else {
					_tips.show();
				}
			}
			return false;
		};
		
		/**
		 * Перемещает подсказку за указателем мыши
		 * @param {Object} ev Объект события
		 */
		var _move = function(ev) {
			_setPosition(ev);
			ev.preventDefault();
			ev.stopPropagation();
			return false;
		};
		
		/**
		 * Закрывает подсказку
		 */
		thisObj.close = function() {
			if(bstdSets.tips.animateClose && typeof _tips[bstdSets.tips.animateClose] == "function") {
				_tips[bstdSets.tips.animateClose](bstdSets.tips.animateCloseParams);
			}
			else {
				_tips.hide(bstdSets.tips.animateCloseParams);
			}
			_HTMLElement.unbind("mouseleave", thisObj.close);
			_HTMLElement.unbind("mousemove", _move);
			_HTMLElement = _position = null;
			_isOpen = false;
		};
		
		/**
		 * Устанавливает элементу обработчик события для отображения подсказки
		 * @param {HTMLElement} HTMLElement HTML-элемент, для которого устанавливается подсказка
		 * @param {string} text Текст посказки
		 */
		thisObj.set = function(HTMLElement, text) {
			if(typeof HTMLElement.bstdTipsFn == "function") return;
			HTMLElement.bstdTipsFn = function(txt, ele) {return function(ev) {thisObj.open(ev, ele, txt);};}(text, HTMLElement);
			$(HTMLElement).bind("mouseenter", HTMLElement.bstdTipsFn);
		};
		
		/**
		 * Удаляет у элемента обработчики события для отображения подсказки
		 * @param {HTMLElement} HTMLElement HTML-элемент, для которого устанавливается подсказка
		 */
		thisObj.unset = function(HTMLElement, reset) {
			var _attr = $(HTMLElement).attr("tips");
			if(!HTMLElement || HTMLElement.nodeType != 1 || typeof HTMLElement.bstdTipsFn != "function") return false;
			$(HTMLElement).unbind("mouseenter", HTMLElement.bstdTipsFn);
			delete HTMLElement.bstdTipsFn;
			if(reset && _attr) {
				thisObj.set(HTMLElement, _attr);
			}
			return true;
		};
		
		/**
		 * Устанавливает позицию подсказки
		 * @param {HTMLElement} HTMLElement HTML-элемент, для которого устанавливается подсказка
		 * @param {Object} ev Объект события
		 */
		var _setPosition = function(ev) {
			var /** @type {integer} Координата X*/
				_x,
				/** @type {integer} Координата Y*/
				_y,
				/** @type {HTMLElement} HTML-элемент для которого устанавливается подсказка*/
				_pos = $(_HTMLElement).offset();
			switch(_position) {
				case "top":
					//TODO: Дописать
					_x = parseInt(_pos.left);
					_y = parseInt( _pos.top) + parseInt(_tips.offset().height);
					break;
					/*
				case "bottom":
					_x = _pos.left;
					_y = _pos.top + HTMLElement.offsetHeight;
					break;
				case "left":
					_x = _pos.left + _tips.offsetWidth;
					_y = _pos.top;
					//TODO: Дописать
					break;
				case "right":
					//TODO: Дописать
					_x = _pos.left + HTMLElement.offsetWidth;
					_y = _pos.top;
					break;
					*/
				default:
					_position = 1;
					_x = ev.pageX;
					_y = ev.pageY;
			}
			_tips.css("left", (_x + 10) + "px");
			_tips.css("top", (_y + 10) + "px");
		};
});