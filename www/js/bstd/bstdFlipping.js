/*
				bstd_flipping_set
				bstd_flipping
				bstd_flipping_slide
				bstd_flipping_next
				bstd_flipping_previous
				bstd_flipping_stop
				bstd_flipping_start
				bstd_flipping_coursor
				bstd_flipping_timer
*/
/**
 * Класс для организации работы с галереями, слайд-шоу и т.п., где необходима смена отображения слайдов (картинок, фреймов, элементов и т.п.) из установленного набора слайдов.
 * 
 */				
var bstdFlipping = function(params) {
	var sThis = this,
		_timer,
		_timerAuto,
		_currentIndex = null,
		_previewIndex = null,
		_disabled = false,
		_cicle = false,
		_turn,
		_isButton,
		
		/**
		 * Контейнер листалки. Необходим, если установлен режим raiseEvent - генерации события
		 */
		_container = params && params.container ? $(params.container) : null,
		/**
		 * коллекция jQuery DOM-элементов слайдов или селектор для получения такой коллекции
		 */
		_slides = params && params.slides ? $(params.slides) : [],
		/**
		 * Направление промотки. back или -1 - назад (в сторону убывания индексов), иное - вперед (в сторону возрастания индексов). 
		 */
		_course = params && params.course && (params.course == "back" || params.course == -1) ? -1 : 1,
		/**
		 * Временной интервал между автоматической сменой слайдов
		 */
		_timeout = params && params.timeout ? params.timeout : 2000,
		/**
		 * Флаг продолжения автоматической смены кадров после нажатия пользователем кнопок.
		 * Если флаг опущен, то после нажатия кнопки ручной смены слайдов, автоматическая смена прекратится.
		 */
		_autostart = params && params.autostart ? params.autostart : true,
		/**
		 * Время ожидания запуска режима автоматической смены слайдов.
		 */
		_autostartTimeout = params && params.autostartTimeout ? params.autostartTimeout : 2000,
		/**
		 * Флаг изменения напраления смены слайдов, при нажатии пользователем кнопки смены слайдов в направлении, противоположном текущему.
		 */
		_autoReverse = params && params.autoReverse ? params.autoReverse : true,
		/**
		 * Пользовательская функция обработки смены слайдов. В функцию возвращается объект формата:
		 * {
		 *		preview: <object>, - Сменяемый слайд
		 *		previewIndex: <integer>, - Индекс сменяемого слайд
		 *		current: <object>, - Сменяющий слайд
		 *		currentIndex: <integer>, - Индекс сменяющего слайда
		 *		course: <integer> - Направление смены: 1 - вперед, -1 - назад
		 *	}
		 */
		_onFlipping = params && params.onFlipping ? params.onFlipping : null,
		/**
		 * Флаг, необходимости генерации события промотки. Имя события flipping.
		 */
		_raiseEvent = params && params.raiseEvent ? params.raiseEvent : false,
		/**
		 * Функция или плагин jQuery для обработки открытия слайда.
		 * Функция должна принимать два агрумента: параметры и функцию обратного вызова.
		 * По завершении работы данной функции или плагина, должна вызываться переданная ей функцию обратного вызова.
		 */
		_showSlide = params && params.showSlide ? params.showSlide : null,
		/**
		 * Функция или плагин jQuery для обработки закрытия слайда.
		 * Функция должна принимать два агрумента: параметры и функцию обратного вызова.
		 * По завершении работы данной функции или плагина, должна вызываться переданная ей функцию обратного вызова.
		 */
		_hideSlide = params && params.hideSlide ? params.hideSlide : null,
		/**
		 * Параметры, передаваемые в функцию или плагин jQuery для обработки открытия слайда.
		 */
		_showParams = params && params.showParams ? params.showParams : null,
		/**
		 * Параметры, передаваемые в функцию или плагин jQuery для обработки закрытия слайда.
		 */
		_hideParams = params && params.hideParams ? params.hideParams : null,
		/**
		 * Флаг создания ошибки, если при изменении индекса методом index значения выходит за допустимые границы.
		 * Если флаг поднят, то если поднят флаг _SYS_DEBUG, выбростися исключение (Error), если флаг _SYS_DEBUG опущен, вернется false,
		 * иначе значение индекса будет приведено в соответствие с допустимым диапазоном
		 */
		_errorIsNotValideIndex = params && params.returnFalseIsNotValideIndex ? params.returnFalseIsNotValideIndex : false,
		/**
		 * Для старта можно использовать параметр start со значением эквивалентным true, например,
		 * params.start = 1
		 */
				
		_slidersContainer = params && params.slidersContainer ? params.slidersContainer : _container;
		
	
	if(!_raiseEvent && !_onFlipping && !_showSlide && !_hideSlide) {
		throw new Error("raiseEvent and onFlipping is null or undefined");
	}
	
	if(_raiseEvent && !_container) {
		throw new Error("Container is not found");
	}
	
	var _do = function(callback, autostart, autoReverse, course, button) {
		if(button) { 
			if(_cicle) {
				return sThis;
			}
			_isButton = true;
		}
		sThis.stop();
		if(callback) {
			if(typeof callback != "function" && typeof callback == "object") {
				autostart = !!callback["autostart"];
				autoReverse = !!callback["autoReverse"];
				if(callback["callback"]) {
					callback = callback["callback"];
				}
			}
		}
		_previewIndex = _currentIndex;
		if(course > 0) {
			_currentIndex ++;
			if(_currentIndex == _slides.length) {
				_currentIndex = 0;
			}
		}
		else {
			_currentIndex --;
			if(_currentIndex < 0) {
				_currentIndex = _slides.length - 1;
			}
		}
		sThis.flip(null, callback);
		if(autostart) {
			if(autoReverse) {
				_course = course;
			}
			_timerAuto = window.setTimeout(sThis.start, _autostartTimeout);
		}
		_isButton = false;
		return sThis;
	};
	
	var _end = function() {
		_cicle = false;
		if(_turn) {
			sThis.flip(_turn.newIndex, _turn.callback);
			_turn = null;
		}
	};
	
	var _addInTurn = function(newIndex, callback) {
		_turn = {
			newIndex: newIndex,
			callback: callback
		};
	};
	
	/**
	 * Производит смену слайдов
	 * newIndex - Целочисленное положительное значение от 0 до количество слайдов - 1. Индекс слайда, который необходимо отобразить.
	 * callback - Функция обратного вызова.
	 */
	this.flip = function(newIndex, callback) {
		var _detail,
			_prewIndex;
		
		if(_cicle) {
			if(!_isButton) {
				_addInTurn(newIndex, callback);
			}
			return sThis;
		}
		
		if(newIndex) {
			_prewIndex = _currentIndex;
			if(!sThis.index(newIndex)) {
				return false;
			}
			_previewIndex = _prewIndex;
		}
		if(_previewIndex === null && _currentIndex === null) {
			if(_SYS_DEBUG) {
				throw new Error("Slide is not found");
			}
			return false;
		}
		
		_detail = {
			preview: _slides[_previewIndex],
			previewIndex: _previewIndex,
			current: _slides[_currentIndex],
			currentIndex: _currentIndex,
			course: _course,
			callback: typeof callback == "function" ? function(callback, detail) {return function() {_end(); callback(detail);}}(callback, _detail) : _end
		};
		
		if(_hideSlide && _showSlide) {
			_cicle = true;
			$(_slides[_previewIndex])[_hideSlide](_hideParams,
				function() {
					$(_slides[_currentIndex])[_showSlide](_showParams,
							typeof callback == "function" ? function(callback, detail) {return function() {_end(); callback(detail);}}(callback, _detail) : _end);
				}
			);
		}
		else if(_onFlipping) {
			_cicle = true;
			_onFlipping(_detail, typeof callback == "function" ? function(callback, detail) {return function() {_end(); callback(detail);}}(callback, _detail) : _end);
		}
		if(_raiseEvent) {
			_container.trigger("flipping", _detail);
		}
		return sThis;
	};
	
	this.isStart = function() {
		return !!_timer
	};
	
	/**
	 * Устанавливает или возвращает текущее направление перемещения
	 * newCourse - направление: forward - вперед (в сторону возрастания индексов), back - назад (в сторону убывания индексов)
	 */
	this.course = function(newCourse, asString) {
		if(newCourse) {
			_course = newCourse == "back" || newCourse == -1 ? -1 : 1;
			return sThis;
		}
		else {
			return asString ? _course == 1 ? "forward" : "back" : _course;
		}
	};
	
	/**
	 * Устанавливает или возвращает текущий индекс
	 * newCourse - направление: forward - вперед (в сторону возрастания индексов), back - назад (в сторону убывания индексов)
	 */
	this.index = function(newIndex) {
		if(newIndex) {
			newIndex = parseInt(newIndex);
			if(isNaN(newIndex)) {
				if(_SYS_DEBUG) {
					throw new Error("newIndex is not integer");
				}
				return false;
			}
			if(!_slides.length) {
				if(_errorIsNotValideIndex) {
					if(_SYS_DEBUG) {
						throw new Error("index can not be installed until added a single slide");
					}
					return false;
				}
			}
			if(newIndex > _slides.length) {
				if(_errorIsNotValideIndex) {
					if(_SYS_DEBUG) {
						throw new Error("index is greater than the permissible value");
					}
					return false;
				}
			}
			if(newIndex < 0) {
				if(_errorIsNotValideIndex) {
					if(_SYS_DEBUG) {
						throw new Error("index is less than the permissible value");
					}
					return false;
				}
			}
			_currentIndex = newIndex;
			return sThis;
		}
		else {
			return _currentIndex;
		}
	};
	
	/**
	 * Добавляет слайд в коллекцию слайдов
	 * slide - объект.
	 * index - необязательный. Индекс, на место которого необходимо поместить слайд. Не заменяет имеющиеся слайды
	 */
	this.addSlide = function(slide, index) {
		if(index === undefined) {
			_slides.push(slide);
			_slidersContainer.append(slide);
		}
		else {
			_slides.splice(index, 0, slide);
			$(slide).insertBefore($(_slidersContainer.children().get(index)));
		}
		return sThis;
	};
	
	/**
	 * Удаляет слайд из коллекции слайдов
	 * slideOrIndex - индекс (числовой) слайда или слайд (объект)
	 */
	this.deleteSlide = function(slideOrIndex) {
		if(typeof slideOrIndex == "object") {
			_slides.splice($.inArray(slideOrIndex, _slides), 1);
		}
		else if(!isNaN(slideOrIndex)) {
			_slides.splice(slideOrIndex, 1);
		}
		return sThis;
	};
	
	/**
	 * Очищает коллекцию слайдов
	 */
	this.clearSlide = function() {
		_slides = [];
		_currentIndex = null;
		_slidersContainer.empty();
		return sThis;
	};
	
	/**
	 * Начинает автоматическую промотку слайдов (слайд-шоу)
	 */
	this.start = function() {
		if(_timerAuto) {
			window.clearTimeout(_timerAuto);
		}
		if(_slides.length) {
			if(_currentIndex === null) {
				_currentIndex = _course > 0 ? 0 : _slides.length;
			}
			_timer = window.setInterval(function() {_do(null, _autostart, _autoReverse, _course);}, _timeout);
		}
		return sThis;
	};
	
	/**
	 * Останавливает автоматическую промотку слайдов (слайд-шоу)
	 */
	this.stop = function() {
		if(_timer) {
			window.clearInterval(_timer);
			_timer = null;
		}
		if(_timerAuto) {
			window.clearTimeout(_timerAuto);
			_timerAuto = null;
		}
		return sThis;
	};

	/**
	 * Переключает к предыдущему слайду. Используется для управления сторонним скриптом, в том числе для синхронизации работы двух flipping'ов.
	 * Останавливает автоматическую промотку слайдов.
	 * Если параметр _autostart эквивалентен истиному значению, то запустит таймер для возобновления автоматической промотки слайдов (слайд-шоу).
	 * Если параметр _autoReverse эквивалентен истиному значению, то будет установлено прямом направление промотки 
	 */
	this.previous = function(callback, autostart, autoReverse) {
		return _do(callback, autostart, autoReverse, -1);
	};
	
	/**
	 * Переключает к следующему слайду. Используется для управления сторонним скриптом, в том числе для синхронизации работы двух flipping'ов.
	 * Останавливает автоматическую промотку слайдов.
	 * Если параметр _autostart эквивалентен истиному значению, то запустит таймер для возобновления автоматической промотки слайдов (слайд-шоу).
	 * Если параметр _autoReverse эквивалентен истиному значению, то будет установлено обратное направление промотки  
	 */
	this.next = function(callback, autostart, autoReverse) {
		return _do(callback, autostart, autoReverse, 1);
	};

	/**
	 * Переключает к предыдущему слайду. Используется для пользовательских действий - нажатий на кнопки переключения
	 * Останавливает автоматическую промотку слайдов.
	 * Если параметр _autostart эквивалентен истиному значению, то запустит таймер для возобновления автоматической промотки слайдов (слайд-шоу).
	 * Если параметр _autoReverse эквивалентен истиному значению, то будет установлено прямом направление промотки 
	 */
	this.previousButton = function(callback, autostart, autoReverse) {
		return _do(null, _autostart, _autoReverse, -1);
	};
	
	/**
	 * Переключает к следующему слайду. Используется для пользовательских действий - нажатий на кнопки переключения
	 * Останавливает автоматическую промотку слайдов.
	 * Если параметр _autostart эквивалентен истиному значению, то запустит таймер для возобновления автоматической промотки слайдов (слайд-шоу).
	 * Если параметр _autoReverse эквивалентен истиному значению, то будет установлено обратное направление промотки  
	 */
	this.nextButton = function() {
		return _do(null, _autostart, _autoReverse, 1);
	};
	
	/**
	 * Устанавливает или возвращает режим игнорирования нажатия кнопок
	 */
	this.disable = function(newMode) {
		if(newMode === undefined) {
			return _disabled;
		}
		else {
			_disabled = !!newMode;
			return sThis;
		}
	};
	
	this.timeout = function(newTimeout) {
		if(newTimeout) {
			newTimeout = Math.abs(parseInt(newTimeout));
			if(isNaN(newTimeout)) {
				if(_SYS_DEBUG) {
					throw new Error("index can not be installed until added a single slide");
				}
				return false; //sThis;
			}
			_timeout = newTimeout;
			return sThis;
		}
		else {
			return _timeout;
		}
	};
	
	if(params && params.buttonNext) {
		$(params.buttonNext).bind("click", sThis.nextButton);
	}
	if(params && params.buttonPrevious) {
		$(params.buttonPrevious).bind("click", sThis.previousButton);
	}
	if(params && params.buttonStop) {
		$(params.buttonStop).bind("click", sThis.stop);
	}
	if(params && params.buttonStart) {
		$(params.buttonStart).bind("click", sThis.start);
	}
	if(params && params.elementCoursor) {
		$(params.elementCoursor).bind("change", function(ev){
				var _val = $(this).val();
				sThis.coursore(_val == -1 || _val == "back" ? -1 : 1);
			});
	}
	if(params && params.elementTimer) {
		$(params.elementTimer).bind("change", function(ev){
				sThis.timeout($(this).val());
			});
	}
	
	if(params && params.start) {
		if(_slides.length) {
			sThis.start();
		}
	}
	
	
	
/*
window.addEventListener('DOMContentLoaded', function() {
  
    var block1 = document.getElementById('block1');

    block1.addEventListener('touchstart', function(event) {
        event.preventDefault();
        block1.className = 'touched';
        block1.innerHTML = 'touched';
    }, false);

    block1.addEventListener('touchmove', function(event) {
        var x = event.touches[0].pageX;
        var y = event.touches[0].pageY;
        block1.style.left = x - 100;
        block1.style.top = y - 50;
        block1.innerHTML = 'touchmove' + x + '=' + y;
    }, false);

    block1.addEventListener('touchend', function(event) {
        event.preventDefault();
        block1.className = '';
        block1.innerHTML = 'dropped';
    }, false);

function updateOrientation() {
    event.preventDefault();
    var orientation = window.orientation;  
    switch(orientation) {  
      case 90: case -90:  
        orientation = 'landscape';  
      break;  
      default:  
        orientation = 'portrait';  
    }
    var h1 = document.getElementById('h1');
    h1.innerHTML = 'Touch demo ' + orientation;
}

var body = document.getElementById('body');
window.addEventListener('orientationchange', updateOrientation, false);

}, false);

 */
	
	
	
	
	delete(params);
};

var bstdFlippingAuto = function() {
	var _attr = $("body").attr("bstd_flipping_set");
	if(_attr !== undefined) {
		$("[bstd_flipping]").each(function(){
			var sThis = $(this),
				_attrParams = sThis.attr("bstd_flipping"),
				_i,
				_val,
				_params = {};
			if(_attrParams) {
				_attrParams = _attrParams.split(",");
				for(_i = 0; _i < _attrParams.length; _i++) {
					_val = _attrParams[_i].split(":");
					_params[_val[0]] = _val[1];
				}
			}
			_params["container"] = sThis;
			_params["slidersContainer"] = sThis.find("[bstd_flipping_container]");;
			_params["slides"] = sThis.find("[bstd_flipping_slide]");
			_params["buttonNext"] = sThis.find("[bstd_flipping_next]");
			_params["buttonPrevious"] = sThis.find("[bstd_flipping_previous]");
			_params["buttonStop"] = sThis.find("[bstd_flipping_stop]");
			_params["buttonStart"] = sThis.find("[bstd_flipping_start]");
			_params["elementCoursor"] = sThis.find("[bstd_flipping_coursor]");
			_params["elementTimer"] = sThis.find("[bstd_flipping_timer]");
			(sThis[0].bstdFlipping = new bstdFlipping(_params));
		});
	}
};

if(window["bstdSystem"] && bstdSystem.onAfterLoad) {
	bstdSystem.onAfterLoad(bstdFlippingAuto);
}