//http://contacts.toyota.ru/z/findPage?eservice={eservice}&corp={corp}&cityId={cityId}&summer={summer}&tsm={tsm}&kuzov={kuzov}&autumn={autumn}&formula_toyota={formula_toyota}&spring_service={spring_service}&tires={tires}&winter={winter}&directreception={directreception}&tested={tested}&tradein={tradein}&refi=findDealers&ref=
var _dealers,
	_filters = [],
	_cityFilter,
	_predefinedCity,
	_facilitiesRef = {alias:{}, id:{}},
	_facilitiesRefLoaded,
	_facilitiesRefLoadedHandler = [];

var facilitiesRefOnLoaded = function(handler) {
	if(typeof handler == "function") {
		if(_facilitiesRefLoaded) {
			handler();
		}
		else {
			_facilitiesRefLoadedHandler.push(handler);
		}
	}
};

var setFacilitiesRef = function(data) {
	var _i,
		_item,
		_len;
	
	if(data && data instanceof Array) {
		_len = data.length;
		for(_i = 0; _i < _len; _i ++) {
			_item = data[_i];
			_facilitiesRef.alias[_item["facilityAlias"]] = _facilitiesRef.id[_item["facilityId"]] = _item;
		}
		_facilitiesRefLoaded = true;
		_len = _facilitiesRefLoadedHandler.length;
		for(_i = 0; _i < _len; _i ++) {
			_facilitiesRefLoadedHandler[_i]();
		}
	}
};

bstdToyotaAPI.facilities.getFacilities(setFacilitiesRef, null, window["_facilitiesType"]);

var getFilteredDealers = function() {
	var _dealers = [];
	
	$("#dealersTable > tbody td").each(function(){
		var _id = $(this).attr("data-dealer-id");
		if(_id) {
			_dealers.push(_id);
		}
	});
	return _dealers;
};

var _applyFilters = function() {
	var _visibleTD = [],
		_tmpVisibleTD = [],
		_first = true,
		j,
		i,
		_city = $("#city"),
		
		_visibleCities = [],
		_tmpCities = [];
	
	if(_filters.length) {
		for(j = 0; j < _filters.length; j ++) {
			var _filter = _filters[j];
			
			if(_first) {
				for(i = 0; i < _dealers.length; i ++) {
					if($("[data-facility-id=" + _filter + "]", _dealers[i]).length) {
						_tmpVisibleTD.push(_dealers[i]);
					}
				};
				_first = false;
			}
			else {
				for(i = 0; i < _visibleTD.length; i ++) {
					if($("[data-facility-id=" + _filter + "]", _visibleTD[i]).length) {
						_tmpVisibleTD.push(_visibleTD[i]);
					}
				};
			}
			_visibleTD = _tmpVisibleTD;
			_tmpVisibleTD = [];
		};
	}
	else {
		_visibleTD = _dealers;
	}
	
	$("option", _city).hide();
	
	$("option[disabled]", _city).show();
	
	if(_cityFilter) {
		for(i = 0; i < _visibleTD.length; i ++) {
			$("option[value=" + $(_visibleTD[i]).attr("data-city-id") + "]", _city).show();
			if($(_visibleTD[i]).attr("data-city-id") == _cityFilter) {
				_tmpVisibleTD.push(_visibleTD[i]);
			}
		};
		_visibleTD = _tmpVisibleTD;
		_tmpVisibleTD = [];
	}
	else {
		for(i = 0; i < _visibleTD.length; i ++) {
			$("option[value=" + $(_visibleTD[i]).attr("data-city-id") + "]", _city).show();
		};
	}
	_changeTable(_visibleTD);
};
/*
var _changeTable = function(visibilityDealers) {
	var _tbody = $("#dealersTable > tbody").empty(),
		_tr,
		_tdCount = 2,
		i;

	if(visibilityDealers) {
		$("#dealersTable").hide();
		for(i = 0; i < visibilityDealers.length; i ++) {
			if(_tdCount == 2) {
				_tbody.append(_tr = $("<tr>"));
				_tdCount = 0;
			}
			_tr.append(visibilityDealers[i]);
			_tdCount ++;
		};
		if(_tdCount == 1) {
			_tr.append($("<td>").html("&nbsp;"));
		}
		$("#dealersTable").show();
	}
	if(window["bstdFrames"] && bstdFrames.setHeight && window != top) {
		bstdFrames.setHeight(50);
	}
};
*/
var _changeTable = function(visibilityDealers) {
	var _tbody = $("#dealersTable > tbody").empty(),
		_tr,
		_tdCount = 2,
		i,
		_curr;

	if(visibilityDealers) {
		$("#dealersTable").hide();
		for(i = 0; i < visibilityDealers.length; i ++) {
			_curr = $(visibilityDealers[i]);
			if(_curr.attr("data-type") == "city") {
				if(_tdCount == 1) {
					_tr.append($("<td>").html("&nbsp;"));
					_tdCount = 2;
				}
				_tbody.append($("<tr>").append(_curr));
				continue;
			}
			if(_tdCount == 2) {
				_tbody.append(_tr = $("<tr>"));
				_tdCount = 0;
			}
			_tr.append(_curr);
			_tdCount ++;
		};
		if(_tdCount == 1) {
			_tr.append($("<td>").html("&nbsp;"));
		}
		$("#dealersTable").show();
	}
	if(window["bstdFrames"] && bstdFrames.setHeight && window != top) {
		bstdFrames.setHeight(50);
	}
};

$(document).ready(function(){
	var _ps,
		_i;
	
	_dealers = $("#dealersTable > tbody td");
	
	$("#filters tr").attr("data-filter-predefined", 0);
	
	$("#city").bind("change", function() {
		_cityFilter = $("#city").val();
		_applyFilters();
	});
	
	$("input[type=reset]").bind("click", function() {
		$("#filters tr[data-filter-predefined=0] input").attr("checked", false);
		$('#city option:selected').attr("selected", false);
		_filters = [];
		$("tr[data-filter-predefined=1]").each(function() {
			_filters.push($(this).attr("data-filter"));
		});
		_cityFilter = _predefinedCity;
		_applyFilters();
	});

	if(location.params && location.params.facilityId) {
		_ps = location.params.facilityId;
		if(_ps instanceof Array) {
			for(_i = 0; _i < _ps.length; _i ++) {
				_ps[_i] = decodeURIComponent(_ps[_i]);
				$("#facility_" + _ps[_i]).attr("checked", true);
				$("input", $("tr[data-filter=" + _ps[_i] + "]").attr("data-filter-predefined", "1")).attr("disabled", true);
			}
		}
		else {
			_ps = decodeURIComponent(_ps);
			if(_ps != "{facilityId}") {
				$("#facility_" + _ps).attr("checked", true);
				$("input", $("tr[data-filter=" + _ps + "]").attr("data-filter-predefined", "1")).attr("disabled", true);
			}
		}
	}
	
	facilitiesRefOnLoaded(function() {
		var _i,
			_ps;
		
		for(_i in location.params) {
			if(location.params[_i] != ("{" + _i + "}") && _facilitiesRef.alias[_i] !== undefined) {
				_ps = _facilitiesRef.alias[_i]["facilityId"];
				$("#facility_" + _ps).attr("checked", true);
				$("input", $("tr[data-filter=" + _ps + "]").attr("data-filter-predefined", "1")).attr("disabled", true);
			}
		}
	});
	if(window["defaultFilter"]) {
		$("#filters tr[data-filter=" + window["defaultFilter"] + "] input").prop("checked", true);
		_filters = [window["defaultFilter"]];
	}
	_applyFilters();
	
	if(location.params && location.params.cityId && !(location.params.cityId instanceof Array)) {
		_ps = decodeURIComponent(location.params.cityId);
		if(_ps != "{cityId}") {
			$("#city option[value=" + location.params.cityId + "]").attr("selected", true);
			_cityFilter = _predefinedCity = location.params.cityId;
		}
	}
	if(window["useRadio"]) {
		$("#filters tr[data-filter]").bind("click", function(e){
			var thisEle = $(this),
				_e,
				_activity = thisEle.attr("data-filter-activity");
			_filters = [thisEle.attr("data-filter")];
			//$("input", thisEle).attr("checked", true);
			_applyFilters();
		});
	}
	else {
		$("#filters tr[data-filter-predefined=0]").bind("click", function(e){
			var thisEle = $(this),
				_e,
				_activity = thisEle.attr("data-filter-activity");
			if(thisEle.attr("data-filter-predefined") == 1) {
				$("input", thisEle).attr("checked", true);
				return;
			}
			if(_activity == "1") {
				thisEle.attr("data-filter-activity", "0");
				if((_e = $.inArray(thisEle.attr("data-filter"), _filters)) != -1) {
					_filters.splice(_e, 1);
				}
				$("input", thisEle).attr("checked", false);
			}
			else {
				thisEle.attr("data-filter-activity", "1");
				_filters.push(thisEle.attr("data-filter"));
				$("input", thisEle).attr("checked", true);
			}
			_applyFilters();
		});
	}
});