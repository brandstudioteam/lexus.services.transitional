﻿var bstdURL, bstdTemplater, bstdElementsTypes;

var bstdError = function(message, code, DOMElement, targetObject) {
	this.getMessage = function() {
		return message;
	};
	
	this.getCode = function() {
		return code;
	};
	
	this.getDOMElement = function() {
		return DOMElement;
	};
	
	this.getObject = function() {
		return targetObject;
	};
};

var bstdSystem = window["bstdSystem"] = (new function() {
	var sThis = this,
		_loaded = !!$("body").length,
		_doc,
		_onLoadCash = new Array(),
		_scriptRepository,
		_cssRepository,
		_initLoad,
		_prepareInit,
		_loadPermission,
		_loadScope,
		_loadAccountData,
		_initScriptLoadCount = 0,
		_brandId = _brandId = /lexus/.test(location.host) ? 2 : 1,
		_isIE = (function() {
					var _m;
					return (_m = navigator.appVersion.match(/\MSIE\s+(\d+\.\d\b)/)) && _m[1] && _m[1] < 10;
				})(),
		_defaultLang = {
			langId: 152,
			name: "русский",
			intSymbols: "ru"
		};

	var _load = function(){
		if(_loaded) {
			return;
		}
		if(!_initLoad && !_loadScope && !_loadAccountData && !_loadPermission) {
			var _j;
			_loaded = true;
			for(_j = 0; _j < _onLoadCash.length; _j ++) {
				_onLoadCash[_j]();
			}
			_onLoadCash = [];
		}
	};
	
	this.getBrandId = function() {
		return _brandId;
	};
	
	this.getBrandName = function () {
		return _brandId == 1 ? "Toyota" : "Lexus";
	};
					
	if(!_loaded) {
		$(document).ready(_load);
	}

	this.brand = function(brandId) {
		if(brandId !== undefined) {
			_brandId = brandId;
		}
		else {
			return _brandId;
		}
	};
	
	this.isIE = _isIE;
	
	this.isIE8 = (function() {
		var browser = navigator.appName,
			b_version,
			re,
			res;
		if (browser == "Microsoft Internet Explorer"){
			b_version = navigator.appVersion;
			re = /\MSIE\s+(\d\.\d\b)/;
			res = b_version.match(re);
			if (res && res[1] && res[1] <= 8){
				return true;
			}
		}
		return false;
	})();

	this.correctCheckRadio = function(labelClass) {
		if(sThis.isIE8) {
			$("body").on("click", labelClass, function() {
				var _ele = $(this),
					_children = _ele.parent().children("#" + _ele.attr("for"));
				if(_children.attr("type").toLowerCase() == "checkbox") {
					if(_children.is(":checked")) {
						_ele.removeClass("checked");
						_ele.removeClass("select");
						_children.prop("checked",  false).trigger("change");
					}
					else {
						_ele.addClass("checked");
						_ele.addClass("select");
						_children.prop("checked",  "checked").trigger("change");
					}
				}
				else if(!_children.is(":checked")) {
					$("[name=" + _children.prop("name") + "]", $(_children.prop("form"))).each(function() {
						$("label[for=" + $(this).prop("checked", false).prop("id") + "]").removeClass("checked").removeClass("select");
					});
					$("label[for=" + _children.prop("id") + "]", $(_children.prop("form"))).addClass("checked").addClass("select");
					_children.prop("checked", "checked").trigger("change");
				}
			});
		}
	};
	
	this.isLexus = _brandId == 2;
	this.isToyota = _brandId == 1;
	
	this.IsCookie = document.cookie ? true : false;
	
	this.isLoaded = function () {
		return _loaded;
	};
	
	this.onAfterLoad = function(callback) {
		if(_loaded && !_initLoad && !_loadScope && !_loadAccountData && !_loadPermission) {
			callback();
		}
		else {
			if($.inArray(callback,_onLoadCash) == -1) {
				_onLoadCash.push(callback);
			}
		}
	};
	
	var _preparePage = function() {
		//bstd_tips
	};

	this.languages = new (function() {
		var _langeages = {},
			_current,
			_default;
		
		this.getCurrent = function() {
			return _current;
		};
		
		this.setCurrent = function(lang) {
			if(_langeages[lang]) {
				$(document).trigger("changeLang", {lang: lang, oldLang: _current});
				_current = lang;
				return true;
			}
			return false;
		};
		
		this.getDefault = function() {
			return _default;
		};
		
		this.setDefault = function(lang, setCurrent) {
			if(_langeages[lang]) {
				$(document).trigger("changeLangDefault", {lang: lang, oldLang: _default});
				_default = lang;
				if(!_current || setCurrent) {
					_current = lang;
				}
			}
		};
		
		this.add = function(lang) {
			var _i,
				_len,
				_langs = [],
				_add = function(lang) {
					_langeages[lang["langId"]] = lang;
					_langs.push(lang["langId"]);
				};
			
			if(lang instanceof Array) {
				_len = lang.length;
				for(_i = 0; _i < _len; _i ++) {
					_add(lang[_i]);
				}
			}
			else if(typeof lang == "object") {
				_add(lang);
			}
			if(_langs.length) {
				$(document).trigger("addLang", {lang: _langs});
			}
		};
		
		this.remove = function(lang) {
			var _i,
				_len,
				_langs = [],
				_remove = function(lang) {
					delete _langeages[lang["langId"]];
					_langs.push(lang["langId"]);
				};
			
			if(lang instanceof Array) {
				_len = lang.length;
				for(_i = 0; _i < _len; _i ++) {
					_remove(lang[_i]);
				}
			}
			else if(lang) {
				_remove(lang);
			}
			if(_langs.length) {
				$(document).trigger("removeLang", {lang: _langs});
			}
		};
	});
	
	this.messagesManager = new (function() {
		var mThis = this,
			_messages = {};
		this.addAlias = function(alias, message, lang) {
			var _i,
				_len,
				_curr,
				_currLang,
				_lang,
				_addArray = function(alias) {
					var _lang;
					_len = alias.length;
					for(_i = 0; _i < _len; _i ++) {
						_curr = alias[_i];
						if(_curr instanceof Array) {
							_lang = _curr[2] ? _curr[2] : _currLang;
							if(!_messages[_lang]) {
								_messages[_lang] = {};
							}
							_messages[_lang][_curr[0]] = _curr[1];
						}
						else if(typeof _curr == "object") {
							if(!_messages[_currLang]) {
								_messages[_currLang] = {};
							}
							_messages[_currLang][_curr["alias"]] = _curr["message"];
						}
					}
				};
			if(alias instanceof Array) {
				_addArray(alias);
			}
			else if(typeof alias == "object") {
				_currLang = alias._lang ? alias._lang : lang ? lang : sThis.languages.getCurrent();
				if(alias.messages) {
					if(alias.messages instanceof Array) {
						_addArray(alias);
					}
					else {
						for(_i in alias) {
							if(!_messages[_currLang]) {
								_messages[_currLang] = {};
							}
							_messages[_currLang][_curr["alias"]] = _curr["message"];
						}
					}
				}
				else {
					for(_i in alias) {
						if(!_messages[_currLang]) {
							_messages[_currLang] = {};
						}
						_messages[_currLang][_curr["alias"]] = _curr["message"];
					}
				}
			}
			else if(alias && message) {
				_currLang = lang ? lang : sThis.languages.getCurrent();
				if(!_messages[_currLang]) {
					_messages[_currLang] = {};
				}
				_messages[_currLang][alias] = message;
			}
		};
		
		this.getMessage = function(alias, lang) {
			var _currLang = lang ? lang : sThis.languages.getCurrent();
			return _messages[_currLang] ? _messages[_currLang][alias] : alias;
		};
	});
	
	
	this.prepareResult = function(response, getResultOnly) {
		if(typeof response == "object" && response.responseText !== undefined) {
			response = response.responseText;
		}
		if(typeof response == "string") {
			response = $.parseJSON(response);
		}
		if(response && typeof response == "object") {
			//Проверяем общее сообщение
			if(response.message) {
				bstdAlert(sThis.messagesManager.getMessage(response.message));
			}
			if(response.error) {
				//Проверяем общее сообщение об ошибке
				if(response.error_message) {
					bstdAlert(sThis.messagesManager.getMessage(response.errorMessage));
				}
			}
			if(getResultOnly) {
				return response.result;
			}
			return response; 
		}
		return null;
	};
	
	this.Request = new function() {
		var sR = this,
			Req = {},
			_testedData = {};
		
		var _getTestedData = function(name) {
			return _testedData[name];
		};
		
		var _response = function(response, callback, errorCallback, getResultOnly) {
			var response = sThis.prepareResult(response, getResultOnly);
			if(getResultOnly) {
				callback(response);
			}
			else {
				if(response.error) {
					if(errorCallback) {
						errorCallback(response);
					}
					else {
						callback(response);
					}
				}
				else {
					callback(response);
				}
			}
		};
		
		this.setTestedData = function(name, data) {
			_testedData[name] = typeof data == "object" ? $.toJSON(data) : data;
		};
		
		this.Send = function(url, data, controller, action, callback, errorCallback, getFull, method, asIs, testedDataName) {
			var _ajaxData = {
				url: 		url,
				type:		method && method.toLowerCase() == "get" ? "get" : "post",
				dataType:	'json'
			};
			if(testedDataName) {
				setTimer(function(callback, errorCallback, getResultOnly, testedDataName) {
								return function() {
									_response(_getTestedData(testedDataName), callback, errorCallback, getResultOnly);
								};
							}(callback, errorCallback, getResultOnly, testedDataName), 10);
			}
			else {
				_ajaxData.complete = function(callback, errorCallback, getResultOnly) {
					return function(data) {
						_response(data, callback, errorCallback, getResultOnly);
					};
				}(callback, errorCallback, !getFull);
				
				if(controller || controller) {
					if(data) {
						data = {
							d: data
						};
					}
					else {
						data = {};
					}
					if(controller) {
						data.a = controller;
					}
					if(action) {
						data.b = action;
					}
				}
				if(data) {
					_ajaxData.data = {prq: $.toJSON(data)};
				}
				$.ajax(_ajaxData);
			}
		};
		
		
		
		var _responseJSONP = function(response, callback, errorCallback, fn, getResultOnly) {
			var response = sThis.prepareResult(response, getResultOnly);
			if(_isIE) {
				window[fn] == "";
			}
			else {
				delete(window[fn]);
			}
			if(!getResultOnly) {
				if(response.error) {
					if(errorCallback) {
						errorCallback(response);
					}
					else {
						callback(response);
					}
				}
				else {
					callback(response);
				}
			}
			else {
				callback(response);
			}
		};
		
		//this.JSONP = function(url, controller, action, data, callback, errorCallback, sendAsJSON, getResultOnly, testedDataName) {
		this.JSONP = function(url, data, callback, errorCallback, sendAsJSON, getResultOnly, testedDataName) {
			var _fn = "jsonpF_" + Math.random().toString().split(".")[1];
			if(testedDataName) {
				setTimeout(function(callback, errorCallback, getResultOnly, testedDataName) {
								return function() {
									_response(_getTestedData(testedDataName), callback, errorCallback, getResultOnly);
								};
							}(callback, errorCallback, getResultOnly, testedDataName), 10);
			}
			else {
				window[_fn] = (function(callback, errorCallback, fn) {
					return function(response) {
						_responseJSONP(response, callback, errorCallback, fn, getResultOnly);
					};
				}(callback, errorCallback, _fn, getResultOnly));
				
				if(sendAsJSON) {
					data["pjb"] = 1;
				}
				
				$.ajax({
					url: url,
					data: data,
					jsonpCallback: _fn,
					dataType: "jsonp"
				});
			}
		};
		
		
		sR.RegisterFileUploader = function(i, ObjMetod, URL, Controller, Action, Params, callback) {
			i.fnct = function(i, ObjMetod, URL, Controller, Action, Params, callback) {
								return function() {
									bstdSystem.Request.SendUpload(i, ObjMetod, URL, Controller, Action, Params, null, callback);
								};}(i, ObjMetod, URL, Controller, Action, Params, callback);
			$(i).bind("change", i.fnct);
		};
		
		/**
		*	Метод используется для отправки файлов
		*/
		sR.SendUpload=function(i, ObjMetod, URL, Controller, Action, Params, notClone, callback) {
			if(!i || !ObjMetod || !Controller || !Action) return false;
			var _clone,
				_parent,
				_frm,
				_newForm,
				_ifrm,
				iKey=URL + ":" + Controller + ":" + Action + ":" + Math.random(),
				_ifrmName = 'uploarerTransitFrame' + iKey;
			
			i = $(i);
			
			Req[iKey]={};
			Req[iKey].Metod=ObjMetod;
			Req[iKey].Rq={};
			
			if(Controller) {
				Req[iKey].Rq["a"] = Controller;
			}
			if(Action) {
				Req[iKey].Rq["b"] = Action;
			}
			
			if(typeof Params=="object")
				for(var p in Params)
					Req[iKey].Rq.d[p]=Params[p];
			Req[iKey].Sending=true;

			if(!notClone) {
				_parent = i.parent();
				_clone = i.clone(true);
				i.unbind("change", i[0].fnct);
				_parent.append(_clone);
				
				sR.RegisterFileUploader(i, ObjMetod, URL, Controller, Action, Params, null, callback);
			}
			
			$("body").append($("<DIV>").css("width", 0)
										.css("height", 0)
										.css.append(Req[iKey].Trans = $('<iframe name="' + _ifrmName + '" src="' + _baseURL + '">').css("width", 0)
																																	.css("height", 0)
																																	.hide())
										.hide()
										.append(
												c = $("<FORM>").attr("action", URL ? (URL.indexOf("://") != -1 ? location.protocol + "//" + URL : URL) : location.protocol + "//" + location.host)
																		.attr("method", "POST")
																		.attr("enctype", "multipart/form-data")
																		.hide()
																		.append(i)
																		.append($('<input type="hidden" name="prq">').val($.toJSON([Req[iKey].Rq])))
																		.append($('<input type="hidden" name="t">').val("fu"))
																		.attr("target", _ifrmName)
										)
			);
			
			(function(e, callback){
				    var tm;
					if (!e[0].contentWindow){
						tm = window.setTimeout(arguments.callee,100);
				         return;
				    }
					window.clearTimeout(tm);
					e.load(function(ifrm, callBack) {
							return function() {
								ResponseUpload(ifrm, callBack);
							};
						}(e, callback));
				})(_ifrm, ObjMetod);
		
			_newForm.submit().remove();
			
			if(typeof callback == "function") {
				callback(i);
			}
		};
		
		var ResponseUpload = function(e, callback) {
			var returned = "";
			if(!e.tagName.toLowerCase()=="iframe") return;
				//throw new Error("Transport Element is not found");
			//e.contentWindow.location = 'about:blank';
			//if(e.contentWindow.name) returned = JSON.parse(e.contentWindow.name)
			
			if(e.contentWindow.document)
				returned = $.parseJSON($("body", $(e.contentWindow.document)).children().first().html());
			
			$(e).parent().remove();
			
			callback(returned);
		};
	};
	
			
	this.loadScript = function(script, callback, reload, key) {
		var _scripts,
			_tmp,
			_load;
		if(!script) {
			return false;
		}
		_load = function(script, callback, key, type) {
			var _ele = document.createElement('script');
			_ele.setAttribute("type", type || "text/javascript");
			_ele.setAttribute("src", script);
			if(typeof callback == "function") {
				if(_ele.onreadystatechange !== undefined) {
					_ele.onreadystatechange = function(_ele, script, callback, key) {
						return function () { //  IE
							if (_ele.readyState == 'complete' || _ele.readyState == 'loaded') {
								setTimeout(function() {callback(script, key);}, 10);
							}
						};
					}(_ele, script, callback, key)
				}
				else {
					_ele.onload = function(script, callback, key) {
						return function() {
							callback(script, key);
						};
					}(script, callback, key);
				}
			}
			$("head")[0].appendChild(_ele);
		};
		if(reload) {
			_tmp = sThis.URL.parseURL(script);
			_tmp.params["_"] = Math.random().toString().split(".")[1];
		}
		else {
			_tmp = sThis.URL.parseURL(script);
			_tmp.params = {};
			_tmp.hash = "";
			
			_scripts = $("script[src^='" + sThis.URL.URLObject2String(_tmp) + "']");
			if(_scripts.length) {
				if(callback) {
					setTimeout(function(script, key) {return function(){callback(script, key, 1);}}(script, key), 10);
				}
			}
			else {
				_load(script, callback, key) 
			}
		}
	};
	
	this.loadCSS = function(css, media) {
		//TODO: prepare css
		$('<link rel="stylesheet" type="text/css" media="' + (media ? media : "screen") + '" href="' + css + '">').appendTo($("head"));
	};
	
	this.prepareInit = function(init) {
		var _scriptPath = "",
			_cssPath = "",
			_i,
			_len,
			_current,
			_loadedScript = function(script, key, already) {
				_initScriptLoadCount --;
				if(_initScriptLoadCount == 0 && !_prepareInit) {
					_initLoad = false;
					setTimeout(_load, 100);
				}
			};
		_prepareInit = 1;
		if(typeof init == "object") {
			if(init.scriptPath) {
				_scriptPath = init.scriptPath;
			}
			if(init.cssPath) {
				_cssPath = init.cssPath;
			}
			if(init.inclideScript instanceof Array) {
				_len = init.inclideScript.length;
				for(_i = 0; _i < _len; _i ++) {
					_current = init.inclideScript[_i] ;
					if(!/^http:\/\//.test(_current[0])) {
						if(_current[0].charAt(0) != "/") {
							_current[0] = _scriptPath + _current[0];
						}
					}
					_initLoad = true;
					_initScriptLoadCount  ++;
					sThis.loadScript(_current[0], _loadedScript, _current[2], null, _current[1]);
				}
			}
			if(typeof init.requireScript == "object") {
				for(_i in init.requireScript) {
					_current = init.requireScript[_i] ;
					if(!/^http:\/\//.test(_current[0])) {
						if(_current[0].charAt(0) != "/") {
							_current[0] = _scriptPath + _current[0];
						}
					}
					_initLoad = true;
					_initScriptLoadCount  ++;
					sThis.loadScript(_current[0], _loadedScript, _current[2], _i, _current[1]);
				}
			}
			if(init.inclideCSS instanceof Array) {
				_len = init.inclideCSS.length;
				for(_i = 0; _i < _len; _i ++) {
					_current = init.inclideCSS[_i] ;
					if(!/^http:\/\//.test(_current[0])) {
						if(_current[0].charAt(0) != "/") {
							_current[0] = _cssPath + _current[0];
						}
					}
					sThis.loadCSS(_current[0], _current[1]);
				}
			}
			if(init.messages) {
				sThis.messagesManager.addAlias(init.messages);
			}
			if(init.access) {
				if(init.access.permissions) {
					if(init.access.permissions instanceof Array && init.access.permissions.length) {
						sThis.access.loadPermission(init.access.permissions);
						_loadPermission = true;
					}
					if(init.access && init.access.scope) {
						sThis.access.loadScope();
						_loadScope = true;
						_loadAccountData = true;
					}
				}
			}
			if(typeof init.testedData == "object") {
				for(_i in init.testedData) {
					sThis.Request.setTestedData(_i, init.testedData[_i]);
				}
			}
			if(init.languages instanceof Array) {
				sThis.languages.add(init.languages);
			}
			else {
				sThis.languages.add(_defaultLang);
			}
			if(init.langDefault) {
				sThis.languages.setDefault(init.langDefault, true);
			}
			else {
				sThis.languages.setDefault(_defaultLang["langId"], true);
			}
		}
		_prepareInit = false;
	};
	
	this.String = new function() {
		var sString=this,
		trimRegex= /^[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]+|[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]+$/g,
		trimHTMLRegex= /^(<br[^>]*?>|&nbsp;|[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000])+|(<br[^>]*?>|&nbsp;|[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000])+$/gi,
		escapeRe= /('|\\)/g,
		formatRe= /\{(\d+)\}/g,
		escapeRegexRe= /([-.*+?^${}()|[\]\/\\])/g,
		unSpaceRegex= /[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]/g,
		keyStr= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		/**
		 * Удаляет пробелы сначала и конца строки, оставляя пробелы внутри строки без изменения
		 * @example
			var s = '  foo bar  ';
			alert('-' + s + '-');		 //выводит "- foo bar -"
			alert('-' + sW.t.String.trim(s) + '-');  //выводит "-foo bar-"
		 * @param {String} string Строка для удаления пробелов
		 * @return {String} Строка без пробелов в начале и конце
		 */
		this.trim= function(string) {
			return (string||"").replace(trimRegex, "");
		};
		
		this.trimHTML=function(string){
			return string.replace(trimHTMLRegex, "");
		};
		
		this.unSpace=function(string,holder) {
			holder=(holder)?holder:"_";
			return string.replace(unSpaceRegex, holder);
		};
	
		// UTF-8 encode / decode by Johan Sundstr?m
		this.utf8=function(s) {
			return unescape( encodeURIComponent( s ) );
		};
		this.unUtf8=function(s) {
			return decodeURIComponent( escape( s ) );
		};
		
		this.base64 = function (input) {
			var output = "";
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;

			if(!input) return "";
			
			input = sString.utf8(input);

			while (i < input.length) {

				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output +
				keyStr.charAt(enc1) + keyStr.charAt(enc2) +
				keyStr.charAt(enc3) + keyStr.charAt(enc4);

			}

			return output;
		};
		this.unBase64 = function (input) {
			var output = "";
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0;
			
			if(!input) return "";
			
			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

			while (i < input.length) {

				enc1 = keyStr.indexOf(input.charAt(i++));
				enc2 = keyStr.indexOf(input.charAt(i++));
				enc3 = keyStr.indexOf(input.charAt(i++));
				enc4 = keyStr.indexOf(input.charAt(i++));

				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;

				output = output + String.fromCharCode(chr1);

				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}

			}

			output = sString.unUtf8(output);

			return output;

		};
	
		this.excerpt = function(str,len){
			len=+len;str=""+str;
			str=this.getPlainText(str);
			if(!str || !len || len<4 || str.length<=len) return str;
			return str.substring(0,len-3)+"...";
		};
		
		this.md5=function(str) {
			// http://kevin.vanzonneveld.net
			// +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
			// + namespaced by: Michael White (http://getsprink.com)
			// +    tweaked by: Jack
			// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			// +      input by: Brett Zamir (http://brett-zamir.me)
			// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			// -    depends on: utf8
			// *     example 1: md5('Kevin van Zonneveld');
			// *     returns 1: '6e658d4bfcb59cc13f96c14450ac40b9'
			var xl;
			
			var rotateLeft = function (lValue, iShiftBits) {
				return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
			};
			
			var addUnsigned = function (lX, lY) {
				var lX4,
				lY4,
				lX8,
				lY8,
				lResult;
				lX8 = (lX & 0x80000000);
				lY8 = (lY & 0x80000000);
				lX4 = (lX & 0x40000000);
				lY4 = (lY & 0x40000000);
				lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
				if (lX4 & lY4) {
					return (lResult^0x80000000^lX8^lY8);
				}
				if (lX4 | lY4) {
					if (lResult & 0x40000000) {
						return (lResult^0xC0000000^lX8^lY8);
					} else {
						return (lResult^0x40000000^lX8^lY8);
					}
				} else {
					return (lResult^lX8^lY8);
				}
			};
			
			var _F = function (x, y, z) {
				return (x & y) | ((~x) & z);
			};
			var _G = function (x, y, z) {
				return (x & z) | (y & (~z));
			};
			var _H = function (x, y, z) {
				return (x^y^z);
			};
			var _I = function (x, y, z) {
				return (y^(x | (~z)));
			};
			
			var _FF = function (a, b, c, d, x, s, ac) {
				a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
				return addUnsigned(rotateLeft(a, s), b);
			};
			
			var _GG = function (a, b, c, d, x, s, ac) {
				a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
				return addUnsigned(rotateLeft(a, s), b);
			};
			
			var _HH = function (a, b, c, d, x, s, ac) {
				a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
				return addUnsigned(rotateLeft(a, s), b);
			};
			
			var _II = function (a, b, c, d, x, s, ac) {
				a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
				return addUnsigned(rotateLeft(a, s), b);
			};
			
			var convertToWordArray = function (str) {
				var lWordCount;
				var lMessageLength = str.length;
				var lNumberOfWords_temp1 = lMessageLength + 8;
				var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
				var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
				var lWordArray = new Array(lNumberOfWords - 1);
				var lBytePosition = 0;
				var lByteCount = 0;
				while (lByteCount < lMessageLength) {
					lWordCount = (lByteCount - (lByteCount % 4)) / 4;
					lBytePosition = (lByteCount % 4) * 8;
					lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
					lByteCount++;
				}
				lWordCount = (lByteCount - (lByteCount % 4)) / 4;
				lBytePosition = (lByteCount % 4) * 8;
				lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
				lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
				lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
				return lWordArray;
			};
			
			var wordToHex = function (lValue) {
				var wordToHexValue = "",
				wordToHexValue_temp = "",
				lByte,
				lCount;
				for (lCount = 0; lCount <= 3; lCount++) {
					lByte = (lValue >>> (lCount * 8)) & 255;
					wordToHexValue_temp = "0" + lByte.toString(16);
					wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
				}
				return wordToHexValue;
			};
			
			var x = [],
			k,
			AA,
			BB,
			CC,
			DD,
			a,
			b,
			c,
			d,
			S11 = 7,
			S12 = 12,
			S13 = 17,
			S14 = 22,
			S21 = 5,
			S22 = 9,
			S23 = 14,
			S24 = 20,
			S31 = 4,
			S32 = 11,
			S33 = 16,
			S34 = 23,
			S41 = 6,
			S42 = 10,
			S43 = 15,
			S44 = 21;
			
			str = sString.utf8(str);
			x = convertToWordArray(str);
			a = 0x67452301;
			b = 0xEFCDAB89;
			c = 0x98BADCFE;
			d = 0x10325476;
			
			xl = x.length;
			for (k = 0; k < xl; k += 16) {
				AA = a;
				BB = b;
				CC = c;
				DD = d;
				a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
				d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
				c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
				b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
				a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
				d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
				c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
				b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
				a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
				d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
				c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
				b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
				a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
				d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
				c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
				b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
				a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
				d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
				c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
				b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
				a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
				d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
				c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
				b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
				a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
				d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
				c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
				b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
				a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
				d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
				c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
				b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
				a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
				d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
				c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
				b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
				a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
				d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
				c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
				b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
				a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
				d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
				c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
				b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
				a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
				d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
				c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
				b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
				a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
				d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
				c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
				b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
				a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
				d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
				c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
				b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
				a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
				d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
				c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
				b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
				a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
				d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
				c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
				b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
				a = addUnsigned(a, AA);
				b = addUnsigned(b, BB);
				c = addUnsigned(c, CC);
				d = addUnsigned(d, DD);
			}
			
			var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);
			
			return temp.toLowerCase();
		};
		
		this.encrypt=function(theText,key) {
			var i,ii=0,rnd,
			output = "",
			textSize = theText.length,
			key=sString.md5(key), // хэш ключа, чтобы гарантировать набор символов и длину (?)
			keylength=key.length; // длина ключа (на случай использования другого хеша или не использоваия таквого)
			
			for (i = 0; i < textSize; i++) {
				output += String.fromCharCode(theText.charCodeAt(i) + key.charCodeAt(ii) ); // добавляем к результату символ с солью
				ii++; if(ii==keylength) ii=0;
			}
			return output;
		};
		this.unEncrypt=function(theText,key) {
			var i,ii=0,output = "",
			textSize = theText.length,
			key=sString.md5(key),
			keylength=key.length;// длина ключа (на случай использования другого хеша или не использоваия таквого)
			
			for (i = 0; i < textSize; i++) {
				output += String.fromCharCode(theText.charCodeAt(i) - key.charCodeAt(ii) );
				ii++; if(ii==keylength) ii=0;
			}
			return output;
		};
		
		/**
		 * Возвращает чистый текст из html сроки
		 */
		this.getPlainText = function(text){
			return st.iT(sW.New.Span(false,false,text))
		};
		
		function get_html_translation_table (table, quote_style) {
			// http://kevin.vanzonneveld.net
			// +   original by: Philip Peterson
			var entities = {},
				hash_map = {},
				decimal;
			var constMappingTable = {},
				constMappingQuoteStyle = {};
			var useTable = {},
				useQuoteStyle = {};

			// Translate arguments
			constMappingTable[0] = 'HTML_SPECIALCHARS';
			constMappingTable[1] = 'HTML_ENTITIES';
			constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
			constMappingQuoteStyle[2] = 'ENT_COMPAT';
			constMappingQuoteStyle[3] = 'ENT_QUOTES';

			useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
			useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';

			if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
				throw new Error("Table: " + useTable + ' not supported');
				// return false;
			}

			entities['38'] = '&amp;';
			if (useTable === 'HTML_ENTITIES') {
				entities['160'] = '&nbsp;';
				entities['161'] = '&iexcl;';
				entities['162'] = '&cent;';
				entities['163'] = '&pound;';
				entities['164'] = '&curren;';
				entities['165'] = '&yen;';
				entities['166'] = '&brvbar;';
				entities['167'] = '&sect;';
				entities['168'] = '&uml;';
				entities['169'] = '&copy;';
				entities['170'] = '&ordf;';
				entities['171'] = '&laquo;';
				entities['172'] = '&not;';
				entities['173'] = '&shy;';
				entities['174'] = '&reg;';
				entities['175'] = '&macr;';
				entities['176'] = '&deg;';
				entities['177'] = '&plusmn;';
				entities['178'] = '&sup2;';
				entities['179'] = '&sup3;';
				entities['180'] = '&acute;';
				entities['181'] = '&micro;';
				entities['182'] = '&para;';
				entities['183'] = '&middot;';
				entities['184'] = '&cedil;';
				entities['185'] = '&sup1;';
				entities['186'] = '&ordm;';
				entities['187'] = '&raquo;';
				entities['188'] = '&frac14;';
				entities['189'] = '&frac12;';
				entities['190'] = '&frac34;';
				entities['191'] = '&iquest;';
				entities['192'] = '&Agrave;';
				entities['193'] = '&Aacute;';
				entities['194'] = '&Acirc;';
				entities['195'] = '&Atilde;';
				entities['196'] = '&Auml;';
				entities['197'] = '&Aring;';
				entities['198'] = '&AElig;';
				entities['199'] = '&Ccedil;';
				entities['200'] = '&Egrave;';
				entities['201'] = '&Eacute;';
				entities['202'] = '&Ecirc;';
				entities['203'] = '&Euml;';
				entities['204'] = '&Igrave;';
				entities['205'] = '&Iacute;';
				entities['206'] = '&Icirc;';
				entities['207'] = '&Iuml;';
				entities['208'] = '&ETH;';
				entities['209'] = '&Ntilde;';
				entities['210'] = '&Ograve;';
				entities['211'] = '&Oacute;';
				entities['212'] = '&Ocirc;';
				entities['213'] = '&Otilde;';
				entities['214'] = '&Ouml;';
				entities['215'] = '&times;';
				entities['216'] = '&Oslash;';
				entities['217'] = '&Ugrave;';
				entities['218'] = '&Uacute;';
				entities['219'] = '&Ucirc;';
				entities['220'] = '&Uuml;';
				entities['221'] = '&Yacute;';
				entities['222'] = '&THORN;';
				entities['223'] = '&szlig;';
				entities['224'] = '&agrave;';
				entities['225'] = '&aacute;';
				entities['226'] = '&acirc;';
				entities['227'] = '&atilde;';
				entities['228'] = '&auml;';
				entities['229'] = '&aring;';
				entities['230'] = '&aelig;';
				entities['231'] = '&ccedil;';
				entities['232'] = '&egrave;';
				entities['233'] = '&eacute;';
				entities['234'] = '&ecirc;';
				entities['235'] = '&euml;';
				entities['236'] = '&igrave;';
				entities['237'] = '&iacute;';
				entities['238'] = '&icirc;';
				entities['239'] = '&iuml;';
				entities['240'] = '&eth;';
				entities['241'] = '&ntilde;';
				entities['242'] = '&ograve;';
				entities['243'] = '&oacute;';
				entities['244'] = '&ocirc;';
				entities['245'] = '&otilde;';
				entities['246'] = '&ouml;';
				entities['247'] = '&divide;';
				entities['248'] = '&oslash;';
				entities['249'] = '&ugrave;';
				entities['250'] = '&uacute;';
				entities['251'] = '&ucirc;';
				entities['252'] = '&uuml;';
				entities['253'] = '&yacute;';
				entities['254'] = '&thorn;';
				entities['255'] = '&yuml;';
			}

			if (useQuoteStyle !== 'ENT_NOQUOTES') {
				entities['34'] = '&quot;';
			}
			if (useQuoteStyle === 'ENT_QUOTES') {
				entities['39'] = '&#39;';
			}
			entities['60'] = '&lt;';
			entities['62'] = '&gt;';


			// ascii decimals to real symbols
			for (decimal in entities) {
				if (entities.hasOwnProperty(decimal)) {
					hash_map[String.fromCharCode(decimal)] = entities[decimal];
				}
			}

			return hash_map;
		}
		
		this.htmlentities=function(string, quote_style, charset, double_encode) {
			// http://kevin.vanzonneveld.net
			// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			var hash_map = get_html_translation_table('HTML_ENTITIES', quote_style),
				symbol = '';
			string = string == null ? '' : string + '';

			if (!hash_map) {
				return false;
			}
			
			if (quote_style && quote_style === 'ENT_QUOTES') {
				hash_map["'"] = '&#039;';
			}
			
			if (!!double_encode || double_encode == null) {
				for (symbol in hash_map) {
					if (hash_map.hasOwnProperty(symbol)) {
						string = string.split(symbol).join(hash_map[symbol]);
					}
				}
			} else {
				string = string.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g, function (ignore, text, entity) {
					for (symbol in hash_map) {
						if (hash_map.hasOwnProperty(symbol)) {
							text = text.split(symbol).join(hash_map[symbol]);
						}
					}
					
					return text + entity;
				});
			}

			return string;
		};
	};
	
	
	
	
	this.URL = (new function() {
		var uThis = this;
		
		var _prepareParams = function(key, val, obj) {
			var _s = key.indexOf("["),
				_e = key.lastIndexOf("]"),
				_key,
				_subKey,
				_obj = obj || {};
			
			if(_s != -1) {
				_key = key.substr(0, _s);
				if(_s + 1 < _e) {
					_subKey = key.substring(_s + 1, _e);
					if(_obj[_key] === undefined) {
						_obj[_key] = {};
					}
					_prepareParams(_subKey, val, _obj[_key]);
				}
				else {
					if(_obj[_key] === undefined) {
						_obj[_key] = [];
					}
					_obj[_key].push(val);
				}
			}
			else {
				_obj[key] = val;
			}
			return _obj;
		};
		
		this.parseURL = function(url) {
			var _url = /(?:((?:http[s]?|file):)\/{1,3})?((?:[\d\w_\-а-яё]+.)[\d\w_\-а-яё.]+)?([^?]*)(?:\?([^#]*))?(?:#(.*))?/i.exec(url), //TODO:
				_params,
				_returned = {},
				_i,
				_tmp;
				
				_returned.protocol = _url[1] || null;
				_returned.host = _url[2] || null;
				_returned.basicHost = (_tmp = /www\.([\d\w.\-_]+)/.exec(_returned.host)) ? _tmp[1] : _returned.host;
				_returned.path = _url[3] || null;
				_returned.paramsString = _url[4] || null;
				_returned.hash = _url[5] || null;
				_returned.params = {};

				if(_returned.paramsString !== null) {
					_params = _returned.paramsString.split("&");
					for(_i = 0; _i < _params.length; _i ++) {
						if(!_params[_i]) {
							continue;
						}
						_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
						if(_tmp[2] === undefined) {
							_tmp[2] = null;
						}
						_prepareParams(_tmp[1], _tmp[2], _returned.params);
					}
				}
			return _returned;
		};
		
		this.replaceParams = function(params1, params2, ignored, added) {
			var _i,
				_returned = {};
			
			for(_i in params1) {
				if(params2[_i] !== undefined) {
					_returned[_i] = params2[_i];
				}
				else {
					if(ignored) {
						if("{" + _i + "}" != params1[_i]) {
							_returned[_i] = params1[_i];
						}
					}
					else {
						_returned[_i] = params1[_i];
					}
				}
			}
			if(added) {
				for(_i in params2) {
					if(_returned[_i] === undefined) {
						_returned[_i] = params2[_i];
					}
				}
			}
			return _returned;
		};
		
		
		var _repeat = function(string, count) {
			var _i,
				_str = "";
			count = Math.abs(parseInt(count));
			if(isNaN(count)) {
				return "";
			}
			for(_i = 0; _i < count; _i ++) {
				_str += string;
			}
			
			return _str;
		};
		
		var _serializeParamsExec = function(values, params/*&$Array*/, key, level)
		{
			var _key,
				_val,
				_ref;
			
			if(level === undefined) {
				level = 0;
			}
			
			if(typeof values == "object") {
				if(values instanceof Array) {
					for(_key = 0; _key < values.length; _key++)
					{
						_val = values[_key];
						if(typeof _val == "object")
						{
							level ++;
							_serializeParamsExec(_val, params, key ? key + "[" + _key : _key, level);
							level --;
						}
						else {
							params.push((key ? key + "[" + _key : _key) + _repeat("]", level) + "=" + _val);
						}
					}
				}
				else {
					for(_key in values)
					{
						_val = values[_key];
						if(typeof _val == "object")
						{
							level ++;
							_serializeParamsExec(_val, params, key ? key + "[" + _key : _key, level);
							level --;
						}
						else {
							if(level === 0) {
								if(_key == "ref") {
									_ref = _key + "=" + _val;
									continue;
								}
							}
							params.push((key ? key + "[" + _key : _key) + _repeat("]", level) + "=" + _val);
						}
					}
					if(_ref) {
						params.push(_ref);
					}
				}
			}
			return params;
		};
		
		this.serializeParams = function(paramsAsObject) {
			var _arr = [];
			_serializeParamsExec(paramsAsObject, _arr);
			return _arr.join("&");
		};
		
		this.URLObject2String = function(url) {
			var _srParams,
				_host = url.host ? url.host : "",
				_path = url.path;
			/*
			if(_host) {
				_host = _host.replace(/\//, "");
			}
			*/
			if(url && typeof url == "object") {
				if(url && url.params) {
					_srParams = uThis.serializeParams(url.params);
				}
				return (_host ? (url.protocol ? url.protocol : location.protocol) + "//" + _host : "") + _path + (_srParams ? "?" + _srParams : "") + (url.hash ? "#" + url.hash : "");
			}
			else return url;
		};
		
		this.addHashParam = function(param) {
			var _i;
			for(_i in param) {
				if(!location.hash) {
					location.hash = _i + "=" + param[_i];
				}
				else {
					if((new RegExp(_i + "=[\\d\\w]+", "g")).test(location.hash)) {
						location.hash = location.hash.replace(new RegExp(_i + "=[\\d\\w]+", "g"), _i + "=" + param[_i], "g");
					}
					else {
						location.hash = location.hash + "&" + _i + "=" + param[_i];
					}
				}
			}
		};
				
		this.deleteHashParam = function(param) {
			var _i;
			if(!(param instanceof Array)) {
				param = [param];
			}
			if(location.hash) {
				for(_i = 0; _i < param.length; _i ++) {
					location.hash = location.hash.replace(new RegExp(_i + "=[\\d\\w]+", "g"), "", "g");
				}
			}
		};
		
		this.getHashParam = function(param) {
			var _i;
			return (location.hash && (_i = (new RegExp(param + "=([\\d\\w]+)")).exec(location.hash))) ? _i[1] : undefined;
		};
		
		this.CorrectedUrlForFrames = function(selectors) {
			var _i,
				_len;
			if(window != top && window["bstdFrames"] && bstdFrames.setHeight) {
				if(typeof selectors == "string") {
					selectors = [selectors];
				}
				if(selectors instanceof Array) {
					_len = selectors.length;
					for(_i = 0; _i < _len; _i++) {
						$(selectors[_i]).each(function(){
							var _e = $(this),
								_tag = this.tagName.toLowerCase(),
								_tagsAttr = {a: "href", form: "action", iframe: "src"},
								_attr = _tagsAttr[_tag],
								_href;
							if(!_attr) {
								return;
							}
							_href = uThis.parseURL(_e.attr(_attr));
							
							if(location.params && location.params.refi) {
								_href.params["refi"] = location.params.refi;
							}
							if(location.params && location.params.ref) {
								_href.params["ref"] = location.params.ref;
							}
							_e.attr(_attr, uThis.URLObject2String(_href));
						});
					}
				}
			}
		};
		
		(function () {
			if(!location.params) {
				var _url = uThis.parseURL(location.href);
				location.stringParams = _url.paramsString;
				location.params = _url.params;
				location.basicHost = _url.basicHost
			}
			location.parent = document.referrer ? uThis.parseURL(document.referrer) : null;
			location.ref = location.params && location.params.ref ? uThis.parseURL(decodeURIComponent(location.params.ref)) : null;
		})();
	});
	
	this.object = {
		min: function(object, prop) {
			var _min,
				_i,
				_curr;
			for(_i in object) {
				_curr = object[_i];
				if(_min == undefined) {
					_min = _curr;
				}
				else {
					if(_min[prop] > _curr[prop]) {
						_min = _curr;
					}
				}
			}
			return _min;
		},
		max: function(object, prop) {
			var _max,
				_i,
				_curr;
			for(_i in object) {
				_curr = object[_i];
				if(_max == undefined) {
					_max = _curr;
				}
				else {
					if(_max[prop] < _curr[prop]) {
						_max = _curr;
					}
				}
			}
			return _max;
		},
		length: function(object) {
			var _len = 0,
				_i;
			for(_i in object) {
				_len ++;
			}
			return _len;
		},
		isEmpty: function(object) {
			var _i;
			for(_i in object) {
				return false;
			}
			return true;
		},
		isNotEmpty: function(object) {
			var _i;
			for(_i in object) {
				return true;
			}
			return false;
		},
		keys: function(object) {
			var _i,
				_res = [];
			if(typeof object == "object") {
				for(_i in object) {
					_res.push(_i);
				}
			}
			return _res;
		},
		values:  function(object) {
			var _i,
				_res = [];
			if(typeof object == "object") {
				for(_i in object) {
					_res.push(object[_i]);
				}
			}
			return _res;
		},
		keysAndValues: function(object) {
			var _i,
				_keys = [],
				_vals = [];
			if(typeof object == "object") {
				for(_i in object) {
					_keys.push(_i);
					_vals.push(object[_i]);
				}
			}
			return [_keys, _vals];
		},
		/**
		 * Из объекта object формата: {<key>: [val1, val2, ...]}, удаляет по значению элементы, значения которых совпадают с values.
		 * values - единичное значение или массив значений.
		 * Исходный объект изменятеся.
		 * Возвращает объект, в котором элементы содержат непустые массивы
		 */
		deleteElementsFromArray: function(object, values) {
			var _i,
				_d = {},
				_curr;
			if(values && !(values instanceof Array)) {
				values = [values];
			}
			for(_i in object) {
				bstdSystem.array.removeByValue(_curr = object[_i], values);
				if(deleteEmpty) {
					if(_curr.length) {
						_d[_i] = _curr;
					}
				}
			}
			return _d;
		},
		keyFilter: function(object, values) {
			var _i,
				_len,
				_d = {},
				_curr;
			if(values && !(values instanceof Array)) {
				values = [values];
			}
			_len = values.length;
			for(_i = 0; _i < _len; _i ++) {
				if(object[_curr = values[_i]]) {
					_d[_curr] = object[_curr];
				}
			}
			return _d;
		},
		fieldArray: function(object, fieldName, unique) {
			var _i,
				_curr,
				_r;
			if(unique) {
				_r = {};
				for(_i in object) {
					_curr = object[_i];
					_r[_curr[fieldName]] = _curr[fieldName];
				}
				_r = bstdSystem.object.keys(_r);
			}
			else {
				_r = [];
				for(_i in object) {
					_curr = object[_i];
					_r.push(_curr[fieldName]);
				}
			}
			return _r;
		},
		/**
		 * Производит выборку объектов из объекта объектов (ассоциативного массива), которые удовлетворяют условиям, заданным filter
		 * @param {Object} object - Исходный объект (ассоциативный массив) вида {<key>: {...}, ...}
		 * @param {Object} filter - Объект формата: {<propNamr>: <propValue>},
		 * 							где <propNamr> - свойство объекта,
		 *								<propValue> - единичное значение или массив значений или объект формата: {min: <min>, max: <max>, out: 0 || 1} или {range: [<min>, <max>], out: 0 || 1}
		 * Будут выбраны только такие объекты из массива объектов, которые удовлетворят хотябы одному (или единственному) значению всех свойств объекта filter
		 */
		applyFilter: function(object, filter) {
			var _i,
				_j,
				_i1,
				_len1,
				_current,
				_currFilter,
				_res = {},
				_fl,
				_fl1;
			for(_i in object) {
				_current = object[_i];
				_fl = false;
				for(_j in filter) {
					_currFilter = filter[_j];
					if(_currFilter instanceof Array) {
						_len1 = _currFilter.length;
						for(_i1 = 0; _i1 < _len1; _i1 ++) {
							if(_current[_j] == _currFilter[_i1]) {
								_fl = true;
								break;
							}
						}
						if(!_fl) {
							break;
						}
					}
					else if(typeof _currFilter == "object") {
						if(_currFilter.out) {
							if(_currFilter.min !== undefined) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.min)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							if(_currFilter.max !== undefined) {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.max)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
						else {
							if(_currFilter.min !== undefined) {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.min)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							if(_currFilter.max !== undefined) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.max)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
						if(_currFilter.range) {
							if(_currFilter.out) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.range[0]) || parseFloat(_current[_j]) >= parseFloat(_currFilter.range[1])) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							else {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.range[0]) && parseFloat(_current[_j]) <= parseFloat(_currFilter.range[1])) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
					}
					else {
						if(_current[_j] == filter[_j]) {
							_fl = true;
						}
						else {
							_fl = false;
							break;
						}
					}
				}
				if(_fl) {
					_res[_i] = _current;
				}
			}
			return _res;
		},
		/*
		 * Удаляет по ключу элементы объекта (ассоциативного массива) object. Значения для удаления values могут быть как одиночным, одномерным массивом значений, массивом объектов, объектом или ассоциативныммассивом объектов.
		 * Если values содержит в качестве элементов объекты, то в аргументе fieldName можно определить поле, значение которого необходимо использовать для удаления элементов из объекта object
		 */
		deleteByKey: function(object, values, fieldName) {
			var _i,
				_len;
			if(values instanceof Array) {
				_len = values.length;
				if(fieldName) {
					for(_i = 0; _i < _len; _i ++) {
						delete object[values[_i][fieldName]];
					}
				}
				else {
					for(_i = 0; _i < _len; _i ++) {
						delete object[values[_i]];
					}
				}
			}
			else if(typeof values == "object") {
				if(fieldName) {
					for(_i in values) {
						delete object[values[_i][fieldName]];
					}
				}
				else {
					for(_i in values) {
						delete object[_i];
					}
				}
			}
			else {
				delete object[values];
			}
			return object;
		},
		difference: function(object1, object2, fieldsName, fieldsName2, left) {
			var _i,
				_i2,
				_res2,
				_res = {},
				_object;
			if(typeof object1 == "object" && typeof object2 == "object") {
				_object = $.extend({}, object2);
				if(fieldsName) {
					if(fieldsName2) {
						for(_i in object1) {
							_res2 = true;
							for(_i2 in object2) {
								if(object1[_i][fieldsName] == object2[_i2][fieldsName2]) {
									_res2 = false;
									delete _object[_i2];
									continue;
								}
							}
							if(_res2) {
								_res[_i] = object1[_i];
							}
						}
					}
					else {
						for(_i in object1) {
							_res2 = true;
							if(object2[object1[_i][fieldsName]]) {
								_res2 = false;
								delete _object[_i2];
								continue;
							}
							if(_res2) {
								_res[_i] = object1[_i];
							}
						}
					}
				}
				else {
					if(fieldsName2) {
						for(_i in object1) {
							_res2 = true;
							for(_i2 in object2) {
								if(_i == object2[_i2][fieldsName2]) {
									_res2 = false;
									delete _object[_i2];
									continue;
								}
							}
							if(_res2) {
								_res[_i] = object1[_i];
							}
						}
					}
					else {	
						for(_i in object1) {
							if(_i in object2) {
								delete _object[_i];
								continue;
							}
							_res[_i] = object1[_i];
						}
					}
				}
			}
			return left ? _res : $.extend({}, _res, _object);
		},
		concat: function(object1) {
			var _i,
				_ret = [];
			for(_i in object1) {
				_ret = _ret.concat(_ret, object1[_i]);
			}
			return _ret;
		}
	};
	
	this.array = {
		sort: function(array, direct, asString) {
			direct = direct == -1 ? -1 : 1;
			if(array instanceof Array) {
				return array.sort(function(a,b) {
					return direct * (asString
								? (a === undefined
									? b === undefined
										? 0
										: 1
									: b === undefined
										? -1
										: a.localeCompare(b))
								: parseFloat(a) - parseFloat(b));
				});
			}
		},
		pushUnique: function(array, value) {
			var _i,
				_len,
				_is,
				_lenV,
				_j,
				_r = false;
			if(!(value instanceof Array)) {
				value = [value];
			}
			_lenV = value.length;
			if(array.indexOf) {
				for(_j = 0; _j < _lenV; _j ++) {
					_curr = value[_j];
					if(array.indexOf(_curr) == -1) {
						array.push(_curr);
						_r = true;
					}
				}
			}
			else {
				_len = array.length;
				for(_j = 0; _j < _lenV; _j ++) {
					_curr = value[_j];
					for(_i = 0; _i < _len; _i ++) {
						if(array[_i] == _curr) {
							_is = true;
							break;
						}
					}
					if(!_is) {
						array.push(_curr);
						_r = true;
					}
					_is = false;
				}
			}
			return _r;
		},
		toObject: function(array, fieldName) {
			var _i,
				_len,
				_curr,
				_ret = {};
			_len = array.length;
			for(_i = 0; _i < _len; _i ++) {
				_curr = array[_i];
				_ret[_curr[fieldName]] = _curr;
			}
			return _ret;
		},
		unique: function(array) {
			var _len,
				_i,
				_arr2 = [],
				_res = {},
				_curr;
			_len = array.length;
			for(_i = 0; _i < _len; _i ++) {
				if(!_res[_curr = array[_i]]) {
					_res[_curr] = 1;
					_arr2.push(_curr);
				}
			}
			return _arr2;
		},
		compareUniqueValues: function(array1, array2) {
			var _len,
				_len2,
				_i,
				_i2,
				_arr2,
				_res = false,
				_res2;
			if(array1 instanceof Array && array2 instanceof Array) {
				if((_len = array1.length) != (array2.length)) {
					return false;
				}
				_res = true;
				_arr2 = array2.concat([]);
				for(_i = 0; _i < _len; _i ++) {
					_res2 = true;
					_len2 = _arr2.length;
					for(_i2 = 0; _i < _len2; _i ++) {
						if(array1[_i] == _arr2[_i2]) {
							_res2 = false;
							_arr2.splice(_i2, 1);
							break;
						}
					}
					if(_res2) {
						_res = false;
						break;
					}
				}
			}
			return _res;
		},
		intersectByValues: function(array1, array2) {
			var _len,
				_len2,
				_i,
				_i2,
				_res = [];
			if(array1 instanceof Array && array2 instanceof Array) {
				array1 = sThis.array.unique(array1);
				array2 = sThis.array.unique(array2);
				for(_i = 0; _i < _len; _i ++) {
					_len2 = array2.length;
					for(_i2 = 0; _i < _len2; _i ++) {
						if(array1[_i] == array2[_i2]) {
							_res.push(array2[_i2]);
							array2.splice(_i2, 1);
							break;
						}
					}
				}
			}
			return _res;
		},
		difference: function(array1, array2, left) {
			var _len,
				_len2,
				_i,
				_i2,
				_res2,
				_res = [],
				_array2;
			if(array1 instanceof Array && array2 instanceof Array) {
				array1 = sThis.array.unique(array1);
				array2 = sThis.array.unique(array2);
				_array2 = array2.concat([]);
				_len = array1.length;
				for(_i = 0; _i < _len; _i ++) {
					_res2 = true;
					_len2 = array2.length;
					for(_i2 = 0; _i < _len2; _i ++) {
						if(array1[_i] == array2[_i2]) {
							_res2 = false;
							_array2.slice(_i2, _i2 + 1);
							break;
						}
					}
					if(_res2) {
						_res.push(array1[_i]);
					}
				}
			}
			return left ? _res : _res.concat(_array2);
		},
		compareByElements: function(array1, array2) {
			var _len,
				_len2,
				_i,
				_i2,
				_res = false;
			if(array1 instanceof Array && array2 instanceof Array) {
				if((_len = array1.length) != (_len2 = array2.length)) {
					return false;
				}
				_res = true;
				for(_i = 0; _i < _len; _i ++) {
					if(array1[_i] != array2[_i]) {
						_res = false;
						break;
					}
				}
			}
			return res;
		},
		removeByValue: function(array, value, all) {
			var _len = array.length,
				_i,
				_i2,
				_len2,
				_r = false;
			if(value instanceof Array) {
				_len2 = value.length;
				for(_i2 = 0; _i2 < _len2; _i2 ++) {
					_len = array.length;
					for(_i = 0; _i < _len; _i ++) {
						if(array[_i] == value) {
							array.splice(_i, 1);
							_r = true;
							if(!all) {
								break;
							};
						}
					}
				}
			}
			else {
				for(_i = 0; _i < _len; _i ++) {
					if(array[_i] == value) {
						array.splice(_i, 1);
						_r = true;
						if(!all) {
							break;
						}
					}
				}
			}
			return _r;
		},
		
		removeObjectByValue: function(array, fieldName, value, all) {
			var _len = array.length,
				_i,
				_i2,
				_len2,
				_curr,
				_r = false;
			if(value instanceof Array) {
			/*
				_len2 = value.length;
				for(_i2 = 0; _i2 < _len2; _i2 ++) {
					_len = array.length;
					for(_i = 0; _i < _len; _i ++) {
						if(array[_i] == value) {
							array.splice(_i, 1);
							_r = true;
							if(!all) {
								break;
							};
						}
					}
				}
*/
			}
			else {
				for(_i = 0; _i < _len; _i ++) {
					if(array[_i][fieldName] == value) {
						array.splice(_i, 1);
						_r = true;
						if(!all) {
							break;
						}
					}
				}
			}
			return _r;
		},
		
		addObjectUnique: function(array, fieldName, object) {
			var _len = array.length,
				_i,
				_i2,
				_len2,
				_curr,
				_r = true;
			if(object instanceof Array) {
			
			}
			else {
				for(_i = 0; _i < _len; _i ++) {
					if(array[_i][fieldName] == object[fieldName]) {
						_r = false;
						break;
					}
				}
				if(_r)
					array.push(object);
			}
			return _r;
		},
		
		concatUnique: function(array1, array2, fieldName, asObject) {
			var _i,
				_len,
				_ret,
				_curr;
			if(!(array1 instanceof Array) || !(array2 instanceof Array)) {
				return;
			}
			_ret = asObject ? [] : {};
			_len = array1.length;
			for(_i =0; _i < _len; _i ++) {
				_curr = array1[_i];
				_ret[_curr[fieldName]] = _curr;
			}
			_len = array2.length;
			for(_i =0; _i < _len; _i ++) {
				_curr = array2[_i];
				_ret[_curr[fieldName]] = _curr;
			}
			return asObject ? _ret : bstdSystem.object.values(_ret);
		},
		concatUniqueA: function(arrayArrays, fieldName, asObject) {
			var _i,
				_j,
				_len2,
				_len,
				_ret,
				_curr,
				_arr;
			if(!(arrayArrays instanceof Array)) {
				return;
			}
			_ret = asObject ? {} : [];
			
			_len2 = arrayArrays.length;
			for(_j =0; _j < _len2; _j ++) {
				_arr = arrayArrays[_j];
				_len = _arr.length;
				for(_i =0; _i < _len; _i ++) {
					_curr = _arr[_i];
					_ret[_curr[fieldName]] = _curr;
				}
			}
			return asObject ? _ret : bstdSystem.object.values(_ret);
		},
		getArrayByField: function(array1, fieldName, concat, unique) {
			var _i,
				_j,
				_len2,
				_len,
				_ret = [],
				_curr,
				_uq = {};
			if(!(array1 instanceof Array)) {
				return;
			}
			_len = array1.length;
			for(_i = 0; _i < _len; _i ++) {
				_curr = array1[_i];
				if(concat && _curr[fieldName] instanceof Array) {
					_ret = _ret.concat(_curr[fieldName]);
				}
				else {
					if(unique) {
						if(!_uq[_curr[fieldName]]) {
							_uq[_curr[fieldName]] = 1;
							_ret.push(_curr[fieldName]);
						}
					}
					else {
						_ret.push(_curr[fieldName]);
					}
				}
			}
			return _ret;
		},
		/**
		 *	{array} array - массив объектов. Обязательный
		 *  {String|array} fieldsName - строковое значение имени поля или массив строковых значений имен полей, которые должны присутствовать в объектах результирующего массива. Обязательный
		 *  {boolean} unique - флаг, указывающий, что в результирующем массиве должны находиться объекты с уникальными значениями. Необязательный. По умолчанию формируется не уникальный набор
		 *  {String|array} uniqueFields - строковое значение имени поля или массив строковых значений имен полей, по которым определяется уникальность объектов.
		 *						Имеет силу при агрументе unique эквивалентном true и только для fieldsName представленного массивом полей. Необязательный.
		 *						Если uniqueFields не установлен и агрумент unique эквивалентен true, для определения уникальности объектов будут использованы все поля из агрумента fieldsName
		 *
		 *  @return {array} массив объектов.
		 */
		getArrayObjectsForFields: function(array, fieldsName, unique, uniqueFields) {
			var _i,
				_j,
				_s,
				_len3,
				_len2,
				_len,
				_ret = [],
				_curr,
				_curr2,
				_curr3,
				_cRet,
				_uqKey,
				_uq = {};
			if(!(array instanceof Array)) {
				return;
			}
			_len = array.length;
			_len2 = fieldsName.length;
			if(fieldsName instanceof Array) {
				if(unique) {
					if(uniqueFields === undefined) {
						uniqueFields = fieldsName;
						_len3 = uniqueFields.length;
					}
					for(_i = 0; _i < _len; _i ++) {
						_curr = array[_i];
						_uqKey = "";
						for(_s = 0; _s < _len3; _s ++) {
							_curr3 = uniqueFields[_s]
							_uqKey += _curr3 + "^$:" + _curr[_curr3] + "#@`";
						}
						if(!_uq[_uqKey]) {
							_uq[_uqKey] = [];
							_cRet = {};
							for(_j = 0; _j < _len2; _j ++) {
								_curr2 = fieldsName[_j]
								_cRet[_curr2] = _curr[_curr2];
							}
							_ret.push(_cRet);
						}
					}
				}
				else {
					for(_i = 0; _i < _len; _i ++) {
						_curr = array[_i];
						_cRet = {};
						for(_j = 0; _j < _len2; _j ++) {
							_curr2 = fieldsName[_j]
							_cRet[_curr2] = _curr[_curr2];
						}
						_ret.push(_cRet);
					}
				}
			}
			else {
				if(unique) {
					for(_i = 0; _i < _len; _i ++) {
						_curr = array[_i];
						_cRet = {};
						if(!_uq[_curr[fieldsName]]) {
							_uq[_curr[fieldsName]] = 1;
							_cRet[fieldsName] = _curr[fieldsName];
							_ret.push(_cRet);
						}
					}
				}
				else {
					for(_i = 0; _i < _len; _i ++) {
						_cRet = {};
						_cRet[fieldsName] = array[_i][fieldsName];
						_ret.push(_cRet);
					}
				}
			}
			return _ret;
		},
		/**
		 * @param {Array} array - исходный массив вида [{...},...] (будет изменен)
		 * @param {Array} criteries - массив критериев по которым будет произведена сортировка ["criteria1","criteria2",...] 
		 * @param {Array} orders - массив вида [1,-(1, 1, 1, 1),] задающий направление сортировки. 1 - прямая сортировка по соответствующему критерию, -1 обратная
		 */
		sortObjects: function(array, criteries, orders) {
			if(typeof criteries == "string") criteries=[criteries];
			if(typeof orders == "string" || typeof orders == "number") orders=[orders];
			function cr_sort(a,b){ // функция сортировки по нескольким критериям
				var _a,_b,j,res=0;
				if(criteries instanceof Array) {
					for(j=0;j<criteries.length;j++){ // цикл по критериям, переход к следующему критерию только если равен текущий. (foo больше undefined,undefined==undefined )
						_a=a[criteries[j]];
						_b=b[criteries[j]];
						if(_a==_b) continue; // по текущему критерию элементы равны, переход к следующему
						if(_a===undefined){res=orders[j] * -1; break;};
						if(_b===undefined){res=orders[j] * 1; break;};
						
						if (parseFloat(_a) && parseFloat(_b)){
							res=orders[j] * (_a-_b); break;
						} else {
							_a = (""+_a).toLowerCase();
							_b = (""+_b).toLowerCase();
							res=orders[j] * _a.localeCompare(_b);
							if(res) break; // если здесь _a==_b то цикл просто продолжится
						}
					}
				}
				else {
					
				}
				return res;
			}
			
			return (criteries) ? array.sort(cr_sort) : array.sort();
		},
		/**
		 * Возвращвет первый найденный объект из массива объектов, у которого свойство prop нестрого равно value
		 * @param {Array} array - Исходный массив вида [{...},...]
		 * @param {String} prop - Свойство объектов массива, ко которому необходимо произвести поиск
		 * @param {String|Numeric} value - Значение свойства prop, которому производится сравнение.
		 */
		findObject: function(array, prop, value) {
			var _i,
				_len,
				_curr,
				_idx,
				_res = value instanceof Array ? [] : null;
			if(array instanceof Array) {
				_len = array.length;
				for(_i = 0; _i < _len; _i ++) {
					_curr = array[_i];
					if(value instanceof Array) {
						if($.inArray(_curr[prop], value) != -1) {
							_res.push(_curr);
							value.splice(_idx, 1);
							if(!value.length) {
								break;
							}
						}
					}
					else {
						if(_curr[prop] == value) {
							_res = _curr;
							break;
						}
					}
				}
			}
			return _res;
		},
		min: function(array, prop, asData) {
			var _min,
				_i,
				_len = array.length,
				_curr;
			for(_i = 0; _i < _len; _i++) {
				_curr = array[_i];
				if(_min == undefined) {
					_min = _curr;
				}
				else {
					if(asData) {
						if(sThis.Date.convertToUnixTimestamp(_min[prop]) > sThis.Date.convertToUnixTimestamp(_curr[prop])) {
							_min = _curr;
						}
					}
					else {
						if(_min[prop] > _curr[prop]) {
							_min = _curr;
						}
					}
				}
			}
			return _min;
		},
		max: function(array, prop, asData) {
			var _max,
				_i,
				_len = array.length,
				_curr;
			for(_i = 0; _i < _len; _i++) {
				_curr = array[_i];
				if(_max == undefined) {
					_max = _curr;
				}
				else {
					if(asData) {
						if(sThis.Date.convertToUnixTimestamp(_max[prop]) < sThis.Date.convertToUnixTimestamp(_curr[prop])) {
							_max = _curr;
						}
					}
					else {
						if(_max[prop] < _curr[prop]) {
							_max = _curr;
						}
					}
				}
			}
			return _max;
		},
		/**
		 * Производит выборку объектов из массива объектов, которые удовлетворяют условиям, заданным filter
		 * @param {Array} array - Исходный массив вида [{...},...]
		 * @param {Object} filter - Объект формата: {<propNamr>: <propValue>},
		 * 							где <propNamr> - свойство объекта,
		 *								<propValue> - единичное значение или массив значений или объект формата: {min: <min>, max: <max>, out: 0 || 1} или {range: [<min>, <max>], out: 0 || 1}
		 * Будут выбраны только такие объекты из массива объектов, которые удовлетворят хотябы одному (или единственному) значению всех свойств объекта filter
		 */
		applyFilter: function(array, filter, byOr) {
			var _i,
				_j,
				_i1,
				_len,
				_len1,
				_current,
				_currFilter,
				_res = [],
				_fl,
				_fl1,
				_fa;
			_len = array.length;
			for(_i = 0; _i < _len; _i ++) {
				_current = array[_i];
				_fl = false;
				for(_j in filter) {
					_currFilter = filter[_j];
					if(_currFilter instanceof Array) {
						_len1 = _currFilter.length;
						if(byOr) {
							for(_i1 = 0; _i1 < _len1; _i1 ++) {
								if(_current[_j] instanceof Array) {
									if($.inArray(_currFilter[_i1], _current[_j]) != -1) {
										_fl = true;
										break;
									}
								}
								else {
									if(_current[_j] == _currFilter[_i1]) {
										_fl = true;
										break;
									}
								}
							}
							if(_fl) {
								break;
							}
						}
						else {
							for(_i1 = 0; _i1 < _len1; _i1 ++) {
								if(_current[_j] instanceof Array) {
									if($.inArray(_currFilter[_i1], _current[_j]) == -1) {
										_fl = false;
										break;
									}
									else {
										_fl = true;
									}
								}
								else {
									if(_current[_j] == _currFilter[_i1]) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
							}
							if(!_fl) {
								break;
							}
						}
					}
					else if(typeof _currFilter == "object") {
						if(_currFilter.out) {
							if(_currFilter.min !== undefined) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.min)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							if(_currFilter.max !== undefined) {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.max)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
						else {
							if(_currFilter.min !== undefined) {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.min)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							if(_currFilter.max !== undefined) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.max)) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
						if(_currFilter.range) {
							if(_currFilter.out) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.range[0]) || parseFloat(_current[_j]) >= parseFloat(_currFilter.range[1])) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							else {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.range[0]) && parseFloat(_current[_j]) <= parseFloat(_currFilter.range[1])) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
					}
					else {
						if(_current[_j] instanceof Array) {
							if($.inArray(filter[_j], _current[_j])) {
								_fl = true;
							}
							else {
								_fl = false;
								break;
							}
						}
						else {
							if(_current[_j] == filter[_j]) {
								_fl = true;
							}
							else {
								_fl = false;
								break;
							}
						}
					}
				}
				if(_fl) {
					_res.push(_current);
				}
			}
			return _res;
		},
				/**
		 * Производит выборку объектов из массива объектов, которые удовлетворяют условиям, заданным filter
		 * @param {Array} array - Исходный массив вида [{...},...]
		 * @param {Object} filter - Объект формата: {<propNamr>: <propValue>},
		 * 							где <propNamr> - свойство объекта,
		 *								<propValue> - единичное значение или массив значений или объект формата: {min: <min>, max: <max>, out: 0 || 1} или {range: [<min>, <max>], out: 0 || 1}
		 * Будут выбраны только такие объекты из массива объектов, которые удовлетворят хотябы одному (или единственному) значению всех свойств объекта filter
		 */
		applyFilter2: function(array, filter) {
			var _i,
				_j,
				_i1,
				_len,
				_len1,
				_current,
				_currFilter,
				_res = [],
				_fl,
				_fl1,
				_fa,
				_by = "and",
				_filter,
				_vCurr;
			_len = array.length;
			for(_i = 0; _i < _len; _i ++) {
				_current = array[_i];
				_fl = false;
				for(_j in filter) {
					_filter = filter[_j];
					_currFilter = _filter.filter;
					_by = _filter.by.toLowerCase();
					if(_currFilter instanceof Array) {
						if(_by == "or") {
							if(_current[_j] instanceof Array) {
								_len1 = _current[_j].length;
								_fl1 = false;
								for(_i1 = 0; _i1 < _len1; _i1 ++) {
									if($.inArray(_current[_j][_i1], _currFilter) != -1) {
										_fl1 = true;
										break;
									}
								}
								_fl = _fl1;
								if(!_fl) {
									break;
								}
							}
							else {
								if($.inArray(_current[_j], _currFilter) == -1) {
									_fl = false;
									break;
								}
								else {
									_fl = true;
								}
							}
						}
						else {
							_len1 = _currFilter.length;
							for(_i1 = 0; _i1 < _len1; _i1 ++) {
								if(_current[_j] instanceof Array) {
									if($.inArray(_currFilter[_i1], _current[_j]) == -1) {
										_fl = false;
										break;
									}
									else {
										_fl = true;
									}
								}
								else {
									if(_current[_j] == _currFilter[_i1]) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
							}
							if(!_fl) {
								break;
							}
						}
					}
					else if(typeof _currFilter == "object") {
						
						if(_currFilter.out) {
							if(_currFilter.min !== undefined) {
								if(_currFilter.type == "date" || _currFilter.type == "datetime") {
									if(bstdSystem.Date.convertToUnixTimestamp(_current[_j], _currFilter.type == "date") <= bstdSystem.Date.convertToUnixTimestamp(_currFilter.min, _currFilter.type == "date")) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
								else {
									if(parseFloat(_current[_j]) <= parseFloat(_currFilter.min)) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
							}
							if(_currFilter.max !== undefined) {
								if(_currFilter.type == "date" || _currFilter.type == "datetime") {
									if(bstdSystem.Date.convertToUnixTimestamp(_current[_j], _currFilter.type == "date") >= bstdSystem.Date.convertToUnixTimestamp(_currFilter.max, _currFilter.type == "date")) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
								else {
									if(parseFloat(_current[_j]) >= parseFloat(_currFilter.max)) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
							}
						}
						else {
							if(_currFilter.min !== undefined) {
								if(_currFilter.type == "date" || _currFilter.type == "datetime") {
									if(bstdSystem.Date.convertToUnixTimestamp(_current[_j], _currFilter.type == "date") >= bstdSystem.Date.convertToUnixTimestamp(_currFilter.min, _currFilter.type == "date")) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
								else {
									if(parseFloat(_current[_j]) >= parseFloat(_currFilter.min)) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
							}
							if(_currFilter.max !== undefined) {
								if(_currFilter.type == "date" || _currFilter.type == "datetime") {
									if(bstdSystem.Date.convertToUnixTimestamp(_current[_j], _currFilter.type == "date") <= bstdSystem.Date.convertToUnixTimestamp(_currFilter.max, _currFilter.type == "date")) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
								else {
									if(parseFloat(_current[_j]) <= parseFloat(_currFilter.max)) {
										_fl = true;
									}
									else {
										_fl = false;
										break;
									}
								}
							}
						}
						if(_currFilter.range) {
							if(_currFilter.out) {
								if(parseFloat(_current[_j]) <= parseFloat(_currFilter.range[0]) || parseFloat(_current[_j]) >= parseFloat(_currFilter.range[1])) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
							else {
								if(parseFloat(_current[_j]) >= parseFloat(_currFilter.range[0]) && parseFloat(_current[_j]) <= parseFloat(_currFilter.range[1])) {
									_fl = true;
								}
								else {
									_fl = false;
									break;
								}
							}
						}
						

					}
					else {
						if(_current[_j] == _currFilter) {
							_fl = true;
						}
						else {
							_fl = false;
							break;
						}
					}
				}
				if(_fl) {
					_res.push(_current);
				}
			}
			return _res;
		},
		
		valuesByFields: function(array, field, unique) {
			var _i,
				_j,
				_curr,
				_len,
				_fLen,
				_uq = {},
				_res = {};
			if(!(array instanceof Array) || typeof field != "string" && !(field instanceof Array)) {
				return _res;
			}
			_len = array.length;
			if(field instanceof Array) {
				_fLen = field.length;
				if(unique) {
					for(_i = 0; _i < _fLen; _i ++) {
						_res[field[_i]] = [];
						_uq[field[_i]] = {};
					}
					for(_i = 0; _i < _len; _i ++) {
						for(_j = 0; _j < _fLen; _j ++) {
							_curr = array[_i][field[_j]];
							if(_uq[field[_j]][_curr]) {
								continue;
							}
							_uq[field[_j]][_curr] = 1;
							_res[field[_j]].push(_curr);
						}
					}
				}
				else {
					for(_i = 0; _i < _fLen; _i ++) {
						_res[field[_i]] = [];
					}
					for(_i = 0; _i < _len; _i ++) {
						for(_j = 0; _j < _fLen; _j ++) {
							_res[field[_j]].push(array[_i][field[_j]]);
						}
					}
				}
			}
			else {
				_res[field] = [];
				if(unique) {
					for(_i = 0; _i < _len; _i ++) {
						_curr = array[_i][field];
						if(_uq[_curr]) {
							continue;
						}
						_uq[_curr] = 1;
						_res[field].push(_curr);
					}
				}
				else {
					for(_i = 0; _i < _len; _i ++) {
						_res[field].push(array[_i][field]);
					}
				}
			}
			return _res;
		},
		
		relatedFilters: function(array, arrayRelate, fields, relateFiler) {
			if(!(array instanceof Array) || !(arrayRelate instanceof Array) || typeof fields == "object" || (fields instanceof Array) || !array.length || !arrayRelate.length) {
				return [];
			}
			return _currRealte = sThis.array.applyFilter(array, sThis.array.valuesByFields(sThis.array.applyFilter(arrayRelate, relateFiler, 1), fields), 1);
		},
		
		
		
		differenceArrForObj: function(array, arrayObjects, field) {
			var _i,
				_j,
				_len,
				_len2,
				_res = [],
				_curr,
				_curr2,
				_f;
			if(!(array instanceof Array) || !(arrayObjects instanceof Array)) {
				return array;
			}
			_len = array.length;
			_len2 = arrayObjects.length;
			for(_i = 0; _i < _len; _i ++) {
				_curr = array[_i];
				for(_j = 0; _j < _len2; _j ++) {
					if((_curr2 = arrayObjects[_j])[field] === undefined || _curr2[field] == _curr) {
						_f = true;
						break;
					}
					if(!_f) {
						_res.push(_curr);
					}
					_f = false;
				}
			}
			return _res;
		},
		differenceObjForArr: function(array, arrayObjects, field) {
			var _i,
				_j,
				_len,
				_len2,
				_res = [],
				_curr,
				_curr2,
				_f;
			if(!(array instanceof Array) || !(arrayObjects instanceof Array)) {
				return array;
			}
			_len = array.length;
			_len2 = arrayObjects.length;
			for(_j = 0; _j < _len2; _j ++) {
				_curr2 = arrayObjects[_j];
				for(_i = 0; _i < _len; _i ++) {
					_curr = array[_i];
					if(_curr2[field] === undefined || _curr2[field] == _curr) {
						_f = true;
						break;
					}
					if(!_f) {
						_res.push(_curr2);
					}
					_f = false;
				}
			}
			return _res;
		},
		next: function(array, field, value, returnerField) {
			var _f,
				_i,
				_len;
			if(!(array instanceof Array)) {
				return;
			}
			_len = array.length;
			for(_i = 0; _i < _len; _i ++) {
				if(array[_i][field] && array[_i][field] == value) {
					_f = true;
					break
				}
			}
			_i ++;
			return _f && array[_i] ? returnerField ? array[_i][returnerField] : array[_i] : undefined;
		},
		
		preview: function(array, field, value, returnerField) {
			var _f,
				_i,
				_len;
			if(!(array instanceof Array)) {
				return;
			}
			_len = array.length;
			for(_i = 0; _i < _len; _i ++) {
				if(array[_i][field] && array[_i][field] == value) {
					_f = true;
					break
				}
			}
			_i --;
			return _f && array[_i] ? returnerField ? array[_i][returnerField] : array[_i] : undefined;
		},
		
		
		sortObjectsA: function(array, field, direct, isString, returnArray) {
			var _fieldName = typeof field == "object" ? field.fieldName : field,
				_direct = parseInt(direct) ? direct < 0 ? -1 : 1 : typeof field == "object" && parseInt(field.direct) < 0 ? -1 : 1,
				_isString = isString === undefined ?  typeof field == "object" && field.isString === undefined ? false : !!field.isString : isString,
				_array = [],
				_sorted,
				_currVal,
				_currArr = [],
				_val;
			
			if(!_fieldName) {
				//throw new Error("Arguments is error");
				return returnArray ? [] : null;
			}
			_sorted = array.sort(function(a,b) {
				_direct * (_isString ?
							a[_fieldName].localeCompare(b[_fieldName]) :
								parseFloat(a[_fieldName]) > parseFloat(b[_fieldName]) ?
									 1 : -1);
			});
			
			if(returnArray) {
				_len = _sorted.length;
				for(_i = 0; _i < _len; _i ++) {
					_curr = _sorted[_i];
					_val = _curr[_fieldName];
					if(_currVal == _val) {
						_currArr.push(_curr);
					}
					else {
						if(_currArr.length) {
							_array.push(_currArr);
						}
						_currArr = [];
						_currArr.push(_curr);
						_currVal = _val;
					}
				}
				return _array;
			}
			else {
				return _sorted;
			}
		},


		/**
		 * fields = [
		 * 		["<field_name>", "<direct>", "<flag_is_string>"]
		 * ]
		 * 
		 * fields = [
		 * 		{
		 * 			fieldName: "<field_name>",
		 * 			fieldIndex: "<field_index>",
		 * 			direct: "<direct>",
		 * 			isString: "<flag_is_string>"
		 * 		}
		 * ]
		 */
		multySortObjects: function(array, fields, direct, isString) {
			var _i,
				_j,
				_fieldCount,
				_fields = [],
				_field,
				_arr,
				_cnt = 1,
				_oneDirect,
				_oneIsString,
				
				_tmpArr = [];
			
			if(!array) {
				return array;
			}
			if(!direct) {
				direct = 1;
				_oneDirect = 1;
			}
			else if(typeof direct == "string" || typeof direct == "number") {
				direct = parseInt(direct) < 0 ? -1 : 1;
				_oneDirect = 1;
			}
			else if(!(direct instanceof Array)) {
				direct = 1;
				_oneDirect = 1;
			}
			
			if(!isString) {
				isString = true;
				_oneIsString = 1;
			}
			else if(typeof isString == "string" || typeof isString == "number" || typeof isString == "boolean") {
				isString = !!isString;
				_oneIsString = 1;
			}
			else if(!(isString instanceof Array)) {
				isString = true;
				_oneIsString = 1;
			}
			
			if(typeof fields == "string" || typeof fields == "number") {
				fields = [fields];
			}
			if(fields instanceof Array) {
				if(!fields.length) {
					return array;
				}
				_fieldCount = fields.length;
				if(fields[0] instanceof Array) {
					for(_i = 0; _i < _fieldCount; _i ++) {
						_fields.push({
							fieldName:	fields[_i][0],
							direct:		fields[_i][1] ? parseInt(fields[_i][1]) < 0 ? -1 : 1 : _oneDirect ? direct : direct[_i] < 0 ? -1 : 1,
							isString:	fields[_i][2] === undefined || 1
						});
					}
				}
				else if(typeof fields[0] == "object") {
					for(_i = 0; _i < _fieldCount; _i ++) {
						_fields.push({
							fieldName:	fields[_i]["fieldName"],
							direct:		fields[_i]["direct"] ? parseInt(fields[_i]["direct"]) < 0 ? -1 : 1 : _oneDirect ? direct : direct[_i] < 0 ? -1 : 1,
							isString:	fields[_i]["isString"] === undefined ? _oneIsString ? isString : isString[_i] !== undefined ? isString[_i] : 1 : !!fields[_i]["isString"]
						});
					}
				}
			}
			else if(typeof fields == "object") {
				for(_i in fields) {
					_fields.push({
						fieldName:	_i,
						direct:		typeof fields[_i] == "string" || typeof fields[_i] == "number"
										? parseInt(fields[_i]) < 0
											? -1
											: 1
										: fields[_i] instanceof Array
											? parseInt(fields[_i][0]) < 0
												? -1
												: 1
											: typeof fields[_i] == "object"
												? fields[_i]["direct"] !== undefined
													? parseInt(fields[_i]["direct"]) < 0
														? -1
														: 1
													: _oneDirect
														? direct
														: direct[_i] < 0
															? -1
															: 1
												: 1,
						isString:	fields[_i] instanceof Array && fields[_i][0] !== undefined
										? !!fields[_i][0]
										: typeof fields[_i] == "object" && fields[_i]["isString"] !== undefined
											? !!fields[_i]["isString"]
											: _oneIsString
												? isString
												: isString[_i] !== undefined
													? isString[_i]
													: 1
					});
				}
			}

			return array.sort(function(a,b) {
				var _i,
					_len = _fields.length,
					_curr,
					_res;
				
				for(_i = 0; _i < _len; _i ++) {
					_curr = _fields[_i];
					_res = _curr["direct"] * (_curr["isString"] ?
							a[_curr["fieldName"]].localeCompare(b[_curr["fieldName"]]) :
								parseFloat(a[_curr["fieldName"]]) > parseFloat(b[_curr["fieldName"]]) ? 1 : parseFloat(a[_curr["fieldName"]]) < parseFloat(b[_curr["fieldName"]]) ? -1 : 0);
					if(_res) {
						break;
					}
				}
				return _res;
			});
		}
		
	};
	
	
	this.ElementsTypes = (new function() {
		var sThis = this;
		
		var _advancedElementsTypes = {};
		
		this.registerElementsType = function(type, func) {
			if(typeof type == "string" && typeof func == "function")  {
				_advancedElementsTypes[type] = func;
			}
			else {
				if(_SYS_DEBUG) {
					throw new Error("Arguments error");
				}
				return false;
			}
		};
		this.has = function(type) {
			return (type && _advancedElementsTypes[type]);
		};
		
		this.create = function(type, ele, params) {
			return sThis.has(type) && ele ? new _advancedElementsTypes[type](ele, params) : undefined;
		};
	});
	
	this.templater = new (function() {
		var sThis = this;
		
		var _calc = (new function() {
			var cThis = this;
			
			this.currentData = function() {
				return new Data();
			};
			
		});
		
		this.calc = function(def){
			var _func = /^\s*_calc\s*:\s*([\w\d\-_]+)\s*/.exec(def);
			return _func === null ? def : _calc[_func] ? _calc[_func]() : undefined;
		};
		
		this.prepareTypeParams = function(paramsString) {
			var _params = {},
				_i,
				_len,
				_curr,
				_val;
			if(!paramsString) {
				return _params;
			}
			paramsString = paramsString.split(/\s*,\s*/);
			
			if(paramsString && (_len = paramsString.length)) {
				for(_i = 0; _i < _len; _i ++) {
					_curr = paramsString[_i].split(/\s*:\s*/);
					if(_curr && _curr.length) {
						_params[_curr[1]] = sThis.calc(_curr);
					}
				}
			}
			return _params;
		};
		
		this.getByTemplate = function(template, data) {
			var _i;
			if(!data || typeof data != "object") {
				return;
			}
			for(_i in data) {
				if(typeof data[_i] == "number" || typeof data[_i] == "string") {
					template = template.replace(new RegExp('{#' + _i + '#}',"g"), data[_i], "g");
				}
			}
			return template.replace(/{#.+?#}/g, "", "g");
		};
		
		this.substitutionData = function(parent, data) {
			var _i,
				_j,
				_i1,
				_item,
				_ele,
				_tag,
				_type,
				_prop;
			parent = $(parent);
			if(!parent.length || typeof data != "object") {
				return;
			}
			for(_i in data) {
				_item = $("[data-field=" + _i + "]", parent);
				for(_j = 0; _j < _item.length; _j ++) {
					_ele = $(_item[_j]);
					if(_prop = _ele.attr("data-prop")) {
						_prop = _prop.split(",");
						for(_i1 = 0; _i1 < _prop.length; _i ++) {
							_ele.prop(_prop[_i], data[_i]);
						}
					}
					else {
						_type = _ele.prop("type");
						switch(_type) {
							case "select-multiple":
							case "select-one":
							case "file":
								break;
							case "checkbox":
							case "password":
							case "textarea":
							case "text":
							case "hidden":
							case "radio":
								_ele.val(data[_i]);
								break;
							default:
								_tag = _ele.prop("tagName").toLowerCase();
								switch(_type) {
									case "form":
									case "script":
									case "link":
										break;
									case "image":
										_ele.prop("src", data[_i]);
									case "a":
										_ele.prop("href", data[_i]);
									case "iframe":
										_ele.prop("src", data[_i]);
										break;
									default:
										_ele.html(data[_i]);
								}
						}
					}
					$("[data-" + _i + "]", parent).attr("[data-" + _i + "]", data[_i]);
				}
			}
		};
	});
	
	this.Cookies = {
		get: function (name) {
			if(!sThis.IsCookie) return false;
			var c=document.cookie.split("; "),cookie;
			for (var i = 0; i < c.length; i ++) {
				cookie=c[i].split("=");
				if (cookie[0] == name)
					return unescape(cookie[1]);
			}
			return null;
		},
		set: function(name, value, exp_y, exp_m, exp_d, path, domain, secure){
			var cookie_string = name + "=" + escape (value);
			if(exp_y) {
				var expires = new Date ( exp_y, exp_m, exp_d );
				cookie_string += "; expires=" + expires.toGMTString();
			}
			if(!domain) {domain = location.host;}
			if(path) {cookie_string += "; path=" + escape ( path )};
			cookie_string += "; domain=" + escape (domain || location.host);
			if(secure || location.protocol == "https:") cookie_string += "; secure";
			document.cookie = cookie_string;
		},
		remove: function(cookie_name, domain) {
			document.cookie = cookie_name + "=; domain="+(domain ? domain : location.host)+"; expires="+(new Date(1000)).toGMTString();
		}
	};
	
	this.format = {
		integer: function(value, params) {
			return sThis.format.digital(value, params);
		},
		
		decimal: function(value, params) {
			if(!params) {
				params = {};
			}
			params["isFloat"] = 1;
			return sThis.format.digital(value, params);
		},
		
		currency: function(value, params) {
			if(!params) {
				params = {};
			}
			params["isDecimal"] = 1;
			params["decimalCount"] = 2;
			return sThis.format.digital(value, params);
		},
		
		digital: function(value, params) {
			var _i,
				_k = 0,
				_separator = params && params.separator ? params.separator : " ",
				_decimalSeparator = params && params.decimalSeparator ? params.decimalSeparator : ",",
				_ret = "",
				_len,
				_isFloat = params && params.isFloat ? params.isFloat : params && params.decimalCount == -1 ? true : false,
				_isDecimal = params && params.isDecimal ? params.isDecimal : params && params.decimalCount != -1 ? true : false,
				_decimalCount = params && params.decimalCount ? params.decimalCount : _isDecimal ? 2 : _isFloat ? -1 : 0,
				_decimal;
			
			value = value.toString();
			if(_isFloat || _isDecimal) {
				value = (_decimal = value.split("."))[0];
				_decimal = _decimal[1];
				if(_decimalCount != -1 ) {
					_decimal = _decimal ? _decimal.substr(0, _decimalCount) : "";
				}
			}
			_len = value.length;
			for (_i = _len - 1; _i > -1; _i --) {
				_k ++;
				_ret = value.charAt(_i) + _ret;
				if (_k == 3) {
					_k = 0;
					_ret = _separator + _ret;
				}
			}
			if (_ret.charAt(0) === _separator) {
				_ret = _ret.substr(1);
			}
			if(_decimal) {
				_ret = _ret + _decimalSeparator + _decimal;
			}
			return _ret;
		},
		
		phone: function(value, code) {
			var _v = /^(?:\+7|8)?\s*[-.\/\\,:]?(\d{10}|(?:(?:\(\s*\d{3}\s*\)|\d{3})\s*[-.\/\\,:]?\d{3}(?:\s*[-.\/\\,:]?\d{2}){2})|(?:(?:\(\s*\d{4}\s*\)|\d{4})(?:(?:\s*[-.\/\\,:]?\d{2}){3}|(?:\s*[-.\/\\,:]?\d{3}){2}))|(?:(?:\(\s*\d{5}\s*\)|\d{5})(?:\s*[-.\/\\,:]?\d{3}\s*[-.\/\\,:]?\d{2}|\s*[-.\/\\,:]?\d{1}(?:\s*[-.\/\\,:]?\d{2}){2}))|(?:(?:\(\s*\d{6}\s*\)|\d{6})(?:(?:\s*[-.\/\\,:]?\d{2}){2}|\s*[-.\/\\,:]?\d{1}\s*[-.\/\\,:]?\d{3}|\s*[-.\/\\,:]?\d{3}\s*[-.\/\\,:]?\d{1}))|(?:(?:\(\s*\d{7}\s*\)|\d{7})(?:\s*[-.\/\\,:]?\d{3}|\s*[-.\/\\,:]?\d{1}\s*[-.\/\\,:]?\d{2}|\s*[-.\/\\,:]?\d{2}\s*[-.\/\\,:]?\d{1})))$/.exec(value),
				_ret;
			if(_v) {
				_v = _v[1].replace(/[^\d]/g, '', "g");
				if(code = parseInt(code)) {
					value = {
						code: _v.slice(0, code),
						phone: _v.slice(code)
					}
				}
				else {
					value = _v;
				}
			}
			else {
				value = "";
			}
			return value;
		},
		unixDate: function(day, month, year) {
			if(bstdSystem.Validator.Validate("day", null, day)
				&& bstdSystem.Validator.Validate("month", null, month)
				&& bstdSystem.Validator.Validate("year", null, year)) {
				day = parseInt(day);
				month = parseInt(month);
				return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day);
			}
			return "";
		},
		rusDate: function(day, month, year) {
			if(bstdSystem.Validator.Validate("day", null, day)
				&& bstdSystem.Validator.Validate("month", null, month)
				&& bstdSystem.Validator.Validate("year", null, year)) {
				day = parseInt(day);
				month = parseInt(month);
				return (day < 10 ? "0" + day : day) + "." + (month < 10 ? "0" + month : month) + "." + year;
			}
			return "";
		}
	};
	
	this.dateFormat = function () {
		var	//token = /d{(1, 1, 4, 1),}|m{(1, 1, 4, 1),}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
			token = /([dmyDHhmMLlTtSsZo]+)/g,
			timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
			timezoneClip = /[^-+\dA-Z]/g,
			pad = function (val, len) {
				val = String(val);
				len = len || 2;
				while (val.length < len) val = "0" + val;
				return val;
			};

		// Regexes and supporting functions are cached through closure
		return function (date, mask, utc) {
			var dF = sThis.dateFormat;

			// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
			if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
				mask = date;
				date = undefined;
			}

			// Passing date through Date applies Date.parse, if necessary
			date = date ? new Date(date) : new Date;
			if (isNaN(date)) throw SyntaxError("invalid date");

			mask = String(dF.masks[mask] || mask || dF.masks["default"]);

			// Allow setting the utc argument via the mask
			if (mask.slice(0, 4) == "UTC:") {
				mask = mask.slice(4);
				utc = true;
			}

			var	_ = utc ? "getUTC" : "get",
				d = date[_ + "Date"](),
				D = date[_ + "Day"](),
				m = date[_ + "Month"](),
				y = date[_ + "FullYear"](),
				H = date[_ + "Hours"](),
				M = date[_ + "Minutes"](),
				s = date[_ + "Seconds"](),
				L = date[_ + "Milliseconds"](),
				o = utc ? 0 : date.getTimezoneOffset(),
				flags = {
					d:    d,
					dd:   pad(d),
					ddd:  dF.i18n.dayNames[D],
					dddd: dF.i18n.dayNames[D + 7],
					m:    m + 1,
					mm:   pad(m + 1),
					mmm:  dF.i18n.monthNames[m],
					mmmm: dF.i18n.monthNames[m + 12],
					yy:   String(y).slice(2),
					yyyy: y,
					h:    H % 12 || 12,
					hh:   pad(H % 12 || 12),
					H:    H,
					HH:   pad(H),
					M:    M,
					MM:   pad(M),
					s:    s,
					ss:   pad(s),
					l:    pad(L, 3),
					L:    pad(L > 99 ? Math.round(L / 10) : L),
					t:    H < 12 ? "a"  : "p",
					tt:   H < 12 ? "am" : "pm",
					T:    H < 12 ? "A"  : "P",
					TT:   H < 12 ? "AM" : "PM",
					Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
					o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
					S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
				};

			return mask.replace(token, function ($0) {
				return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
			});
		};
	}();

	// Some common format strings
	this.dateFormat.masks = {
		"default":      "ddd mmm dd yyyy HH:MM:ss",
		shortDate:      "m/d/yy",
		mediumDate:     "mmm d, yyyy",
		longDate:       "mmmm d, yyyy",
		fullDate:       "dddd, mmmm d, yyyy",
		shortTime:      "h:MM TT",
		mediumTime:     "h:MM:ss TT",
		longTime:       "h:MM:ss TT Z",
		isoDate:        "yyyy-mm-dd",
		isoTime:        "HH:MM:ss",
		isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
		isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
	};

	// Internationalization strings
	this.dateFormat.i18n = {
		dayNames: [
			"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
			"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
		],
		monthNames: [
			"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
			"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
		]
	};

	// For convenience...
	Date.prototype.format = function (mask, utc) {
		return sThis.dateFormat(this, mask, utc);
	};

	this.units = function(text) {
		return text.replace(/([\s,.])куб.\s*см/g, "$1см<sup>3</sup>", "g");
	};
	
	this.Date = {
		convertToUnixTimestamp: function(date) {
			var _r = /^(?:\s*([0-2]?\d|3[01])(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)(\d{4})|(\d{4})(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)(?:([0-2]?\d|3[01])\s*)\s*(?:([01]?\d|2[0-3])(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d)(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d))?)$/.exec(date),
				_d;
			if(_r) {
				return Date.UTC(parseInt(_r[3] || _r[4]), parseInt(_r[2] || _r[5]) - 1, parseInt(_r[1] || _r[6]), parseInt(_r[7] || 0), parseInt(_r[8] || 0), parseInt(_r[9] || 0)) / 1000;
			}
			else {
				return undefined;
			}
		},
		convertFromUnixTimestamp: function(timestamp, asDate) {
			var _r = new Date(timestamp * 1000);
			return _r ? _r.getFullYear() + " - " + (_r.getMonth() < 10 ? "0" : "") + _r.getMonth() + "-" + (_r.getDate() < 10 ? "0" : "") + _r.getDate() + (asDate ? "" : + (_r.getHours() < 10 ? "0" : "") + _r.getHours() + (_r.getMinutes() < 10 ? "0" : "") + _r.getMinutes() + (_r.getSeconds() < 10 ? "0" : "") + _r.getSeconds()) : undefined;
		}
	};
	
	this.UI = (new function() {
		var uThis = this;
		
		this.correctCheckRadio = function(labelClass) {
			if(sThis.isIE8) {
				$("body").on("click", labelClass, function() {
					var _ele = $(this),
						_children = _ele.parent().children("#" + _ele.attr("for"));
					if(_children.attr("type").toLowerCase() == "checkbox") {
						if(_children.is(":checked")) {
							_ele.removeClass("checked");
							_ele.removeClass("select");
							_children.prop("checked",  false).trigger("change");
						}
						else {
							_ele.addClass("checked");
							_ele.addClass("select");
							_children.prop("checked",  "checked").trigger("change");
						}
					}
					else if(!_children.is(":checked")) {
						$("[name=" + _children.prop("name") + "]", $(_children.prop("form"))).each(function() {
							$("label[for=" + $(this).prop("checked", false).prop("id") + "]").removeClass("checked").removeClass("select");
						});
						$("label[for=" + _children.prop("id") + "]", $(_children.prop("form"))).addClass("checked").addClass("select");
						_children.prop("checked", "checked").trigger("change");
					}
				});
			}
		};
		
		this.createOptions = function(select, data, callback, idFiled, nameField, selected, first, fireEvent) {
			var _i,
				_ele,
				_items = [],
				_item,
				_curr,
				_len,
				_selected,
				_isSelect;
			
			if(!idFiled) {
				idFiled = "id";
			}
			if(!nameField) {
				nameField = "name";
			}
			
			select = $(select).empty();
			if(data) {
				_len = data.length
				if(_len > 1) {
					if(first != "none") {
						select.append(_item = typeof first == "object" ? $('<option value="' + first[idFiled] + '">' + first[nameField] + '</option>') : $('<option></option>'));
						_items.push(_item);
					}
				}
				for(i = 0; i < data.length; i++) {
					_curr = data[i];
					_isSelect = (selected !== undefined
									? (selected instanceof Array
											? ($.inArray(_curr[idFiled], selected)
												? 1
												: 0)
											: _curr[idFiled] == selected
													? 1
													: 0)
									: 0);
					
					select.append(_item = $('<option value="' + _curr[idFiled] + '">' + _curr[nameField] + '</option>'));
					if(_isSelect) {
						_item.prop("selected", true);
						_selected = true;
					}
					_item.data("data", _curr);
					_items.push(_item);
				}
				if(!_selected) {
					if(_items[0]) {
						_items[0].prop("selected", true);
					}
				}
				if(callback) {
					callback(data);
				}
				if(!callback || fireEvent) {
					select.trigger("optionCreate", {data: data});
				}
				if(_isIE) {
					select.width(select.width());
				}
			}
		};
	});
	
	this.iframes = new function() {
		var _fThis = this;
		
		this.correctUrl = function(src) {
			if(src) {
				src = sThis.URL.parseURL(src);
				if(location.params.refi) {
					src.params.refi = location.params.refi
				}
				src.params.ref = location.params.ref ? location.params.ref : encodeURIComponent(document.referrer.split("#")[0]);
			}
			return src ? bstdSystem.URL.URLObject2String(src) : "";
		};
	};
	
	
	this.Validator = new (function() {
		var sVThis = this,
		_DataValidateTypes = {};
		
		/**
		 * Валидатор регулярными выражениями
		 */
		_DataValidateTypes.regexp = function(data, regexp) {
			var _reg,
				_regExp,
				_i,
				_options = "",
				_opt;
			if(!regexp) {
				if(_SYS_DEBUG) {
					throw new Error("Argument regexp is error");
				}
				return false;
			}
			
			_reg = regexp.match(/\/?(.+?)\/(\w?)/);
			_opt = _reg[2].toLowerCase();
			
			for(_i = 0; _i < _opt.length; _i ++) {
				switch(_opt.charAt(_i)) {
					case "i":
						_options += "i";
						break;
					case "m":
						_options += "m";
						break;
					case "g":
						_options += "g";
						break;
				}
			}
			return ((new RegExp(_reg[1], _options)).test(data)) ? data : false;
		};
		_DataValidateTypes.reg = 
			_DataValidateTypes.rg = 
				_DataValidateTypes.r = _DataValidateTypes.regexp;
		
		this.AddValidator = function(names, func) {
			var _i;
			
			if(!names || typeof names != "string" && !(names instanceof Array) || typeof func != "function") {
				if(_SYS_DEBUG) {
					throw new Error("names or function is error");
				}
				return;
			}
			if(typeof names == "string") {
				_DataValidateTypes[names] = func;
			}
			else {
				for(_i = 0; _i < names.length; _i ++) {
					if(_SYS_DEBUG) {
						throw new Error("One value from array names is not string");
					}
					else continue;
					_DataValidateTypes[names[_i]] = func;
				}
			}
		};
		
		this.IsValidator = function(name) {
			if(typeof name != "string") {
				if(_SYS_DEBUG) {
					throw new Error("Argumrnt name is not string");
				}
				return false;
			}
			return !!(name in _DataValidateTypes);
		};
		
		this.Validate = function(validateType, value, data, currentObject) {
			var _ret,
				_val,
				_current;
			switch(validateType) {
				case "compare":
					if(_val = /(?:(_?[\w\d]+):)([#.]?)([\w\d\-_]+)/.exec(value)) {
						if(_val[1] == "_current") {
							if(currentObject) {
								if(_val[2] == "#") {
									_ret = ($("#" + _val[3], currentObject).val() == data);
								}
							}
						}
					}
					break;
				default:
					if(validateType in _DataValidateTypes) {
						_ret = _DataValidateTypes[validateType](data, value);
					}
					else {
						if(_SYS_DEBUG) {
							throw new Error("validateType '" + validateType + "' is not found");
						}
					}
			}
			return _ret;
		};
	});
	
	
	this.access = new (function() {
		var aThis = this,
			_permissions = {},
			_scope,
			_currentParent,
			_accountType;
		
		var _prepare = function(data) {
			var _i,
				_len,
				_current,
				_service;
			if(data instanceof Array) {
				_len = data.length;
				for(_i = 0; _i < _len; _i ++) {
					_current = data[_i];
					_permissions[_current["serviceId"]] = parseInt(_current["permissions"]);
				}
			}
			else if(typeof data == "object" && data["serviceId"] && data["permissions"]) {
				_permissions[data["serviceId"]] = parseInt(data["permissions"]);
			}
		};
		
		var _prepareScope = function(data) {
			_scope = data;
		};
		
		this.checkPermission = function(service, permission) {
			var _res = false,
				_i;
			if(!service || !permission || !_permissions[service]) {
				return false;
			}
			if(permission instanceof Array) {
				for(_i = 0; _i < permission.length; _i ++) {
					if(parseInt(permission[_i]) & _permissions[service]) {
						_res = true;
						break;
					}
				}
				return _res;
			}
			else {
				return !!(parseInt(permission) & _permissions[service]);
			}
		};
		
		this.checkScope = function(scope, type) {
			var _res = false;
			if(_scope instanceof Array) {
				switch(_scope[0]) {
					case "all":
						_res = true;
						break;
					case "partner&dealers":
						_res = type && $.inArray(type, ["dealer", "partner", 1, 2, 3, 4, 5, 6]);
						break;
					case "dealer":
						if(scope == "_current") {
							scope = _currentParent;
						}
						_res = type && $.inArray(type, ["dealer", 1, 6]) && $.inArray(scope, _scope[1]);
						break;
					case "partner":
						if(scope == "_current") {
							scope = _currentParent;
						}
						_res = type && $.inArray(type, ["partner", 2, 4, 5]) && $.inArray(scope, _scope[1]);
						break;
				}
			}
			return _res;
		};
		
		this.loadScope = function(callback) {
			sThis.Request.Send(_brandId == 1 ? "http://contacts.toyota.ru/admin/getScope" : "http://content.lexus-russia.ru/admin/getScope",
								{},
								null,
								null,
								function(callback) {
									return function(data) {
										_prepareScope(data);
										_loadScope = false;
										_load();
										if(typeof callback == "function") {
											callback();
										}
									};
								}(callback));
								
			sThis.Request.Send(_brandId == 1 ? "http://contacts.toyota.ru/admin/data" : "http://content.lexus-russia.ru/admin/data",
								{},
								null,
								null,
								function(data) {
									_accountType = data.userTypeId;
									_currentParent = data.partnerId;
									_loadAccountData = false;
									_load();
								});
		};
		
		this.loadPermission = function(service, callback) {
			sThis.Request.Send(_brandId == 1 ? "http://contacts.toyota.ru/admin/getPermissions" : "http://content.lexus-russia.ru/admin/getPermissions",
								{d: {serviceId: service}},
								null,
								null,
								function(callback) {
									return function(data) {
										_prepare(data);
										_loadPermission = false;
										_load();
										if(typeof callback == "function") {
											callback();
										}
									};
								}(callback));
		};
		
		
		this.checkAccountType = function(type) {
			return type instanceof Array ? $.inArray(_accountType, type) != -1 : type == _accountType;
		};
	});
	
	
	this.ga = function(accountId, test, domain) {
		if(!test) {
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		}
		try {
			_gaq = window["_gaq"] || [];
		}
		catch(Error) {
			_gaq = [];
		}
		sThis.ga = {
			event: function(category, action, label, value, noninteraction) {
				_gaq.push(['_trackEvent', category, action, label, value, noninteraction]);
				_gaq.push(['_trackPageview']);
			},
			customVar: function(slot, name, value, scope) {
				_gaq.push(['_setCustomVar', slot, name, value, scope]);
				_gaq.push(['_trackPageview']);
			}
		};
		
		_gaq.push(['_setAccount', accountId]);
		if(domain) {
			_gaq.push ([ '_setDomainName',  domain]);
			_gaq.push ([ '_setAllowLinker' ,  true ]);
			sThis.ga["isCrossdomain"] = true;
			sThis.ga["link"] = function(link) {
				_gat._getTrackerByName()._link(link);
				_gaq.push(['_trackPageview']);
				return false;
			};
			sThis.ga["linkPost"] = function(form) {
				_gat._getTrackerByName()._linkByPost(form);
				return false;
			};
		}
		_gaq.push(['_trackPageview']);
	};
	
	this.storage = new function() {
		var tThis = this,
			_handlers = {},
			_ignored = {};
		
		var _change = function(e) {
			var _group,
				_key,
				_i,
				_len,
				_curr,
				_type,
				_operation,
				_data;
			
			if (!e) {
				e = window.event;
			}
			if(!e || !e.key) {
				return;
			}
			if(_ignored && _ignored[e.key] == e.newValue) {
				return;
			}
			_group = /^(~?)(.+?)~(.+)$/.exec(e.key);
			_type = !!_group[1];
			_key = _group[3];
			_group = _group[2];
			_operation = e.oldValue === null ? "remove" : e.oldValue === null ? "add" : "update";
			_data = {group: _decodeKey(_group),
					key: _decodeKey(_key),
					oldValue: _prepareType(e.oldValue),
					newValue: _prepareType(e.newValue),
					url: e.url ? e.url : e.uri,
					operation: _operation};
			
			if(_handlers[_group]) {
				if("*" in _handlers[_group]) {
					if("*" in _handlers[_group]["*"]) {
						_curr = _handlers[_group]["*"]["*"];
						_len = _curr.length;
						for(_i = 0; _i < _len; _i ++) {
							_curr[_i](_data);
						}
					}
					if(_operation in _handlers[_group]["*"]) {
						_curr = _handlers[_group]["*"][_operation];
						_len = _curr.length;
						for(_i = 0; _i < _len; _i ++) {
							_curr[_i](_data);
						}
					}
				}
				if(_handlers[_group][_key]) {
					if("*" in _handlers[_group][_key]) {
						_curr = _handlers[_group][_key]["*"];
						_len = _curr.length;
						for(_i = 0; _i < _len; _i ++) {
							_curr[_i](_data);
						}
					}
					if(_operation in _handlers[_group][_key]) {
						_curr = _handlers[_group][_key][_operation];
						_len = _curr.length;
						for(_i = 0; _i < _len; _i ++) {
							_curr[_i](_data);
						}
					}
				}
			}
		};
		
		var _codeKey = function(key) {
			return key === undefined ? "~^" : key === null ? "~%" : key;
		}
		
		var _decodeKey = function(key) {
			return key == "~^" ? undefined : key == "~%" ? null : key;
		};
		
		var _prepareType = function(data) {
			var _ret = data,
				_type;

			if(data) {
				_type = data.charAt(0);
				_ret = data.slice(1);
				switch(_type) {
					case 0:
						_ret = $.parseJSON(_ret);
						break;
					case 1:
						break;
					case 2:
						_ret = parseInt(_ret);
						break;
					case 3:
						_ret = parseFloat(_ret);
						break;
					case 4:
						_ret = null;
						break;
					case 5:
						_ret = undefined;
						break;
					case 6:
						_ret = NaN;
						break;
				}
			}
			return _ret;
		};
		
		var _repareData = function(data) {
			if(data === null) {
				data = "4";
			}
			else if(data === undefined) {
				data = "5";
			}
			else if(typeof data == "object") {
				data = "0" + $.toJSON(data);
			}
			else if(typeof data == "string" || typeof data == "number") {
				if(/\d+.\d+/.test(data)) {
					data = "3" + data;
				}
				else if(/\d+/.test(data)) {
					data = "2" + data;
				}
				else {
					data = "1" + data;
				}
			}
			else if(isNaN(data)) {
				data = "6";
			}
			return data;
		};
		
		var _getFullKey = function(group, key) {
			return _codeKey(group) + "~" + _codeKey(key);
		};
		
		this.is = (function() {
			try {
				return 'localStorage' in window && window['localStorage'] !== null;
			} catch (e) {
				return false;
			}
		})();
		
		this.set = this.is ? function (group, key, data) {
			var _key = _getFullKey(group, key);
			data = _repareData(data);
			_ignored[_key] = data;
			try {
				return window.localStorage.setItem(_key, data);
			}
			catch(e) {
				//TODO:
			}
		} : function () {return false;};
		
		this.get = this.is ? function (group, key) {
			var _ret;
			key = _codeKey(key);
			group = _codeKey(group);
			_ret = window.localStorage.getItem(_getFullKey(group, key));
			if(_ret !== null) {
				_ret = _prepareType(_ret);
			}
			return _ret;
		} : function () {return false;};
		
		this.remove = this.is ? function (group, key) {
			window.localStorage.removeItem(_getFullKey(group, key));
			
		} : function () {return false;};
		
		this.clear = this.is ? function (group, all) {
			if(all) {
				window.localStorage.clear();
			}
			else if(group) {
				group = _codeKey(group);
				for(_i in window.localStorage) {
					if((new RegExp('^' + group + '~')).test(_i)) {
						window.localStorage.removeItem(_i);
					}
				}
			}
			
		} : function () {return false;};
		
		this.onChange = this.is ? function (group, key, operation, handler) {
			if(typeof handler == "function") {
				key = _codeKey(key);
				group = _codeKey(group);
				if(!_handlers[group]) {
					_handlers[group] = {};
				}
				if(!_handlers[group][key]) {
					_handlers[group][key] = [];
				}
				operation = operation.toLowerCase();
				if(!operation || !$.inArray(operation, ["add", "update", "remove"])) {
					operation = "*"
				}
				if(!_handlers[group][key][operation]) {
					_handlers[group][key][operation] = [];
				}
				_handlers[group][key][operation].push(handler);
				return true;
			}
			return false;
		} : function () {return false;};
	};
});

var bstdElementsTypes = window["bstdElementsTypes"] = bstdSystem.ElementsTypes;
var bstdURL = window["bstdURL"] = bstdSystem.URL;
var bstdTemplater = window["bstdTemplater"] = bstdSystem.templater;

var bstdData = window["bstdData"] = {
	Validator: bstdSystem.Validator
};
