var bstdCatalogueViewer = function(ele) {
	
	var sThis = this,
		_viewer = $(ele),
		_menu = ele.find("[bstd_cviewer_menu]"),
		_catalogueViewerView = ele.find("[catalogue_viewer_view]"),
		_btnPreview = ele.find("[bstd_cviewer_preview]"),
		_btnNext = ele.find("[bstd_cviewer_next]"),
		_pagesContainer = ele.find("[bstd_cviewer_pages_container]"),
		_pagesTemplate = $(ele.find("[bstd_cviewer_page]").remove()),
		_pagesNavigator = ele.find("[bstd_cviewer_nav]"),
		_pages = [],
		_Flipping,
		_FlippingEnd,
		_path,
		_prefix,
		_posrfix,
		_start,
		_count,
		_height,
		_width;
	
	var _loadPagesInfo = function(url) {
		$.ajax(
			{
				url: 		url,
				type:		"get",
				dataType:	'json',
				success:	_preparePagesInfo
			}
		);
	};
	
	var _createPage = function(pageInfo, index) {
		var _page = _pagesTemplate.clone(),
			_left = $("bstd_cviewer_left", _page)[0],
			_right = $("bstd_cviewer_right", _page)[0];

		if(!pageInfo) {
			return;
		}
		if(pageInfo.left) {
			_left._src = pageInfo.left
		}
		else {
			_left.hide();
		}
		if(pageInfo.right) {
			_right._src = pageInfo.right
		}
		else {
			_right.hide();
		}
		_Flipping.addSlide(_page, index);
		//TODO: Добавить позиционирование в DOM-дереве
		_pagesContainer.append(_page);
	};
	
	var _loadPages = function(pagesInfo) {
		var _i,
			_sets,
			_page,
			_pageObj,
			_p,
			_url,
			_st = _start - 1;
		
		if(pagesInfo && pagesInfo.sets) {
			_sets = pagesInfo.sets;
			
			_path = _sets.path ? _sets.path : "";
			
			if(_sets.start !== undefined) {
				_start = parseInt(_sets.start);
			}
			
			if(_sets.count !== undefined) {
				_count = parseInt(_sets.count);
			}
			
			if(_sets.prefix !== undefined) {
				_prefix = _sets.prefix;
			}
			
			if(_sets.posrfix !== undefined) {
				_posrfix = _sets.posrfix;
			}
			
			if(_count) {
				for(_i = 1; _i <= _count; _i++) {
					
					_url = pagesInfo.pages && pagesInfo.pages[_i.toString()] ? 
							pagesInfo.pages[_i.toString()].url ?
									pagesInfo.pages[_i.toString()].url :
										_path + _prefix + (_i + _st) + posrfix :
											_path + _prefix + (_i + _st) + posrfix
					
					if(_i%2 == _i / 2) {
						_pageObj = {};
						_pageObj.left = _url;
					}
					else {
						_pageObj.rigth = _url;
						_createPage(_pageObj);
					}
				}
			}
			else {
				for(_i in pagesInfo.pages) {
					//TODO
				}
			}
		}
	};
	
	var _collapse = function(ele, ele2) {
		var _dw = parseInt(_width / 30),
			_timer;
		_timer = window.setInterval(10 function(ele, ele2) {
			return function() {
				var _w = ele.width() - _dw;
				if(_w <= 0) {
					_w = 0;
					window.clearInterval(_timer);
					_expand(ele2);
				}
				else {
					_w += "px"; 
				}
				ele.width(_w);
			};
		}($(ele, ele2)));
	};
	
	var _expand = function(ele) {
		var _dw = parseInt(_width / 30),
			_timer;
		ele.hide();
		
		ele.css("z-index", 3);
		
		_timer = window.setInterval(10 function(ele, ele2) {
			return function() {
				var _w = ele.width() + _dw;
				if(_w >= _width) {
					_w = _width;
					window.clearInterval(_timer);
					_FlippingEnd();
				}
				else {
					_w += "px"; 
				}
				ele.width(_w);
			};
		}($(ele, ele2)));
		
		ele.css("z-index", 2);
	};
	
	var _filp = function(e, detail) {
		var _ele,
			_ele2;
		
		if(detail.course = -1) {
			_ele2 = $("bstd_cviewer_left", detail.preview)[0];
			_ele = $("bstd_cviewer_right", detail.current)[0];
		}
		else {
			_ele = $("bstd_cviewer_left", detail.current)[0];
			_ele2 = $("bstd_cviewer_right", detail.preview)[0];
		}
		_left = $("bstd_cviewer_left", detail.current)[0],
		_right = $("bstd_cviewer_right", detail.current)[0];
		
		_FlippingEnd = detail.callback;
		
		_collapse(_ele, _ele2);
	};
	
	_Flipping = _viewer.bstdFlipping = new bstdFlipping({
		"container" = _viewer,
		"buttonNext" = _btnNext,
		"buttonPrevious" = _btnPreview,
		"autostart" = false,
		"onFlipping" = filp
	})
};
//bstd_cviewer