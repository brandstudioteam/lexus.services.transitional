	var _prepareData = function(_currentAction) {
		var _td = _currentAction.testDrives,
			_races = _currentAction.races,
			_carsTD = _currentAction.testDrivesCars,
			_carInTestDrives = _currentAction.testDrivesCars,
			_tdPartners = _currentAction.testDrivesPartners,
			_tdPartnersTypes = _currentAction.testDrivesPartnersTypes,
			_racesPartners = _currentAction.racesPartners,
			_racesPartnersTypes = _currentAction.racesPartnersTypes,
			_dealers,
			_agency,
			_i,
			_len = _td.length,
			_current,
			_currObj,
			_tmpCarsObj;
//Подготавливаем тест-драйвы
		_currentAction.testDrivesObj = {};
		for(_i = 0; _i < _len; _i ++) {
			_current = _td[_i];
			_current["races"] = {};
			_current["partnersTypes"] = {};
			_current["dealers"] = {};
			if(_currentCampaign.campaignUseAgency == 1) {
				_current["agencies"] = {};
			}
			if(_currentCampaign.campaignUseCars == 1) {
				_current["cars"] = {};
			}
			_currentAction.testDrivesObj[_current["testDriveId"]] = _current;
		}
		_td = _currentAction.testDrivesObj;
		
		if(_currentCampaign.campaignUseCars == 1) {
//Подготавливаем автомобили в тест-драйве
			if(_carInTestDrives) {
				_currentAction.testDrivesCarsObj = {};
				_tmpCarsObj = {};
				_len = _carsTD.length;
				for(_i = 0; _i < _len; _i ++) {
					_current = _carsTD[_i];
					_td[_current["testDriveId"]]["cars"][_current["modelId"]] = _currentAction.testDrivesCarsObj[_current["modelId"]] = _current;
					_current["partnersTypes"] = {};
					_current["dealers"] = {};
					if(_currentCampaign.campaignUseAgency == 1) {
						_current["agencies"] = {};
					}
					
					
					_tmpCarsObj[_current["modelId"]] = {};
					_tmpCarsObj[_current["modelId"]]["modelId"] = _current["modelId"];
					_tmpCarsObj[_current["modelId"]]["carName"] = _current["carName"];
					_tmpCarsObj[_current["modelId"]]["testDriveId"] = _current["testDriveId"];
					
					_tmpCarsObj[_current["modelId"]]["partnersTypes"] = {};
					
					_tmpCarsObj[_current["modelId"]]["dealers"] = {};
					_tmpCarsObj[_current["modelId"]]["partnersTypes"]["dealers"] = {};
					
					if(_currentCampaign.campaignUseAgency == 1) {
						_tmpCarsObj[_current["modelId"]]["agencies"] = {};
						_tmpCarsObj[_current["modelId"]]["partnersTypes"]["agencies"] = {};
					}
				}
			}
		}
		
//Подготавливаем типы партнеров в тест-драйвах
		_currentAction.testDrivesPartnersTypesObj = {};
		_len = _tdPartnersTypes.length;
		for(_i = 0; _i < _len; _i ++) {
			_current = _tdPartnersTypes[_i];
			if(_currentCampaign.campaignUseCars == 1) {
				if(_current["partnerTypeId"] == 2) {
					_td[_current["testDriveId"]]["cars"][_current["modelId"]]["partnersTypes"]["agencies"] = _currentAction.testDrivesPartnersTypesObj["agencies"][_current["modelId"]] = _current;
				}
				else {
					_td[_current["testDriveId"]]["cars"][_current["modelId"]]["partnersTypes"]["dealers"] = _currentAction.testDrivesPartnersTypesObj["dealers"][_current["modelId"]] = _current;
				}
			}
			else {
				if(_current["partnerTypeId"] == 2) {
					_td[_current["testDriveId"]]["partnersTypes"]["agencies"] = _currentAction.testDrivesPartnersTypesObj["agencies"] = _current;
				}
				else {
					_td[_current["testDriveId"]]["partnersTypes"]["dealers"] = _currentAction.testDrivesPartnersTypesObj["dealers"] = _current;
				}
			}
		}
		_tdPartnersTypes = _currentAction.testDrivesPartnersTypesObj;
//Подготавливаем партнеров в тест-драйвах
		_currentAction.testDrivesPartnersObj = {};
		_currentAction.testDrivesPartnersObj["dealers"] = {};
		if(_currentCampaign.campaignUseAgency == 1) {
			_currentAction.testDrivesPartnersObj["agencies"] = {};
		}
		_len = _tdPartners.length;
		for(_i = 0; _i < _len; _i ++) {
			_current = _tdPartners[_i];
			if(_currentCampaign.campaignUseCars == 1) {
				if(_current["partnerTypeId"] == 2) {
					_td[_current["testDriveId"]]["cars"][_current["modelId"]]["agencies"][_current["partnerId"]] = _currentAction.testDrivesPartnersObj["agencies"][_current["modelId"]] = _current;
				}
				else {
					_td[_current["testDriveId"]]["cars"][_current["modelId"]]["dealers"][_current["partnerId"]] = _currentAction.testDrivesPartnersObj["dealers"][_current["modelId"]] = _current;
				}
			}
			else {
				if(_current["partnerTypeId"] == 2) {
					_td[_current["testDriveId"]]["agencies"][_current["partnerId"]] = _currentAction.testDrivesPartnersObj["agencies"][_current["partnerId"]] = _current;
				}
				else {
					_td[_current["testDriveId"]]["dealers"][_current["partnerId"]] = _currentAction.testDrivesPartnersObj["dealers"][_current["partnerId"]] = _current;
				}
			}
		}
		_tdPartners = _currentAction.testDrivesPartnersObj;
		
//Подготавливаем заезды
		_currentAction.racesObj = {};
		_len = _races.length;
		for(_i = 0; _i < _len; _i ++) {
			_current = _races[_i];
			if(_currentCampaign.campaignUseCars == 1) {
				_current["cars"] = jQuery.extend({}, _tmpCarsObj);
			}
			else {
				_current["partnersTypes"] = {};
				_current["dealers"] = {};
				if(_currentCampaign.campaignUseAgency == 1) {
					_current["agencies"] = {};
				}
			}
			_td[_current["testDriveId"]]["races"][_current["raceId"]] = _currentAction.racesObj[_current["raceId"]] = _current;
		}
		_races = _currentAction.racesObj;
		
//Подготавливаем типы партнеров в заездах
		if(_racesPartnersTypes) {
			_currentAction.racesPartnersTypesObj = {};
			_len = _racesPartnersTypes.length;
			if(_currentCampaign.campaignUseCars == 1) {
				for(_i = 0; _i < _len; _i ++) {
					_current = _racesPartnersTypes[_i];
					if(_current["partnerTypeId"] == 2) {
						_races[_current["racesId"]]["cars"][_current["modelId"]]["partnersTypes"]["agencies"] = _current;
						if(!_currentAction.racesPartnersTypesObj["agencies"]) {
							_currentAction.racesPartnersTypesObj["agencies"] = {};
						}
						if(!_currentAction.racesPartnersTypesObj["agencies"][_current["racesId"]]) {
							_currentAction.racesPartnersTypesObj["agencies"][_current["racesId"]] = {};
						}
						_currentAction.racesPartnersTypesObj["agencies"][_current["racesId"]][_current["modelId"]] = _current;
					}
					else {
						_races[_current["racesId"]]["cars"][_current["modelId"]]["partnersTypes"]["dealers"] = _current;
						if(!_currentAction.racesPartnersTypesObj["dealers"]) {
							_currentAction.racesPartnersTypesObj["dealers"] = {};
						}
						if(!_currentAction.racesPartnersTypesObj["dealers"][_current["racesId"]]) {
							_currentAction.racesPartnersTypesObj["dealers"][_current["racesId"]] = {};
						}
						_currentAction.racesPartnersTypesObj["dealers"][_current["racesId"]][_current["modelId"]] = _current;
					}
				}
			}
			else {
				for(_i = 0; _i < _len; _i ++) {
					_current = _racesPartnersTypes[_i];
					if(_current["partnerTypeId"] == 2) {
						_races[_current["racesId"]]["partnersTypes"]["agencies"] = _current;
						if(!_currentAction.racesPartnersTypesObj["agencies"]) {
							_currentAction.racesPartnersTypesObj["agencies"] = {};
						}
						_currentAction.racesPartnersTypesObj["agencies"][_current["racesId"]] = _current;
					}
					else {
						_races[_current["racesId"]]["partnersTypes"]["dealers"] = _current;
						if(!_currentAction.racesPartnersTypesObj["dealers"]) {
							_currentAction.racesPartnersTypesObj["dealers"] = {};
						}
						_currentAction.racesPartnersTypesObj["dealers"][_current["racesId"]] = _current;
					}
				}
			}
			_racesPartnersTypes = _currentAction.racesPartnersTypesObj;
		}
		
//Подготавливаем партнеров в заездах
		if(_racesPartners) {
			_currentAction.racesPartnersObj = {};
			_currentAction.racesPartnersObj["dealers"] = {};
			if(_currentCampaign.campaignUseAgency == 1) {
				_currentAction.racesPartnersObj["agencies"] = {};
			}
			_len = _racesPartners.length;
			if(_currentCampaign.campaignUseCars == 1) {
				for(_i = 0; _i < _len; _i ++) {
					_current = _racesPartners[_i];
					if(_current["partnerTypeId"] == 2) {
						_races[_current["racesId"]]["cars"][_current["modelId"]]["agencies"][_current["partnerId"]] = _current;
						if(!_currentAction.racesPartnersObj["agencies"][_current["partnerId"]]) {
							_currentAction.racesPartnersObj["agencies"][_current["partnerId"]] = {};
						}
						if(!_currentAction.racesPartnersObj["agencies"][_current["partnerId"]][_current["racesId"]]) {
							_currentAction.racesPartnersObj["agencies"][_current["partnerId"]][_current["racesId"]] = {};
						}
						_currentAction.racesPartnersObj["agencies"][_current["partnerId"]][_current["racesId"]][_current["modelId"]] = _current;
					}
					else {
						_races[_current["racesId"]]["cars"][_current["modelId"]]["dealers"][_current["partnerId"]] = _current;
						if(!_currentAction.racesPartnersObj["dealers"][_current["partnerId"]]) {
							_currentAction.racesPartnersObj["dealers"][_current["partnerId"]] = {};
						}
						if(!_currentAction.racesPartnersObj["dealers"][_current["partnerId"]][_current["racesId"]]) {
							_currentAction.racesPartnersObj["dealers"][_current["partnerId"]][_current["racesId"]] = {};
						}
						_currentAction.racesPartnersObj["dealers"][_current["partnerId"]][_current["racesId"]][_current["modelId"]] = _current;
					}
				}
			}
			else {
				for(_i = 0; _i < _len; _i ++) {
					_current = _racesPartners[_i];
					if(_current["partnerTypeId"] == 2) {
						_races[_current["racesId"]]["agencies"][_current["partnerId"]] = _current;
						if(!_currentAction.racesPartnersObj["agencies"][_current["partnerId"]]) {
							_currentAction.racesPartnersObj["agencies"][_current["partnerId"]] = {};
						}
						if(!_currentAction.racesPartnersObj["agencies"][_current["partnerId"]][_current["racesId"]]) {
							_currentAction.racesPartnersObj["agencies"][_current["partnerId"]][_current["racesId"]] = {};
						}
						_currentAction.racesPartnersObj["agencies"][_current["partnerId"]][_current["racesId"]] = _current;
					}
					else {
						_races[_current["racesId"]]["dealers"][_current["partnerId"]] = _current;
						if(!_currentAction.racesPartnersObj["dealers"][_current["partnerId"]]) {
							_currentAction.racesPartnersObj["dealers"][_current["partnerId"]] = {};
						}
						if(!_currentAction.racesPartnersObj["dealers"][_current["partnerId"]][_current["racesId"]]) {
							_currentAction.racesPartnersObj["dealers"][_current["partnerId"]][_current["racesId"]] = {};
						}
						_currentAction.racesPartnersObj["dealers"][_current["partnerId"]][_current["racesId"]] = _current;
					}
				}
			}
			_racesPartners = _currentAction.racesPartnersObj;
		}
		
		return _currentAction;
	};