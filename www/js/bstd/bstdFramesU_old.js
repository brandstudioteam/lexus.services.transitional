var _bstdBrowsers = new function(){
	var _tmp;
	
	this.ie6 = function() {
	  var browser = navigator.appName;
	  if (browser == "Microsoft Internet Explorer"){
	    var b_version = navigator.appVersion;
	    var re = /\MSIE\s+(\d\.\d\b)/;
	    var res = b_version.match(re);
	    if (res && res[1] && res[1] <= 6){
	      return true;
	    }
	  }
	  return false;
	}();
	this.ie7 = function() {
		  var browser = navigator.appName;
		  if (browser == "Microsoft Internet Explorer"){
		    var b_version = navigator.appVersion;
		    var re = /\MSIE\s+(\d\.\d\b)/;
		    var res = b_version.match(re);
		    if (res && res[1] && res[1] <= 7){
		      return true;
		    }
		  }
		  return false;
		}();
	
	_tmp = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
	this.opera = !!_tmp;
	this.operaUnder97 = !!(_tmp && parseFloat(_tmp[1]) < 9.7);
	this.chrome = !!/Chrome\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
	this.webkit = !!/WebKit\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
};

$bstd = {};
var _bstdA = function() {
	var r20 = /%20/g,
	rbracket = /\[\]$bstd/,
	rCRLF = /\r?\n/g,
	rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$bstd/i,
	rselectTextarea = /^(?:select|textarea)/i,
	core_push = Array.prototype.push,
	core_slice = Array.prototype.slice,
	core_indexOf = Array.prototype.indexOf,
	core_toString = Object.prototype.toString,
	core_hasOwn = Object.prototype.hasOwnProperty,
	core_trim = String.prototype.trim;
	
	if(!$bstd.isFunction) {
		$bstd.isFunction = function(val) {return typeof val == "function";};
	}
	if($bstd.isObject) {
		$bstdisObject = function() {return typeof c == "object";};
	}
	if(!$bstd.isArray) {
		$bstd.isArray = function(val) {return val instanceof Array;};
	}
	if(!$bstd.isWindow) {
		$bstd.isWindow = function( obj ) {
			return obj != null && obj == obj.window;
		};
	}
	if(!$bstd.isPlainObject) {
		$bstd.isPlainObject = function( obj ) {
			// Must be an Object.
			// Because of IE, we also have to check the presence of the constructor property.
			// Make sure that DOM nodes and window objects don't pass through, as well
			if (!obj || typeof obj !== "object" || obj.nodeType || $bstd.isWindow( obj ) ) {
				return false;
			}
			try {
				// Not own constructor property must be Object
				if (obj.constructor &&
					!core_hasOwn.call(obj, "constructor") &&
					!core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
						return false;
				}
			} catch (e) {
				// IE8,9 Will throw exceptions on certain host objects #9897
				return false;
			}

			// Own properties are enumerated firstly, so to speed up,
			// if last one is own, then all properties are own.

			var key;
			for(key in obj) {}
			return key === undefined || core_hasOwn.call(obj, key);
		};
	}
	if(!$bstd.isEmptyObject) {
		$bstd.isEmptyObject = function(obj) {
			var name;
			for (name in obj) {
				return false;
			}
			return true;
		};
	}

	function buildParams(prefix, obj, add) {
		var name;
	
		if($bstd.isArray(obj)) {
		// Serialize array item.
			$bstd.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
					// Treat each array item as a scalar.
					add(prefix, v);
	
				} else {
					// If array item is non-scalar (array or object), encode its
					// numeric index to resolve deserialization ambiguity issues.
					// Note that rack (as of 1.0.0) can't currently deserialize
					// nested arrays properly, and attempting to do so may cause
					// a server error. Possible fixes are to modify rack's
					// deserialization algorithm or to provide an option or flag
					// to force array serialization to be shallow.
					buildParams(prefix + "[" + ($bstd.isObject(v) ? i : "" ) + "]", v, traditional, add);
				}
			});
		} else if ($bstd.isObject(obj) === "object" ) {
			//Serialize object item.
			for ( name in obj ) {
				buildParams( prefix + "[" + name + "]", obj[ name ], add );
			}
		} else {
			// Serialize scalar item.
			add( prefix, obj );
		}
	};

	$bstd.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, value ) {
				// If value is a function, invoke it and return its value
				value = $bstd.isFunction( value ) ? value() : ( value == null ? "" : value );
				s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
			};

		// If an array was passed in, assume that it is an array of form elements.
		if($bstd.isArray(a) || !$bstd.isPlainObject(a)) {
			// Serialize the form elements
			$bstd.each(a, function() {
				add(this.name, this.value);
			});

		} else {
			// If traditional, encode the "old" way (the way 1.3.2 or older
			// did it), otherwise encode params recursively.
			for(prefix in a) {
				buildParams(prefix, a[prefix], traditional, add);
			}
		}

		// Return the resulting serialization
		return s.join("&").replace(r20, "+");
	};
	
	$bstd.inArray = function(elem, arr, i) {
		var _len,
			_core_indexOf = Array.prototype.indexOf;
		if(arr) {
			if(_core_indexOf) {
				return _core_indexOf.call(arr, elem, i);
			}
			_len = arr.length;
			i = i ? i < 0 ? Math.max(0, _len + i) : i : 0;
			for(; i < _len; i++) {
				if(i in arr && arr[ i ] === elem) {
					return i;
				}
			}
		}
		return -1;
	};
	$bstd.each = function(obj, callback, args) {
		var name,
			i = 0,
			length = obj.length,
			isObj = length === undefined || $bstd.isFunction(obj);
		
		if(args) {
			if(isObj) {
				for(name in obj) {
					if(callback.apply(obj[ name ], args) === false) {
						break;
					}
				}
			} else {
				for(; i < length;) {
					if(callback.apply( obj[ i++ ], args ) === false) {
						break;
					}
				}
			}
		} else {
			if(isObj) {
				for(name in obj) {
					if(callback.call(obj[ name ], name, obj[ name ]) === false) {
						break;
					}
				}
			} else {
				for(; i < length;) {
					if(callback.call(obj[ i ], i, obj[ i++ ]) === false) {
						break;
					}
				}
			}
		}
		return obj;
	};
	  var interval_id,
	    last_hash,
	    cache_bust = 1,
	    rm_callback,
	    window = this,
	    FALSE = !1,
	    postMessage = 'postMessage',
	    addEventListener = 'addEventListener',
	    _opera,
	    p_receiveMessage,
	    has_postMessage;
		_opera = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
		_opera = !!(_opera && parseFloat(_opera[1]) < 9.7);
	  	has_postMessage = window[postMessage] && !_opera;
	  $bstd[postMessage] = function( message, target_url, target ) {
	    if ( !target_url ) { return; }
	    message = typeof message === 'string' ? message : $bstd.param( message );
	    target = target || parent;
	    if ( has_postMessage ) {
	      target[postMessage]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$bstd1' ) );
	    } else if ( target_url ) {
	      target.location = target_url.replace( /#.*$bstd/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
	    }
	  };
	  $bstd.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
	    if ( has_postMessage ) {
	      if ( callback ) {
	        rm_callback && p_receiveMessage();
	        rm_callback = function(e) {
	        	if(e.originalEvent) {
	        		e = e.originalEvent;
	        	}
	          if ( ( typeof source_origin === 'string' && source_origin != "*" && e.origin !== source_origin )
	            || ( $bstd.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
	            return FALSE;
	          }
	          callback( e );
	        };
	      }
		if(window.addEventListener){
			window.addEventListener('message', rm_callback, false);
		} else {
			window.attachEvent('onmessage', rm_callback);
		}
	      
	    } else {
	      interval_id && clearInterval( interval_id );
	      interval_id = null;
	      if ( callback ) {
	        delay = typeof source_origin === 'number'
	          ? source_origin
	          : typeof delay === 'number'
	            ? delay
	            : 100;
	        interval_id = setInterval(function(){
	          var hash = document.location.hash,
	            re = /^#?\d+&/;
	          if ( hash !== last_hash && re.test( hash ) ) {
	            last_hash = hash;
	            callback({ data: hash.replace( re, '' ) });
	          }
	        }, delay );
	      }
	    }
	  };
	  
	  
	$bstd.receiveMessage(function(e){
		var _inData = e.data.split("&"),
			_outData = {},
			_iframe,
			_i,
			_itm;

		 for(_i = 0; _i < _inData.length; _i ++) {
			 _itm = _inData[_i].split("=");
			 if(_itm[0]) {
				 _outData[_itm[0]] = _itm[1];
			 }
		 }
		
		if(!isNaN(_outData["if_height"]) && _outData["if_height"] > 0) {
			if(_outData["id"]) {
				var _iframeId = _outData["id"];
				_iframe = document.getElementById(_iframeId);
				var _style = _iframe.style;
				if(parseInt(_outData["if_height"])) {
					_style.height = parseInt(_outData["if_height"]) + "px";
				}
			}
			else {
				var _iframes = document.getElementsByTagName("iframe");
				if(_iframes.length) {
					var _iframeItem = _iframes.item(0);
					var _iframeId = _iframeItem.id;
					_iframe = document.getElementById(_iframeId);
					var _style = _iframe.style;
					if(parseInt(_outData["if_height"])) {
						_style.height = parseInt(_outData["if_height"]) + "px";
					}
				}
			}
			
		}
	}, '*' );
	
		var _params = location.href.split("?"),
			_i,
			_tmp,
			_s,
			_str;
		location["params"] = {};
		if(_params.length > 1) {
			location["stringParams"] = _str = decodeURIComponent(_params[1].split("#")[0]);
			_params = location["stringParams"].split("&");
			for(_i = 0; _i < _params.length; _i++) {
				_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
	            if((_s = _tmp[1].indexOf("[]")) != -1) {
	                _tmp[1] = _tmp[1].substr(0, _s);
	            }
	            if(_tmp[2] === undefined) {
	                _tmp[2] = null;
	            }
				if(_tmp[1] in location["params"]) {
					if(location["params"][_tmp[1]] instanceof Array) {
						location["params"][_tmp[1]].push(_tmp[2]);
					}
					else location["params"][_tmp[1]] = [location["params"][_tmp[1]], _tmp[2]];
				}
				else location["params"][_tmp[1]] = _tmp[2];
			}
		}
	};
	_bstdA();