/*
 * @required
 * 
 * jquery
 * location.js
 * jquery.ba-postmessage.js
 * 
 */

var bstdFrames = (new function() {
	var sThis = this;
	var parent_url,
		_name,
		_id;

	if(window != top) {
		parent_url = decodeURIComponent(location.params["ref"] ? location.params["ref"] : document.location.hash.replace( /^#/, '' )),
		_name = location.params["refn"] ? location.params["refn"] : null,
		_id = location.params["refi"] ? location.params["refi"] : null;

		this.setHeight = function(add) {
			var _data = {};
			_data["if_height"] = parseInt($("#resizer").length ? $("#resizer").outerHeight(true) : $('#body').height()) + (!isNaN(parseInt(add)) ? parseInt(add) : 0) + (_bstdBrowsers.webkit ? 100 : 0); //$('body').outerHeight(true)
			_data["name"] = _name;
			_data["refi"] = _id;
			$.postMessage(_data, parent_url, parent);
		};
	}
	else {
		this.browsers = new function(){
			var _tmp;
			
			this.ie6 = function() {
			  var browser = navigator.appName;
			  if (browser == "Microsoft Internet Explorer"){
			    var b_version = navigator.appVersion;
			    var re = /\MSIE\s+(\d\.\d\b)/;
			    var res = b_version.match(re);
			    if (res && res[1] && res[1] <= 6){
			      return true;
			    }
			  }
			  return false;
			}();
			this.ie7 = function() {
				  var browser = navigator.appName;
				  if (browser == "Microsoft Internet Explorer"){
				    var b_version = navigator.appVersion;
				    var re = /\MSIE\s+(\d\.\d\b)/;
				    var res = b_version.match(re);
				    if (res && res[1] && res[1] <= 7){
				      return true;
				    }
				  }
				  return false;
				}();
			
			_tmp = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
			this.opera = !!_tmp;
			this.operaUnder97 = !!(_tmp && parseFloat(_tmp[1]) < 9.7);
			this.chrome = !!/Chrome\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
			this.webkit = !!/WebKit\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
		};
		
		this.registerIFrame = function(iframe, url) {
			var _id;
			
			if(!iframe) {return;}
			iframe = $(iframe)[0];
			_id = iframe.id;
			if(!_id) {iframe.id = _id = "ifrm_" +  + Math.random().toString().replace(".", "");}
			
			var _s = url.indexOf("?");
			if(_s == -1) {
				url = url + "?ref=" + encodeURIComponent(location.href.replace(/#.*$/, '')) + "&refi=" + encodeURIComponent(_id);
			}
			else {
				url = url + "&ref=" + encodeURIComponent(location.href.replace(/#.*$/, ''));
				if(url.indexOf("refi=", _s) == -1) {
					url = url + "&refi=" + encodeURIComponent(_id);
				}
			}
			
			iframe.src = url;
		};

		this.createIFrame = function(parent, url, name, id, width) {
			var _iframe = $("<iframe frameborder=\"0\" name=\"" + (name ? name : "ifrm_" +  + Math.random().toString().replace(".", "")) + "\" id=\"" + (id ? id : "ifrm_" +  + Math.random().toString().replace(".", "")) + "\">");
			if(url) {sThis.registerIFrame(_iframe, url);}
			if(width) {_iframe.width(width);}
			if(parent) {$(parent).append(_iframe);}
			return _iframe;
		};
		
		this.resizeIFrame = function(e){
			var _inData = e.data.split("&"),
				_outData = {},
				_iframe,
				_i,
				_itm;
	
			 for(_i = 0; _i < _inData.length; _i ++) {
				 _itm = _inData[_i].split("=");
				 if(_itm[0]) {
					 _outData[_itm[0]] = _itm[1];
				 }
			 }
			
			if(!isNaN(_outData["if_height"]) && _outData["if_height"] > 0) {
				if(_outData["id"]) {
					_iframe = $("#" + _outData["id"]);
				}
				else if(_outData["refi"]) {
					_iframe = $("#" + _outData["refi"]);
				}
				else if(_outData["name"]) {
					_iframe = $("iframe[name=" + _outData["name"] + "]");
				}
				else {
					_iframe = $("iframe").first();
				}
				if(_iframe) {
					$(_iframe).height(_outData["if_height"]).trigger("bstdIFrameResize", {event: e, height: _outData["if_height"]});
				}
			}
		};
		
		$.receiveMessage(this.resizeIFrame, '*' );
	}
});