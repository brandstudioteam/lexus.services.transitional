$(document).ready(function(){
	
	if(!_gaAccountId) {
		throw new Error("Google analitics account is not found");
	}
	
	var _gaq = _gaq || [];
    _gaq.push(['_setAccount', _gaAccountId]);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

      
    function track_ga (el)
    {
        var link = el.innerHTML  ;
        var _gaq = _gaq || [];  
        _gaq.push(['_trackEvent',
					'dealer_url', // category of activity
					link, // Action
				]); 

    }
};



$("[data-ga-te]").bind("click", function(e) {
		var e = $(e.currentTarget),
			_ename = e.attr("data-ga-te"),
			_eval = e[0].href && e[0].href != "javascript:void(0);" ? e[0].href : e.attr("data-link");
		
		try {
			_gaq = _gaq || [];
			_gaq.push(["_trackEvent",
	           _ename,
	           _eval
			]); 
		} catch (err){
			
		} 
	});