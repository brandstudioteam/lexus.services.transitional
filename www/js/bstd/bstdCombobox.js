/*
function( request, response ) {
						var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" ); //"^" + 
						response( select.children( "option" ).map(function() {
							var text = $( this ).text();
							if ( this.value && ( !request.term || matcher.test(text) ) )
								return {
									label: text.replace(
										new RegExp(
											"(?![^&;]+;)(?!<[^<>]*)(" + //^
											$.ui.autocomplete.escapeRegex(request.term) +
											")(?![^<>]*>)(?![^&;]+;)", "gi"
										), "<strong>$1</strong>" ),
									value: text,
									option: this
								};
						}) );
					},
*/

//http://www.marghoobsuleman.com/mywork/jcomponents/image-dropdown/samples/index.html

var bstdCombobox = function(params) {
	var sThis = this;
	var _select,
		_input,
		_button,
		_dataSource,
		_parent,
		_offset,
		_fields,
		_selectOpened,
		_timer,
		_mode,
		_pos;
	
	if(!params) {
		return;
	}
	
	_mode = params.mode ? params.mode : 1;
	
	if(!(params.select && (_select = $(params.select)) && typeof _select[0] == "object" && _select[0].tagName.toLowerCase() == "select")) {
		_select = null;
	}
	if(!(params.input && (_input = $(params.input)) && typeof _input[0] == "object" && _input[0].tagName.toLowerCase() == "input"))  {
		_input = null;
	}
	if(!(params.parent && (_parent = $(params.parent)) && typeof _parent[0] == "object" && _parent[0].tagName)) {
		_parent = null;
	}
	if(!_parent && !_input && !_select) {
		return;
	}
	
	if(!_input) {
		_input = $("<input type=\"text\" class=\"bstdComboboxInput\" >");
	}
	else {
		_parent = _input.parent();
		_offset = {};
		_offset.width = _input.width();
	}
	if(!_select) {
		$("body").append(_select = $("<select class=\"bstdComboboxSelect\">").hide());
	}
	else {
		if(!_parent) {
			_parent = _select.parent();
			_input.insertBefore(_select);
			_offset = {};
			_offset.width = _select.width();
		}
		$("body").append(_select.hide());
	}
	if(!_offset) {
		_parent.append(_input);
		_offset = {};
		_offset.width = _parent.innerWidth();
	}
	
	_button = $("<input type=\"button\">");
	_button.insertAfter(_input);
	_button.css({position: "relative", left: "-" + _button.outerWidth()});
	
	if(params.fields) {
		_fields = params.fields;
	}
	
	
	//$(".ui-autocomplete-input").each(function(){console.log($(this).next(), $(this).width(), $(this).next().width()); $(this).width($(this).width() + $(this).next().outerWidth() + "px"); $(this).next().css({left: "-" + $(this).next().css({position: "relative"}).outerWidth()})})
	
	this.Load = function(data, fields) {
		var _items = "",
			_item,
			j,
			_val,
			_text,
			_option;
		_select.empty();
		if(!fields) {
			fields = _fields;
		}
		if(!fields) {
			fields.value = "id";
			fields.text = "title";
		}
		if(data) {
			for(j = 0; j < data.length; j ++) {
				_item = _items[j];
				if(typeof _item == "object") {
					_val = "value=\"" + (_item[fields.value] !== undefined ? _item[fields.value] : "") + "\"";
					_text = _item[fields.text] !== undefined ? _item[fields.text] : "";
					(_option = $("<option " + _val + ">").text(_text))[0]["bstdComboItem"] = _item;
				}
				else {
					(_option = $("<option value=\"" + _item + ">").text(_text))[0]["bstdComboItem"] = _item;
				}
				_select.append(_option);
			}
		}
	};
	
	
	
	
	this.Open = function(text) {
		var _offset;
		
		if(typeof text != "string" || !text.length) return;
		
		_position();
		
		if(_mode == 1) {
			if(_findItems(text)) {
				_select.show();
				_selectOpened = true;
			}
		}
	};
	
	this.Close = function() {
		_select.hide();
		_selectOpened = false;
	};
	
	this.GetSelectedValue = function() {
		
	};
	
	this.GetSelectedText = function() {
		
	};
	
	this.GetSelectedItem = function() {
		
	};
	
	this.SetSelected = function() {
		
	};
	
	
	var _findItems = function(text) {
		var _ch = _select.children().hide(),
			_i,
			_cnt = false,
			_txt,
			_length,
			_item,
			_a;
		
		text = text.replace(/^\s*/, "\1").replace(/\s*$/, "").toLowerCase();
		_length = text.length;
		if(!_length) return false;
		
		for(_i = 0; _i < _ch.length; _i ++) {
			_item = $(_ch[_i]);
			_txt = _item.text().substr(0, _length).toLowerCase();
			_a = text.localeCompare(_txt);
			if(_a == 1) {
				continue;
			}
			else if(_a == 0) {
					_item.show();
					_cnt = true;
			}
			else if(_a == -1) {
				break;
			}
		}
		
		return _cnt;
	};
	
	
	var _position = function() {
		var _p = {},
			_o;

		if(!_pos && _input) {
			_o = _input.offset();
			_p.top = _o.top + _input.height + "px";
			_p.left = _o.left + "px";
			_select.css({top: _o.top + "px", left: _o.left + "px"});
		}
	};
	
	var _keyPress = function(e) {
		_timer = setTimeout(
			function() {
				clearTimeout(_timer);
				sThis.Open(_input.val());
			}
		); 
		
	};
	
	/*
	var _keyDown = function(e) {
		var _k = e.which || e.keyCode;
		if()
	};
	
	.bind("keydown", _keyDown)
	*/
	
	$(_input).bind("keydown", _keyPress);
	
	
	
	delete(params);
};