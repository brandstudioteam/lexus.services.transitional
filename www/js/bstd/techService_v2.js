var techService = (function() {
	var _formObj,
		_brands,
		_cities,
		_currentBrand,
		_currentDealer,
		_defaultCity,
		_dealerRCode,
		_userActions,
		_servicesTypes;

	var _loadedBrands = function(data) {
		_brands = true;
	};
	
	var _selectDefaultCity = function() {
		if(_cities && _defaultCity) {
			$("#cityId").children().prop("selected", false);
			$("#cityId option[value=" + _defaultCity + "]").prop("selected", true);
		}
	};
	
	var _loadedDefaultCity = function(data) {
		_defaultCity = data[0].cityId;
		_selectDefaultCity();
	};
	
	var _loadedCitiesList = function(data) {
		_selectDefaultCity();
	};
	
	var _loadDealers = function() {
		bstdLTAPI.UI.createDealers({
									cityId: $("#cityId").val(),
									callback: _loadedDealersList
								},
								"#partnerId");
	};
	
	var _loadedDealersList = function(data) {
		if($("#partnerId").val()) {
			_changeDealer();
		}
	};

	var _loadModels = function() {
		bstdLTAPI.UI.createTOModels({
										brandId: bstdSystem.isLexus ? _currentBrand : $("#brandId").val(),
										callback: _loadedModelsList
									},
									"#modelId");
	};
	
	var _loadedModelsList = function(data) {
		//createDealers = function(data, select, selected, first)
	};
	
	var _loadModelByVIN = function() {
		bstdLTAPI.toService.getModelByVin({
											vin: $("#vin").val(),
											callback: function(data) {
												if(bstdSystem.isLexus) {
													if(data && data.brandId !== _currentBrand) {
														bstdAlert("К облуживанию принимаются только автомобили Lexus", "Запись на техобслуживание");
														return;
													}
												}
												_loadedModelByVIN(data);
											}
										},
										"#modelId");
	};
	
	var _loadedModelByVIN = function(data) {
		$("#brandId").children().prop("selected", false);
		$("#brandId option[value=" + data.brandId + "]").prop("selected", true);
		bstdLTAPI.UI.createTOModels({
										brandId: data.brandId,
										callback: _loadedModelsList
									},
									"#modelId",
									data.modelId);
	};
	
	
	if(location.params.dealer) {
		_currentDealer = location.params.dealer;
		if(_currentDealer instanceof Array && _currentDealer.length == 1) {
			_currentDealer = _currentDealer[0];
		}
	}
	if(_currentDealer instanceof Array) {
		bstdLTAPI.UI.createDealers({
									partnerId: _currentDealer,
									callback: _loadedDealersList
								},
								"#partnerId");
	}
	else {
		//"count" => 0, "cityCount" => 0, "gLt" => 0, "gLg" => 0
		bstdLTAPI.geo.getNearestCities({
										gLg: bstdLTAPI.YMap.prototype.curretnLongitude(),
										gLn: bstdLTAPI.YMap.prototype.curretnLatitude(),
										count: 1,
										callback: _loadedDefaultCity
									});
		bstdLTAPI.UI.createDealersCities({callback: _loadedCitiesList}, "#cityId");
	}
	if(bstdSystem.isLexus) {
		_currentBrand = 2;
		_loadModels();
	}
	else {
		bstdLTAPI.UI.createTOBrands({callback: _loadedBrands}, "#brandId");
	}
	
	var _createUsersActions = function(data) {
							//select, data, callback, idFiled, nameField, selected, first, fireEvent
		bstdSystem.UI.createOptions($("#userActionId"),
									data,
									null,
									"usersActionId",
									"usersActionName");
	};
	
	var _changeDealer = function() {
		//_createUsersActions(bstdSystem.array.applyFilter(_userActions, {partnerId: $("#partnerId").val()}));
		var _tmp = {callback: function(data) {
									_createUsersActions(data);
								},
					partnerId: $("#partnerId").val()};
		bstdLTAPI.toService.getUsersActionList2(_tmp);
	}
	
	
	
	bstdSystem.onAfterLoad(function() {
		var _form = $("#toRecord"),
			_tmp;
		if(bstdSystem.isLexus) {
			$("#brandId").parents("tr").remove();
		}
		if(!_currentDealer) {
			$("tr[data-type=cityId]").show();
			$("tr[data-type=partnerId]").show();
			$("#cityId").on("change", _loadDealers);
		}
		else if(_currentDealer) {
			$("tr[data-type=cityId]").remove();
			if(_currentDealer instanceof Array) {
				$("tr[data-type=partnerId]").show();
			}
			else {
				_tmp = {callback: function(data) {
							_createUsersActions(data);
						},
						partnerId: _currentDealer
					};
				bstdLTAPI.toService.getUsersActionList2(_tmp);
				$("tr[data-type=partnerId]")
					.empty()
					.append('<input type="hidden" name="partnerId" value="' + _currentDealer + '">');
			}
			$("#isOwner").val("1");
		}
		$("#brandId").change(_loadModels);
		
		$("#agreement").bind("change", function() {
					if($("#agreement").attr("checked")) {
						$("#submit").show();
					}
					else {
						$("#submit").hide();
					}
				});

		$("#partnerId").on("change", _changeDealer);
		if(!bstdSystem.isLexus) {
			$("#brandId").on("change", _loadModels);
		}
		$("#vin").on("change", _loadModelByVIN);
		bstdLTAPI.UI.createTOServicesTypes({}, "#serviceType");
		
		_formObj = new bstdForm(_form,
								bstdForm.prototype.MODE_ADD, null,
								{
									action: "/z/techservice/register/",
									method: "POST"
								});
		_form.bind("succesfullSubmiting", function(e, data){
		/*
			if(typeof data.result == "object" && data.result.dealerRCode) {
				_dealerRCode = data.result.dealerRCode;
			}
		*/
			_formObj.disable();
			bstdAlert("Спасибо!<br />Ваше сообщение отправлено дилеру.<br />В ближайшее время представитель дилера свяжется с Вами.", "Запись на техобслуживание");
			//bstdLTAPI.ga.event('Click', 'Submit', _activeDealers ? 'Dealer' : 'Main', undefined, false);
		})
	});
}());