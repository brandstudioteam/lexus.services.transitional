window["isLexus"] = /lexus.ru/.test(location.href);
if(!window["bstdURL"]) {
	var bstdURL = window["bstdURL"] = new (function() {
		var uThis = this;

		var _prepareParams = function(key, val, obj) {
			var _s = key.indexOf("["),
				_e = key.lastIndexOf("]"),
				_key,
				_subKey,
				_obj = obj || {};

			if(_s != -1) {
				_key = key.substr(0, _s);
				if(_s + 1 < _e) {
					_subKey = key.substring(_s + 1, _e);
					if(_obj[_key] === undefined) {
						_obj[_key] = {};
					}
					_prepareParams(_subKey, val, _obj[_key]);
				}
				else {
					if(_obj[_key] === undefined) {
						_obj[_key] = [];
					}
					_obj[_key].push(val);
				}
			}
			else {
				_obj[key] = val;
			}
			return _obj;
		};

		this.isCrossdomain = function(url) {
			var _ret = false,
				_url;
			if(typeof url == "string") {
				_url = uThis.parseURL(url);
				if(_url.host && _url.host != location.host) {
					_ret = true;
				}
			}
			return _ret;
		};

		this.parseURL = function(url) {
/*
// This scary looking regular expression parses an absolute URL or its relative
			// variants (protocol, site, document, query, and hash), into the various
			// components (protocol, host, path, query, fragment, etc that make up the
			// URL as well as some other commonly used sub-parts. When used with RegExp.exec()
			// or String.match, it parses the URL into a results array that looks like this:
			//
			//     [0]: http://jblas:password@mycompany.com:8080/mail/inbox?msg=1234&type=unread#msg-content
			//     [1]: http://jblas:password@mycompany.com:8080/mail/inbox?msg=1234&type=unread
			//     [2]: http://jblas:password@mycompany.com:8080/mail/inbox
			//     [3]: http://jblas:password@mycompany.com:8080
			//     [4]: http:
			//     [5]: //
			//     [6]: jblas:password@mycompany.com:8080
			//     [7]: jblas:password
			//     [8]: jblas
			//     [9]: password
			//    [10]: mycompany.com:8080
			//    [11]: mycompany.com
			//    [12]: 8080
			//    [13]: /mail/inbox
			//    [14]: /mail/
			//    [15]: inbox
			//    [16]: ?msg=1234&type=unread
			//    [17]: #msg-content
			//
			urlParseRE: /^\s*(((([^:\/#\?]+:)?(?:(\/\/)((?:(([^:@\/#\?]+)(?:\:([^:@\/#\?]+))?)@)?(([^:\/#\?\]\[]+|\[[^\/\]@#?]+\])(?:\:([0-9]+))?))?)?)?((\/?(?:[^\/\?#]+\/+)*)([^\?#]*)))?(\?[^#]+)?)(#.*)?/,
*/
			var _url =/(?:((?:http[s]?|file):)\/{1,3})?([\d\w_\-а-яё]+\.[\d\w_\-а-яё.]+)?([^?]*?)(?:\?([^#]*))?(?:#(.*))?$/i.exec(url), // /(?:((?:http[s]?|file):)\/{1,3})?((?:[\d\w_\-а-яё]+.)[\d\w_\-а-яё.]+)?([^?]*)(?:\?([^#]*))?(?:#(.*))?/i.exec(url), //TODO:
				_params,
				_returned = {},
				_i,
				_tmp;

				_returned.protocol = _url[1] || null;
				_returned.host = _url[2] || null;
				_returned.basicHost = (_tmp = /www\.([\d\w.\-_]+)/.exec(_returned.host)) ? _tmp[1] : _returned.host;
				_returned.path = _url[3] || null;
				_returned.paramsString = _url[4] || null;
				_returned.hash = _url[5] || null;
				_returned.params = {};

				if(_returned.paramsString !== null) {
					_params = _returned.paramsString.split("&");
					for(_i = 0; _i < _params.length; _i ++) {
						if(!_params[_i]) {
							continue;
						}
						_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
						if(_tmp[2] === undefined) {
							_tmp[2] = null;
						}
						_prepareParams(_tmp[1], _tmp[2], _returned.params);
					}
				}
			return _returned;
		};

		this.replaceParams = function(params1, params2, ignored, added) {
			var _i,
				_returned = {};

			for(_i in params1) {
				if(params2[_i] !== undefined) {
					_returned[_i] = params2[_i];
				}
				else {
					if(ignored) {
						if("{" + _i + "}" != params1[_i]) {
							_returned[_i] = params1[_i];
						}
					}
					else {
						_returned[_i] = params1[_i];
					}
				}
			}
			if(added) {
				for(_i in params2) {
					if(_returned[_i] === undefined) {
						_returned[_i] = params2[_i];
					}
				}
			}
			return _returned;
		};


		var _repeat = function(string, count) {
			var _i,
				_str = "";
			count = Math.abs(parseInt(count));
			if(isNaN(count)) {
				return "";
			}
			for(_i = 0; _i < count; _i ++) {
				_str += string;
			}

			return _str;
		};

		var _serializeParamsExec = function(values, params/*&$Array*/, key, level)
		{
			var _key,
				_val,
				_ref;

			if(level === undefined) {
				level = 0;
			}

			if(typeof values == "object") {
				if(values instanceof Array) {
					for(_key = 0; _key < values.length; _key++)
					{
						_val = values[_key];
						if(typeof _val == "object")
						{
							level ++;
							_serializeParamsExec(_val, params, key ? key + "[" + _key : _key, level);
							level --;
						}
						else {
							params.push((key ? key + "[" + _key : _key) + _repeat("]", level) + "=" + _val);
						}
					}
				}
				else {
					for(_key in values)
					{
						_val = values[_key];
						if(typeof _val == "object")
						{
							level ++;
							_serializeParamsExec(_val, params, key ? key + "[" + _key : _key, level);
							level --;
						}
						else {
							if(level === 0) {
								if(_key == "ref") {
									_ref = _key + "=" + _val;
									continue;
								}
							}
							params.push((key ? key + "[" + _key : _key) + _repeat("]", level) + "=" + _val);
						}
					}
					if(_ref) {
						params.push(_ref);
					}
				}
			}
			return params;
		};

		this.serializeParams = function(paramsAsObject) {
			var _arr = [];
			_serializeParamsExec(paramsAsObject, _arr);
			return _arr.join("&");
		};

		this.URLObject2String = function(url) {
			var _srParams,
				_host = url.host ? url.host : "",
				_path = url.path ? url.path : "";
			if(url && typeof url == "object") {
				if(url && url.params) {
					_srParams = uThis.serializeParams(url.params);
				}
				return (_host ? (url.protocol ? url.protocol : location.protocol) + "//" + _host : "") + _path + (_srParams ? "?" + _srParams : "") + (url.hash ? "#" + url.hash : "");
			}
			else return url;
		};

		this.addHashParam = function(param) {
			var _i;
			for(_i in param) {
				if(!location.hash) {
					location.hash = _i + "=" + param[_i];
				}
				else {
					if((new RegExp(_i + "=[\\d\\w]+", "g")).test(location.hash)) {
						location.hash = location.hash.replace(new RegExp(_i + "=[\\d\\w]+", "g"), _i + "=" + param[_i], "g");
					}
					else {
						location.hash = location.hash + "&" + _i + "=" + param[_i];
					}
				}
			}
		};

		this.deleteHashParam = function(param) {
			var _i;
			if(!(param instanceof Array)) {
				param = [param];
			}
			if(location.hash) {
				for(_i = 0; _i < param.length; _i ++) {
					location.hash = location.hash.replace(new RegExp(_i + "=[\\d\\w]+", "g"), "", "g");
				}
			}
		};

		this.getHashParam = function(param) {
			var _i;
			return (location.hash && (_i = (new RegExp(param + "=([\\d\\w]+)")).exec(location.hash))) ? _i[1] : undefined;
		};

		this.CorrectedUrlForFrames = function(selectors) {
			var _i,
				_len;
			if(window != top && window["bstdFrames"] && bstdFrames.setHeight) {
				if(typeof selectors == "string") {
					selectors = [selectors];
				}
				if(selectors instanceof Array) {
					_len = selectors.length;
					for(_i = 0; _i < _len; _i++) {
						$(selectors[_i]).each(function(){
							var _e = $(this),
								_tag = this.tagName.toLowerCase(),
								_tagsAttr = {a: "href", form: "action", iframe: "src"},
								_attr = _tagsAttr[_tag],
								_href;
							if(!_attr) {
								return;
							}
							_href = uThis.parseURL(_e.attr(_attr));

							if(location.params && location.params.refi) {
								_href.params["refi"] = location.params.refi;
							}
							if(location.params && location.params.ref) {
								_href.params["ref"] = location.params.ref;
							}
							_e.attr(_attr, uThis.URLObject2String(_href));
						});
					}
				}
			}
		};
		
		this.correctTestUrl = function(url) {
			url = sThis.URL.parseURL(url);
			if(_isTest) {
				url.params["___dev_test"] = 1;
			}
			if(_clientUID) {
				url.params["cuid"] = _clientUID;
			}
			if(location.params["___view_error"]) {
				url.params["___view_error"] = 1;
			}
			if(location.params["___debug_log"]) {
				url.params["___debug_log"] = location.params["___debug_log"];
			}
			url = sThis.URL.URLObject2String(url);
			return url;
		};

		(function () {
			if(!location.params) {
				var _url = uThis.parseURL(location.href);
				location.stringParams = _url.paramsString;
				location.params = _url.params;
				location.basicHost = _url.basicHost;
			}
			location.parent = document.referrer ? uThis.parseURL(document.referrer) : null;
			location.ref = location.params && location.params.ref ? uThis.parseURL(decodeURIComponent(location.params.ref)) : null;
		})();
	});
}

if(!location["params"]) {
	(function(){var _params=location.href.split("?"),_i,_tmp,_s,_str;location["params"]={};if(_params.length > 1){location["stringParams"]=_str=decodeURIComponent(_params[1].split("#")[0]);_params=location["stringParams"].split("&");for(_i=0;_i < _params.length;_i++){_tmp=new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);if((_s=_tmp[1].indexOf("[]"))!=-1){_tmp[1]=_tmp[1].substr(0, _s);}if(_tmp[2]===undefined){_tmp[2]=null;}if(_tmp[1] in location["params"]){if(location["params"][_tmp[1]] instanceof Array){location["params"][_tmp[1]].push(_tmp[2]);}else location["params"][_tmp[1]]=[location["params"][_tmp[1]], _tmp[2]];}else location["params"][_tmp[1]]=_tmp[2];}}return location["params"];})();
};

var bstdFrames = new function() {
	var sThis = this;
	var r20 = /%20/g,
		rbracket = /\[\]$/,
		rCRLF = /\r?\n/g,
		rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
		rselectTextarea = /^(?:select|textarea)/i,
		core_push = Array.prototype.push,
		core_slice = Array.prototype.slice,
		core_indexOf = Array.prototype.indexOf,
		core_toString = Object.prototype.toString,
		core_hasOwn = Object.prototype.hasOwnProperty,
		core_trim = String.prototype.trim,
		
		interval_id,
	    last_hash,
	    cache_bust = 1,
	    rm_callback,
	    FALSE = !1,
	    p_receiveMessage,
	    has_postMessage,
	    
	    _readyHandlers = [],
	    _isReady;
	
	this.browsers = new function(){
		var _tmp;
		
		this.ie6 = function() {
		  var browser = navigator.appName;
		  if (browser == "Microsoft Internet Explorer"){
		    var b_version = navigator.appVersion;
		    var re = /\MSIE\s+(\d\.\d\b)/;
		    var res = b_version.match(re);
		    if (res && res[1] && res[1] <= 6){
		      return true;
		    }
		  }
		  return false;
		}();
		this.ie7 = function() {
			  var browser = navigator.appName;
			  if (browser == "Microsoft Internet Explorer"){
			    var b_version = navigator.appVersion;
			    var re = /\MSIE\s+(\d\.\d\b)/;
			    var res = b_version.match(re);
			    if (res && res[1] && res[1] <= 7){
			      return true;
			    }
			  }
			  return false;
			}();
		
		_tmp = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
		this.opera = !!_tmp;
		this.operaUnder97 = !!(_tmp && parseFloat(_tmp[1]) < 9.7);
		this.chrome = !!/Chrome\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
		this.webkit = !!/WebKit\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
	};
	has_postMessage = window["postMessage"] && !sThis.browsers.operaUnder97;
	this.isFunction = function(val) {return typeof val == "function";};
	this.isObject = function() {return typeof c == "object";};
	this.isArray = function(val) {return val instanceof Array;};
	this.isWindow = function( obj ) {return obj != null && obj == obj.window;};
	this.isPlainObject = function( obj ) {
		var key;
		if (!obj || typeof obj !== "object" || obj.nodeType || sThis.isWindow( obj ) ) {
			return false;
		}
		try {
			if (obj.constructor &&
				!core_hasOwn.call(obj, "constructor") &&
				!core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
					return false;
			}
		} catch (e) {
			return false;
		}
		for(key in obj) {}
		return key === undefined || core_hasOwn.call(obj, key);
	};
	this.isEmptyObject = function(obj) {
		var name;
		for (name in obj) {
			return false;
		}
		return true;
	};

	this.onReady = function(func) {
		if(typeof func == "function") {
			if(_isReady) {
				_readyHandlers();
			}
			else {
				_readyHandlers.push(func);
			}
		}
	};
	
	
	var _ready = function() {
		var _i,
			_len = _readyHandlers.length - 1;
		_isReady = true;
		for(_i = _len; _i >= 0; _i --){
			_readyHandlers[_i]();
		}
		_readyHandlers = [];
	};
	
	// Mozilla, Opera and webkit nightlies currently support this event
	if ( document.addEventListener ) {
		// Use the handy event callback
		document.addEventListener( "DOMContentLoaded", function(){
			document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
			_ready();
		}, false );

	// If IE event model is used
	} else if ( document.attachEvent ) {
		// ensure firing before onload,
		// maybe late but safe also for iframes
		document.attachEvent("onreadystatechange", function(){
			if ( document.readyState === "complete" ) {
				document.detachEvent( "onreadystatechange", arguments.callee );
				_ready();
			}
		});

		// If IE and not an iframe
		// continually check to see if the document is ready
		if ( document.documentElement.doScroll && window == window.top ) (function(){
			if (_isReady) return;

			try {
				// If IE is used, use the trick by Diego Perini
				// http://javascript.nwbox.com/IEContentLoaded/
				document.documentElement.doScroll("left");
			} catch( error ) {
				setTimeout( arguments.callee, 0 );
				return;
			}

			// and execute any waiting functions
			_ready();
		})();
	}
	
	var buildParams = function(prefix, obj, add) {
		var name;
		if(sThis.isArray(obj)) {
			sThis.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
					add(prefix, v);
				} else {
					buildParams(prefix + "[" + (sThis.isObject(v) ? i : "" ) + "]", v, traditional, add);
				}
			});
		} else if (sThis.isObject(obj) === "object" ) {
			for ( name in obj ) {
				buildParams( prefix + "[" + name + "]", obj[ name ], add );
			}
		} else {
			add( prefix, obj );
		}
	};

	this.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, value ) {
				value = sThis.isFunction( value ) ? value() : ( value == null ? "" : value );
				s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
			};
		if(sThis.isArray(a) || !sThis.isPlainObject(a)) {
			sThis.each(a, function() {
				add(this.name, this.value);
			});

		} else {
			for(prefix in a) {
				buildParams(prefix, a[prefix], traditional, add);
			}
		}
		return s.join("&").replace(r20, "+");
	};
	
	this.inArray = function(elem, arr, i) {
		var _len,
			_core_indexOf = Array.prototype.indexOf;
		if(arr) {
			if(_core_indexOf) {
				return _core_indexOf.call(arr, elem, i);
			}
			_len = arr.length;
			i = i ? i < 0 ? Math.max(0, _len + i) : i : 0;
			for(; i < _len; i++) {
				if(i in arr && arr[ i ] === elem) {
					return i;
				}
			}
		}
		return -1;
	};
	this.each = function(obj, callback, args) {
		var name,
			i = 0,
			length = obj.length,
			isObj = length === undefined || sThis.isFunction(obj);
		
		if(args) {
			if(isObj) {
				for(name in obj) {
					if(callback.apply(obj[ name ], args) === false) {
						break;
					}
				}
			} else {
				for(; i < length;) {
					if(callback.apply( obj[ i++ ], args ) === false) {
						break;
					}
				}
			}
		} else {
			if(isObj) {
				for(name in obj) {
					if(callback.call(obj[ name ], name, obj[ name ]) === false) {
						break;
					}
				}
			} else {
				for(; i < length;) {
					if(callback.call(obj[ i ], i, obj[ i++ ]) === false) {
						break;
					}
				}
			}
		}
		return obj;
	};
	
	this.postMessage = function( message, target_url, target ) {
		if ( !target_url ) { return; }
		message = typeof message === 'string' ? message : sThis.param( message );
		target = target || parent;
		if ( has_postMessage ) {
			target["postMessage"]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );
		} else if ( target_url ) {
			target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
		}
	};
	
	this.registerIFrame = function(iframe, url) {
		var _id;
		
		if(!iframe) {return;}
		iframe = $(iframe)[0];
		_id = iframe.id;
		if(!_id) {iframe.id = _id = "ifrm_" +  + Math.random().toString().replace(".", "");}
		
		var _s = url.indexOf("?");
		if(_s == -1) {
			url = url + "?ref=" + encodeURIComponent(location.href.replace(/#.*$/, '')) + "&refi=" + encodeURIComponent(_id);
		}
		else {
			url = url + "&ref=" + encodeURIComponent(location.href.replace(/#.*$/, ''));
			if(url.indexOf("refi=", _s) == -1) {
				url = url + "&refi=" + encodeURIComponent(_id);
			}
		}
		
		iframe.src = url;
	};

	this.createIFrameFromParent = function(parent, ignored) {
		var _url,
			_id,
			_iframe = null;
		
		if(!window["bstdURL"]) {return;}
		
		parent = $(parent);
		
		if(!parent) {
			return false;
		}
		_url = parent.attr("url") || null;
		
		if(_url) {
			_url = a = bstdURL.parseURL(_url);
			//url + (url.indexOf("?") == -1 ? "?" : "&") + "ref=" + encodeURIComponent(location.protocol + "//" + location.host + location.pathname) + "&refi=" + _id;
			_url.params["ref"] = encodeURIComponent(location.href.replace(/#.*$/, ''));
			if(_url.params["refi"]) {
				_id = _url.params["refi"];
			}
			else {
				_id = "ifrm_" +  + Math.random().toString().replace(".", "");
				_url.params["refi"] = _id;
			}
			_url.params = bstdURL.replaceParams(_url.params, location.params, ignored);
			_url = bstdURL.URLObject2String(_url);
		}
		
		_iframe = _createIFrame(parent, _url, null, _id);
		parent.append(_iframe);
		parent.id = parent.id + "_old";
		return _iframe;
	};
	
	var _createIFrame = function(parent, url, name, id) {
		return $('<iframe id="' + id + '" ' + (name ? 'name="' + name + '" ' : "") + 'frameborder="0" src="' + (url ? url : "javascript:void(0);") + '" />');
	};
	
	this.createIFrame = function(parent, url, name, id) {
		var _iframe = $("<iframe name=\"" + (name ? name : "ifrm_" +  + Math.random().toString().replace(".", "")) + "\" id=\"" + (id ? id : "ifrm_" +  + Math.random().toString().replace(".", "")) + "\">");
		$(parent || "body").append(_iframe);
		if(url) {sThis.registerIFrame(_iframe, url);}
	};
	
	this.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
		if ( has_postMessage ) {
			if ( callback ) {
				rm_callback && p_receiveMessage();
				rm_callback = function(e) {
					if(e.originalEvent) {
						e = e.originalEvent;
					}
					if ( ( typeof source_origin === 'string' && source_origin != "*" && e.origin !== source_origin )
							|| ( sThis.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
						return FALSE;
					}
					callback( e );
				};
			}
			if(window.addEventListener){
				window.addEventListener('message', rm_callback, false);
			} else {
				window.attachEvent('onmessage', rm_callback);
			}
		} else {
			interval_id && clearInterval( interval_id );
			interval_id = null;
			if ( callback ) {
				delay = typeof source_origin === 'number'
					? source_origin
							: typeof delay === 'number'
								? delay
										: 100;
				interval_id = setInterval(function(){
					var hash = document.location.hash,
						re = /^#?\d+&/;
					if ( hash !== last_hash && re.test( hash ) ) {
						last_hash = hash;
						callback({data: hash.replace(re, '')});
					}
				}, delay );
			}
		}
	};
	  
	this.receiveMessage(function(e){
		var _inData = e.data.split("&"),
			_outData = {},
			_iframe,
			_i,
			_itm;

		for(_i = 0; _i < _inData.length; _i ++) {
			_itm = _inData[_i].split("=");
			if(_itm[0]) {
				_outData[_itm[0]] = _itm[1];
			}
		}
		if(!isNaN(_outData["if_height"]) && _outData["if_height"] > 0) {
			if(_outData["refi"]) {
				var _iframeId = _outData["refi"];
				_iframe = document.getElementById(_iframeId);
				var _style = _iframe.style;
				if(parseInt(_outData["if_height"])) {
					_style.height = parseInt(_outData["if_height"]) + "px";
				}
				//_style.width = "1200px";
			}
			else {
				var _iframes = document.getElementsByTagName("iframe");
				if(_iframes.length) {
					var _iframeItem = _iframes.item(0);
					var _iframeId = _iframeItem.id;
					_iframe = document.getElementById(_iframeId);
					var _style = _iframe.style;
					if(parseInt(_outData["if_height"])) {
						_style.height = parseInt(_outData["if_height"]) + "px";
					}
				}
			}
		}
	}, '*' );
};
bstdFrames.onReady(function(){
	var _url;
	if(/\/used-cars\/search.tmex/.test(location.href) || /\/trade-in\/search.tmex/.test(location.href)) {
		/*
		if((_ele = $(".c-l-container-12[url]")).length && /trade\./.test(_ele.attr("url"))) {
			bstdFrames.createIFrameFromParent(".c-l-container-12[url]", true);
		}
		*/
		_url = $('section.c-l-container-12').attr("url") || "http://trade.lexus.ru/x-findcars";
		if(location.params && location.params.id){
			//_url += (_url.indexOf("?") == -1 ? "?" : "&") + "id=" + location.params.id;
			_url += "#ci=" + location.params.id;
		}
		
		bstdFrames.createIFrameFromParent($('section.c-l-container-12').attr("url", _url), true);
	}
});