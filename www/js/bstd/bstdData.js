var bstdData = new function() {
	var sThis = this;
	
	this.Validator = new function() {
		var sVThis = this,
		_DataValidateTypes = {};
		
		/**
		 * Валидатор регулярными выражениями
		 */
		_DataValidateTypes.regexp = function(data, regexp) {
			var _reg,
				_regExp,
				_i,
				_options = "",
				_opt;
			if(!regexp) {
				if(_SYS_DEBUG) {
					throw new Error("Argument regexp is error");
				}
				return false;
			}
			
			_reg = regexp.match(/\/?(.+?)\/(\w?)/);
			_opt = _reg[2].toLowerCase();
			
			for(_i = 0; _i < _opt.length; _i ++) {
				switch(_opt.charAt(_i)) {
					case "i":
						_options += "i";
						break;
					case "m":
						_options += "m";
						break;
					case "g":
						_options += "g";
						break;
				}
			}
			return ((new RegExp(_reg[1], _options)).test(data)) ? data : false;
		};
		_DataValidateTypes.reg = 
			_DataValidateTypes.rg = 
				_DataValidateTypes.r = _DataValidateTypes.regexp;
		
		this.AddValidator = function(names, func) {
			var _i;
			
			if(!names || typeof names != "string" && !(names instanceof Array) || typeof func != "function") {
				if(_SYS_DEBUG) {
					throw new Error("names or function is error");
				}
				return;
			}
			if(typeof names == "string") {
				_DataValidateTypes[names] = func;
			}
			else {
				for(_i = 0; _i < names.length; _i ++) {
					if(_SYS_DEBUG) {
						throw new Error("One value from array names is not string");
					}
					else continue;
					_DataValidateTypes[names[_i]] = func;
				}
			}
		};
		
		this.IsValidator = function(name) {
			if(typeof name != "string") {
				if(_SYS_DEBUG) {
					throw new Error("Argumrnt name is not string");
				}
				return false;
			}
			return !!(name in _DataValidateTypes);
		};
		
		this.Validate = function(validateType, value, data) {
			if(validateType in _DataValidateTypes) {
				return _DataValidateTypes[validateType](data, value);
			}
			else {
				if(_SYS_DEBUG) {
					throw new Error("validateType '" + validateType + "' is not found");
				}
			}
		};
	};
	
	this.CheckMin = function() {
		
	};
	
	this.CheckMax = function() {
		
	};
	
	this.Converter = new function() {
		var _DataConvertTypes = {};
		
		_DataConvertTypes.timestamp = function(data) {
			
		};
		
		_DataConvertTypes.base64 = function(data) {
			if(!uString || typeof uString != "object" || !uString.base64) {
				if(_SYS_DEBUG) {
					throw new Error("Not found library uString");
				}
				return false;
			}
			return uString.base64.encode(data);
		};
		
		_DataConvertTypes.test = function(data) {
			return data;
		};
		
		this.AddConverter = function(names, func) {
			var _i;
			
			if(!names || typeof names != "string" && !(names instanceof Array) || typeof func != "function") {
				if(_SYS_DEBUG) {
					throw new Error("names or function is error");
				}
				return;
			}
			if(typeof names == "string") {
				_DataConvertTypes[names] = func;
			}
			else {
				for(_i = 0; _i < names.length; _i ++) {
					if(_SYS_DEBUG) {
						throw new Error("One value from array names is not string");
					}
					else continue;
					_DataConvertTypes[names[_i]] = func;
				}
			}
		};

		this.IsConverter = function(name) {
			if(typeof name != "string") {
				if(_SYS_DEBUG) {
					throw new Error("Argumrnt name is not string");
				}
				return false;
			}
			return !!(name in _DataConvertTypes);
		};
		
		this.Convert = function(data, type) {
			if(typeof type != "string" && data === undefined) {
				if(_SYS_DEBUG) {
					throw new Error("Argument type not string or argument data is undefined");
				}
				return false;
			}
			type = type.toLowerCase();
			if(_DataConvertTypes[type] === undefined) {
				if(_SYS_DEBUG) {
					throw new Error("Converter with type '" + type + "' is not found");
				}
				return false;
			}
			return  _DataConvertTypes[type](data);
		};
	};
};