//http://contacts.toyota.ru/z/findPage?eservice={eservice}&corp={corp}&cityId={cityId}&summer={summer}&tsm={tsm}&kuzov={kuzov}&autumn={autumn}&formula_toyota={formula_toyota}&spring_service={spring_service}&tires={tires}&winter={winter}&directreception={directreception}&tested={tested}&tradein={tradein}&refi=findDealers&ref=
var _dealers,
	_cityFilter,
	_predefinedCity;

var getFilteredDealers = function() {
	var _dealers = [];
	
	$("#dealersTable > tbody td").each(function(){
		var _id = $(this).attr("data-dealer-id");
		if(_id) {
			_dealers.push(_id);
		}
	});
	return _dealers;
};

var _applyFilters = function() {
	var _visibleTD = [],
		_tmpVisibleTD = [],
		_first = true,
		j,
		i,
		_city = $("#city"),
		
		_visibleCities = [],
		_tmpCities = [];

	_visibleTD = _dealers;
	
	if(_cityFilter != 0) {
		for(i = 0; i < _visibleTD.length; i ++) {
			//$("option[value=" + $(_visibleTD[i]).attr("data-city-id") + "]", _city).show();
			if($(_visibleTD[i]).attr("data-city-id") == _cityFilter) {
				_tmpVisibleTD.push(_visibleTD[i]);
			}
		};
		_visibleTD = _tmpVisibleTD;
		_tmpVisibleTD = [];
	}
	else {
	/*
		for(i = 0; i < _visibleTD.length; i ++) {
			$("option[value=" + $(_visibleTD[i]).attr("data-city-id") + "]", _city).show();
		};
	*/
	}
	_changeTable(_visibleTD);
	_resizeFrame();
};

var _changeTable = function(visibilityDealers) {
	var _tbody = $("#dealersTable > tbody").empty(),
		_tr,
		_tdCount = 2,
		i,
		_curr;

	if(visibilityDealers) {
		$("#dealersTable").hide();
		for(i = 0; i < visibilityDealers.length; i ++) {
			_curr = $(visibilityDealers[i]);
			if(_curr.attr("data-type") == "city") {
				if(_tdCount == 1) {
					_tr.append($("<td>").html("&nbsp;"));
					_tdCount = 2;
				}
				_tbody.append($("<tr>").append(_curr));
				continue;
			}
			if(_tdCount == 2) {
				_tbody.append(_tr = $("<tr>"));
				_tdCount = 0;
			}
			_tr.append(_curr);
			_tdCount ++;
		};
		if(_tdCount == 1) {
			_tr.append($("<td>").html("&nbsp;"));
		}
		$("#dealersTable").show();
	}
	_resizeFrame();
};

var _resizeFrame = function() {
	if(window["bstdFrames"] && bstdFrames.setHeight && window != top) {
		bstdFrames.setHeight(50);
	}
};

$(document).ready(function(){
	var _ps,
		_i;

	_dealers = $("#dealersTable > tbody td");
	
	$("#filters tr").attr("data-filter-predefined", 0);
	
	$("#city").bind("change", function() {
		_cityFilter = $("#city").val();
		_applyFilters();
	});
	$("body").on("click", "a[data-map]", function() {
		if($(this).attr("data-map")) {
			eval('opened=window.open("","none","top=100, left=100, width=800, height=800, resizable=0","replace=yes")');
			opened.document.open();
			opened.document.write('<body leftmargin=0 topmargin=0><img width="800" src="' + $(this).attr("data-map") + '"></body>');
			opened.document.close(); 
		}
	});
	if(location.params && location.params.cityId && !(location.params.cityId instanceof Array)) {
		_ps = decodeURIComponent(location.params.cityId);
		if(_ps != "{cityId}") {
			$("#city option[value=" + location.params.cityId + "]").attr("selected", true);
			_cityFilter = _predefinedCity = location.params.cityId;
		}
	}
	if(_predefinedCity) {
		_applyFilters();
	}
	setInterval(_resizeFrame, 1000);
});