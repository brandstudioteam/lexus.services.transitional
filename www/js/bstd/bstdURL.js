var bstdURL = window["bstdURL"] = new function() {
	var sThis = this;
	
	var _prepareParams = function(key, val, obj) {
		var _s = key.indexOf("["),
			_e = key.lastIndexOf("]"),
			_key,
			_subKey,
			_obj = obj || {};
		
		if(_s != -1) {
			_key = key.substr(0, _s);
			if(_s + 1 < _e) {
				_subKey = key.substring(_s + 1, _e);
				if(_obj[_key] === undefined) {
					_obj[_key] = {};
				}
				_prepareParams(_subKey, val, _obj[_key]);
			}
			else {
				if(_obj[_key] === undefined) {
					_obj[_key] = [];
				}
				_obj[_key].push(val);
			}
        }
		else {
			_obj[key] = val;
		}
		return _obj;
	};
	
	this.parseURL = function(url) {
		var _url = /(?:((?:http[s]?|file):)\/{2,3})?((?:[\d\w_\-а-яё]+.)[\d\w_\-а-яё.]+)?([^?]*)(?:\?([^#]*))?(?:#(.*))?/.exec(url),
			_params,
			_returned = {},
			_i,
			_tmp;
			
			_returned.protocol = _url[1] || null;
			_returned.host = _url[2] || null;
			_returned.path = _url[3] || null;
			_returned.paramsString = _url[4] || null;
			_returned.hash = _url[5] || null;
			_returned.params = {};

			if(_returned.paramsString !== null) {
				_params = _returned.paramsString.split("&");
				for(_i = 0; _i < _params.length; _i ++) {
					if(!_params[_i]) {
						continue;
					}
					_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
					if(_tmp[2] === undefined) {
		                _tmp[2] = null;
		            }
					_prepareParams(_tmp[1], _tmp[2], _returned.params);
				}
			}
		return _returned;
	};
	
	this.replaceParams = function(params1, params2, ignored, added) {
		var _i,
			_returned = {};
		
		for(_i in params1) {
			if(params2[_i] !== undefined) {
				_returned[_i] = params2[_i];
			}
			else {
				if(ignored) {
					if("{" + _i + "}" != params1[_i]) {
						_returned[_i] = params1[_i];
					}
				}
				else {
					_returned[_i] = params1[_i];
				}
			}
		}
		if(added) {
			for(_i in params2) {
				if(_returned[_i] === undefined) {
					_returned[_i] = params2[_i];
				}
			}
		}
		return _returned;
	};
	
	
	var _repeat = function(string, count) {
		var _i,
			_str = "";
		count = Math.abs(parseInt(count));
		if(isNaN(count)) {
			return "";
		}
		for(_i = 0; _i < count; _i ++) {
			_str += string;
		}
		
		return _str;
	};
	
	var _serializeParamsExec = function(values, params/*&$Array*/, key, level)
	{
		var _key,
			_val;
		
		if(level === undefined) {
			level = 0;
		}
		
		if(typeof values == "object") {
			if(values instanceof Array) {
				for(_key = 0; _key < values.length; _key++)
				{
					_val = values[_key];
					if(typeof _val == "object")
					{
						level ++;
						_serializeParamsExec(_val, params, key ? key + "[" + _key : _key, level);
						level --;
					}
					else
						params.push((key ? key + "[" + _key : _key) + _repeat("]", level) + "=" + _val);
				}
			}
			else {
				for(_key in values)
				{
					_val = values[_key];
					if(typeof _val == "object")
					{
						level ++;
						_serializeParamsExec(_val, params, key ? key + "[" + _key : _key, level);
						level --;
					}
					else {
						params.push((key ? key + "[" + _key : _key) + _repeat("]", level) + "=" + _val);
					}
				}
			}
		}
		return params;
	};
	
	this.serializeParams = function(paramsAsObject) {
		var _arr = [];
		_serializeParamsExec(paramsAsObject, _arr);
		return _arr.join("&");
	};
	
	this.URLObject2String = function(url) {
		var _srParams;
		if(url && typeof url == "object") {
			if(url && url.params) {
				_srParams = sThis.serializeParams(url.params);
			}
			return (url.protocol ? url.protocol : location.protocol) + "//" + (url.host ? url.host : "") + (url.path ? url.path : "") + (_srParams ? "?" + _srParams : "") + (url.hash ? "#" + url.hash : "");
		}
		else return url;
	};
	
	if(!location.params) {
		var _url = this.parseURL(location.href);
		location.stringParams = _url.paramsString;
		location.params = _url.params;
	}
};