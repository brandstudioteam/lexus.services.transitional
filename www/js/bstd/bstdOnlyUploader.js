var bstdOnlyUploader = function(title, name, callback, URL, Controller, Action, Params, inPopup) {
	var _e,
		_popup;
	
	if(!name) {
		return false;
	}
	
	_e = $('<input class="bstdOnlyUploader" type="file" name="' + name + '">');
	
	if(inPopup) {
		_popup = new bstdPopup(title, _e, 1, {button: "Закрыть"}, true, true);
		bstdSystem.Request.RegisterFileUploader(_e, callback, URL, Controller, Action, Params, true, function(_popup) {
			return function() {
				_popup.close();
			};
		}(_popup));
		_popup.open();
	}
};