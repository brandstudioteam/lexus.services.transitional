if(!location["params"]) {
	var locationParams = function(){var _params=location.href.split("?"),_i,_tmp,_s,_str;location["params"]={};if(_params.length > 1){location["stringParams"]=_str=decodeURIComponent(_params[1].split("#")[0]);_params=location["stringParams"].split("&");for(_i=0;_i < _params.length;_i++){_tmp=new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);if((_s=_tmp[1].indexOf("[]"))!=-1){_tmp[1]=_tmp[1].substr(0, _s);}if(_tmp[2]===undefined){_tmp[2]=null;}if(_tmp[1] in location["params"]){if(location["params"][_tmp[1]] instanceof Array){location["params"][_tmp[1]].push(_tmp[2]);}else location["params"][_tmp[1]]=[location["params"][_tmp[1]], _tmp[2]];}else location["params"][_tmp[1]]=_tmp[2];}}return location["params"];};
	locationParams();
};

var bstdFrames = new function() {
	var sThis = this;
	var r20 = /%20/g,
		rbracket = /\[\]$/,
		rCRLF = /\r?\n/g,
		rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
		rselectTextarea = /^(?:select|textarea)/i,
		core_push = Array.prototype.push,
		core_slice = Array.prototype.slice,
		core_indexOf = Array.prototype.indexOf,
		core_toString = Object.prototype.toString,
		core_hasOwn = Object.prototype.hasOwnProperty,
		core_trim = String.prototype.trim,
		
		interval_id,
	    last_hash,
	    cache_bust = 1,
	    rm_callback,
	    FALSE = !1,
	    p_receiveMessage,
	    has_postMessage;
	
	this.browsers = new function(){
		var _tmp;
		
		this.ie6 = function() {
		  var browser = navigator.appName;
		  if (browser == "Microsoft Internet Explorer"){
		    var b_version = navigator.appVersion;
		    var re = /\MSIE\s+(\d\.\d\b)/;
		    var res = b_version.match(re);
		    if (res && res[1] && res[1] <= 6){
		      return true;
		    }
		  }
		  return false;
		}();
		this.ie7 = function() {
			  var browser = navigator.appName;
			  if (browser == "Microsoft Internet Explorer"){
			    var b_version = navigator.appVersion;
			    var re = /\MSIE\s+(\d\.\d\b)/;
			    var res = b_version.match(re);
			    if (res && res[1] && res[1] <= 7){
			      return true;
			    }
			  }
			  return false;
			}();
		
		_tmp = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
		this.opera = !!_tmp;
		this.operaUnder97 = !!(_tmp && parseFloat(_tmp[1]) < 9.7);
		this.chrome = !!/Chrome\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
		this.webkit = !!/WebKit\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
	};
	has_postMessage = window["postMessage"] && !sThis.browsers.operaUnder97;
	this.isFunction = function(val) {return typeof val == "function";};
	this.isObject = function() {return typeof c == "object";};
	this.isArray = function(val) {return val instanceof Array;};
	this.isWindow = function( obj ) {return obj != null && obj == obj.window;};
	this.isPlainObject = function( obj ) {
		var key;
		if (!obj || typeof obj !== "object" || obj.nodeType || sThis.isWindow( obj ) ) {
			return false;
		}
		try {
			if (obj.constructor &&
				!core_hasOwn.call(obj, "constructor") &&
				!core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
					return false;
			}
		} catch (e) {
			return false;
		}
		for(key in obj) {}
		return key === undefined || core_hasOwn.call(obj, key);
	};
	this.isEmptyObject = function(obj) {
		var name;
		for (name in obj) {
			return false;
		}
		return true;
	};

	var buildParams = function(prefix, obj, add) {
		var name;
		if(sThis.isArray(obj)) {
			sThis.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
					add(prefix, v);
				} else {
					buildParams(prefix + "[" + (sThis.isObject(v) ? i : "" ) + "]", v, traditional, add);
				}
			});
		} else if (sThis.isObject(obj) === "object" ) {
			for ( name in obj ) {
				buildParams( prefix + "[" + name + "]", obj[ name ], add );
			}
		} else {
			add( prefix, obj );
		}
	};

	this.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, value ) {
				value = sThis.isFunction( value ) ? value() : ( value == null ? "" : value );
				s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
			};
		if(sThis.isArray(a) || !sThis.isPlainObject(a)) {
			sThis.each(a, function() {
				add(this.name, this.value);
			});

		} else {
			for(prefix in a) {
				buildParams(prefix, a[prefix], traditional, add);
			}
		}
		return s.join("&").replace(r20, "+");
	};
	
	this.inArray = function(elem, arr, i) {
		var _len,
			_core_indexOf = Array.prototype.indexOf;
		if(arr) {
			if(_core_indexOf) {
				return _core_indexOf.call(arr, elem, i);
			}
			_len = arr.length;
			i = i ? i < 0 ? Math.max(0, _len + i) : i : 0;
			for(; i < _len; i++) {
				if(i in arr && arr[ i ] === elem) {
					return i;
				}
			}
		}
		return -1;
	};
	this.each = function(obj, callback, args) {
		var name,
			i = 0,
			length = obj.length,
			isObj = length === undefined || sThis.isFunction(obj);
		
		if(args) {
			if(isObj) {
				for(name in obj) {
					if(callback.apply(obj[ name ], args) === false) {
						break;
					}
				}
			} else {
				for(; i < length;) {
					if(callback.apply( obj[ i++ ], args ) === false) {
						break;
					}
				}
			}
		} else {
			if(isObj) {
				for(name in obj) {
					if(callback.call(obj[ name ], name, obj[ name ]) === false) {
						break;
					}
				}
			} else {
				for(; i < length;) {
					if(callback.call(obj[ i ], i, obj[ i++ ]) === false) {
						break;
					}
				}
			}
		}
		return obj;
	};
	
	this.postMessage = function( message, target_url, target ) {
		if ( !target_url ) { return; }
		message = typeof message === 'string' ? message : sThis.param( message );
		target = target || parent;
		if ( has_postMessage ) {
			target["postMessage"]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );
		} else if ( target_url ) {
			target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
		}
	};
	this.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
		if ( has_postMessage ) {
			if ( callback ) {
				rm_callback && p_receiveMessage();
				rm_callback = function(e) {
					if(e.originalEvent) {
						e = e.originalEvent;
					}
					if ( ( typeof source_origin === 'string' && source_origin != "*" && e.origin !== source_origin )
							|| ( sThis.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
						return FALSE;
					}
					callback( e );
				};
			}
			if(window.addEventListener){
				window.addEventListener('message', rm_callback, false);
			} else {
				window.attachEvent('onmessage', rm_callback);
			}
		} else {
			interval_id && clearInterval( interval_id );
			interval_id = null;
			if ( callback ) {
				delay = typeof source_origin === 'number'
					? source_origin
							: typeof delay === 'number'
								? delay
										: 100;
				interval_id = setInterval(function(){
					var hash = document.location.hash,
						re = /^#?\d+&/;
					if ( hash !== last_hash && re.test( hash ) ) {
						last_hash = hash;
						callback({ data: hash.replace( re, '' ) });
					}
				}, delay );
			}
		}
	};
	  
	this.receiveMessage(function(e){
		var _inData = e.data.split("&"),
			_outData = {},
			_iframe,
			_i,
			_itm;

		for(_i = 0; _i < _inData.length; _i ++) {
			_itm = _inData[_i].split("=");
			if(_itm[0]) {
				_outData[_itm[0]] = _itm[1];
			}
		}
		if(!isNaN(_outData["if_height"]) && _outData["if_height"] > 0) {
			if(_outData["refi"]) {
				var _iframeId = _outData["refi"];
				_iframe = document.getElementById(_iframeId);
				var _style = _iframe.style;
				if(parseInt(_outData["if_height"])) {
					_style.height = parseInt(_outData["if_height"]) + "px";
				}
			}
			else {
				var _iframes = document.getElementsByTagName("iframe");
				if(_iframes.length) {
					var _iframeItem = _iframes.item(0);
					var _iframeId = _iframeItem.id;
					_iframe = document.getElementById(_iframeId);
					var _style = _iframe.style;
					if(parseInt(_outData["if_height"])) {
						_style.height = parseInt(_outData["if_height"]) + "px";
					}
				}
			}
		}
	}, '*' );
	
	if($("iframe[src^='http://trade.toyota.ru']").length) {
		if(location.param && location.param.id) {
			var _tmpUrl = $("iframe[src^='http://trade.toyota.ru']").attr("src").split("?");
			$("iframe[src^='http://trade.toyota.ru']").attr("src", _tmpUrl[0] + location.param.id + "?" + _tmpUrl[1]);
		}
	}
};