/**
 * Свойства форм:
 *
 * bstd_not_send_not_changed 			- Не отправлять форму, если данные не были изменены
 * bstd_send_only_changed 				- Отправлять только измененные данные
 * bstd_not_alert_is_change 			- Не предупреждать о не сохраненных измененных данных при переходе на другую страницу или ликвидации формы
 * bstd_validate_only_before_send 		- Валидировать данные только перед отправкой формы
 * bstd_asinch							- Флаг асинхронной отправки формы
 * bstd_error_type						- Способ отображения ошибок: tag (1) - в тегах, с идентификатором <element_id> + "_error", tips (2) - всплывающими подсказаками, both (3) - использовать оба списоба
 * 
 * 
 * bstd_name							- Имя поля формы для DOM-элементов, не являющихся элементами формы, например, div, span и т.д.
 * bstd_value							- Значение поля формы для DOM-элементов, не являющихся элементами формы, например, div, span и т.д.
 * bstd_type							- Тип поля формы для DOM-элементов, не являющихся элементами формы, например, div, span и т.д. или для неспецифического использования элементов формы (например, для органиации работы каптчи)
 * 
 * bstd_valide_type						- Тип валидатора
 * bstd_valide_values 					- Значения валидатора
 * bstd_valide_max						- Максимальное значение для числовых полей, полей даты и/или времени; максимальное количество символов для текстовых
 * bstd_valide_min						- Минимальное значение для числовых полей, полей даты и/или времени; минимальное количество символов для текстовых
 * bstd_validate_error					- Сообщение об ошибке валидатора
 * bstd_validate_min_error				- Сообщение об ошибке валидатора для минимального значения
 * bstd_validate_max_error				- Сообщение об ошибке валидатора для максимального значения
 * bstd_convert							- Конвертирует значения полей в специфический формат или представление.
 * bstd_control_value					- Контролирует введенные символы. Применимо для полей со свободным вводом.
 * 
 * bstd_autoupload						- Флаг асинхронной отправки файлов
 * bstd_multiselect						- Флаг множественной загрузки файлов
 * bstd_limit							- Ограничения количества загружаемых файлов
 * bstd_max								- Ограничение размера одного загружаемого файла
 * bstd_one_max							- Синоним bstd-max
 * bstd_all_max							- Ограничение размера одного загружаемого файла
 * bstd_types							- Типы загружаемых файлов
 * 
 * 
 * Формат входяших (с сервера) данных:
 * error - номер ошибки выполнения. 0 - ошибок нет
 * errors - ассоциативный массив (хэш) ошибок для формы в формате:
 * 		{
 * 			<field_name>: "error_message"
 * 		}
 * error_message - общее сообщение об ошибках (для всплывающего окна)
 * message - общее сообщение (для всплывающего окна)
 * result - данные. Единичное значение, массив, объект
 * data - возвращенные данные. При возникновении ошибки в этом объекте могут вернуться отправленные данные без обработки.
 * 
 */


var _SYS_DEBUG = true;

$.ajaxSetup({
		headers: {
			"X-Requested-With": "XMLHttpRequest"
		}
	});

/**
 * Объект системных сообщений
 */
var bstdSysMessages = {};
bstdSysMessages.errors = {};
bstdSysMessages.errors.forms = {};
bstdSysMessages.errors.forms.NotSendForNotChangedData = "Форма не может быть отправлена, поскольку данные не изменились";
bstdSysMessages.errors.forms.reqiredSelect = "Необходимо выбрать элемент из списка";
bstdSysMessages.errors.forms.reqiredText = "Необходимо ввести данные";
bstdSysMessages.errors.forms.reqiredFile = "Необходимо выбрать файл для загрузки";
bstdSysMessages.errors.forms.NotValideText = "Введены некорректные символы или значение";
bstdSysMessages.errors.forms.capthaNotValide = "Не верный код подтверждения";
bstdSysMessages.errors.forms.capthaRequired = "Необходимо ввести код подтверждения";
bstdSysMessages.errors.forms.custom = {}; //Индивидуальные сообщения об ошибках. "mnemocode" : "Error message"


bstdTypes = {};


var bstdFormsSets = {};
bstdFormsSets.default = {
	isSendOnlyChanged: true,
	isNotSendNotChanged: false,
	method: "post",
	enctype: "auto",
	asinch: true,
	errorType: 2 //использовать подсказки для отображения ошибок
};


/**
 * Объект работы с формами
 */
var bstdForms = new function() {
	var _forms = [],
		_formsIdx = {},
		sThis = this,
		_errors = {},
		_values = {},
		_currForm,
		_currFormProp,
		_currFormName,
		_rnd = Math.random(),
		_advancedElementsTypes = {};
	
	var bstdCurrentForm = function() {
		this.elements = {};
		this.changedElements = {};
		this.isChange = false;
		this.uploaders = {};
		this.radio = {};
		this.filesUploaded = {};
		this.isSendOnlyChanged = false;
		this.isNotSendNotChanged = false;
		this.action = null;
		this.enctype = null;
		this.method = null;
		this.encript = null;
		this.target = null;
		this.asinch = false;
		this.errorType;
	};
	
	this.ENCODING = {};
	this.ENCODING.MULTIPART = "multipart/form-data"; 
	this.ENCODING.URL = "application/x-www-form-urlencoded";
	this.ENCODING.TEXT = "text/plain";
	
	this.ENCODING.DEFAULT = null;
	
	this.ENCTYPE = {};
	this.ENCTYPE.MULTIPART = "multipart/form-data"; 
	this.ENCTYPE.URL = "application/x-www-form-urlencoded";
	this.ENCTYPE.TEXT = "text/plain";
	
	this.ENCTYPE = this.ENCTYPE.URL;
	
	this.METHODS = {};
	this.METHODS.POST = "post";
	this.METHODS.GET = "get";
	
	this.METHODS.DEDAULT = this.METHODS.POST;
	
	
	
	
	
	/**
	 * Устанавливает флаги "Данные изменены" у элемента и его формы
	 */
	var _changeValues = function(ele, form, type) {
		var _name = ele.name;
		
		sThis.setCurrent(form);
		_currFormProp.isChange = true;
		if(ele.type.toLowerCase() == "radio") {
			_currFormProp.radio[_name].isChange = true;
			_currFormProp.radio[_name].currentValue = ele.value;
			_currFormProp.radio[_name].selected = ele;
		}
		else {
			ele.isChange = true;
			_currFormProp.changedElements[_name] = ele;
		}
	};
	
	var _result = function(response, form) {
		if(typeof response == "string") {
			response = eval('(' + response + ')');
		}
		if(response && typeof response == "object") {
			//Проверяем общее сообщение
			if(response.message) {
				bstdAlert(response.message);
			}
			if(!response.error && response.result) {
				//_result(response);
				$(form).trigger("succesfullSubmiting", {form:form, result:response.result});
			} else {
				if(response.errors) {
					sThis.setErrors(form, response.errors);
				}
				//Проверяем общее сообщение об ошибке
				if(response.error_message) {
					bstdAlert(response.error_message);
				}
			}
		}
	};
	
	var _blockForm = function(form) {
		//2 способа блокировки: маскирование и запрет изменения полей
	};
	
	
	this.registerElementsType = function(type, func) {
		if(typeof type == "string" && typeof func == "function")  {
			_advancedElementsTypes[type] = func;
		}
		else {
			if(_SYS_DEBUG) {
				throw new Error("Arguments error");
			}
			return false;
		}
	};
	
	
	
	this.action = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.action;
	};
	
	this.enctype = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.enctype;
	};

	this.method = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.method;
	};

	this.encript = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.encript;
	};

	this.target = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.target;
	};
	
	this.name = function() {
		return _currFormName;
	};
	
	/**
	 * Возвращает флаг "Отправлять только измененные данные"
	 */
	this.isSendOnlyChanged = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.isSendOnlyChanged;
	};
	
	/**
	 * Возвращает флаг "Не отправлять форму, если данные не были изменены"
	 */
	this.isNotSendNotChanged = function() {
		if(!_currFormProp) {
			if(_SYS_DEBUG) {
				throw new Error("Current form not found");
			}
			return false;
		}
		return _currFormProp.isNotSendNotChanged;
	};
	
	var _changeTextValidate = function(form, ele) {
		var _value = bstdData.Validator.Validate($(ele).attr("bstd_valide_type"), $(ele).attr("bstd_valide_values"), ele.value),
		_validateError;
		if(_value === false) {
			sThis.setErrors(form, (_validateError = $(ele).attr("bstd_validate_error")) ? _validateError : "NotValideText", ele);
			$(ele).focus();
		}
		else {
			if(ele.error) {
				sThis.clearErrors(form, ele);
			}
		}
	};
	
	/**
	 * Производит инициализацию формы
	 */
	this.formInit = function(form) {
		var _eles,
			_i,
			_name,
			_type,
			_ele,
			_currentForm,
			_validator;
		
		form = sThis.getForm(form);
		if(!form)			
			throw new Error("Object is not a form of");
		
		form.bstdCurrentForm = new bstdCurrentForm;
		
		_currentForm = form.bstdCurrentForm;
		
		_currentForm.isSendOnlyChanged = window["bstdFormsSets"] && window["bstdFormsSets"]["isSendOnlyChanged"] !== undefined ? window["bstdFormsSets"]["isSendOnlyChanged"] : $(form).attr("bstd_send_only_changed") == 1;
		if(_currentForm.isSendOnlyChanged === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.isSendOnlyChanged !== undefined) {
			_currentForm.isSendOnlyChanged = bstdFormsSets.default.isSendOnlyChanged;
		}
		
		_currentForm.isNotSendNotChanged = window["bstdFormsSets"] && window["bstdFormsSets"]["isNotSendNotChanged"] !== undefined ? window["bstdFormsSets"]["isNotSendNotChanged"] : $(form).attr("bstd_form bstd_not_send_not_changed");
		if(_currentForm.isNotSendNotChanged === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.isNotSendNotChanged !== undefined) {
			_currentForm.isNotSendNotChanged = bstdFormsSets.default.isNotSendNotChanged;
		}
		
		_currentForm.action = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["action"] !== undefined ?
				window["bstdFormsSets"][_currFormName]["action"] : $(form).attr("action");
		if(_currentForm.action === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.action !== undefined) {
			_currentForm.action = bstdFormsSets.default.action;
		}
		
		_currentForm.enctype = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["enctype"] !== undefined ? window["bstdFormsSets"][_currFormName]["enctype"] : $(form).attr("enctype");
		if(_currentForm.enctype === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.enctype !== undefined) {
			_currentForm.enctype = bstdFormsSets.default.enctype;
		}
		
		_currentForm.method = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["method"] !== undefined ? window["bstdFormsSets"][_currFormName]["method"] : $(form).attr("method");
		if(_currentForm.enctype === undefined) {
			if(window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.method !== undefined) {
				_currentForm.method = bstdFormsSets.default.method;
			}
			else {
				_currentForm.method = sThis.METHODS.DEDAULT
			}
		}
		
		_currentForm.encript = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["encript"] !== undefined ? window["bstdFormsSets"][_currFormName]["encript"] : $(form).attr("encript");
		if(_currentForm.encript === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.encript !== undefined) {
			_currentForm.encript = bstdFormsSets.default.encript;
		}
		
		_currentForm.target = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["target"] !== undefined ? window["bstdFormsSets"][_currFormName]["target"] : $(form).attr("target");
		if(_currentForm.target === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.target !== undefined) {
			_currentForm.target = bstdFormsSets.default.target;
		}
		
		_currentForm.asinch = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["asinch"] !== undefined ? window["bstdFormsSets"][_currFormName]["asinch"] : $(form).attr("bstd_asinch");
		if(_currentForm.asinch === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.asinch !== undefined) {
			_currentForm.asinch = bstdFormsSets.default.asinch;
		}
		
		_currentForm.errorType = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["errorType"] !== undefined  ? window["bstdFormsSets"][_currFormName]["errorType"] :
			$(form).attr("bstd_error_type");
		if(_currentForm.errorType === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.errorType !== undefined) {
			_currentForm.errorType = bstdFormsSets.default.errorType;
		}
		
		sThis.setCurrent(form);
		
		$(form).bind("submit", function(sForm) {
			return function(e) {return sThis.submit(sForm, e);};
		}(form));
		
		$(form).bind("reset", function(sForm) {
			return function() {return sThis.reset(sForm);};
		}(form));
		
		_eles = form.elements;
		
		$("[bstd-type]").each(function() {
			var _type = $(this).attr("bstd-type");
			if(bstdTypes[_type]) {
				this.bstdElement = new bstdTypes[_type](this);
				this.bstdRequired = $(this).attr("required") ? true : false;
				this.isChange = false;
			}
		});
		
		for(_i = 0; _i < _eles.length; _i ++) {
			_ele = _eles[_i];
			_name = _ele.name;
			_type = _ele.type;
			//button | checkbox | file | hidden | image | password | radio | reset | select-multiple | select-one | submit | text | textarea
			if($(_ele).attr("bstd-form-ignore") !== null)
				continue;
			switch(_type) {
				case "hidden":
					continue;
				case "select-multiple":
				case "select-one":
				case "checkbox":
				case "password":
				case "textarea":
				case "text":
					_ele.isChange = false;
					_ele.bstdOldValue = $(_ele).val();
					
					//Устанавливаем валидацию
					_validator = $(_ele).attr("bstd_valide_type");
					if(_validator) {
						$(_ele).bind("change", function(ele, sForm) {
							return function() {
								_changeTextValidate(sForm, ele);
								_changeValues(ele, sForm);};
						}(_ele, form));
						continue
					}
					break;
				case "radio":
					if(!_currentForm.radio[_name]) {
						_currentForm.radio[_name] = {
								isChange: false,
								currentValue: undefined,
								oldValue: undefined,
								required: false,
								selected: null
							};
					}
					if(_ele.checked) {
						_currentForm.radio[_name].oldValue = _ele.value;
						_currFormProp.radio[_name].selected = _ele;
					}
					if($(_ele).attr("required") !== undefined) {
						_currentForm.radio[_name].required = true;
					}
					break;
				case "file":
					if(!_currentForm.uploaders[_name]) {
						_currentForm.uploaders[_name] = {
								isChange: false,
								required: false,
								files: null
						};
					}
					if($(_ele).attr("required") !== undefined) {
						_currentForm.uploaders[_name].required = true;
					}
					break;
				default:
					continue;
			}
			
			//_currentForm.elements[_name] = _ele;
			if(_type != "hidden") {
				$(_ele).bind("change", function(ele, sForm) {
					return function() {
						_changeValues(ele, sForm);};
				}(_ele, form));
			}
		}
		
		if(!form.id) {
			form.id = "auto_frm_id_" + _rnd;
		}
		if(!form.name) {
			form.name = "auto_frm_name_" + _rnd;
		}
		_formsIdx[form.id] = _forms.length;
		_forms.push(form);
		_currentForm.isChange = false;
	};
	
	this.setCurrent = function(form) {
		_currForm = sThis.getForm(form);
		if(!_currForm) return false;
		_currFormName = $(form).attr("name");
		_currFormProp = _currForm.bstdCurrentForm;
	};

	/**
	 * Выполняет асинхронную передачу данных
	 */
	this.submit = function(form, e) {
		var _validateResult,
			_data = {};
		if(e) {
			e.preventDefault();
		}
		if(!(form = sThis.getForm(form))) {
			throw new Error("Object is not a form of");
		}
		if((sThis.isSendOnlyChanged(form) || sThis.isNotSendNotChanged(form)) && !_currFormProp.isChange) {
			alert(bstdSysMessages.errors.forms.NotSendForNotChangedData);
		}
		else {
			_validateResult = sThis.Valdate(form);
			sThis.clearErrors(form);
			if(_validateResult.isError) {
				sThis.setErrors(form, _validateResult.errors);
			}
			else {
				_blockForm(form);
				
				if(_validateResult.a) {
					_data["a"] = _validateResult.a;
					delete(_validateResult.a);
				}
				if(_validateResult.b) {
					_data["b"] = _validateResult.b;
					delete(_validateResult.b);
				}
				_data["d"] = _validateResult;
				
				form.data = _validateResult;
				$.ajax({
					url: 		_currFormProp.action,
					type:		_currFormProp.method,
					data:		{prn: $.toJSON(_data)},
					dataType:	'json',
					complete:	function(_sendingForm) {
						return function(response) {
							_result(response, _sendingForm);
						}
					}(form)
				});
			}
		}
		
		return false;
	};
	
	this.reset = function(form) {
		
	};
	
	
	/**
	 * Возвращает DOM-объект формы
	 */
	this.getForm = function(form, ignoreException) {
		if(typeof form == "object"){
			if(!form.tagName || form.tagName.toLowerCase() != "form") {
				form = false;
			}
		}
		else {
			form = $(form)[0];
		}
		if(!form) {
			if(!ignoreException)
				throw new Error("Object is not a form of");
		}
		return form;
	};
	
	this.getAllData = function(form) {
		if(!(form = sThis.getForm(form)))			
			throw new Error("Object is not a form of");
	};
	
	/**
	 * Проверяет введенные данные перед отправкой формы
	 */
	this.Valdate = function(form) {
		var _i,
			_ele,
			_eles,
			_check,
			_value,
			_data = {},
			_j,
			_tagName,
			_type,
			_errors = {},
			_isError,
			_isOnlyChanged,
			_oldValue,
			_radio = {},
			_err,
			_validator,
			_validateError,
			_bstdType;

		var setError = function(elementName, errorType, errorMessage) {
			_isError = true;
			_errors[elementName] = errorMessage ? errorMessage : bstdSysMessages.errors.forms[errorType];
		};
			
		if(!(form = sThis.getForm(form)))			
			throw new Error("Object is not a form of");
		_isOnlyChanged = sThis.isSendOnlyChanged(form);
		
		$("[bstd-type]").each(function() {
			if(_isOnlyChanged && this.isChange) {
				_data["d"][this.bstdElement.name()] = this.bstdElement.val();
			}
		});
		
		_eles = form.elements;
		for(_i = 0; _i < _eles.length; _i ++) {
			_ele = _eles[_i];
			if($(_ele).attr("bstd-form-ignore") !== null)
				continue;
			//button | checkbox | file | hidden | image | password | radio | reset | select-multiple | select-one | submit | text | textarea
			if(_isOnlyChanged) {
				if(_ele.isChange === false) {
					continue;
				}
			}
			switch(_ele.type) {
				case "select-multiple":
					if(_ele.selectedIndex > -1) {
						_value = [];
						for(_j = 0; _j < _ele.options.length; _j ++){
							_option = _ele.options[_j];
							if(_option.selected && !$(_option).attr("disabled")) {
								if(_option.value !== undefined && _option.value !== "") {
									_value.push(_option.value);
								}
								else if(_option.text) {
									_value.push(_option.text);
								}
							}
						}
					}
					break;
				case "select-one":
					if(_ele.selectedIndex > -1) {
						_option = _ele.options[_ele.selectedIndex];
						if(!$(_option).attr("disabled")) {
							if(_option.value !== undefined && _option.value !== "") {
								_value = _option.value;
							}
							else if(_option.text) {
								_value = _option.text;
							}
						}
					}
					break;
				/*
				case "radio":
				case "file":
				case "submit":
				case "reset":
					
					break;
				*/
				case "button":
					continue;
					break;
				case "text":
				case "textarea":
					_validator = $(_ele).attr("bstd_valide_type");
					if(_validator) {
						_value = bstdData.Validator.Validate(_validator, $(_ele).attr("bstd_valide_values"), $(_ele).val());
						if(_value === false) {
							_isError = true;
							_errors[_ele.name] = (_validateError = $(_ele).attr("bstd_validate_error")) ? _validateError : "NotValideText";
						}
					}
					else {
						_value = $(_ele).val();
					}
					break;
				case "checkbox":
					_value = _ele.value;
					break;
				case "hidden":
					//if(_ele.oldValue != _ele.value) {
						_value = _ele.value;
					//}
					break;
				default:
					continue;
			}
			
			if((_bstdType = $(_ele).attr("bstd_type"))) {
				_value = _advancedElementsTypes[_bstdType].validate(_ele);
				if(typeof _value == "object") {
					if(_value instanceof bstdError) {
						_isError = true;
						_errors[_ele.name] = _value.getMessage();
						_value = null;
					}
				}
			}
			
			if(!_value) {
				if($(_ele).attr("required")) {
					_isError = true;
					_errors[_ele.name] = _ele.type == "select-multiple" || _ele.type == "select-one" ? "reqiredSelect" : "reqiredText";
				}
			}
			else if(_isOnlyChanged && _ele.isChange || !_isOnlyChanged) {
				_data[_ele.name] = _value;
			}
			_value = null;
		}
		
		for(_i in _currFormProp.radio) {
			if(_currentForm.radio[_i].isChange) {
				_data[_ele.name] = _currentForm.radio[_i].currentValue;
			}
			else {
				if(_currentForm.radio[_i].required && (_currentForm.radio[_i].currentValue === undefined
						|| _currentForm.radio[_i].currentValue !== undefined && $(_currentForm.radio[_i].selected).attr("disabled") !== undefined)) {
					_isError = true;
					_errors[_ele.name] = "reqiredSelect";
				}
				else if(!_isOnlyChanged) {
					_data[_ele.name] = null;
				}
			}
		}
		
		//TODO: Проверка файлов
		
		return _isError ? {isError: true, errors: _errors} : _data;
	};
	
	/**
	 * Устанавливает сообщения об ошибках. 
	 */
	this.setErrors = function(form, errors, ele) {
		var _elementName,
			_errorContainer,
			_eles,
			_i;
		form = sThis.getForm(form);
		sThis.setCurrent(form);
		if(!ele) {
			if(typeof errors == "object") {
				for(_elementName in errors) {
					if(_currFormProp.errorType == 1 || _currFormProp.errorType == 3) {
						if(_errorContainer = sThis.getElementErrorContainer(form, _elementName)) {
							$(_errorContainer).html(errors[_elementName]);
						}
					}
					_eles = form[_elementName];
					if(_eles.tagName) {
						_eles = [_eles];
					}
					for(_i = 0; _i < _eles.length; _i ++) {
						if(_currFormProp.errorType == 2 || _currFormProp.errorType == 3) {
							if(window["bstdTips"]) {
								bstdTips.unset(_eles[_i]);
								bstdTips.set(_eles[_i], errors[_elementName]);
							}
							else if(_SYS_DEBUG) {
								throw new Error("Arguments errors must by object");
							}
							$(_eles[_i]).addClass("error");
							_eles[_i].error = true;
						}
					}
				}
			}
			else {
				if(_SYS_DEBUG) {
					throw new Error("Arguments errors must by object");
				}
				return false;
			}
		}
		else {
			_elementName = ele.name;
			if(_currFormProp.errorType == 1 || _currFormProp.errorType == 3) {
				if(_errorContainer = sThis.getElementErrorContainer(form, _elementName)) {
					$(_errorContainer).html(errors);
				}
			}
			if(_currFormProp.errorType == 2 || _currFormProp.errorType == 3) {
				if(window["bstdTips"]) {
					bstdTips.unset(ele);
					bstdTips.set(ele, errors);
				}
				else if(_SYS_DEBUG) {
					throw new Error("Arguments errors must by object");
				}
			}
			$(ele).addClass("error");
			ele.error = true;
		}
	};
	
	/**
	 * Очищает сообщения об ошибках
	 */
	this.clearErrors = function(form, ele) {
		var _element,
			_i;
		form = sThis.getForm(form);
		if(ele) {
			if(ele.error) {
				$(ele).removeClass("error");
				
				if(_currFormProp.errorType == 1 || _currFormProp.errorType == 3) {
					if(_errorContainer = sThis.getElementErrorContainer(form, ele.name)) {
						$(_errorContainer).html("");
					}
				}
				if(_currFormProp.errorType == 2 || _currFormProp.errorType == 3) {
					if(window["bstdTips"]) {
						bstdTips.unset(ele, true);
					}
					else if(_SYS_DEBUG) {
						throw new Error("Arguments errors must by object");
					}
					$(ele).removeClass("error");
					ele.error = false;
				}
			}
		}
		else {
			for(_i = 0; _i < form.elements.length; _i ++) {
				ele = form.elements[_i];
				if(!ele.error) {
					continue;
				}
				$(ele).removeClass("error");
				
				if(_currFormProp.errorType == 1 || _currFormProp.errorType == 3) {
					if(_errorContainer = sThis.getElementErrorContainer(form, ele.name)) {
						$(_errorContainer).html("");
					}
				}
				if(_currFormProp.errorType == 2 || _currFormProp.errorType == 3) {
					if(window["bstdTips"]) {
						bstdTips.unset(ele, true);
					}
					else if(_SYS_DEBUG) {
						throw new Error("Arguments errors must by object");
					}
					$(ele).removeClass("error");
					ele.error = false;
				}
			}
		}
	};
	
	this.getElementErrorContainer = function(form, elementName) {
		var _element;
		
		form = sThis.getForm(form);
		if(typeof elementName == "string") {
			_element = form[elementName];
		}
		else {
			_element = elementName;
		}
		if(typeof _element == "object" && _element.tagName) {
			return $((_element.id ? _element.id : (form.name ? form.name : form.id ? form.id : "") + _element.name) + "_error")[0];
		}
		else {
			if(_SYS_DEBUG) {
				throw new Error("Element name '" + elementName + "' is error or element not found");
			}
			return false;
		}
	};
};



$(document).ready(function(){
	$("form").each(function(){bstdForms.formInit(this);});
});