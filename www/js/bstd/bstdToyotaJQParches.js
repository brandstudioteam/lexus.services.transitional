if($) {
	(function() {
		var r20 = /%20/g,
		rbracket = /\[\]$/,
		rCRLF = /\r?\n/g,
		rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
		rselectTextarea = /^(?:select|textarea)/i,
		core_push = Array.prototype.push,
		core_slice = Array.prototype.slice,
		core_indexOf = Array.prototype.indexOf,
		core_toString = Object.prototype.toString,
		core_hasOwn = Object.prototype.hasOwnProperty,
		core_trim = String.prototype.trim;
		
		if(!$.isFunction) {
			$.isFunction = function(val) {return typeof val == "function";};
		}
		if($.isObject) {
			$isObject = function() {return typeof c == "object"};
		}
		if(!$.isArray) {
			$.isArray = function(val) {return val instanceof Array;};
		}
		if(!$.isWindow) {
			$.isWindow = function( obj ) {
				return obj != null && obj == obj.window;
			};
		}
		if(!$.isPlainObject) {
			$.isPlainObject = function( obj ) {
				// Must be an Object.
				// Because of IE, we also have to check the presence of the constructor property.
				// Make sure that DOM nodes and window objects don't pass through, as well
				if (!obj || typeof obj !== "object" || obj.nodeType || $.isWindow( obj ) ) {
					return false;
				}
				try {
					// Not own constructor property must be Object
					if (obj.constructor &&
						!core_hasOwn.call(obj, "constructor") &&
						!core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
							return false;
					}
				} catch (e) {
					// IE8,9 Will throw exceptions on certain host objects #9897
					return false;
				}

				// Own properties are enumerated firstly, so to speed up,
				// if last one is own, then all properties are own.

				var key;
				for(key in obj) {}
				return key === undefined || core_hasOwn.call(obj, key);
			};
		}
		if(!$.isEmptyObject) {
			$.isEmptyObject = function(obj) {
				var name;
				for (name in obj) {
					return false;
				}
				return true;
			};
		}
	
		function buildParams(prefix, obj, add) {
			var name;
		
			if($.isArray(obj)) {
			// Serialize array item.
				$.each( obj, function( i, v ) {
					if ( traditional || rbracket.test( prefix ) ) {
						// Treat each array item as a scalar.
						add(prefix, v);
		
					} else {
						// If array item is non-scalar (array or object), encode its
						// numeric index to resolve deserialization ambiguity issues.
						// Note that rack (as of 1.0.0) can't currently deserialize
						// nested arrays properly, and attempting to do so may cause
						// a server error. Possible fixes are to modify rack's
						// deserialization algorithm or to provide an option or flag
						// to force array serialization to be shallow.
						buildParams(prefix + "[" + ($.isObject(v) ? i : "" ) + "]", v, traditional, add);
					}
				});
			} else if ($.isObject(obj) === "object" ) {
				//Serialize object item.
				for ( name in obj ) {
					buildParams( prefix + "[" + name + "]", obj[ name ], add );
				}
			} else {
				// Serialize scalar item.
				add( prefix, obj );
			}
		};

		$.param = function( a, traditional ) {
			var prefix,
				s = [],
				add = function( key, value ) {
					// If value is a function, invoke it and return its value
					value = $.isFunction( value ) ? value() : ( value == null ? "" : value );
					s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
				};
	
			// If an array was passed in, assume that it is an array of form elements.
			if($.isArray(a) || !$.isPlainObject(a)) {
				// Serialize the form elements
				$.each(a, function() {
					add(this.name, this.value);
				});
	
			} else {
				// If traditional, encode the "old" way (the way 1.3.2 or older
				// did it), otherwise encode params recursively.
				for(prefix in a) {
					buildParams(prefix, a[prefix], traditional, add);
				}
			}
	
			// Return the resulting serialization
			return s.join("&").replace(r20, "+");
		};
		
		$.inArray = function(elem, arr, i) {
			var _len,
				_core_indexOf = Array.prototype.indexOf;
			if(arr) {
				if(_core_indexOf) {
					return _core_indexOf.call(arr, elem, i);
				}
				_len = arr.length;
				i = i ? i < 0 ? Math.max(0, _len + i) : i : 0;
				for(; i < _len; i++) {
					if(i in arr && arr[ i ] === elem) {
						return i;
					}
				}
			}
			return -1;
		};
		$.each = function(obj, callback, args) {
			var name,
				i = 0,
				length = obj.length,
				isObj = length === undefined || $.isFunction(obj);
			
			if(args) {
				if(isObj) {
					for(name in obj) {
						if(callback.apply(obj[ name ], args) === false) {
							break;
						}
					}
				} else {
					for(; i < length;) {
						if(callback.apply( obj[ i++ ], args ) === false) {
							break;
						}
					}
				}
		
			// A special, fast, case for the most common use of each
			} else {
				if(isObj) {
					for(name in obj) {
						if(callback.call(obj[ name ], name, obj[ name ]) === false) {
							break;
						}
					}
				} else {
					for(; i < length;) {
						if(callback.call(obj[ i ], i, obj[ i++ ]) === false) {
							break;
						}
					}
				}
			}
			return obj;
		};
	});
}