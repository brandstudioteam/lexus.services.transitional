/**
 * @class
 */
var bstdToyotaAPI = (new function() {
	/**
	 * @private
	 */
	var sThis = this;
	
	/**
	 * 
	 * @param response
	 * @param callback
	 * @param errorCallback
	 * @param eventName
	 * @returns
	 */
	var _checkResponse = function(response, callback, errorCallback, eventName) {
		if(response && (response.response || response.responseText)) {
			response = response.response || response.responseText;
			if(typeof response == "string") {
				response = $.parseJSON(response);
			}
		}
		if(response && typeof response == "object") {
			if(response.error) {
				if(typeof errorCallback == "function") errorCallback(response.errorMessage || response.message, response.errors);
				$(window).trigger("toyotaAPI", {eventType: eventName, data: null, error: response.error, errorMessage: response.errorMessage || response.message, errors: response.errors});
			}
			else {
				callback(response.result || response);
				//$.trigger("on" + eventName, response.result);
				$(window).trigger("toyotaAPI", {eventType: eventName, data: response.result || response});
			}
		}
		else {
			if(typeof errorCallback == "function") if(typeof errorCallback == "function") errorCallback(null);
			//$.trigger("on" + eventName, {error: "LoadError"});
			
			$(window).trigger("toyotaAPI", {eventType: eventName, data: null, errorMessage: "Not response error", error: 1});
		}
	};
	
	/**
	 * @private
	 * @param url
	 * @param data
	 * @param callback
	 * @param errorCallback
	 * @param eventName
	 * @returns
	 */
	var _send = function(url, data, callback, errorCallback, eventName) {
		var _fn = "jsonpF_" + Math.random().toString().split(".")[1];
		window[_fn] = (function(callback, errorCallback, eventName) {
			return function(response) {
				_checkResponse(response, callback, errorCallback, eventName);
			};
		}(callback, errorCallback, eventName));
		
		$.ajax({
		    url: url,
		    data: data,
		    jsonpCallback: _fn,
		    dataType: "jsonp"
		});
	};
	
	
	/**
	 * @class
	 * @public
	 */
	this.geo = (new function() {
		var oThis = this,
			_url = "http://promo.toyota.ru/z/geo/";
		
		/**
		 * Получить список стран, в которых есть дилеры, поиск страны, в которой есть дилеры, по строке
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{String} findText - Строку поиска дилера по названию
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getCountries = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params && params.findText) {
				_data = {findText: params.findText};
			}
			if(typeof params.callback == "function") {
				_callback = params.callback;
			}
			if(typeof params.errorCallback == "function") {
				_errorCallback = params.errorCallback;
			}
			_send(_url + "countries", _data, _callback, _errorCallback, "GetCountries");
		};
		
		/**
		 * Получить список стран, в которых есть дилеры
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{String} findText - Строку поиска дилера по названию
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersCountries = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.findText) {
					_data = {findText: params.findText};
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "dealerCountries", _data, _callback, _errorCallback, "GetDealersCountries");
		};
		
		/**
		 * Получить список  регионов для заданной страны, поиск региона по строке
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			countryId - Необязательный. Идентификатор страны
		 * 			findText - Необязательный. Строка поиска по названию региона.
		 * 			Внимание! Один из параметров countryId или findText должен быть задан
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 
		 * Внимание! Один из аргументов countryId или findText должен быть задан
		 */
		this.getRegion = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(!params.countryId && !params.findText) {
					if(typeof params.errorCallback == "function") {
						params.errorCallback({errorMessage: "Должен быть установлен один из параметров"});
					}
					return false;
				}
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(params.countryId) {
					_data["countryId"] = params.countryId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			else {
				return false;
			}
			_send(_url + "regions", _data, _callback, _errorCallback, "GetRegion");
		};
		
		/**
		 * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} countryId - Необязательный. Идентификатор страны
		 * 			{String} findText - Необязательный. Строка поиска
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersRegion = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			
			if(params) {
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(params.countryId) {
					_data["countryId"] = params.countryId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "dealerRegions", _data, _callback, _errorCallback, "GetDealersRegion");
		};
		
		/**
		 * Получить список всех городов для заданного региона, поиск города по строке
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} regionId - Необязательный. Идентификатор региона
		 * 			{String} findText - Необязательный. Строка поиска
		 * 			Внимание! Один из аргументов regionId или findText должен быть задан
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 
		 */
		this.getCities = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(!params.regionId && !params.findText) {
					if(typeof params.errorCallback == "function") {
						params.errorCallback({errorMessage: "Должен быть установлен один из параметров"});
					}
					return false;
				}
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(params.regionId) {
					_data["regionId"] = params.regionId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			else {
				return false;
			}
			_send(_url + "cities", _data, _callback, _errorCallback, "GetCities");
		};
		
		/**
		 * Получить список городов, в которых есть дилеры для заданной страны или региона, поиск города, в котором есть дилеры по строке
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} regionId - Необязательный. Идентификатор региона
		 * 			{int} countryId - Необязательный. Идентификатор страны
		 * 			{String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersCities = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(params.regionId) {
					_data["regionId"] = params.regionId;
				}
				if(params.countryId) {
					_data["countryId"] = params.countryId;
				}
				if(params.facilityId) {
					_data["facilityId"] = params.facilityId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "dealerCities", _data, _callback, _errorCallback, "GetDealersCities");
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{float} gLt - Необязательный. Широта.
		 * 			{float} gLg - Необязательный. Долгота.
		 * 			{int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getNearestCities = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.gLt) {
					_data["gLt"] = params.gLt;
				}
				if(params.gLg) {
					_data["gLg"] = params.gLg;
				}
				if(params.count) {
					_data["count"] = params.count;
				}
				if(params.facilityId) {
					_data["facilityId"] = params.facilityId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "nearcity", _data, _callback, _errorCallback, "GetNearestCities");
		};
		
		/**
		 * Получить список федеральных округов
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} countryId - Необязательный. Не используется в настоящее время. Или должен быть равен 172.
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 *			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getFederalDistrict = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(countryId && countryId != 172) {
					callback([]);
					return;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "federalDistrict", _data, _callback, _errorCallback, "GetFederalDistrict");
		};
		
		/**
		 * Получить список федеральных округов, в которых есть дилеры
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} countryId - Необязательный. Не используется в настоящее время. Или должен быть равен 172.
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersFederalDistrict = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(countryId && countryId != 172) {
					callback([]);
					return;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "dealersFederalDistrict", _data, _callback, _errorCallback, "GetDealersFederalDistrict");
		};
	});
	
	/**
	 * @class
	 * @public
	 */
	this.dealers = (new function(){
		var oThis = this,
			_url = "http://promo.toyota.ru/z/dealers/";
		
		/**
		 * Получить список федеральных округов, в которых есть дилеры
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} cityId - Необязательный. Идентификатор города.
		 * 			{int} partnerId - Необязательный. Идентификатор дилера.
		 * 			{String} findText - Необязательный. Строка поиска
		 * 			{int} facilityId - Необязательный. Идентификатор или массив идентификаторов возможностей дилеров.
		 * 			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealers = function(params) {
			var _data = {},
				_tUrl = _url,
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(params.partnerId) {
					_data["partnerId"] = params.partnerId;
				}
				if(params.cityId) {
					_tUrl = _tUrl + cityId;
					//_data["cityId"] = params.cityId;
				}
				if(params.facilityId) {
					_data["facilityId"] = params.facilityId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_tUrl, _data, _callback, _errorCallback, "GetDealers");
		};
		
		/**
		 * Получить список ближайших к пользователю дилеров
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{float} gLt - Необязательный. Широта.
		 * 			{float} gLg - Необязательный. Долгота.
		 * 			{int} count - Необязательный. Количество возвращаемых дилеров.
		 * 			{int} citiesCount - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 * 			{int} facilityId - Необязательный. Идентификатор или массив идентификаторов возможностей дилеров.
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getNearestDealers = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.count) {
					_data["count"] = params.count;
				}
				if(params.gLt) {
					_data["gLt"] = params.gLt;
				}
				if(params.gLg) {
					_data["gLg"] = params.gLg;
				}
				if(params.citiesCount) {
					_data["citiesCount"] = citiesCount;
				}
				if(params.facilityId) {
					_data["facilityId"] = params.facilityId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "nearest/", _data, _callback, _errorCallback, "GetNearestDealers");
		};
		
		/**
		 * Получить список типов дилеров
		 * @public
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersTypes = function(params) {
			var _callback = null,
				_errorCallback = null;
			if(params) {
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "types/", {}, _callback, _errorCallback, "GetDealersTypes");
		};
	});
	
	/**
	 * @class
	 * @public
	 */
	this.cars = (new function(){
		var oThis = this,
			_url = "http://promo.toyota.ru/z/cars/";
		
		/**
		 * Получить список моделей автомобилей Toyota или поиск модели Toyota
		 * 
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{int} count - Необязательный. Количество возвращаемых дилеров.
		 * 			{int} citiesCount - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 * 			{String} findText - Необязательный. Строка поиска
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getToyotaCars = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(findText) {
					_data["findText"] = findText;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url, _data, _callback, _errorCallback, "GetToyotaCars");
		};
		/**
		 * Получить список всех сторонних производителей автомобилей или поиск стороннего производителя автомобилей
		 * 
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{String} findText - Необязательный. Строка поиска
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getManufacturers = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url + "manufacturers", _data, _callback, _errorCallback, "GetManufacturers");
		};
		/**
		 * Получить список моделей автомобилей для стороннего производителя или поиск модели автомобиля стороннего производителя
		 * 
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 * 			{String} findText - Необязательный. Строка поиска
		 * 			{int} manufacturerId - Необязательный. Идентификатор производителя
		 * 			Внимание! Один из параметров findText или manufacturerId должен быть задан
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getCars = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(!params.findText && !manufacturerId.manufacturerId) {
					if(typeof params.errorCallback == "function") {
						params.errorCallback({messageError: "Arguments errror"});
						return false;
					}
				}
				if(params.findText) {
					_data["findText"] = params.findText;
				}
				if(params.manufacturerId) {
					_data["manufacturerId"] = params.manufacturerId;
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			else {
				return false;
			}
			_send(_url + "models", _data, _callback, _errorCallback, "GetCars");
		};
	});
	
	this.facilities = (new function() {
		var oThis = this,
		_url = "http://contacts.toyota.ru/z/facilities/";
		/**
		 * Получить список возможностей дилеров
		 * 
		 * @param {Object} params - Необязательный. Параметры
		 * 		Может содержать:
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getFacilities = function(params) {
			var _callback = null,
				_errorCallback = null;
			if(params) {
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			_send(_url, {}, _callback, _errorCallback, "GetFacilities");
		};
		/**
		 * Получить список возможностей, которые имеются у данного дилера
		 * 
		 * @param {Object} params - Параметры
		 * 		Может содержать:
		 * 			{int} dealer - Обязательный. Идентификатор дилера
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getFacilitiesDealersRelated = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.dealer) {
					_data["dealer"] = params.dealer;
				}
				else {
					if(typeof params.errorCallback == "function") {
						params.errorCallback({errorMessage: "Arguments error"});
						return false;
					}
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			else {
				return false;
			}
			_send(_url + "dealers_related", _data, _callback, _errorCallback, "GetFacilities");
		};
		/**
		 * Получить список возможностей, которых нет у данного дилера
		 * 
		 * @param {Object} params - Параметры
		 * 		Может содержать:
		 * 			{int} dealer - Обязательный. Идентификатор дилера
		 *			{Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 			{Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getFacilitiesDealersNotRelated = function(params) {
			var _data = {},
				_callback = null,
				_errorCallback = null;
			if(params) {
				if(params.dealer) {
					_data["dealer"] = params.dealer;
				}
				else {
					if(typeof params.errorCallback == "function") {
						params.errorCallback({errorMessage: "Arguments error"});
						return false;
					}
				}
				if(typeof params.callback == "function") {
					_callback = params.callback;
				}
				if(typeof params.errorCallback == "function") {
					_errorCallback = params.errorCallback;
				}
			}
			else {
				return false;
			}
			_send(_url + "dealers_not_related", _data, _callback, _errorCallback, "GetFacilities");
		};
	});
	
	
	
	
	this.UI = (new function() {
		var uThis = this;
		
		var _createOptions = function(select, data, callback, eventName, idFiled, nameField, selected) {
			var _i,
				_ele,
				_items = [],
				_item;
			
			if(!idFiled) {
				idFiled = "id";
			}
			if(!nameField) {
				nameField = "title";
			}
			
			select = $(select).empty();
			if(data) {
				if(data.length > 1) {
					_item = $('<option disabled selected></option>');
					_items.push(_item);
					if(select.length) {
						select.append(_item);
					}
				}
				for(i = 0; i < data.length; i++) {
					_item = $('<option' + (selected !== undefined && data[i][idFiled] == selected ? ' selected="selected"' : '') + ' value="' + data[i][idFiled] + '">' + data[i][nameField] + '</option>');
					_item[0].bstdData = data[i];
					_items.push(_item);
					if(select.length) {
						select.append(_item);
					}
				}
				if(select.length) {
					select.attr("disabled", false);
					if(callback) {
						callback(data.length);
					}
				}
				else {
					if(callback) {
						callback(_items);
					}
				}
				if(!callback) {
					select.trigger("on" + eventName + "OptionCreate", {items:_items});
				}
			}
		};

		
		var _prepareResponse = function(response, select, callback, errorCallback, eventName, idFiled, nameField, selected) {
			_createOptions(select, response, callback, eventName, idFiled, nameField, selected);
		};
		
		
		var _createQuery = function(data, select, selected, section, method, idField, nameField, eventName) {
			var _callback;
			if(data) {
				_callback = data.callback || null;
			}
			else {
				data = {};
			}
			data.callback = function(select, callback, errorCallback, eventName, idField, nameField, selected) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, eventName, idField, nameField, selected);
				};
			}(select, _callback, data.errorCallback || null, eventName, idField, nameField, selected);
			if(sThis[section] && typeof sThis[section][method] == "function") {
				sThis[section][method](data);
			}
		};
		
		this.createCountries = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getCountries", "countryId", "countryName", "Cuntries")) {
				return false;
			}
		};
		
		this.createDealersCountries = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getDealersCountries", "countryId", "countryName", "DealersCountries")) {
				return false;
			}
		};
		
		
		this.createRegion = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getRegion", "regionId", "regionName", "Region")) {
				return false;
			}
		};
		
		this.createDealersRegion = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getDealersRegion", "regionId", "regionName", "DealersRegion")) {
				return false;
			}
		};
		
		//federalDistrict
		this.createFederalDistrict = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getFederalDistrict", "federalDistrictId", "federalDistrictName", "FederalDistrict")) {
				return false;
			}
		};
		
		this.createDealersFederalDistrict = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getDealersFederalDistrict", "federalDistrictId", "federalDistrictName", "DealersFederalDistrict")) {
				return false;
			}
		};
		
		
		
		
		this.createCities = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getCities", "cityId", "cityName", "Cities")) {
				return false;
			}
		};
		
		/**
		 * Получить список городов, в которых есть дилеры
		 * @public
		 */
		this.createDealersCities = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getDealersCities", "cityId", "cityName", "DealersCities")) {
				return false;
			}
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.createNearestCities = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "geo", "getNearestCities", "cityId", "cityName", "NearestCities")) {
				return false;
			}
		};
		
		
		
		
		this.createDealers = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "dealers", "getDealers", "partnerId", "partnerName", "Dealers")) {
				return false;
			}
		};
		
		this.createPartners = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "dealers", "getPartners", "partnerId", "partnerName", "Dealers")) {
				return false;
			}
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.createNearestDealers = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "dealers", "getNearestDealers", "partnerId", "partnerName", "NearestDealers")) {
				return false;
			}
		};
		
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.createDealersTypes = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "dealers", "getDealersTypes", "partnerId", "partnerName", "DealersTypes")) {
				return false;
			}
		};
		
		
		this.createToyotaCars = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "cars", "getToyotaCars", "modelId", "modelName", "ToyotaCars")) {
				return false;
			}
		};
		
		this.createManufacturers = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "cars", "getManufacturers", "id", "title", "Manufacturers")) {
				return false;
			}
		};
		
		
		this.createCars = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "cars", "getCars", "id", "title", "Cars")) {
				return false;
			}
		};
		
		
		this.createFacilitiesRelated = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "facilities", "getFacilitiesDealersRelated", "facilityId", "facilityName", "FacilitiesRelated")) {
				return false;
			}
		};
		
		this.createFacilitiesNotRelated = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "facilities", "getFacilitiesDealersNotRelated", "facilityId", "facilityName", "FacilitiesNotRelated")) {
				return false;
			}
		};
		
		this.createFacilities = function(data, select, selected) {
			if(!_createQuery(data, select, selected, "facilities", "getFacilities", "facilityId", "facilityName", "Facilities")) {
				return false;
			}
		};
		
		
		this.createGeoSelects = function(countrySelect, regionSelect, citySelect, callback, errorCallback, countrySelected, regionSelected, citySelected) {
			var _data = {};
			
			$(countrySelect).attr("disabled", true);
			$(regionSelect).attr("disabled", true);
			$(citySelect).attr("disabled", true);
			
			_data["callback"] = 
			uThis.createCountries(
				{
					callback: function(){
						$(countrySelect).attr("disabled", false);
						if(typeof callback == "function") {
							callback({currentTarget: countrySelect, type: "countriesLoaded"});
						}
					},
					errorCallback: errorCallback
				},
				countrySelect,
				countrySelected);
			
			$(countrySelect).bind("change", function() {
				$(regionSelect).empty().attr("disabled", true);
				$(citySelect).empty().attr("disabled", true);
				
				uThis.createRegion(
					{
						countryId: $(countrySelect).find("option:selected").val(),
						callback: function(count){
							$(regionSelect).attr("disabled", false);
							if(typeof callback == "function") {
								callback({currentTarget: countrySelect, type: "regionsLoaded"});
							}
							if(count == 1) {
								_createCities();
							}
						}
					},
					regionSelect,
					regionSelected);
			});
			
			var _createCities = function() {
				$(citySelect).empty().attr("disabled", true);
				
				
				uThis.createCities(
					{
						regionId: $(regionSelect).find("option:selected").val(),
						callback: function(){
							$(citySelect).attr("disabled", false);
							if(typeof callback == "function") {
								callback({currentTarget: countrySelect, type: "citiesLoaded"});
							}
						}
					},
					citySelect, citySelected);
			};
			
			$(regionSelect).bind("change", _createCities);
		};
	});
});