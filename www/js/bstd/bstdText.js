var bstdText = {};
bstdText["NotValideChars"] = "Введены некорректные символы";
bstdText["ErrRequired"] = "Обязательное поле";

bstdText.NotSendForNotChangedData = "Форма не может быть отправлена, поскольку данные не изменились";
bstdText.reqiredSelect = "Необходимо выбрать элемент из списка";
bstdText.reqiredText = "Необходимо ввести данные";
bstdText.reqiredFile = "Необходимо выбрать файл для загрузки";
bstdText.NotValideText = "Введены некорректные символы или значение";
bstdText.capthaNotValide = "Не верный код подтверждения";
bstdText.capthaRequired = "Необходимо ввести код подтверждения";