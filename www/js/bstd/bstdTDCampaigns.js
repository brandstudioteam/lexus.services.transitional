var _urlGetAction = "/admin/campaigns/",
	_currentCampaign,
	_dataTypeSets = {
			"actions": {
				"actions": {
					"custom":	{},
					"basic":	{
						"delete":	{
							"url":			"/admin/campaigns/actiondelete/#id#",
							"title":		"Удалить",
							"confirm":		"Вы действительно хотите удалить мероприятие \"#title#\"",
							"error":		"При попытке удаления мероприятия возникла ошибка"
						},
						"add":		{
							"url":			"/admin/campaigns/actioncreate",
							"title":		"Создать",
							"mode":			"popup",
							"form": {
					 			"id":		"actionForm",
					 			"title":	"Создание мероприятия",
					 			"onInit":	bstdTDAdmin.actionFormInit
				 			}
						},
						"view":		{
							"url":			"/admin/campaigns/actionsave/#id#",
							"title":		"Смотреть",
							"mode":			"popup",
							"form": {
					 			"id":		"actionForm",
					 			"title":	"Просмотр мероприятия",
					 			"onInit":	bstdTDAdmin.actionFormInit
				 			}
						},
						"edit":		{
							"url":			"/admin/campaigns/actionsave/#id#",
							"title":		"Редактировать",
							"mode":			"popup",
							"form": {
					 			"id":		"actionForm",
					 			"title":	"Редактирование мероприятия",
					 			"onInit":	bstdTDAdmin.actionFormInit
				 			}
						}
						//"edit", "load", "search"
					}
	 			},
	 			"table": {
	 				"id":		"campaignsTable",
	 				"actions":	["view", "delete"],
	 				"fields":	[
	 					"actionName",
	 					"actionDescription",
	 					"actionDate",
	 					"actionStart",
	 					"actionEnd",
	 					"testDriverQuotes",
	 					"testDriverBusyQuotes",
	 					"testDriverAvailableQuotes"
	 				]
	 			},
	 			"form":		{
		 			"id": "actionForm"
	 			},
	 			"loadType":		"preload",
	 			"idFields":		"actionId",
	 			"titleFields":	"actionName"
			}
		},
	_dataTypeSets,
	_setsData = parseInt(0, 2),
	_tableActionButtonTemplate = '<a data-button-action="#buttonActionName#" href="javascript:void(0);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">&nbsp;#buttonActionTitle#</span></a>';;

var bstdTDAdmin = new function() {
	var sThis = this;
	
	this.loadAction = function(e) {
		var _ele = $(this),
			action = _ele.parent().parent().attr("data-id");
		$.ajax({
			url: 		_urlGetAction + "actionview/" + action,
			type:		"post",
			dataType:	'json',
			complete:	function(ele) {
							return function(result) {
								_actionLoaded(bstdSystem.prepareResult(result, true), ele);
							};
						}(_ele)
		});
	};
	
	var _actionLoaded = function(result, ele) {
		_currentAction = _prepareData(result);
		_currentAction.currentAction = "view";
		bstdData.setAction(ele, "view");
	};
	
	var _redrawForm = function() {
		$("form .form-field-box:visible:even").removeClass("odd").addClass("even");
		$("form .form-field-box:visible:odd").removeClass("even").addClass("odd");
	};
	
	var _calculateRaces = function(races, start, end, membersCount, partnerCount) {
		var _duration,
			_raceDuration,
			_dDuration,
			_i,
			_start0,
			_start = _currentAction.action.actionStart,
			_end = _currentAction.action.actionEnd,
			_racesCount = _currentAction.testDrivesObj[_getCurrentTestDrive()].racesCount,
			_races = _currentAction.testDrivesObj[_getCurrentTestDrive()].races;
		
		
		if(!_racesCount || !_start || !_end) {
			return;
		}
		
		_duration = bstdDate.time2Sec(_end) - (_start0 = bstdDate.time2Sec(_start));
		_raceDuration = parseInt(_duration / _racesCount);
		_dDuration = _duration - _raceDuration * _racesCount;
		for(_i = 0; _i < _racesCount; _i ++) {
			_races[_i] = {
				raceId: _i,
				raceStart: bstdDate.sec2Time(_start0 + _raceDuration * _i + (_i == _racesCount - 1 ? _dDuration : 0)),
				raceDuration: _raceDuration,
				raceQuotes: membersCount,
				raceAvailableQuotes: membersCount,
				raceBusyQuotes: membersCount,
				receNew: 1
			};
			if(_currentAction.action.quotesMethod == 1) {
				if(_currentCampaign.campaignUseCars == 1) {
					if(_currentCampaign.campaignUseAgency == 1) {
						_races[_i]["cars"] = {};
					}
				}
				else {
					_races[_i].partnersTypes = {};
					_races[_i].partnersTypes.dealers = {};
					if(_currentCampaign.campaignUseAgency == 1) {
						_races[_i].partnersTypes.agencies = {};
					}
				}
			}
			if(_currentCampaign.campaignUseCars == 1) {
				if(_currentCampaign.campaignUseAgency == 1) {
					_races[_i]["cars"] = {};
				}
			}
			else {
				_races[_i]["dealers"] = {};
				if(_currentCampaign.campaignUseAgency == 1) {
					_races[_i]["agencies"] = {};
				}
			}
		}
		
		if(_currentAction.currentAction == bstdForm.prototype.MODE_ADD) {
			_partnersTables.create(1);
		}
		
		return _races;
	};
	
	
	var _changeDealer = function(e, data) {
		var _dealersObj = _currentAction.dealersObj,
			_data = $("#dealerId option[value=" + data.selected + "]").first().data(),
			_tr,
			_form = bstdData.getForm("actions");
		if(data.deselected) {
			if(_dealersObj[data.deselected]) {
				delete(_currentAction.dealersObj[data.deselected]);
				_tr = $("#dealersCodeTable tbody [data-dealer-id=" + data.deselected + "]");
				_form.removeElement(_tr.find("input"));
				_tr.remove();
				
				//TODO: Удалить дилера из заездов и таблицы заездов
				
				delete(_dealersObj[data.deselected]);
				_currentAction.dealersCount --;
			}
		}
		if(data.selected) {
			_dealersObj[data.selected] = _data;
			_currentAction.dealersCount ++;
			_addDealerInADTable(_data);
		}
		
		if(_currentAction.dealersCount) {
			if(_setsData & 16 != 16) {
				_setsData = _setsData + parseInt(16, 2);
			}
		}
		else {
			if(_setsData & 16 == 16) {
				_setsData = _setsData - parseInt(16, 2);
			}
		}
	};
	
	var _changeDealerCode = function(e) {
		var _ele = $(this),
			_dealers = _currentAction.dealersObj,
			_isSet = true,
			_i;

		_dealers[_ele.attr("data-dealer-id")].code = _ele.val();
		
		for(_i in _dealers) {
			if(!_dealers[_i].code) {
				_isSet = false;
				break;
			}
		}
		
		if(_isSet) {
			if(_setsData & 32 != 32) {
				_setsData = _setsData + parseInt(32, 2);
			}
		}
		else {
			if(_setsData & 32 == 32) {
				_setsData = _setsData - parseInt(32, 2);
			}
		}
	};
	
	var _changeDealerRacesQuotes = function() {
		var _races = _currentAction.testDrivesObj[_getCurrentTestDrive()].races,
			_dealersObj,
			_ele = $(this),
			_parent = _ele.parent(),
			_dealerId = _parent.attr("data-partner-id"),
			_raceId = _parent.parent().attr("data-race-id"),
			_isSet = true,
			_dealers,
			_i,
			_j;
		
		if(_currentAction.action.quotesMethod == 1) {
			_races[_raceId].partnersTypes.dealers.quote = _ele.val();
			_dealersObj = _currentAction.dealersObj;
			
			for(_i in _dealersObj) {
				_races[_raceId].dealers[_i] = jQuery.extend({}, _dealersObj[_i]);
				_races[_raceId].dealers[_i].quote = "";
			}
		}
		else {
			_races[_raceId].dealers[_dealerId] = jQuery.extend({}, _currentAction.dealersObj[_dealerId]);
			_races[_raceId].dealers[_dealerId].quote = _ele.val();
		}
		
		for(_j in _races) {
			_dealers = _races[_j].dealers;
			for(_i in _dealers) {
				if(!_dealers[_i].quote === undefined) {
					_isSet = false;
					break;
				}
			}
		}
		if(_currentCampaign.campaignUseCars == 1) {
		}
		else {
			if(_isSet) {
				if(_setsData & 256 != 256) {
					_setsData += parseInt(256, 2);
				}
			}
			else {
				if(_setsData & 256 == 256) {
					_setsData -= parseInt(256, 2);
				}
			}
		}
	};
	
	var _changeAgency = function() {
		
	};
	var _changeRegion = function(e, data) {
		var _idx;
		
		if(data.deselected) {
			if((_idx = _currentAction.action.regionId.indexOf(data.deselected)) > -1) {
				_currentAction.action.regionId.splice(_idx, 1);
			}
		}
		if(data.selected) {
			_currentAction.action.regionId.push(data.selected);
		}
		
		_createPartnersList();
		if(!$("#actionName").val()) {
			$("#actionName").val(_currentAction["actionName"] = $("#actionRegionId option:selected").text()).trigger("change");
		}
		else if(/(\d{2}).(\d{2}).(\d{4})/g.test($("#actionName").val())) {
			$("#actionName").val(_currentAction["actionName"] = $("#actionRegionId option:selected").text() + " " + $("#actionName").val()).trigger("change");
		}
		_currentAction["actionRegionId"] = $("#actionRegionId").val();
		
		if(_currentAction["actionRegionId"].length) {
			if(_setsData & 1 != 1) {
				_setsData = _setsData + parseInt(1, 2);
			}
		}
		else {
			if(_setsData & 1 == 1) {
				_setsData = _setsData - parseInt(1, 2);
			}
		}
	};
	
	var _changeActionName = function () {
		_currentAction.action["actionName"] = $("#actionName").val();
	};
	var _changeData = function(e, data) {
		var _oldVal = _currentAction.action.actionDate,
			_val = data.date;
		
		if(_oldVal == data.unixDate) {
			return;
		}
		
		_currentAction.action.actionDate = data.unixDate;
		
		$("#actionStart").attr("disabled", false);
		$("#actionEnd").attr("disabled", false);

		if(!$("#actionName").val()) {
			$("#actionName").val($("#actionDate").val()).trigger("change");
		}
		else if(/(\d{2}).(\d{2}).(\d{4})/g.test($("#actionName").val())) {
			$("#actionName").val($("#actionName").val().replace(/(\d{2}).(\d{2}).(\d{4})/g, _val)).trigger("change");
		}
		else {
			$("#actionName").val($("#actionName").val() + " " + _val).trigger("change");
		}
		
		if(data.unixDate) {
			if(_setsData & 2 != 2) {
				_setsData = _setsData + parseInt(2, 2);
			}
		}
		else {
			if(_setsData & 2 == 2) {
				_setsData = _setsData - parseInt(2, 2);
			}
		}
		
	};
	var _changeStart = function() {
		var _val = $(this).val(),
			_oldVal = _currentAction.action.actionStart;
		
		if(_oldVal && _oldVal != _val) {
			//TODO: Пересчитать время заездов
		}
		if(_val) {
			if(_setsData & 4 != 4) {
				_setsData = _setsData + parseInt(4, 2);
			}
		}
		else {
			if(_setsData & 4 == 4) {
				_setsData = _setsData - parseInt(4, 2);
			}
		}
		_currentAction.action.actionStart = _val;
	};
	var _changeEnd = function() {
		var _val = $(this).val(),
			_oldVal = _currentAction.action.actionEnd;
		
		if(_oldVal && _oldVal != _val) {
			//TODO: Пересчитать время заездов
		}
		if(_val) {
			if(_setsData & 8 != 8) {
				_setsData = _setsData + parseInt(8, 2);
			}
		}
		else {
			if(_setsData & 8 == 8) {
				_setsData = _setsData - parseInt(8, 2);
			}
		}
		_currentAction.action.actionEnd = _val;
	};
	var _changeRacesCount = function(e) {
		var _val = $(this).val();
		_currentAction.testDrivesObj[_getCurrentTestDrive()].racesCount = _val;
	};
	var _changeCars = function(e, data) {
		var _item,
			_idx,
			_form = bstdData.getForm("actions");
		
		if(data.selected !== undefined) {
			_currentAction.testDrivesObj[_getCurrentTestDrive()].cars[data.selected] = $("#modelId option[value=" + data.selected + "]").first().data();
			_currentAction.testDrivesObj[_getCurrentTestDrive()].carsIdx.push(data.selected);
		}
		else if(data.deselected !== undefined) {
			delete(_currentAction.testDrivesObj[_getCurrentTestDrive()].cars[data.deselected]);
			if((_idx = _currentAction.testDrivesObj[_getCurrentTestDrive()].carsIdx.indexOf(data.deselected)) > -1) {
				_currentAction.testDrivesObj[_getCurrentTestDrive()].carsIdx.splice(_idx, 1);
			}
		}
		
		//TODO: Изменить строки в таблице квот
	};
	
	var _changeDealerMembersCount = function() {
		
	};
	var _changeAgencyMembersCount = function() {
		
	};
	var _changeTestDrivesCount = function(e) {
		//Создаем элементы тест-драйвов
	};
	
	var _regionsLoaded = function() {
		if(bstdData.getForm("actions").mode() != bstdForm.prototype.MODE_ADD) {
			$("#actionRegionId option").not(":selected").attr("disabled", true);
		}
		$("#actionRegionId").trigger("liszt:updated");
	};
	
	
	var _createTestDrive = function() {
		
	};

/**
 * Добавляет модель в таблицу моделей в тест-драйве для типов партнеров
 */
	var _addModelsTDTable = function(testDrive, table) {
		var _td,
			_tr,
			_carsInTestDrives = _currentAction.testDrivesObj[testDrive].cars,
			_i,
			_current,
			_input,
			_tbody,
			_form = bstdData.getForm("actions");
		
		if(!table) {
			table = "#modelsTDQoutesTable_" + testDrive;
		}
		table = $(table);
		_tbody = $("tbody", table);
		
		for(_i in _carsInTestDrives) {
			_current = _carsInTestDrives[_i];
			if(!_current["partnersTypes"]) {
				_current["partnersTypes"] = {};
				_current["partnersTypes"]["dealers"] = {};
				_current["partnersTypes"]["agencies"] = {};
				_current["partnersTypes"]["dealers"]["testDriverQuotes"] = null;
			}
			_tr = $("<tr>").appendTo(_tbody).attr("data-car-id", _current["carId"]).append($("<td>").text(_current["carName"])).append(_td = $("<td>"));
			_td.append(_input = $('<input type="text" name="testDrivesDealersCar[' + testDrive + '][' + _current["carId"] + ']">'));
			_form.addElement(_input);
			if(_current["partnersTypes"]["dealers"]["testDriverQuotes"]) {
				_input.val(_current["partnersTypes"]["dealers"]["testDriverQuotes"]).trigger("change");
			}
			if(_currentCampaign.campaignUseAgency == 1) {
				_td = $("<td>").appendTo(_tr).append(_input = $('<input type="text" name="testDrivesDealersCar[' + testDrive + '][' + _current["carId"] + ']">'));
				_form.addElement(_input);
				if(_current["partnersTypes"]["agencies"]["testDriverQuotes"]) {
					_input.val(_current["partnersTypes"]["agencies"]["testDriverQuotes"]).trigger("change");
				}
			}
		}
	};
	
	
	var _addDealerInADTable = function(dealerData) {
		var _td,
			_tr,
			_i,
			_current,
			_input,
			_form = bstdData.getForm("actions"),
			_tbody = $("#dealersCodeTable tbody");
		
		_tr = $("<tr>").appendTo(_tbody).attr("data-dealer-id", dealerData["partnerId"]).append($("<td>").text(dealerData["partnerName"])).append(_td = $("<td>"));
		_td.append(_input = $('<input type="text" data-dealer-id="' + dealerData["partnerId"] + '" name="partnerCode[' + dealerData["partnerId"] + ']">'));
		
		if(dealerData.code !== undefined) {
			_input.val(dealerData.code);
		}
		
		_form.addElement(_input);
		if(dealerData["partnerCode"]) {
			_input.val(dealerData["partnerCode"]).trigger("change");
		}
	};
	
	
	
	
	var _changeSection = function() {
		var _current = $(this).attr("id");
		
		if(bstdData.getForm("actions").mode() == bstdForm.prototype.MODE_ADD) {
			switch(_current) {
				case "basicInfoButton":
					$("#infoDataContainer").hide();
					$("#testDrivesContainer").hide();
					$("#basicData").show();
					break;
				case "advancedInfoButton":
					if((_setsData & 1) && (_setsData & 2) && (_setsData & 4) && (_setsData & 8) && (_setsData & 16) && (_setsData & 32)) {
						console.log(_setsData, (_setsData & 1), (_setsData & 2), (_setsData & 4), (_setsData & 8), (_setsData & 16), (_setsData & 32));
					}
						$("#basicData").hide();
						$("#testDrivesContainer").hide();
						$("#infoDataContainer").show();
					/*
					}
					else {
						alert("Необходимо заполнить все обязательные поля на странице \"Основные данные\"");
					}
					*/
					break;
				case "racesInfoButton":
					if((_setsData & 1) && (_setsData & 2) && (_setsData & 4) && (_setsData & 8) && (_setsData & 16) && (_setsData & 32)) {
						console.log(_setsData, (_setsData & 1), (_setsData & 2), (_setsData & 4), (_setsData & 8), (_setsData & 16), (_setsData & 32));
					}
						$("#infoDataContainer").hide();
						$("#basicData").hide();
						$("#testDrivesContainer").show();
					/*
					}
					else {
						alert("Необходимо заполнить все обязательные поля на странице \"Основные данные\"");
					}
					*/
					break;
			}
		}
		else {
			switch(_current) {
				case "basicInfoButton":
					$("#infoDataContainer").hide();
					$("#testDrivesContainer").hide();
					$("#basicData").show();
					break;
				case "advancedInfoButton":
					$("#basicData").hide();
					$("#testDrivesContainer").hide();
					$("#infoDataContainer").show();
					break;
				case "racesInfoButton":
					$("#infoDataContainer").hide();
					$("#basicData").hide();
					$("#testDrivesContainer").show();
					break;
			}
		}
	};
	
	var _getSelectedCars = function() {
		var _cars,
			_i;
		if(_currentAction.testDrivesCarsObj) {
			for(_i in _currentAction.testDrivesCarsObj) {
				_cars = _currentAction.testDrivesCarsObj[_i];
				break;
			}
		}
		return _cars;
	};
	
	var _createCurrentAction = function() {
		_currentAction = {
			action: {
				actionId: 0,
				campaignId: _currentCampaign.campaignId,
				actionDate: null,
				actionStart: null,
				actionEnd: null,
				actionName: null,
				regionId: [],
				testDriversCount: 1,
				quotesMethod: 0
			},
			dealersCount: 0,
			testDrivesObj: {
				0: {
					races: {},
					partnersTypes: {},
					dealers: {},
					dealersIdx: []
				}
			},
			dealersObj: {},
			currentAction: 1
		};
		if(_currentCampaign.campaignUseCars == 1) {
			_currentAction.testDrivesObj[_getCurrentTestDrive()].cars = {};
			_currentAction.testDrivesObj[_getCurrentTestDrive()].carsIdx = [];
		}
		if(_currentCampaign.campaignUseAgency == 1) {
			_currentAction.testDrivesObj[_getCurrentTestDrive()].agencies = {};
			_currentAction.testDrivesObj[_getCurrentTestDrive()].agenciesIdx = []; 
		}
	};
	
	var _submit = function(formObj, resultFunction) {
		var data = {};
		
		if(bstdData.getForm("actions").mode == bstdForm.prototype.MODE_ADD) {
			if(_currentCampaign.campaignUseCars == 1) {
				if(_setsData != 1023) {
					return false;
				}
			}
			else {
				if(_setsData != 511) {
					return false;
				}
			}
		}
		_currentAction.action["actionVenue"] = $("#actionVenue").val();
		_currentAction.action["actionEmailText"] = $("#actionEmailText").val();
		_currentAction.action["actionDescription"] = $("#actionDescription").val();
		
		
		data["a"] = "n";
		data["b"] = "g";
		data["d"] = {
			"action": $.toJSON(_currentAction.action),
			"dealers": $.toJSON(_currentAction.dealersObj),
			//"dealers": $.toJSON(_currentAction.dealersObj),
			"testDrive": $.toJSON(_currentAction.testDrivesObj[_getCurrentTestDrive()])
		};
		
		
		
		$.ajax({
			url: 		_urlGetAction + (_currentAction.currentAction == bstdForm.prototype.MODE_ADD ? "actioncreate" : "actionsave"),
			type:		"post",
			data:		{prq: $.toJSON(data)},
			dataType:	'json',
			complete:	function(formObj, resultFunction) {
							return function(result) {
								_resultSubmit(bstdSystem.prepareResult(result, true), formObj, resultFunction);
							};
						}(formObj, resultFunction)
		});
	};
	
	var _resultSubmit = function(result, formObj, resultFunction) {
		formObj.saved();
		formObj.getForm().trigger("succesfullSubmiting", {form:formObj.getForm(), result:result, sendData: _currentAction.action, formObject: formObj, dataType: formObj.getDataType(), mode: formObj.mode()});
	};
	
	
	var _setData = function() {
		var _i,
			_r = [];
		_createPartnersList();
		$("#actionName").val(_currentAction.action.actionName);
		$("#actionDate").val(_currentAction.action.actionDate);
		$("#actionStart").val(_currentAction.action.actionStart);
		$("#actionEnd").val(_currentAction.action.actionEnd);
		
		$("#actionDescription").val(_currentAction.action.actionDescription);
		$("#actionVenue").val(_currentAction.action.actionVenue);
		$("#actionEmailText").val(_currentAction.action.actionEmailText);
		
		$("#testDriverRacesCount").val(_currentAction.testDrivesObj[_getCurrentTestDrive()].racesCount);
		_partnersTables.create(1);
		
		$("#quotesMethod option[value=" + _currentAction.action.quotesMethod + "]").attr("selected", true).attr("disabled", true);
		
		for(_i in _currentAction.dealersObj) {
			_addDealerInADTable(_currentAction.dealersObj[_i]);
		}
			
	};
	
	var _changeQuotesMethod = function() {
		_currentAction.action.quotesMethod = $("#quotesMethod").val();
	};
	
	this.actionFormInit = function(form, formId) {
		var _form = form.getForm();
		
		_setsData = 0;
		
		if(form.mode() == bstdForm.prototype.MODE_ADD) {
			_createCurrentAction();
			$("#quotesMethod").bind("change", _changeQuotesMethod);
			/*
			$("#racesInfoButton").attr("disabled", true);
			$("#advancedInfoButton").attr("disabled", true);
			$("#basicInfoButton").attr("disabled", false);
			$("#submitButton").attr("disabled", true);
			*/
		//Изменение количества дополнительных тест-драйвов
		//$("#testDriversCount").bind("change", _changeTestDrivesCount);
		//Выбор региона
			$("#actionRegionId").bind("change", _changeRegion);
		//Изменение даты
			$("#actionDate").bind("change", _changeData);
		//Изменение времени начала
			$("#actionStart").bind("change", _changeStart);
		//Изменение времени конца
			$("#actionEnd").bind("change", _changeEnd);
		//Изменение дилеров
			$("#dealerId").bind("change", _changeDealer);
			
		//Изменение количества заездов
			$("#testDriverRacesCount").bind("change", _changeRacesCount);
			$("#racesCountButton").bind("click", _calculateRaces);
			
		//Если участники записываются на автомобили
			if(_currentCampaign.campaignUseCars == 1) {
		//Выбор моделей автомобилей
				$("#modelId").bind("change", _changeCars);
			}
			else {
				//$("#testDriverDealersMembersCount").bind("change", _changeDealerMembersCount);
				if(_currentCampaign.campaignUseAgency == 1) {
					$("#testDriverAgenciesMembersCount").bind("change", _changeAgencyMembersCount);
				}
			}
		}
		else {
			_setData(formId);
			$("#racesCountButtonContainer").remove();
			$("#dealerId").attr("disabled", true);
			$("#actionEnd").attr("disabled", true);
			$("#actionStart").attr("disabled", true);
			$("#actionDate").attr("disabled", true);
			$("#quotesMethod").attr("disabled", true);
	//Выбор региона
	//		$("#actionRegionId option[value=" + _currentAction.action.regionId + "]").attr("selected", true);
	//Изменение названия мероприятия
			$("#actionName").text(_currentAction.action.actionName);
	//Изменение количества дополнительных тест-драйвов
			$("#testDriversCount").text(_currentAction.action.testDriversCount);
			$("#testDriverRacesCount").attr("disabled", true);
			form.removeElement($("#racesCountButton"));
			$("#racesCountButtonContainer").remove();
			//Если участники записываются на автомобили
			if(_currentCampaign.campaignUseCars == 1) {
				$("#modelId").attr("disabled", true);
			}
			/*
			$("#racesInfoButton").attr("disabled", false);
			$("#advancedInfoButton").attr("disabled", false);
			$("#basicInfoButton").attr("disabled", false);
			$("#submitButton").attr("disabled", false);
			*/
		}
		form.customSubmit(_submit);
		
		$("#racesInfoButton").bind("click", _changeSection);
		$("#advancedInfoButton").bind("click", _changeSection);
		$("#basicInfoButton").bind("click", _changeSection);
		
		
		bstdToyotaAPI.UI.createDealersRegion({callback: _regionsLoaded}, "#actionRegionId", _currentAction.action && _currentAction.action.regionId.length ? _currentAction.action.regionId : null);
	
	//Изменение названия мероприятия
		$("#actionName").bind("change", _changeActionName);
	//Изменение кодов дилеров
		$("#dealersCodeTable input").live("change", _changeDealerCode);
		
		//Если участники записываются на автомобили
		if(_currentCampaign.campaignUseCars == 1) {
			$("#modelsContainer").show();
			bstdToyotaAPI.UI.createToyotaCars({callback: function() {
						if(bstdData.getForm("actions").mode() != bstdForm.prototype.MODE_ADD) {
							$("#modelId option").not(":selected").attr("disabled", true);
						}
						$("#modelId").trigger("liszt:updated");
					}
				},
				"#modelId", _getSelectedCars());
		}
		else {
			$("#testDriverMembersCountForDealerContainer").show();
			if(_currentCampaign.campaignUseAgency == 1) {
				$("#testDriverMembersCountForAgencyContainer").show();
			}
		}
		
		$("textarea.texteditor", _form).tinymce(_tinymceSets);
		
		var _opt = {"display_disabled_options" : false};
		
		$(".chosen", _form).chosen(_opt);
		_redrawForm();
	};
	
	var _getDealersArray = function() {
		var _i,
			_r = [];
		for(_i in _currentAction.dealersObj) {
			_r.push(_i);
		}
		return _r;
	};
	
	var _getAgenciesArray = function() {
		
	};
	
	var _createPartnersList = function() {
		if(_currentCampaign.campaignUseAgency == 1) {
			$("#partnerIdContainer").show();
			_redrawForm();
		}
		else {
			$("#partnerIdContainer").hide();
			_redrawForm();
		}
		
		if(_currentAction.action.regionId.length) {
			$("#dealerId").attr("disabled", false);
			bstdToyotaAPI.UI.createPartners({
												partnerTypeId: [1, 6],
												regionId: _currentAction.action.regionId,
												callback: function() {
													if(bstdData.getForm("actions").mode() != bstdForm.prototype.MODE_ADD) {
														$("#dealerId option").not(":selected").attr("disabled", true);
													}
				
													$("#dealerId").trigger("liszt:updated");
												}
											}, "#dealerId", _getDealersArray());
			if(_currentCampaign.campaignUseAgency == 1) {
				$("#partnerId").attr("disabled", false);
				bstdToyotaAPI.UI.createPartners({partnerTypeId: 2, regionId: _currentAction.action.regionId, callback: function() {$("#partnerId").trigger("liszt:updated");}}, "#partnerId", _getAgenciesArray());
			}
			else {
				$("#partnerId").attr("disabled", true).find("option").attr("selected", false);
				$("#partnerId").trigger("liszt:updated");
			}
		}
		else {
			$("#dealerId").empty().trigger("liszt:updated").attr("disabled", true);
			if(_currentCampaign.campaignUseAgency == 1) {
				$("#partnerId").empty().trigger("liszt:updated").attr("disabled", true);
			}
		}
	};
	
	var _getCurrentTestDrive = function() {
		var _i;
		
		for(_i in _currentAction.testDrivesObj) {
			break;
		};
		if(_i == undefined) {
			_i = 0;
		}
		return _i;
	};
	
	var _partnersTables = new function() {
		var tThis = this;

		this.addAgency = function() {
			
		};

		this.addPartner = function(table, data, testDrive) {
			var _tbody = $("tbody", table),
				_thead = $("thead", table),
				_tfoot = $("tfoot", table),
				_rows = $("tr", _tbody),
				_theadRow1 = $("tr", _thead).first(),
				_theadRow2 = $("tr", _thead).last(),
				_tbodyRow,
				_tfootRow = $("tr", _tfoot),
				_lastCollumn,
				_row,
				_ele,
				_val,
				_partners,
				_partnersCount,
				_partnerData,
				_i,
				_j,
				_k,
				_d,
				_current,
				_len,
				_isAuto,
				_th,
				_currentTestDrive = _currentAction.testDrivesObj[_getCurrentTestDrive()],
				_racesCount = _currentTestDrive.racesCount,
				_races = _currentTestDrive.races,
				_modelCount = _currentTestDrive.testDriveCarModelsCount,
				_available,
				_busy;

				_current  = data;//[_d];
				//Создаем заголовок
				$('<th colspan="3">').attr("data-partner-id", _current.partnerId).text(_current.partnerName).insertBefore($("th", _theadRow1).last());
				
				$('<th>').attr("data-partner-id", _current.partnerId).text("всего").appendTo(_theadRow2);
				$('<th>').attr("data-partner-id", _current.partnerId).text("занято").appendTo(_theadRow2);
				$('<th>').attr("data-partner-id", _current.partnerId).text("свободно").appendTo(_theadRow2);
				
				_tfootRow.append('<td>').append('<td>').append('<td>');
				
				if(_currentCampaign.isAuto) {
					for(_i in _races) {
						for(_j = 0; _j < _modelCount; _j ++) {
							
						}
					}
				}
				else {
					_rows.each(function(){
						var _r = $(this),
							_raceId = _r.attr("data-race-id"),
							_lastCollumn = $("td", _r).last(),
							_currentFromRace = _currentAction.action.quotesMethod == 1 ? _races[_raceId].partnersTypes.dealers : _races[_raceId]["dealers"][_current.partnerId];
						if(_currentFromRace === undefined) {
							_currentFromRace = {};
						}
						
						_th = $('<td>')	.attr("data-partner-id", _current.partnerId)
									.attr("data-field-name", "allQuotes")
									.insertBefore(_lastCollumn);
						if(_currentAction.action.quotesMethod == 1) {
							_th.html(_available = "&mdash;");
							_busy = _races[_raceId]["dealers"] && _races[_raceId]["dealers"][_current.partnerId] && _races[_raceId]["dealers"][_current.partnerId].busyQuote ? _races[_raceId]["dealers"][_current.partnerId].busyQuote : 0;
						}
						else {
							_ele = $('<input type="text" name="racesDealersQuotes[' + _raceId + '][' + _current.partnerId + ']" data-field-name="racesDealersQuotes">').val(_currentFromRace.quote === undefined ? "" : _currentFromRace.quote);
							_th.append(_ele.bind("change", _changeDealerRacesQuotes));
							_form = bstdData.getForm("actions");
							_form.addElement(_ele);
							_available = _currentFromRace.availableQuote || 0;
							_busy = _currentFromRace.busyQuote || 0;
						}
						$('<td>').attr("data-partner-id", _current.partnerId).attr("data-field-name", "busyQuotes").insertBefore(_lastCollumn).html(_busy);
						$('<td>').attr("data-partner-id", _current.partnerId).attr("data-field-name", "availableQuotes").insertBefore(_lastCollumn).html(_available);
						//Изменение квот дилеров в заездах
					});
				}
		};

		
		this.create = function(partnerType) {
			var _tbody = $("<tbody>"),
				_thead = $("<thead>"),
				_tfoot = $("<tfoot>"),
				_theadRow1 = $("<tr>"),
				_theadRow2 = $("<tr>"),
				_tbodyRow,
				_tfootRow = $("<tr>").appendTo(_tfoot),
				_row,
				_ele,
				_val,
				_currentTestDrive = _currentAction.testDrivesObj[_getCurrentTestDrive()],
				_racesCount = _currentTestDrive.racesCount,
				_races = _currentTestDrive.races,
				_current,
				_modelCount = _currentTestDrive.testDriveCarModelsCount,
				_i,
				_j,
				_k,
				_len,
				_isAuto = _currentCampaign.campaignUseCars == 1,
				_partners = _currentAction.dealersObj,
				_th;
			
			_table = $('<table id="dealersInRacesTable">').attr("data-partner-type", partnerType).addClass("display").append(_thead).append(_tbody).append(_tfoot);


			//Собираем первый столбец

			_thead.append(_theadRow1).append(_theadRow2);
			_theadRow1.append(_th = $("<th>"));
			_th.attr('data-field', 'raceName').attr('rowspan', '2').text("Заезды");
			_tfootRow.append(_th = $("<td>").text("Итого"));
			if(_isAuto) {
				_th.attr('colspan="2"');
				_theadRow1.append($("<th>").attr('data-field', 'carName').attr('rowspan', '2').text("Модель"));
			}
			_tfootRow.append($('<td>'));
			
			
			if(_currentAction.action.quotesMethod == 1) {
				_theadRow1.append($("<th>").attr('data-field', 'raceName').attr('rowspan', '2').text("Общие квоты"));
				
				_tfootRow.append($('<td>'));
			}
			
			
			_theadRow1.append(_th = $("<th>"));
			_th.attr('rowspan', '2').text("Итого");
			
			//Собираем tbody
			if(_currentCampaign.isAuto) {
				for(_i in _races) {
					for(_j = 0; _j < _modelCount; _j ++) {
						 
					}
				}
			}
			else {
				for(_i in _races) {
					_current = _races[_i];
					_tbody.append(_tr = $('<tr data-race-id="' + _i + '">').append($('<td>').text(_current.raceStart)));
					if(_currentAction.action.quotesMethod == 1) {
						_tr.append(_th = $("<th>"));
						_ele = $('<input type="text" name="racesDealersQuotes[' + _current.raceId + '][dealers]" data-field-name="racesDealersQuotes">')
								.val(_current.partnersTypes.dealers.quote === undefined ? "" : _current.partnersTypes.dealers.quote)
								.bind("change", _changeDealerRacesQuotes);
						_th.append(_ele);
						_form = bstdData.getForm("actions");
						_form.addElement(_ele);
					}
					_tr.append($('<td data-race-sum-quotes="' + _i + '">').text(0))
				}
			}
			
			if(_currentAction.dealersCount) {
				for(_i in _partners) {
					tThis.addPartner(_table, _currentAction.dealersObj[_i]);
				}
			}
			
			$("#racesContainerForDealers").append(_table).show();
			
		};


		this.removeCar = function(carId, carData) {
			
		};

		this.addCar = function(carId, carData) {
			
		};

		this.removeDealer = function(partnerId, partnerData) {
			
		};

		this.addDealer = function(partnerId, partnerData) {
			
		};

		this.removePartner = function(partnerId, partnerData) {
			
		};
	};
	
	$(document).ready(function(){
		var _c = $("#campaignContainer");
		_currentCampaign = {
			campaignId: _c.attr("data-campaign-id"),
			campaignStatusId: _c.attr("data-campaign-status"),
			campaignUseCars: _c.attr("data-campaign-use-cars"),
			campaignUseAgency: _c.attr("data-campaign-use-agency")
		};
		
		bstdData.init(_dataTypeSets);
		$("[data-button-action=view]").bind("click", sThis.loadAction).removeAttr("data-button-action");
		//$("#dealerId").attr("disabled", false);
	});
};