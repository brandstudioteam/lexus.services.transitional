var _tableDefParams = {
		"bJQueryUI":			true,
		"sPaginationType":		"full_numbers",
		"bStateSave":			true,
		"oLanguage":{
		    "sProcessing":			"Загрузка...",
		    "sLengthMenu":			"Показать _MENU_ записей",
		    "sZeroRecords":			"Нет записей для отображения",
		    "sInfo":				"Показано от _START_ до _END_ из _TOTAL_ записей",
		    "sInfoEmpty":			"Показано от 0 до 0 из 0 записей",
		    "sInfoFiltered":		"(отфильтровано из _MAX_ записей)",
		    "sSearch":				"Поиск:",
		    "oPaginate": {
		        "sFirst":				"Первая",
		        "sPrevious":			"Предыдущая",
		        "sNext":				"Следующая",
		        "sLast":				"Последняя"
		    }		
		}
	},
	validation_url = '',
	list_url = '',
	upload_a_file_string = 'Загрузить файл',
	message_alert_edit_form = "Внесенные изменения не сохранены.\nВы уверены, что хотите закрыть форму?",
	message_update_error = "При сохранении информации произошла ошибка.",
	_tableActionButtonTemplate = '<a data-button-action="#buttonActionName#" href="javascript:void(0);" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">&nbsp;#buttonActionTitle#</span></a>';

var bstdData = new function() {
	var sThis = this;
	var _tables = {},
		_sets,
		_formsTemplates = {},
		_forms = {},
		_popup;
	
	this.setAction = function(ele, action) {
		//ele = $(ele).attr("data-type", action);
		_doActions.apply(ele, [null, action]);
	};
	
	var _doActions = function(e, action) {
		var _ele = $(this),
			_tr = _ele.parent().parent(),
			_dataType = _ele.attr("data-type") ||_tr.parent().parent().attr("data-type"),
			_actionName = action || _ele.attr("data-button-action"),
			_dataId = _tr.attr("data-id"),
			_currSets,
			_currAction,
			_currOTable = _tables[_dataType],
			_dataTitle,
			_table;

		if(!_actionName || !_dataType) {
			return false;
		}

		_currSets = _sets[_dataType];
		_table = _tables[_dataType];
		switch(_actionName) {
			case "add":
				_currAction = _currSets["actions"]["basic"]["add"];
				switch(_currAction.mode) {
					case "reload":
						location.href = _currAction["url"];
						return;
					case "popup":
					default:
						_openForm("add", _currSets, _dataType);
				}
				break;
			case "delete":
				_currAction = _currSets["actions"]["basic"]["delete"];
				_dataTitle = _dataTitle = _currOTable.fnGetData(_table.getRowIndex(_dataId), _currSets.table.fieldsIndex[_currSets.titleFields]);
				if(!confirm(_currAction["confirm"].replace("#title#", _dataTitle))) {
					return false;
				}
				
				$.ajax({
					url: 		_currAction["url"].replace("#id#", _dataId),
					type:		"GET",
					dataType:	'json',
					complete:	function(id, table) {
						return function(result) {
							result = bstdSystem.prepareResult(result);
							if(result && !result.error) {
								table.deleteRecord(id);
							}
							else {
								alert(_currAction["error"] ? _currAction["error"].replace("#title#", _dataTitle) : "При попытке удаления возникла ошибка");
							}
						};
					}(_dataId, _currOTable),
					error: function() {
						alert(_currAction["error"] ? _currAction["error"].replace("#title#", _dataTitle) : "При попытке удаления возникла ошибка");
					}
				});
				break;
			case "view":
				_currAction = _currSets["actions"]["basic"]["view"];
				switch(_currAction.mode) {
					case "reload":
						location.href = _currAction["url"].replace("#id#", _dataId);
						return;
					case "popup":
					default:
						_dataTitle = _currOTable.fnGetData(_table.getRowIndex(_dataId), _currSets.table.fieldsIndex[_currSets.titleFields]);
						_openForm("view", _currSets, "actions", _dataId);
				}
				break;
			case "edit":
				/*
				_currAction = _currSets["actions"]["basic"]["edit"];
				switch(_currAction.mode) {
					case "reload":
						location.href = _currAction["url"];
						return;
					case "popup":
					default:
						_dataTitle = _currOTable.fnGetData(_table.getRecordByDataId(_dataId), _currSets.table.fieldsIndex[_currSets.titleFields]);
						_openForm("edit", _currSets);
				}
				break;
				*/
			case "load":
				/*
				_currAction = _currSets["actions"]["basic"]["load"];
				switch(_currAction.mode) {
					case "reload":
						location.href = _currAction["url"];
						return;
					case "popup":
					default:
						_dataTitle = _currOTable.fnGetData(_table.getRecordByDataId(_dataId), _currSets.table.fieldsIndex[_currSets.titleFields]);
						_openForm("edit", _currSets);
				}
				break;
				*/
			case "search":
				/*
				_currAction = _currSets["actions"]["basic"]["edit"];
				switch(_currAction.mode) {
					case "reload":
						location.href = _currAction["url"];
						return;
					case "popup":
					default:
						_dataTitle = _currOTable.fnGetData(_table.getRecordByDataId(_dataId), _currSets.table.fieldsIndex[_currSets.titleFields]);
						_openForm("edit", _currSets);
				}
				break;
				*/
			default:
				
		}
		return false;
	};
	
	var _initTable = function(table, dataType) {
		var _table = $(table).dataTable(_tableDefParams);
		if(!_table.length) {
			return false;
		};
		_tables[dataType] = _table.attr("data-type", dataType);

		$('.fg-toolbar').width(parseInt(_table.width())-12);
		
		_table.getRecordByDataId = function(id) {
			return $("tr[data-id=" + id + "]", _table);
		};
		_table.getRowIndex = function(id) {
			//var _d = $("tbody tr", _currOTable).get(0).rowIndex;
			return _table.getRecordByDataId(id).get(0)._DT_RowIndex;
		};
		_table.deleteRecord = function(id) {
			_table.fnDeleteRow(_table.getRowIndex(id));
		};
		_table.addRecord = function(recordArray) {
			_table.fnAddData(recordArray);
		};
		_table.getFieldsNames = function() {
			var _fields = [];
			$("thead th", _table).each(function() {
				var _fieldName = $(this).attr("data-field-name");
				if(_fieldName !== undefined) {
					_fields.push(_fieldName);
				};
			});
			return _fields;
		};
		//$("[data-button-action]", table).bind("click", _doActions);
		return _table;
	};

	var _openForm = function(mode, currSets, dataType, dataId) {
		var _formMode,
			_title,
			_form,
			_currMode,
			_formObj;

		if(mode == "view") {
			_formMode = 3;
		}
		else if(mode == "add") {
			_formMode = 1;
		}
		else if(mode == "edit") {
			_formMode = 2;
		}
		else {
			return false;
		}
		_currMode = currSets.actions.basic[mode];

		_title = _currMode.form["title"] || "";
		
		_form = _formsTemplates[dataType].clone(true).show();
		_formObj = new bstdForm(_form, _formMode,dataType);
		_forms[dataType] = _formObj;
		_form.bind("afterClose", function(e, data){
			delete(_forms[data.dataType]);
		});
		_popup = new bstdPopup(_title, _form, null, {onClose: _formObj.close}, true);
		$(_form).bind("succesfullSubmiting", _savedData);
		$(_form).bind("resize", _popup.resize);
		if(_currMode.form.onInit) {
			_currMode.form.onInit(_formObj, dataId);
		}
		return false;
	};

	var _savedData = function(e, data) {
		//{form:form, result:response.result, sendData: _sendData, formObj: sThis, dataType: dataType, mode: mode}
		var _idFields;
		if(data) {
			switch(data.mode) {
				case bstdForm.prototype.MODE_ADD:
					_idFields = _sets[data.dataType].idFields;
					data.sendData[_idFields] = typeof data.result == "object" ? data.result[_idFields] : data.result;
					sThis.addData(data.sendData, data.dataType);
					_popup.close();
					break;
				case bstdForm.prototype.MODE_EDIT:
					sThis.updateData();
					break;
				default:
					return false;
			}
		}
	};

	var _prepareData = function(data, dataType) {
	};

	
	this.addData = function(data, dataType) {
		var _table = _sets[dataType].table,
			_fields = _table.fields,
			_len = _fields.length,
			_i,
			_r = [],
			_field,
			_template,
			_actions = _sets[dataType].actions.basic,
			_actionsC = _sets[dataType].actions.custom,
			_tableActions = _table.actions,
			_action;

		for(_i = 0; _i < _len; _i ++) {
			_field = data[_fields[_i]];
			_r.push(_field === undefined ? "&nbsp;" : _field);
		}
		
		if(_table.actions) {
			_len = _tableActions.length;
			_template = _table.actionTemplate || _tableActionButtonTemplate;
			_field = "";
			for(_i = 0; _i < _len; _i ++) {
				_action = _actions[_tableActions[_i]] || _actionsC[_tableActions[_i]];
				if(!_action) {
					continue;
				}
				_field += _template.replace("#buttonActionName#", _tableActions[_i], "g").replace("#buttonActionTitle#", _action.title, "g");
			}
			_r.push(_field === undefined ? "&nbsp;" : _field);
		}
		
		_fields = _tables[dataType].fnAddData(_r);
		_len = 10;
	};

	this.updateData = function(dataType, data) {
		
	};
	
	this.getForm = function(dataType) {
		return _forms[dataType];
	};
	
	this.getTable = function(dataType) {
		return _tables[dataType];
	};
	
	this.init = function(dataTypeSets) {
		var _i,
			_j,
			_item,
			_len,
			_current;
		
		if(typeof dataTypeSets === undefined) {
			return false;
		}
		_sets = dataTypeSets;
		for(_i in dataTypeSets) {
			_item = dataTypeSets[_i];
			if(_item.table) {
				_current = _item.table;
				_initTable("#" + _current["id"], _i);
				if(!_current["fieldsIndex"]) {
					_len = _current["fields"].length;
					_current["fieldsIndex"] = {};
					for(_j = 0; _j < _len; _j ++) {
						_current["fieldsIndex"][_current["fields"][_j]] = _j;
					}
				}
			}
			if(_item.form) {
				_current = _item.form;
				_formsTemplates[_i] = $("#" + _current.id).remove();
			}
		}
		$("[data-button-action]").live("click", _doActions);
	};
};

var bstdDate = {
	sec2Time: function(sec) {
		var _t = sec / 3600,
			_h = parseInt(_t) || 0,
			_d = _h * 3600,
			_e = (sec - _d) / 60,
			_m = parseInt(_e) || 0,
			_s = (sec - _d - _m * 60) || 0;
		return (_h < 10 ? "0" : "") + _h + ":" + (_m < 10 ? "0" : "") + _m + ":" + (_s < 10 ? "0" : "") + _s;
	},
	time2Sec: function(time) {
		time = time.replace(":", "", "g").split("");
		return time ? parseInt((time[0] * 10 + parseInt(time[1])) * 3600 + (time[2] * 10 + parseInt(time[3])) * 60 + (time[4] * 10 + parseInt(time[5])) * 60) : 0;
	}
};