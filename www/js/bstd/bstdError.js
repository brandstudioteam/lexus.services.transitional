var bstdError = function(message, code, DOMElement, targetObject) {
	this.getMessage = function() {
		return message;
	}
	
	this.getCode = function() {
		return code;
	}
	
	this.getDOMElement = function() {
		return DOMElement;
	}
	
	this.getObject = function() {
		return targetObject;
	}
}