var bstdNumberPanel = new function() {
	var sThis = this,
		_template = {a:'<div class="bstd-number_panel"><div class="bstd-number_panel-row"><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="7">7</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="8">8</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="9">9</div><div class="clear"></div></div>' +
				'<div class="bstd-number_panel-row"><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="4">4</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="5">5</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="6">6</div><div class="clear"></div></div>' +
				'<div class="bstd-number_panel-row"><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="1">1</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="2">2</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="3">3</div><div class="clear"></div></div>' +
				'<div class="bstd-number-panel-button" data-number="0">0</div></div>',
				b: '<div class="bstd-number_panel"><div class="float-letf"><div class="bstd-number_panel-row"><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="7">7</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="8">8</div>' +
				'<div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="9">9</div><div class="clear"></div></div><div class="bstd-number_panel-row"><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="4">4</div>' +
				'<div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="5">5</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="6">6</div><div class="clear"></div></div><div class="bstd-number_panel-row">' +
				'<div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="1">1</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="2">2</div><div class="bstd-number-panel-button bstd-number-panel-button-notnull" data-number="3">3</div>' +
				'<div class="clear"></div></div></div><div class="float-letf"><div class="bstd-number-panel-button bstd-number_panel-vnull" data-number="0">0</div></div><div class="clear"></div></div>'
		},
	_panel;

	this.register = function(ele, params) {
		ele = $(ele);
		if(!ele.length) {
			return false;
		}
		ele.bind("focus", function(params) {
			return function() {
				sThis.open(params);
			};
		}(params));
	};

	this.open = function(params) {
		var _max,
			_min,
			_offset,
			_left,
			_top,
			_controlElement,
			_valueElement;
		
		if(params) {
			if(params.controlElement) {
				_controlElement = $(params.controlElement);
				_offset = _controlElement.offset();
			}
			if(params.valueElement) {
				_valueElement = $(params.valueElement);
			}
			if(params.left !== undefined) {
				_left = params.left;
			}
			else if(_offset && _offset.left !== undefined){
				_left = _offset.left;
			}
			if(params.top !== undefined) {
				_top = params.top;
			}
			else if(_offset && _offset.top !== undefined){
				_top = _offset.top + _controlElement.outerHeight(true);
			}
		}
		
		$("body").append(_panel = $(_template[params && params.type ? params.type : "a"]));

		if(params) {
			if(params.max !== undefined || params.min !== undefined) {
				if(params.max !== undefined) {
					_max = params.max;
				}
				if(params.min !== undefined) {
					_min = params.min;
				}
				$(".bstd-number-panel-button", _panel).each(function() {
					var _ele = $(this),
						_val = _ele.attr("data-number");
					if(_max !== undefined && _min !== undefined) {
						if(_val > _max || _val < _min) {
							_ele.addClass("bstd-number-panel-disabled");
						}
						else {
							_ele.removeClass("bstd-number-panel-disabled");
						}
					}
					else if(_min !== undefined) {
						if(_val < _min) {
							_ele.addClass("bstd-number-panel-disabled");
						}
						else {
							_ele.removeClass("bstd-number-panel-disabled");
						}
					}
					else {
						if(_val > _max) {
							_ele.addClass("bstd-number-panel-disabled");
						}
						else {
							_ele.removeClass("bstd-number-panel-disabled");
						}
					}
				});
			}
		}
		
		_panel
				.offset({top: _top, left: _left})
				.show()
				.bind("mousedown", function(e) {
					var _ele = $(e.target);
					if(_ele.hasClass("bstd-number-panel-disabled")) {
						return;
					}
					_ele.addClass("select");
				})
				.bind("mouseup", function(e) {
						var _ele = $(e.target),
							_val;
						if(_ele.hasClass("bstd-number-panel-disabled")) {
							return;
						}
						_val = _ele.removeClass("select").text();
						$(_valueElement).val(_val);
						_panel.remove();
						$(_controlElement || "body").trigger("bstdNumberPanelSelect", {curretn: _val});
					});
		
	};
	
	this.close = function() {
		_panel.remove();
	};
};

var bstdTimeTablo = function(ele) {
	var sThis = this,
		_template = '<div class="bstd-tablo-a"><div class="bstd-tablo-panel bstd-tablo-left"><input type="text" class="bstd-tablo-input" maxlength="1" /><input type="text" class="bstd-tablo-input" maxlength="1" /><div class="clear"></div></div>' +
					'<div class="bstd-tablo-panel bstd-tablo-separator">:</div><div class="bstd-tablo-panel bstd-tablo-center"><input type="text" class="bstd-tablo-input" maxlength="1" /><input type="text" class="bstd-tablo-input" maxlength="1" /><div class="clear"></div></div>' +
					'<div class="bstd-tablo-panel bstd-tablo-separator">:</div><div class="bstd-tablo-panel bstd-tablo-right"><input type="text" class="bstd-tablo-input" maxlength="1" /><input type="text" class="bstd-tablo-input" maxlength="1" /><div class="clear"></div></div>' +
					'<div class="clear"></div></div>',
		_tablo,
		_current,
		_coll,
		_oldValue,
		_name;
	
	ele = $(ele);
	if(!ele.length) {
		return false;
	}
	_oldValue = ele.val();
	_name = ele.attr("bstd-name");
	if(!_name) {
		_name = ele.attr("name");
	}
	
	this.ignored = ele.attr("bstd-form-ignore");
	this.isChange = false;
	this.name = _name;
	this.required = !!(ele.attr("bstd-required") !== undefined ? ele.attr("bstd-required") : ele.attr("required") !== undefined ? ele.attr("required") : false);
	this.type = "bstdTimeTablo";
	
	this.onChange = function(callback) {
		if(typeof callback == "function") {
			_ele.bind("change", callback);
		}
		return sThis;
	};
	
	this.val = function(value) {
		var _val;
		if(value !== undefined) {
			if(!value) {
				ele.val("");
				if(ele.val() == _oldValue) {
					sThis.isChange == false;
				}
			}
			else {
				_val = /(\d)(\d):(\d)(\d):(\d)(\d)/.exec(value);
				if(_val) {
					ele.val(_val[1] + _val[2] + ":" + _val[3] + _val[4] + ":" + _val[5] + _val[6]);
					sThis.isChange == true;
				}
			}
			return sThis;
		}
		else {
			return ele.val();
		}
	};
	
	this.open = function() {
		var _offset,
			_oVal,
			_i;
		
		var _getMaxVal = function() {
			var _max;
			switch(_current) {
				case 0:
					_max = 2;
					break;
				case 1:
					_max = $(_coll.get(0)).text() == 2 ? 4 : 9;
					break;
				case 2:
				case 4:
					_max = 5;
					break;
				case 3:
				case 5:
					_max = 9;
					break;
			}
			return _max;
		};
		
		if(_tablo) {
			_tablo.remove();
			bstdNumberPanel.close();
		}

		_offset = ele.offset();
		$("body").append(_tablo = $(_template));
		_coll = $("input", _tablo);
		if((_oVal = /(\d)(\d):(\d)(\d):(\d)(\d)/.exec(ele.val()))) {
			_oVal.shift();
			_i = 0;
			_coll.each(function() {
				$(this).val(_oVal[_i]);
				_i ++;
			});
		}
		else {
			_coll.val('');
		}
		
		_tablo.offset({top: _offset.top + ele.outerHeight(true), left: _offset.left}).show();
		bstdNumberPanel.open({type:"b", controlElement: _tablo, valueElement: _coll.attr("disabled", true).first().attr("disabled", false).addClass("select"), max: 2});
		_current = 0;
		$(_tablo).bind("bstdNumberPanelSelect", function() {
			var _val;
			
			$(_coll.get(_current)).removeClass("select").attr("disabled", true);
			_current ++;
			if(_current == 6) {
				_val = $(_coll.get(0)).val() + $(_coll.get(1)).val() + ":" +  $(_coll.get(2)).val() + $(_coll.get(3)).val() + ":" + $(_coll.get(4)).val() + $(_coll.get(5)).val();
				_tablo.remove();
				ele.val(_val);
				ele.trigger("bstdTimeTabloSelect", {value: _val}).trigger("change", {value: _val});
				return;
			}
			bstdNumberPanel.open({type:"b", controlElement: _tablo, valueElement: $(_coll.get(_current)).attr("disabled", false).addClass("select"), max: _getMaxVal()});
		});
	};
	
	this.validate = function() {
		return true;
	};
	
	this.name = function() {
		return _name;
	};
	
	this.clear = function() {
		ele.val("");
		return sThis;
	};
	
	this.reset = function() {
		ele.val(_oldValue);
		return sThis;
	};
	ele.bind("focus", sThis.open);
};

if(window["bstdElementsTypes"]) {
	bstdElementsTypes.registerElementsType("bstdTimeTablo", bstdTimeTablo);
}