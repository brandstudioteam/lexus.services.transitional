/**
 * @class
 */
var bstdToyotaAPI = (new function() {
	/**
	 * @private
	 */
	var sThis = this;
	
	/**
	 * 
	 * @param response
	 * @param callback
	 * @param errorCallback
	 * @param eventName
	 * @returns
	 */
	var _checkResponse = function(response, callback, errorCallback, eventName) {
		if(response && (response.response || response.responseText)) {
			response = response.response || response.responseText;
			if(typeof response == "string") {
				response = $.parseJSON(response);
			}
		}
		if(response && typeof response == "object") {
			if(response.error) {
				if(typeof errorCallback == "function") errorCallback(response.errorMessage || response.message, response.errors);
				//$.trigger("on" + eventName, {error: response.error, errorMessage: response.errorMessage || response.message, errors: response.errors});
			}
			else {
				callback(response.result || response);
				//$.trigger("on" + eventName, response.result);
			}
		}
		else {
			if(typeof errorCallback == "function") if(typeof errorCallback == "function") errorCallback(null);
			//$.trigger("on" + eventName, {error: "LoadError"});
		}
	};
	
	/**
	 * @private
	 * @param url
	 * @param data
	 * @param callback
	 * @param errorCallback
	 * @param eventName
	 * @returns
	 */
	var _send = function(url, data, callback, errorCallback, eventName) {
		var _fn = "jsonpF_" + Math.random().toString().split(".")[1];
		window[_fn] = (function(callback, errorCallback, eventName) {
			return function(response) {
				_checkResponse(response, callback, errorCallback, eventName);
			};
		}(callback, errorCallback, eventName));
		
		$.ajax({
		    url: url,
		    data: data,
		    jsonpCallback: _fn,
		    dataType: "jsonp"
		});
	};
	
	
	/**
	 * @class
	 * @public
	 */
	this.geo = (new function() {
		var oThis = this,
			_url = "http://promo.toyota.ru/z/geo/";
		
		/**
		 * Получить список стран, в которых есть дилеры, поиск страны, в которой есть дилеры, по строке
		 * @public
		 * @param {String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getCountries = function(findText, callback, errorCallback) {
			_send(_url + "countries", findText ? {find: findText} : null, callback, errorCallback, "GetCountries");
		};
		
		/**
		 * Получить список стран, в которых есть дилеры
		 * @public
		 * @param {String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersCountries = function(findText, callback, errorCallback) {
			_send(_url + "dealerCountries", findText ? {find: findText} : null, callback, errorCallback, "GetDealersCountries");
		};
		
		/**
		 * Получить список  регионов для заданной страны, поиск региона по строке
		 * @public
		 * @param {int} countryId - Необязательный. Идентификатор страны
		 * @param {String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 
		 * Внимание! Один из аргументов countryId или findText должен быть задан
		 */
		this.getRegion = function(countryId, findText, callback, errorCallback) {
			var data = {};
			if(!countryId && !findText)
					return false;
			
			if(findText)
				data["findText"] = findText;
			if(countryId)
				data["countryId"] = countryId;
			
			_send(_url + "regions", data, callback, errorCallback, "GetRegion");
		};
		
		/**
		 * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
		 * @public
		 * @param {int} countryId - Необязательный. Идентификатор страны
		 * @param {String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersRegion = function(countryId, findText, facilityId, callback, errorCallback) {
			var data = {};
			
			if(findText)
				data["findText"] = findText;
			if(countryId)
				data["countryId"] = countryId;
			
			_send(_url + "dealerRegions", data, callback, errorCallback, "GetDealersRegion");
		};
		
		/**
		 * Получить список всех городов для заданного региона, поиск города по строке
		 * @public
		 * @param {int} regionId - Необязательный. Идентификатор региона
		 * @param {String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * 
		 * Внимание! Один из аргументов regionId или findText должен быть задан
		 */
		this.getCities = function(regionId, findText, callback, errorCallback) {
			var data = {};
			if(!regionId && !findText)
					return false;
			
			if(findText)
				data["findText"] = findText;
			if(regionId)
				data["regionId"] = regionId;
			
			_send(_url + "cities", data, callback, errorCallback, "GetCities");
		};
		
		/**
		 * Получить список городов, в которых есть дилеры для заданной страны или региона, поиск города, в котором есть дилеры по строке
		 * @public
		 * @param {int} regionId - Необязательный. Идентификатор региона
		 * @param {int} countryId - Необязательный. Идентификатор страны
		 * @param {String} findText - Необязательный. Строка поиска
		 * @param {Function} callback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 * @param {Function} errorCallback - Функция обратного вызова. Будет вызвана после загрузки данных.
		 */
		this.getDealersCities = function(regionId, countryId, findText, facilityId, callback, errorCallback) {
			var data = {};
			
			if(findText)
				data["findText"] = findText;
			if(regionId)
				data["regionId"] = regionId;
			if(countryId)
				data["countryId"] = countryId;
			if(facilityId)
				data["facilityId"] = facilityId;
			
			_send(_url + "dealerCities", data, callback, errorCallback, "GetDealersCities");
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.getNearestCities = function(count, facilityId, callback, errorCallback) {
			var data = {};
			
			if(count)
				data["count"] = count;
			if(facilityId)
				data["facilityId"] = facilityId;
			
			_send(_url + "dealerCities", data, callback, errorCallback, "GetNearestCities");
		};
		
		
		this.getFederalDistinct = function(countryId, callback, errorCallback) {
			if(countryId && countryId != 242) {
				callback([]);
				return;
			}
			_send(_url + "federalDistinct", {}, callback, errorCallback, "GetFederalDistinct");
		};
		
		this.getDealersFederalDistinct = function(countryId, callback, errorCallback) {
			if(countryId && countryId != 242) {
				callback([]);
				return;
			}
			_send(_url + "dealersFederalDistinct", {}, callback, errorCallback, "GetDealersFederalDistinct");
		};
	});
	
	/**
	 * @class
	 * @public
	 */
	this.dealers = (new function(){
		var oThis = this,
			_url = "http://promo.toyota.ru/z/dealers/";
		
		this.getDealers = function(cityId, findText, facilityId, callback, errorCallback) {
			var data = {};
			
			if(findText)
				data["findText"] = findText;
			if(cityId)
				data["cityId"] = cityId;
			if(facilityId)
				data["facilityId"] = facilityId;
			
			_send(_url + (cityId ? "/" + cityId : ""), data, callback, errorCallback, "GetDealers");
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.getNearestDealers = function(count, citiesCount, facilityId, callback, errorCallback) {
			var data = {};
			
			if(count)
				data["count"] = count;
			if(citiesCount)
				data["citiesCount"] = citiesCount;
			if(facilityId)
				data["facilityId"] = facilityId;
			
			_send(_url + "nearest", data, callback, errorCallback, "GetNearestDealers");
		};
		
		this.getDealersTypes = function(callback, errorCallback) {
			_send(_url + "types/", {}, callback, errorCallback, "GetDealers");
		};
	});
	
	/**
	 * @class
	 * @public
	 */
	this.cars = (new function(){
		var oThis = this,
			_url = "http://promo.toyota.ru/z/cars/";
		
		/**
		 * Получить список моделей автомобилей Toyota или поиск модели  Toyota
		 */
		this.getToyotaCars = function(findText, callback, errorCallback) {
			var data = {};
			
			if(findText)
				data["findText"] = findText;
			
			_send(_url, data, callback, errorCallback, "GetToyotaCars");
		};
		/**
		 * Получить список всех сторонних производителей автомобилей или поиск стороннего производителя автомобилей
		 */
		this.getManufacturers = function(findText, callback, errorCallback) {
			var data = {};
			
			if(findText)
				data["findText"] = findText;
			
			_send(_url + "manufacturers", data, callback, errorCallback, "GetManufacturers");
		};
		/**
		 * Получить список моделей автомобилей для стороннего производителя или поиск модели автомобиля стороннего производителя
		 */
		this.getCars = function(manufacturerId, findText, callback, errorCallback) {
			var data = {};
			
			if(manufacturerId)
				data["manufacturerId"] = manufacturerId;
			if(findText)
				data["findText"] = findText;
			
			_send(_url + "models", data, callback, errorCallback, "GetCars");
		};
	});
	
	this.facilities = (new function() {
		var oThis = this,
		_url = "http://contacts.toyota.ru/z/facilities/";
		
		this.getFacilities = function(callback, errorCallback) {
			_send(_url, {}, callback, errorCallback, "GetFacilities");
		};
		
		this.getFacilitiesDealersRelated = function(dealer, callback, errorCallback) {
			var data = dealer ? {dealer:dealer} : {};
			_send(_url + "dealers_related", data, callback, errorCallback, "GetFacilities");
		};
		
		this.getFacilitiesDealersNotRelated = function(dealer, callback, errorCallback) {
			if(!dealer) {
				return false;
			}
			_send(_url + "dealers_not_related", {dealer:dealer}, callback, errorCallback, "GetFacilities");
		};
	});
	
	
	
	
	this.UI = (new function() {
		var uThis = this;
		
		var _createOptions = function(select, data, callback, eventName, idFiled, nameField, selected) {
			var _i,
				_ele,
				_items = [],
				_item;
			
			if(!idFiled) {
				idFiled = "id";
			}
			if(!nameField) {
				nameField = "title";
			}
			
			select = $(select).empty();
			if(data) {
				if(data.length > 1) {
					_item = $('<option disabled selected></option>');
					_items.push(_item);
					if(select.length) {
						select.append(_item);
					}
				}
				for(i = 0; i < data.length; i++) {
					_item = $('<option' + (selected !== undefined && data[i][idFiled] == selected ? ' selected="selected"' : '') + ' value="' + data[i][idFiled] + '">' + data[i][nameField] + '</option>');
					_item[0].bstdData = data[i];
					_items.push(_item);
					if(select.length) {
						select.append(_item);
					}
				}
				if(select.length) {
					select.attr("disabled", false);
					if(callback) {
						callback(data.length);
					}
				}
				else {
					if(callback) {
						callback(_items);
					}
				}
				if(!callback) {
					select.trigger("on" + eventName + "OptionCreate", {items:_items});
				}
			}
		};

		
		var _prepareResponse = function(response, select, callback, errorCallback, eventName, idFiled, nameField) {
			_createOptions(select, response, callback, eventName, idFiled, nameField);
		};
		
		
		this.createCountries = function(findText, select, callback, errorCallback, selected) {
			sThis.geo.getCountries(findText, function(select, callback, errorCallback) {
					return function(response) {
						_prepareResponse(response, select, callback, errorCallback, "Cuntries", "countryId", "countryName", selected);
					};
				}(select, callback, errorCallback),  errorCallback);
		};
		
		this.createDealersCountries = function(findText, select, callback, errorCallback, selected) {
			sThis.geo.getDealersCountries(findText, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "DealersCountries", "countryId", "countryName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		
		this.createRegion = function(countryId, findText, select, callback, errorCallback, selected) {
			sThis.geo.getRegion(countryId, findText,  function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "Region", "regionId", "regionName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		this.createDealersRegion = function(countryId, findText, facilityId, select, callback, errorCallback, selected) {
			sThis.geo.getDealersRegion(countryId, findText, facilityId,  function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "DealersRegion", "regionId", "regionName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		//federalDistinct
		this.createFederalDistinct = function(countryId, select, callback, errorCallback, selected) {
			sThis.geo.getFederalDistinct(countryId, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "FederalDistinct", "federalDistrictId", "federalDistrictName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		this.createDealersFederalDistinct = function(countryId, select, callback, errorCallback, selected) {
			sThis.geo.getDealersFederalDistinct(countryId, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "DealersFederalDistinct", "federalDistrictId", "federalDistrictName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		
		
		
		this.createCities = function(regionId, findText, select, callback, errorCallback, selected) {
			sThis.geo.getCities(regionId, findText, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "Cities", "cityId", "cityName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		/**
		 * Получить список городов, в которых есть дилеры
		 * @public
		 */
		this.createDealersCities = function(regionId, countryId, findText, facilityId, select, callback, errorCallback, selected) {
			sThis.geo.getDealersCities(regionId, countryId, findText, facilityId, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "DealersCities", "cityId", "cityName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.createNearestCities = function(count, facilityId, select, callback, errorCallback, selected) {
			sThis.geo.getNearestCities(count, facilityId, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "NearestCities", "cityId", "cityName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		
		
		
		this.createDealers = function(cityId, findText, facilityId, select, callback, errorCallback, selected) {
			sThis.dealers.getDealers(cityId, findText, facilityId, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "Dealers", "partnerId", "partnerName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		/**
		 * Получить список ближайших городов, в которых есть дилеры, к пользователю
		 * @public
		 * @param {int} count - Необязательный. Количество возвращаемых городов. Если не задан или меньше 1, вернется один город.
		 */
		this.createNearestDealers = function(count, citiesCount, facilityId, select, callback, errorCallback, selected) {
			sThis.dealers.getNearestDealers(count, citiesCount, facilityId, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "NearestDealers", "partnerId", "partnerName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		
		
		
		this.createToyotaCars = function(findText, select, callback, errorCallback) {
			sThis.cars.getToyotaCars(findText, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "Dealers");
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		this.createManufacturers = function(findText, select, callback, errorCallback) {
			sThis.cars.getManufacturers(findText, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "CreateManufacturers");
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		
		this.createCars = function(manufacturerId, findText, select, callback, errorCallback) {
			sThis.cars.getCars(manufacturerId, findText, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "CreateCars");
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		
		this.createGeoSelects = function(countrySelect, regionSelect, citySelect, callback, errorCallback, countrySelected, regionSelected, citySelected) {
			$(countrySelect).attr("disabled", true);
			$(regionSelect).attr("disabled", true);
			$(citySelect).attr("disabled", true);
			uThis.createCountries(null, countrySelect, function(){
				$(countrySelect).attr("disabled", false);
				if(typeof callback == "function") {
					callback({currentTarget: countrySelect, type: "countriesLoaded"});
				}
			});
			$(countrySelect).bind("change", function() {
				$(regionSelect).empty().attr("disabled", true);
				$(citySelect).empty().attr("disabled", true);
				uThis.createRegion($(countrySelect).find("option:selected").val(), null, regionSelect, function(count){
					$(regionSelect).attr("disabled", false);
					if(typeof callback == "function") {
						callback({currentTarget: countrySelect, type: "regionsLoaded"});
					}
					if(count == 1) {
						_createCities();
					}
				});
			});
			
			var _createCities = function() {
				$(citySelect).empty().attr("disabled", true);
				uThis.createCities($(regionSelect).find("option:selected").val(), null, citySelect, function(){
					$(citySelect).attr("disabled", false);
					if(typeof callback == "function") {
						callback({currentTarget: countrySelect, type: "citiesLoaded"});
					}
				});
			};
			
			$(regionSelect).bind("change", _createCities);
		};
		
		this.createFacilitiesRelated = function(dealer, select, callback, errorCallback, selected) {
			sThis.facilities.getFacilitiesDealersRelated(dealer, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "CreateFacilitiesRelated", "facilityId", "facilityName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		this.createFacilitiesNotRelated = function(dealer, select, callback, errorCallback, selected) {
			sThis.facilities.getFacilitiesDealersNotRelated(dealer, function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "CreateFacilitiesNotRelated", "facilityId", "facilityName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
		
		this.createFacilities = function(select, callback, errorCallback, selected) {
			sThis.facilities.getFacilities(function(select, callback, errorCallback) {
				return function(response) {
					_prepareResponse(response, select, callback, errorCallback, "CreateFacilities", "facilityId", "facilityName", selected);
				};
			}(select, callback, errorCallback),  errorCallback);
		};
	});
});