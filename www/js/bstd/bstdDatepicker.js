var bstdDatepicker = function(ele, params) {
	var sThis = this,
		_oldValue,
		_name;
	
	ele = $(ele);
	if(!ele.length) {
		return false;
	}
	_oldValue = ele.val();
	_name = ele.attr("bstd-name");
	if(!_name) {
		_name = ele.attr("name");
	}
	
	this.ignored = ele.attr("bstd-form-ignore");
	this.isChange = false;
	this.name = _name;
	this.required = !!(ele.attr("bstd-required") !== undefined ? ele.attr("bstd-required") : ele.attr("required") !== undefined ? ele.attr("required") : false);
	this.type = "bstdDatepicker";
	this.bstdTipe = "bstdDatepicker";
	
	this.onChange = function(callback) {
		if(typeof callback == "function") {
			_ele.bind("change", callback);
		}
		return sThis;
	};
	
	this.val = function(value) {
		var _val;
		if(value !== undefined) {
			if(!value) {
				ele.val("");
				if(ele.val() == _oldValue) {
					sThis.isChange == false;
				}
			}
			else {
				if(_val = /(?:(\d{2})\.(\d{2})\.(\d{4})|(\d{4})-(\d{2})-(\d{2}))/g.exec(ele.val())) {
					ele.val((_val[1] || _val[6]) + "." + (_val[2] || _val[5]) + "." + (_val[3] || _val[4]));
					sThis.isChange == true;
				}
			}
			return sThis;
		}
		else {
			return ele.val().replace(/(\d{2})\.(\d{2})\.(\d{4})/g, "$3-$2-$1");
		}
	};
	
	this.validate = function() {
		return true;
	};
	
	this.name = function() {
		return _name;
	};
	
	this.clear = function() {
		ele.val("");
		return sThis;
	};
	
	this.reset = function() {
		ele.val(_oldValue);
		return sThis;
	};
	
	if(!params) {
		params = {};
	}
	params.onSelect = function(ele) {
		return function() {
			if(ele.val() == _oldValue) {
				sThis.isChange == false;
			}
			ele.trigger("change", {date: ele.val(), unixDate: ele.val().replace(/(\d{2})\.(\d{2})\.(\d{4})/g, "$3-$2-$1")});
		};
	}(ele);
	ele.datepicker(params);
};

if(window["bstdDatepicker"]) {
	bstdElementsTypes.registerElementsType("bstdDatepicker", bstdDatepicker);
}