var bstdCaptchaSets = {
	controller: "z",
	action: "a"
};

var bstdCaptcha = function(ele, form) {
	var _img = $("#" + ele.id + "_img"),
		sThis = this
		_ele = $(ele)
		_asichValidate = _ele.attr("bstd_captha_validate");
	
	if(!_img[0]) {
		if(_SYS_DEBUG) {
			throw new Error("Image for captha is not found");
		}
		return false;
	}
	
	if(_ele.attr("required") === undefined) {
		_ele.attr("required", "required");
	}
	
	this.change = function() {
		_img.attr("src", _img.attr("src").split("?")[0] + "?r=" + Math.random());
		_ele.val('');
	};
	
	this.validate = function() {
		var _value = _ele.val();
		
		if(!_value) {
			return new bstdError("capthaRequired");
		}
		if(!/[\d\w]+/i.test(_value)) {
			return new bstdError("capthaNotValide");
		}
		if(_asichValidate) {
			$.post(form.bstdCurrentForm.action, "a=" + bstdCaptchaSets.controller + "&b=" + bstdCaptchaSets.action + "captcha=" + _value, function(data) {
				 if(data.error) {
					 sThis.change();
					 return new bstdError("capthaNotValide");
				 }
				 else {
					 return _value;
				 }
			});
			return true;
		}
		else {
			return _value;
		}
	};
	_img.click(sThis.change);
}

bstdForms.registerElementsType("captcha", bstdCaptcha);