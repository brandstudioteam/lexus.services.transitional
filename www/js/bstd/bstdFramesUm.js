window["isLexus"] = /lexus.ru/.test(location.href);

if(!location["params"]) {
	//(function(){var _params=location.href.split("?"),_i,_tmp,_s,_str;location["params"]={};if(_params.length > 1){location["stringParams"]=_str=decodeURIComponent(_params[1].split("#")[0]);_params=location["stringParams"].split("&");for(_i=0;_i < _params.length;_i++){_tmp=new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);if((_s=_tmp[1].indexOf("[]"))!=-1){_tmp[1]=_tmp[1].substr(0, _s);}if(_tmp[2]===undefined){_tmp[2]=null;}if(_tmp[1] in location["params"]){if(location["params"][_tmp[1]] instanceof Array){location["params"][_tmp[1]].push(_tmp[2]);}else location["params"][_tmp[1]]=[location["params"][_tmp[1]], _tmp[2]];}else location["params"][_tmp[1]]=_tmp[2];}}return location["params"];})();
	
	(function() {
		var _params = location.href.split("?"),
			_i,
			_tmp,
			_s,
			_str;
		location["params"] = {};
        location["stringParams"] = '';
		if(_params.length > 1) {
			location["stringParams"] = _str = decodeURIComponent(_params[1].split("#")[0]);
			_params = location["stringParams"].split("&");
			if(_params) {
				for(_i = 0; _i < _params.length; _i++) {
					if(!_params[_i]) {
						continue;
					}
					_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
		            if((_s = _tmp[1].indexOf("[]")) != -1) {
		                _tmp[1] = _tmp[1].substr(0, _s);
		            }
		            if(_tmp[2] === undefined) {
		                _tmp[2] = null;
		            }
					if(_tmp[1] in location["params"]) {
						if(location["params"][_tmp[1]] instanceof Array) {
							location["params"][_tmp[1]].push(_tmp[2]);
						}
						else location["params"][_tmp[1]] = [location["params"][_tmp[1]], _tmp[2]];
					}
					else location["params"][_tmp[1]] = _tmp[2];
				}
			}
		}
		return location["params"];
	})();
};

if(location.params && location.params.id) {
	var _tmpUrl = location.href.split("?");
	_tmpUrl[1] = _tmpUrl[1].replace(/[^\w\d]id=[^&]*/, "").replace(/^id=[^&]*/, "").replace(/^&/, "");
	location.href = _tmpUrl[0] + location.params.id + (_tmpUrl[1] ? "?" + _tmpUrl[1] : "");
}
if(location.hostname.split(".")[0] == "trade") {
	//if(!isLexus) {
		$(document).ready(function(){
			var _tmpParams = location.stringParams.replace(/[^\w\d]id=[^&]*/, "").replace(/^id=[^&]*/, "").replace(/^&/, ""),
				_tmpNextPrevButton = $(".next").first();
			if(_tmpNextPrevButton && _tmpNextPrevButton.length) {
				_tmpNextPrevButton.attr("href", _tmpNextPrevButton.attr("href") + (_tmpParams ? "?" + _tmpParams : ""));
			}
			_tmpNextPrevButton = $(".prev").first();
			if(_tmpNextPrevButton && _tmpNextPrevButton.length) {
				_tmpNextPrevButton.attr("href", _tmpNextPrevButton.attr("href") + (_tmpParams ? "?" + _tmpParams : ""));
			}
			_tmpNextPrevButton = $(".search-menu a");
			if(_tmpNextPrevButton && _tmpNextPrevButton.length) {
				_tmpNextPrevButton.each(function(){
					$(this).attr("href", $(this).attr("href") + (_tmpParams ? "?" + _tmpParams : ""));
				});
			}
		});
	//}
}

//(function($){var interval_id, last_hash, cache_bust=1, rm_callback, window=this, FALSE=!1, postMessage='postMessage', addEventListener='addEventListener', _opera, p_receiveMessage, has_postMessage;_opera=/Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);_opera=!!(_opera && parseFloat(_opera[1])< 9.7);has_postMessage=window[postMessage] && !_opera;$[postMessage]=function(message, target_url, target){if(!target_url){return;}message=typeof message==='string' ? message : $.param(message);target=target || parent;if(has_postMessage){target[postMessage](message, target_url.replace(/([^:]+:\/\/[^\/]+).*/, '$1'));}else if(target_url){target.location=target_url.replace(/#.*$/, '')+ '#' +(+new Date)+(cache_bust++)+ '&' + message;}};$.receiveMessage=p_receiveMessage=function(callback, source_origin, delay){if(has_postMessage){if(callback){rm_callback && p_receiveMessage();rm_callback=function(e){if(e.originalEvent){e=e.originalEvent;}if((typeof source_origin==='string' && source_origin !="*" && e.origin !==source_origin)||($.isFunction(source_origin)&& source_origin(e.origin)===FALSE)){return FALSE;}callback(e);};}$(window).bind('message', rm_callback);}else{interval_id && clearInterval(interval_id);interval_id=null;if(callback){delay=typeof source_origin==='number' ? source_origin : typeof delay==='number' ? delay : 100;interval_id=setInterval(function(){var hash=document.location.hash, re=/^#?\d+&/;if(hash !==last_hash && re.test(hash)){last_hash=hash;callback({data: hash.replace(re, '')});}}, delay);}}};})(jQuery);


(function($){
	  var interval_id,
	    last_hash,
	    cache_bust = 1,
	    rm_callback,
	    window = this,
	    FALSE = !1,
	    postMessage = 'postMessage',
	    addEventListener = 'addEventListener',
	    _opera,
	    p_receiveMessage,
	    has_postMessage;
		_opera = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
		_opera = !!(_opera && parseFloat(_opera[1]) < 9.7);
	  	has_postMessage = window[postMessage] && !_opera;
	  $[postMessage] = function( message, target_url, target ) {
	    if ( !target_url ) { return; }
	    message = typeof message === 'string' ? message : $.param( message );
	    target = target || parent;
	    if ( has_postMessage && !/promo.toyota.ru\/z\/test/.test(target_url)) {
	      target[postMessage]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );
	    } else if ( target_url ) {
	      target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
	    }
	  };
	  $.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
	    if ( has_postMessage ) {
	      if ( callback ) {
	        rm_callback && p_receiveMessage();
	        rm_callback = function(e) {
	        	if(e.originalEvent) {
	        		e = e.originalEvent;
	        	}
	          if ( ( typeof source_origin === 'string' && source_origin != "*" && e.origin !== source_origin )
	            || ( $.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
	            return FALSE;
	          }
	          callback( e );
	        };
	      }
	      $(window).bind('message', rm_callback);
	    } else {
	      interval_id && clearInterval( interval_id );
	      interval_id = null;
	      if ( callback ) {
	        delay = typeof source_origin === 'number'
	          ? source_origin
	          : typeof delay === 'number'
	            ? delay
	            : 100;
	        interval_id = setInterval(function(){
	          var hash = document.location.hash,
	            re = /^#?\d+&/;
	          if ( hash !== last_hash && re.test( hash ) ) {
	            last_hash = hash;
	            callback({ data: hash.replace( re, '' ) });
	          }
	        }, delay );
	      }
	    }
	  };
	})(jQuery);
var _bstdBrowsers = new function(){
	var _tmp;
	
	this.ie6 = function() {
	  var browser = navigator.appName;
	  if (browser == "Microsoft Internet Explorer"){
	    var b_version = navigator.appVersion;
	    var re = /\MSIE\s+(\d\.\d\b)/;
	    var res = b_version.match(re);
	    if (res && res[1] && res[1] <= 6){
	      return true;
	    }
	  }
	  return false;
	}();
	this.ie7 = function() {
		  var browser = navigator.appName;
		  if (browser == "Microsoft Internet Explorer"){
		    var b_version = navigator.appVersion;
		    var re = /\MSIE\s+(\d\.\d\b)/;
		    var res = b_version.match(re);
		    if (res && res[1] && res[1] <= 7){
		      return true;
		    }
		  }
		  return false;
		}();
	
	_tmp = /Opera\s*\/\s*([\d.\d]+)/.exec(navigator.userAgent);
	this.opera = !!_tmp;
	this.operaUnder97 = !!(_tmp && parseFloat(_tmp[1]) < 9.7);
	this.chrome = !!/Chrome\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
	this.webkit = !!/WebKit\s*\/\s*([\d.\d]+)/.exec(navigator.appVersion);
};

var bstdFrames = (new function() {
	var sThis = this;
	var parent_url,
		_name,
		_id;

	if(window != top) {
		parent_url = decodeURIComponent(location.params["ref"] ? location.params["ref"] : document.location.hash.replace( /^#/, '' )),
		_name = location.params["refn"] ? location.params["refn"] : "",
		_id = location.params["refi"] ? location.params["refi"] : "";

		this.setHeight = function(add) {
			var _data = {};
			_data["if_height"] = parseInt($("#resizer").length ? $("#resizer").outerHeight(true) : $('body').height()) + (!isNaN(parseInt(add)) ? parseInt(add) : 0) + (_bstdBrowsers.webkit ? 100 : 0); //$('body').outerHeight(true)
			_data["name"] = _name;
			_data["refi"] = _id;
			$.postMessage(_data, parent_url, parent);
		};
	}
	else {
		this.registerIFrame = function(iframe, url) {
			var _id;
			
			if(!iframe) {return;}
			iframe = $(iframe)[0];
			_id = iframe.id;
			if(!_id) {iframe.id = _id = "ifrm_" +  + Math.random().toString().replace(".", "");}
			iframe.src = url + (url.indexOf("?") == -1 ? "?" : "&") + "ref=" + encodeURIComponent(location.protocol + "//" + location.host + location.pathname) + "&refi=" + _id;
		};

		this.createIFrame = function(parent, url, name, id) {
			var _iframe = $("<iframe name=\"" + (name ? name : "ifrm_" +  + Math.random().toString().replace(".", "")) + "\" id=\"" + (id ? id : "ifrm_" +  + Math.random().toString().replace(".", "")) + "\">");
			$(parent || "body").append(_iframe);
			if(url) {sThis.registerIFrame(_iframe, url);}
		};
		
		$.receiveMessage(function(e){
			var _inData = e.data.split("&"),
				_outData = {},
				_iframe,
				_i,
				_itm;
	
			 for(_i = 0; _i < _inData.length; _i ++) {
				 _itm = _inData[_i].split("=");
				 if(_itm[0]) {
					 _outData[_itm[0]] = _itm[1];
				 }
			 }
			
			if(!isNaN(_outData["if_height"]) && _outData["if_height"] > 0) {
				if(_outData["id"]) {
					_iframe = $("#" + _outData["id"]);
				}
				else if(_outData["name"]) {
					_iframe = frames[_outData["name"]];
				}
				if(_iframe) {
					$(_iframe).height(_outData["if_height"]);
				}
			}
		}, '*' );
	}
});