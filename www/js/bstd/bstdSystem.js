(function() {
	var _params = location.href.split("?"),
		_i,
		_tmp,
		_s;
	location["params"] = {};
	if(_params.length > 1) {
		_params = _params[1].split("#")[0].split("&");
		for(_i = 0; _i < _params.length; _i++) {
			_tmp = new RegExp(/^([^=]+)(?:=(.+))?/).exec(_params[_i]);
            if((_s = _tmp[1].indexOf("[]")) != -1) {
                _tmp[1] = _tmp[1].substr(0, _s);
            }
            if(_tmp[2] === undefined) {
                _tmp[2] = null;
            }
			if(_tmp[1] in location["params"]) {
				if(location["params"][_tmp[1]] instanceof Array) {
					location["params"][_tmp[1]].push(_tmp[2]);
				}
				else location["params"][_tmp[1]] = [location["params"][_tmp[1]], _tmp[2]];
			}
			else location["params"][_tmp[1]] = _tmp[2];
		}
	}
	return location["params"];
}());


var bstdError = function(message, code, DOMElement, targetObject) {
	this.getMessage = function() {
		return message;
	};
	
	this.getCode = function() {
		return code;
	};
	
	this.getDOMElement = function() {
		return DOMElement;
	};
	
	this.getObject = function() {
		return targetObject;
	};
};

var bstdSystem = new function() {
	var sThis = this,
		_loaded = !!$("body").length,
		_doc,
		_onLoadCash = new Array();
		
	if(!_loaded) {
		$(document).ready(function(){
			var _j;
			_loaded = true;
			for(_j = 0; _j < _onLoadCash.length; _j ++) {
				_onLoadCash[_j]();
			}
		});
	}
		
	this.isLoaded = function () {
		return _loaded;
	};
	
	this.onAfterLoad = function(callback) {
		if(_loaded) {
			callback();
		}
		else {
			if($.inArray(callback,_onLoadCash) == -1) {
				_onLoadCash.push(callback);
			}
		}
	};
	
	var _preparePage = function() {
		//bstd_tips
	};
	
	this.prepareResult = function(response, getResultOnly) {
		if(typeof response == "object" && typeof response.responseText !== undefined) {
			response = response.responseText;
		}
		if(typeof response == "string") {
			response = $.parseJSON(response);
		}
		if(response && typeof response == "object") {
			//Проверяем общее сообщение
			if(response.message) {
				bstdAlert(response.message);
			}
			if(response.error && response.result) {
				//Проверяем общее сообщение об ошибке
				if(response.error_message) {
					bstdAlert(response.errorMessage);
				}
			}
			if(getResultOnly) {
				return response.result;
			}
			return response; 
		}
		return null;
	};
	
	Request = new function() {
		var sR = this,
		Req = {};
		
		sR.RegisterFileUploader = function(i, ObjMetod, Controller, Action, Params, callback) {
			i.fnct = function(i, ObjMetod, Controller, Action, Params, callback) {
								return function() {
									bstdSystem.Request.SendUpload(i, ObjMetod, Controller, Action, Params, null, callback);
								};}(_clone, ObjMetod, Controller, Action, Params, callback);
			$(i).bind("change", i.fnct);
		};
		
		/**
		*	Метод используется для отправки файлов
		*/
		sR.SendUpload=function(i, ObjMetod, URL, Controller, Action, Params, notClone, callback) {
			if(!i || !ObjMetod || !Controller || !Action) return false;
			var _clone,
				_parent,
				_frm,
				_newForm,
				_ifrm,
				iKey=URL + ":" + Controller + ":" + Action + ":" + Math.random(),
				_ifrmName = 'uploarerTransitFrame' + iKey;
			
			i = $(i);
			
			Req[iKey]={};
			Req[iKey].Metod=ObjMetod;
			Req[iKey].Rq={};
			
			if(Controller) {
				Req[iKey].Rq["a"] = Controller;
			}
			if(Action) {
				Req[iKey].Rq["b"] = Action;
			}
			
			if(typeof Params=="object")
				for(var p in Params)
					Req[iKey].Rq.d[p]=Params[p];
			Req[iKey].Sending=true;

			if(!notClone) {
				_parent = i.parent();
				_clone = i.clone(true);
				i.unbind("change", i[0].fnct);
				_parent.append(_clone);
				
				sR.RegisterFileUploader(i, ObjMetod, URL, Controller, Action, Params, null, callback);
			}
			
			$("body").append($("<DIV>").css("width", 0)
										.css("height", 0)
										.css.append(Req[iKey].Trans = $('<iframe name="' + _ifrmName + '" src="' + _baseURL + '">').css("width", 0)
																																	.css("height", 0)
																																	.hide())
										.hide()
										.append(
												c = $("<FORM>").attr("action", URL ? (URL.indexOf("://") != -1 ? location.protocol + "//" + URL : URL) : location.protocol + "//" + location.host)
																		.attr("method", "POST")
																		.attr("enctype", "multipart/form-data")
																		.hide()
																		.append(i)
																		.append($('<input type="hidden" name="prq">').val($.toJSON([Req[iKey].Rq])))
																		.append($('<input type="hidden" name="t">').val("fu"))
																		.attr("target", _ifrmName)
										)
			);
			
			(function(e, callback){
				    var tm;
					if (!e[0].contentWindow){
						tm = window.setTimeout(arguments.callee,100);
				         return;
				    }
					window.clearTimeout(tm);
					e.load(function(ifrm, callBack) {
							return function() {
								ResponseUpload(ifrm, callBack);
							};
						}(e, callback));
				})(_ifrm, ObjMetod);
		
			_newForm.submit().remove();
			
			if(typeof callback == "function") {
				callback(i);
			}
		};
		
		var ResponseUpload = function(e, callback) {
			var returned = "";
			if(!e.tagName.toLowerCase()=="iframe") return;
				//throw new Error("Transport Element is not found");
			//e.contentWindow.location = 'about:blank';
			//if(e.contentWindow.name) returned = JSON.parse(e.contentWindow.name)
			
			if(e.contentWindow.document)
				returned = $.parseJSON($("body", $(e.contentWindow.document)).children().first().html());
			
			$(e).parent().remove();
			
			callback(returned);
		};
	};
};