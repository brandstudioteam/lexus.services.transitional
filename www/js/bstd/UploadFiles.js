if(!bstdConfig)
	var bstdConfig = {};
bstdConfig["upload"] = {
	availableType: {
			1: {
			"image/jpeg": 1,
			"image/gif": 1,
			"image/jpg": 1,
			"image/png": 1,
			"image/bmp": 1,
			"image/tiff": 1,
			"image/x-png": 1,
			"image/pjpeg": 1
		},
		2: {
			"video/mpeg": "/images/icons/ic_dt_5.png",
			"video/quicktime": "/images/icons/ic_dt_5.png",
			"video/x-msvideo": "/images/icons/ic_dt_5.png",
			"video/x-sgi-movie": "/images/icons/ic_dt_5.png",
			"video/x-ms-wmv": "/images/icons/ic_dt_5.png"
		},
		3: {
			"text/richtext": "/images/icons/ic_dt_2.png",
			"text/rtf": "/images/icons/ic_dt_2.png",
			"application/excel": "/images/icons/ic_dt_3.png",
			"text/plain": "/images/icons/ic_dt_2.png",
			"application/vnd.ms-excel": "/images/icons/ic_dt_3.png",
			"application/vnd.ms-powerpoint": 1,
			"application/powerpoint": 1,
			"application/x-shockwave-flash": 1
		}
	},
	availableTypeFBr: {
		1: {
			"image/*": 1
		},
		2: {
			"video/*": "/images/icons/ic_dt_5.png"
		},
		3: {
			"text/richtext": "/images/icons/ic_dt_2.png",
			"text/rtf": "/images/icons/ic_dt_2.png",
			"application/excel": "/images/icons/ic_dt_3.png",
			"text/plain": "/images/icons/ic_dt_2.png",
			"application/vnd.ms-excel": "/images/icons/ic_dt_3.png",
			"application/vnd.ms-powerpoint": 1,
			"application/powerpoint": 1,
			"application/x-shockwave-flash": 1
		}
	},
	innerTypes: {
		1: "Изображение",
		2: "Видео",
		3: "Файлы"
	},
	headersForInnerTypes: {
		1: "Загрузка изображений",
		2: "Загрузка видео",
		3: "Загрузка файлов"
	}
};

var bstdE = {
	hide: function(Element) {
		if(typeof Element == "object" && Element.style) {
			Element.style.display = "none";
		}
	},
	b: document.body
};

var bstd = {};
bstd.Browser = new function() {
	var sBrowser=this;

	sBrowser.agent = navigator.userAgent.toLowerCase();
console.log(sBrowser.agent);
	//Тут не хватает chrome
	//TODO:: Chrome определяется как Safari
	sBrowser.names = sBrowser.agent.match(/(win|mac|mozilla|compatible|chrome|webkit|safari|opera|msie|iphone|ipod|ipad)/gi);
console.log(sBrowser.names);
	var len = sBrowser.names.length;
	while(len-- > 0)sBrowser[sBrowser.names[len]] = true;

	sBrowser.win = sBrowser["win"];
	sBrowser.mac = sBrowser["mac"];
	sBrowser.chrome = sBrowser["chrome"];
	sBrowser.mozilla = sBrowser["mozilla"];
	//sBrowser.webkit = sBrowser["webkit"];
	sBrowser.webkit = /webkit/.test(sBrowser.agent) ? parseFloat(sBrowser.agent.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false;
	sBrowser.safari = sBrowser["safari"];
	sBrowser.opera = sBrowser["opera"];
	sBrowser.msie = sBrowser["msie"];
	sBrowser.iphone = sBrowser["iphone"];
	sBrowser.ipod = sBrowser["ipod"];
	sBrowser.ipad = sBrowser["ipad"];

	if(sBrowser["compatible"] || sBrowser.webkit) {
		sBrowser.mozilla = false;
		delete sBrowser["mozilla"];
	} else if(sBrowser.opera) {
		sBrowser.msie = false;
		delete sBrowser["msie"];
	}
	if(sBrowser.msie) {
		for(var i = 6 ; i < 10 && !sBrowser.msieV ; i++ ) {//IE from 6 to 9
			if(new RegExp('msie ' + i).test(sBrowser.agent)) {
				sBrowser.msieV = i;
			}
		}
	}
	//Определяем поддерживаемые браузером технолигии
	sBrowser.testElement = document.createElement('div');
	sBrowser.traversal = (sBrowser.testElement.childElementCount !== undefined)?true:false;
	sBrowser.isPlaceholder = (document.createElement("INPUT").placeholder !== undefined)?true:false;
};

var bstdO = {
		progress: function() {
			return new function() {
				var _container = bstdNew.div(),
					_progress = bstdNew.div(_container),
					_value = 0,
					_max = 0,
					_sum = 0,
					_isFull = false;
				
				_progress.style.width = "0px";
				
				this.SetValue = function(pValue) {
					_value = pValue;
				}
				
				this.SetMax = function(pValue) {
					_max = pValue;
				}

				this.SetSum = function(pValue) {
					_sum = pValue ? pValue : 0;
				}
				
				this.GetContainer = function() {
					return _container;
				}
				
				this.GetValue = function(pValue) {
					return _value;
				}
				
				this.GetMax = function(pValue) {
					return _max;
				}

				this.GetSum = function() {
					return _sum;
				}
				
				this.Change = function(pValue) {
					_value = _sum ? pValue - _sum + _max : pValue;
					_progress.style.width = Math.round(100 * _value / _max) + '%';
					if(_value == _max) {
						_isFull = true;
					}
				}
				
				this.Full = function() {
					_isFull = true;
					_value = _max;
					_progress.style.width = '100%';
				}
				
				this.IsFull = function() {
					return _isFull;
				}
			}
		}
	};

var bstdNew = {
	form: function(pAction, pMethod, pEnctype, pTarget, pParent, pHidde) {
		var _e = document.createElement("FORM");
		_e.action = pAction;
		_e.method = pMethod ? pMethod : "POST";
		_e.enctype = pEnctype ? pEnctype : "multipart/form-data";
		if(pTarget) {
			_e.target = pTarget;
			_e.setAttribute("target", pTarget);
		}
		if(pHidde) {
			bstdE.hide(_e);
		}
		if(pParent) {
			pParent.appendChild(_e)
		}
		return _e;
	},
	hidden: function(pName, pValue, pParent, pHidde) {
		var _e = document.createElement("input");
		_e.type = "hidden";
		_e.name = pName;
		_e.value = pValue;
		if(pHidde) {
			bstdE.hide(_e);
		}
		if(pParent) {
			pParent.appendChild(_e)
		}
		return _e;
	},
	iframe: function(pName, pSource, pParent, pHidde) {
		var _e = '<iframe',
			_p = bstdNew.div();
		if(pName) {
			_e += ' name="' + pName + '"';
			_e += ' id="' + pName + '"';
		}
		if(pSource) {
			_e += ' src="' + _baseURL + '"';
		}
		else {
			_e += ' src="about:blank"';
		}
		_p.innerHTML = _e + '></iframe>';
		_e = _p.childNodes[0];
		if(pHidde) {
			bstdE.hide(_e);
		}
		if(pParent) {
			pParent.appendChild(_e)
		}
		return _e;
	},
	div: function(pParent, pHidde) {
		var _e = document.createElement("div");
		if(pHidde) {
			bstdE.hide(_e);
		}
		if(pParent) {
			pParent.appendChild(_e)
		}
		return _e;
	}
};

var bstdFileUpload = new function() {
	var _fileBuffer = {},
		_packetBuffer = {},
		_getXR = function() {return new XMLHttpRequest()},
		_sumSize = 0,
		_file,
		_files = [],
		_count = 0,
		_errors = [],
		_packet = [],
		_packets = [],
		_currentProgress = {},
		_packetNumber = 0,
		_maxFileCountInPacket = 5,
		_fileSizeForSimpleUpload = 50000000, //104857600,
		_minPacketSize = 5857600,//104857600,
		_maxPacketSize = 214700000000,
		LOADED = 1,
		ERROR = 2,
		ABORT = 3,
		PROGRESS = 4,
		_imageResizer;
		
	/**
	 * Создает индикаторы процесса загрузки
	 */
	var createProgressBar = function(file, prLoadId) {
		var _preview = file.iIndex > 0 ? _files[file.iIndex - 1] : null;
		//Создаем индикатор тегом progress
		file.progress = bstdO.progress();
		file.progress.GetContainer().className = "progress";
		file.progress.SetMax(file.size);
		if(_preview && _preview.prLoadId == prLoadId) {
			file.progress.SetSum(file.size + _preview.progress.GetSum());
		}
		else {
			file.progress.SetSum(file.size);
		}
	}
	
	this.loadImageFromLocal = function(file, onLoad) {
		if(typeof FileReader != "undefined" || window.FileReader) {
			return function(f, callBack) {
				var _reader	= new FileReader(),
					_img = document.createElement("img");
				
				f.image = document.createElement("img");
				
				_reader.onloadend = function(e) {
					if(callBack) {
						f.image.onload = callBack;
					}
					f.image.src = e.target.result;
				};
				_reader.onerror = function(e) {
				    //console.error("Файл не может быть прочитан! код " + e.target.error.code);
				    f.error = "Файл не может быть прочитан\n" + f.name;
				};
				_reader.readAsDataURL(f);
			}(file, onLoad);
		}
		else {
			file.notImageLoaded = true;
			/*
			if(file.locPath) {
				var _img = bstdNew.div();
				_img.innerHTML = '<img src="' + file.locPath + '" />';
				file.image = _img.childNodes[0];
			}*/
		}
	};

	/**
	 * Выполняет отправку пакета файлов на сервер
	 */
	var send = function(packet, type, uuid) {
		var _fData,
			_xR,
			_frm,
			_parent,
			_action = bstdConfig.urls.upload + uuid,
			_fileUploader,
			_file;
		
		if(!packet.length)
			return;
		
		_packetBuffer[uuid] = [];
		
		if(typeof FormData != "undefined" || typeof window.FormData != "undefined") {
			_fData = new FormData();
			_xR = _getXR();
		
			_fData.append("upload[id]", bstdConfig.pressId);
			_fData.append("X-Progress-ID", uuid);
			_fData.append("upload[type]", type);
			
			for(var i = 0; i < packet.length; i++) {
				_packetBuffer[uuid].push(packet[i]);
				_fData.append("upload[file][" + i + "]", packet[i]); //_fData.append("upload[file][" + i + "]", packet[i]);
			}
			$(_xR).bind("load", function(buffer){
				return function(e) {
					var _dP,
						_i;
					for(_i = 0; _i < buffer.length; _i ++) {
							buffer[_i].progress.Full();
						}
					};}(_packetBuffer[uuid]));
			
			$(_xR.upload).bind("progress", function(buffer){
				return function(e) {
					var _dP,
						_prewSum = 0,
						_i;
					e = e.originalEvent;
					for(_i = 0; _i < buffer.length; _i ++) {
						_dP = buffer[_i].progress;
						if(_dP.IsFull()) {
							continue;
						}
						if(e.loaded > _dP.GetSum()) {
							_dP.Full();
							continue;
						}
						_dP.Change(e.loaded);
						break;
					}
				}}(_packetBuffer[uuid]));
			
			_xR.open("post", _action, true);
			_xR.setRequestHeader("DNT", "1");
			_xR.send(_fData);
		}
		else
		{
			_file = packet[0];
			_fileUploader = _file.fileUploader;
			_ifrmName = 'uploarerTransitFrame_' + uuid;
			_fileUploader.parentNode.appendChild(_fileUploader.cloneNode(true));
			(_frm = bstdNew.form(_action, "POST", "multipart/form-data", _ifrmName, bstdE.b, true)).appendChild(_fileUploader);
			
			_ifrm = bstdNew.iframe(_ifrmName, null, bstdNew.div(bstdE.b, true), true);
			
			bstdNew.hidden("upload[id]", bstdConfig.pressId, _frm);
			bstdNew.hidden("X-Progress-ID", uuid, _frm);
			bstdNew.hidden("upload[type]", type, _frm);

			$(_ifrm).bind("load", function(e){
				var _e = e.target,
					_returned,
					_d;
				if(!_e.tagName.toLowerCase() == "iframe") return;
				
				if (_e.contentDocument) {
					_d = _e.contentDocument;
				} else if (_e.contentWindow) {
					_d = _e.contentWindow.document;
				} else {
					_d = window.frames[_ifrmName].document;
				}
				if (_d.location.href == "about:blank") {
					return;
				}
				try{
					_response = JSON.parse(_d.body.childNodes[0].innerHTML);
					_e.parentNode.removeChild(_e);
					getUploadResult(_response);
				}
				catch(err) {
					
				}
			});
			
			_frm.submit();
			
			(function(progress, uuid) {
				var _a = function() {
					$.ajax({
						url: '/progress',
					    beforeSend: function(xhr) {
					    	xhr.setRequestHeader("X-Progress-ID", uuid);
					    },
					    success: function(resp)
					    {
					        var info = eval('('+resp+')');
					        if (info.state == 'uploading')
					        {
					        	progress.SetMax(info.size);
					        	progress.Change(info.received);
					        }
					        
					        if (info.state == 'done') {
					        	progress.Full();
					        }
					
					        if(!(info.size - info.received)) {
					            setTimeout(_a, 500);
					        }
					    }
					});
				}
				_a();
			}(_file.progress, uuid));
		}
	};
	
	var getUploadResult = function(response) {
		
	};
	
	
	var createUUID = function() {
		var _uuid = "";
		for (var i = 0; i < 32; i++) {
			_uuid += Math.floor(Math.random() * 16).toString(16);
		}
		return _uuid;
	};
	
	
	this.upload = function(fileUploader, typesNumber, callBack, errorCallBack) {
		var _uuid = createUUID(),
			_fso,
			_fsoFile,
			_s;
		
		if(!fileUploader.files) {
			fileUploader.files = [];
		
			_file = {};
			_file.name = fileUploader.value.substring(fileUploader.value.lastIndexOf('\\') + 1);
			_file.locPath = fileUploader.value;
			_file.fileUploader = fileUploader;
			//"C:\\fakepath\\DSC_0054.JPG"
			
			/*
			try {
				_fso = new ActiveXObject("Scripting.FileSystemObject"); 
				_fsoFile = _fso.getFile(fileUploader.value);
				_file.size = _fsoFile.Size;
				_file.type = _fsoFile.Type;
				_file.isFSO = true;
			}
			catch($e) {
				
			}
			*/
			fileUploader.files.push(_file);
			
			_file.iIndex = _files.length;
			_files.push(_file);
			createProgressBar(_file, _uuid);
			_file.innerType = typesNumber;
			_fileBuffer[j] = _file;
			//_packet.push(_file);
			send([_file], typesNumber, _uuid);
		}
		else {
			for(var j = 0; j < fileUploader.files.length; j ++)
			{
				_file = fileUploader.files[j];
				if(!bstdConfig.upload.availableType[typesNumber][_file.type])
				{
					_errors.push(j);
					continue;
				}
console.log(_file.type);
				_file.iIndex = _files.length;
				_files.push(_file);
				_file.innerType = typesNumber;
				_fileBuffer[j] = _file;
				
				//Отправляем отдельным пакетом
				/*
				if(_file.size > _maxPacketSize)
				{
					//Отправляем файл частями
				}
				else
				{*/
					if(_sumSize + _file.size > _fileSizeForSimpleUpload) {
						//Отправляем пакет
						send(_packet, typesNumber, _uuid);
						_packet = [];
						_uuid = createUUID();
					}
					_file.prLoadId = _uuid;
					createProgressBar(_file, _uuid);
					_packet.push(_file);
					_sumSize += _file.size;
				//}
			}
			if(_packet.length)
			{
				send(_packet, typesNumber, _uuid);
			}
		}
		_packet = [];
		_sumSize = 0;

		if(_files.length) {
			if(callBack && typeof callBack == "function")
			{
				callBack(_fileBuffer);
			}
		}
		else {
			this.Clear();
			if(errorCallBack && typeof errorCallBack == "function") {
				errorCallBack();
			}
		}
	}
	
	this.getFiles = function()
	{
		return _fileBuffer;
	}
	
	this.Clear = function() {
		_fileBuffer = {};
		_packetBuffer = {};
		_sumSize = 0;
		_file = undefined;
		_count = 0;
		_errors = [];
		_packet = [];
		_packets = [];
		_packetNumber = 0;
		_currentProgress = {};
	}
}

var bstdPopupWin = new function() {
	var _win = document.getElementById("bstdPopupWin"), //document.createElement("div"),
		_content,
		_header,
		_buttons,
		_closeButton,
		_onClose = [],
		_sObj = this;
	
	//_win.id = "bstdPopupWin";
	//_win.className = "dialog add-photo-dialog";
	//_win.innerHTML = '<div id="bstdPopupWin" class="dialog"><table><tr><td class="d-lt"><div></div></td><td class="d-t"></td><td class="d-rt"><div></div></td></tr><tr><td class="d-l"><div></div></td><td><div class="d-inner"><div id="bstdPopupCloseButton" class="close"></div><h3 id="bstdPopupHeader"></h3><div id="bstdPopupContent"></div><div id="bstdPopupButtons" class="buttons-block"></div></div></td><td class="d-r"><div></div></td></tr><tr><td class="d-lb"><div></div></td><td class="d-b"></td><td class="d-rb"><div></div></td></tr></table></div>';
	_header = document.getElementById("bstdPopupHeader");//_win.getElementsByTagName("h3")[0];
	_content = document.getElementById("bstdPopupContent");//_header.nextSibling;
	_buttons = document.getElementById("bstdPopupButtons");//_content.nextSibling;
	_closeButton = document.getElementById("bstdPopupCloseButton");//_win.childNodes[0].childNodes[2].childNodes[1].childNodes[0].childNodes[0];
	
	document.body.appendChild(_win);
	
	this.Open = function(header, content, buttons, append, width, height, onClose, hiddeCloseButton) {
		if(content) _sObj.SetContent(content, append);
		if(header) _sObj.SetHeader(header);
		if(buttons) _sObj.SetButton(buttons);
		if(onClose) {
			_sObj.OnClose(onClose);
		}
		if(hiddeCloseButton) {
			_sObj.HiddenCloseButton();
		}
		else if(hiddeCloseButton !== undefined) {
			_sObj.ShowCloseButton();
		}
		_sObj.Show();
		_sObj.Resize(width, height);
	};
	
	this.Close = function() {
		var _cancel = false;
		for(var i = 0; i < _onClose.length; i ++){
			_cancel = _cancel ? _cancel : _onClose[i]();
		}
		if(_cancel) {
			return;
		}
		_content.innerHTML = "";
		_header.innerHTML = "";
		_buttons.innerHTML = "";
		_win.style.height = "";
		_win.style.width = "";
		
		_sObj.Hidden();
		_sObj.ShowCloseButton();
		_sObj.ShowButtons();
	};
	
	this.SetHeader = function(header) {
		_header.innerHTML = typeof header != "string" ? "" : header;
	};
	
	this.SetContent = function(content, append) {
		if(!content) new Error("bstdPopupWin::Open. Error content");
		if(typeof content == "object") {
			if(!append) {
				_content.innerHTML = "";
			}
			_content.appendChild(content);
		}
		else if(typeof content == "string") {
			_content.innerHTML = (append ? _content.innerHTML : "") + content;
		}
	};
	
	this.GetContent = function() {
		return _content.childNodes;
	};

	this.SetButton = function(buttonObject) {
		_buttons.appendChild(buttonObject);
	}
	
	this.Resize = function(width, height) {
		if(width) {
			_win.style.width = width + "px";
		}
		if(height) {
			_win.style.height = height + "px";
		}
		_win.style.position = "absolute";
		_win.style.top = "20px";
		_win.style.left = "50%";
		_win.style.zIndex = "50000";
		_win.style.left = Math.round((document.body.clientWidth - _win.offsetWidth) / 2) + "px"
		
		//document.body.clientWidth
		
		//_win.style.marginRight = _win.style.marginLeft = "-" + Math.round(_win.offsetWidth / 2) + "px";
	};
	
	this.OnClose = function(callBack) {
		if(typeof callBack == "function") {
			_onClose.push(callBack);
		}
	};
	
	this.Hidden = function() {
		_win.style.display = "none";
	};
	
	this.Show = function() {
		_win.style.display = "block";
	};
	
	this.HiddenButtons = function() {
		_buttons.style.display = "none";
	};
	
	this.ShowButtons = function() {
		_buttons.style.display = "";
	};
	
	this.HiddenCloseButton = function() {
		_closeButton.style.display = "none";
	};
	
	this.ShowCloseButton = function() {
		_closeButton.style.display = "";
	}
	
	$(_closeButton).click(_sObj.Close);
	
	//_closeButton.addEventListener("click", _sObj.Close);
	this.Hidden();
}

var bstdUtils = {
	calculateImageBox: function(boxWidht, boxHeight, imageWidht, imageHeight) {
		var _kW = boxWidht / imageWidht,
			_kH = boxHeight / imageHeight,
			_ko = _kW < _kH ? _kW : _kH;
		
		return {
			imageWidht: Math.round(imageWidht * _ko),
			imageHeight: Math.round(imageHeight * _ko)
		};
	},
	resizeImage: function(boxWidht, boxHeight, imageObj) {
		var _imageWidht,
			_imageHeight,
			_new;
		if(!imageObj) return;
		_imageWidht = (typeof imageObj.naturalWidth != "undefined") ? imageObj.naturalWidth :
				(imageObj.width) ? imageObj.width : imageObj.offsetWidth;
		_imageHeight = (typeof imageObj.naturalHeight != "undefined") ? imageObj.naturalHeight :
				(imageObj.height) ? imageObj.height : imageObj.offsetHeight;
		_new = bstdUtils.calculateImageBox(boxWidht, boxHeight, _imageWidht, _imageHeight);
		if(_new.imageWidht) {
			imageObj.width = _new.imageWidht;
			imageObj.style.width = _new.imageWidht + "px";
		}
		if(_new.imageHeight) {
			imageObj.height = _new.imageHeight;
			imageObj.style.height = _new.imageHeight + "px";
		}
	}

};

	
var bstdRenderUtils = {
	descriptionsCounter: 0,
	descriptionsBuffer: {},
	createMediaForm: function(files) {
			var _template,
				_buttonTemplate = '<a class="button upload-file-save" href="#"><span>Сохранить</span></a>',
				_item,
				_type;
				_div = document.createElement("div"),
				_parent = document.createElement("ul");
				_parent.style.listStyleType = "none";
			
				_template  = '<li style="margin-top: 20px;"><label class="upload-file-label" style="margin-bottom: 5px; display: block; font-size: 12px; font-weight: bold; text-align: center;">%fileName%</label>';
				_template += '<div class="upload-file-image-block" style="float: left;width: 156px;"><div style="width:150px; height:100px;"></div></div>';
				_template += '<textarea class="upload-file-description placeholder" placeholder name="%fileName%" id="fileDescription_%fileNumber%" style="width: 350px; height: 120px; margin-left: 10px;">Добавить описание</textarea>';
				_template += '<div style="clear:both;"></li>';
				
			bstdRenderUtils.descriptionsCounter = 0;
			bstdRenderUtils.descriptionsBuffer = {};
				
			if(files) {
				_parent.className="uploads-file-list";
				for(var i in files) {
					if(!_type) {
						_type = files[i].innerType;
					}
					_div.innerHTML = _template.replace(/%fileName%/g, files[i].name).replace(/%fileNumber%/g, i);
					_item = _div.childNodes[0];
					
					
					if(files[i].innerType == 1) {
						//files[i]
						bstdFileUpload.loadImageFromLocal(files[i],
								function(parent) {
									return function(e) {
										var img = e.target;
										img.className = "upload-file-image";
										img.style.display = "block";
										img.style.padding = "3px";
										bstdUtils.resizeImage(150, 100, img);
										//parent.appendChild(img);
										
										parent.innerHTML = "<img class=\"" + img.className + "\" width=\"" + img.width + "\" height=\"" + img.height + "\" src=\"" + img.src + "\" />" + parent.innerHTML;
									}
								}(_item.childNodes[1].childNodes[0]));
						/*
						if(files[i].locPath) {
							bstdUtils.resizeImage(150, 100, files[i].image);
							_item.childNodes[1].childNodes[0].appendChild(files[i].image);
						}
						*/
					}
					else {
						files[i].image = document.createElement("img");
						
						files[i].image.onload = function(parent) {
							return function(e) {
								var img = e.target;
								img.className = "upload-file-image";
								img.style.display = "block";
								img.style.padding = "3px";
								bstdUtils.resizeImage(150, 100, img);
								
								parent.innerHTML = "<img class=\"" + img.className + "\" width=\"" + img.width + "\" height=\"" + img.height + "\" src=\"" + img.src + "\" />" + parent.innerHTML;
							}
						}(_item.childNodes[1].childNodes[0]);
						
						files[i].image.src = bstdConfig.upload.availableType[files[i].innerType][files[i].type];
					}
					
					
					_item.childNodes[1].appendChild(files[i].progress.GetContainer());
					_parent.appendChild(_item);
					bstdRenderUtils.descriptionsBuffer[i] = 0;
					bstdRenderUtils.descriptionsCounter ++;
					
					$(_item.childNodes[1].childNodes[0]).change(
							function(index) {
								return function(e) {
									
									if (e.target.value == '' || e.target.value == 'Добавить описание')
						       		{
						       			if(bstdRenderUtils.descriptionsBuffer[index] !== 0) {
											bstdRenderUtils.descriptionsBuffer[index] = 0;
											bstdRenderUtils.descriptionsCounter ++;
										}
						       		}
						       		else {
						       			if(bstdRenderUtils.descriptionsBuffer[index] === 0) {
											bstdRenderUtils.descriptionsBuffer[index] = 1;
											bstdRenderUtils.descriptionsCounter --
										}
						       		}
								}
							}(i));
					
					
					$(_item.childNodes[2]).bind("focus",
										function(e) {
								       		if (e.target.value == 'Добавить описание')
								       		{
								       			e.target.value = "";
								       			e.target.a.classList.remove('placeholder');
								       		}
								       });
			       
					$(_item.childNodes[2]).bind('blur',
										function(index) {
											return function(e) {
									       		if (e.target.value == '')
									       		{
									       			e.target.value = 'Добавить описание';
									       			e.target.a.classList.add('placeholder');
									       			
									       			if(bstdRenderUtils.descriptionsBuffer[index] !== 0) {
														bstdRenderUtils.descriptionsBuffer[index] = 0;
														bstdRenderUtils.descriptionsCounter ++;
													}
									       		}
									       		else {
									       			if(bstdRenderUtils.descriptionsBuffer[index] === 0) {
														bstdRenderUtils.descriptionsBuffer[index] = 1;
														bstdRenderUtils.descriptionsCounter --
													}
									       		}
									       }
										}(i));
				}
				
				_div.innerHTML = _buttonTemplate;
				$(_div.childNodes[0]).bind("click", bstdRenderUtils.saveFilesWithDesc);
				//				 (header, content, buttons, append, width, height, onClose, hiddeCloseButton)
				
				bstdPopupWin.Open(bstdConfig.upload.headersForInnerTypes[_type], _parent, _div.childNodes[0], null, 600, null, bstdFileUpload.Clear);
			}
		},
	createFileUploader: function(fileInnerType) {
			var _availableType = "",
				_template = '<input type="file" placeholder="Выбор файла" id="mnDwnldInpt" name="upload[file][]" value="" multiple accept="%acceptMimeTypes%"/>',
				_div = document.createElement("div");
			
			if(!fileInnerType || !bstdConfig.upload.availableType[fileInnerType]) {
				new Error("Argument fileInnerType error");
			}
			
			if(bstd.Browser.chrome) {
				for(var i in bstdConfig.upload.availableType[fileInnerType]) {
					_availableType += (_availableType != "" ? "," : "") + i;
				}
			}
			else {
				for(var i in bstdConfig.upload.availableTypeFBr[fileInnerType]) {
					_availableType += (_availableType != "" ? "," : "") + i;
				}
			}

			_div.innerHTML = _template.replace(/%acceptMimeTypes%/g, _availableType);

			$(_div.childNodes[0]).change(
				function(sObj, type) {
					return function() {
							bstdFileUpload.upload(sObj, type, bstdRenderUtils.createMediaForm, bstdRenderUtils.errorFileUploader);
						}
				}(_div.childNodes[0], fileInnerType), false);
								//(header, content, buttons, append, width, height, onClose, hiddeCloseButton)
			bstdPopupWin.Open(bstdConfig.upload.headersForInnerTypes[fileInnerType], _div.childNodes[0], null, null, 300);
		},
	errorFileUploader: function() {
			bstdPopupWin.Close();
		},
	addItemInFileHolder: function(filesWithId) {
			var _template,
				_div = document.createElement("div"),
				_filesBuffer = bstdFileUpload.getFiles(),
				_file,
				_btns,
				_imgPath,
				_img;
			
			_template  = '<li><div><input id="%fileId%" type="checkbox" name="file[%fileName%]"></div><a class="lightbox image" rel="image" href="%sourceFiles%%fileName%/%fileName%">';
			_template += '<span></span><strong>%fileName%</strong></a>';
			_template += '<ol style="height:19px;padding-left:30px;" class="image-buttons"><li><a href="" class="add-basket" data-id="%fileId%" title="Добавить в корзину"><img src="/i/buy.gif" alt="Добавить в корзину"></a></li>';
			_template += '<li><a href="%downloadUrl%" title="Скачать"><img src="/i/save.gif" alt="Скачать"></a></li></ol></li>';
			
			for(_i in _filesBuffer) {
				_file = _filesBuffer[_i];
				_div.innerHTML = _template.replace(/%fileName%/g, _file.name)
										.replace(/%pressId%/g, bstdConfig.pressId)
										.replace(/%fileId%/g, filesWithId[_file.name])
										.replace(/%downloadUrl%/g, bstdConfig.urls.downloadFile + filesWithId[_file.name])
										.replace(/%sourceFiles%/g, bstdConfig.urls.sourceFiles);
				
				$(_div.childNodes[0].childNodes[0].childNodes[0]).click(selectedFileForCheckBox);
				
				$(_div.getElementsByTagName("a")[1]).click(addEventAddBasket);
				
				$(_div.childNodes[0].childNodes[1]).click(_file.innerType == 1 ?
						function(obj) {
							return function(e) {
								e.preventDefault();
								
							    jQuery.fn.bsLightBox.anchor = obj;//_this;
								jQuery.fn.bsLightBox.DetectBrothers();
							    jQuery('#bsLightBoxImg').attr('src', obj.href);//_this.href);
							
								return false;
							}
						}(_div.childNodes[0].childNodes[1]) : _file.innerType == 2 ?
						function(obj) {
							return function(e) {
								e.preventDefault();
							    jQuery.fn.bsLightBox.anchor = obj;//_this;
							    jQuery.fn.bsLightBox.videoOpen();
							
							    return false;
							}
						}(_div.childNodes[0].childNodes[1]) : 
						function(e) {
							e.preventDefault();
						    return false;
						});
				
				if(_file.notImageLoaded) {
					_imgPath = bstdConfig.urls.sourceFiles + _file.name + "/" + _file.name;
					_img = document.createElement("img");
					_img.onload = function(parent) {
						return function(e) {
							var img = e.target;
							bstdUtils.resizeImage(112, 51, img);
							//parent.appendChild(img);
							parent.innerHTML = "<img width=\"" + img.width + "\" height=\"" + img.height + "\" src=\"" + img.src + "\" />";
						}
					}(_div.childNodes[0].childNodes[1].childNodes[0]);
					_img.src = _imgPath;
				}
				else {
					bstdUtils.resizeImage(112, 51, _file.image);
					_file.image.style.padding = "";
					_div.childNodes[0].childNodes[1].childNodes[0].appendChild(_file.image);
				}
				
				document.getElementById("edit_photo_block_" + _file.innerType).nextElementSibling.appendChild(_div.childNodes[0]);
				//selectedFileForCheckBox
			}
		},
	saveFilesWithDesc: function() {
			var _filesBuffer = bstdFileUpload.getFiles(),
				_i,
				_ele,
				_file,
				_fData = new FormData(),
				_type,
				_xR = new XMLHttpRequest();
			
			if(bstdRenderUtils.descriptionsCounter > 0) {
				alert("Необходимо заполнить описание к каждому файлу");
				return;
			}
			
			for(_i in _filesBuffer) {
				_file = _filesBuffer[_i];
				_ele = document.getElementById("fileDescription_" + _i); //_file.iIndex
				_file.desc = _ele.value;
				if(!_type) {
					_type = _file.innerType;
				}
				_fData.append("files[" + _i + "]", _file.name);
				_fData.append("descriptions[" + _i + "]", _ele.value);
			}
			_fData.append("id", bstdConfig.pressId);
			_fData.append("type", _type);
			
			$(_xR).bind("load", bstdRenderUtils.saveFilesWithDescSuccess);
			$(_xR).bind("error", bstdRenderUtils.saveFilesWithDescError);
			
			/*
			_xR.addEventListener("load", bstdRenderUtils.saveFilesWithDescSuccess, false);
			_xR.addEventListener("error", bstdRenderUtils.saveFilesWithDescError, false);
			*/
			_xR.open("post", bstdConfig.urls.saveDesc, true);
			_xR.setRequestHeader("DNT", "1");
			_xR.send(_fData);
		},
	saveFilesWithDescSuccess: function(e) {
			var _response = e.target.responseText;
			
			try {
				_response = JSON.parse(_response);
			} catch($e)
			{
				alert("При сохранении данных возникла ошибка");
				return;
			}
			
			if(_response.files) {
				bstdRenderUtils.addItemInFileHolder(_response.files);
			}
			
			bstdPopupWin.Close();
		},
	saveFilesWithDescError: function() {
			
		},
	imageResizer: function(imageObject) {
			bstdUtils.resizeImage(150, 100, imageObject);
		},
	onLoad: function() {
			$(".add-photo-button-in").click(function(e) {
				bstdRenderUtils.createFileUploader(e.target.getAttribute("data-type"));
			});
		}
};
