/**
 * Свойства форм:
 *
 * bstd_not_send_not_changed 			- Не отправлять форму, если данные не были изменены
 * bstd_send_only_changed 				- Отправлять только измененные данные
 * bstd_not_alert_is_change 			- Не предупреждать о не сохраненных измененных данных при переходе на другую страницу или ликвидации формы
 * bstd_validate_only_before_send 		- Валидировать данные только перед отправкой формы
 * bstd_asinch							- Флаг асинхронной отправки формы
 * bstd_error_type						- Способ отображения ошибок: tag (1) - в тегах, с идентификатором <element_id> + "_error", tips (2) - всплывающими подсказаками, both (3) - использовать оба списоба
 * 
 * 
 * bstd_name							- Имя поля формы для DOM-элементов, не являющихся элементами формы, например, div, span и т.д.
 * bstd_value							- Значение поля формы для DOM-элементов, не являющихся элементами формы, например, div, span и т.д.
 * bstd_type							- Тип поля формы для DOM-элементов, не являющихся элементами формы, например, div, span и т.д. или для неспецифического использования элементов формы (например, для органиации работы каптчи)
 * 
 * bstd_valide_type						- Тип валидатора
 * bstd_valide_values 					- Значения валидатора
 * bstd_valide_max						- Максимальное значение для числовых полей, полей даты и/или времени; максимальное количество символов для текстовых
 * bstd_valide_min						- Минимальное значение для числовых полей, полей даты и/или времени; минимальное количество символов для текстовых
 * bstd_validate_error					- Сообщение об ошибке валидатора
 * bstd_validate_min_error				- Сообщение об ошибке валидатора для минимального значения
 * bstd_validate_max_error				- Сообщение об ошибке валидатора для максимального значения
 * bstd_convert							- Конвертирует значения полей в специфический формат или представление.
 * bstd_control_value					- Контролирует введенные символы. Применимо для полей со свободным вводом.
 * 
 * bstd_autoupload						- Флаг асинхронной отправки файлов
 * bstd_multiselect						- Флаг множественной загрузки файлов
 * bstd_limit							- Ограничения количества загружаемых файлов
 * bstd_max								- Ограничение размера одного загружаемого файла
 * bstd_one_max							- Синоним bstd-max
 * bstd_all_max							- Ограничение размера одного загружаемого файла
 * bstd_types							- Типы загружаемых файлов
 * 
 * 
 * Формат входяших (с сервера) данных:
 * error - номер ошибки выполнения. 0 - ошибок нет
 * errors - ассоциативный массив (хэш) ошибок для формы в формате:
 * 		{
 * 			<field_name>: "error_message"
 * 		}
 * error_message - общее сообщение об ошибках (для всплывающего окна)
 * message - общее сообщение (для всплывающего окна)
 * result - данные. Единичное значение, массив, объект
 * data - возвращенные данные. При возникновении ошибки в этом объекте могут вернуться отправленные данные без обработки.
 * 
 */


var _SYS_DEBUG = true;

$.ajaxSetup({
		headers: {
			"X-Requested-With": "XMLHttpRequest"
		}
	});

/**
 * Объект системных сообщений
 */
var bstdSysMessages = {};
bstdSysMessages.errors = {};
bstdSysMessages.errors.forms = {};
bstdSysMessages.errors.forms.NotSendForNotChangedData = "Форма не может быть отправлена, поскольку данные не изменились";
bstdSysMessages.errors.forms.reqiredSelect = "Необходимо выбрать элемент из списка";
bstdSysMessages.errors.forms.reqiredText = "Необходимо ввести данные";
bstdSysMessages.errors.forms.reqiredFile = "Необходимо выбрать файл для загрузки";
bstdSysMessages.errors.forms.NotValideText = "Введены некорректные символы или значение";
bstdSysMessages.errors.forms.capthaNotValide = "Не верный код подтверждения";
bstdSysMessages.errors.forms.capthaRequired = "Необходимо ввести код подтверждения";
bstdSysMessages.errors.forms.custom = {}; //Индивидуальные сообщения об ошибках. "mnemocode" : "Error message"


bstdTypes = {};

if(!window.bstdError) {
	var bstdError = function(message, code, DOMElement, targetObject) {
		this.getMessage = function() {
			return message;
		};
		
		this.getCode = function() {
			return code;
		};
		
		this.getDOMElement = function() {
			return DOMElement;
		};
		
		this.getObject = function() {
			return targetObject;
		};
	};
}

bstdError.prototype.ERRORS_TYPE_REQUIRED = 1;
bstdError.prototype.ERRORS_TYPE_NOT_VALIDE = 2;
bstdError.prototype.ERRORS_TYPE_NOT_ACCEPTED = 3;

var bstdFormsSets = {};
bstdFormsSets.default = {
	isSendOnlyChanged: true,
	isNotSendNotChanged: false,
	method: "post",
	enctype: "auto",
	asinch: true,
	errorType: 2 
};
//использовать подсказки для отображения ошибок

if(!window["bstdElementsTypes"]) {
	var bstdElementsTypes = window["bstdElementsTypes"] = new function() {
		var sThis = this;
		
		var _advancedElementsTypes = {};
		
		this.registerElementsType = function(type, func) {
			if(typeof type == "string" && typeof func == "function")  {
				_advancedElementsTypes[type] = func;
			}
			else {
				if(_SYS_DEBUG) {
					throw new Error("Arguments error");
				}
				return false;
			}
		};
		this.has = function(type) {
			return (type && _advancedElementsTypes[type]);
		};
		
		this.create = function(type, ele, params) {
			return sThis.has(type) && ele ? new _advancedElementsTypes[type](ele, params) : undefined;
		};
	};
}


/**
 * Объект работы с формами
 */
var bstdForm = function(form, mode, dataType) {
	var sThis = this;
	var _errors = {},
		_sendData,
		_action,
		_enctype,
		_method,
		_encript,
		_asinch,
		_errorType,
		_elements = {
			___uploaders: {},
			___radio: {},
			___eles: {}
		},
		_filesUploaded = {},
		_currFormName,
		_saved,
		_customSubmit;
	
	form = $(form);
	if(!form[0].tagName || form[0].tagName.toLowerCase() != "form") {
		return false;
	}
	
	if(!form.attr("id")) {
		form.attr("id", "form_" + Math.random().replace(".", ""));
	}
	
	this.getForm = function() {
		return form;
	};
	this.customSubmit = function(func) {
		_customSubmit = func;
	};
	this.length = 0;
	this.isChange = false;
	this.isSendOnlyChanged = false;
	this.isNotSendNotChanged = false;
	
	this.getDataType = function () {
		return dataType;
	};
	
	_currFormName = form.attr("name") || null;
	
	var _createResizeEvent = function() {
		form.trigger("resize", {target: sThis, formObject: sThis, form: form, dataType: dataType});
	};
	
	this.mode = function() {
		return mode;
	};
	
	this.addElement = function(ele) {
		ele = $(ele);
		if(form.find(ele).length) {
			_registerElement(ele);
			_createResizeEvent();
		}
		return sThis;
	};
	
	this.saved = function() {
		_saved = true;
	}
	
	this.removeElement = function(ele, name) {
		var _type;
		
		if(ele) {
			ele = $(ele);
			if(form.find(ele).length) {
				if((_type = ele.attr("bstd-type"))) {
					name = ele.attr("bstd-name");
				}
				else if(ele.attr("name")) {
					name = ele.attr("name");
				}
			}
			else ele = null;
		}
		if(name) {
			if(_elements.___eles[name]) {
				ele = _elements.___eles[name];
				delete(_elements.___eles[name]);
				sThis.length --;
				_createResizeEvent();
			}
			else if(_elements.___radio[_name]) {
				ele = _elements.___radio[name];
				delete(_elements.___radio[name]);
				sThis.length --;
				_createResizeEvent();
			}
			else if(_elements.___uploaders[_name]) {
				ele = _elements.___uploaders[name];
				delete(_elements.___uploaders[name]);
				sThis.length --;
				_createResizeEvent();
			}
			else ele = null;
		}
		else ele = null;
		return ele ? ele : sThis;
	};
	
	var _registerElement = function(ele) {
		var _type,
			_name,
			_params,
			_typeObj;
		
		ele = $(ele);
		if((_type = ele.attr("bstd-type"))) {
			if(ele.attr("bstdRegistred") == "1") {
				return;
			}
			if(bstdElementsTypes.has(_type)) {
				_params = bstdTemplater.prepareTypeParams(ele.attr("bstd-type-prams"));
				_elements.___eles[ele.attr("bstd-name")] = _typeObj = bstdElementsTypes.create(_type, ele[0], _params);
				sThis.length ++;
				ele.attr("bstdRegistred", "1");
				$(ele).bind("change", function(ele) {
					return function() {
						_changeValues(ele);
					};
				}(_typeObj));
				return _typeObj;
			}
			else return false;
		}
		else {
			ele = $(ele)[0];
			_name = ele.name;
			_type = ele.type;
			//button | checkbox | file | hidden | image | password | radio | reset | select-multiple | select-one | submit | text | textarea
			if($(ele).attr("bstd-form-ignore") || !$(ele).attr("name"))
				return null;
			switch(_type) {
				case "hidden":
					_elements.___eles[_name] = ele;
					break;
				case "select-multiple":
				case "select-one":
				case "checkbox":
				case "password":
				case "textarea":
				case "text":
					ele.isChange = false;
					ele.bstdOldValue = $(ele).val();
					_elements.___eles[_name] = ele;
					//Устанавливаем валидацию
					_validator = $(ele).attr("bstd_valide_type");
					if(_validator) {
						$(ele).bind("change", function(ele) {
							return function() {
								_changeTextValidate(ele);
								_changeValues(ele);
							};
						}(ele));
					}
					else {
						$(ele).bind("change", function(ele) {
							return function() {
								_changeValues(ele);
							};
						}(ele));
					}
					break;
				case "radio":
					if(!_elements.___radio[_name]) {
						_elements.___radio[_name] = {
							isChange: false,
							currentValue: undefined,
							oldValue: undefined,
							required: !!$(ele).attr("required"),
							selected: null
						};
					}
					if(ele.checked) {
						_elements.___radio[_name].oldValue = ele.value;
						_elements.___radio[_name].selected = ele;
					}
					$(ele).bind("change", function(ele) {
						return function() {
							_changeValues(ele);
						};
					}(ele));
					break;
				case "file":
					if(!_elements.___uploaders[_name]) {
						_elements.___uploaders[_name] = {
								isChange: false,
								required: !!$(ele).attr("required"),
								files: null
						};
					}
					$(ele).bind("change", function(ele) {
						return function() {
							_changeValues(ele);
						};
					}(ele));
					break;
				default:
					return undefined;
			}
		}
		sThis.length ++;
		return $(ele);
	};
	
	
	/**
	 * Производит инициализацию формы
	 */
	var _init = function() {
		var _eles,
			_i,
			_name,
			_type,
			_ele,
			_validator;
		
		sThis.isSendOnlyChanged = window["bstdFormsSets"] && window["bstdFormsSets"]["isSendOnlyChanged"] !== undefined ? window["bstdFormsSets"]["isSendOnlyChanged"] : $(form).attr("bstd_send_only_changed") == 1;
		if(sThis.isSendOnlyChanged === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.isSendOnlyChanged !== undefined) {
			sThis.isSendOnlyChanged = bstdFormsSets.default.isSendOnlyChanged;
		}
		
		sThis.isNotSendNotChanged = window["bstdFormsSets"] && window["bstdFormsSets"]["isNotSendNotChanged"] !== undefined ? window["bstdFormsSets"]["isNotSendNotChanged"] : $(form).attr("bstd_form bstd_not_send_not_changed");
		if(sThis.isNotSendNotChanged === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.isNotSendNotChanged !== undefined) {
			sThis.isNotSendNotChanged = bstdFormsSets.default.isNotSendNotChanged;
		}
		
		sThis.action = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["action"] !== undefined ?
				window["bstdFormsSets"][_currFormName]["action"] : $(form).attr("action");
		if(sThis.action === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.action !== undefined) {
			sThis.action = bstdFormsSets.default.action;
		}
		
		sThis.enctype = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["enctype"] !== undefined ? window["bstdFormsSets"][_currFormName]["enctype"] : $(form).attr("enctype");
		if(sThis.enctype === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.enctype !== undefined) {
			sThis.enctype = bstdFormsSets.default.enctype;
		}
		
		sThis.method = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["method"] !== undefined ? window["bstdFormsSets"][_currFormName]["method"] : $(form).attr("method");
		if(sThis.enctype === undefined) {
			if(window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.method !== undefined) {
				sThis.method = bstdFormsSets.default.method;
			}
			else {
				sThis.method = sThis.METHODS.DEDAULT
			}
		}
		
		sThis.encript = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["encript"] !== undefined ? window["bstdFormsSets"][_currFormName]["encript"] : $(form).attr("encript");
		if(sThis.encript === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.encript !== undefined) {
			sThis.encript = bstdFormsSets.default.encript;
		}
		
		sThis.asinch = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["asinch"] !== undefined ? window["bstdFormsSets"][_currFormName]["asinch"] : $(form).attr("bstd_asinch");
		if(sThis.asinch === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.asinch !== undefined) {
			sThis.asinch = bstdFormsSets.default.asinch;
		}
		
		sThis.errorType = window["bstdFormsSets"] && window["bstdFormsSets"][_currFormName]  && window["bstdFormsSets"][_currFormName]["errorType"] !== undefined  ? window["bstdFormsSets"][_currFormName]["errorType"] :
			$(form).attr("bstd_error_type");
		if(sThis.errorType === undefined && window["bstdFormsSets"] && bstdFormsSets.default && bstdFormsSets.default.errorType !== undefined) {
			sThis.errorType = bstdFormsSets.default.errorType;
		}
		
		form.bind("submit", sThis.submit);
		form.bind("reset", sThis.reset);
		
		_eles = form[0].elements;
		
		$("[bstd-type]", form).each(function() {
			_registerElement(this);
		});
		
		for(_i = 0; _i < _eles.length; _i ++) {
			_registerElement(_eles[_i]);
		}
		sThis.isChange = false;
	};
	
	this.hasElement = function(name) {
		return !!sThis.element(name);
	};
	
	this.element = function(name) {
		return _elements.___eles[name] || _elements.___radio[name] || _elements.___uploaders[name] || undefined;
	};
	
	/**
	 * Устанавливает флаги "Данные изменены" у элемента и его формы
	 */
	var _changeValues = function(ele, type) {
		var _name = ele.name;
		
		sThis.isChange = true;
		if(ele.type.toLowerCase() == "radio") {
			_elements.___radio[_name].isChange = true;
			_elements.___radio[_name].currentValue = ele.value;
			_elements.___radio[_name].selected = ele;
		}
		else {
			ele.isChange = true;
		}
	};
	
	var _result = function(response) {
		if(typeof response == "object" && typeof response.responseText !== undefined) {
			response = response.responseText;
		}
		if(typeof response == "string") {
			response = $.parseJSON(response);
		}
		if(response && typeof response == "object") {
			//Проверяем общее сообщение
			if(response.message) {
				bstdAlert(response.message);
			}
			if(!response.error && response.result) {
				//_result(response);
				_saved = true;
				form.trigger("succesfullSubmiting", {form:form, result:response.result, sendData: _sendData, formObject: sThis, dataType: sThis.getDataType(), mode: sThis.mode()});
				_sendData = null;
			} else {
				if(response.errors) {
					sThis.setErrors(response.errors);
				}
				//Проверяем общее сообщение об ошибке
				if(response.error_message) {
					bstdAlert(response.error_message);
				}
			}
		}
		sThis.unblockForm();
	};
	
	this.blockForm = function() {
		form.css({position: "relative"}).append($("<div>").attr("id", "mask_" + form.attr("id")).css({position:"absolute",top:"0px",left:"0px",bottom:"0px",right:"0px",backgroundColor:"#999999",opacity: "0.2"}));
	};
	
	this.unblockForm = function() {
		$("#mask_" + form.attr("id"), form).remove();
	};
	
	this.action = function(action) {
		if(action) {
			sThis.action = action;
			return sThis;
		}
		return sThis.action;
	};
	
	this.enctype = function(enctype) {
		if(enctype) {
			sThis.enctype = enctype;
			return sThis;
		}
		return sThis.enctype;
	};

	this.method = function(method) {
		if(method) {
			sThis.method = method;
			return sThis;
		}
		return sThis.method;
	};

	this.encript = function(encript) {
		if(encript) {
			sThis.encript = encript;
			return sThis;
		}
		return sThis.encript;
	};
	
	this.name = function() {
		return form.attr("name");
	};
	
	/**
	 * Возвращает флаг "Отправлять только измененные данные"
	 */
	this.isSendOnlyChanged = function() {
		return sThis.isSendOnlyChanged;
	};
	
	/**
	 * Возвращает флаг "Не отправлять форму, если данные не были изменены"
	 */
	this.isNotSendNotChanged = function() {
		return sThis.isNotSendNotChanged;
	};
	
	var _changeTextValidate = function(ele) {
		var _value = bstdData.Validator.Validate($(ele).attr("bstd_valide_type"), $(ele).attr("bstd_valide_values"), ele.value),
		_validateError;
		if(_value === false) {
			sThis.setErrors((_validateError = $(ele).attr("bstd_validate_error")) ? _validateError : "NotValideText", ele);
			$(ele).focus();
		}
		else {
			if(ele.error) {
				sThis.clearErrors(ele);
			}
		}
	};
	

	/**
	 * Выполняет асинхронную передачу данных
	 */
	this.submit = function(e) {
		var _validateResult,
			_data = {};
		if(e) {
			e.preventDefault();
		}
		if((sThis.isSendOnlyChanged || sThis.isNotSendNotChanged) && !sThis.isChange) {
			alert(bstdSysMessages.errors.forms.NotSendForNotChangedData);
		}
		else {
			if(typeof _customSubmit == "function") {
				_customSubmit(sThis, _result);
				return false;
			}
			
			_validateResult = sThis.Valdate();
			sThis.clearErrors();
			if(_validateResult.isError) {
				sThis.setErrors(_validateResult.errors);
			}
			else {
				sThis.blockForm();
				if(_validateResult.a) {
					_data["a"] = _validateResult.a;
					delete(_validateResult.a);
				}
				if(_validateResult.b) {
					_data["b"] = _validateResult.b;
					delete(_validateResult.b);
				}
				_data["d"] = _validateResult;
				
				_sendData = _validateResult;
				
				$.ajax({
					url: 		sThis.action,
					type:		sThis.method,
					data:		{prq: $.toJSON(_data)},
					dataType:	'json',
					complete:	_result
				});
			}
		}
		return false;
	};
	
	this.reset = function(form) {
		
	};
	
	this.setHeader = function(text) {
		$(".header").html(text);
	};

	
	this.getAllData = function(form) {
		if(!(form = sThis.getForm(form)))			
			throw new Error("Object is not a form of");
	};
	
	/**
	 * Проверяет введенные данные перед отправкой формы
	 */
	this.Valdate = function() {
		var _i,
			_ele,
			_check,
			_value,
			_data = {},
			_j,
			_tagName,
			_type,
			_errors = {},
			_isError,
			_oldValue,
			_radio,
			_err,
			_validator,
			_validateError,
			_bstdType;

		var setError = function(elementName, errorType, errorMessage) {
			_isError = true;
			_errors[elementName] = errorMessage ? errorMessage : bstdSysMessages.errors.forms[errorType];
		};
		
		for(_i in _elements.___eles) {
			_ele = _elements.___eles[_i];
			
			if($(_ele).attr("bstd-form-ignore"))
				continue;
			//button | checkbox | file | hidden | image | password | radio | reset | select-multiple | select-one | submit | text | textarea
			if(sThis.isSendOnlyChanged) {
				if(_ele.isChange === false) {
					if(mode == sThis.MODE_ADD) {
						if(_ele.attr("required")) {
							setError(_ele.name, bstdError.ERRORS_TYPE_REQUIRED, _ele.type == "select-multiple" || _ele.type == "select-one" ? "reqiredSelect" : "reqiredText");
						}
					}
					continue;
				}
			}
			
			if(_ele.bstdType) {
				_value = _ele.val();
				if(!_ele.validate()) {
					if(_ele.required) {
						setError(_ele.name, bstdError.ERRORS_TYPE_NOT_VALIDE, _ele.getErrorMessage());
					}
					continue;
				}
			}
			else {
				switch(_ele.type) {
					case "select-multiple":
						if(_ele.selectedIndex > -1) {
							_value = [];
							for(_j = 0; _j < _ele.options.length; _j ++){
								_option = _ele.options[_j];
								if(_option.selected && !$(_option).attr("disabled")) {
									if(_option.value !== undefined && _option.value !== "") {
										_value.push(_option.value);
									}
									else if(_option.text !== "") {
										_value.push(_option.text);
									}
								}
							}
						}
						else if($(_ele).attr("required")) {
							setError(_ele.name, bstdError.ERRORS_TYPE_REQUIRED, "reqiredSelect");
						}
						break;
					case "select-one":
						if(_ele.selectedIndex > -1) {
							_option = _ele.options[_ele.selectedIndex];
							if(!$(_option).attr("disabled")) {
								if(_option.value !== undefined && _option.value !== "") {
									_value = _option.value;
								}
								else if(_option.text !== "") {
									_value = _option.text;
								}
							}
						}
						else if($(_ele).attr("required")) {
							setError(_ele.name, bstdError.ERRORS_TYPE_REQUIRED, "reqiredSelect");
						}
						break;
					/*
					case "radio":
					case "file":
					*/
					case "button":
						continue;
						break;
					case "text":
					case "textarea":
						_validator = $(_ele).attr("bstd_valide_type");
						if(_validator) {
							_value = $(_ele).val();
							if(bstdData.Validator.Validate(_validator, $(_ele).attr("bstd_valide_values"), _value) === false) {
								setError(_ele.name, bstdError.ERRORS_TYPE_NOT_VALIDE, "NotValideText");
								continue;
							}
						}
						else {
							if((_value = $(_ele).val()) === "") {
								if($(_ele).attr("required")) {
									setError(_ele.name, bstdError.ERRORS_TYPE_REQUIRED, "reqiredText");
								}
							}
						}
						break;
					case "checkbox":
						_value = _ele.value;
						break;
					case "hidden":
						//if(_ele.oldValue != _ele.value) {
							_value = _ele.value;
						//}
						break;
					default:
						continue;
				}
			}
			
			if(sThis.isSendOnlyChanged && _ele.isChange || !sThis.isSendOnlyChanged) {
				_data[_ele.name] = _value;
			}
			_value = null;
		}
		
		for(_i in _elements.___radio) {
			_radio = _elements.___radio[_i];
			if(_radio.isChange) {
				_data[_ele.name] = _radio.currentValue;
			}
			else {
				if(_radio.required && (_radio.currentValue === undefined
						|| _radio.currentValue !== undefined && $(_radio.selected).attr("disabled") === undefined)) {
					setError(_ele.name, bstdError.ERRORS_TYPE_REQUIRED, "reqiredSelect");
				}
				else if(!sThis.isSendOnlyChanged) {
					_data[_ele.name] =  _radio.currentValue !== undefined && $(_radio.selected).attr("disabled") !== undefined ? _radio.currentValue : "";
				}
			}
		}
		
		//TODO: Проверка файлов
		
		return _isError ? {isError: true, errors: _errors} : _data;
	};
	
	/**
	 * Устанавливает сообщения об ошибках. 
	 */
	this.setErrors = function(form, errors, ele) {
		var _elementName,
			_errorContainer,
			_eles,
			_i;
		if(!ele) {
			if(typeof errors == "object") {
				for(_elementName in errors) {
					if(sThis.errorType == 1 || sThis.errorType == 3) {
						if(_errorContainer = _getElementErrorContainer(_elementName)) {
							$(_errorContainer).html(errors[_elementName]);
						}
					}
					_eles = form[_elementName];
					if(_eles.tagName) {
						_eles = [_eles];
					}
					for(_i = 0; _i < _eles.length; _i ++) {
						if(sThis.errorType == 2 || sThis.errorType == 3) {
							if(window["bstdTips"]) {
								bstdTips.unset(_eles[_i]);
								bstdTips.set(_eles[_i], errors[_elementName]);
							}
							else if(_SYS_DEBUG) {
								throw new Error("Arguments errors must by object");
							}
							$(_eles[_i]).addClass("error");
							_eles[_i].error = true;
						}
					}
				}
			}
			else {
				if(_SYS_DEBUG) {
					throw new Error("Arguments errors must by object");
				}
				return false;
			}
		}
		else {
			_elementName = ele.name;
			if(sThis.errorType == 1 || sThis.errorType == 3) {
				if(_errorContainer = _getElementErrorContainer(_elementName)) {
					$(_errorContainer).html(errors);
				}
			}
			if(sThis.errorType == 2 || sThis.errorType == 3) {
				if(window["bstdTips"]) {
					bstdTips.unset(ele);
					bstdTips.set(ele, errors);
				}
				else if(_SYS_DEBUG) {
					throw new Error("Arguments errors must by object");
				}
			}
			$(ele).addClass("error");
			ele.error = true;
		}
	};
	
	/**
	 * Очищает сообщения об ошибках
	 */
	this.clearErrors = function(ele) {
		var _element,
			_i,
			_frm;
		if(ele) {
			if(ele.error) {
				$(ele).removeClass("error");
				
				if(sThis.errorType == 1 || sThis.errorType == 3) {
					if(_errorContainer = _getElementErrorContainer(ele.name)) {
						$(_errorContainer).html("");
					}
				}
				if(sThis.errorType == 2 || sThis.errorType == 3) {
					if(window["bstdTips"]) {
						bstdTips.unset(ele, true);
					}
					else if(_SYS_DEBUG) {
						throw new Error("Arguments errors must by object");
					}
					$(ele).removeClass("error");
					ele.error = false;
				}
			}
		}
		else {
			_frm = form.get(0);
			for(_i = 0; _i < _frm.elements.length; _i ++) {
				ele = _frm.elements[_i];
				if(!ele.error) {
					continue;
				}
				$(ele).removeClass("error");
				
				if(sThis.errorType == 1 || sThis.errorType == 3) {
					if(_errorContainer = _getElementErrorContainer(ele.name)) {
						$(_errorContainer).html("");
					}
				}
				if(sThis.errorType == 2 || sThis.errorType == 3) {
					if(window["bstdTips"]) {
						bstdTips.unset(ele, true);
					}
					else if(_SYS_DEBUG) {
						throw new Error("Arguments errors must by object");
					}
					$(ele).removeClass("error");
					ele.error = false;
				}
			}
		}
	};
	
	var _getElementErrorContainer = function(elementName) {
		var _element;
		if(typeof elementName == "string") {
			_element = form[elementName];
		}
		else {
			_element = elementName;
		}
		if(typeof _element == "object" && _element.tagName) {
			return $((_element.id ? _element.id : (form.attr("name") ? form.attr("name") : form.attr("id") ? form.attr("id") : "") + _element.name) + "_error")[0];
		}
		else {
			if(_SYS_DEBUG) {
				throw new Error("Element name '" + elementName + "' is error or element not found");
			}
			return false;
		}
	};
	
	this.close = function() {
		var _r;
		
		if(form.trigger("close", {formObject: sThis, form: form, dataType: dataType}) === false) {
			return false;
		}
		if(sThis.isChange && !_saved) {
			_r = confirm("Данные не сохранены. Вы уверены, что хотите закрыть форму?");
		}
		if(_r === false) {
			return false;
		}
		form.trigger("afterClose", {formObject: sThis, form: form, dataType: dataType});
	}
	
	_init();
	delete(_init);
};
bstdForm.prototype.MODE_ADD = 1;
bstdForm.prototype.MODE_EDIT = 2;
bstdForm.prototype.MODE_VIEW = 3;

var bstdTemplater = new function() {
	var sThis = this;
	
	var _calc = (new function() {
		var cThis = this;
		
		this.currentData = function() {
			return new Data();
		};
		
	});
	
	this.calc = function(def){
		var _func = /^\s*_calc\s*:\s*([\w\d\-_]+)\s*/.exec(def);
		return _func === null ? def : _calc[_func] ? _calc[_func]() : undefined;
	};
	
	this.prepareTypeParams = function(paramsString) {
		var _params = {},
			_i,
			_len,
			_curr,
			_val;
		if(!paramsString) {
			return _params;
		}
		paramsString = paramsString.split(/\s*,\s*/);
		
		if(paramsString && (_len = paramsString.length)) {
			for(_i = 0; _i < _len; _i ++) {
				_curr = paramsString[_i].split(/\s*:\s*/);
				if(_curr && _curr.length) {
					_params[_curr[1]] = sThis.calc(_curr);
				}
			}
		}
		return _params;
	};
};