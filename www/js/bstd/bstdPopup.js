var _bstdPopupTemplate = "<div class=\"bstdPopup\"><div class=\"closeButton\"></div><div class=\"title\"></div><div class=\"content\"></div><div class=\"buttons\"></div></div>";
var bstdPopup = function(title, content, mode, params, toContent, permanents) {
	var sThis = this,
		_popup = $(_bstdPopupTemplate),
		_mask,
		_btn,
		_offset,
		_j,
		_button,
		_c;
	if(mode && mode == 1 && !permanents) {
		_mask = bstdMask.create();
	}
	
	this.getContentSize = function() {
		var _content = $($(".content", _popup)[0]);
		return {
			height:_content.innerHeight(),
			width:_content.innerWidth()
		};
	};
	
	if(params.onclose) {
		params.onClose = params.onclose;
		delete(params.onclose);
	}
	
	this.close = function() {
		if(permanents) {
			if(params.onClose || typeof params.onClose == "function") {
				if(params.onClose(_popup) === false) {
					return;
				}
				else _popup.hide();
			}
			_popup.hide();
		}
		else {
			if(params.onClose || typeof params.onClose == "function") {
				if(params.onClose(_popup) === false) {
					return;
				}
				else _popup.remove();
			}
			else _popup.remove();
		}
		if(_mask) _mask.close();
	};
	
	if(params) {
		if(params.width) {
			_popup.css({width:params.width});
		}
		if(params.height) {
			_popup.css({height:params.height});
		}
		if(params.button) {
			$(".buttons", _popup).append((_btn = $("<input class=\"button\" type=\"button\" />")).val(params.button).click(this.close));
		}
		if(params.buttons && params.buttons.length) {
			for(_j = 0; _j < params.buttons.length; _j ++) {
				_button = params.buttons[_j];
				$(".buttons", _popup).append((_btn = $("<input class=\"button\" type=\"button\" />")));
				_btn.val(_button.text).click(_button.callback ? function(callback, value) {return function() {if(callback(value) !== false) sThis.close();};} (_button.callback, _button.value) : sThis.close);
			}
		}
	}
	if(permanents) {
		this.open = function() {
			_popup.css({
				top: (params ? (params.toLocation ? $(document).scrollTop() : (params.top ? params.top : "80")) : "80") + "px",
				left: (parseInt(($(window).width() - _popup.width()) / 2) + "px")
			});
			_popup.show();
			sThis.resize();
		};
		_popup.hide();
	}
	$("body").append(_popup);
	if(title) {
		$(".title", _popup).html(title);
	}
	$(".content", _popup).html(content);
	
	$(".closeButton", _popup).click(this.close);
	
	if(toContent) {
		_c = $($(".content", _popup)[0]);
		_c.css("width", $(_c.children().first()).outerWidth() + "px");
		
		_popup.css("width", _c.outerWidth() + "px");
	}
	
	_offset = _popup.offset();
	_popup.css({
		top: (params ? (params.toLocation ? $(document).scrollTop() : (params.top ? params.top : "80")) : "80") + "px",
		left: (parseInt(($(window).width() - _popup.width()) / 2) + "px")
	});
	
	this.resize = function() {
		if(toContent) {
			_c = $($(".content", _popup)[0]);
			_c.css("width", $(_c.children().first()).outerWidth() + "px");
			
			_popup.css("width", _c.outerWidth() + "px");
		}
		
		_offset = _popup.offset();
		_popup.css({
			top: (params ? (params.toLocation ? $(document).scrollTop() : (params.top ? params.top : "80")) : "80") + "px",
			left: (parseInt(($(window).width() - _popup.width()) / 2) + "px")
		});
	};
	
	this.getContainer = function() {
		return _popup;
	};
};

var bstdMask = (new function() {
	var _maskTemlate = "<div class=\"bstdMask\"></div>";
		
	this.create = function() {
		var _mask = $(_maskTemlate);
		$("body").append(_mask);
		_mask.close = function() {_mask.remove();};
		return _mask;
	};
	
	this.close = function(_mask) {
		_mask.remove();
	};
});


var bstdAlert = function(message, head) {
	new bstdPopup(head, message, 1, {button:"Закрыть"});
};

var bstdConfirm = function(message, head, callback) {
	new bstdPopup(head, message, 1, {buttons:[{text:"Да", callback: callback, value: true},{text:"Нет", callback: callback, value: false}]});
};
