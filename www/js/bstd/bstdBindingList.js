var bstdBindingList = function(ele) {
		var sThis = this,
			_ele = $(ele),
			_type = ele["bstdType"] = "bstdBindingList",
			_source = $("[bstd-bindlist-source]", _ele),
			_distination = $("[bstd-bindlist-distination]", _ele),
			_bASel = $("[bstd-bindlist-addselect]", _ele),
			_bAAll = $("[bstd-bindlist-addall]", _ele),
			_bDSel = $("[bstd-bindlist-delselect]", _ele),
			_bDAll = $("[bstd-bindlist-delall]", _ele),
			_oldSource = [],
			_oldDist = [],
			_name = _ele.attr("bstd-name"),
			_isReset = false,
			_errorMessage = "",
			_sourceDirect = 1,
			_distinationDirect = 1,
			_sourceIsString = true,
			_distinationIsString = true;

		delete(ele);
		
		this.ignored = _ele.attr("bstd-form-ignore");
		this.isChange = false;
		this.name = _name;
		this.required = !!(_ele.attr("bstd-required") !== undefined ? _ele.attr("bstd-required") : _ele.attr("required") !== undefined ? _ele.attr("required") : false);
		this.type = "bstdBindingList";
		
		_source.dblclick(function(e) {
			_distination.append(e.target);
			_distination[0].selectedIndex = -1;
			sThis.sortDistination();
			_ele.trigger("change", {target:sThis, type: "appendDistination", item: e.target});
			_ele.isChange = true;
		});
		
		_distination.dblclick(function(e) {
			_source.append(e.target);
			_source[0].selectedIndex = -1;
			sThis.sortSource();
			_ele.trigger("change", {target:sThis, type: "appendSource", item: e.target});
			_ele.isChange = true;
			sThis.isChange = true;
		});
		
		_bASel.click(function(e) {
			_distination.append($("option:selected", _source));
			_distination[0].selectedIndex = -1;
			sThis.sortDistination();
			_ele.trigger("change", {target:sThis, type: "appendDistination", item: e.target});
			_ele.isChange = true;
			sThis.isChange = true;
		});
		
		_bAAll.click(function(){
			_distination.append($("option", _source));
			_distination[0].selectedIndex = -1;
			sThis.sortDistination();
			_ele.trigger("change", {target:sThis, type: "appendDistination", item: e.target});
			_ele.isChange = true;
			sThis.isChange = true;
		});
		
		_bDSel.click(function(e) {
			_source.append($("option:selected", _distination));
			_source[0].selectedIndex = -1;
			sThis.sortSource();
			_ele.trigger("change", {target:sThis, type: "appendSource", item: e.target});
			_ele.isChange = true;
			sThis.isChange = true;
		});
		
		_bDAll.click(function(){
			_source.append($("option", _distination));
			_source[0].selectedIndex = -1;
			sThis.sortSource();
			_ele.trigger("change", {target:sThis, type: "appendSource", item: e.target});
			_ele.isChange = true;
			sThis.isChange = true;
		});
		
		_source.attr("bstd-form-ignore", 1);
		_distination.attr("bstd-form-ignore", 1);
		_bASel.attr("bstd-form-ignore", 1);
		_bAAll.attr("bstd-form-ignore", 1);
		_bDSel.attr("bstd-form-ignore", 1);
		_bDAll.attr("bstd-form-ignore", 1);
		
		var _addOption = function(sel, item) {
			var _opt;
			
			if(!item) {
				return;
			}
			if(item instanceof jQuery.fn.init) {
				item = item[0];
			}
			if(item.tagName && item.tagName.toLowerCase() == "option") {
				_opt = item;
			}
			else {
				_opt = $("<option>");
				if(item.value) {
					_opt.val(item.value);
				}
				_opt.text(item.text);
			}
			sel.append(_opt);
			return _opt;
		};
		
		this.onChange = function(callback) {
			if(typeof callback == "function") {
				_ele.bind("change", callback);
			}
			return sThis;
		};
		
		var _addArray = function(sel, items, old) {
			var _j,
				_group;
			
			for(_j = 0; _j < items.length; _j++) {
				old.push(_addOption(sel, items[_j]));
			}
		};
		
		this.val = function() {
			var _vals = [];
			$("option", _distination).each(function() {
				_vals.push($(this).val());
			});
			return _vals;
		};
		
		this.loadSource = function(items) {
			var _opt;
			
			if(items instanceof Array || items instanceof jQuery.fn.init) {
				_addArray(_source, items, _oldSource);
			}
			else if(typeof items == "object") {
				for(_j in items) {
					_group = $("<optgroup>");
					_group.text(items[_j].label);
					if(items[_j].items) {
						_addArray(_group, items[_j].items, _oldSource);
						_source.append(_group);
					}
				}
			}
			sThis.sortSource();
			if(!_isReset) {
				_ele.trigger("load", {target:sThis, loaded: "source"});
			}
			return sThis;
		};
		
		this.loadDistination = function(items) {
			var _opt;
			
			if(items instanceof Array || items instanceof jQuery.fn.init) {
				_addArray(_distination, items, _oldDist);
			}
			else if(typeof items == "object") {
				for(_j in items) {
					_group = $("<optgroup>");
					_group.text(items[_j].label);
					if(items[_j].items) {
						_addArray(_group, items[_j].items, _oldDist);
						_distination.append(_group);
					}
				}
			}
			sThis.sortDistination();
			if(!_isReset) {
				_ele.trigger("load", {target:sThis, loaded: "distination"});
			}
			return sThis;
		};
		
		this.validate = function() {
			return true;
		};
		
		this.name = function() {
			return _name;
		};
		
		this.clear = function() {
			_source.empty();
			_distination.empty();
			_ele.isChange = false;
			return sThis;
		};
		
		this.reset = function() {
			sThis.clear();
			_isReset = true;
			if(_oldDist.length) {
				sThis.loadDistination(_oldDist);
			}
			if(_oldSource.length) {
				sThis.loadSource(_oldSource);
			}
			_isReset = false;
			return sThis;
		};
		
		this.getErrorMessage = function() {
			return _errorMessage;
		};
		
		this.sortSource = function(direct, isString) {
			if(direct) {
				_sourceDirect = direct;
			}
			if(isString !== undefined) {
				_sourceIsString = !!isString;
			}
			
			_source.append(_sort($("option", _source), _sourceDirect, _sourceIsString));
			return sThis;
		};
		
		this.sortDistination = function(direct, isString) {
			if(direct) {
				_distinationDirect = direct;
			}
			if(isString !== undefined) {
				_distinationIsString = !!isString;
			}
			_distination.append(_sort($("option", _distination), _distinationDirect, _distinationIsString));
			return sThis;
		};
		
		var _sort = function(options, direct, isString) {
			if(!options) {
				return returnArray ? [] : null;
			}
			if(!direct) {
				direct = 1;
			}
			
			return options.sort(function(a,b) {
				return direct * (isString ? $(a).text().localeCompare($(b).text()): (parseInt($(a).text().replace(/[^0-9]+/g, ''), 10) > parseInt($(b).text().replace(/[^0-9]+/g, ''), 10)) ? 1 : -1);
			});
		};
		
		this.sourceElement = function() {
			return _source;
		};
		
		this.distinationElement = function() {
			return _distination;
		};
};

if(window["bstdElementsTypes"]) {
	bstdElementsTypes.registerElementsType("bstdBindingList", bstdBindingList);
}