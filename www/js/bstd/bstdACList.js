var bstdACList = function(params) {
	var sThis = this;
	var _id = params.id,
		_text = params.text,
		_list = $("<select class=\"bstdACListContainer\" style=\"display: none;\">"), //$("<ui class=\"bstdACListContainer\" style=\"display: none;\">"),
		//_data = params.data ? params.data : null,
		_request = params.request ? params.request : null,
		_mode = params.mode ? params.mode : 1,
		_textField = $(params.ele),
		_currentValue,
		_pos = params.position ? params.position : null;
	
	if(!_textField.length)
		_textField = null;
	
	$("body").append(_list);
	
	var _sort = function() {
		
	};
	
	var _findItems = function(text) {
		var _ch = _list.children().hide(),
			_i,
			_cnt = false,
			_txt,
			_length,
			_item,
			_a;
		
		text = text.replace(/^\s*/, "\1").replace(/\s*$/, "").toLowerCase();
		_length = text.length;
		if(!_length) return false;
		
		for(_i = 0; _i < _ch.length; _i ++) {
			_item = $(_ch[_i]);
			_txt = _item.text().substr(0, _length).toLowerCase();
			_a = text.localeCompare(_txt);
			if(_a == 1) {
				continue;
			}
			else if(_a == 0) {
					_item.show();
					_cnt = true;
			}
			else if(_a == -1) {
				break;
			}
		}
		
		return _cnt;
	};
	
	this.Load = function(data) {
		if(data) {
			data.each(function(){
				//_list.append($("<li data-item_id=\"" + this[_id] + "\">").text(this[_text]));
				_list.append($("<option value=\"" + this[_id] + "\">").text(this[_text]).hide());
			});
		}
	};
	
	this.Open = function(text) {
		var _offset;
		
		if(typeof text != "string" || !text.length) return;
		
		_position();
		
		if(mode == 1) {
			if(_findItems(text))
				_list.show();
		}
	};
	
	this.Close = function() {
		_list.hide();
	};
	
	var _keyPress = function(e) {
		sThis.Open(_textField.val());
	};	
	
	if(_textField) {
		
		$(ele).bind("keypress", _keyPress);
	}
	
	var _position = function() {
		var _p = {},
			_o;

		if(!_pos && _textField) {
			_o = _textField.offset();
			_p.top = _o.top + _textField.height + "px";
			_p.left = _o.left + "px";
			_list.css({top: _o.top + "px", left: _o.left + "px"});
		}
	};
	
	var _listSelect = function() {
		if(_textField) {
			_textField.val(_listSelect.val());
		}
		
		_position();
		
		_listSelect.hide();
	};
	
	_list.bind("change", _listSelect);
	
	if(params.data) {
		this.Load(params.data);
	}
	else if(_request && mode == 1) {
		
	}
	
	
	if(_textField) {
		
	}
	
	if(_pos) {
		if(_pos.top) {
			_list.css({top: _pos.top + "px"});
		}
		if(_pos.left) {
			_list.css({left: _pos.left + "px"});
		}
	}
	
	delete(params);
};


var bstdTextFieldControl = function(ele, params) {
	
	var _keyDown = function(e) {
		
	};
	
	var _keyPress = function(e) {
		
	};
	
	$(ele).bind("keydown", _keyDown).bind("keypress", _keyPress);
};