<?php
date_default_timezone_set('Europe/Moscow');

if(preg_match('/___view_error=1/iu', $_SERVER["REQUEST_URI"], $Z))
{
	error_reporting(E_ALL);
	ini_set("display_errors","1");
	ini_set("display_startup_errors","1");
	ini_set('error_reporting', E_ALL);
	define("SYSTEM_ON_DEBUG",	1);
	//echo "+++";
}
else
{
	define("SYSTEM_ON_DEBUG",	0);
}

if(preg_match('/\/?z(\/admin(?:\/.)*)/iu', $_SERVER["REQUEST_URI"], $Z))
{
    header("Location:".(isset($_SERVER["HTTPS"]) ? "https://" : "http://").$_SERVER["HTTP_HOST"].$Z[1]);
    exit;
}

if(substr($_SERVER['DOCUMENT_ROOT'], -1, 1) != "/") $_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT']."/";

define("SYSTEM_LOCATION_NETBOOK",					1);
define("SYSTEM_LOCATION_WORK",						2);
define("SYSTEM_LOCATION_SERVER_DEV",				3);
define("SYSTEM_LOCATION_SERVER_PROD",				4);
define("SYSTEM_LOCATION_HOME",						5);
define("SYSTEM_LOCATION_PRODACTION",				6);

define("SYSTEM_LOCATION_CURRENT",					SYSTEM_LOCATION_PRODACTION);
define("PATH_ROOT",									"/work/www/lexus/content.lexus-russia.ru/");
define("PATH_STATIC",								"/work/www/lexus/content.lexus-russia.ru/");
define("URL_SA",									"content");

$s = mb_strpos($_SERVER["REQUEST_URI"], "z/");
$_SERVER["REQUEST_URI"] = mb_substr($_SERVER["REQUEST_URI"], $s + 1, mb_strlen($_SERVER["REQUEST_URI"]) - $s);

require_once(PATH_ROOT.'libraries/Constatns.php');
require_once(PATH_LIBS."Sets.php");
/*
	try {
		session_save_path("/work/www/toyota/promo.toyota.ru/session/");
	}
	catch(dmtException $e)
	{
		if(!file_exists("/work/www/toyota/promo.toyota.ru/session/"))
		{
			mkdir("/work/www/toyota/promo.toyota.ru/session/");
			session_save_path("/work/www/toyota/promo.toyota.ru/session/");
		}
	}
	*/
WS::Init()->Dispatcher();