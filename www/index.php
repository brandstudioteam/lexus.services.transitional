<?php
date_default_timezone_set('Europe/Moscow');

if(preg_match('/___view_error=1/iu', $_SERVER["REQUEST_URI"], $Z))
{
	ini_set("display_errors","1");
	ini_set("display_startup_errors","1");
	ini_set('error_reporting', E_ALL);
	define("SYSTEM_ON_DEBUG",	1);
}
else
{
	define("SYSTEM_ON_DEBUG",	0);
}

if(preg_match('/\/?z(\/admin(?:\/.)*)/iu', $_SERVER["REQUEST_URI"], $Z))
{
    header("Location:".(isset($_SERVER["HTTPS"]) ? "https://" : "http://").$_SERVER["HTTP_HOST"].$Z[1]);
    exit;
}

if(substr($_SERVER['DOCUMENT_ROOT'], -1, 1) != "/") $_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT']."/";

define("SYSTEM_LOCATION_DEVELOPMENT",				1);
define("SYSTEM_LOCATION_PRODACTION",				2);

define("SYSTEM_LOCATION_CURRENT",					SYSTEM_LOCATION_DEVELOPMENT);

/*
if(SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_PRODACTION)
{
	define("PATH_ROOT",									"/");
	define("PATH_STATIC",								dirname(__FILE__));
	define("URL_SA",									"services");
}
 else
{
	define("PATH_ROOT",									realpath('../')."/");
	define("PATH_STATIC",								PATH_ROOT."www/");
	define("URL_SA",									"current_services");
}
 * 
 */
define("PATH_ROOT",									realpath('../')."/");
define("PATH_STATIC",								dirname(__FILE__));
define("URL_SA",									"current_services");

$s = mb_strpos($_SERVER["REQUEST_URI"], "z/");
$_SERVER["REQUEST_URI"] = mb_substr($_SERVER["REQUEST_URI"], $s + 1, mb_strlen($_SERVER["REQUEST_URI"]) - $s);

require_once(PATH_ROOT.'libraries/Constatns.php');
require_once(PATH_LIBS."Sets.php");

WS::Init()->Dispatcher();