<?php
if(!defined("PARTNERS_SERVICE_ID"))
	define("PARTNERS_SERVICE_ID",												7);


class ModelsProcessor extends Processor
{
	/**
	 *
	 * @var ModelsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var ModelsData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return ModelsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new ModelsData();
	}


	public function GetModelName($Model)
	{
		return $this->CData->GetModelName($Model);
	}

	public function GetModels($Actuality = null)
	{
		return $this->CData->GetModels($Actuality);
	}

	public function GetSubmodel($Model = null, $Find = null, $Actuality = null)
	{
		if(SITE_CURRENT != SITE_LEXUS)
			throw new dmtException("Site error");
		return $this->CData->GetSubmodel($Model, $Actuality, $Find);
	}

    public function GetSubmodelName($Submodel)
    {
        return $this->CData->GetSubmodelName($Submodel);
    }

    public function GetSubmodelData($Submodel)
    {
        return $this->CData->GetSubmodelData($Submodel);
    }

    public function GetSubmodelAlias($Submodel)
    {
        return $this->CData->GetSubmodelAlias($Submodel);
    }

	
	
	/*
	 * Методы для работы с базой references
	 */
	/**
	 * Возвращает список моделей. В идеологии Лексус это субмодели
	 * @param integer $Actuality - Идентификатор актуальности моделей
	 * @param integer $ModelFamily - Идентификатор семейтва моделей. Действительно только для Луксус
	 * @param integer $Model - Идентификатор модели. Если задан, остальные аргументы игнорируется, вернется одна запись
	 * @return array
	 */
	public function GetModelsNew($Facility = null, $Actuality = null, $ModelFamily = null, $Model = null)
	{
		$this->Dump(__METHOD__.": ".__LINE__, "+++++++++++");
		return $this->CData->GetModelsNew(WS::Init()->GetBrandId(), $Facility, $Actuality, $ModelFamily, $Model);
	}
	/**
	 * Возвращает список моделей для импорта данных из файлов
	 * @return type
	 */
	public function GetModelsNewForImport()
	{
		return $this->CData->GetModelsNewForImport(WS::Init()->GetBrandId());
	}
	/**
	 * Возвращает список семейств моделей. В идеологии Лексус это собственно модели
	 * @param type $Actuality
	 * @param type $ModelFamily
	 * @return type
	 */
	public function GetModelsFamilies($Facility = null, $Actuality = null, $ModelFamily = null)
	{
		return $this->CData->GetModelsFamilies(WS::Init()->GetBrandId(), $Facility, $Actuality, $ModelFamily);
	}
	
	public function GetFacilitiesModel($Brand, $Facility)
	{
		return $this->CData->GetFacilitiesModel($Brand, $Facility);
	}

	public function GetOldId($Model)
	{
		return $this->CData->GetOldId($Model);
	}
	
		
	public function GetModelsFamiliesFacilities($Facility)
	{
		PermissionsProcessor::Init()->CheckAccess(array(
													PERMISSIONS_MODELS_FAMILIES_FACILITY_RELATED,
													PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_COUNT_MANAGEMENT));
		return $this->CData->GetModelsFamiliesFacilities($Facility);
	}
	
	public function SaveModelsFamiliesFacilities($Facility, $ModelFamily, $Status, $Page, $Order, $Comment, $Image)
	{
$this->Dump(__METHOD__.": ".__LINE__, "+++++++++++++++++", $Facility, $ModelFamily, $Status, $Page, $Order, $Comment, $Image);
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_MODELS_FAMILIES_FACILITY_RELATED);
		$this->CData->SaveModelsFamiliesFacilities(WS::Init()->GetBrandId(), $Facility, $ModelFamily, $Status, $Page, $Order, $Comment, $Image[0]);
		return $this->CData->GetModelsFamiliesFacilities($Facility, $ModelFamily);
	}

	
	public function GetModelsFacilities($Facility)
	{
		PermissionsProcessor::Init()->CheckAccess(array(
													PERMISSIONS_MODELS_FAMILIES_FACILITY_RELATED,
													PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_COUNT_MANAGEMENT));
		return $this->CData->GetModelsFacilities($Facility);
	}
	
	public function SaveModelsFacilities($Facility, $Model, $Status, $Page, $Order, $Comment, $Image)
	{
$this->Dump(__METHOD__.": ".__LINE__, "+++++++++++++++++", $Facility, $Model, $Status, $Page, $Order, $Comment, $Image);
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_MODELS_FAMILIES_FACILITY_RELATED);
		$this->CData->SaveModelsFacilities(WS::Init()->GetBrandId(), $Facility, $Model, $Status, $Page, $Order, $Comment, $Image[0]);
		return $this->CData->GetModelsFacilities($Facility, $Model);
	}
}