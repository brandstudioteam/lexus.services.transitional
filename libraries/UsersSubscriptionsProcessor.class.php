<?php
define("DB_VARS_SUBSCRIBTION_MEMBER",               "@SubscrMember");
define("SUBSCRIBTION_MEMBER_AUTH_URL",              "http://contacts.toyota.ru/z/subscribe/cabinet/auth/");
define("SUBSCRIBTION_MEMBER_CABINET_URL",           "http://contacts.toyota.ru/z/subscribe/cabinet/");


class UsersSubscriptionsProcessor extends Processor
{
	/**
	 *
	 * @var UsersSubscriptionsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var UsersSubscriptionsData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return UsersSubscriptionsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new UsersSubscriptionsData();
	}

	public function SubscribeNew(	$LastName, $FirstName, $MiddleName, $Gender, $Email, $Age = null, $MaritalStatus = null, $CompanyType = null,
									$Occupation = null, $CarType = null, $Dealer = null, $Themes = null, $Model = null, $IP = null)
	{
		return $this->Subscribe($LastName, $FirstName, $MiddleName, $Gender, $Email, $Age, $MaritalStatus,
								$CompanyType, $Occupation, $CarType, $Dealer, $Themes, $Model, $IP);
	}

    protected function CreatePassword()
    {
        $R = substr(base64_encode(pack("H*", sha1(mt_rand()))), 0, 6);
        return str_replace(array('+', '/', '='), array('1', '2', '3'), $R);
    }

	public function Subscribe(	$LastName, $FirstName, $MiddleName, $Gender, $Email, $Age = null, $MaritalStatus = null, $CompanyType = null,
								$Occupation = null, $CarType = null, $Dealer = null, $Themes = null, $Model = null, $IP = null)
	{
		if(!$IP) $IP = $_SERVER['REMOTE_ADDR'];

        $Password = $this->CreatePassword();
        $ECode = sha1($Password);//sha1($Password.PASSWORD_PREFIX);
        $UCode = substr(base64_encode($Email.PASSWORD_PREFIX.$Password.microtime(true).$LastName), 0, 30);
        $UCode = str_replace(array('+', '/', '='), array('1', '2', '3'), $UCode);

		$Member = $this->CData->Subscribe(	WS::Init()->GetBrandId(), $LastName, $FirstName, $MiddleName, $Gender, $Email, $Age, $MaritalStatus,
											$CompanyType, $Occupation, $CarType, $Dealer, $Themes, $Model, $UCode, $ECode, $IP);
        if($Member)
            $this->SendRegistredEmail($Password);
        return array("type" => $Member ? 1 : 2);
	}

	public function UnSubscribe($UCode, $Email)
	{
        try
        {
            $this->CData->UnSubscribe($Email, $UCode);
            $R = 'Адрес '.$Email.' удален из списка информационной рассылки Toyota.';
        }
        catch(dmtException $e)
        {
            $R = 'Во время удаления адреса '.$Email.' из списка информационной рассылки Toyota произошла ошибка.<br />Повторите операцию позднее или обратитесь в службу технической поддержки.<br /><br />Приносим извинения за неудобства';
        }

        if(isset($_SESSION["SUBSCRIBTION_MEMBER"]))
            unset($_SESSION["SUBSCRIBTION_MEMBER"]);
        return array("message"  => $R,
                     "email"    => $Email);
	}

	public function GetThemesForModel($Model)
	{
		return $this->CData->GetThemesForModel($Model);
	}

    public function GetSubscribeTheme($Status = null)
    {
		$this->Dump(__METHOD__);
        $R = $this->CData->GetSubscribeTheme(WS::Init()->GetBrandId(), $Status);
		$this->Dump(__METHOD__, $R);
		return $R;
    }

    public function SavePersonalData($LastName, $FirstName, $MiddleName, $Gender, $Email, $Age, $MaritalStatus, $CompanyType, $Occupation, $CarType, $Dealer = null, $Themes = null, $Model = null, $Stop = null)
    {
        $this->CheckAuth();
        $Stop = $Stop ? 2 : 1;
        $this->CData->UpdateUserData($LastName, $FirstName, $MiddleName, $Gender, $Email, $Age, $_SERVER['REMOTE_ADDR'], $MaritalStatus, $CompanyType, $Occupation, $CarType, $Dealer, $Themes, $Model, $Stop);
        $_SESSION["SUBSCRIBTION_MEMBER"]["Data"] = $this->CData->GetFullMemberData();
    }


    public function SendRegistredEmail($Password)
    {
        $MemberData = $this->CData->GetMember();
        $Themes = $this->CData->GetThemesForMember();
        $ThemesCount = $this->CData->GetThemesCount();
        $Mailer = new MailDriver(false, true);
        $Mailer->AddAddress($MemberData["memberEmail"]);
        if(WS::Init()->GetBrandId() == 2)
        {
            $Mailer->From = "info@lexus.ru";
            $Mailer->FromName = "Lexus news";
            $Mailer->Subject = "Регистрация в системе рассылки новостей LEXUS";
        }
        else
        {
            $Mailer->From = "info@toyota.ru";
            $Mailer->FromName = "Toyota news";
            $Mailer->Subject = "Регистрация в системе рассылки новостей TOYOTA";
        }
        $Message = "<h3>";
        if($MemberData["memberFirstName"] && $MemberData["memberGender"] !== null)
        {
            $Message .= "Уважаем".($MemberData["memberGender"] ? "ая " : "ый ").$MemberData["memberFirstName"].$MemberData["memberMiddleName"];
        }
        else
        {
            $Message .= "Уважаемый(ая) ".$MemberData["memberLastName"];
        }
        $Message .= '</h3>
<p>Мы рады сообщить Вам, что регистрация Вашей подписки на новости '.(WS::Init()->GetBrandId() == 2 ? 'Lexus' : 'Toyota').' прошла успешно.<br>
Мы благодарим Вас и с удовольствием будем информировать о';
        if(WS::Init()->GetBrandId() == 2 || WS::Init()->GetBrandId() == 1 && sizeof($Themes) >= $ThemesCount)
        {
            $Message .= " всех новостях ".(WS::Init()->GetBrandId() == 2 ? 'Lexus' : 'Toyota').".</p>";
        }
        else
        {
            $Message .= "<br /><ul>";
            foreach($Themes as $v)
                $Message .= "<li>".$v["subscriptionThemeNameGen"]."</li>";
            $Message .= "</ul></p>";
        }
		if(WS::Init()->GetBrandId() == 1)
        $Message .= '<br /><br /><br /><p>Для доступа к настройкам подписки воспользуйтесь следующими данными:<br />
Адрес: <a href="http://www.toyota.ru/forms/cab_login.tmex">http://www.toyota.ru/forms/cab_login.tmex</a><br />
Логин: '.$MemberData["memberEmail"].'<br />
Пароль: '.$Password.'<br />
<br /><br /><p>С уважением,<br>ООО «Тойота Мотор»</p>';

        //$UnsUrl = 'http://contacts.toyota.ru/z/unsubscribe/'.$MemberData["memberUnsubscribeCode"]."?e=".$MemberData["memberEmail"];
        //$Message .= '<br /><br /><br /><a href="'.$UnsUrl.'">'.$UnsUrl.'</a>';


        $Mailer->MsgHTML($Message);
        $Mailer->Send();
    }





    public function MemberAuth($Email, $Password)
    {
        if(isset($_SESSION["SUBSCRIBTION_MEMBER"]) && isset($_SESSION["SUBSCRIBTION_MEMBER"]["Data"]))
            WS::Init()->Redirect(SUBSCRIBTION_MEMBER_CABINET_URL);
        $R = $this->CData->MemberAuth($Email, sha1($Password));
        $_SESSION["SUBSCRIBTION_MEMBER"] = array();
        $_SESSION["SUBSCRIBTION_MEMBER"]["Data"] = $R;
    }

    public function CheckAuth()
    {
        if(!(isset($_SESSION["SUBSCRIBTION_MEMBER"]) && isset($_SESSION["SUBSCRIBTION_MEMBER"]["Data"])))
            WS::Init()->Redirect(SUBSCRIBTION_MEMBER_AUTH_URL);
        $this->CData->SetCurrentMember($_SESSION["SUBSCRIBTION_MEMBER"]["Data"]["memberId"]);
    }

    public function MemberExit()
    {
        $this->CheckAuth();
        unset($_SESSION["SUBSCRIBTION_MEMBER"]);
        WS::Init()->Redirect(SUBSCRIBTION_MEMBER_AUTH_URL);
    }

    public function GetPersonalData()
    {
        $this->CheckAuth();
        return array(
            "member"    => $_SESSION["SUBSCRIBTION_MEMBER"]["Data"],
            "themes"    => $this->CData->GetMemberThemes()
        );
    }


    public function RestorPassword($Email)
    {
$this->Dump(__METHOD__.": ".__LINE__, "++++++++++++++++++");
        $Member = $this->CData->GetSubscribe($Email);
        if(!$Member)
            throw new dmtException("Member is not found", 5);
        $Password = $this->CreatePassword();
        $this->CData->RestorPassword($Member, sha1($Password), $Email);
        $IsLexus = SITE_CURRENT == SITE_LEXUS;
        $Mailer = new MailDriver(false, true);
        $Mailer->AddAddress($Email);
        if($IsLexus)
        {
            $Mailer->From = "info@lexus.ru";
            $Mailer->FromName = "Toyota news";
        }
        else
        {
            $Mailer->From = "info@toyota.ru";
            $Mailer->FromName = "Toyota news";
        }
        $Mailer->Subject = "Смена пароля личного кабинета";
        $Message = "<p>Ваш новый пароль - ".$Password."</p>";
        $Mailer->MsgHTML($Message);
        $Mailer->Send();
    }











    public function Test()
    {
        $this->CData->Test();
    }
}