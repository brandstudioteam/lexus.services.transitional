<?php
class UsersPollingProcessor extends Processor
{
	/**
	 *
	 * @var UsersPollingProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var UsersPollingData
	 */
	private $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return UsersPollingProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new UsersPollingData();
	}


    public function AnswerPolling($Code, $Ansver, $Comment)
    {
        $this->CData->AnswerPolling($Code, $Ansver, $Comment);
    }

    public function GetPollings($Member, $Dealers, $DateStart, $DateEnd)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->GetPollings($Member, $Dealers, $DateStart, $DateEnd);
    }

    public function CreateCode($Member)
    {
        return md5(";kl;jl ;lj i".$Member."fdl [9u t".microtime(true));
    }

    public function SendPolling($Member, $Text, $Email, $Code = null)
    {
        $IsLexus = SITE_CURRENT == SITE_LEXUS;
        if($Email)
        {
            if(!$Code)
            $Code = $this->CreateCode($Member);
            if($this->CData->SendPolling($Member, $Code))
            {
                $Mailer = new MailDriver(false, true);
                $Mailer->From = $IsLexus ? "mailer@lexus.ru" : "mailer@toyota.ru";
                $Mailer->FromName = ($IsLexus ? "LEXUS" : "TOYOTA");
                $Mailer->Subject = ($IsLexus ? "LEXUS" : "TOYOTA")." Запись на техобслуживание";
                $Mailer->MsgHTML($Text);

                $Mailer->AddAddress($Email);
                try
                {
                    $Mailer->Send();
                }
                catch(Exception $e)
                {
                    throw new dmtException("Send e-mail to dealers error", 2);
                }
            }
        }
    }
}