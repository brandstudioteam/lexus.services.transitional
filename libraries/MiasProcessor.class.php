<?php
class MiasProcessor extends Processor
{
	/**
	 *
	 * @var MiasProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var MiasData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return MiasProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new MiasData();
	}


    public function AddMember($UserFirstname, $UserLastname, $UserDealerId, $Model, $Dealer, $Gender, $FirstName, $LastName, $CityId, $Age,
                                $Phone, $Email, $OldBrand, $OldCarId, $News, $Catalog, $CatalogCars, $Price, $PriceCars, $Test, $Order, $Callme, $SourceType)
    {
        $this->CData->AddMember($UserFirstname, $UserLastname, $UserDealerId, $Model, $Dealer, $Gender, $FirstName, $LastName, $CityId, $Age, $Phone, $Email, $OldBrand, $OldCarId, $News, $Catalog, $CatalogCars, $Price, $PriceCars, $Test, $Order, $Callme, $SourceType);

        if($Catalog && $CatalogCars)
            try {$this->SendCatalog($FirstName, $LastName, $Gender, $Email, $CatalogCars);}
            catch(dmtException $e){}
        if($Price && $PriceCars)
            try {$this->SendPrice($FirstName, $LastName, $Gender, $Email, $PriceCars);}
            catch(dmtException $e){}
		if($SourceType == 23 || $SourceType == 24 || $SourceType == 25)
		{
			if($Test)
				try {CustomActionsProcessor::Init()->AddMembersTD($Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, null, null, $SourceType ? $SourceType : 21);}
				catch(dmtException $e){}
			if($Order)
				try {CustomActionsProcessor::Init()->AddMembersOM($Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, null, null, $OldCarId, null, null, $SourceType ? $SourceType : 21, 1);}
				catch(dmtException $e){}
		}
        if($Callme)
            try {CustomActionsProcessor::Init()->AddMembersOM($Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, null, null, $OldCarId, null, null, $SourceType ? $SourceType : 21, 2);}
            catch(dmtException $e){}
        if($News)
            try {UsersSubscriptionsProcessor::Init()->Subscribe($FirstName, $LastName, null, $Gender, $Email, $Age);}
            catch(dmtException $e){}
    }

    public function SendCatalog($FirstName, $LastName, $Gender, $Email, $CatalogCars)
    {
        $Catalogues = array(
            "CT"    => "http://content.v10.lexus-russia.ru/pdf/brochures/LEXUS_CT_BROCHURE_2014.pdf",
            "IS"    => "http://content.v10.lexus-russia.ru/pdf/brochures/ng_is.pdf",
            "GS"    => "http://content.v10.lexus-russia.ru/pdf/brochures/ng_gs_combined.pdf",
            "ES"    => "http://content.v10.lexus-russia.ru/pdf/brochures/ES_350_2013.pdf",
            "LS"    => "http://content.v10.lexus-russia.ru/pdf/brochures/ls-2013.pdf",
            "LX"    => "http://content.v10.lexus-russia.ru/pdf/brochures/lx_brochure_full_2013.pdf",
            "RX"    => "http://content.v10.lexus-russia.ru/pdf/brochures/rx_brochure_2013.pdf",
            "GX"    => "http://content.v10.lexus.ru/pdf/brochures/gx_brochure_full_2013.pdf",
            "NX"    => "http://content.v10.lexus.ru/pdf/brochures/NX_brochure_2014.pdf",
            "RC"    => "http://content.v10.lexus-russia.ru/pdf/brochures/RC350.pdf",
            "RC F"  => "http://content.v10.lexus-russia.ru/pdf/brochures/RCF.pdf"
        );

        $Subject = "Каталог Lexus";
        $Links = "";

        $NotFound = array();

        $IsOne = sizeof($CatalogCars) == 1;
        foreach($CatalogCars as $v)
        {
            if(isset($Catalogues[$v]))
                $Links .= 'Lexus '.$v.': <a target="_blank" href="'.$Catalogues[$v].'">'.$Catalogues[$v].'</a><br/>';
            else $NotFound[] = $v;
        }
        $Message = ($LastName || $FirstName ? ($Gender !== null && !is_empty($Gender) ? ($Gender ? "Уважаемая " : "<h1> Уважаемый ") : "Уважаемый(ая) ").$FirstName." ".$LastName."!</h1>" : "")."
<p>Благодарим Вас за интерес, проявленный к ".($IsOne ? "Lexus ".$CatalogCars[0] : " автомобилям Lexus")."!</p>
<p>Каталог".($IsOne ? " доступен по ссылке" : "и доступны по ссылкам").":<br />
    ".$Links."
</p>
<p>С уважением,<br />
команда Lexus!</p>
<p>Служба клиентской поддержки Lexus: 8 800 200 38 83</p>";

        $this->Dump(__METHOD__.": ".__LINE__, "*********************************", $CatalogCars, $Message, "NotFound", $NotFound);

        $this->Send($Email, $Message, $Subject);
    }

    public function SendPrice($FirstName, $LastName, $Gender, $Email, $PriceCars)
    {
        $Subject = "Прайс-лист Lexus";
        $Prices = array(
            "CT 200h"       => "http://content.v10.lexus-russia.ru/pdf/price_list/CT200h_price.pdf",
            "IS 250"        => "http://content.v10.lexus-russia.ru/pdf/price_list/IS250.pdf",
            "IS 300h"       => "http://content.v10.lexus-russia.ru/pdf/price_list/IS300h.pdf",
            "GS 250"        => "http://content.v10.lexus-russia.ru/pdf/price_list/GS250.pdf",
            "GS 350"        => "http://content.v10.lexus-russia.ru/pdf/price_list/GS350AWD.pdf",
            "GS 450h"       => "http://content.v10.lexus-russia.ru/pdf/price_list/GS450h.pdf",
            "ES 250"        => "http://content.v10.lexus-russia.ru/pdf/price_list/ES_250_2013_price.pdf",
            "ES 350"        => "http://content.v10.lexus-russia.ru/pdf/price_list/ES_350_2013_price.pdf",
            "ES 300h"       => "http://content.v10.lexus-russia.ru/pdf/price_list/ES_300h_2013_price.pdf",
            "LS 460"        => "http://content.v10.lexus-russia.ru/pdf/price_list/LS460_2013_price.pdf",
            "LS 460 AWD"    => "http://content.v10.lexus-russia.ru/pdf/price_list/LS460AWD_price_2013.pdf",
            "LS 460 L AWD"  => "http://content.v10.lexus-russia.ru/pdf/price_list/LS460AWDL_price_2013.pdf",
            "LS 600h"       => "http://content.v10.lexus-russia.ru/pdf/price_list/LS600h_F_SPORT_price_2013.pdf",
            "LS 600h L"     => "http://content.v10.lexus-russia.ru/pdf/price_list/LS600hL_price_2013.pdf",
            "LX 570"        => "http://content.v10.lexus-russia.ru/pdf/price_list/LX570_price_2013.pdf",
            "RX 270"        => "http://content.v10.lexus.ru/pdf/price_list/RX270.pdf",
            "RX 350"        => "http://content.v10.lexus.ru/pdf/price_list/RX350.pdf",
            "RX 450h"       => "http://content.v10.lexus.ru/pdf/price_list/RX450h.pdf",
            "GX 460"        => "http://content.v10.lexus-russia.ru/pdf/price_list/GX460_pricelist_2013.pdf",
            "NX 200"        => "http://content.v10.lexus-russia.ru/pdf/price_list/NX_200.pdf",
            "NX 200 AWD"    => "http://content.v10.lexus-russia.ru/pdf/price_list/NX_200_AWD.pdf",
            "NX 200t"       => "http://content.v10.lexus-russia.ru/pdf/price_list/NX_200t_AWD.pdf",
            "NX 300h"       => "http://content.v10.lexus-russia.ru/pdf/price_list/NX_300h_AWD.pdf"
        );

        $Links = "";
        $IsOne = sizeof($PriceCars) == 1;

        $NotFound = array();

        foreach($PriceCars as $v)
        {
            if(isset($Prices[$v]))
                $Links .= 'Lexus '.$v.': <a target="_blank" href="'.$Prices[$v].'">'.$Prices[$v].'</a><br/>';
            else $NotFound[] = $v;
        }
        $Message = ($LastName || $FirstName ? ($Gender !== null && !is_empty($Gender) ? ($Gender ? "Уважаемая " : "<h1> Уважаемый ") : "Уважаемый(ая) ").$FirstName." ".$LastName."!</h1>" : "")."
<p>Благодарим Вас за интерес, проявленный к ".($IsOne ? "Lexus ".$PriceCars[0] : " автомобилям Lexus")."!</p>
<p>Прайс-лист".($IsOne ? " доступен по ссылке" : "ы доступны по ссылкам").":<br />
    ".$Links."
</p>
<p>С уважением,<br />
команда Lexus!</p>
<p>Служба клиентской поддержки Lexus: 8 800 200 38 83</p>";

        $this->Dump(__METHOD__.": ".__LINE__, "*********************************", $PriceCars, $Message, "NotFound", $NotFound);

        $this->Send($Email, $Message, $Subject);
    }

    protected function Send($Email, $Message, $Subject)
    {
        if(!$Email)
            throw new dmtException("Email is not found", 5);
        $Mailer = new MailDriver(false, true);
        $Mailer->From = "order@lexus.ru";
        $Mailer->FromName = "Lexus";
        $Mailer->Subject = $Subject;
        $Mailer->AddAddress($Email);
        $Mailer->MsgHTML($Message);
        $Mailer->Send();
    }

    public  function AddQuestions($Questions)
    {
        try
        {
            $this->CData->AddQuestions($Questions);
        }
        catch (dmtException $e)
        {

        }
    }

    public  function AddQuest($Car, $Category, $Text)
    {
        try
        {
            $this->CData->AddQuest($Car, $Category, $Text);
        }
        catch (dmtException $e)
        {

        }
    }

	public function GetMembersCSV($RegisterStart, $RegisterEnd = null)
    {
        $R = $this->CData->GetMembersForReport($RegisterStart, $RegisterEnd);
        array_unshift(  $R,
                        array(
                            "Имя",
							"Фамилия",
                            "Пол",
							"Возраст",
							"Электронная почта",
							"Телефон",
                            "Дата отправки заявки",
							"Новости",
							"Тест-драв",
							"Прайс",
							"Каталог",
							"Заказ",
							"Обратный звонок",
							"Модели прайса",
							"Модели каталога",
							"Источник трафика",
							"Дилер",
							"Модель",
							"Автомобиль пользователя",
							"Город"
                        ));
        WS::Init()->FileOriginalName = "Zayavki_mias_".$RegisterStart.($RegisterEnd ? "_".$RegisterEnd : "").".csv";
        return $R;
    }

	public function GetMembersTDCSV($RegisterStart, $RegisterEnd = null)
    {
        $R = $this->CData->GetMembersTDForReport($RegisterStart, $RegisterEnd);
        array_unshift(  $R,
                        array(
                            "Фамилия",
                            "Имя",
                            "Пол",
                            "Дилерский центр",
                            "Модель",
                            "Электронная почта",
                            "Телефон",
                            "Источник трафика",
                            "Дата отправки заявки"
                        ));
        WS::Init()->FileOriginalName = "Testdrive_mias_".$RegisterStart.($RegisterEnd ? "_".$RegisterEnd : "").".csv";
        return $R;
    }
}