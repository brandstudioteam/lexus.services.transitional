<?php
class CarValuationsData extends Data
{
	public function SaveMember($Dealer, $FIO, $Phone, $Email, $Car, $Mileage, $Manufactured, $Description, $TradeInModel, $IsMobile, $FromOwnerSite, $Type, $PhotoCount, $CalcURL)
	{
		$this->Exec("INSERT INTO `references`.`valuations_members`
	(`name`,
	`partners_division_id`,
    `brand_id`,
	`phone`,
	`email`,
	`car_id`,
	`manufacturer_year`,
	`mileage`,
	`description`,
	`trade_in_model_id`,
	`is_mobile`,
	`photo_count`,
	`from_owner_site`,
	`registred`,
    `advanced_flag`,
    `calculator`,
    `send_status`,
    `current_status_id`)
VALUES
	(".$this->Esc($FIO).",
	".$Dealer.",
    ".(SITE_CURRENT == SITE_LEXUS ? 2 : 1).",
	".$this->Esc($Phone).",
	".$this->Esc($Email).",
	".$Car.",
	".$this->Esc($Manufactured).",
	".$Mileage.",
	".$this->Esc($Description).",
	".$this->CheckNull($TradeInModel).",
	".$IsMobile.",
	".$PhotoCount.",
	".$FromOwnerSite.",
	NOW(),
    ".$Type.",
    ".$this->Esc($CalcURL).",
    1,
    1);");
        return $this->DB->GetLastID();
	}


    public function UpdateStatus($Record, $Status, $Xml = null)
    {
        $this->Exec("UPDATE `references`.`valuations_members`
SET `send_status`=".$Status.($Xml !== null ? ",
    `xml`=".$this->Esc($Xml) : "")."
WHERE member_id=".$Record.";");
    }

    public function GetNotSended()
    {
        return $this->Get("SELECT
    `valuations_members`.`member_id` AS memberId,
    `valuations_members`.`xml`
FROM `references`.`valuations_members`
WHERE `send_status`=0;");
    }


    public function PrepareEmails()
	{
		if($this->Count("SELECT COUNT(*) AS Cnt FROM partners_divisions_facilities_emails WHERE `facility_id`=16;"))
			return;
		$R = $this->Get("SELECT
	LOWER(b.`email`) AS email,
	a.`partners_division_id` AS id
FROM `toyota_references`.`partners_divisions` AS a
LEFT JOIN `ttrade`.`t_dealer_fixed` AS b ON b.`id`=a.`rcode`
WHERE `email` IS NOT NULL
	AND `email`<>'';");

		foreach ($R as $v)
		{
			$E = preg_split('/\s*[,;]\s*/u', $v["email"]);
			foreach($E as $e)
			{
				$this->Exec("INSERT IGNORE INTO `toyota_references`.`partners_divisions_facilities_emails`
	(`partners_division_id`,
	`facility_id`,
	`email`)
VALUES
	(".$v["id"].",
	16,
	".$this->Esc(trim($e)).")");
			}
		}

		$R = $this->Get("SELECT
	LOWER(b.`email`) AS email,
	a.`partners_division_id` AS id
FROM `lexus_references`.`partners_divisions` AS a
LEFT JOIN `ttrade`.`t_dealer_fixed` AS b ON b.`id`=a.`rcode`
WHERE `email` IS NOT NULL
	AND `email`<>'';");

		foreach ($R as $v)
		{
			$E = preg_split('/\s*[,;]\s*/u', $v["email"]);
			foreach($E as $e)
			{
				$this->Exec("INSERT IGNORE INTO `lexus_references`.`partners_divisions_facilities_emails`
	(`partners_division_id`,
	`facility_id`,
	`email`)
VALUES
	(".$v["id"].",
	16,
	".$this->Esc(trim($e)).")");
			}
		}
	}


    public function GetMembers($Brand, $Dealer = null)
    {
        $Where[] = "`brand_id`".$this->PrepareValue($Brand);
        $Where[] = "`calculator` IS NOT NULL AND `calculator`<>''";
        if($Dealer)
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealer);
        $Where[] = "`register` >= DATE_ADD(NOW(), INTERVAL '-2' MONTH)";
        $Query = "SELECT
    `member_id` AS memberId,
    `member_name` AS name,
    `partners_division_id` AS partnersDivisionId,
    `brand_id` AS brandId,
    `phone` AS phone,
    `email` AS email,
    `car_id` AS carId,
    `manufacturer_year` AS manufacturerYear,
    `mileage` AS mileage,
    `description` AS description,
    `trade_in_model_id` AS tradeInModelId,
    `is_mobile` AS isMobile,
    `photo_count` AS photoCount,
    `from_owner_site` AS fromOwnerSite,
    `register` AS registredU,
    DATE_FORMAT(`register`, '%d.%m.%Y %H:%i') AS registred,
    `advanced_flag` AS trafficSource,
    `current_status_record_id` AS currentStatusRecordId,
    -- `old_dealer_id`,
    `cost` AS cost,
    -- `calculator`,
    -- `send_status`,
    `current_status_id` AS statusId,
	`cost_max` AS costMax,
    `partners_division_name` AS partnersDivisionName,
    `trade_in_model_name` AS tradeInModelName,
    `car_name` AS carName,
    `status_name` AS statusName,
    `user_id` AS userId,
    `status_change_datetime` AS statusChangeDatetimeU,
    DATE_FORMAT(`status_change_datetime`, '%d.%m.%Y %H:%i') AS statusChangeDatetime,
    `first_name` AS userFirstName,
    `last_name` AS userLastName
FROM `references`.`valuations_members_f`";
        return $this->Get($Query.$this->PrepareWhere($Where)."
ORDER BY `register` DESC;");
    }

    public function GetMember($Member)
    {
        $Query = "SELECT
    `member_id` AS memberId,
    `member_name` AS name,
    `partners_division_id` AS partnersDivisionId,
    `brand_id` AS brandId,
    `phone` AS phone,
    `email` AS email,
    `car_id` AS carId,
    `manufacturer_year` AS manufacturerYear,
    `mileage` AS mileage,
    `description` AS description,
    `trade_in_model_id` AS tradeInModelId,
    `is_mobile` AS isMobile,
    `photo_count` AS photoCount,
    `from_owner_site` AS fromOwnerSite,
    `register` AS registredU,
    DATE_FORMAT(`register`, '%d.%m.%Y %H:%i') AS registred,
    `advanced_flag` AS trafficSource,
    `current_status_record_id` AS currentStatusRecordId,
    -- `old_dealer_id`,
    `cost` AS cost,
    `calculator`,
    -- `send_status`,
    `current_status_id` AS statusId,
	`cost_max` AS costMax,
    `partners_division_name` AS partnersDivisionName,
    `trade_in_model_name` AS tradeInModelName,
    `car_name` AS carName,
    `status_name` AS statusName,
    `user_id` AS userId,
    `status_change_datetime` AS statusChangeDatetimeU,
    DATE_FORMAT(`status_change_datetime`, '%d.%m.%Y %H:%i') AS statusChangeDatetime,
    `first_name` AS userFirstName,
    `last_name` AS userLastName
FROM `references`.`valuations_members_f`
WHERE `member_id`=".$Member.";";
        return $this->Get($Query, true);
    }

    public function GetMembersForReport($Brand, $Dealer, $Model = null, $RegisterStart = null, $RegisterEnd = null, $TradeInModel = null, $All = null)
    {
        $Where = array();

        $Where[] = "`brand_id`".$this->PrepareValue($Brand);
        if($Dealer)
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealer);
        if($Model)
            $Where[] = "`car_id`".$this->PrepareValue($Model);
        if($RegisterStart)
            $Where[] = "DATE(`register`) >= DATE(".$this->Esc($RegisterStart).")";
        if($RegisterEnd)
            $Where[] = "DATE(`register`) <= DATE(".$this->Esc($RegisterEnd).")";
        if($TradeInModel)
            $Where[] = "`trade_in_model_id`=".$this->PrepareValue($TradeInModel);
		if(!$All)
			$Where[] = "`register` >= DATE_ADD(NOW(), INTERVAL '-2' MONTH)";

        $Query = "SELECT
    `partners_division_name` AS partnersDivisionName,
    `member_name` AS name,
    `phone` AS phone,
    `email` AS email,
    DATE_FORMAT(`register`, '%d.%m.%Y %H:%i') AS registred,
    `car_name` AS carName,
    `manufacturer_year` AS manufacturerYear,
    `mileage` AS mileage,
    -- `description` AS description,
    `photo_count` AS photoCount,
    `trade_in_model_name` AS tradeInModelName,
    `status_name` AS statusName,
    `cost` AS cost,
	`cost_max` AS costMax,
    DATE_FORMAT(`status_change_datetime`, '%d.%m.%Y %H:%i') AS statusChangeDatetime,
    CONCAT_WS('', `first_name`, `last_name`) userName,
    `from_owner_site` AS fromOwnerSite,
	`advanced_flag` AS trafficSourceId
FROM `references`.`valuations_members_f`";
        return $this->Get($Query.$this->PrepareWhere($Where));
    }

    public function AddCost($Member, $Cost, $CostMax, $IP)
    {
        $this->Begin();
        try
        {
            $this->Exec("INSERT INTO `references`.`u_members_statuses_history`
	(`members_type`,
	`member_id`,
	`change_datetime`,
	`status_id`,
	`user_id`,
	`ip`)
VALUES
	(2,
    ".$Member.",
    NOW(),
    2,
    ".DB_VARS_ACCOUNT_CURRENT.",
    INET_ATON(".$this->Esc($IP)."));");
            $R = $this->DB->GetLastID();
            $this->Exec("UPDATE `references`.`valuations_members`
SET `cost`=".$Cost.",
	`cost_max`=".$CostMax.",
	`current_status_id`=2,
    `current_status_record_id`=".$R."
WHERE `member_id`=".$Member.";");
            $this->Commit();
        }
        catch (dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }

	public function ChangeStatus($Member, $Status, $IP)
	{
		$this->Begin();
        try
        {
            $this->Exec("INSERT INTO `references`.`u_members_statuses_history`
	(`members_type`,
	`member_id`,
	`change_datetime`,
	`status_id`,
	`user_id`,
	`ip`)
VALUES
	(2,
    ".$Member.",
    NOW(),
    2,
    ".DB_VARS_ACCOUNT_CURRENT.",
    INET_ATON(".$this->Esc($IP)."));");
            $R = $this->DB->GetLastID();
            $this->Exec("UPDATE `references`.`valuations_members`
SET `current_status_id`=".$Status.",
    `current_status_record_id`=".$R."
WHERE `member_id`=".$Member.";");
            $this->Commit();
        }
        catch (dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
	}

    public function GetCalcURL($Member)
    {
        return $this->Count("SELECT
	`calculator` AS Cnt
FROM  `references`.`valuations_members`
WHERE `member_id`=".$Member.";");
    }

    public function GetDealerForMember($Member)
    {
        return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM  `references`.`valuations_members`
WHERE `member_id`=".$Member.";");
    }





    public function GetNewDealer($Dealer)
    {
        return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `references`.`partners_divisions`
WHERE `old_id`=".$Dealer."
    AND `brand_id`=".(SITE_CURRENT == SITE_LEXUS ? 2 : 1).";");
    }

    public function GetOldDealer($Dealer)
    {
        return $this->Count("SELECT
	`old_id` AS Cnt
FROM `references`.`partners_divisions`
WHERE `partners_division_id`=".$Dealer.";");
    }


    public function GetEmailsForFacilities($Dealers, $Facilities)
    {
        return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`facility_id` AS facilityId,
	`email` AS email
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`".$this->PrepareValue($Dealers)."
	AND `facility_id`".$this->PrepareValue($Facilities).";");
    }

    public function GetDealerDataOld($Dealer)
    {
		return $this->Get("SELECT
	d.`partners_division_id` AS partnerId,
	d.`partners_type_id` AS partnerTypeId,
	d.`name` AS partnerName,
	d.`site` AS partnerSite,
	d.`city_id` AS cityId,
	d.`latitude` AS gLt,
	d.`longitude` AS gLg,
	d.`address` AS partnerAddress,
	d.`phones` AS partnerPhone,
	d.`rcode` AS partnerRCode,
	d.`status` AS partnerStatus,
	d.`map_image` AS  partnerMap
FROM `".DBS_UNIVERSAL_REFERENCES."`.`dealers` AS d
WHERE d.`old_id`=". $Dealer."
    AND `brand_id`=".(SITE_CURRENT == SITE_LEXUS ? 2 : 1).";", true);
    }

    public function GetDealerData($Dealer)
    {
		return $this->Get("SELECT
	d.`partners_division_id` AS partnerId,
	d.`partners_type_id` AS partnerTypeId,
	d.`name` AS partnerName,
	d.`site` AS partnerSite,
	d.`city_id` AS cityId,
	d.`latitude` AS gLt,
	d.`longitude` AS gLg,
	d.`address` AS partnerAddress,
	d.`phones` AS partnerPhone,
	d.`rcode` AS partnerRCode,
	d.`status` AS partnerStatus,
	d.`map_image` AS  partnerMap
FROM `".DBS_UNIVERSAL_REFERENCES."`.`dealers` AS d
WHERE d.`partners_division_id`=". $Dealer.";", true);
    }

    public function GetNewModel($Model)
    {
        return $this->Count("SELECT
	`model_id` AS Cnt
FROM `references`.`models`
WHERE `old_id`=".$Model."
    AND `brand_id`=".(SITE_CURRENT == SITE_LEXUS ? 2 : 1).";");
    }

    public function GetNewModelName($Model)
    {
        return $this->Count("SELECT
	`name` AS Cnt
FROM `references`.`models`
WHERE `model_id`=".$Model.";");
    }
}