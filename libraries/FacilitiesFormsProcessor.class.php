<?php
define("SERVICES_FACILITIES_FORMS_ID",                         25);

define("MEMBERS_STATUS_NEW",									1);

class FacilitiesFormsProcessor extends Processor
{
	/**
	 *
	 * @var FacilitiesFormsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var FacilitiesFormsData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return FacilitiesFormsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new FacilitiesFormsData();
	}

	public function GetDealersFormSet($Facility, $FormType, $Dealer = null)
	{
		return $this->CData->GetDealersFormSet($Facility, $FormType, $Dealer);
	}

	public function GetDealersFormSetForOld($Facility, $FormType, $Dealer = null)
	{
$this->Dump(__METHOD__.": ".__LINE__, $Facility, $FormType, $Dealer);
		return $this->CData->GetDealersFormSetForOld($Facility, $FormType, $Dealer);
	}



	public function GetDealersFormSetForAdmin($Dealer = null)
	{
		$Dealer = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_FACILITIES_FORMS_MANAGEMENT, $Dealer);
		return $this->CData->GetDealersFormSetForAdmin($Dealer);
	}

	public function SaveDealersFormSet($Facility, $FormType, $Dealer, $AgreementTitle, $AgreementText, $GA)
	{
		PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_FACILITIES_FORMS_MANAGEMENT, $Dealer);

		if($this->CData->SaveDealersFormSet($Facility, $FormType, $Dealer, $AgreementTitle, $AgreementText, $GA))
		{
			$Detail = $this->CData->GetDealersFormSetForAdmin($Dealer, $Facility, $FormType);
			$this->AddEvent(SERVICES_FACILITIES_FORMS_ID, 22, 2, $Detail, $Dealer, User::Init()->GetAccount(), WS::Init()->GetClientUID());
		}
		return $Detail;
	}

	public function AddMember(  $Facility, $FormType, $Dealer, $Model, $LastName, $FirstName, $MiddleName, $Email, $Phone, $ContactMethod, $Gender, $Age,
								$IsOwner, $AdvancedInfo, $TrafficSource, $CurrentCar = null, $CurrentCarManufacturer = null, $CarYear = null, $Miliage = null,
								$RegNumber = null, $City = null, $SourceInfo = null, $VizitDate = null, $VizitTime = null, $VIN = null,
								$Configuration = null, $ModelColor = null, $UserAction = null, $TOType = null, $FromDealerSite = null, $FromMobile = null,
								$Subscribe = null, $ContactingTheme = null, $CustomCity = null, $Photos = null, $ForShaduler = null)
	{
$this->Dump(__METHOD__.": ".__LINE__, "****************", "ForShaduler", $ForShaduler);
		if(!$FormType)
            $FormType = 1;

		$Status = MEMBERS_STATUS_NEW;

		//TODO:
		$ActionCode = null;
		$ActionStatus = null;
		$PhotoCount = null;
		$PhotosNames = null;

		if(!$ForShaduler)
		{
			if($this->CData->CheckMember(	WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, $Model, $LastName, $FirstName, $MiddleName, $Email, $Phone,
											$ContactMethod, $Gender, $Age, $IsOwner, $AdvancedInfo, $TrafficSource, $CurrentCar, $CurrentCarManufacturer, $CarYear,
											$Miliage, $RegNumber, $City, $SourceInfo, $VizitDate, $VizitTime, $VIN, $Status, $Configuration, $ModelColor, $ActionCode,
											$ActionStatus, $UserAction, $PhotoCount, $PhotosNames, $TOType, WS::Init()->GetOuterURL(), $FromDealerSite, $FromMobile,
											$_SERVER["HTTP_USER_AGENT"], $ContactingTheme, $CustomCity))
			{
				$FacilityName = FacilitiesProcessor::Init()->GetFacilityName($Facility);
				$Mailer = new MailDriver(false, true);
				if(WS::Init()->GetBrandId() == 1)
				{
					$Mailer->From = "support@toyota.ru";
					$Mailer->FromName = "Toyota";
					$Mailer->Subject = "Дубликат записи на Toyota ".$FacilityName;
				}
				else
				{
					$Mailer->From = "support@lexus.ru";
					$Mailer->FromName = "Lexus";
					$Mailer->Subject = "Дубликат записи на Lexus ".$FacilityName;
				}
				$Mailer->AddAddress("lexus.support@bstd.ru");
				$Mailer->Body = "Зафиксирована попытка записи дубликата. С формы ".$FacilityName.($FormType == 2 ? " Заказ обратного звонка" : "").". Referrer:".WS::Init()->GetOuterURL()."; UserAgent: ".$_SERVER["HTTP_USER_AGENT"]."; SourceFlag: ".$TrafficSource;
				$Mailer->Send();
				throw new dmtException("MembersAlreadyExists", 5);
			}
		}
		$Member = $this->CData->AddMember(  WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, $Model, $LastName, $FirstName, $MiddleName, $Email, $Phone,
											$ContactMethod, $Gender, $Age, $IsOwner, $AdvancedInfo, $TrafficSource, $CurrentCar, $CurrentCarManufacturer, $CarYear,
											$Miliage, $RegNumber, $City, $SourceInfo, $VizitDate, $VizitTime, $VIN, $Status, $Configuration, $ModelColor, $ActionCode,
											$ActionStatus, $UserAction, $PhotoCount, $PhotosNames, $TOType, WS::Init()->GetOuterURL(), $FromDealerSite, $FromMobile,
											$_SERVER["HTTP_USER_AGENT"], $ContactingTheme, $CustomCity);
		if($ForShaduler)
		{
$this->Dump(__METHOD__.":".__LINE__, "**************** 2", $Member);
			return $Member;
		}
        try
        {
			if($Dealer)
			{
				$DealerData = DealersProcessor::Init()->GetDealersNew(null, $Dealer);
				$RCode = $DealerData["RCode"];
				$DealerData["partnerName"] = mb_str_replace("&ndash;", "—", $DealerData["partnerName"]);
			}
			else
			{
				$RCode = null;
				$DealerData = null;
			}
			if($Model)
			{
				$ModelData = ModelsProcessor::Init()->GetModelsNew(null, null, null, $Model);
			}
			else
			{
				$ModelData = null;
			}
            $MemberData = $this->CData->GetMembersForEmail($Facility, $Member);

            $this->Send($Facility, $FormType, $MemberData);
        }
        catch(dmtException $e)
        {

        }
		if($Subscribe)
		{
			try
			{
				//SubscribeDriver::Subscribe($FirstName, $LastName, null, $Gender, $Email, $Age, null, $Dealer, null, $Model);
                UsersSubscriptionsProcessor::Init()->Subscribe($LastName, $FirstName, null, $Gender, $Email, $Age, null, $Dealer, null, null);
			}
			catch (dmtException $e)
			{

			}
		}
		return array("dealerRcode" => $RCode);
	}

	public function DeleteMember($Member)
	{
		return $this->CData->DeleteMember($Member);
	}

	public function SaveMember(	$Member, $Model = null, $LastName = null, $FirstName = null, $MiddleName = null, $Email = null, $Phone = null, $ContactMethod = null, $Gender = null,
								$Age = null, $IsOwner = null, $AdvancedInfo = null, $TrafficSource = null, $CurrentCar = null, $CurrentCarManufacturer = null, $CarYear = null,
								$Miliage = null, $RegNumber = null, $City = null, $SourceInfo = null, $VizitDate = null, $VizitTime = null, $VIN = null, $Configuration = null,
								$ModelColor = null, $UserAction = null, $TOType = null, $ContactingTheme = null, $CustomCity = null, $Status = null, $ForShaduler = null)
	{
		return $this->CData->SaveMember($Member, $Model, $LastName, $FirstName, $MiddleName, $Email, $Phone, $ContactMethod, $Gender, $Age, $IsOwner, $AdvancedInfo,
										$TrafficSource, $CurrentCar, $CurrentCarManufacturer, $CarYear, $Miliage, $RegNumber, $City, $SourceInfo, $VizitDate,
										$VizitTime, $VIN, $Configuration, $ModelColor, $UserAction, $TOType, $ContactingTheme, $CustomCity, $Status, $ForShaduler);
	}

    /**
	 *
	 * Возвращает информацию для построения формы заказа
	 * @param integer $Model - Идентификатор модели
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetFullDataForm($Facility)
	{
        $R = array();
		$this->Dump("+++ 1");
        $R["partners"] = DealersProcessor::Init()->GetDealersNew(1);
		
		$this->Dump("+++ 2");
		
		$R["models"] = ModelsProcessor::Init()->GetModelsNew($Facility);
		
		$this->Dump("+++ 3");
		
		$R["modelsFamilies"] = ModelsProcessor::Init()->GetModelsFamilies($Facility);
		
		$this->Dump("+++ 4");
		
        $R["cities"] = GeoProcessor::Init()->GetDealersCitiesNew();
		
		$this->Dump("+++ 5");
		
        $R["partnersModels"] = FacilitiesProcessor::Init()->GetDealersModelsAll($Facility);
		
		$this->Dump("+++ 6");
        return $R;
	}

	public function GetContactingThemes($Facility)
	{
		return $this->CData->GetContactingThemes(WS::Init()->GetBrandId(), $Facility);
	}


	public function GetSourceInfoList($Facility)
	{
		return $this->CData->GetSourceInfoList($Facility);
	}




    public function Send($Facility, $FormType, $MemberData)
	{
        $Tempaltes = $this->CData->GetTemplates(WS::Init()->GetBrandId(), $Facility, $FormType);

		$this->Dump(__METHOD__, $Facility, $FormType, $MemberData, $Tempaltes);

        $PhoneInfo = UtilsProcessor::Init()->GetPhoneInfo($MemberData["phone"]);
		/*
        if($PhoneInfo["error"] == 0)
        {
            $MemberData["phone"] = $PhoneInfo["phone"];
            $MemberData["code"] = $PhoneInfo["code"];
        }
        else $MemberData["code"] = "";
		 */
		$MemberData["code"] = "";
        if($Tempaltes["dealerSend"])
        {
            $Emails = $this->CData->GetDealerAndUniversalEmail($Facility, $FormType, $Dealer["partnerId"], WS::Init()->IsTest());
            $this->SendEmail($Emails,
                            $MemberData,
                            $Tempaltes["dealerMailForm"],
                            $Tempaltes["dealerMailFormName"],
                            $Tempaltes["dealerMailSubject"],
                            $Tempaltes["dealerMailTemplate"],
                            $Tempaltes["dealerAsHTML"],
                            $Tempaltes["dealerCharset"]);
        }
$this->Dump(__METHOD__, "+++", $Tempaltes["tmrSend"]);
        if($Tempaltes["tmrSend"])
        {
			$Emails = $this->CData->GetUniversalEmail($Facility, $FormType, WS::Init()->IsTest(), array(1, 2));
$this->Dump(__METHOD__, "+++ 2", $Emails);
            if($Emails)
	            $this->SendEmail($Emails,
								$MemberData,
								$Tempaltes["tmrMailForm"],
								$Tempaltes["tmrMailFormName"],
								$Tempaltes["tmrMailSubject"],
								$Tempaltes["tmrMailTemplate"],
								$Tempaltes["tmrAsHTML"],
								$Tempaltes["tmrCharset"],
								$Tempaltes["tmrAttachmentMailTemplate"],
								$Tempaltes["tmrAttachmentMailTemplateName"]);
        }
        if($Tempaltes["userSend"] && $MemberData["email"])
        {
            if(isset($MemberData["genderId"]))
                $MemberData["name"] = ($MemberData["genderId"] ? "Уважаемая " : "Уважаемый ").$MemberData["firstName"];
            $this->SendEmail(	$MemberData["email"],
                                $MemberData,
                                $Tempaltes["userMailForm"],
                                $Tempaltes["userMailFormName"],
                                $Tempaltes["userMailSubject"],
                                $Tempaltes["userMailTemplate"],
                                $Tempaltes["userAsHTML"],
                                $Tempaltes["userCharset"]);
        }
	}

								/*
									"facilityId" => 2, "formType" => 2, "partnerDivisionId" => 0, "modelId" => 0, "registerStart" => 0, "registerEnd" => 0,
									"visitDateStart" => 0, "visitDateEnd" => 0
								 */
    public function GetMembers($Facility, $FormType = null, $Dealer = null, $Model = null, $RegisterStart = null, $RegisterEnd = null, $VisitStart = null, $VisitEnd = null)
    {
        $Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT,
																	PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
        return $this->CData->GetMembers(WS::Init()->GetBrandId(), $Facility, $FormType ? $FormType : 1, $Dealer, $Model, $RegisterStart, $RegisterEnd, $VisitStart, $VisitEnd);
    }

	public function ExportMembers($Facility, $FormType = null, $Dealer = null, $Model = null, $RegisterStart = null, $RegisterEnd = null, $VisitStart = null, $VisitEnd = null)
    {
        if(!$FormType)
            $FormType = 1;
        switch($Facility)
        {
            case 50:
				$Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT,
																			PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
				$R = $this->CData->GetMembersForReport(WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, $Model, $RegisterStart, $RegisterEnd, $VisitStart, $VisitEnd);
                array_unshift(  $R,
                                array(
                                    "Фамилия",
                                    "Имя",
                                    "Пол",
                                    "Возраст",
                                    "Дилерский центр",
                                    "Модель",
                                    "Электронная почта",
                                    "Телефон",
                                    "Заявка получена с сайта дилера",
                                    "Дата отправки заявки",
									"Желаемое время для звонка",
									"Источник трафика",
									"Идентификатор заявки"
                                ));
                                break;
            case 52:
				//TODO: insert correct permissions
				$Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT,
																			PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
				$R = $this->CData->GetMembersForReport(WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, null, $RegisterStart, $RegisterEnd);
                if($FormType == 1)
				{
                    array_unshift(  $R,
                                    array(
                                        "Фамилия",
                                        "Имя",
                                        "Пол",
                                        "Возраст",
                                        "Дилерский центр",
                                        "Модель",
                                        "Электронная почта",
                                        "Телефон",
                                        "Заявка получена с сайта дилера",
                                        "Автомобиль в настоящее время",
                                        "Дата отправки заявки",
										"Идентификатор заявки"
                                    ));
					WS::Init()->FileOriginalName = "Zayavki_na_testdrive.csv";
				}
                else array_unshift(  $R,
                                    array(
                                        "Фамилия",
                                        "Имя",
                                        "Пол",
                                        "Дилерский центр",
                                        "Модель",
                                        "Электронная почта",
                                        "Телефон",
                                        "Заявка получена с сайта дилера",
                                        "Дата отправки заявки",
										"Идентификатор заявки"
                                    ));
                break;
            case 53:
				//TODO: insert correct permissions
				$Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT,
																			PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
				$R = $this->CData->GetMembersForReport(WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, null, $RegisterStart, $RegisterEnd);
                array_unshift(  $R,
                                array(
                                    "Фамилия",
                                    "Имя",
                                    "Пол",
                                    "Дилерский центр",
                                    "Модель",
                                    "Электронная почта",
                                    "Телефон",
                                    "Заявка получена с сайта дилера",
                                    "Дата отправки заявки",
									"Идентификатор заявки"
                                ));
                break;
			 case 76:
				$Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT,
																			PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
				$R = $this->CData->GetMembersForReport(WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, $Model, $RegisterStart, $RegisterEnd, $VisitStart, $VisitEnd);
                array_unshift(  $R,
                                array(
                                    "Имя",
                                    "Дилерский центр",
                                    "Модель",
                                    "Электронная почта",
                                    "Телефон",
                                    "Заявка получена с сайта дилера",
                                    "Дата отправки заявки",
									"Желаемая дата визита",
									"Желаемое время визита",
									"Источник трафика",
									"Идентификатор заявки"
                                ));
                                break;
			 case 77:
				$Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT,
																			PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
				$R = $this->CData->GetMembersForReport(WS::Init()->GetBrandId(), $Facility, $FormType, $Dealer, $Model, $RegisterStart, $RegisterEnd, $VisitStart, $VisitEnd);
                array_unshift(  $R,
                                array(
                                    "Имя",
                                    "Дилерский центр",
                                    "Модель",
                                    "Электронная почта",
                                    "Телефон",
                                    "Заявка получена с сайта дилера",
                                    "Дата отправки заявки",
									"Желаемая дата визита",
									"Желаемое время визита",
									"Источник трафика",
									"Идентификатор заявки"
                                ));
                                break;
        }
        return $R;
    }





    public function GetEmails($Facility, $Dealer = null)
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT, PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY), $Dealer);
        return $this->CData->GetEmails(WS::Init()->GetBrandId(), $Facility, $Scope);
    }

    public function DeleteEmail($Email, $Dealer = null)
    {
        PermissionsProcessor::Init()->CheckAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_MANAGEMENT), $Dealer);
        $this->CData->DeleteEmail(1, $Dealer, $Email);
    }

    public function ClearEmail($Dealer = null)
    {
        PermissionsProcessor::Init()->CheckAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_MANAGEMENT), $Dealer);
        $this->CData->ClearEmail(1, $Dealer);
    }

    public function AddEmail($Email, $Dealer = null)
    {
        if(!$Dealer)
            $Dealer = User::Init()->GetPartnerId();
        PermissionsProcessor::Init()->CheckAccess(array(PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_MANAGEMENT), $Dealer);
        return $this->CData->AddEmail(1, $Dealer, $Email);
    }




    protected function SendEmail($Emails, $Data, $From, $FromName, $Subject, $Template, $AsHTML, $Charset = null, $AttachmentTemplate = null, $AttachmentName = null)
    {
        if(!$Emails)
            throw new dmtException("Email is not found", 5);
        $Mailer = new MailDriver(false, true);
        if(is_array($Emails))
            foreach($Emails as $v)
			{
				switch($v["sendAs"])
				{
					case 1:
						$Mailer->AddBCC($v["email"]);
						break;
					case 2:
						$Mailer->AddCC($v["email"]);
						break;
					default:
						$Mailer->AddAddress($v["email"]);
				}
			}
        else $Mailer->AddAddress($Emails);
        $Message = Templater2::Init()->Merge(null, $Data, null, null, $Template);

		$Mailer->From = $From;
        if($Charset && $Charset != CHARSET)
		{
			$Mailer->CharSet = $Charset == "cp1251" ? CHARSET_WIN1251 : $Charset;
            $Message = iconv(CHARSET, $Charset.'//IGNORE', $Message);
			$Mailer->FromName = iconv(CHARSET, $Charset.'//IGNORE', $FromName);
			$Mailer->Subject = iconv(CHARSET, $Charset.'//IGNORE', Templater2::Init()->Merge(null, $Data, null, null, $Subject));
		}
		else
		{
			$Mailer->CharSet = CHARSET;
			$Mailer->FromName = $FromName;
			$Mailer->Subject = Templater2::Init()->Merge(null, $Data, null, null, $Subject);
		}
        if($AsHTML)
            $Mailer->MsgHTML($Message);
        else $Mailer->Body = $Message;
        $Attachment = null;
        if($AttachmentTemplate)
        {
            $Attachment = Templater2::Init()->Merge(null, $Data, null, null, $AttachmentTemplate);
            if($Charset != CHARSET)
                $Attachment = iconv(CHARSET, $Charset.'//IGNORE', $Attachment);
            $Mailer->AddStringAttachment($Attachment, $AttachmentName ? $AttachmentName : 'file.txt');
        }
        $Mailer->Send();
    }




	public function EventAcceptTest($Detail)
	{
		$this->Dump(__METHOD__.": ".__LINE__, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", $Detail);
	}
}