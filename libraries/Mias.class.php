<?php
class Mias extends Controller
{
  	/**
	 *
	 * @var Mias
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return Mias
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"userFirstname"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"userLastname"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
				"userDealerId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"newEngineId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"dealerId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"gender"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
				"firstname"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"cityId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"lastname"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "age"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "phone"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
				),
                "email"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "oldBrandId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "oldCarId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "news"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "catalog"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "catalogCars"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "price"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "priceCars"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "test"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "order"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "callme"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "question"             => array(
                    "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FULL_TEXT)
                ),
                "answer"             => array(
                     "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FULL_TEXT)
                 ),
                "comment"             => array(
                     "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FULL_TEXT)
                 ),
                "questions"				=> array(
                    "TplVars"             => array("question" => 0, "answer" => 0, "comment" => 0)
				),
                "car"             => array(
                     "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT)
                 ),
                "category"             => array(
                     "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FULL_TEXT)
                 ),
                "text"             => array(
                     "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FULL_TEXT)
                 ),
				"type"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"registerStart"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerEnd"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
			)
		);
		$this->Modes = array(
			//Возвращает
			"forms"	=> array(
				"exec"		=> array("MiasProcessor", "AddMember"),
				"TplVars"		=> array("userFirstname" => 0, "userLastname" => 0, "userDealerId" => 0, "newEngineId" => 0, "dealerId" => 0, "gender" => 0,
                                        "firstname" => 0, "lastname" => 0, "cityId" => 0, "age" => 0, "phone" => 0, "email" => 0, "oldBrandId" => 0, "oldCarId" => 0,
                                        "news" => 0, "catalog" => 0, "catalogCars" => 0, "price" => 0, "priceCars" => 0, "test" => 0, "order" => 0, "callme" => 0, "type" => 0)
			),
            //Возвращает
			"forms2"	=> array(
				"exec"		=> array("MiasProcessor", "AddQuestions"),
				"TplVars"		=> array("questions" => 2)
			),
            //Возвращает
			"forms3"	=> array(
				"exec"		=> array("MiasProcessor", "AddQuest"),
				"TplVars"		=> array("car" => 2, "category" => 2, "text" => 2)
			),
			"x"	=> array(
				"exec"		=> array("MiasProcessor", "GetMembersCSV"),
				"TplVars"		=> array("registerStart" => 2, "registerEnd" => 0)
			),
			"x1"	=> array(
				"exec"		=> array("MiasProcessor", "GetMembersTDCSV"),
				"TplVars"		=> array("registerStart" => 2, "registerEnd" => 0)
			),
		);
	}
}