<?php
/**
 * Константы базы данных
 */
define("DBS_TEST_DRIVE",					"lexusmasterclass_masterclass");
define("DBS_REFERENCES",					"lexus_references");
define("DBS_UNIVERSAL_REFERENCES",			"references");
define("DBS_USERS",							"lexus_users");


define("DBS_SUBSCRIBE_TMP",					"lsub");


switch (SYSTEM_LOCATION_CURRENT)
{
	case SYSTEM_LOCATION_PRODACTION:
		/**
		 * Пароль
		 */
		define("DB_SUBSCRIBE_PASSWORD",				"jYesAqhi");

		/**
		 * Хост БД
		 */
		define("DB_SUBSCRIBE_HOSTNAME",				"localhost");

		/**
		 * Порт
		 */
		define("DB_SUBSCRIBE_PORT",					3306);



		/**
		* Имя пользователя
		*/
		define("DB_USERNAME",						"igor_borschenkov");

		/**
		* Пароль
		*/
		define("DB_PASSWORD",						"K2opmckSDgbf");


		//define("DB_PASSWORD_GENERAL",				"123456789");

		/**
		 * Хост БД
		 */
		define("DB_HOSTNAME",						"192.168.0.2");

		/**
		 * Порт
		 */
		define("DB_PORT",							3306);
		
		define("DB_SUBSCRIBE_USERNAME",				"lsub");
		break;
	case SYSTEM_LOCATION_DEVELOPMENT:
		/**
		 * Имя пользователя
		 */
		define("DB_USERNAME",						"root");
		/**
		 * Пароль
		 */
		define("DB_PASSWORD",						"");
		/**
		 * Хост БД
		 */
		define("DB_HOSTNAME",						"127.0.0.1");
		/**
		 * Порт
		 */
		define("DB_PORT",							3306);
		define("DB_SUBSCRIBE_USERNAME",				"root");
		/**
		* Пароль
		*/
		define("DB_SUBSCRIBE_PASSWORD",				"");

		/**
		* Хост БД
		*/
		define("DB_SUBSCRIBE_HOSTNAME",				"127.0.0.1");

		/**
		* Порт
		*/
		define("DB_SUBSCRIBE_PORT",					3306);
		break;
}




/**
 * Тип используемой СУБД
 */
define("DB_TYPE",							"mysql");

/**
 * Название БД
 */
define("DB_DBNAME",							DBS_REFERENCES);

/**
 *
 * Кодировка БД
 * @var string
 */
define("DB_CHARSET",						"UTF8");

/**
 *
 * Время ожидания установки именованной блокировки в секундах
 * @var integer
 */
define("DB_TIMEOUT_NAMED_LOCKS",			1);

/**
 *
 * Уровень изоляции транзакций
 *
 * REPEATABLE-READ  1
 * READ-UNCOMMITTED 2
 * READ-COMMITTED   3
 * SERIALIZABLE     4
 * @var integer
 */
define("DB_ISOLATION_LEVEL",				4);


/**
 * Основные пользовательские переменные БД (для MySQL)
 */

define("DB_VARS_CURRENT_DATE", 				"@CurrDT");
define("DB_VARS_ACCOUNT_CURRENT",			"@CurrUser");
define("DB_VARS_PARTNER_CURRENT",			"@CurrPartner");
define("DB_VARS_LANG_CURRENT",				"@CurrLang");
define("DB_VARS_BRAND_CURRENT",				"@CurrBrand");
define("DB_VARS_CLIENT_CURRENT",			"@CUID");
define("DB_VARS_CLIENT_CURRENT_CODE",		"@ClientCode");
