<?php
class ViewAdminMenu extends Base
{
	public static function GetMenu()
	{
		$Mn = array(
			array('<a %{%menu.campaigns.select%}% href="/admin/campaigns">Кампании</a>', array(ROLE_ADMIN, ROLE_SUPER_PARTNER, ROLE_TMR)),
			array('<a %{%menu.campaigns.select%}% href="/admin/campaigns/members">Участники заездов</a>', array(ROLE_DEALER, ROLE_PARTNER, ROLE_SUPER_PARTNER)),
			array('<a %{%menu.reports.select%}% href="/admin/reports">Отчеты</a>', array(ROLE_ADMIN, ROLE_DEALER, ROLE_PARTNER, ROLE_SUPER_PARTNER, ROLE_TMR)),
			array('<a %{%menu.data.select%}% href="/admin/import">Загрузка данных</a>', array(ROLE_ADMIN, ROLE_SUPER_PARTNER, ROLE_TMR, ROLE_SPATIAL)),
			array('<a %{%menu.data.select%}% href="/admin/export">Выгрузка данных</a>', array(ROLE_ADMIN, ROLE_SUPER_PARTNER, ROLE_TMR, ROLE_SPATIAL)),
			array('<a %{%menu.partners.select%}% href="/admin/partners">Партнеры</a>', array(ROLE_ADMIN, ROLE_TMR)),
			array('<a %{%menu.admins.select%}% href="/admin/users">Пользователи</a>', array(ROLE_ADMIN, ROLE_TMR)),
			array('<a href="https://www.google.com/analytics/web/?hl=ru&pli=1#report/visitors-overview/a31748530w58605937p59829693/" target="_black">Статистика сайта</a>', array(ROLE_ADMIN, ROLE_TMR)),
			array('<a href="/admin/?a=a&b=c">Выход</a>', array(ROLE_ADMIN, ROLE_DEALER, ROLE_PARTNER, ROLE_SUPER_PARTNER, ROLE_TMR, ROLE_SPATIAL)),
			//array('<div style="float:right;">'.User::Init()->GetName()."</div>", array(ROLE_ADMIN, ROLE_DEALER, ROLE_PARTNER, ROLE_SUPER_PARTNER, ROLE_TMR)),
			//array('<div style="clear:both;"></div>', array(ROLE_ADMIN, ROLE_DEALER, ROLE_PARTNER, ROLE_SUPER_PARTNER, ROLE_TMR))
		);
		$R = array();

		foreach ($Mn as $v)
		{
			if(PermissionsProcessor::Init()->CheckAccess($v[1], User::Init()->GetAccount(), true))
				$R[] = $v[0];
		}

		return implode(" | ", $R).'<div style="float:right;margin-right:20px;margin-left:50px;">'.User::Init()->GetName().'</div><div style="clear:both;"></div>';
	}
}