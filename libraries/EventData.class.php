<?php
class EventData extends Data
{
	/**
	 * Формирует событие для пользователей или клиентов пользователей
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $Type - Идентификатор типа события
	 * @param integer $Category - Категория события
	 * @param array $Detail - Детали события
	 * @param integer $Dealer - Идентификатор дилера, связанного с событием
	 * @param mixed $ExclUser - Идентификатор или массив идентификаторов пользователя, исключаемого из адресатов рассылки события
	 * @param string $ExclCUID - Идентификатор клиента, исключаемого из адресатов рассылки события
	 */
	public function CreateEvent($Service, $Type, $Category, $Detail, $Dealer, $ExclUser = null, $ExclCUID = null)
	{
        $this->Exec("Call `references`.`CreateEvent`(
                                                ".$Service.",
                                                ".$Type.",
                                                ".$Category.",
                                                ".$this->Esc($Detail).",
                                                ".$this->Esc($Dealer).",
                                                ".$this->CheckNull($ExclUser).",
                                                ".$this->Esc($ExclCUID).");");
	}

	/**
	 * Возвращает события для текущего пользователя и текущего клиента
	 * @param integer $Timestamp - Метка времени последнего запроса в секундах
	 * @return array
	 */
	public function GetEvents($Timestamp)
	{
        $Id = ClientsProcessor::Init()->GetClientId();
        if(!$Id)
            throw new dmtException("Invalid CUID", 81113);
            $R = $this->Get("SELECT
	`event_id` AS eventId,
    `service_id` AS serviceId,
	`events_type_id` AS eventType,
	`events_category_id` AS eventCategory,
	`status` AS eventStatus,
	`detail` AS eventDetail,
	`create` AS eventCreate
FROM `references`.`users_events`
WHERE `event_id` IN (
	SELECT
		`event_id`
	FROM `references`.`users_events_users`
	WHERE `user_id`=".DB_VARS_ACCOUNT_CURRENT."
	UNION
	SELECT
		`event_id`
	FROM `references`.`users_clients_users_events`
	WHERE `client_id`=".$Id."
)".($Timestamp ? "
    AND UNIX_TIMESTAMP(`create`)" : "").";");

        $this->Exec("DELETE FROM `references`.`users_events`
WHERE `event_id` IN (
	SELECT
		`event_id`
	FROM `references`.`users_clients_users_events`
	WHERE `client_id`=".$Id."
);");
        return $R;
	}


	public function DeliveredEvent($Event)
	{

	}

	public function ConfirmedEvent($Event)
	{

	}

	public function WorkedEvent($Event)
	{

	}
	
	/**
	 * Удаляет события-уведомления с подтверждением прочтения для текущего пользователя
	 * @param integer $Event - Идентификатор события
	 */
	public function ClosedEvent($Event)
	{
        if(!is_array($Event))
            $Event = array($Event);
        foreach($Event as $v)
            $this->Exec("CALL `references`.`DeleteEvent`(".$v.")");
        return $Event;
	}
	
	/**
	 * Возвращает системные подписки сервисов для заданного типа события
	 * @param integer $EventType - Идентификатор типа события
	 * @return array
	 */
	public function GetServicesSubscription($EventType)
	{
		return $this->Get("SELECT
	`class_name` AS className,
	`method_name` AS methodName,
	`detail` AS detail
FROM `references`.`events_services_subscriptions`
WHERE `events_type_id`".$this->PrepareValue($EventType).";");
	}
	
	
	public function AutoUpdate($Script)
	{
		$this->Exec("INSERT INTO `references`.`sys_auto_update`
	(`script`,
	`last_update`)
VALUES
	(".$this->Esc($Script).",
	NOW())
ON DUPLICATE KEY UPDATE
	`last_update`=VALUES(`last_update`);");
	}
	
	public function DateDiffAutoUpdate($Script)
	{
		return $this->Count("SELECT
    DATEDIFF(NOW(), `last_update`) AS Cnt
FROM `references`.`sys_auto_update`
WHERE `script`=".$this->Esc($Script).";");
	}
	
	public function GetLastAutoUpdate($Script)
	{
		return $this->Count("SELECT
    `last_update` AS Cnt
FROM `references`.`sys_auto_update`
WHERE `script`=".$this->Esc($Script).";");
	}
}