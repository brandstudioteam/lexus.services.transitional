<?php
class PartnersTypes extends Controller
{
	/**
	 *
	 * @var PartnersTypes
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return PartnersTypes
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"partnerTypeId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"pertnerTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"pertnerTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
			)
		);
		$this->Modes = array(
			//Создает тип партнера
			"a"	=> array(
				"exec"		=> array("PartnersProcessor", "CreatePartnerType"),
										//$Name, $Description
				"TplVars"		=> array("pertnerTypeName" => 1, "pertnerTypeDescription" => 0)
			),
            //Сохраняет изменения типа партнера
			"b"	=> array(
				"exec"		=> array("PartnersProcessor", "SavePartnerType"),
										//$Name, $Description
				"TplVars"		=> array("pertnerTypeId" => 1, "pertnerTypeName" => 1, "pertnerTypeDescription" => 0)
			),
             //Удаляет тип партнера
			"c"	=> array(
				"exec"		=> array("PartnersProcessor", "DeletePartnerType"),
										//$Name, $Description
				"TplVars"		=> array("pertnerTypeId" => 1)
			),
			//Возвращает список типов партнеров
			"d"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnersTypeList")
			)
		);
	}
}