<?php
class UtilsProcessor extends Processor
{
	/**
	 * 
	 * @var UtilsProcessor
	 */
	protected static $Inst = false;
	
	/**
	 * 
	 * @var UtilsData
	 */
	protected $CData;
	
	/**
	 * 
	 * Инициализирует класс
	 * 
	 * @return UtilsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}
	
	protected function __construct()
	{
		parent::__construct();
		$this->CData = new UtilsData();
	}
	
	public function GetPhoneInfo($Phone)
	{
		return $this->CData->GetPhoneInfo($Phone);
	}
}