<?php
class Roles extends Controller
{
	/**
	 *
	 * @var Roles
	 */
	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Roles
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				//Логин
				"roleId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				//Пароль
				"roleTypeId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"roleName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"roleDescription"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"roleTypeName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"roleTypeDescription"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Логин
				"permissions"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Логин
				"serviceId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
			)
		);

		$this->Modes = array(
			//Получить список ролей
			"a"	=> array(
				"exec"			=> array("RolesProcessor", "GetRoles"),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					),
				),
				//"TplVars"			=> array("roleId" => 1)
			),
			//Создать роль
			"b"	=> array(
				"exec"				=> array("RolesProcessor", "CreateRole"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleTypeId" => 2, "roleName" => 2, "roleDescription" => 0, "permissions" => 2)
			),
			//Изменить данные роли
			"c"	=> array(
				"exec"				=> array("RolesProcessor", "SaveRole"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 2, "roleName" => 0, "roleDescription" => 0, "permissions" => 0)
			),
			//Удалить роль
			"d"	=> array(
				"exec"				=> array("RolesProcessor", "DeleteRole"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							70007			=> "CannotDeleteAParentData",
						)
					)
				),
				"TplVars"			=> array("roleId" => 2)
			),
			//Получить права доступа к роли
			"e"	=> array(
				"exec"				=> array("RolesProcessor", "GetRolesAccess"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 0)
			),
			//Установить права доступа к роли
			"f"	=> array(
				"exec"				=> array("RolesProcessor", "SetRoleAccess"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
			//Получить права доступа к сервисам, привязанные к роли
			"g"	=> array(
				"exec"				=> array("RolesProcessor", "GetRoleServicePermissions"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
			//Получить права доступа, привязанные к роли для заданного сервиса
			"h"	=> array(
				"exec"				=> array("RolesProcessor", "GetRolePermissionsForService"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1, "serviceId" => 1)
			),
			//Установить права доступа, привязанные к роли
			"j"	=> array(
				"exec"				=> array("RolesProcessor", "SaveRolePermissions"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1, "permission" => 1)
			),
			//Получить список пользователей, к которым привязана данная роль
			"i"	=> array(
				"exec"				=> array("RolesProcessor", "GetUsersForRole"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
			//Получить список ролей
			"l"	=> array(
				"exec"			=> array("RolesProcessor", "GetRolesForType"),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					),
					"succes"			=> array(7, "RegisterSucTxt")
				),
				"TplVars"			=> array("roleTypeId" => 1)
			),
			//Удалить права доступа, привязанные к роли для заданного сервиса
			"m"	=> array(
				"exec"				=> array("RolesProcessor", "DeleteRolePermissionsForService"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1, "serviceId" => 1)
			),
			//Получить базовые права доступа к сервисам, не привязанные к роли
			"n"	=> array(
				"exec"				=> array("RolesProcessor", "GetBasicAccessForServices"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
            //Получить зависимоти ролей от типов пользователей
			"o"	=> array(
				"exec"				=> array("RolesProcessor", "GetRolesTypesUsersTypes"),
			),
			//Получить зависимоти ролей от типов пользователей
			"p"	=> array(
				"exec"				=> array("RolesProcessor", "GetRolesTypes"),
			),
		);
	}
}