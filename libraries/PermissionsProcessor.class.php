<?php
define("PERMISSIONS_SERVICE_ID",												14);

class PermissionsProcessor extends Processor
{
	/**
	 *
	 * @var PermissionsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var PermissionsData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return PermissionsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new PermissionsData();
	}

	/**
	 *
	 * Проверяет право у пользователя
	 * @param integer $Permission - Идентификатор права
	 * @param integer $User - Идентификатор пользователя
	 */
	public function CheckAccess($Permission, $Partner = null, $AsBoolean = null)
	{
		return $this->CData->CheckAccess($Permission, $Partner, $AsBoolean);
	}

    /**
	 *
	 * Проверяет право у пользователя
	 * @param integer $Permission - Идентификатор права
	 * @param integer $User - Идентификатор пользователя
	 */
	public function CheckScopeAccess($CheckedPartner, $Permission, $AsExeption = null)
	{
		$R = $this->CData->CheckAccess($Permission, $CheckedPartner, !$AsExeption);
        if($R)
            $R = PartnersProcessor::Init()->CheckPartnersHierarchy($CheckedPartner, $AsExeption);
	}

    /**
	 *
	 * Проверяет право у пользователя
	 * @param integer $Permission - Идентификатор права
	 */
	public function GetScopeAccess($Permission, $Partner = null, $NotException = null)
	{
		$R = $this->CData->GetScopeAccess($Permission, $Partner);
        if(!sizeof($R) && !$NotException)
            throw new dmtException("Scope access denid", 1005);
        $R = $this->PrepareArray($R);
        return $R;
	}



	/**
	 *
	 * Возвращает сумму кодов прав персоны для заданного сервиса
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $User - Идентификатор пользователя
	 */
	public function GetAccess($Service, $User)
	{
		return $this->CData->GetAccess($Service, $User);
	}

	/**
	 *
	 * Возвращает список прав для заданного сервиса или заданного сервиса и заданного пользователя
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $User - Идентификатор пользователя
	 */
	public function GetPermissions()
	{
        $this->CheckAccess(array(PERMISSIONS_ROLES_READ, PERMISSIONS_PERMISSIONS_READY, PERMISSIONS_PERMISSIONS_READY_FOR_CLIENT));
		return $this->CData->GetPermissions();
	}

    public function GetScopeForClient($Service)
    {
        return $this->CData->GetScopeAccessForService($Service);
    }

    public function GetPermissionsForClient($Service = null)
    {
        return $this->CData->GetPermissionsForClient($Service);
    }

    public function GetUserPermissionsData()
    {
        
    }
}