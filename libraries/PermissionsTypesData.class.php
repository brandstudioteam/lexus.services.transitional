<?php
class PermissionsTypesData extends Data
{
    public function GetPermissionsTypesList()
    {
        return $this->Get("SELECT
	`permissions_type_id` AS permissionsTypeId,
	`name` AS permissionsTypeName,
	`description` AS permissionsTypeDescription
FROM `".DBS_USERS."`.`permissions_types`
ORDER BY `name`;");
    }

    public function CreatePermissionsType($Name, $Description)
    {
        $this->Exec("INSERT INTO `".DBS_USERS."`.`permissions_types`
	(`name`,
	`description`)
VALUES
	(".$this->Esc($Name, true, false, true)."
    ".$this->Esc($Description).");");
        return $this->DB->GetLastID();
    }

    public function SavePermissionsType($PermissionType, $Name, $Description)
    {
        $U = array();
        if($Name)
            $U[] = "`name`=".$this->Esc($Name);
        if($Description)
            $U[] = "`description`=".$this->Esc($Description);
        return sizeof($U) ? $this->Exec("UPDATE `".DBS_USERS."`.`permissions_types`
SET ".implode(",", $U)."
WHERE `permissions_type_id`=".$PermissionType.";") : null;
    }

    public function DeletePermissionsType($PermissionType)
    {
        return $this->Exec("DELETE FROM `".DBS_USERS."`.`permissions_types`
WHERE `permissions_type_id`=".$PermissionType.";");
    }
}