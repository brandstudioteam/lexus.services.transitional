<?php
class RolesData extends Data
{
	/**
	 *
	 * Получить список ролей для заданного типа
	 */
	public function GetRolesForType($Type)
	{
		return $this->Get("SELECT
	r.`role_id` AS roleId,
	r.`roles_type_id` AS roleTypeId,
	r.`name` AS roleName,
	r.`descriptioin` AS roleDescription,
	rt.`name` AS roleTypeName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles` AS r
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_types` AS rt ON rt.`roles_type_id`=r.`roles_type_id`
WHERE r.`roles_type_id`=".$Type.";");
	}

	/**
	 *
	 * Получить список ролей
	 */
	public function GetRoles($Role = null)
	{
		return $this->Get("SELECT
	a.`role_id` AS roleId,
	a.`roles_type_id` AS roleTypeId,
	a.`name` AS roleName,
	a.`descriptioin` AS roleDescription,
	b.`name` AS roleTypeName
FROM `references`.`roles` AS a
LEFT JOIN `references`.`roles_types` AS b ON b.`roles_type_id`=a.`roles_type_id`
LEFT JOIN `references`.`roles_types_users_types` AS c ON c.`roles_type_id`=a.`roles_type_id`
WHERE ".($Role ? "a.`role_id`=".$Role : "c.`users_type_id`=".User::Init()->GetType()).";", (boolean) $Role);
		
		/*
		return $this->Get("SELECT
	r.`role_id` AS roleId,
	r.`roles_type_id` AS roleTypeId,
	r.`name` AS roleName,
	r.`descriptioin` AS roleDescription,
	rt.`name` AS roleTypeName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles` AS r
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_types` AS rt ON rt.`roles_type_id`=r.`roles_type_id`".($Role ? "
WHERE r.`role_id`=".$Role : "").";", (boolean) $Role);
		
		if($Role) $Query = "SELECT
	r.`role_id` AS roleId,
	r.`roles_type_id` AS roleTypeId,
	r.`name` AS roleName,
	r.`descriptioin` AS roleDescription,
	rt.`name` AS roleTypeName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles` AS r
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_types` AS rt ON rt.`roles_type_id`=r.`roles_type_id`
WHERE r.`role_id`=".$Role.";";
		else $Query = "SELECT
	a.`role_id` AS roleId,
	a.`roles_type_id` AS roleTypeId,
	a.`name` AS roleName,
	a.`descriptioin` AS roleDescription,
	b.`name` AS roleTypeName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_types` AS b ON b.`roles_type_id`=a.`roles_type_id`
WHERE a.`role_id` IN (
		SELECT DISTINCT
			`role_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
		WHERE `user_id`=".DB_VARS_ACCOUNT_CURRENT."
	)";

		return $this->Get($Query, (boolean) $Role);
		 */
	}

	/**
	 *
	 * Создать роль
	 * @param integer $Type - Тип роли
	 * @param string $Name - Название роли
	 * @param string $Desc - Описание роли
	 */
	public function CreateRole($Type, $Name, $Desc, $Permissions)
	{
        $this->Begin();
        try
        {
            $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`roles`
	(`roles_type_id`,
	`name`,
	`descriptioin`)
VALUES
	(".$Type.",
	".$this->Esc($Name, true, false, true).",
	".$this->Esc($Desc).");");
            $Role = $this->DB->GetLastID();
            $this->SetRoleAccess($Role, $Permissions);
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
		return $Role;
	}

	/**
	 *
	 * Изменить данные роли
	 * @param integer $Role - Идентификатор роли
	 * @param string $Name - Название роли
	 * @param string $Desc - Описание роли
	 */
	public function SaveRole($Role, $Name, $Desc, $Permissions)
	{
		$A = array();
		if($Name)
		{
			$A[] = "`name`=".$this->Esc($Name, true, false, true);
		}
		if($Desc !== null)
		{
			$A[] = "`descriptioin`=".$this->Esc($Desc);
		}
        $this->Begin();
        try
        {
            if(sizeof($A))
                $this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`roles`
SET ".implode(", ", $A)."
WHERE `role_id`=".$Role.";");
            if($Permissions)
                $this->SetRoleAccess($Role, $Permissions);
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
	}

	/**
	 *
	 * Удалить роль
	 * @param integer $Role - Идентификатор роли
	 */
	public function DeleteRole($Role)
	{
        $this->Begin();
        try
        {
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions`
WHERE `role_id`=".$Role.";");
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles`
WHERE `role_id`=".$Role.";");
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
	}

	/**
	 *
	 * Получить права доступа к роли
	 * @param integer $Role - Идентификатор роли
	 */
	public function GetRolesAccess($Role = null)
	{
        $Query = "SELECT
	a.`role_id` AS roleId,
	a.`permission_id` AS permissionId,
	b.`name` AS permissionName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS b ON b.`permission_id`=a.`permission_id`";
        if($Role)
            $Query .= "
WHERE a.`role_id`".$this->PrepareValue($Role);
        return $this->Get($Query);
	}

	/**
	 *
	 * Установить права доступа к роли
	 * @param integer $Role - Идентификатор роли
	 * @param array $Access - Массив прав доступа
	 */
	public function SetRoleAccess($Role, $Access)
	{
        if(!$Role || !is_array($Access) || !sizeof($Access))
            throw new dmtException("Agruments is error");
        $A = array();
        foreach($Access as $v)
            $A[] = "(".$Role.", ".$v.")";
        if(sizeof($A))
        {
            $this->Begin();
            try
            {
                $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions`
WHERE `role_id`=".$Role.";");
                $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions`
    (`role_id`,
    `permission_id`)
VALUES ".implode(", ", $A));
                $this->Commit();
            }
            catch(dmtException $e)
            {
                $this->Rollback();
                throw new dmtException($e->getMessage(), $e->getCode(), true);
            }
        }
    }


	/**
	 *
	 * Получить права доступа к сервисам, привязанные к роли
	 * @param integer $Role - Идентификатор роли
	 */
	public function GetRoleServicePermissions($Role)
	{
		return $this->Get("SELECT
	p.`permission_id` AS permissionId,
	p.`service_id` AS serviceId,
	p.`permissions_type_id` AS permissionTypeId,
	p.`code` AS permissionCode,
	p.`name` AS permissionName,
	p.`description` AS permissionDescription,
	rp.`role_id` AS roleId,
	s.`name` AS serviceName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services` AS s ON s.`service_id`=p.`service_id`
WHERE rp.`role_id`=".$Role."
	AND p.`permissions_type_id`=2;");
	}

	/**
	 *
	 * Получить права доступа, привязанные к роли для заданного сервиса
	 * @param integer $Role - Идентификатор роли
	 * @param integer $Service - Идентификатор сервиса
	 */
	public function GetRolePermissionsForService($Role, $Service)
	{
		return $this->Get("SELECT
	p.`permission_id` AS permissionId,
	p.`service_id` AS serviceId,
	p.`permissions_type_id` AS permissionTypeId,
	p.`code` AS permissionCode,
	p.`name` AS permissionName,
	p.`description` AS permissionDescription,
	rp.`role_id` AS roleId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
WHERE rp.`role_id`=".$Role."
	AND p.`service_id`=".$Service."
	AND p.`permissions_type_id` IN (2, 3);");
	}

	public function GetRolePermissions($Role)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_GET_PERMISSIONS);

		return $this->Get("SELECT
	p.`permission_id` AS permissionId,
	p.`service_id` AS serviceId,
	p.`permissions_type_id` AS permissionTypeId,
	p.`code` AS permissionCode,
	p.`name` AS permissionName,
	p.`description` AS permissionDescription,
	rp.`role_id` AS roleId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
WHERE rp.`role_id`=".$Role.";");
	}

	public function GetBasicAccessForServices($Role)
	{
		return $this->Get("SELECT
	p.`permission_id` AS permissionId,
	p.`service_id` AS serviceId,
	p.`permissions_type_id` AS permissionTypeId,
	p.`code` AS permissionCode,
	p.`name` AS permissionName,
	p.`description` AS permissionDescription,
	rp.`role_id` AS roleId,
	s.`name` AS serviceName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services` AS s ON s.`service_id`=p.`service_id`
WHERE rp.`role_id`=".$Role."
	AND p.`service_id` NOT IN (
		SELECT
			p.`service_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
		LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
		WHERE rp.`role_id`=".$Role."
		AND p.`permissions_type_id`=2
	)
	AND p.`permissions_type_id`=2;");
	}

	/**
	 *
	 * Установить права доступа, привязанные к роли
	 * @param integer $Role - Идентификатор роли
	 * @param array $Permissions - Массив прав доступа
	 */
	public function SaveRolePermissions($Role, $Permissions)
	{
		if(!is_array($Permissions))
			$Permissions = array($Permissions);
		$A = array();
		foreach($Permissions as $v)
			$A[] = "(".$Role.", ".$v.")";
		if(sizeof($A))
			$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions`
	(`role_id`,
	`permission_id`)
VALUES ".implode(",", $A).";");
	}

	public function DeleteRolePermissionsForService($Role, $Service)
	{
		$this->Exec("DELETE rp FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
WHERE rp.`role_id`=".$Role."
	AND p.`service_id`=".$Service.";");
	}

	/**
	 *
	 * Получить список пользователей, к которым привязана данная роль
	 * @param integer $Role - Идентификатор роли
	 */
	public function GetUsersForRole($Role)
	{
		return $this->Get("SELECT
	u.`user_id` AS userId,
	u.`users_type_id` AS userTypeId,
	u.`partner_id` AS partnerId,
	u.`first_name` AS userFirstName,
	u.`middle_name` AS userMiddleName,
	u.`last_name` AS userLastName,
	u.`email` AS userEmail,
	u.`create` AS userCreateDate,
	CONCAT_WS('', u.`first_name`, ' ', u.`middle_name`, ' ', u.`last_name`) AS userName,
	ur.`role_id` AS roleId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_roles` AS ur
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users` AS u ON u.`user_id`=ur.`user_id`
WHERE ur.`role_id`=".$Role.";");
	}


	public function GetRolesTypes($Type)
	{
		return $this->Get("SELECT DISTINCT
	c.`roles_type_id` AS roleTypeId,
	c.`name` AS roleTypeName,
	c.`description` AS roleTypeDescription
FROM `references`.`users_types_access` AS a
LEFT JOIN `references`.`roles_types_users_types` AS b ON b.`users_type_id`=a.`access_users_type_id`
LEFT JOIN `references`.`roles_types` AS c ON c.`roles_type_id`=b.`roles_type_id`
WHERE a.`users_type_id`".$this->PrepareValue($Type).";");
	}

    public function GetRolesTypesUsersTypes($Type)
    {
        return $this->Get("SELECT DISTINCT
	b.`roles_type_id` AS roleTypeId,
	b.`users_type_id` AS userTypeId
FROM `references`.`users_types_access` AS a
LEFT JOIN `references`.`roles_types_users_types` AS b ON b.`users_type_id`=a.`access_users_type_id`
WHERE a.`users_type_id`".$this->PrepareValue($Type).";");
    }
}