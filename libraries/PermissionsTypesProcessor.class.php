<?php
class PermissionsTypesProcessor extends Processor
{
	/**
	 *
	 * @var PermissionsTypesProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var PermissionsTypesData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return PermissionsTypesProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new PermissionsTypesData();
	}


    public function GetPermissionsTypesList()
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PERMISSIONS_TYPES_READY);
        return $this->CData->GetPermissionsTypesList();
    }

    public function CreatePermissionsType($Name, $Description)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PERMISSIONS_TYPES_CREATE);
        return $this->CData->CreatePermissionsType($Name, $Description);
    }

    public function SavePermissionsType($PermissionType, $Name, $Description)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PERMISSIONS_TYPES_SAVE);
        return $this->CData->SavePermissionsType($PermissionType, $Name, $Description);
    }

    public function DeletePermissionsType($PermissionType)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PERMISSIONS_TYPES_DELETE);
        return $this->CData->DeletePermissionsType($PermissionType);
    }
}