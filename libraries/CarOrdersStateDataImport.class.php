<?php
class CarOrdersStateDataImport extends Data
{
    public function SaveState($Order, $Password, $State, $Dealer, $UserType, $Name, $Model)
    {
        $this->Exec("INSERT INTO `lexus_references`.`orders_cars_statuses`
	(`order_id`,
	`password_md5`,
	`status`,
	`partners_division_id`,
	`user_type`,
	`user_name`,
	`submodel_id`,
	`create`,
	`update`)
VALUES
	(".$Order.",
    ".$this->Esc($Password).",
    ".$State.",
    ".$Dealer.",
    ".$UserType.",
    ".$this->Esc($Name).",
    ".$Model.",
    NOW(),
    NOW())
ON DUPLICATE KEY UPDATE
    `status` = VALUES(`status`),
    `update` = VALUES(`update`);");
    }

    public function GetModel($Alias)
    {
        return $this->Count("SELECT
	`submodel_id` AS Cnt
FROM `lexus_references`.`submodels`
WHERE `alias`=".$this->Esc($Alias).";");
    }

    public function GetDealer($RCode)
    {
        return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `lexus_references`.`partners_divisions`
WHERE `rcode`=".$this->Esc($RCode).";");
    }
}