<?php
class RolesTypesProcessor extends Processor
{
	/**
	 *
	 * @var RolesTypesProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var RolesTypesData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return RolesTypesProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new RolesTypesData();
	}



    public function GetRolesTypes()
    {
        PermissionsProcessor::Init()->CheckAccess(array(PERMISSIONS_ROLES_READ, PERMISSIONS_ROLESTYPE_CREATE, PERMISSIONS_ROLESTYPE_EDIT, PERMISSIONS_ROLESTYPE_DELETE));
        return $this->CData->GetRolesTypes();
    }

    public function CreateRoleTypes()
    {

    }

    public function SaveRoleTypes()
    {

    }

    public function DeleteRoleTypes()
    {

    }

    public function GetRolesTypesUsersTypes()
    {

    }
}