<?php
define("EVENTS_DRIVER_HEADER_NAME",						"X-REQUESTED-SDCFAONJYWNHUWQ");
define("EVENTS_DRIVER_HEADER_VALUE",					"1g3Qn91-u_E10-t-r_58");
define("EVENTS_DRIVER_HEADER",							EVENTS_DRIVER_HEADER_NAME.":".EVENTS_DRIVER_HEADER_VALUE);
define("EVENTS_DRIVER_HOST",							"contacts.toyota.ru");
define("EVENTS_DRIVER_PATH",							"/z/__users-events_951-for-driver");


/**
 *
 * Категории событий
 */
/**
 * Внутренние события
 */
define("EVENTS_CATEGORY_INNER_SYSTEM",					1);
/**
 * События, требующие синхронизации клиентов. Не требуют просмотра и реакции пользователя.
 */
define("EVENTS_CATEGORY_SYNCHRONIZED",					2);
/**
 * События, уведомляющие пользователя об изменении в системе. Для закрытия требуют просмотра (команды "просмотрено").
 */
define("EVENTS_CATEGORY_NOTICE",						4);
/**
 * События, уведомляющие пользователя об изменении в системе. Для закрытия требуют просмотра (команды "просмотрено").
 */
define("EVENTS_CATEGORY_NOTICE_CONFIRMED",				8);
/**
 * События, уведомляющие пользователя об изменении в системе. Для закрытия требуют отображения пользователю (команды "доставлено").
 */
define("EVENTS_CATEGORY_NOTICE_DELIVERED",				16);




/**
 * Создано
 */
define("EVENTS_STATUS_CREATED",							1);
/**
 * Отправлено
 */
define("EVENTS_STATUS_SENDED",							2);
/**
 * Доставлено
 */
define("EVENTS_STATUS_DELIVERED",						3);
/**
 * Подтверждено
 */
define("EVENTS_STATUS_CONFIRMED",						4);
/**
 * Отработано
 */
define("EVENTS_STATUS_WORKED",							5);
/**
 * Закрыто
 */
define("EVENTS_STATUS_CLOSED",							6);
