<?php
class Strings extends BaseStatic
{
	public static function PrepareText2($Text, $DeleteHTML = null)
	{
		if($DeleteHTML)
		{
			$Find = array(
				//Убиваем всю херню ворда
				'/<!--\[if.*?if\]-->/uis',
				//Убиваем комментарии
				'/(<!--).*?(-->)/uis',
				//Убиваем скрипты
				'~<script[^>]*?>.*?</script>~si',
				//убиваем стили
				'~<style[^>]*>.*?<\/style>~uis',
				//Заменяем разрывы страниц, переводы каретки, табуляшки пробелами
				'/(\r\n|\r|\n|\t)/um',
				/*"'<[\/\!]*?[^<>]*?>'si",
				"/([\r\n])[\s]+/",
				"/&(quot|#34);/i",
				"/&(amp|#38);/i",
				"/&(lt|#60);/i",
				"/&(gt|#62);/i",
				"/&(nbsp|#160);/i",
				"/&(iexcl|#161);/i",
				"/&(cent|#162);/i",
				"/&(pound|#163);/i",
				"/&(copy|#169);/i",
				"/&#(\d+);/e"*/
			);
			$Replace = array(
				"",
				"",
				"",
				"",
				"",
				/*"$1",
				"\"",
				"&",
				"<",
				">",
				" ",
				chr(161),
				chr(162),
				chr(163),
				chr(169),
				"chr(\\1)"*/
			);
			//$Text = preg_replace($Find, $Replace, $Text);

			$Text = strip_tags(preg_replace($Find, $Replace, $Text));
		}
		else
		{
			$Find = array(
			//1. Убиваем всю херню ворда
				'/<!--\[if.*?if\]-->/uis',
			//2. Убиваем комментарии
				'/(<!--).*?(-->)/uis',
			//3. Убиваем скрипты
				'~<script[^>]*?>.*?</script>~si',
			//4. убиваем стили
				'~<style[^>]*>.*?<\/style>~uis',
			//5. Очищаем атрибуты тегов, кроме тегов a, span, img, object, embed, iframe
				'~(<(?!(a\s|span|img|object|embed|param|iframe|div))\w+)[^>]*?>~xium',
			//6. Убиваем пустые теги P
				'~<p\s*>(?:\s|&nbsp;)*</p>~isu',
			//7. Убиваем в оставленных тегах вызов js
				//'/(<(?:a\s|span|img|object|embed|param|iframe|div)+\s+.*?)on\w+\s*=\s*("|\').*?\2(.*?\s*>)/usim',
			//8-0. Убиваем теги a без href
				//'~<a\s+(?:(?!href).+?)>(.*?)</a>~uis',
			//8. Очищаем тег a от всех атрибутов, кроме href и добавляем атрибут target="_blank" к чужим ссылкам
				'~<a\s+[^>]*?(href\s*=\s*(\'|")http[s]?://[^\2]*?\2).*?>~uis',
			//8-1. Очищаем тег a от всех атрибутов, кроме href собственные ссылки
				'~<a\s+[^>]*?(href\s*=\s*(\'|")(?!http[s]?://)[^\2]*?\2).*?>~uis',
			//9. Очищаем тег span
				'/<span\s+.*?(style\s*=\s*(\'|").*?\2).*?>/us',
			//10. Очищаем тег img
				'/(<img\s+).*?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\3).*?)?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\5).*?)?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\7).*?)?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\9).*?)?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\11).*?)?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\13).*?)?(?:(\s?(?:alt|src|title|style|width|height|class)\s*=\s*(\'|").*?\15).*?)?>/uis',
			//11. Убиваем ненужные атрибуты в теге DIV
				'~(<div\s+).*?(?:(\s?(?:emd|emt|class|style|contenteditable)\s*=\s*(\'|").*?\3).*?)?(?:(\s?(?:emd|emt|class|style|contenteditable)\s*=\s*(\'|").*?\5).*?)?(?:(\s?(?:emd|emt|class|style|contenteditable)\s*=\s*(\'|").*?\7).*?)?(?:(\s?(?:emd|emt|class|style|contenteditable)\s*=\s*(\'|").*?\9).*?)?(?:(\s?(?:emd|emt|class|style|contenteditable)\s*=\s*(\'|").*?\11).*?)?>~us',
			//12. Заменяем разрывы страниц, переводы каретки, табуляшки пробелами
				'/(\r\n|\r|\n|\t)/um',
			//13. Заменяем двойные пробелы одинарными
				'/\s{2,}/',
			//14. Сносим управляющие символы
				//"/([\x00-\x1f])/um",
				"/©/us",
				"/®/us",
				"/«/us",
				"/»/us",
				"/–/us",
				"/—/us",
				"/§/us",
				"/°/us",
				"/™/us",
			/*
			//Убиваем пустые теги
				"'<[\s]*[\w]+[\s]*>[^<]{0}</[\s]*[\w]+\s*>'iu",
			//Теги содержищие только пробельные символы заменяем на пробел
				"'<[\s]*[\w]+[\s]*>[\s]</[\s]*[\w]+\s*>'iu",
			//Заменяем теги <b> и <i> и <u> соответствующими span'ами
				"'<[\s]*u[\s]*>[\s]*<[\s]*i[\s]*>[\s]*<[\s]*b[\s]*>([^>]+)</[\s]*b[\s]*>[\s]*</[\s]*i[\s]*>[\s]*</[\s]*u[\s]*>'iu",
				"'<[\s]*u[\s]*>[\s]*<[\s]*b[\s]*>[\s]*<[\s]*i[\s]*>([^>]+)</[\s]*i[\s]*>[\s]*</[\s]*b[\s]*>[\s]*</[\s]*u[\s]*>'iu",
				"'<[\s]*b[\s]*>[\s]*<[\s]*u[\s]*>[\s]*<[\s]*i[\s]*>([^>]+)</[\s]*i[\s]*>[\s]*</[\s]*u[\s]*>[\s]*</[\s]*b[\s]*>'iu",
				"'<[\s]*i[\s]*>[\s]*<[\s]*u[\s]*>[\s]*<[\s]*b[\s]*>([^>]+)</[\s]*b[\s]*>[\s]*</[\s]*u[\s]*>[\s]*</[\s]*i[\s]*>'iu",
				"'<[\s]*b[\s]*>[\s]*<[\s]*i[\s]*>[\s]*<[\s]*u[\s]*>([^>]+)</[\s]*u[\s]*>[\s]*</[\s]*i[\s]*>[\s]*</[\s]*b[\s]*>'iu",
				"'<[\s]*i[\s]*>[\s]*<[\s]*b[\s]*>[\s]*<[\s]*u[\s]*>([^>]+)</[\s]*u[\s]*>[\s]*</[\s]*b[\s]*>[\s]*</[\s]*i[\s]*>'iu",

			//Заменяем теги <b> и <i> соответствующими span'ами
				"'<[\s]*i[\s]*>[\s]*<[\s]*b[\s]*>([^>]+)</[\s]*b[\s]*>[\s]*</[\s]*i[\s]*>[\s]*'iu",
				"'<[\s]*b[\s]*>[\s]*<[\s]*i[\s]*>([^>]+)</[\s]*i[\s]*>[\s]*</[\s]*b[\s]*>[\s]*'iu",

				"'<[\s]*u[\s]*>([^>]+)</[\s]*u[\s]*>'iu",
				"'<[\s]*i[\s]*>([^>]+)</[\s]*i[\s]*>'iu",
				"'<[\s]*b[\s]*>([^>]+)</[\s]*b[\s]*>'iu",

			//Удаляем все обработчики событий
			//Заменяем несколько пробельных элементов на один
				"/ +/",
				"/@#@#@#@#@#/u",
				"/@=#/iu",
				"/#=@/iu"

			*/
			);

			$Replace = array(
			//1. Убиваем всю херню ворда
				"",
			//2. Убиваем комментарии
				"",
			//3. Убиваем скрипты
				"",
			//4. убиваем стили
				"",
			//5. Очищаем атрибуты тегов, кроме тегов a, span, img, object, embed, iframe
				"$1>",
			//6. Убиваем пустые теги P
				"",
			//7. Убиваем в оставленных тегах вызов js
				//"$1$3",
			//8-0. Убиваем теги a без href
				//'$1',
			//8. Очищаем тег a от всех атрибутов, кроме href и добавляем атрибут target="_blank" к чужим ссылкам
				"<a $1 target=\"_blank\" rel=\"nofollow\">",
			//8-1. Очищаем тег a от всех атрибутов, кроме href собственные ссылки
				"<a $1>",
			//9. Очищаем тег span
				"<span $1>",
			//10. Очищаем тег img
				"$1$2$4$6$8$10$12$14>",
			//11. Убиваем ненужные атрибуты в теге DIV
				"$1$2$4$6$8$10>",
			//12. Заменяем разрывы страниц, переводы каретки, табуляшки пробелами
				" ",
			//13. Заменяем двойные пробелы одинарными
				" ",
			//Сносим управляющие символы
				//"",
				"&copy;",
				"&reg;",
				"&laquo;",
				"&raquo;",
				"&#8211;",
				"&#8212;",
				"&sect;",
				"&deg;",
				"&#8482;"
			/*
				"<span style=\"font-style: italic; font-weight: bold; text-decoration: underline;\">$1</span>",
				"<span style=\"font-style: italic; font-weight: bold; text-decoration: underline;\">$1</span>",
				"<span style=\"font-style: italic; font-weight: bold; text-decoration: underline;\">$1</span>",
				"<span style=\"font-style: italic; font-weight: bold; text-decoration: underline;\">$1</span>",
				"<span style=\"font-style: italic; font-weight: bold; text-decoration: underline;\">$1</span>",
				"<span style=\"font-style: italic; font-weight: bold; text-decoration: underline;\">$1</span>",

				"<span style=\"font-style: italic; font-weight: bold;\">$1</span>",
				"<span style=\"font-style: italic; font-weight: bold;\">$1</span>",

				"<span style=\"text-decoration: underline;\">$1</span>",
				"<span style=\"font-style: italic;\">$1</span>",
				"<span style=\"font-weight: bold;\">$1</span>",

				"",
				" ",
				">",
				"<a",
				"<span",
				">"
				*/
			);
			$Text = preg_replace($Find, $Replace, $Text);
		}
		//убиваем управляющие символы
		//$Text = preg_replace("/([\x00-\x1f])/um", "", $Text);

		return trim($Text);
	}


	public static function PrepareText($Text)
	{
		if(self::ValidateText($Text))
		{
			$Text = html_entity_decode(strip_tags($Text));
			$Text=preg_replace(
						array("/\r+/ui",
							"/ +/ui",
							"/\n{2,}/"
						),
						array("",
							" ",
							"\n"), $Text);
		}
		else $Text = false;
		return $Text;
	}

    public static function PrepareFullText($Text)
    {
        return preg_replace('/[\x0-x19]/', " ", $Text);
    }

	public static function PrepareText0($Text)
	{
		if(self::ValidateText($Text))
		{
			$Text = strip_tags($Text);
/*
			$Text = preg_replace(
						array('/\r+/u',
							'/ +/u',
							'/\n{2,}/u'
						),
						array("",
							" ",
							"\n"), $Text);
*/
		}
		else $Text = false;
		return $Text;
	}

	static public function PrepareTextHTML($Text)
	{
		if(preg_match('`^[+=~\'":;.,-—–«»\`/\{\}\(\)\[\]№@$*|%!?#&\^<>\d\wа-яё\s\\\]*$`ui', $Text))
			$R = $Text;//self::PrepareText2($Text);
		else $R = false;
		return $R;
	}

	static public function ValidateText($Text)
	{
		if(preg_match('`^[+=~\'":;.,-—–«»\`\/\{\}\(\)\[\]№@$*|%!?#&\^<>\d\wа-яё\s\\\]*$`ui', $Text))
			$R = $Text;
		else $R = false;
		return $R;
	}

	static public function PrepareFindString($Text)
	{
		$Text = preg_replace(array(
								'/[^a-zа-яё]+/uim',
								'/%+/'),
							array('%', '%'), strip_tags(trim($Text)));
		if(!$Text) $Text = false;
		return $Text;
	}

	public static function PrepareTextB64($Text, $Full = null)
	{
		if(preg_match('~^[\d\w+= /]*$~ui', $Text))
		{
			$Text = base64_decode(preg_replace("/ /", "+", $Text));
			if(!$Full) $Text = self::PrepareText($Text);
		}
		else $Text = false;
		return $Text;
	}

    public static function PrepareTextB64O($Text)
	{
		if(preg_match('~^[\d\w+= /]*$~ui', $Text))
		{
			$Text = preg_replace("/ /", "+", $Text);
		}
		else $Text = false;
		return $Text;
	}

	public static function PrepareTextB64S($Text)
	{
		if(preg_match('~^[\d\w+= /]*$~ui', $Text))
		{
			$Text = base64_decode(preg_replace("/ /", "+", $Text));
			if(!preg_match('`^[+=~\'":;.,-—–«»\`/\{\}\(\)\[\]№@$*|%!?#&\^<>\d\wа-яё ]*$`ui', $Text))
				$Text = false;
		}
		else $Text = false;
		return $Text;
	}


	public static function PrepareTextB64E($Text)
	{
		if(preg_match('~^[\d\w+= /]*$~ui', $Text))
		{
			$Text = self::ValidateText($Text);
			if($Text !== false)
			{
				$Text = self::PrepareText2(base64_decode(preg_replace("/ /", "+", $Text)));
			}
		}
		else $Text = false;
		return $Text;
	}


	public static function PrepareTextB64Find($Text)
	{
		if(preg_match('~^[\d\w+= /]*$~ui', $Text))
		{
			$Text = self::ValidateText($Text);
			if($Text !== false)
			{
				$Text = base64_decode(preg_replace("/ /", "+", $Text));
				$Text = trim(preg_replace('/\s(\w{1,3})\s/', " ",
					preg_replace("/ +/", "  ",
						preg_replace('/[^\w\x7F-\xFF\s]/', " ",
							$Text))));
			}
		}
		else $Text = false;
		return $Text;
	}


	static public function ClearString($Text)
	{
		$Text = html_entity_decode(strip_tags($Text));
		$Text=preg_replace(
					array('/(\r|\n)+/ui',
						'/\s+/ui'
					),
					array("",
						" "), $Text);
	}

	static public function Left($String, $Count, $Encoding = null)
	{
		return $Encoding ? mb_substr($String, 0, $Count, $Encoding) : mb_substr($String, 0, $Count);
	}

	static public function Right($String, $Count, $Encoding = null)
	{
		return $Encoding ? mb_substr($String, mb_strlen($String, $Encoding) - $Count, $Count, $Encoding) : mb_substr($String, mb_strlen($String) - $Count, $Count);
	}

	static public function CutOne($String, $Start, $End, $Include = null, $Charset = null)
	{
		$R = "";
		if($String && is_string($String))
		{
			$s = mb_strpos($String, $Start, null, $Charset);
			if($s !== false)
			{
				$sl = mb_strlen($Start, $Charset);
				$e = mb_strpos($String, $End, $s + $sl, $Charset);
				if($e !== false)
				{
					$R = $Include ? mb_substr($String, $s + $sl, $e - $s - mb_strlen($End, $Charset), $Charset) : mb_substr($String, $s, $e - $s, $Charset);
				}
			}

		}
		return $R;
	}

	static public function PrepareUserPosition($Text)
	{
		$Text = trim($Text);
		if(!preg_match('/^[а-яё ,\-\w\d"]+$/iu', $Text))
		{
			$Text = false;
		}
		return $Text;
	}

	static public function ValidatePhoneOne($Text)
	{
		$Text = trim($Text);
		if(preg_match('/^(?:\+7|8)?\s*[-.\/\\,:]?(\d{10}|(?:(?:\(\s*\d{3}\s*\)|\d{3})\s*[-.\/\\,:]?\d{3}(?:\s*[-.\/\\,:]?\d{2}){2})|(?:(?:\(\s*\d{4}\s*\)|\d{4})(?:(?:\s*[-.\/\\,:]?\d{2}){3}|(?:\s*[-.\/\\,:]?\d{3}){2}))|(?:(?:\(\s*\d{5}\s*\)|\d{5})(?:\s*[-.\/\\,:]?\d{3}\s*[-.\/\\,:]?\d{2}|\s*[-.\/\\,:]?\d{1}(?:\s*[-.\/\\,:]?\d{2}){2}))|(?:(?:\(\s*\d{6}\s*\)|\d{6})(?:(?:\s*[-.\/\\,:]?\d{2}){2}|\s*[-.\/\\,:]?\d{1}\s*[-.\/\\,:]?\d{3}|\s*[-.\/\\,:]?\d{3}\s*[-.\/\\,:]?\d{1}))|(?:(?:\(\s*\d{7}\s*\)|\d{7})(?:\s*[-.\/\\,:]?\d{3}|\s*[-.\/\\,:]?\d{1}\s*[-.\/\\,:]?\d{2}|\s*[-.\/\\,:]?\d{2}\s*[-.\/\\,:]?\d{1})))$/u', $Text, $Z))
		{
			$Text = preg_replace('/[^\d]+/ui', "", $Z[1]);
		}
		else $Text = false;
		return $Text;
	}

	static public function ValidatePhoneOne2($Text)
	{
		$Text = trim($Text);
		if(!preg_match('/^[\d]{10}$/u', $Text))
		{
			$Text = false;
		}
		return $Text;
	}

	static public function ValidatePhoneAdvances($Text)
	{
		$Text = trim($Text);
		if(preg_match('/^(\d{1,5}|\d{1,3}-\d\d|\d-\d\d-\d\d|\d\d-\d{3})$/ui', $Text, $Z))
		{
			$Text = preg_replace('/[^\d]+/ui', "", $Z[1]);
		}
		else $Text = false;
		return $Text;
	}
	
	static public function ValidatePhoneForToyotaDealerTmp($Text)
	{
		$Text = trim($Text);
		if(!preg_match('/^(?:Тел\.|Факс|Тел.\/факс):?\s+<a href="tel:\+7\d{10}"(?:\s+style\s*=\s*"\s*text-decoration\s*:\s*none\s*;\s*color\s*:\s*black\s*;\s*")?\s*>(?:\+7|8)?\s?((?:(?:\(\d{3}\)\s|\d{3}-)\d{3}(?:-\d{2}){2})|(?:\(\d{4}\)\s\d{2}(?:-\d{2}){2})|(?:\(\d{5}\)\s\d(?:-\d{2}){2})|(?:\(\d{6}\)\s\d{2}-\d{2})|(?:\(\d{7}\)\s\d-\d{2}))<\/a>(?:,\s+<a href="tel:\+7\d{10}"(?:\s+style\s*=\s*"\s*text-decoration\s*:\s*none\s*;\s*color\s*:\s*black\s*;\s*")?\s*>(?:\+7|8)?\s?((?:(?:\(\d{3}\)\s|\d{3}-)\d{3}(?:-\d{2}){2})|(?:\(\d{4}\)\s\d{2}(?:-\d{2}){2})|(?:\(\d{5}\)\s\d(?:-\d{2}){2})|(?:\(\d{6}\)\s\d{2}-\d{2})|(?:\(\d{7}\)\s\d-\d{2}))<\/a>){0,2}(?:\s*(?:<br>|<br\s*\/>)\s*(?:Тел\.|Факс|Тел.\/факс):?\s+<a href="tel:\+7\d{10}"(?:\s+style\s*=\s*"\s*text-decoration\s*:\s*none\s*;\s*color\s*:\s*black\s*;\s*")?\s*>(?:\+7|8)?\s?((?:(?:\(\d{3}\)\s|\d{3}-)\d{3}(?:-\d{2}){2})|(?:\(\d{4}\)\s\d{2}(?:-\d{2}){2})|(?:\(\d{5}\)\s\d(?:-\d{2}){2})|(?:\(\d{6}\)\s\d{2}-\d{2})|(?:\(\d{7}\)\s\d-\d{2}))<\/a>(?:,\s+<a href="tel:\+7\d{10}"(?:\s+style\s*=\s*"\s*text-decoration\s*:\s*none\s*;\s*color\s*:\s*black\s*;\s*")?\s*>(?:\+7|8)?\s?((?:(?:\(\d{3}\)\s|\d{3}-)\d{3}(?:-\d{2}){2})|(?:\(\d{4}\)\s\d{2}(?:-\d{2}){2})|(?:\(\d{5}\)\s\d(?:-\d{2}){2})|(?:\(\d{6}\)\s\d{2}-\d{2})|(?:\(\d{7}\)\s\d-\d{2}))<\/a>){0,2}){0,2}$/iu', $Text))
		{
			$Text = false;
		}
		return $Text;
	}
	



	static public function ValidateEmailOne($Text)
	{
		$Text = mb_strtolower(trim($Text));
		if($Text == "toyota@noemail" || $Text == "lexus@noemail")
		{
			$R = $Text;
		}
		else
		{
			if(preg_match('~^[_a-z0-9-]+(?:\.[_a-z0-9\-]+)*[.]*@[_a-z0-9-]+(?:\.[_a-z0-9\-]+)+$~ui', $Text))
				$R = $Text;
			else $R = false;
		}
		return $R;
	}

	static public function PrepareEmailMulti($Text)
	{
		$Text = mb_strtolower(trim($Text));
		if(preg_match_all('~^[_a-z0-9-]+(?:\.[_a-z0-9\-]+)*@[_a-z0-9-]+(?:\.[_a-z0-9\-]+)+(?:\s*[,]\s*[_a-z0-9-]+(?:\.[_a-z0-9\-]+)*@[_a-z0-9-]+(?:\.[_a-z0-9\-]+)+)*$~', $Text, $Z))
		{
			$Text = implode(",", $Z[0]);
		}
		else $Text = false;
		return $Text;
	}


	static public function PrepareFirstAndMiddleName($Text)
	{
		//return $Text;
		$Text = mb_strtolower(trim($Text));
		if(preg_match('/^[а-яё]+(?:[ -][а-яё]+)*$/iu', $Text, $Z))
		{
			$Text = mb_strtoupper(mb_substr($Z[0], 0, 1)).mb_substr($Z[0], 1);
		}
		else $Text = false;
		return $Text;
	}

	static public function PrepareFirstAndMiddleNameWEn($Text)
	{
		$Text = mb_strtolower(trim($Text));
		if(preg_match('/^(?:[а-яё]+(?:[ -][а-яё]+)*|\w+(?:[ -]\w+)*)$/iu', $Text, $Z))
		{
			$Text = mb_strtoupper(mb_substr($Z[0], 0, 1)).mb_substr($Z[0], 1);
		}
		else $Text = false;
		return $Text;
	}

	static public function PrepareFirstAndMiddleAndLansName($Text)
	{
		$Text = mb_strtolower(trim($Text));
		if(preg_match('/^\s*[а-яё]+(?:[ \-][а-яё]+)*\s*$/ui', $Text, $Z))
		{
			$R = explode(" ", $Text);
			$Text = array();
			foreach ($R as $k => $v)
			{
				$v = explode("-", trim($v));
				$TextA = array();
				foreach ($v as $v0)
				{
					$TextA[] = mb_strtoupper(mb_substr($v0, 0, 1)).mb_substr($v0, 1);
				}
				$Text[] = implode("-", $TextA);
			}
			$Text = implode(" ", $Text);
		}
		else $Text = false;
		return $Text;
	}

	static public function PrepareFirstAndMiddleAndLansNameWEn($Text)
	{
		//return $Text;
		$Text = mb_strtolower(trim($Text));
		if(!preg_match('/^\s*(?:([а-яё]+(-[а-яё])*)+\s+([а-яё]+(-[а-яё])*)+(\s+[а-яё]+(-[а-яё])*)*|(\w+(-\w)*)+\s+(\w+(-\w)*)+(\s+\w+(-\w)*)*)\s*$/ui', $Text, $Z))
			$Text = false;
		return $Text;
	}

	static public function PrepareLastName($Text)
	{
		$Text = mb_strtolower(trim($Text));
		if(preg_match('/^[а-яё]+(?:\s*-\s*[а-яё]+)*$/iu', $Text, $Z))
		{
			$R = explode("-", $Text);
			$Text = array();
			foreach ($R as $k => $v)
			{
				$Text[] = mb_strtoupper(mb_substr($v, 0, 1)).mb_substr($v, 1);
			}
			$Text = implode("-", $Text);
		}
		else $Text = false;
		return $Text;
	}

	static public function PrepareLastNameWEn($Text)
	{
		$Text = mb_strtolower(trim($Text));

		if(preg_match('/^(?:[а-яё]+(?:[ \-][а-яё]+)*|\w+(?:[ \-]\w+)*)$/iu', $Text, $Z))
		{
			$R = explode("-", $Text);
			$Text = array();
			foreach ($R as $k => $v)
			{
				$Text[] = mb_strtoupper(mb_substr($v, 0, 1)).mb_substr($v, 1);
			}
			$Text = implode("-", $Text);
		}
		else $Text = false;
		return $Text;
	}

	static public function PrepareURLPath($Text)
	{
		$Text = trim($Text);
		return (preg_match('/^[\/a-z0-9\-._~ ?=&%+#]+$/ui', $Text)) ? $Text : false;
	}

	static public function ValidateIP($Text)
	{
		$Text = trim($Text);
		return (preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/u', $Text)) ? $Text : false;
	}

	static public function TranclitRE($Text)
	{
    	return strtr($Text, array(
	        'а' => 'a',   'б' => 'b',   'в' => 'v',
	        'г' => 'g',   'д' => 'd',   'е' => 'e',
	        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
	        'и' => 'i',   'й' => 'y',   'к' => 'k',
	        'л' => 'l',   'м' => 'm',   'н' => 'n',
	        'о' => 'o',   'п' => 'p',   'р' => 'r',
	        'с' => 's',   'т' => 't',   'у' => 'u',
	        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
	        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
	        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
	        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

	        'А' => 'A',   'Б' => 'B',   'В' => 'V',
	        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
	        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
	        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
	        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
	        'О' => 'O',   'П' => 'P',   'Р' => 'R',
	        'С' => 'S',   'Т' => 'T',   'У' => 'U',
	        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
	        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
	        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
	        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
	    ));
	}

	static public function ValidateVIN($Text)
	{
		$Text = mb_strtoupper(trim($Text));
		return preg_match('/[\dABCDEFGHJKLMNPRSTUVWXYZ]{17}/', $Text) ? $Text : false;
	}

	static public function ValidatePromoCode($Text)
	{
		$Text = trim($Text);
		return preg_match('/^(?:A|Z)[BCDEFGHKMNPRSTUVWYA]{4}-[BCDEFGHKMNPRSTUVWYA]{5}$/u', $Text) ? $Text : false;
	}

    static public function ValidateAutoRegNumber($Text)
    {
        return preg_match('/^((?:(?:[авекмнорстух]\s*\d{3}\s*[авекмнорстух]{2}|[авекмнорстух]{2}\s*\d{3}\s*[авекмнорстух]|Т[авекмнорстух]{2}\s*\d{3}|\d{4}\s*[авекмнорстух]{2}|[авекмнорстух]{2}\s*\d{4}|[авекмнорстух]{2}\s*\d{3}|\d{3}\s*CD\s*\d|\d{3}\s*[dt]\s*\d{3})(?:(?:\s+регион)?\s*)\d{2,3})|[авекмнорстух]\s*\d{3}\s*[авекмнорстух]{2}|[a-z\d\s\- ]{3,15})$/ui', $Text)
                    ? mb_strtoupper($Text) : false;
    }
	
	static public function ValidateAutoRegNumberSimply($Text)
    {
        return preg_match('/^[\d\w\- а-я]{5,15}$/ui', $Text)
                    ? mb_strtoupper($Text) : false;
    }

	static public function ValidateURL($Text)
	{
		return preg_match('/^(?:([a-z]+):(?:([a-z]*):)?\/\/)?(?:([^:@]*)(?::([^:@]*))?@)?((?:[a-z0-9_-]+\.)+[a-z]{2,}|localhost|(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])\.){3}(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])))(?::(\d+))?(?:([^:\?\#]+))?(?:\?([^\#]+))?(?:\#([^\s]+))?$/ui', $Text)
                    ? mb_strtolower($Text) : false;
	}
	
	static function ValidateRCode($Text)
	{
		$Text = trim($Text);
		return preg_match('/^\w\d{3}l?$/ui', $Text)
                    ? mb_strtoupper($Text) : false;
	}


    static public function ValidateLoginAndPassword($Text)
    {
        $Text = trim($Text);
        return preg_match('/^[а-яё\w\d\-=+@#$%\^&\{\}\(\)\|\\!.,:\[\]~\>\<\/№\*]+$/iu', $Text) ? $Text : false;
    }
	
	static public function ValidateGAAccount($Text)
	{
		$Text = trim($Text);
		return preg_match('/^ua-\d{7,8}-\d{1,3}$/ui', $Text)
                    ? mb_strtoupper($Text) : false;
	}
}
