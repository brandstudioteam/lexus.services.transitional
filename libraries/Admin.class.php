<?php
class Admin extends Controller
{
	/**
	 *
	 * @var Admin
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return Admin
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}




	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"brandId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "serviceId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "actionId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "template"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
            )
		);
		$this->Modes = array(
            //Возвращает список сервисов и акций, доступных данному пользователю
			"c"		=> array(
				"exec"		=> array("AdminProcessor", "GetServicesDataForAdmin")
			),
            //Возвращает настройки акции сервиса
			"d2"		=> array(
				"exec"		=> array("AdminProcessor", "GetServiceSets"),
                "Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("serviceId" => 2, "actionId" => 0)
			),
            //Возвращает настройки акции сервиса
			"e2"		=> array(
				"exec"		=> array("AdminProcessor", "GetServiceTemplate"),
                "Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("template" => 2)
			),
			"f"		=> array(
				"exec"		=> array("AdminProcessor", "GetSession")
			),
        );
	}
}