<?php
class CarOrdersStateImport extends Controller
{
	/**
	 *
	 * @var CarOrdersStateImport
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return CarOrdersStateImport
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"orderId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"name"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT)
				)
			)
		);
		$this->Modes = array(
            "b"		=> array(
				"exec"		=> array("CarOrdersStateProcessorImport", "Import")
			)
		);
	}
}