<?php
class UsersSubscriptions extends Controller
{
	/**
	 *
	 * @var UsersSubscriptions
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return UsersSubscriptions
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
                /*
				"lastName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
				"firstName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"gender"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
				),
				"email"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"age"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"captcha"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"verifier"	=> array("Captcha", "CheckCode")
				),
                 */
                "dealers"   => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "model"   => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "subscriptionThemeId"   => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "memberId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
                "memberEmail"                 => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
                "subscriptionThemeId"   => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "memberLastName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
                "memberFirstName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
                "memberMiddleName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
                "memberName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
                "memberSubscribeStop"			=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "memberGender"			=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "memberAge"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "memberMaritalStatus"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES,
                                        array(	"Замужем / Женат",
                                                "Не замужем / Холост")),
				),
                "memberCompanyType"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES,
                                        array(	"Частная российская",
                                                "Частная иностранная",
                                                "Государственная российская")),
				),
                "memberOccupation"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES,
                                        array(	"Студент",
                                                "Аспирант",
                                                "Частный предприниматель",
                                                "Домохозяйка",
                                                "Безработный",
                                                "Руководитель предприятия",
                                                "Менеджер",
                                                "Специалист",
                                                "Государственный служащий")),
				),
                "memberCarType"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES,
                                        array(	"Иностранного производства",
                                                "Российского производства",
                                                "Отсутствует",
                                                "Планирую покупать")),
				),
                "em"                 => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
                "e"                 => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
                "salt"              => array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_CHARANDDIGITAL),
				),
                "memberCode"              => array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_CHARANDDIGITAL),
				),
				"memberName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FIO),
				),


			)
		);
		$this->Modes = array(
			//Подписка
			"a"		=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "SubscribeNew"),
				"TplVars"	=> array("memberLastName" => 2, "memberFirstName" => 2, "memberMiddleName" => 0, "memberGender" => 2, "memberEmail" => 2,
                                        "memberAge" => 0, "memberMaritalStatus" => 0, "memberCompanyType" => 0, "memberOccupation" => 0, "memberCarType" => 0,
                                        "dealers" => 0, "subscriptionThemeId" => 2, "model" => 0, "memberSubscribeStop" => 0)
			),
			//Отписка
			"b"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "UnSubscribe"),
				"TplVars"		=> array("memberCode" => 2, "e" => 2)
			),
            //Возвращает список тем подписки
			"c"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "GetSubscribeTheme")
			),
            //Аутенификация в кабинете подписки
			"d"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "MemberAuth"),
				"TplVars"		=> array("memberEmail" => 2, "memberCode" => 2),
                "Results"		=> array(
					"exceptions"		=>  array(1,
						array(
							5 => "ErrorLoginOrPassword",
						)
					),
				)
			),
            //Получение личных данных для кабинета подписки
			"e"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "GetPersonalData"),
				"TplVars"		=> array()
			),
            //Сохранение личных данных из кабинета подписки
			"f"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "SavePersonalData"),
                                    //$LastName, $FirstName, $MiddleName, $Gender, $Email, $Age, $MaritalStatus, $CompanyType, $Occupation, $CarType,
                                    //$Dealer = null, $Themes = null, $Model = null, $Stop = null
				"TplVars"		=> array("memberLastName" => 0, "memberFirstName" => 0, "memberMiddleName" => 0, "memberGender" => 0, "memberEmail" => 0,
                                        "memberAge" => 0, "memberMaritalStatus" => 0, "memberCompanyType" => 0, "memberOccupation" => 0, "memberCarType" => 0,
                                        "dealers" => 0, "subscriptionThemeId" => 0, "model" => 0, "memberSubscribeStop" => 0)
			),


            //Страница администрирования
			"g"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "GetSubscribeTheme"),
				"TplVars"		=> array()
			),

            "h"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "CheckAuth"),
			),
            "i"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "MemberExit"),
			),
            "j"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "RestorPassword"),
                "TplVars"		=> array("memberEmail" => 2),
                "Results"		=> array(
					"exceptions"		=>  array(1,
						array(
							5 => "EmailIsNotFound",
						)
					),
				)
			),


            //Возвращает список тем подписки
			"z"	=> array(
				"exec"		=> array("UsersSubscriptionsProcessor", "Test")
			),
		);
	}
}