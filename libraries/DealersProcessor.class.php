<?php
if(!defined("PARTNERS_SERVICE_ID"))
	define("PARTNERS_SERVICE_ID",												4);



/**
 *
 * Изменения привязки возможностей дилеров к дилерам
 * @var integer
 * 128
 */
define("PERMISSIONS_PARTNERS_FACILITIES_RELATED",								47);

class DealersProcessor extends Processor
{
	/**
	 *
	 * @var DealersProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var DealersData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return DealersProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new DealersData();
	}

	/**
	 *
	 * Возвращает название дилера (партнера типа 1 или 6)
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetDealerName($Dealer)
	{
		return $this->CData->GetDealerName($Dealer);
	}

	/**
	 *
	 * Возвращает RCode дилера (партнера типа 1 или 6)
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetDealerRCode($Dealer)
	{
		return $this->CData->GetDealerRCode($Dealer);
	}

	/**
	 *
	 * Возвращает основную информацию об одном или нескольких дилерах
	 * @param mixed $Dealer - Идентификатор дилера или массив идентификаторов дилеров или строка идентификаторов дилеров, разделенных запятыми
	 */
	public function GetDealers($Dealer)
	{
		return $this->CData->GetDealers($Dealer);
	}

    public function GetDealerData($Dealer)
	{
		return $this->CData->GetDealerData($Dealer);
	}

	public function GetDealersListFull($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null, $FacilityStatus = null)
	{
                                                //($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null, $FacilityStatus = null)
		return $this->CData->GetDealersListFull($Only, $Exclude, $City, $Find, $Facility, $Dealer, $Status, $FacilityStatus);
	}

	public function GetDealersListFullNew($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null, $FacilityStatus = null)
	{
                                                //($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null, $FacilityStatus = null)
		return $this->CData->GetDealersListFullNew($Only, $Exclude, $City, $Find, $Facility, $Dealer, $Status, $FacilityStatus);
	}

	public function GetDealersListWithGeoFull($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null)
	{
		return $this->CData->GetDealersListWithGeoFull($Only, $Exclude, $City, $Find, $Facility, $Dealer, $Status);
	}





	public function GetCitiesForPartners($Model, $PartnerType = null)
	{
		return $this->CData->GetCitiesForPartners($Model, $PartnerType);
	}






	public function GetCitiesForDealers($Only = null, $Exclude = null, $Status = null)
	{
		return $this->CData->GetCitiesForPartners(array(PARTNERS_TYPE_DEALER, PARTNERS_TYPE_AUTHORIZED), $Only, $Exclude, $Status);
	}

	public function GetPartnersForCity($City, $Status = null)
	{
		return $this->CData->GetPartnersForCity($City, $Status);
	}

	public function GetDealersForCity($City, $Status = null)
	{
		return $this->CData->GetPartnersForCity($City, $Status);
	}

	public function GetNearestDealers($Count = null, $CityCount = null, $Facility = null, $Latitude = null, $Longitude = null)
	{
		return GeoProcessor::Init()->GetNearestDealers($Count, $CityCount, null, $Facility, $Latitude, $Longitude);
	}

								//"partnerTypeId" => 0, "cityId" => 0, "regionId" => 0, "federalDistrictId" => 0, "countryId" => 0, "facilityId" => 0,
								//"gLt" => 0, "gLg" => 0, "count" => 0, "citiesCount" => 0, "actionId" => 0, "modelId" => 0, "promoId" => 0, "ref" => 0, "refi" => 0, "facilityAliasPool" =>
	public function FindDealers($DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $Status = null, $FacilityAlias = null)
	{
		return $this->CData->FindDealers($DealerType, $City, $Region, $FederalDistrict, $Country, $Facility,
										$Latitude, $Longitude, $Count, $CityCount, $Action, $Model, $Promo,
										$Ref, $RefFrameId, $FacilityAlias, $Status);
	}

    							//"partnerTypeId" => 0, "cityId" => 0, "regionId" => 0, "federalDistrictId" => 0, "countryId" => 0, "facilityId" => 0,
								//"gLt" => 0, "gLg" => 0, "count" => 0, "citiesCount" => 0, "actionId" => 0, "modelId" => 0, "promoId" => 0, "ref" => 0, "refi" => 0, "facilityAliasPool" =>
	public function FindDealers2($DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $Status = null, $FacilityAlias = null)
	{
		return $this->CData->FindDealers2(2, $DealerType, $City, $Region, $FederalDistrict, $Country, $Facility,
										$Latitude, $Longitude, $Count, $CityCount, $Action, $Model, $Promo,
										$Ref, $RefFrameId, $FacilityAlias, $Status);
	}


								/*
								 * "partnerTypeId" => 0, "cityId" => 0, "regionId" => 0, "federalDistrictId" => 0, "countryId" => 0, "facilityId" => 0,
										"gLt" => 0, "gLg" => 0, "count" => 0, "citiesCount" => 0, "actionId" => 0, "modelId" => 0, "promoId" => 0,
										"ref" => 0, "refi" => 0, "facilityAliasPool" => 0
								 */
	public function FindDealersWN($DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $Status = null, $FacilityAlias = null)
	{
		$R = GeoProcessor::Init()->GetNearestDealersCity(1, null, $Latitude, $Longitude, $Facility, $FacilityAlias, $Action, $Model);
		$R = array(
			"list"		=> $this->CData->FindDealers($DealerType, $City, $Region, $FederalDistrict, $Country, $Facility,
													$Latitude, $Longitude, null, null, $Action, $Model, null,
													$Ref, $RefFrameId, $FacilityAlias, $Status),
													/*
													 * $DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $FacilityAlias = null
													 */

			"defCity"	=> $R[0]
		);
		if($Promo)
			$R["dealersModels"] = array();// $this->CData->GetDealersModelsForPromo($Promo);
		return $R;
	}


	public function GetRelatedFacilities($Dealer = null)
	{
		return $this->CData->GetRelatedFacilities($Dealer);
	}

	public function GetRelatedFacilitiesNT()
	{
		return $this->CData->GetRelatedFacilitiesNT(WS::Init()->GetBrandId());
	}

    public function GetRelatedFacilities2()
	{
		return $this->CData->GetRelatedFacilities(null, 2);
	}

	public function GetRelatedFacilitiesFull($Dealer = null)
	{
		return $this->CData->GetRelatedFacilitiesFull($Dealer);
	}

	public function GetNotRelatedFacilities($Dealer)
	{
		return $this->CData->GetNotRelatedFacilities($Dealer);
	}

	public function GetNotRelatedFacilitiesFull($Dealer)
	{
		return $this->CData->GetNotRelatedFacilitiesFull($Dealer);
	}

	public function GetDealersTypes()
	{
		return $this->CData->GetDealersTypes();
	}



	public function GetCityFromDealers($Dealers)
	{
		return $this->CData->GetCityFromDealers($Dealers);
	}


	public function GetEmailsForFacilities($Dealers, $Facilities)
	{
		return $this->CData->GetEmailsForFacilities($Dealers, $Facilities);
	}

	public function GetEmailsForFacilitiesNew($Dealers, $Facilities)
	{
		return $this->CData->GetEmailsForFacilitiesNew($Dealers, $Facilities);
	}

    public function GetDealerPhone($Dealer)
    {
        return $this->CData->GetDealerPhone($Dealer);
    }


	public function GetDealersNew($Status = null, $Dealer = null)
	{
		return $this->CData->GetDealersNew(WS::Init()->GetBrandId(), $Status, $Dealer);
	}

	public function GetOldId($Model)
	{
		return $this->CData->GetOldId($Model);
	}

	public function GetDealersNewForImport()
	{
		return $this->CData->GetDealersNewForImport(WS::Init()->GetBrandId());
	}
}