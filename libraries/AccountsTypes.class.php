<?php
class AccountsTypes extends Controller
{
	/**
	 *
	 * @var AccountsTypes
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return AccountsTypes
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"userTypeId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"userTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"userTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "roleTypeId"            => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "operation"            => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "links"            => array(
                    "TplVars" => array("operation" => 0, "roleTypeId" => 1, "userTypeId" => 1)
				),
			)
		);
		$this->Modes = array(
			//Возвращает
			"a"	=> array(
				"exec"		=> array("AccountsTypesProcessor", "GetAccountsTypes")
			),
            //Создает
			"b"	=> array(
				"exec"		=> array("AccountsTypesProcessor", "CreateAccountType"),
				"TplVars"		=> array("userTypeName" => 1, "userTypeDescription" => 0)
			),
             //Сохраняет
			"c"	=> array(
				"exec"		=> array("AccountsTypesProcessor", "SaveAccountType"),
				"TplVars"		=> array("userTypeId" => 1, "userTypeName" => 0, "userTypeDescription" => 0)
			),
			//Удаляет
			"d"	=> array(
				"exec"		=> array("AccountsTypesProcessor", "DeleteAccountType"),
                "TplVars"		=> array("userTypeId" => 1)
			),
            //Удаляет
			"e"	=> array(
				"exec"		=> array("AccountsTypesProcessor", "LinkAccountsTypesWithRolesTypes"),
                "TplVars"		=> array("userTypeId" => 1)
			),
            //Удаляет
			"f"	=> array(
				"exec"		=> array("AccountsTypesProcessor", "GetAccountsTypesPartnersTypes")
			)
		);
	}
}