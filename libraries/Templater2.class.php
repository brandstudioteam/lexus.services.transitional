<?php
error_reporting(E_ALL);

class Templater2 extends Processor
{
	/**
	 * 
	 * @var Templater2
	 */
	protected static $Inst;
	
	protected $Template;
	
	protected $Data;
	
	protected $CurrBlocks;
	
	
	
	protected $CurrBlockKey;
	
	protected $CurrReplacedData;
	
	protected $CurrCount;
	
	
	protected $Itterater = 0;
	
	
	protected $Constructions;
	
	protected $Collections = array();
	/**
	 * Значение по умолчанию указателя начала блока инструкций
	 * @var string
	 */
	protected $InstrBlockSepStart= "<!--#";
	/**
	 * Значение по умолчанию указателя окончания блока инструкций
	 * @var string
	 */
	protected $InstrBlockSepEnd = "#-->";
	/**
	 * Значение по умолчанию указателя начала блока переменной
	 * @var string
	 */
	protected $VarBlockSepStart = "{%";
	/**
	 * Значение по умолчанию указателя окончания блока переменной
	 * @var string
	 */
	protected $VarBlockSepEnd = "%}";
	/**
	 * Значение по умолчанию разделитель переменных
	 * @var string
	 */
	protected $VarSep = ":";
	/**
	 * Значение по умолчанию разделитель свойств
	 * @var string
	 */
	protected $PropsSep = ":";
	/**
	 * Значение по умолчанию указателя начала инструкции
	 * @var string
	 */
	protected $InstrSepStart = "<!--@";
	/**
	 * Значение по умолчанию указателя окончания инструкции
	 * @var string
	 */
	protected $InstrSepEnd = "@-->";
	/**
	 * Значение по умолчанию разделитель имен блоков инструкций и инструкций
	 * @var string
	 */
	protected $BlockNameSep = "::";
	
	protected $DebugMode;
	
	/**
	 * 
	 * Инициализирует класс
	 * @return Templater2
	 */
    public static function Init($Template = null)
    {
		if(!self::$Inst) self::$Inst = new self($Template);
		return self::$Inst;
    }
    
    protected function __construct($Template = null)
    {
    	parent::__construct();
    	$this->Sets();
    	if($Template) $this->SetTemplate($Template);
    }
	
    /**
     * 
     * Enter description here ...
     */
    protected function Sets()
    {
    	$this->Constructions = array(
    		"item"			=> array("Templater2", "Item"),
    		"selectoption"	=> array("Templater2", "SelectOption"),
    		"condition"		=> array("Templater2", "ConditionView"),
    		"include"		=> array("Templater2", "DoInclude")
    	);
    }
    
    
    
    
    public function SetTemplate($Template)
    {
    	$this->Template = $Template;
    }
    
	public function Merge($Template = null, $Data = null, $Message = null, $Errors = null, $TemplateString = null)
	{
//$this->Dump(__METHOD__."   LINE = ". __LINE__, $Data);
		if($TemplateString)
		{
			$Template = &$TemplateString;
		}
		else
		{	
			if(!$Template) $Template = $this->Template;
			$Template = file_get_contents($Template);
		}
     	if(!$Data)
     		$Data = array();
     	elseif(!is_array($Data))
     		$Data = array($Data);
     	if($Message) $Data["SysMessage"] = $Message;
     	if($Errors) $Data["Errors"] = $Errors;
     	$this->Data = $Data;
     	$Template = preg_replace_callback('/templater_info\s*::\s*start(.*?)templater_info\s*::\s*end/uims', array("Templater2", "ConfigTemplater"), $Template);
     	$Template = $this->PrepareTemplate($Template);
     	$Template = $this->ReplaceVariables($Template, $Data);
     	$Template = preg_replace('/(?:'.preg_quote($this->VarBlockSepStart).'){1}[^('.preg_quote($this->VarBlockSepEnd).')]+(?:'.preg_quote($this->VarBlockSepEnd).'){1}/sui', "", $Template);
     	$Template = preg_replace('/'.preg_quote($this->InstrBlockSepStart).'\s*([\w\d\-_]+)\s*'.preg_quote($this->PropsSep).'\s*(.+?)'.preg_quote($this->InstrBlockSepEnd).'(.*?)'.preg_quote($this->InstrBlockSepStart).'\s*\1\s*'.preg_quote($this->InstrBlockSepEnd).'/uims', "", $Template);
     	return $Template;
	}
    
	public static function ArrayToPath(&$Array, $Prefix = null, $Postfix = null, $AsArray = null)
	{
		return self::ArrayToPathEx($Array, $Prefix, $Postfix, $AsArray);
	}
	
	protected static function ArrayToPathEx($Values, &$Prefix = null, &$Postfix = null, $AsArray = null, $Key = null, &$Path = null)
	{
		if(!is_array($Path))
		{
			if($AsArray)
			{
				$Path = array();
			}
			else
			{
				$Path = array(
					"keys"		=> array(),
					"values"	=> array()
				);
			}
		}
try {
		if($Values)
		{
			if($AsArray)
			{
				foreach ($Values as $k => $v)
				{
					if(is_array($v))
						self::ArrayToPathEx($v, $Prefix, $Postfix, $AsArray, ($Key ? $Key.".".$k : $k), $Path);
					else
						$Path[($Prefix ? $Prefix : "").($Key ? $Key.".".$k : $k).($Postfix ? $Postfix : "")] = $v;
				}
			}
			else
			{
				foreach ($Values as $k => $v)
				{
					if(is_array($v))
						self::ArrayToPathEx($v, $Prefix, $Postfix, $AsArray, ($Key ? $Key.".".$k : $k), $Path);
					else
					{
						$Path["keys"][] = ($Prefix ? $Prefix : "").($Key ? $Key.".".$k : $k).($Postfix ? $Postfix : "");
						$Path["values"][] = $v;
					}
				}
			}
		}			
}
catch(dmtException $e)
{
	GetDebug()->Dump($Values, $Key, $Path);
}
		return $Path;
	}
	
	
	protected function ConfigTemplater($Text)
	{
		$Sets = array(
			"INSTRUCTION_BLOCK_SEPARATOR"			=> array("InstrBlockSepStart", "InstrBlockSepEnd"),
			"INSTRUCTION_BLOCK_SEPARATOR_START"		=> "InstrBlockSepStart",
			"INSTRUCTION_BLOCK_SEPARATOR_END"		=> "InstrBlockSepEnd",
			"PROPERTY_SEPARATOR"					=> "PropsSep",
			"VARIABLE_BLOCK_SEPARATOR_START"		=> "VarBlockSepStart",
			"VARIABLE_BLOCK_SEPARATOR_END"			=> "VarBlockSepEnd",
			"VARIABLE_BLOCK_SEPARATOR"				=> array("VarBlockSepStart", "VarBlockSepEnd"),
			"VARIABLE_SEPARATOR"					=> "VarSep",
			"INSTRUCTION_SEPARATOR_START"			=> "InstrSepStart",
			"INSTRUCTION_SEPARATOR_END"				=> "InstrSepEnd",
			"INSTRUCTION_SEPARATOR"					=> array("InstrSepStart", "InstrSepEnd"),
			"BLOCK_NAME_SEPARATOR"					=> "BlockNameSep",
			"DEBUG_MODE"							=> "DebugMode"									
		);
		if(!$Text) return "";
		$Text = explode("\n", trim($Text[1]));
		foreach ($Text as $t)
		{
			$t = trim($t);
			if(!$t) continue;
			if(preg_match('`^(//|-- )+`uims', $t))
				continue;
			$t = explode("=", $t, 2);
			$t[0] = mb_strtoupper(trim($t[0]));
			$t[1] = trim($t[1]);
			if(array_key_exists($t[0], $Sets) && $t[1])
			{
				$Vars = $Sets[$t[0]];
				if(!is_array($Vars))
					$this->{$Vars} = $t[1];
				else foreach ($Vars as $v)
					$this->{$v} = $t[1];
			}
		}
		if($this->DebugMode)
		{
			$v = mb_strtolower($this->DebugMode);
			$this->DebugMode = ($v == "false" || $v == "null" || $v == "off" || $v == "no" || $v = "not") ? false : true;
		}
		return "";
	}
	
	
	protected function PrepareTemplate($Template)
	{
		$R = preg_replace_callback('/'.preg_quote($this->InstrBlockSepStart).'\s*([\w\d\-_]+)\s*'.preg_quote($this->PropsSep).'\s*(.+?)'.preg_quote($this->InstrBlockSepEnd).'(.*?)'.preg_quote($this->InstrBlockSepStart).'\s*\1\s*'.preg_quote($this->InstrBlockSepEnd).'/uims', array("Templater2", "DoTemplate"), $Template);
		return $R;
	}
	
	protected function DoTemplate($Text)
	{
		$R = array();
		if($Text[2])
		{
			$Info = explode($this->PropsSep, $Text[2]);
			if(!$Info[0]) array_shift($Info);
			$Keys = array();
			foreach ($Info as $v)
			{
				$v = explode("=", trim($v), 2);
				$v[0] = trim($v[0]);
				if(sizeof($v) == 2)
				{
					$v[1] = trim($v[1]);
					$Keys[mb_strtolower($v[0])] = isset($v[1]) ? $v[1] : null;
				}
				else
				{
					if(mb_strtolower(mb_substr($v[0], 0, 2)) == "if")
						$Keys["if"] = mb_substr($v[0], 3, -1);
					else $Keys[mb_strtolower(trim($v[0]))] = null;
				}
			}
			if(array_key_exists("type", $Keys) && array_key_exists(mb_strtolower($Keys["type"]), $this->Constructions))
			{
				$Type = $this->Constructions[mb_strtolower($Keys["type"])];
				unset($Keys["type"]);
				$R[] = call_user_func($Type, $Text[3], $Keys, $Text[1]);
			}
		}
		else throw new dmtException("Tmplate error in block ".$v[1]);
		return implode("", $R);
	}
	
	
	protected function ReplaceVariables($Template, &$Data)
	{
		$this->CurrReplacedData = self::ArrayToPath($Data, $this->VarBlockSepStart, $this->VarBlockSepEnd);
		return preg_replace_callback('/(?:'.preg_quote($this->VarBlockSepStart).'){1}[\d\w\-_: .]+(?:'.preg_quote($this->VarBlockSepEnd).'){1}/sui', array("Templater2", "DoReplaceVariables"), $Template);
	}
	
	
	private function DoReplaceVariables($Text)
	{
		$key = array_search($Text[0], $this->CurrReplacedData["keys"]);
		$R = $key === false ? "" : $this->CurrReplacedData["values"][$key];
		return $R;
	}
	
	
	protected function PrepareSwitch($Template, $Vars = null, $VarsPrefix = null)
	{
		if(!$Vars) $Vars = array();
		//filesize($filename)
		
	}
	
	protected function PrepareWhere($Text)
	{
		if(preg_match('~^if\s*\(\s*(.+?)\)$~iu', $Text, $Z))
		{
			$Text = $Z[1];
		}
		$Z = null;
		if(preg_match_all('~\s*((?:\|\||&&|&|\|)?)\s*((?:\^|\!)*)([\w\d\-_\$.]+)\s*?\s*(?:(={1,3}|!={1,2}|>|>=|<|<=|<>)\s*((\'|")[^\6]*?\6|(?:[\w\d\-_\$.]+)))?~iu', $Text, $Z))
		{
			array_shift($Z);
		}
		elseif($this->DebugMode)
			throw new dmtException("Operations in where clause not found");
		return $Z;
	}
	
	protected function PrepareBlocks($Text)
	{
		$Options = explode($this->PropsSep, $Text[2]);
		$Item = null;
		$Where = null;
		if(sizeof($Options))
		{
			foreach ($Options as $k => $v)
			{
				if(mb_strpos(mb_strtolower($v), "if") !== false)
				{
					$Where = $this->PrepareWhere($v);
				}
				else
				{
					$v = explode("=", $v, 2);
					$v[0] = mb_strtolower(trim($v[0]));
					if($v[0] == "item")
						$Item = sizeof($v) > 1 ? intval($v[1]) : 0;
					elseif($v[0] == "where" && sizeof($v) == 2)
						$Where = $this->PrepareWhere($v[1]);
				}
			}
		}
		if($Item === null)
			$Item = $this->CurrCount > 1 ? sizeof($this->CurrBlocks) - 1 : 0;
		$Ex = true;
		if(!isset($this->CurrBlocks[$Item]))
		{
			$this->CurrBlocks[$Item] = array();
			$Ex = false;
		}
		$Current = array(
			"template"	=> $Text[3],
			"variable"	=> $this->CurrBlockKey.$Item
		);
		if($Where) $Current["where"] = $Where;
		$this->CurrBlocks[$Item][] = $Current;
		$R = $Ex ? "" : $this->VarBlockSepStart.$Current["variable"].$this->VarBlockSepEnd;
		return $R;
	}
	
	protected function GetBlocks(&$Template, $Key)
	{
		$this->CurrBlockKey = $Key.rand(0, 10000);
		$Template = preg_replace_callback('/'.preg_quote($this->InstrSepStart).'\s*('.preg_quote($Key).preg_quote($this->BlockNameSep).'[\w\d\-_]+)\s*'.preg_quote($this->PropsSep).'\s*(.*?)'.preg_quote($this->InstrSepEnd).'(.*?)'.preg_quote($this->InstrSepStart).'\s*\1\s*'.preg_quote($this->InstrSepEnd).'/uims', array("Templater2", "PrepareBlocks"), $Template);
	}
	
	protected function DoInclude($Template, $Options = null, $BlockKey= null) {
		if(!$Options || !$Options["data"])
			throw new dmtException("Template error");
		return $this->PrepareTemplate(file_get_contents(PATH_TEMPLATES.$Options["data"]));
	}
	
	protected function Item($Template, $Options = null, $BlockKey= null)
	{
		if(!$Options)
			throw new dmtException("Template error");
			
		$Template = $this->PrepareTemplate($Template);
			
		$KeyData = isset($Options["datakey"]) ? $Options["datakey"] : null;
		$Keep = isset($Options["keepdata"]) ? (boolean) $Options["keepdata"] : false;
		if(isset($Options["where"]))
		{
			$Where = explode("-", $Options["where"]);
			if(sizeof($Where) == 2)
			{
				$WhereKey = $Where[0];
				$WhereValue = trim($Where[1]);
				$Where = true;
			}
			else $Where = false;
		}
		else $Where = false;
		
		
		$Count = 1;
		if(isset($Options["count"]))
		{
			$Count = intval($Options["count"]);
		}
		$this->CurrCount = $Count;
		$Blk = array();
		$this->GetBlocks($Template, $BlockKey);
		if((!$this->CurrBlocks || !sizeof($this->CurrBlocks)))
		{
			$this->CurrBlocks = array(
				array(
					array(
						"template"	=> $Template,
						"variable" => $this->CurrBlockKey
					)
				)
			);
			$Template = $this->VarBlockSepStart.$this->CurrBlockKey.$this->VarBlockSepEnd;
		}
		$BlkSize = sizeof($this->CurrBlocks);
		$ItemsKey = sizeof($this->CurrBlocks) == 1 ? $this->CurrBlockKey : null;

		$Blocks = array();
		if($BlkSize < $Count)
		{
			for($i = 0; $i < $Count; $i ++)
			{
				if($i < $BlkSize)
					$Blocks[$i] = $this->CurrBlocks[$i];
				else
				{
					$Blocks[$i] = $Blocks[$BlkSize - 1];
					
					$variableName = $this->CurrBlockKey."_auto_".$i;
					foreach ($Blocks[$i] as &$Blk)
						$Blk["variable"] = $variableName;
					$Template = preg_replace('/('.preg_quote($this->VarBlockSepStart).preg_quote($Blocks[$i - 1][0]["variable"]).preg_quote($this->VarBlockSepEnd).')/iums', '\1'.$this->VarBlockSepStart.$variableName.$this->VarBlockSepEnd, $Template);
				}
			}
		}
		else $Blocks = $this->CurrBlocks;

		$this->CurrBlocks = null;
		$this->CurrBlockKey = null;
		
		if(isset($Options["addcollection"]))
		{
			$AddCollection = true;
			$CollectionName = $Options["addcollection"];
			$CollectionKey = $Options["collectionkey"];
			$GroupRelate = null;
			if(preg_match('/\s*\[\s*([.\w\d_\-*]+)\s*=\s*([.\w\d_\-*]+)(?:\s*,\s*([.\w\d_\-*]+)\s*=\s*([.\w\d_\-*]+))?\s*\]/iu', $Options["group"], $Z))
			{
				if(!$Z[1] || !$Z[2] || $Z[3] && !$Z[4] || !$Z[3] && $Z[4])
					throw new dmtException("Goup definition error");
				if($Z[3])
				{
					if(mb_substr($Z[1], 0, 1) == "." || mb_strpos($Z[1], $KeyData.".") === 0)
					{
						$Group = explode(".", $Z[1]);
						$Group = array_pop($Group);
						if(mb_strpos($Z[2], $CollectionName.".") === 0)
							throw new dmtException("Goup definition error");
						$GroupRelate = explode(".", $Z[2]);
						$GroupRelateCurrKey = array_pop($GroupRelate);
						if($GroupRelate[sizeof($GroupRelate) - 1] == "*")
							array_pop($GroupRelate);
				$GroupRelate = implode(".", $GroupRelate);
						if(mb_strpos($Z[3], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[3]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
							
							if(mb_strpos($Z[4], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[4]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						elseif(mb_strpos($Z[4], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[4]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
							if(mb_strpos($Z[3], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[3]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						else throw new dmtException("Goup definition error");
					}
					elseif(mb_substr($Z[2], 0, 1) == "." || mb_strpos($Z[1], $KeyData.".") === 0)
					{
						$Group = explode(".", $Z[2]);
						$Group = array_pop($Group);
						if(mb_strpos($Z[1], $CollectionName.".") === 0)
							throw new dmtException("Goup definition error");
						$GroupRelate = explode(".", $Z[1]);
						$GroupRelateCurrKey = array_pop($GroupRelate);
						if($GroupRelate[sizeof($GroupRelate) - 1] == "*")
							array_pop($GroupRelate);
						if(mb_strpos($Z[3], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[3]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
							
							if(mb_strpos($Z[4], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[4]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						elseif(mb_strpos($Z[4], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[4]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
							
							if(mb_strpos($Z[4], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[3]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						else throw new dmtException("Goup definition error");
					}
					elseif(mb_substr($Z[3], 0, 1) == "." || mb_strpos($Z[3], $KeyData.".") === 0)
					{
						$Group = explode(".", $Z[3]);
						$Group = array_pop($Group);
						if(mb_strpos($Z[4], $CollectionName.".") === 0)
							throw new dmtException("Goup definition error");
						$GroupRelate = explode(".", $Z[4]);
						$GroupRelateCurrKey = array_pop($GroupRelate);
						if($GroupRelate[sizeof($GroupRelate) - 1] == "*")
							array_pop($GroupRelate);
						if(mb_strpos($Z[1], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[1]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
							
							if(mb_strpos($Z[2], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[2]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						elseif(mb_strpos($Z[2], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[2]);
							$GroupCollectionKey = array_pop();
							
							if(mb_strpos($Z[1], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[1]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						else throw new dmtException("Goup definition error");
					}
					elseif(mb_substr($Z[4], 0, 1) == "." || mb_strpos($Z[4], $KeyData.".") === 0)
					{
						$Group = explode(".", $Z[4]);
						$Group = array_pop($Group);
						if(mb_strpos($Z[3], $CollectionName.".") === 0)
							throw new dmtException("Goup definition error");
						$GroupRelate = explode(".", $Z[3]);
						$GroupRelateCurrKey = array_pop($GroupRelate);
						if($GroupRelate[sizeof($GroupRelate) - 1] == "*")
							array_pop($GroupRelate);
						if(mb_strpos($Z[1], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[1]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
							
							if(mb_strpos($Z[2], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[2]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						elseif(mb_strpos($Z[2], $CollectionName.".") === 0)
						{
							$GroupCollectionKey = explode(".", $Z[2]);
							$GroupCollectionKey = array_pop();
							
							if(mb_strpos($Z[1], $GroupRelate.".") === 0)
							{
								$GroupRelateCollKey = explode(".", $Z[1]);
								$GroupRelateCollKey = array_pop($GroupRelateCollKey);
							}
							else throw new dmtException("Goup definition error");
						}
						else throw new dmtException("Goup definition error");
					}
					else throw new dmtException("Goup definition error");
				}
				else
				{
					if(mb_substr($Z[1], 0, 1) == ".")
					{
						$Group = mb_substr($Z[1], 1);
						
						if(mb_strpos($Z[2], $CollectionName) === 0)
						{
							$GroupCollectionKey = explode(".", $Z[2]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
						}
						else throw new dmtException("Goup definition error");
					}
					elseif(mb_substr($Z[2], 0, 1) == ".")
					{
						$Group = mb_substr($Z[2], 1);
						
						if(mb_strpos($Z[1], $CollectionName) === 0)
						{
							$GroupCollectionKey = explode(".", $Z[1]);
							$GroupCollectionKey = array_pop($GroupCollectionKey);
						}
						else throw new dmtException("Goup definition error");
					}
					else throw new dmtException("Goup definition error");
				}
			}
			else $Group = $Options["group"];
			
			$this->Collections[$CollectionName] = array();
			$Collection = array();
			if(isset($Options["where"]))
			{
				$Where = explode("-", $Options["where"]);
				if(sizeof($Where) == 2)
				{
					$WhereKey = $Where[0];
					$WhereValue = trim($Where[1]);
					$Where = true;
				}
				else $Where = false;
			}
			else $Where = false;
		}
		else
		{
			$AddCollection = false;
			$CollectionName = null;
			$CollectionKey = null;
		}
		$R = array();
		if($KeyData && $this->Data)
		{
			$KeyData = explode(".", $KeyData);
			if($KeyData[0] === null)
				array_shift($KeyData);
			$Data = $this->Data;
			$TData = true;
			foreach ($KeyData as $v)
			{
				if(array_key_exists($v, $Data))
				{
					$Data = &$Data[$v];
				}
				else
				{
					$TData = false;
					break;
				}
			}
			if($TData && $Data)
			{
				if(!is_array($Data)) $Data = array($Data);
				//else $Data = array_keys($Data);
				$DataSize = sizeof($Data);
				if($AddCollection)
				{
					$URecIndex = array();
					$SBlockNumber = array();
					$SBlock = array();
					
					for($i = 0; $i < $DataSize; $i ++)
					{
						$v = $Data[$i];
						$v["recordNumber"] = $i;
						if($Where)
						{
							if(!(array_key_exists($WhereKey, $v) && $v[$WhereKey] == $WhereValue))
								continue;
						}
						
						$key = $v[$Group];
						if(!array_key_exists($key, $Collection))
						{
							$Collection[$key] = "";
							$SBlock[$key] = array();
							$URecIndex[$key] = 0;
							$SBlockNumber[$key] = 0;
						}
						if($SBlockNumber[$key] == $Count)
						{
							$SBlockNumber[$key] = 0;
							$Collection[$key] .= $this->ReplaceVariables($Template, $SBlock[$key]);
							$SBlock[$key] = array();
						}
						$CurrentBlock = $Blocks[$SBlockNumber[$key]];
						$v["recordIndex"] = $URecIndex[$key];
						$CB = null;
						foreach($CurrentBlock as $bv)
						{
							if(isset($bv["where"]))
							{
								if(!$this->CheckWhere($bv["where"], $v))
									continue;
							}
							$CB = $bv;
							break;
						}
						if($CB)
						{
							$SBlock[$key][$CB["variable"]] = $this->ReplaceVariables($CB["template"], $v);
							$URecIndex[$key] ++;
							$SBlockNumber[$key] ++;
						}
					}
					
					foreach ($SBlock as $key => $v)
					{
						$Collection[$key] = array_key_exists($key, $Collection) ? $Collection[$key].$this->ReplaceVariables($Template, $v) : $this->ReplaceVariables($Template, $v); //$vTmp
					}
				}
				else
				{
					$URecIndex = 0;
					$SBlockNumber = 0;
					$R = array();
					$SBlock = array();
					for($URecNumber = 0; $URecNumber < $DataSize; $URecNumber ++)
					{
						$v = $Data[$URecNumber];
						
						//TODO: Проверка условий добавления (условия блока инструкций)
						if($Where)
						{
							if(!(array_key_exists($WhereKey, $v) && $v[$WhereKey] == $WhereValue))
								continue;
						}
						
						if($SBlockNumber == $Count)
						{
							$R[] = $this->ReplaceVariables($Template, $SBlock);
							$SBlock = array();
							$SBlockNumber = 0;
						}
						$CurrentBlock = $Blocks[$SBlockNumber];
						$v["recordIndex"] = $URecIndex;
						$v["recordNumber"] = $URecNumber;
						$CB = null;
						foreach($CurrentBlock as $bv)
						{
							if(isset($bv["where"]))
							{
								if(!$this->CheckWhere($bv["where"], $v))
									continue;
							}
							$CB = $bv;
							break;
						}
						if($CB)
						{
							$SBlock[$CB["variable"]] = $this->ReplaceVariables($CB["template"], $v);
							$URecIndex ++;
							$SBlockNumber ++;
						}
					}
					
					if(sizeof($SBlock))
					{
						//if($ItemsKey) $SBlock = array($ItemsKey => implode("", $SBlock));
						$R[] = $this->ReplaceVariables($Template, $SBlock);
					}
				}
				
				unset($SBlock, $URecIndex, $SBlockNumber);
				
				if($AddCollection)
				{
					if($GroupRelate)
					{
						$Relation = array();
						$CurrData = $this->GetSubtreeData($GroupRelate);
						foreach($CurrData as $v)
						{
							if(!array_key_exists($GroupRelateCurrKey, $v) || !array_key_exists($GroupRelateCollKey, $v))
								continue;
							if(isset($Collection[$v[$GroupRelateCurrKey]]))
							{
								if(!isset($Relation[$v[$GroupRelateCollKey]]))
									$Relation[$v[$GroupRelateCollKey]] = "";
								$Relation[$v[$GroupRelateCollKey]] .= $Collection[$v[$GroupRelateCurrKey]];
							}
						}
						unset($Collection);
						$CurrData = &$this->GetSubtreeData($CollectionName);
						foreach($CurrData as $k => &$v)
						{
							if(isset($v[$GroupRelateCollKey]))
							{
								if(isset($Relation[$v[$GroupRelateCollKey]]))
								{
									$v[$CollectionKey] = $Relation[$v[$GroupRelateCollKey]];
								}
							}
						}
					}
					else
					{
						$AData = &$this->GetSubtreeData($CollectionName);
						foreach ($AData as $k => $v)
						{
							if(isset($Collection[$v[$Group]]))
							{
								$AData[$k][$CollectionKey] = $Collection[$v[$Group]];
							}
						}
					}
				}
				if(!$Keep) unset($Data);
			}
		}
		if(!$AddCollection) $R = implode("", $R);
		else $R = "";
		return $R;
	}
	
	protected function CheckWhere($WhereDef, $Data)
	{
		$R = false;
		if($WhereDef)
		{
			$Formula  = array();
			$Operations = array();
			
			foreach($WhereDef[2] as $k => $v)
			{
				$Var1 = $this->GetVariableValue($v, $Data);
				if($WhereDef[1][$k])
				{
					$Formula[] = floor($WhereDef[1][$k]/2) == $WhereDef[1][$k]/2 ? !$Var1 : $Var1;
				}
				else
				{
					if(!$WhereDef[3][$k])
						$Formula[] = (boolean) $Var1;
					else $Formula[] = $this->CompareVariables($Var1, $this->GetVariableValue($WhereDef[4][$k], $Data), $WhereDef[3][$k]);
				}
				if($WhereDef[0][$k])
				{
					$Operations[] = $WhereDef[0][$k];
				}
			}
			if(sizeof($Formula) == sizeof($Operations) + 1)
			{
				$R = $this->ExecFormula($Formula, $Operations);
			}
			else
			{
				if ($this->DebugMode)
				{
					$this->Dump($WhereDef);
					throw new dmtException("Where clause is not valide");
				}
			}
		}
		else
		{
			if ($this->DebugMode)
			{
				$this->Dump($WhereDef);
				throw new dmtException("Where is empty");
			}
		}
		return $R;
	}
	
	protected function GetVariableValue($VarDef, $CurrData, $IsValue = null)
	{
		if($IsValue) return $VarDef;
		$s = mb_substr($VarDef, 0, 1);
		if($s == "'" || $s == '"')
			$R = mb_substr($VarDef, 1, -1);
		elseif(!$VarDef)
			$R = $VarDef;
		else
		{
			$Var = explode(".", $VarDef);
			if(!$Var[0])
			{
				if(sizeof($Var) == 1 && !mb_strlen($Var[0]))
					throw new dmtException("VarDef error");
				array_shift($Var);
			}
			else $CurrData = null;
			$R = $this->GetSubtreeData($Var, false, $CurrData);
			if(is_array($R)) $R = null;
		}
		return $R;
	}
	
	protected function GetFormulaLogicOperator($Operator)
	{
		switch(mb_strtolower($Operator))
		{
			case "&":
			case "&&":
			case "and":
				$R = "&&";
				break;
			case "|":
			case "||":
			case "or":
				$R = "||";
				break;
			case "xor":
				$R = "xor";
			default:
				throw new dmtException("Logic operator is not found. Operator = '".$Operator."'");
		}
		return $R;
	}
	
	protected function ExecFormula($Values, $Operations)
	{
		if(sizeof($Values) == sizeof($Operations) + 1)
		{
			$R = "";
			foreach($Values as $k => $v)
				$R .= (is_bool($v) ? ($v ? "true" : "false") : $v).(array_key_exists($k, $Operations) ? " ".$this->GetFormulaLogicOperator($Operations[$k])." " : "");
			eval("\$R = (boolean) (".$R.");");
		}
		else $R = false;
		return $R;
	}
	
	protected function ExecFormulaOperation()
	{
		
	}
	
	protected function CompareVariables($VarLeft, $VarRight, $Operation)
	{
		switch($Operation)
		{
			/*
			case "":
				if($VarRight === null || is_empty($VarRight))
					$R = (boolean) $VarLeft;
				elseif($this->DebugMode)
					throw new dmtException("Operation is empty and right variable is not empty or null. VarRight = '".$VarRight."'");
				break;
			*/
			case "=":
			case "==":
				$R = ($VarLeft == $VarRight);
				break;
			case "===":
				$R = ($VarLeft === $VarRight);
				break;
			case "!=":
			case "<>":
				$R = ($VarLeft != $VarRight);
				break;
			case "!==":
				$R = ($VarLeft !== $VarRight);
				break;
			case ">":
				$R = ($VarLeft > $VarRight);
				break;
			case ">=":
				$R = ($VarLeft >= $VarRight);
				break;
			case "<":
				$R = ($VarLeft < $VarRight);
				break;
			case "<=":
				$R = ($VarLeft <= $VarRight);
				break;
			default:
				if($this->DebugMode)
					throw new dmtException("Compare operators not found. Operators = '".$Operation."'");
				$R = false;
		}
		return $R;
	}
	
	protected function SelectOption($Template, $Options = null)
	{
		if(!$Options)
			throw new dmtException("Template error");
//$this->Dump(__METHOD__."   LINE = ". __LINE__, $Options);
		$KeyData = isset($Options["datakey"]) ? $Options["datakey"] : null;
		$Selected = isset($Options["selecteddatakey"]) ? $Options["selecteddatakey"] : null;
		$Keep = isset($Options["keepdata"]) ? (boolean) $Options["keepdata"] : false;
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 2", $KeyData, $Selected, $Keep, '/'.preg_quote($this->VarBlockSepStart).'isSelected(?:'.preg_quote($this->VarSep).'(.*?))?'.preg_quote($this->VarBlockSepEnd).'/', $Template);
		if(preg_match('/'.preg_quote($this->VarBlockSepStart).'isSelected(?:'.preg_quote($this->VarSep).'(.*?))?'.preg_quote($this->VarBlockSepEnd).'/', $Template, $SelectedTemplate))
		{
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 3");
			$Substitute = (isset($SelectedTemplate[1]) && $SelectedTemplate[1]) ? $SelectedTemplate[1] : "selected=\"selected\"";
			$SelectedTemplate = $SelectedTemplate[0];
			$Template = preg_replace('/'.preg_quote($this->VarBlockSepStart).'isSelected(?:'.preg_quote($this->VarSep).'(.*?))?'.preg_quote($this->VarBlockSepEnd).'/', $this->VarBlockSepStart.'isSelected'.$this->VarBlockSepEnd, $Template);
		}
		elseif($Selected)
		{
			throw new dmtException("Not found key 'isSelected' in template. ".__METHOD__);
		}
		else
		{
			$SelectedTemplate = $this->VarBlockSepStart.'isSelected'.$this->VarBlockSepEnd;
			$Substitute = "selected=\"selected\"";
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 4", $Substitute, $SelectedTemplate, $Template);
		}
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 5");
		if($Selected)
		{
			$Selected = explode(".*.", $Selected);
			$SelectedSubKey = $Selected[0];
			if(sizeof($Selected) == 2)
			{
				$SelectedKey = $Selected[1];
				$SelectedAsArray = true;
			}
			else
			{
				$SelectedKey = $Selected[0];
				$SelectedAsArray = false;
			}
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 6", $Selected, $SelectedSubKey, $SelectedAsArray);
			//Получаем выделенный элемент
			$SelectedData = $this->GetSubtreeData($SelectedSubKey, !$SelectedAsArray);
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 7", $SelectedData);
			$SelectedKey = !$SelectedAsArray && isset($SelectedData[1]) ? $SelectedData[1] : $SelectedKey;
			$SelectedData = !$SelectedAsArray && isset($SelectedData[0]) ? $SelectedData[0] : $SelectedData;
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 8", $SelectedKey, $SelectedData);
		}
		else
		{
			$SelectedKey = null;
			$SelectedData = null;
			$SelectedAsArray = false;
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 9", $Selected, $SelectedKey, $SelectedAsArray);
		}
		
		$R = array();
		if($KeyData && $this->Data)
		{
			$Data = $this->GetSubtreeData($KeyData);
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 10", $Data);
			if($Data)
			{
				if(!is_array($Data))
					$Data = array($Data);
				$SelectedData = $SelectedAsArray ? $this->GetKeysFromData($SelectedData, $SelectedKey) : array($SelectedData);
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 11", $SelectedData);
				foreach ($Data as $k => $v)
				{
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ ".($k + 12), $v);
					if($SelectedKey && isset($v[$SelectedKey]) && in_array($v[$SelectedKey], $SelectedData))
					{
						$v["isSelected"] = $Substitute;
					}
					$R[] = $this->ReplaceVariables($Template, $v);
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ ".($k + 13), $this->ReplaceVariables($Template, $v));
				}
				if(!$Keep) unset($Data);
			}
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ 000 +++");
		}
//$this->Dump(__METHOD__."   LINE = ". __LINE__, "+++ ******** +++");
		return implode("", $R);
	}
	
	
	protected function GetKeysFromData($Data, $Key)
	{
		$R = array();
		if(is_array($Data) && !isset($Data[$Key]))
		{
			if(sizeof($Data))
			{
				foreach ($Data as $v)
				{
					$R[] = $v[$Key];
				}
			}
		}
		else
		{

		}
		return $R;
	}
	
	
	protected function ConditionView($Template, $Options = null)
	{
		if(!$Options)
			throw new dmtException(__METHOD__." Template error.");
		
		$R = "";
		if(isset($Options["datakey"]) && isset($Options["value"]))
		{
			if($this->Data)
			{
				$Data = $this->GetSubtreeData($Options["datakey"]);
				if($Data)
				{
					$Value = $this->GetValues($Options["value"]);
					if(is_array($Value) && in_array($Data, $Value) || !is_array($Value) && $Data == $Value)
					{
						//$R = $Template;
						$R = $this->PrepareTemplate($Template);
					}
				}
			}
		}
		else throw new dmtException("Error Value or DataKey option in ".__METHOD__);
		return $R;
	}
	
	
	
	/**
	 * 
	 * Возвращает поддерево данных из массива данных по заданному пути
	 * @param string $KeyData - Путь
	 * @param boolean $GetLastKey - При значении аргумента, эквивалентном true, вернется массив, первый элемент, которого - выбранные данные, второй - последный ключ
	 * @param array $Data - Необязательный. Массив данных.
	 * Если аргумент не задан, будет использоваться массив данных $this->Data. Запланировано. Не реализовано в текущей версии.
	 */
	protected function &GetSubtreeData($KeyData, $GetLastKey = null, $Data = null)
	{
		if(!$KeyData)
			throw new dmtException("Key data error");
		$TData = true;
		if(!is_array($KeyData))
			$KeyData = explode(".", $KeyData);
		if(!$Data)
			$Data = &$this->Data;
		
		if(is_array($Data))
		{
			foreach ($KeyData as $v)
			{
				if(!$Data || !array_key_exists($v, $Data))
				{
					$TData = false;
					break;
				}
				$Data = &$Data[$v];
			}
		}
		else $TData = false;
		if($TData)
		{
			if($GetLastKey)
			{
				$R = array(0 => &$Data, 1 => $v);
			}
			else $R = &$Data;
		}
		else $R = array();
		$R = &$R;
		return $R;
	}
	
	protected function GetValues($Value)
	{
		if(mb_substr($Value, 0, 1) == "[")
		{
			$Value = preg_split('/\s*,\s*/', mb_substr($Value, 1, -1));
			explode(",", mb_substr($Value, 1, -1));
		}
		return $Value;
	}
}