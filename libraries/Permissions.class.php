<?php
class Permissions extends Controller
{
	/**
	 *
	 * @var Permissions
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return Permissions
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"permissionsTypeId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"permissionsTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"permissionsTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
                "serviceId"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
			)
		);
		$this->Modes = array(
			//Возвращает
			"a"	=> array(
				"exec"		=> array("PermissionsProcessor", "GetPermissionsTypesList")
			),
            //Создает
			"b"	=> array(
				"exec"		=> array("PermissionsProcessor", "CreatePermissionsType"),
				"TplVars"		=> array("permissionsTypeName" => 1, "permissionsTypeDescription" => 0)
			),
             //Сохраняет
			"c"	=> array(
				"exec"		=> array("PermissionsProcessor", "SavePermissionsType"),
				"TplVars"		=> array("pertnerTypeId" => 1, "permissionsTypeName" => 0, "permissionsTypeDescription" => 0)
			),
			//Удаляет
			"d"	=> array(
				"exec"		=> array("PermissionsProcessor", "DeletePermissionsType"),
                "TplVars"		=> array("pertnerTypeId" => 1)
			),
            "e"	=> array(
				"exec"		=> array("PermissionsProcessor", "GetScopeForClient"),
                "TplVars"		=> array("serviceId" => 0)
			),
            "f"	=> array(
				"exec"		=> array("PermissionsProcessor", "GetPermissionsForClient"),
                "TplVars"		=> array("serviceId" => 0)
			),
            "g"	=> array(
				"exec"		=> array("PermissionsProcessor", "GetPermissions")
			),
		);
	}
}