<?php
class Models extends Controller
{
	/**
	 *
	 * @var Models
	 */
 	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Models
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				"findText"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FIND_STRING)
				),
				"modelId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "submodelActuality"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "modelActuality"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "modelActuality"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "actuality"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"facilityId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelFamilyId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelFamilyStatus"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"default"		=> 0
				),
				"modelFamilyImage"		=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("gif", "jpeg", "jpg", "png")
						),
    					"name"			=> "_auto",
						"savepath"		=> PATH_MODELS_IMAGES,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
				"modelFamilyPageUrl"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_URL)
				),
				"modelFamilyOrder"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelFamilyComment"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0)
				),
				"modelStatus"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"default"		=> 0
				),
				"modelPageUrl"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_URL)
				),
				"modelOrder"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelComment"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0)
				),
				"facilityModelStatus"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"default"		=> 0
				),
				"facilityModelPage"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_URL)
				),
				"facilityModelOrder"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"facilityModelComment"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0)
				),
				"facilityModelImage"		=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("gif", "jpeg", "jpg", "png")
						),
    					"name"			=> "_auto",
						"savepath"		=> PATH_MODELS_IMAGES,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
			)
		);

		$this->Modes = array(
			//Получить список моделей или найти модель по названию
			"a"	=> array(
				"exec"			=> array("ModelsProcessor", "GetModels"),
				"TplVars"		=> array("findText" => 0, "modelActuality" => 0)
			),
			//Получить список субмоделей или найти субмодель по названию
			"b"	=> array(
				"exec"			=> array("ModelsProcessor", "GetSubmodel"),
				"TplVars"		=> array("modelId" => 0, "findText" => 0, "submodelActuality" => 0)
			),
			
			
			//Получить список моделей или найти модель по названию
			//Получить список субмоделей или найти субмодель по названию
			"a1"	=> array(
				"exec"			=> array("ModelsProcessor", "GetModelsNew"),
				"TplVars"		=> array("facilityId" => 0, "modelActuality" => 0, "modelFamilyId" => 0)
			),
			"b1"	=> array(
				"exec"			=> array("ModelsProcessor", "GetModelsFamilies"),
				"TplVars"		=> array("facilityId" => 0, "modelActuality" => 0)
			),
			
			"c"	=> array(
				"exec"			=> array("ModelsProcessor", "GetModelsFamiliesFacilities"),
				"TplVars"		=> array("facilityId" => 2)
			),
			"d"	=> array(
				"exec"			=> array("ModelsProcessor", "SaveModelsFamiliesFacilities"),
				"TplVars"		=> array("facilityId" => 2, "modelFamilyId" => 2, "modelFamilyStatus" => 2, "modelFamilyPageUrl" => 0, "modelFamilyOrder" => 0, "modelFamilyComment" => 0),
				"getFiles"		=> array("modelFamilyImage" => 0)
			),
			"f"	=> array(
				"TplVars"		=> array("modelFamilyImage" => 2)
			),
			
			"e"	=> array(
				"exec"			=> array("ModelsProcessor", "GetModelsFacilities"),
				"TplVars"		=> array("facilityId" => 2)
			),
			"g"	=> array(
				"exec"			=> array("ModelsProcessor", "SaveModelsFacilities"),
				"TplVars"		=> array("facilityId" => 2, "modelId" => 2, "facilityModelStatus" => 2, "facilityModelPage" => 0, "facilityModelOrder" => 0, "facilityModelComment" => 0),
				"getFiles"		=> array("facilityModelImage" => 0)
			),
			"h"	=> array(
				"TplVars"		=> array("facilityModelImage" => 2)
			),
		);
	}
}