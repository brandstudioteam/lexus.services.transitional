<?php
class InetDriver extends BaseDebug
{
	private $Socket = null;
	private $TimeOut = 3;
	private $Result;
	private $ProxyIP;
	private $ProxyPort;
	private $ProxyLogin;
	private $ProxyPassword;
	private $ProxyAth="";

	private $CharsetIn="";

	//private $HeadersTxt="";
	private $Body="";
	private $Headers=array();
	private $Cookies=array();
	private $CookiesTxt="";
	private $URL="";
	private $QueryString="";
	private $Query=array();
	private $ResponseResult="";
	private $SendingRequest="";

	private $Getting = array("ResponseResult", "Result", "Body", "Headers", "Cookies", "CookiesTxt", "QueryString", "Query", "URL", "SendingRequest");

     function __get($Name)
     {
     	if(in_array($Name, $this->Getting)) return $this->$Name;
     	else throw new dmtException("Property \"".$Name."\" can not be obtained or not found in class \"".__CLASS__."\"");
     }

	function __construct()
	{
		parent::__construct();
		if(ISPROXY) $this->SetProxy(PROXY_IP, PROXY_PORT, PROXY_LOGIN, PROXY_PASSWORD);
	}

	public function SetProxy($ProxyIP, $ProxyPort = null, $ProxyLogin = null, $ProxyPassword = null)
	{
		$this->ProxyIP = $ProxyIP;
		$this->ProxyPort = $ProxyPort;
		$this->ProxyPassword = $ProxyPassword;
		$this->ProxyLogin = $ProxyLogin;
	}

	/**
	 *
	 * Выполняет запрос через сокет и сохраняет ответ в собственных переменных
	 * @param string $Data данные для отправки
	 * @param string $Host - имя хоста
	 * @param string $Path - путь на хосте
	 * @param string $Port - порт подключения
	 * @param string $Method - метод передачи данных: POST или GET
	 * @param string $Protocol - протокол подключения: http: или другие
	 * @param string $Charset - кодировка, в которой ожидается получить ответ
	 * @param array $Headers - массив заголовков, которые необходимо отправить серверу
	 * @param bool $Parse - разобрать ответ на залоговки и содержимое
	 * @param bool $Converted - конвертировать содержимое из кодировки, указанной в $Charset в текущую кодировку, определенную константой CHARSET.
	 * @throws dmtException
	 */
	public function Request($Data, $Host, $Path = null, $Port=80, $Method="GET", $Protocol="http:", $Charset = "utf-8", $Headers = null, $Parse = false, $Converted = false, $Timeout = null)
	{
		$Method = strtoupper($Method);
		$this->CharsetIn = $Charset;
		if($Timeout)
			$this->TimeOut = $Timeout;
		if(is_array($Data))
			$Data = self::PrepareData($Data);

		if($Method == "GET") $Data = mb_strlen($Data) ? ((substr($Data, 0, 1) == "?") ? $Data : "?".$Data) : "";
		if($Path)
		{
			$Path = trim((substr($Path, 0, 1) == "/") ? $Path : "/".$Path);
		}
		$URL = $Protocol."//".$Host.($Path ? $Path : "").($Method == "GET" ? mb_substr($Data, 0, 1) == "?" ? $Data : "?".$Data : "");
		$H = $Host;
		$Host = parse_url($URL);

$this->Dump(__METHOD__.": ".__LINE__, ($Protocol=="https:" ? "ssl://" :  ($Protocol=="udp:" ? "udp://" : "")).$Host["host"], $Port, $this->TimeOut);

		if($this->ProxyIP)
		{
			$this->Socket = fsockopen($this->ProxyIP, $this->ProxyPort, $E, $ES, $this->TimeOut);
			if($this->ProxyLogin != "")
				$this->ProxyAth = "Proxy-Authorization: Basic ".base64_encode("$this->ProxyLogin:$this->ProxyPassword")."\r\n";
		}
		else $this->Socket = fsockopen(($Protocol=="https:" ? "ssl://" :  ($Protocol=="udp:" ? "udp://" : "")).$Host["host"], $Port, $E, $ES, $this->TimeOut);
		if($this->Socket === false)
			throw new dmtException("fsockopen error: ErrorNumber = ".$E.", ErrorMessage = \"".$ES."\"");
	    if($Headers)
        {
        	if(is_array($Headers)) $Headers = implode("\r\n", $Headers);
        	$Headers = $Headers."\r\n";
        }

		if($Method == "GET")
        {
	        //$p  = $Method." ".($Host["path"] ? $Host["path"] : "/").($Host["query"] ? "?".$Host["query"] : "")." HTTP/1.1\r\n";
	        $this->SendingRequest  = $Method." ".$URL." HTTP/1.1\r\n";
	        $this->SendingRequest .=  "Host: ".$Host["host"]."\r\n";//$H."\r\n"; //

	        if($this->ProxyIP) $this->SendingRequest .= $this->ProxyAth;
	        if($Charset) $this->SendingRequest .=  "Content-Type: text/html; charset=".$Charset."\r\n";
	        if($Headers) $this->SendingRequest .= $Headers;
	        $this->SendingRequest .=  "Connection: close\r\n";
	        $this->SendingRequest .=  "\r\n";

	        if(!fputs($this->Socket, $this->SendingRequest))
	        	throw new dmtException("fputs in socket error");
        }
        else {
        	/*
			if(mb_substr($URL, -1) != "/")
				$URL .= "/";
				*/
			fwrite($this->Socket, $Method." ".$URL." HTTP/1.1\r\n");
	        fwrite($this->Socket, "Host: ".$Host["host"]."\r\n");
	        if($this->ProxyIP && $this->ProxyAth) fwrite($this->Socket, $this->ProxyAth);
	        fwrite($this->Socket, "Connection: close\r\n");
	        if($Headers) fwrite($this->Socket, $Headers);
            fwrite($this->Socket, "Content-Type: application/x-www-form-urlencoded\r\n");
            fwrite($this->Socket, "Content-length: ".strlen($Data)."\r\n");
            fwrite($this->Socket, "\r\n");
            fwrite($this->Socket, $Data."\r\n");
        }

        $R="";
		while(!feof($this->Socket))
			$R .= fgets($this->Socket, 4096);
        fclose($this->Socket);
        $this->Result = $R;
        if($Parse) $this->Parse($Converted);
	}

	/**
	 *
	 * Выполняет запрос через сокет и сохраняет ответ в собственных переменных
	 * @param string $Data данные для отправки
	 * @param string $Host - имя хоста
	 * @param string $Path - путь на хосте
	 * @param string $Port - порт подключения
	 * @param string $Method - метод передачи данных: POST или GET
	 * @param string $Protocol - протокол подключения: http: или другие
	 * @param string $Charset - кодировка, в которой ожидается получить ответ
	 * @param array $Headers - массив заголовков, которые необходимо отправить серверу
	 * @param bool $Parse - разобрать ответ на залоговки и содержимое
	 * @param bool $Converted - конвертировать содержимое из кодировки, указанной в $Charset в текущую кодировку, определенную константой CHARSET.
	 * @throws dmtException
	 */
	public function Request1($Data, $Host, $Path = null, $Port=80, $Method="GET", $Protocol="http:", $Charset = "utf-8", $Headers = null, $Parse = false, $Converted = false)
	{
		$this->Socket = fsockopen(($Protocol=="https:" ? "ssl://" :  ($Protocol=="udp:" ? "udp://" : "")).$Host, $Port, $E, $ES, $this->TimeOut);
		if($this->Socket === false)
			throw new dmtException("fsockopen error: ErrorNumber = ".$E.", ErrorMessage = \"".$ES."\"", 3);
		fwrite($this->Socket, $Data."\r\n");
        $R="";
		while(!feof($this->Socket))
			$R .= fgets($this->Socket, 4096);
        fclose($this->Socket);
        $this->Result = $R;
        return $R;
	}


	private function Parse($Converted)
	{
		$Res = explode("\r\n\r\n", $this->Result);
		$this->HeadersTxt = $Res[0];

		$this->ParseHeaders($Res[0]);
		$this->Body = $this->ParseBody($Res[1], $Converted);
	}


    private function ParseBody($Data, $Converted)
    {
        $Headers = is_array($this->Headers) ? array_change_key_case($this->Headers, CASE_UPPER) : null;
        if($Headers && isset($Headers["TRANSFER-ENCODING"]) && mb_strtolower($Headers["TRANSFER-ENCODING"]) == 'chunked')
        {
            $Body = "";
            $Start = 0;
            $Len = mb_strlen($Data);
            while(1)
            {
                $Count = "";
                $Char = mb_substr($Data, $Start, 1);
                while(ord($Char) != 13 && $Start < $Len)
                {
                    $Count .= $Char;
                    $Start ++;
                    $Char = mb_substr($Data, $Start, 1);
                }
                 $Start +=2;
                $Count = intval(hexdec($Count));
                if($Count == 0)
                    break;
                $Body = mb_substr($Data, $Start, $Count);
                $Start += $Count + 2;
            }
        }
        else $Body = $Data;
        return ($this->CharsetIn && $Converted) ? mb_convert_encoding($Body, CHARSET, $this->CharsetIn) : $Body;
    }

	private function ParseHeaders($Text)
	{
		$Text = explode("\r\n", $Text);
		$R = explode(" ", $Text[0]);
		$this->ResponseResult = trim($R[1]);
		unset($Text[0]);
		foreach ($Text as $v)
		{
			$v = explode(":", $v);
			if(mb_strtolower($v[0]) == "set-cookie")
			{
				$this->CookiesTxt = $v[1];
				$v = explode(";", $v[1]);
				foreach ($v as $c)
				{
					$c = explode("=", $c);
					$this->Cookies[trim($c[0])]=trim($c[1]);
				}
			}
			else $this->Headers[trim($v[0])] = trim($v[1]);
		}
	}

	public function SetTimeout($TimeOut=100)
	{
		if(is_integer($TimeOut)) $this->TimeOut = $TimeOut;
	}

	public function Reset()
	{
		$this->Body="";
		$this->Headers=array();
		$this->Cookies=array();
		$this->CookiesTxt="";
		$this->URL="";
		$this->QueryString="";
		$this->Query=array();
		$this->ResponseResult="";
		$this->SendingRequest="";
	}

	public function AsyncSend($Data, $Host, $Path = null, $Port=80, $Method="GET", $Protocol="http:", $Charset = "utf-8", $Headers = null)
	{
		$Method = strtoupper($Method);
		$this->CharsetIn = $Charset;
		if($Method == "GET") $Data = mb_strlen($Data) ? ((substr($Data, 0, 1) == "?") ? $Data : "?".$Data) : "";
		if($Path) $Path = (substr($Path, 0, 1) == "/") ? $Path : "/".$Path;
		$URL = $Protocol."//".$Host.($Path ? $Path : "").($Method == "GET" ? $Data : "");

		$H = $Host;

		$Host = parse_url($URL);


		$Socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		// задаем неблокирующий режим сокетов
		socket_set_nonblock($Socket);
		// соединяемся
		socket_connect($Socket, $Host, $Port);
		// записываем в сокет


		if($Headers)
		{
			if(is_array($Headers)) $Headers = implode("\r\n", $Headers);
			$Headers = $Headers."\r\n";
		}

		if($Method == "GET")
        {
	        //$p  = $Method." ".($Host["path"] ? $Host["path"] : "/").($Host["query"] ? "?".$Host["query"] : "")." HTTP/1.1\r\n";
	        $this->SendingRequest  = $Method." ".$URL." HTTP/1.1\r\n";
	        $this->SendingRequest .=  "Host: "."Host: ".$Host["host"]."\r\n";//$H."\r\n"; //

	        if($this->ProxyIP) $this->SendingRequest .= $this->ProxyAth;
	        if($Charset) $this->SendingRequest .=  "Content-Type: text/html; charset=".$Charset."\r\n";
	        if($Headers) $this->SendingRequest .= $Headers;
	        $this->SendingRequest .=  "Connection: close\r\n";
	        $this->SendingRequest .=  "\r\n";

        }
        else {
			socket_write($Socket, $Method." / HTTP/1.1\r\n");
	        socket_write($Socket, "Host: ".$Data."\r\n");
	        //if($this->ProxyIP && $this->ProxyAth) socket_write($Socket, $this->ProxyAth);
	        socket_write($Socket, "Connection: close\r\n");
	        if($Headers) socket_write($Socket, $Headers);
            socket_write($Socket, "Content-Type: application/x-www-form-urlencoded\r\n");
            socket_write($Socket, "Content-length: ".strlen($Data)."\r\n");
            socket_write($Socket, "\r\n");
            socket_write($Socket, $Data."\r\n");
        }
	}

	static public function PrepareData($Data, $Separator = null, $KeySeparator = null)
	{
		$R = array();
		self::PrepareDataExec($Data, $R);

		return implode($Separator ? $Separator : "&", $R);
	}

	static protected function PrepareDataExec($Values, &$Array, $Key = null, $Level = null, $KeySeparator = null)
	{
		if(!$Level)
			$Level = 0;

		foreach ($Values as $k => $v)
		{
			if(is_array($v))
			{
				$Level ++;
				self::PrepareDataExec($v, $Array, $Key ? $Key."[".$k : $k, $Level, $KeySeparator);
				$Level --;
			}
			else
				$Array[] = ($Key ? $Key."[".$k : $k).(str_repeat("]", $Level)).($KeySeparator ? $KeySeparator : "=").urlencode($v);
		}
		return $Array;
	}
}