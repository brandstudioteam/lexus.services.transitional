<?php

class AccountsProcessor extends Processor
{
	/**
	 *
	 * @var AccountsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var AccountsData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return AccountsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new AccountsData();
	}

	/**
	 *
	 * Создание учетной записи
	 *
	 * @param string $Login - Имя учетной записи
	 * @param string $Email - Адрес электронной почты
	 * @param integer $Lang - Идентификатор языка пользователя
	 * @param string $Pssw - Пароль в формате md5
	 * @param string $ConfPassw - Подтверждение пароля в формате md5
	 * @param integer $AccType - Тип учетной записи
	 * @throws dmtException
	 *
	 */
	public function Register($Name, $Login, $Passw, $Partner = null, $Email = null)
	{
		User::Init()->CheckRole(ROLE_ADMIN);

		if(User::Init()->IsLogged())
			throw new dmtException("User is already register. User ID = ".User::Init()->GetId(), 7);
		/*
		if($this->CData->IsEmailExists($Email))
			throw new dmtException("Email is exists. Email: ".$Email, 1);
		if($this->CData->IsLoginExists($Login))
			throw new dmtException("Login is exists. Login: ".$Login, 2);
		if($Passw != $ConfPassw)
			throw new dmtException("Password is not confirm.", 3);
		*/

		$Type = $Partner ? ROLE_PARTNER : ROLE_ADMIN;

		//$this->CData->Register($Name, $Login, $Passw, $Partner, $Email);
	}

	public function CreateAccount($Type, $FName, $MName, $LName, $Partner, $Role, $Children, $ChildrenRoles, $Login, $Email)
	{
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_USERS_CREATE, $Partner);

		if(!$Login)
			throw new dmtException("Login is empty");
		if(!$Email)
			throw new dmtException("Email is empty");
		$Email = mb_strtolower($Email);
		$Account = $this->CData->CreateAccount($Type, $Email, $FName, $MName, $LName, $Login, $Partner, $Role, $Children, $ChildrenRoles);
        $Hash = sha1(($Email ? $Email : $Login).microtime(true));
        $Code = sha1(PASSWORD_PREFIX.md5(($Email ? $Email : $Login)));
        $this->CData->Restore($Account, $Hash, $Code);
        $Mailer = new MailDriver(false, true);
		if(WS::init()->BrandId == 1)
		{
			$Mailer->From = 'mailer@toyota.ru';
			$Mailer->FromName = 'TOYOTA';
			$Url = "http://contacts.toyota.ru/admin/";
			$Link = 'http://service.toyota.ru/__restore_password_/'.$Hash.'?_sC='.sha1($Code."753951").'&_sR='.$Code.'&_sV='.sha1("753951".$Hash);
			$Mailer->Subject = 'Учетная запись на contacts.toyota.ru';
		}
		else
		{
			$Mailer->From = 'mailer@lexus.ru';
			$Mailer->FromName = 'LEXUS';
			$Url = "http://content.lexus-russia.ru/admin/";
			$Link = 'http://accounts.lexus.ru/__restore_password_/'.$Hash.'?_sC='.sha1($Code."753951").'&_sR='.$Code.'&_sV='.sha1("753951".$Hash);
			$Mailer->Subject = 'Учетная запись на content.lexus-russia.ru';
		}
		$Data = array(
			"adminName" => User::Init()->GetName(),
			"userLogin" => $Login,
			"siteUrl" => $Url,
			"link" => $Link
		);
        $Mailer->MsgHTML(Templater2::Init()->Merge(TEMPLATES_REGISTER_USER, $Data));
        $Mailer->AddAddress($Email);
        $Mailer->Send();
		
		$Detail = $this->CData->GetUserData($Account);
		
		EventProcessor::Init()->CreateSystemEvent(19, $Detail);
		
		$this->AddEvent(1, 19, 2, $Detail, $Partner, User::Init()->GetAccount(), WS::Init()->GetClientUID());
		
		return $Detail;
	}


    public function CreateAccountsFromFile($File)
    {
GetDebug()->SetDebugParams(array("file" => DEBUG_FILES_PATH."/importUsers.log"));
$this->Dump(__METHOD__.": ".__LINE__, "------------", $File);
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_CREATE_FROM_FILE);
        $A = array();
        $E = array();
        if(is_array($File))
        {
            foreach($File as $v)
            {
                $v = PATH_ROOT."/debug/".$v;
                //$Content = file_get_contents(PATH_ROOT."/debug/".$v);
                if(preg_match_all('/(\d+),\'([^\']+)\',\'([^\']*)\',\'([^\']+)\',(\d+),(\'[\d,\s]+\'|\d+),\'([^\']+)\',\'([^\']+)\'/ui', file_get_contents($v), $Content))
                {
$this->Dump($Content);
                    foreach($Content[0] as $k => $c)
                    {
                        try
                        {
                            $Type = Math::PrepareUnsignedInteger($Content[1][$k]);
                            $FName = Strings::PrepareFirstAndMiddleName($Content[2][$k]);
                            $MName = Strings::PrepareFirstAndMiddleName($Content[3][$k]);
                            $LName = Strings::PrepareLastName($Content[4][$k]);
                            $Partner = Math::PrepareUnsignedInteger($Content[5][$k]);
                            $R = trim($Content[6][$k], "'");
                            $R = explode(",", $R);
                            $Roles = array();
                            foreach($R AS $r)
                                $Roles[] = Math::PrepareUnsignedInteger($r);
                            $Login = Strings::PrepareText($Content[7][$k]);
                            $Email = Strings::ValidateEmailOne($Content[8][$k]);
                            $Email = mb_strtolower($Email);
    $this->Dump(__METHOD__.": ".__LINE__, $Type, $FName, $MName, $LName, $Partner, $Roles, $Login, $Email);
                            $A[] = $this->CreateAccount($Type, $FName, $MName, $LName, $Partner, $Roles, $Login, $Email);
                        }
                        catch(dmtException $e)
                        {
                            $E[] = "Тип пользователя: <b>".$Content[1][$k]."</b><br />
Имя: <b>".$Content[2][$k]."</b><br />Отчество: <b>".$Content[3][$k]."</b><br />Фамилия: <b>".$Content[4][$k]."</b><br />
Идентификатор партнера: <b>".$Content[5][$k]."</b><br />Роли: <b>".$Content[6][$k]."</b><br />Логин: <b>".$Content[7][$k]."</b><br />E-mail: <b>".$Content[8][$k]."</b>";
                        }
                    }
                }
				unlink($v);
            }
        }
        $R = array("added" => $A);
        if(sizeof($E))
            $R["notAdded"] = implode("<br /><br />", $E);
        return $R;
    }


	public function Save($Account, $Type = null, $FName = null, $MName = null, $LName = null, $Partner = null, $Role = null, $Children = null, $ChildrenRoles = null, $Login = null, $Email = null)
	{
$this->Dump(__METHOD__.": ".__LINE__, $Children, $ChildrenRoles);
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_EDIT);
        $OldPartner = $this->CData->GetPartnerForUser($Account);
        if($OldPartner != $Partner)
            //PartnersProcessor::Init()->CheckPartnersHierarchy($OldPartner, true);
            PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_USERS_EDIT, $OldPartner);
        //PartnersProcessor::Init()->CheckPartnersHierarchy($Partner, true);
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_USERS_EDIT, $Partner);
		$this->CData->Save($Account, $Type, $Email, $FName, $MName, $LName, $Partner, $Login, $Role, $Children, $ChildrenRoles);
		/*
		$this->CData->DeleteRolesForAccount($Account);
        if($Role)
			$this->CData->RoleAdd($Account, $Role);
		*/
		$Detail = $this->CData->GetUserData($Account);
		/*
		$Detail["children"] = $this->CData->GetUsersChildrenParents($Account);
		$Detail["childrenRoles"] = $this->CData->GetUsersChildrenRoles($Account);
		*/
		EventProcessor::Init()->CreateSystemEvent(21, $Detail);
		
		$this->AddEvent(1, 21, 2, $Detail, $Partner, User::Init()->GetAccount(), WS::Init()->GetClientUID());
		
		return $Detail;
	}

	public function SaveFIO($FName = null, $MName = null, $LName = null, $Position = null, $Phone = null, $AdvPhone = null)
	{
		$this->CData->SaveFIO($FName, $MName, $LName, $Position, $Phone, $AdvPhone);
	}

    public function DeleteAccount($Account)
    {
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_USERS_DELETE, $this->CData->GetPartnerForUser($Account));
        return array("deleted" => $this->CData->DeleteAccount($Account));
    }

	public function AdminChangePassword($Account, $Password)
	{
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_USERS_PASSWORD, $this->CData->GetPartnerForUser($Account));
        $this->CData->AdminChangePassword($Account, sha1($Password.PASSWORD_PREFIX));
	}


	public function ChangePassword($Password, $NewPassword)
	{
        $this->CData->ChangePassword($Password, sha1($NewPassword.PASSWORD_PREFIX));
	}

	public function GetUsersList($Type = null)
	{
		return $this->CData->GetUsersList($Type);
	}

	public function GetUsersListF($Type = null)
	{
		$Partners = PermissionsProcessor::Init()->GetScopeAccess(array(
																	PERMISSIONS_USERS_VIEW,
																	PERMISSIONS_USERS_CREATE,
																	PERMISSIONS_USERS_DELETE,
																	PERMISSIONS_USERS_EDIT
																));
		return $this->CData->GetUsersListF(WS::Init()->GetBrandId(), $Partners, $Type);
	}

	public function GetUser($User)
	{
		return $this->CData->GetUser($User);
	}


	/**
	 *
	 * Возвращает результат проверки пароля текущего пользователя
	 * @param string $Password - Пароль текущего пользователя, который необходимо проверить.
	 */
	public function CheckPassword($Password)
	{
		if(User::Init()->IsSessionStarted)
			$R = (md5($Password) == $_SESSION[SESSIOIN_SECTION_USERS]["PsswrdMD5"]);
		else $R = false;
		return $R;
	}

	public function IsEmailNotExists($Email)
	{
		if($this->CData->IsEmailExists($Email))
			throw new dmtException("EmailIsExists");
		return $Email;
	}

	public function IsEmailExists($Email)
	{
		if(!$this->CData->IsEmailExists($Email))
			throw new dmtException("EmailIsNotFound");
		return $Email;
	}

	public function IsLoginExists($Login)
	{
		if(!$this->CData->IsLoginExists($Login))
			throw new dmtException("LoginIsNotFound");
		return $Login;
	}

	public function IsLoginNotExists($Login)
	{
		if($this->CData->IsLoginExists($Login))
			throw new dmtException("LoginIsExists");
		return $Login;
	}



	/**
	 *
	 * Возвращает список ролей указанного пользователя
	 * @param integer $Account - Идентификатор учетной записи пользователя
	 */
	public function GetRoles($Account)
	{
		$Partners = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_USERS_CREATE, PERMISSIONS_USERS_EDIT, PERMISSIONS_ROLES_CREATE, PERMISSIONS_ROLES_EDIT));
		$R = $this->CData->GetRoles($Account, $Partners);
		return $R;
	}

	/**
	 *
	 * Добавляет роль пользователю
	 * @param integer $Account - Идентификатор учетной записи пользователя
	 * @param integer $Role - Идентификатор роли
	 */
	public function RoleAdd($Account, $Role)
	{
		$this->CData->RoleAdd($Account, $Role);
		return ;
	}

	/**
	 *
	 * Удаляет роль пользователя
	 * @param integer $Account - Идентификатор учетной записи пользователя
	 * @param integer $Role - Идентификатор роли
	 */
	public function RoleDelete($Account, $Role)
	{
		$this->CData->RoleDelete($Account, $Role);
	}




	public function GetCurrentPartnerId()
	{
		return (User::Init()->GetType() == ACCOUNT_TYPE_DEALER) ? User::Init()->GetPartnerId() : "";
	}

	public function GetPartnerForUser($Account)
	{
		return $this->CData->GetPartnerForUser($Account);
	}


    public function GetCurrentData()
    {
        return $this->CData->GetCurrentData();
    }


    public function CheckEmailForUpdate($Email, $Account)
    {
        return $this->CData->CheckEmailForUpdate(WS::Init()->GetBrandId(), $Email, $Account);
    }

    public function CheckLoginForUpdate($Login, $Account)
    {
        return $this->CData->CheckLoginForUpdate(WS::Init()->GetBrandId(), $Login, $Account);
    }

	public function GetUsersChildrenParents()
	{
		$Scope = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_USERS_CREATE, PERMISSIONS_USERS_EDIT));
		return $this->CData->GetUsersChildrenParents($Scope);
	}

	public function GetUsersChildrenRoles()
	{
		$Scope = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_USERS_CREATE, PERMISSIONS_USERS_EDIT));
		return $this->CData->GetUsersChildrenRoles($Scope);
	}






    public function CreateAccountFromEmail()
	{
		ignore_user_abort(true);
		set_time_limit(0);
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_USERS_CREATE);
		$Errors = array();
        $Emails = $this->CData->GetAllEmailsForFacilities(16);

		foreach($Emails as $v)
		{
			$Email = mb_strtolower($v["email"]);
			$Email = Strings::ValidateEmailOne($Email);
			try
			{
				$Account = $this->CData->CreateAccountFromEmail(5, $Email, $this->CData->GetNewDealer($v["partnersDivisionId"]), 35);
				$Hash = sha1($Email.microtime(true));
				$Code = sha1(PASSWORD_PREFIX.md5($Email));
				$this->CData->Restore($Account, $Hash, $Code);

				$Mailer = new MailDriver(false, true);
				$Mailer->From = 'mailer@toyota.ru';
				$Mailer->FromName = 'TOYOTA';
				$Mailer->Subject = 'Учетная запись на contacts.toyota.ru';
				$Link = 'http://service.toyota.ru/__restore_password_/'.$Hash.'?_sC='.sha1($Code."753951").'&_sR='.$Code.'&_sV='.sha1("753951".$Hash);
				$Mailer->MsgHTML('<div>Система упарвления сервисами Toyota автоматически создала для Вас учетную запись на <a href="http://contacts.toyota.ru"><b>contacts.toyota.ru</b></a> поскольку адрес вашей электронной почты указан в качестве получателя информации о заявках на оценку автомобилей с пробегом.<br />
Имя учетной записи (Логин): <b>'.$Email.'</b><br />
Для установки пароля к учетной записи, пожалуйста, перейдите ссылке:<br /><a href="'.$Link.'" target="_blank">'.$Link.'</a><br /><br />
Для входа в систему используйте адрес: <a href="http://contacts.toyota.ru/admin/"><b>http://contacts.toyota.ru/admin/</b></a>.<br /><br />
Если у Вас возникли вопросы, обратитесь в службу технической поддержки.</div>');
				$Mailer->AddAddress($Email);
				$Mailer->Send();
			}
			catch(dmtException $e)
			{
				$Errors[] = $v;
			}
		}
$this->Dump(__METHOD__.": ".__LINE__, "Созданы учетные записи для ответственных за заявки на оценку автомобилей.", $Errors);
	}
}