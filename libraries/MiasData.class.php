<?php
class MiasData extends Data
{
    public function AddMember($UserFirstname, $UserLastname, $UserDealerId, $NewEngineId, $Dealer, $Gender, $FirstName, $LastName, $CityId, $Age,
                                $Phone, $Email, $OldBrand, $OldCarId, $News, $Catalog, $CatalogCars, $Price, $PriceCars, $Test, $Order, $Callme, $SourceType)
    {
        $this->Exec("INSERT INTO `lexus_references`.`mias_members`
	(`partners_division_id`,
	`model_id`,
	`first_name`,
	`last_name`,
	`gender`,
	`age`,
	`email`,
	`phone`,
	`register`,
	`current_model_id`,
	`city_id`,
	`news`,
	`testdrive`,
	`price`,
	`catalog`,
	`order`,
	`call`,
	`price_cars`,
	`catalog_cars`,
	`source_type`)
VALUES
	(".$this->CheckNull($Dealer).",
    ".$this->CheckNull($NewEngineId).",
    ".$this->Esc($FirstName).",
    ".$this->Esc($LastName).",
    ".$this->CheckNull($Gender).",
    ".$this->CheckNull($Age).",
    ".$this->Esc($Email).",
    ".$this->Esc($Phone).",
    NOW(),
    ".$this->CheckNull($OldCarId).",
    ".$this->CheckNull($CityId).",
    ".$this->CheckNull($News).",
    ".$this->CheckNull($Test).",
    ".$this->CheckNull($Price).",
    ".$this->CheckNull($Catalog).",
    ".$this->CheckNull($Order).",
    ".$this->CheckNull($Callme).",
    ".$this->Esc(implode(", ", $PriceCars)).",
	".$this->Esc(implode(", ", $CatalogCars)).",
    ".$this->CheckNull($SourceType).");");
    }

    public  function AddQuestions($Questions)
    {
        $this->Begin();
        try
        {
            $this->Exec("INSERT INTO `lexus_references`.`mias_questions`
	(`create`)
VALUES (NOW());");
            $Record = $this->DB->GetLastID();
            foreach($Questions as $v)
                $this->Exec("INSERT INTO `lexus_references`.`mias_questions_data`
	(`record_id`,
    `question_hash`,
	`question`,
	`answer`,
	`comment`)
VALUES
	(".$Record.",
    ".$this->Esc(md5($v["question"])).",
    ".$this->Esc($v["question"]).",
    ".$this->Esc($v["answer"]).",
    ".$this->Esc($v["comment"]).");");
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }

    public  function AddQuest($Car, $Category, $Text)
    {
        $this->Exec("INSERT INTO `lexus_references`.`mias_polling`
	(`category`,
	`model`,
	`text`,
	`create`)
VALUES
	(".$this->Esc($Category).",
    ".$this->Esc($Car).",
    ".$this->Esc($Text).",
    NOW());");
    }


	public function GetMembersForReport($RegisterStart, $RegisterEnd)
    {
        $Query = "SELECT
    a.`first_name`,
    a.`last_name`,
    IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
    a.`age`,
    a.`email`,
    a.`phone`,
    a.`register`,
    a.`news`,
    a.`testdrive`,
    a.`price`,
    a.`catalog`,
    a.`order`,
    a.`call`,
    a.`price_cars`,
    a.`catalog_cars`,
    a.`source_type`,
    REPLACE(b.`name`, '&ndash;', '-'),
    c.`name`,
    d.`name`,
    e.`name`
FROM `lexus_references`.`mias_members` AS a
LEFT JOIN `lexus_references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
LEFT JOIN `lexus_references`.`submodels` AS c ON c.`submodel_id`=a.`model_id`
LEFT JOIN `references`.`cars_wm` AS d ON d.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`cities` AS e ON e.`city_id`=a.`city_id`";

        $Where = array();
        if($RegisterStart)
            $Where[] = "DATE(a.`register`) >= ".$this->Esc($RegisterStart);
        if($RegisterEnd)
            $Where[] = "DATE(a.`register`) <= ".$this->Esc($RegisterEnd);
        return $this->Get($Query.$this->PrepareWhere($Where));
    }

	public function GetMembersTDForReport($RegisterStart, $RegisterEnd)
    {
        $Query = "SELECT
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName,
	a.`email` AS email,
	IF(a.`phone` IS NOT NULL AND a.`phone`<>'', CONCAT('+7 ', a.`phone`), '-') AS phone,
	a.`advanced_flag`,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS registred
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`old_id`=a.`partners_division_id`";

        $Where = array();
        $Where[] = "a.`custom_actions_id`=1";
        $Where[] = "a.`form_type_id`=1";
		$Where[] = "a.`advanced_flag`>=20";
        if($RegisterStart)
            $Where[] = "DATE(a.`register`) >= ".$this->Esc($RegisterStart);
        if($RegisterEnd)
            $Where[] = "DATE(a.`register`) <= ".$this->Esc($RegisterEnd);
        return $this->Get($Query.$this->PrepareWhere($Where));
    }
}