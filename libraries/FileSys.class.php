<?php
define("FILESYS_MODE_READ",					"r");
define("FILESYS_MODE_WRITE",				"w");
define("FILESYS_MODE_READ_WRITE",			"w+");
define("FILESYS_MODE_APPEND",				"a");


define("FILESYS_TYPE_TEXT",					"t");
define("FILESYS_TYPE_BINARY",				"b");

class FileSys extends Processor
{
	private $Getting = array("Current");

	private $File;

	private $Current;

	static private $Mime = array(
	    'text/plain'										=> 'txt',
	    'text/html'											=> 'html',
	    'text/css'											=> 'css',
	    'application/javascript'							=> 'js',
	    'application/json'									=> 'json',
	    'application/xml'									=> 'xml',
	    'application/x-shockwave-flash'						=> 'swf',
	    'video/x-flv'										=> 'flv',
		'video/mp4'											=> 'mp4',
		'audio/mpeg'										=> 'mp3',

	    // images
	    'image/png'											=> 'png',
	    'image/jpeg'										=> 'jpg',
	    'image/gif'											=> 'gif',
	    'image/bmp'											=> 'bmp',
		'image/vnd.microsoft.icon'							=> 'ico',
		'image/tiff'										=> 'tiff',
		'image/tiff'										=> 'tif',
		'image/svg+xml'										=> 'svg',
		'image/svg+xml'										=> 'svgz',
		// archives
		'application/zip'									=> 'zip',
		'application/x-rar-compressed'						=> 'rar',
		'application/x-msdownload'							=> 'exe',
		'application/x-msdownload'							=> 'msi',
		'application/vnd.ms-cab-compressed'					=> 'cab',
		// adobe
		'application/pdf'									=> 'pdf',
		'image/vnd.adobe.photoshop'							=> 'psd',
		'application/postscript'							=> 'ai',
		'application/postscript'							=> 'eps',
		'application/postscript'							=> 'ps',
		// ms office
		'application/msword'								=> 'doc',
		'application/rtf'									=> 'rtf',
		'application/vnd.ms-excel'							=> 'xls',
		'application/vnd.ms-powerpoint'						=> 'ppt',
		// open office
		'application/vnd.oasis.opendocument.text'			=> 'odt',
		'application/vnd.oasis.opendocument.spreadsheet'	=> 'ods',
	);

	public static function CreateFile($FileName, $Ignore = null)
	{
		if(file_exists($FileName))
		{
			if(!$Ignore)
				throw new dmtException("File is exists");
		}
		$F = fopen($FileName, "w");
		fclose($F);
	}

	public function Open($FileName, $Mode = null, $Type = null)
	{
		if(!$Mode) $Mode = FILESYS_MODE_READ;
		if(!$Type) $Type = FILESYS_TYPE_TEXT;
		$this->File = fopen($FileName, $Type.$Mode);
		if(!$this->File)
		{
			if(!file_exists($FileName))
				throw new dmtException("File is not exists", 1001);
			throw new dmtException("Failed to open file", 1001);
		}
	}

	public function NextString($Unserialize = null)
	{
		if($this->File)
			$this->Current = fgets($this->File);
		if($Unserialize)
			$this->Current = unserialize($this->Current);
		return $this->Current === false;
	}

	public function Write($Data)
	{
		if(is_array($Data))
			$Data = serialize($Data);
		fwrite($this->File, $Data);
	}

        /**
         *
         * @param string $BaseFileName
         * @param string $BasePath
         * @param string $Prefix
         * @param string $Postfix
         */
	public static function DeleteGroupFiles($BaseFileName, $BasePath = null, $Prefix = null, $Postfix = null)
	{
		$Flag = null;
		if(!is_array($BaseFileName))
			$BaseFileName = array($BaseFileName);
		if($Prefix === null)
			$Prefix = "";
		elseif(is_array($Prefix))
		{
			$Prefix = "{" + implode(",", $Prefix) + "}";
			$Flag = GLOB_BRACE;
		}
		if($Postfix === null)
			$Postfix = "";
		elseif(is_array($Postfix))
		{
			$Postfix = "{" + implode(",", $Prefix) + "}";
			$Flag = GLOB_BRACE;
		}
		foreach($BaseFileName as $RFile)
		{
			$Files = glob($BasePath.$Prefix.$RFile.$Postfix, $Flag);
			if($Files) array_map("unlink", $Files);
		}
	}

    public static function GetMIME($File)
    {
    	$R = false;
    	if(function_exists("finfo_open"))
    	{
	    	$FI = finfo_open(FILEINFO_MIME);
			if(!$FI) throw new dmtException("File is error");
			$R = finfo_file($FI, $File);
			if($R === false)
				throw new dmtException("GetMIME is error");
			$MIME = explode(";", $R);
			finfo_close($FI);
			$R = trim($MIME[0]);
    	}
    	return $R;
    }

    static public function CheckMIME($File, $Accept, $Except)
    {
    	//$R = self::GetMIME($File);
GetDebug()->Dump(__METHOD__.": ".__LINE__, $File, $Accept, $Except);
    	$R = array_key_exists($R, self::$Mime) ? self::$Mime[$R] : 'octet-stream';
    	return is_array($Accept) ? (
    				is_array($Except) ?
    					(in_array($R, $Accept) && !in_array($R, $Except) ?
    						$R : false) :
    					(in_array($R, $Accept) ? $R : false)) :
    				(is_array($Except) ?
    					(in_array($R, $Except) ? false : $R) : $R);
    }

	static public function CheckMIME2($Type, $Accept, $Except)
    {
GetDebug()->Dump(__METHOD__.": ".__LINE__, $Type, $Accept, $Except);
    	return is_array($Accept) ? (
    				is_array($Except) ?
    					(in_array($Type, $Accept) && !in_array($Type, $Except) ?
    						$Type : false) :
    					(in_array($Type, $Accept) ? $Type : false)) :
    				(is_array($Except) ?
    					(in_array($Type, $Except) ? false : $Type) : $Type);
    }

    public static function ClearDirectory($Path)
    {
        $Objs = glob($Path."/*");
        if ($Objs)
            foreach($Objs as $v)
                is_dir($v) ? self::DeleteDirectory($v) : unlink($v);
    }

    public static function DeleteDirectory($Dir)
    {
		$Objs = glob($Dir."/*");
		if ($Objs)
			foreach($Objs as $v)
				is_dir($v) ? self::ClearDirectory($v) : unlink($v);
		rmdir($Dir);
    }

	public function __destruct()
	{
		if($this->File) fclose($this->File);
	}


    static public function CopyDir($Source, $Distionation, $Over = null, $ExludePrefix = null)
    {
		if(!is_dir($Distionation))
			mkdir($Distionation);
		$Handle = opendir($Source);
		if($Handle)
		{
			$PxL = $ExludePrefix ? mb_strlen($ExludePrefix) : 0;
			while(false !== ($File = readdir($Handle)))
			{
				if($ExludePrefix)
				{
					if(mb_substr($File, 0, $PxL) == $ExludePrefix)
						continue;
				}
				if($File != '.' && $File != '..')
				{
					$Path = $Source . '/' . $File;
					if(is_file($Path))
					{
						if(!is_file($Distionation . '/' . $File || $Over))
						copy($Path, $Distionation . '/' . $File);
					}
					elseif(is_dir($Path))
					{
						if(!is_dir($Distionation . '/' . $File))
							mkdir($Distionation . '/' . $File);
						self::CopyDir($Path, $Distionation . '/' . $File, $Over);
					}
				}
			}
			closedir($Handle);
		}
    }

	/**
	 *
	 * Возвращает расширение файла из имени файла
	 *
	 * @param string $FileName - полное имя файла
	 * @return string Расширение файла, содержащееся в имени файла.
	 */
	static public function GetExtFile($FileName)
	{
		$R = pathinfo($FileName);
    	return isset($R["extension"]) ? $R["extension"] : null;
	}


	public function __construct($FileName, $Mode = null, $Type = null)
	{
		parent::__construct();
		if($FileName)
			$this->Open($FileName, $Mode, $Type);
	}

    function __get($Name)
    {
    	if(in_array($Name, $this->Getting)) return $this->$Name;
    	else throw new dmtException("Property \"".$Name."\" can not be obtained or not found in class \"".__CLASS__."\"");
    }
}