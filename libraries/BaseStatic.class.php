<?php
class BaseStatic
{
	/**
	 * 
	 * Транслирует протоколирование
	 * @param string $Text текст, для протоколирования
	 */
	static protected function Debug($Text)
	{
		Debug::Init()->Debug($Text);
	}
	
	static protected function Trace()
	{
		Debug::Init()->Trace();
	}
	
	static protected function Dump($Var)
	{
		call_user_func_array(array(Debug::Init(), "Dump"), func_get_args());
	}
	
	/**
	 * 
	 * Возвращает текст протокола
	 * @param bool $ForHTML - флаг, означающий необходимость перевода спецсимволов в html-сущности
	 * @return srting Строка текста протокола
	 */	
	static protected function GetDebug()
	{
		return Debug::Init()->GetInfo();
	}
}