<?php
class User extends Session
{
	protected static $Inst;
	private $Getters = array("IsSessionStarted");

	/**
	 *
	 * Класс данных
	 * @var UserData
	 */
	private $CData;

	protected function SetCurrent()
	{
		$this->CData->SetCurrentAccount($this->GetAccount());
        $Partner = $this->GetPartnerId();
        $this->CData->SetCurrentPartner($Partner ? $Partner : 0);
	}

	public function IsLogged()
	{
		$this->Check();
		return (isset($_SESSION) && isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["IsLogged"]) && $_SESSION[SESSIOIN_SECTION_USERS]["IsLogged"] == 1);
	}

	/**
	 * Возвращает адрес электронной почты пользователя
	 *
	 * @return mixed string адрес электронной почты или false, если адрес определить не удалось
	 */
	public function GetEmail()
	{
		$this->Check();
		return ($this->IsLogged() && isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["Email"])) ? $_SESSION[SESSIOIN_SECTION_USERS]["Email"] : null;
	}

	public function Start()
	{
		$this->Check();
		$this->CData->SetCurrentDateTime();
		$this->SetSessionName(SESSION_NAME_USER);
		$this->SessionStart(null, WS::Init()->GetMobileAppSession());
		//Проверка данных сессий
		if($this->IsLogged())
		{
			$this->CheckConnect();
			$this->SetCurrent();
		}
		else
		{
			if(!isset($_SESSION[SESSIOIN_SECTION_USERS]))
			{
				$this->ResetSessionData();
			}
			else
			{
				$this->CheckConnect();
			}
		}
	}

	protected function CheckConnect()
	{
		if(isset($_SESSION[SESSIOIN_SECTION_CONNECTED]) && isset($_SESSION[SESSIOIN_SECTION_CONNECTED]["IP"]) && ($_SESSION[SESSIOIN_SECTION_CONNECTED]["IP"] != $_SERVER["REMOTE_ADDR"] || $_SESSION[SESSIOIN_SECTION_CONNECTED]["UserAgent"] != $_SERVER["HTTP_USER_AGENT"]))
		{
			$this->End();
		}
	}

	public function ResetSessionData()
	{
		if(!$this->IsSessionStarted) throw new dmtException("Can't modification session data.");
		$_SESSION[SESSIOIN_SECTION_USERS] 					= array();
		$_SESSION[SESSIOIN_SECTION_USERS]						= array();
		$_SESSION[SESSIOIN_SECTION_USERS]["IsLogged"] 			= -1;
		$_SESSION[SESSIOIN_SECTION_USERS]["Type"]				= ACCOUNT_TYPE_GUEST;
		$_SESSION[SESSIOIN_SECTION_USERS]["Account"]			= null;

		$this->CData->SetCurrentAccount(null);
		$_SESSION[SESSIOIN_SECTION_CONNECTED]				= array();
		$_SESSION[SESSIOIN_SECTION_CONNECTED]["IP"]			= $_SERVER['REMOTE_ADDR'];
		$_SESSION[SESSIOIN_SECTION_CONNECTED]["UserAgent"]	= $_SERVER["HTTP_USER_AGENT"];
		$_SESSION["SYSTEMS"]								= array();
	}

	/**
	 * Загружает данные пользователя
	 */
	public function LoadUser($Data = null)
	{
		$this->Check();
		if(!$Data)
		{
			$Data = AccountsDriver::GetUserData($_SESSION["token"]);
			if(isset($Data["depricated"]))
			{
				$this->End();
				WS::Init()->Redirect(URL_BASE_FULL.$_SERVER["REQUEST_URI"]);
			}
		}
		$_SESSION[SESSIOIN_SECTION_USERS]						= array();
		$_SESSION[SESSIOIN_SECTION_USERS]["IsLogged"] 			= 1;
		$_SESSION[SESSIOIN_SECTION_USERS]["Account"]			= $Data["userId"];
		$_SESSION[SESSIOIN_SECTION_USERS]["Type"]				= $Data["userTypeId"];
		$_SESSION[SESSIOIN_SECTION_USERS]["Name"]				= $Data["userFirstName"]." ".$Data["userLastName"];
		$_SESSION[SESSIOIN_SECTION_USERS]["FullName"]			= $Data["userFirstName"].($Data["userMiddleName"] ? " ".$Data["userMiddleName"] : "")." ".$Data["userLastName"];
		$_SESSION[SESSIOIN_SECTION_USERS]["FirstName"]			= $Data["userFirstName"];
		$_SESSION[SESSIOIN_SECTION_USERS]["MiddleName"]			= $Data["userMiddleName"];
		$_SESSION[SESSIOIN_SECTION_USERS]["LastName"]			= $Data["userLastName"];
		$_SESSION[SESSIOIN_SECTION_USERS]["PartnerId"]			= $Data["partnerId"];
		$_SESSION[SESSIOIN_SECTION_USERS]["RegDate"]			= $Data["userCreateDate"];
		$_SESSION[SESSIOIN_SECTION_USERS]["Email"]				= $Data["userEmail"];
		if(!array_key_exists("userPhone", $Data)) {
			$Data = AccountsProcessor::Init()->GetUser($Data["userId"]);
		}
		$_SESSION[SESSIOIN_SECTION_USERS]["Phone"]				= $Data["userPhone"];
		$_SESSION[SESSIOIN_SECTION_USERS]["AdvPhone"]			= $Data["userAdvPhone"];
		$_SESSION[SESSIOIN_SECTION_USERS]["Position"]			= $Data["userPosition"];

		$_SESSION[SESSIOIN_SECTION_CONNECTED] 					= array();
		$_SESSION[SESSIOIN_SECTION_CONNECTED]["IP"] 			= $_SERVER['REMOTE_ADDR'];
		$_SESSION[SESSIOIN_SECTION_CONNECTED]["UserAgent"]		= $_SERVER["HTTP_USER_AGENT"];

		$this->CData->SetCurrentAccount($_SESSION[SESSIOIN_SECTION_USERS]["Account"]);
        $this->CData->SetCurrentPartner($_SESSION[SESSIOIN_SECTION_USERS]["PartnerId"]);
	}

	public function End()
	{
		$this->Destroy();
		$this->Start();
	}

	/**
	 * Возвращает идентификатор пользователя
	 *
	 * @return mixed int идентификатор пользователя или false, если не удалось получить идентификатор
	 */
	public function GetAccount()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["Account"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["Account"] : null;
	}

	public function GetType()
	{
		$this->Check();
		return $_SESSION[SESSIOIN_SECTION_USERS]["Type"];
	}

	public function GetName()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["Name"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["Name"] : "Гость";
	}

	public function GetFirstName()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["FirstName"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["FirstName"] : "Гость";
	}

	public function GetMiddleName()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["MiddleName"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["MiddleName"] : "";
	}

	public function GetLastName()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["LastName"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["LastName"] : "";
	}

	public function GetFullName()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["FullName"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["FullName"] : "";
	}

	public function GetPhone()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["Phone"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["Phone"] : "";
	}

	public function GetAdvancedPhone()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["AdvPhone"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["AdvPhone"] : "";
	}

	public function GetPosition()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["Position"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["Position"] : "";
	}

	public function GetPartnerId()
	{
		$this->Check();
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) && isset($_SESSION[SESSIOIN_SECTION_USERS]["PartnerId"]) ? $_SESSION[SESSIOIN_SECTION_USERS]["PartnerId"] : null;
	}

	public function GetAllData()
	{
		return isset($_SESSION[SESSIOIN_SECTION_USERS]) ? $_SESSION[SESSIOIN_SECTION_USERS]["PartnerId"] : array();
	}

    //Служебные функции
	public function __get($Name)
	{
		$this->Check();
		if(in_array($Name, $this->Getters)) return $this->$Name;
		else throw new dmtException("Property \"".$Name."\" not found in class \"".__CLASS__."\"");
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new UserData();
		$this->Start();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return User
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }
}