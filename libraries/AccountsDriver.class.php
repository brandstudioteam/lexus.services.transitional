<?php
class AccountsDriver extends BaseStatic
{
	static protected function GetResult($Data, $Path)
	{
self::Dump(__METHOD__.": ".__LINE__, "Data", $Data, "Path", $Path);
		$M = new InetDriver();
		$M->Request("prq=".json_encode(array(
								"d" => $Data
							)),
							ACCOUNTS_CONNECTIONS_HOST,
							$Path,
							ACCOUNTS_CONNECTIONS_PORT,
							ACCOUNTS_CONNECTIONS_METHOD,
							ACCOUNTS_CONNECTIONS_PROTOCOL,
							"utf-8",
							ACCOUNTS_CONNECTIONS_HEADER,
							true);
self::Dump(__METHOD__.": ".__LINE__, $M->Body);
		if(!$M->Body)
			throw new dmtException("Empty response body");
		$R = json_decode($M->Body, true);
		if($R["error"])
			throw new dmtException(isset($R["message"]) ? $R["message"] : "", $R["error"]);
self::Dump(__METHOD__.": ".__LINE__, $R["result"]);
		return $R["result"];
	}

	static public function GetAuthToken($Session, $Domain, $RedirectUrl)
	{
		return self::GetResult(array(
							"sessionId" => $Session,
							"domain"	=> $Domain,
							"url"		=> $RedirectUrl
						), ACCOUNTS_CONNECTIONS_PATH_GET_AUTH_TOKEN);
	}

	static public function CheckToken($Token)
	{
		return self::GetResult(array(
									"token" => $Token
								),
								ACCOUNTS_CONNECTIONS_PATH_CHECK_TOKEN);
	}

	static public function GetUserData($Token)
	{
		return self::GetResult(array(
									"token" => $Token
								),
								ACCOUNTS_CONNECTIONS_PATH_GET_USER_DATA);
	}

	static public function Logout($Token)
	{
		try
		{
			self::GetResult(array(
									"token" => $Token
								),
								ACCOUNTS_CONNECTIONS_PATH_EXIT);
		}
		catch(dmtException $e) {}
	}
}