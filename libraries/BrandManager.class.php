<?php
class BrandManager
{
	/**
	 *
	 * @var TDCampaignsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return TDCampaignsProcessor
	 */
	public static function Init($Brand = null)
	{
		if(!self::$Inst) self::$Inst = new self($Brand);
		return self::$Inst;
	}

	protected function __construct($Brand)
	{
        $DB = WS::Init()->DB;
        if($Brand === null)
            $Brand = SITE_CURRENT == SITE_LEXUS ? BRAND_LEXUS : BRAND_TOYOTA;
        if($Brand == BRAND_LEXUS)
            define("BRAND_NAME", "Lexus");
        else define("BRAND_NAME", "Toyota");
        define("BRAND_CURRENT", $Brand);
        $DB->Exec("SET ".DB_VARS_BRAND_CURRENT."=".$Brand);
	}
}