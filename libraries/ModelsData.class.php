<?php
class ModelsData extends Data
{
	public function GetModelName($Model)
	{
		return $this->Count("SELECT
	`name` AS Cnt
FROM `".DBS_REFERENCES."`.`models`
WHERE `model_id`=".$Model.";");
	}

	public function GetModels($Actuality = null)
	{
		return $this->Get("SELECT
	`model_id` AS modelId,
	`name` AS modelName,
	CONCAT(".$this->Esc(BRAND_NAME).", ' ',`name`) AS modelFullName,
	`url_special_page` AS modelUrl,
    `actuality` AS modelActuality
FROM `".DBS_REFERENCES."`.`models`
WHERE `actuality`=".($Actuality ? $this->PrepareValue($Actuality) : 1).";");
	}

	public function GetSubmodel($Model = null, $Actuality = null, $Find = null)
	{
		if(SITE_CURRENT != SITE_LEXUS)
			throw new dmtException("Site error");
		return $this->Get("SELECT
	a.`submodel_id` AS submodelId,
	a.`model_id` AS modelId,
	a.`name` AS submodelName,
	CONCAT(".$this->Esc(BRAND_NAME).", ' ',a.`name`) AS modelFullName,
    a.`actuality` AS submodelActuality,
	a.`engine` AS modelEngine,
	b.`name` AS modelName
FROM `".DBS_REFERENCES."`.`submodels` AS a
LEFT JOIN `".DBS_REFERENCES."`.`models` AS b ON b.`model_id`=a.`model_id`
WHERE ".($Model ? "
    a.`model_id`=".$Model."
        AND " : "")."a.`actuality`".($Actuality ? $this->PrepareValue($Actuality) : "=1").";");
	}

    public function GetSubmodelData($Submodel)
    {
        if(SITE_CURRENT != SITE_LEXUS)
			throw new dmtException("Site error");
		return $this->Get("SELECT
	`submodel_id` AS submodelId,
	`model_id` AS modelId,
	`name` AS submodelName,
    `alias` AS modelAlias,
	CONCAT(".$this->Esc(BRAND_NAME).", ' ',`name`) AS modelFullName,
    `actuality` AS submodelActuality
FROM `".DBS_REFERENCES."`.`submodels`
WHERE `submodel_id`=".$Submodel.";", true);
    }

    public function GetSubmodelName($Submodel)
    {
        if(SITE_CURRENT != SITE_LEXUS)
			throw new dmtException("Site error");
		return $this->Count("SELECT
	CONCAT(".$this->Esc(BRAND_NAME).", ' ',`name`) AS Cnt
FROM `".DBS_REFERENCES."`.`submodels`
WHERE `submodel_id`=".$Submodel.";");
    }

    public function GetSubmodelAlias($Submodel)
    {
        if(SITE_CURRENT != SITE_LEXUS)
			throw new dmtException("Site error");
		return $this->Count("SELECT
	`alias` AS Cnt
FROM `".DBS_REFERENCES."`.`submodels`
WHERE `submodel_id`=".$Submodel.";");
    }

	public function GetModelsNewForImport($Brand)
	{
		return $this->Get("SELECT
	a.`model_id` AS modelId,
	UCASE(a.`name`) AS modelName,
	UCASE(a.`alias`) AS modelAlias
FROM `references`.`models` AS a
WHERE a.`brand_id`=".$Brand.";");
	}

	public function GetModelsNew($Brand, $Facility = null, $Actuality = null, $ModelFamily = null, $Model = null)
	{
		$Query = "SELECT
	a.`model_id` AS modelId,
	a.`brand_id` AS brandId,
	a.`model_family_id` AS modelFamilyId,
	a.`inner_number` AS modelInnNumber,
	a.`name` AS modelName,
	a.`alias` AS modelAlias,
	a.`url_special_page` AS modelPageUrl,
	a.`order` AS modelOrder,
	a.`actuality` AS modelActuality,
	a.`old_id` AS modelOldId,
	a.`engine` AS modelEngine,
	a.`modified` AS modelLastModified
FROM `references`.`models` AS a";
		$Where = array();
		if($Model)
		{
			$Where[] = "`model_id`=".$Model;
		}
		else
		{
			$Where[] = "a.`brand_id`=".$Brand;
			if($Facility)
			{
				$Query .= "
LEFT JOIN `references`.`facilities_models` AS b ON b.`model_id`=a.`model_id`";
				$Where[] = "`facility_id`=".$Facility;
			}
			else $Where[] = "a.`actuality`".$this->PrepareValue($Actuality !== null && !is_empty($Actuality) ? $Actuality : 1);
			if($ModelFamily)
				$Where[] = "a.`model_family_id`".$this->PrepareValue($ModelFamily);
		}
		return $this->Get($Query.$this->PrepareWhere($Where), (boolean) $Model);
	}

	public function GetModelsFamilies($Brand, $Facility = null, $Actuality = null, $ModelFamily = null, $Status = null)
	{
		$Query = "
FROM `references`.`model_families` AS a";
		$Where = array();
		if($ModelFamily)
		{
			$Fields = "SELECT
	a.`model_family_id` AS modelFamilyId,
	a.`brand_id` AS brandId,
	a.`inner_number` AS modelFamilyInnNumber,
	a.`name` AS modelFamilyName,
	a.`alias` AS modelFamilyAlias,
	a.`url_special_page` AS modelFamilyPageUrl,
	a.`order` AS modelFamilyOrder,
	a.`actuality` AS modelFamilyActuality,
	a.`update` AS modelFamilyLastModified";
			$Where[] = "a.`model_family_id`=".$ModelFamily;
		}
		else
		{
			$Where[] = "a.`brand_id`=".$Brand;
			$Where[] = "a.`actuality`".($Actuality ? $this->PrepareValue($Actuality) : "=1");
			if($Facility)
			{
				$Fields = "SELECT
	a.`model_family_id` AS modelFamilyId,
	a.`brand_id` AS brandId,
	a.`inner_number` AS modelFamilyInnNumber,
	a.`name` AS modelFamilyName,
	a.`alias` AS modelFamilyAlias,
	IF(b.`spec_page`, b.`spec_page`, a.`url_special_page`) AS modelFamilyPageUrl,
	IF(b.`order` IS NOT NULL, b.`order`, a.`order`) AS modelFamilyOrder,
	a.`actuality` AS modelFamilyActuality,
	a.`update` AS modelFamilyLastModified,
	CONCAT('".URL_MODELS_IMAGE."', b.`image`) AS modelFamilyImage,
	b.`comment` AS modelFamilyComment";
				$Query .= "
LEFT JOIN `references`.`facilities_models_families` AS b ON b.`model_family_id`=a.`model_family_id`";
				$Where[] = "b.`status`".($Status !== null ? $this->PrepareValue($Status) : "=1");
				$Where[] = "b.`facility_id`=".$Facility;
			}
			else
			{
				$Fields = "SELECT
	a.`model_family_id` AS modelFamilyId,
	a.`brand_id` AS brandId,
	a.`inner_number` AS modelFamilyInnNumber,
	a.`name` AS modelFamilyName,
	a.`alias` AS modelFamilyAlias,
	a.`url_special_page` AS modelFamilyPageUrl,
	a.`order` AS modelFamilyOrder,
	a.`actuality` AS modelFamilyActuality,
	a.`update` AS modelFamilyLastModified";
			}
		}
		
		return $this->Get($Fields.$Query.$this->PrepareWhere($Where), (boolean) $ModelFamily);
	}
	
	/**
	 * Возвращает модели для возможности для форм
	 * @param type $Brand
	 * @param type $Facility
	 * @return type
	 */
	public function GetFacilitiesModel($Brand, $Facility)
	{
		return $this->Get("SELECT 
	`facility_id` AS facilityId,
    `model_id` AS modelId,
    `brand_id` AS brandId,
    `status` AS facilityModelStatus,
    `image` AS facilityModelImage,
    `spec_page` AS facilityModelPage,
    `order` AS facilityModelOrder,
	`comment` AS facilityModelComment,
    `update` AS facilityModelUpdate
FROM `references`.`facilities_models`
WHERE `brand_id`=".$Brand."
	AND `facility_id`=".$Facility."
	AND `status`=1;");
	}

	public function GetOldId($Model)
	{
		return $this->Count("SELECT
	`old_id` AS Cnt
FROM `references`.`models`
WHERE `model_id`=".$Model.";");
	}
	
	
		
	public function GetModelsFamiliesFacilities($Facility, $ModelFamily = null)
	{
		return $this->Get("SELECT
	`facility_id` AS facilityId,
    `model_family_id` AS modelFamilyId,
    `brand_id` AS brandId,
    `status` AS modelFamilyStatus,
    CONCAT('".URL_MODELS_IMAGE."', `image`) AS modelFamilyImage,
    `spec_page` AS modelFamilyPageUrl,
    `order` AS modelFamilyOrder,
    `comment` AS modelFamilyComment,
    `update` AS modelFamilyLastModified
FROM `references`.`facilities_models_families`
WHERE `facility_id`=".$Facility.($ModelFamily ? "
	AND `model_family_id`=".$ModelFamily : "").";", (boolean) $ModelFamily);
	}
	
	public function SaveModelsFamiliesFacilities($Brand, $Facility, $ModelFamily, $Status, $Page, $Order, $Comment, $Image)
	{
		$this->Exec("INSERT INTO `references`.`facilities_models_families`
	(`facility_id`,
	`model_family_id`,
	`brand_id`,
	`status`,
	`image`,
	`spec_page`,
	`order`,
	`comment`,
	`update`)
VALUES
	(".$Facility.",
	".$ModelFamily.",
	".$Brand.",
	".$Status.",
	".$this->Esc($Image).",
	".$this->Esc($Page).",
	".$this->CheckNull($Order).",
	".$this->Esc($Comment).",
	NOW())
ON DUPLICATE KEY UPDATE
	`brand_id`=VALUES(`brand_id`),
	`status`=VALUES(`status`),
	`image`=VALUES(`image`),
	`spec_page`=VALUES(`spec_page`),
	`order`=VALUES(`order`),
	`comment`=VALUES(`comment`),
	`update`=VALUES(`update`);");
	}
	
	/**
	 * Возвращает модели для возможности для системы управления
	 * @param type $Facility
	 * @param type $Model
	 * @return type
	 */
	public function GetModelsFacilities($Facility, $Model = null)
	{
		return $this->Get("SELECT
	`facility_id` AS facilityId,
    `model_id` AS modelId,
    `brand_id` AS brandId,
    `status` AS facilityModelStatus,
    `image` AS facilityModelImage,
    `spec_page` AS facilityModelPage,
    `order` AS facilityModelOrder,
	`comment` AS facilityModelComment,
    `update` AS facilityModelUpdate			
FROM `references`.`facilities_models`
WHERE `facility_id`=".$Facility.($Model ? "
	AND `model_id`=".$Model : "").";", (boolean) $Model);
	}
	
	public function SaveModelsFacilities($Brand, $Facility, $Model, $Status, $Page, $Order, $Comment, $Image)
	{
		$this->Exec("INSERT INTO `references`.`facilities_models`
	(`facility_id`,
	`model_id`,
	`brand_id`,
	`status`,
	`image`,
	`spec_page`,
	`order`,
	`comment`,
	`update`)
VALUES
	(".$Facility.",
	".$Model.",
	".$Brand.",
	".$Status.",
	".$this->Esc($Image).",
	".$this->Esc("$Page").",
	".$this->CheckNull($Order).",
	".$this->Esc($Comment).",
	NOW())
ON DUPLICATE KEY UPDATE
	`status`=VALUES(`status`),
	`image`=VALUES(`image`),
	`spec_page`=VALUES(`spec_page`),
	`order`=VALUES(`order`),
	`comment`=VALUES(`comment`),
	`update`=VALUES(`update`);");
	}
}