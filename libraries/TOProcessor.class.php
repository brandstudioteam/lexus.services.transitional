<?php
define("SERVICES_TOSERVICE_ID",                         23);
define("TOSERVICE_FACILITY_ID",                         17);
if(!defined("BRAND_NAME"))
    define("BRAND_NAME",                                    WS::Init()->GetBrandId() == 2 ? "Lexus" : "Toyota");
if(!defined("TO_DEALERS_ACTIONS_FILE_PATH"))
    define("TO_DEALERS_ACTIONS_FILE_PATH",                  PATH_DOCROOT."to_dealers_actions_files/");

class TOProcessor extends Processor
{
	/**
	 *
	 * @var TOProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var TOData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return TOProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new TOData();
	}


    public function GetBrand()
    {
        return $this->CData->GetBrand();
    }

    public function GetModels($Brand = null)
    {
        return $this->CData->GetModels($Brand);
    }

    public function GetModelsFullName($Brand = null)
    {
        return $this->CData->GetModelsFullName($Brand);
    }




    public function GetServicesType($Dealer = null)
    {
        return $this->CData->GetServicesType($Dealer, SITE_CURRENT == SITE_LEXUS ? 2 : 1);
    }

    public function GetUserActions()
    {
        return $this->CData->GetUserActions();
    }

    public function GetUserActions2($Dealer)
    {
        return $this->CData->GetUserActions2($Dealer);
    }


    public function GetUserActions3($Dealer = null)
    {
        $Dealer = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_EMAILS_MANAGEMENT, $Dealer);
        return $this->CData->GetUserActions3($Dealer);
    }

    public function GetUserActionsFromDealer()
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT);
        return $this->CData->GetUserActions3($Scope);
    }

    public function AddUserActions($Dealer, $Name, $Description)
    {
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT, $Dealer);
        $UserAction = $this->CData->AddUserActions($Dealer ? $Dealer : User::Init()->GetPartnerId(), $Name, $Description);
        return $this->CData->GetUserActionOne($UserAction);
    }

    public function SaveUserActions($UserAction, $Name, $Description)
    {
        $Dealer = $this->CData->GetDealerForUserAction($UserAction);
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT, $Dealer);
        $this->CData->SaveUserActions($UserAction, $Name, $Description);
        return $this->CData->GetUserActionOne($UserAction);
    }

    public function DeleteUserActions($UserAction)
    {
        $Dealer = $this->CData->GetDealerForUserAction($UserAction);
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT, $Dealer);
        return array("deleted"  =>  $this->CData->DeleteUserActions($UserAction));
    }






    public function GetModelsByVIN($VIN)
    {
        $R = $this->CData->GetModelsByVIN(mb_substr($VIN, 0, 3), mb_substr($VIN, 3, 6), mb_substr($VIN, 3, 5));
        if(!$R)
            throw new dmtException("Model is not found for VIN='".$VIN."'", 1);
        return $R;
    }




    public function SaveMember($FName, $MName, $LName, $Email, $Phone, $VisitDate, $VisitTime, $UserAction, $Model, $Year, $VIN, $Mileage, $Number, $TOType, $Dealer, $IsOwner, $Info, $Type, $NewDealer)
    {
        if(!$NewDealer)
			$NewDealer = $this->CData->GetNewDealer($Dealer);
        if(!$Type)
            $Type = 1;

        $UserActionName = $this->CData->GetUserActionName($UserAction);

        $TOCampaignsIds = null;
        $TOCampaignsNames = null;
        $TOCampaignsDesc = null;

		$TOCampaigns = $this->CheckVIN($VIN);
		if($TOCampaigns && sizeof($TOCampaigns))
		{
			$TOCampaignsIds = array();
			$TOCampaignsNames = array();
			$TOCampaignsDesc = array();
			foreach($TOCampaigns as $v)
			{
				$TOCampaignsIds[] = $v["toCampaignId"];
				$TOCampaignsNames[] = $v["toCampaignName"];
				$TOCampaignsDesc[] = $v["toCampaignDescription"];
			}
		}

        $DealerActions = $this->CData->CheckDealersAction($NewDealer, $Mileage, $Year, $Model);
        $Detail = $this->CData->SaveMember($FName, $MName, $LName, $Email, $Phone, $VisitDate, $VisitTime, $UserAction, $Model, $Year, $VIN, $Mileage, $Number, $TOType, WS::Init()->GetBrandId(), $NewDealer, $IsOwner, $Info, $UserActionName, $TOCampaignsIds, $Type, $NewDealer, WS::Init()->GetOuterURL(), WS::Init()->IsTest());

		$Mailer = new MailDriver(false, true);
		if(WS::Init()->GetBrandId() == 2)
		{
			$Mailer->From =  "mailer@lexus.ru";
			$Mailer->FromName = "TOYOTA";
			$Mailer->Subject = "LEXUS Запись на техобслуживание";
		}
		else
		{
			$Mailer->From =  "mailer@toyota.ru";
			$Mailer->FromName = "TOYOTA";
			$Mailer->Subject = "TOYOTA Запись на техобслуживание";
		}

        $PhoneInfo = UtilsProcessor::Init()->GetPhoneInfo($Phone);
$this->Dump(__METHOD__.": ".__LINE__, "-------------------- PHONE ---------------------", $PhoneInfo);
		/*
		if($PhoneInfo["error"] == 0)
		{
			$Phone = $PhoneInfo["phone"];
			$Code = $PhoneInfo["code"];
		}
		else $Code = "";
		*/

		$Code = "";


        $ModelName = $this->CData->GetModelName($Model);
        $TOType = $this->CData->GetServiceTypeName($TOType);
        if($VIN)
        {
            $WDI = mb_substr($VIN, 0, 3);
            $VDS = mb_substr($VIN, 3, 5);
            $ModelData = $this->CData->GetModelDataByVIN($WDI, $VDS);
        }
        else $ModelData = null;
        $DeaslerInfo = DealersProcessor::Init()->GetDealersListFull($Dealer);
        $DeaslerInfo = $DeaslerInfo[0];
        $DealerName = mb_str_replace("&ndash;", "—", $DeaslerInfo["partnerName"]);
        $VisitDate = DTs::ConvertDate($VisitDate);
        $Message = "Имя: ".$FName."
Отчество: ".$MName."
Фамилия: ".$LName."
E-Mail: ".$Email."
Телефон: ".$Code." ".$Phone."
Модель: ".$ModelName."
VIN: ".$VIN."
Код модели: ".($ModelData ? $ModelData["modelcode"] : "-")."
Тип двигателя: ".($ModelData ? $ModelData["engine"] : "-")."
Объем двигателя: ".($ModelData ? $ModelData["engVolume"] : "-")."
Трансмиссия: ".($ModelData ? $ModelData["transmission"] : "-")."
Год выпуска: ".$Year."
Пробег: ".$Mileage."
Номер государственной регистрации: ".$Number."
Тип обслуживания: ".$TOType."
Желаемая дата визита: ".$VisitDate."
Желаемое время визита: ".$VisitTime."
Во время прохождения обслуживания: ".$UserActionName."
Дополнительная информация: ".$Info."
Дилерский центр: ".$DealerName."
";
        if($TOCampaignsDesc)
        {
            $Message .= "


ВНИМАНИЕ! автомобиль VIN ".$VIN." возможно попадает под проводимые сейчас Специальные Сервисные Кампании:
";
            foreach($TOCampaignsDesc as $v)
                $Message .= $v."
";
            $Message .= "
Уточните необходимость выполнения кампании и согласуйте с клиентом дополнительное время для проведения необходимых работ.";
        }

        if($DealerActions)
        {
            $Message .= "


Пользователь попадает под условия проведения акций:
";
            foreach($DealerActions as $v)
                $Message .= $v["dealersActionName"]." (файл \"".$v["dealersActionFileName"]."\")
";
        }

        $Mailer->Body = $Message;

        if(!WS::Init()->IsTest())
        {
            $Mailer->AddBCC("formresult@bstd.ru");
        }
        else {
            $Mailer->AddAddress("ib@bstd.ru");
            $Mailer->AddAddress("mr@bstd.ru");
        }

        if(!WS::Init()->IsTest())
        {
            $Emails = DealersProcessor::Init()->GetEmailsForFacilitiesNew($NewDealer, TOSERVICE_FACILITY_ID); //$this->CData->GetEmailsForFacilities($Dealer, TOSERVICE_FACILITY_ID);//
            foreach ($Emails as $v)
            {
                $Mailer->AddAddress($v["email"]);
            }
        }

        try
        {
            $Mailer->Send();
        }
        catch(Exception $e)
        {
            throw new dmtException("Send e-mail to dealers error", 2);
        }

        if($Email)
        {
            $Mailer = new MailDriver(false, true);
			if(WS::Init()->GetBrandId() == 2)
			{
				$Mailer->From = "mailer@lexus.ru";
				$Mailer->FromName = "LEXUS";
				$Mailer->Subject = "LEXUS Запись на техобслуживание";
			}
			else
			{
				$Mailer->From = "mailer@toyota.ru";
				$Mailer->FromName = "TOYOTA";
				$Mailer->Subject = "TOYOTA Запись на техобслуживание";
			}

            $Message = "<h1>Вы подали заявку на техобслуживание</h1>
<p>Ваш дилер: <b>".$DealerName."</b><br />
Адрес дилера: <b>".$DeaslerInfo["partnerAddress"]."</b><br />
Телефон дилера: <b>".$DeaslerInfo["partnerPhone"]."</b><br />
Модель: <b>".$ModelName."</b><br />
Год выпуска: <b>".$Year."</b><br />
Пробег: <b>".$Mileage."</b><br />
Номер государственной регистрации: <b>".$Number."</b><br />
Тип обслуживания: <b>".$TOType."</b><br />
Желаемая дата визита: <b>".$VisitDate."</b><br />
Желаемое время визита: <b>".$VisitTime."</b><br />
Во время прохождения обслуживания: <b>".$UserActionName."</b></p>";

            if($DealerActions)
            {
                $Message .= "<h3>Специально для Вас мы подготовили предложение, подробная информация содержится в приложении к письму.</h3>";
                foreach($DealerActions as $v)
                {
                    $Mailer->AddAttachment(TO_DEALERS_ACTIONS_FILE_PATH.$v["dealersActionFile"], $v["dealersActionFileName"], "base64", Tools::GetMime($v["dealersActionFileMime"]));//Tools::GetMime($v["dealersActionFileMime"])
                }
            }

            if($TOCampaignsNames)
            {
                $Message .= "<div style=\"border-bottom: 1px solid #DDDDDD;\"></div><h3>Уважаемый владелец автомобиля ".(BRAND_NAME)."!</h3>
<br /><br />Позвольте обратить Ваше внимание на то, что на Вашем автомобиле VIN ".$VIN." возможно требуется проведение Специальной сервисной кампании:<ul>";
                foreach($TOCampaigns as $v)
                    $Message .= "<li>".$v["toCampaignDescription"]."<br />".$v["toCampaignName"]."</li>";
                $Message .="</ul><p>Сотрудники дилерского центра уточнят необходимость выполнения кампании на Вашем автомобиле и согласуют с Вами дату и время визита для проведения работ.</p>";
            }

            $Mailer->MsgHTML($Message);
            $Mailer->AddAddress($Email);
            try
            {
                $Mailer->Send();
            }
            catch(Exception $e)
            {
                throw new dmtException("Send e-mail to dealers error", 2);
            }
        }

        $this->AddEvent(SERVICES_TOSERVICE_ID, 1, 10, $Detail, $NewDealer);
	}

	public function DeleteMember($Member)
	{
		$Data = $this->CData->GetMember($Member);
		PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_MEMBERS_DELETE, $Data["partnerId"]);
		$this->CData->DeleteMember($Member);
		return array("memberId" => $Member);
	}


	public function GetMembers($Dealer = null)
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT, $Dealer);
        return $this->CData->GetMembers($Scope, SITE_CURRENT == SITE_LEXUS ? 2 : 1);
    }


    public function ChangeStatus($Member, $Status, $Comment)
    {
        $R = array();
        $Data = $this->CData->GetMember($Member);
        $Code = $Data["email"] ? UsersPollingProcessor::Init()->CreateCode($Member) : null;
        if($Comment)
            $R["comment"] = CommentsProcessor::Init()->CreateComment(ESS_TO_MEMBERS, $Member, $Comment);
        $this->CData->ChangeStatus($Member, WS::Init()->GetBrandId(), $Status, $Code, $_SERVER["REMOTE_ADDR"], $Comment ? $R["comment"]["commentId"] : null);
		$R["member"] = $this->CData->GetMember($Member);
        if($Data["email"])
            UsersPollingProcessor::Init()->SendPolling($Member, '<h3>Уважаемый(-ая) '.$Data["firstName"].($Data["middleName"] ? " ".$Data["middleName"] : "").'!</h3><br />
Вы направляли онлайн заявку для записи на техническое обслуживание автомобиля в '.$Data["partnerName"].'.<br />
Мы благодарны за Ваш выбор и будем признательны, если Вы уделите время, чтобы ответить на 1 вопрос.<br />
Пожалуйста, сообщите нам о результате Вашего обращения, ответив на вопрос по ссылке:<br />
<a href="http://contacts.toyota.ru/z/techservice/polling/'.$Code.'">http://contacts.toyota.ru/z/techservice/polling/'.$Code.'</a><br /><br />
С уважением, ООО «Тойота Мотор»', $Data["email"], $Code);
        $Data = $this->CData->GetMember($Member);
        $this->AddEvent(SERVICES_TOSERVICE_ID, 2, 10, $Data, $Data["partnerId"], User::Init()->GetAccount(), WS::Init()->ClientUID);
        return $R;
    }

    public function GetStatuses()
    {
        return $this->CData->GetStatuses();
    }

    public function GetMembersCSV($Dealer, $Model = null, $RegisterStart = null, $RegisterEnd = null, $ViziteStart = null, $ViziteEnd = null)
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT, $Dealer);
        $R = $this->CData->GetMembersForReport(WS::Init()->GetBrandId(), $Scope, $Model, $RegisterStart, $RegisterEnd, $ViziteStart, $ViziteEnd);
        array_unshift(  $R,
                        array(
							"Дилерский центр",
                            "Фамилия",
                            "Имя",
                            "Отчество",
                            "Электронная почта",
                            "Телефон",
                            "Дата отправки заявки",
                            "Желаемая дата визита",
                            "Желаемое время визита",
                            "Модель автомобиля",
                            "Год выпуска",
                            "VIN",
                            "Пробег",
							"Номер государственной регистрации ТС",
                            "Во время прохождения ТО",
                            "Тип обслуживания",
							//"Дополнительная информация",
                            "Статус заявки",
                            "Комментарий к изменению статуса заявки",
                            "Дата изменения статуса заявки",
                            "Представитель дилера, изменивший статус заявки",
                            "IP-адрес представителся дилера, изменившего статус заявки",
                            "Ответ пользователя",
                            "Комментарий пользователя",
                            "Дата ответа пользователя",
                            "Заявка получена с сайта дилера",
							"Получено с сайта"
                        ));
        return $R;
    }


    public function CheckVIN($VIN)
    {
        return $this->CData->CheckVIN($VIN);
    }




    public function AddDealersAction($Name, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models, $File)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT);
        $Action = $this->CData->AddDealersAction($Name, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models, $File[0]["name"], $File[0]["originalName"], $File[0]["mime"]);
        return $this->CData->GetDealersAction($Action);
    }

    public function SaveDealersAction($Action, $Status = null, $Name = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null, $File = null)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT);
        if($File)
            $this->CData->SaveDealersAction($Action, $Status, $Name, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models, $File["name"], $File["originalName"], $File["mime"]);
        else $this->CData->SaveDealersAction($Action, $Status, $Name, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models);
        return $this->CData->GetDealersAction($Action);
    }

    public function GetDealersActionsList($Dealers = null, $Status = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null)
    {
        $Dealers = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT, PERMISSIONS_TO_DEALERS_ACTIONS_READY), $Dealers);
        return $this->CData->GetDealersActionsList(WS::Init()->GetBrandId(), $Dealers, $Status, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models);
    }

    public function GetDealersActionsModels($Dealers = null, $Status = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null)
    {
        $Dealers = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT, PERMISSIONS_TO_DEALERS_ACTIONS_READY), $Dealers);
        $R = $this->CData->GetDealersActionsList(WS::Init()->GetBrandId(), $Dealers, $Status, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models);
        $Actions = $this->PrepareArray($R);
        return $Actions ? $this->CData->GetModelsForDealersActionFull($Actions) : array();
    }

    public function GetDealersActionsListFull($Dealers = null, $Status = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null)
    {
        $Dealers = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT, PERMISSIONS_TO_DEALERS_ACTIONS_READY), $Dealers);
        $R = $this->CData->GetDealersActionsList(WS::Init()->GetBrandId(), $Dealers, $Status, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models);
        $Actions = $this->PrepareArray($R);
        return array(
            "actions"   => $R,
            "models"    => $this->CData->GetModelsForDealersAction($Actions)
        );
    }

    public function ChangeStatusDealersAction($Action, $Status)
    {
        $Dealer = $this->CData->GetDealerForDealersAction($Action);
        PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT), $Dealer);
        $this->CData->ChangeStatusDealersAction($Action, $Status);
    }


    public function SaveEmails($Emails, $Dealer = null)
    {
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_EMAILS_MANAGEMENT, $Dealer);
        $this->CData->SaveEmails($Emails, $Dealer);
    }

    public function AddEmail($Email, $Dealer)
    {
        if(!$Dealer)
            $Dealer = User::Init()->GetPartnerId();
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_EMAILS_MANAGEMENT, $Dealer);
        $this->CData->AddEmail(WS::Init()->GetBrandId(), $Email, $Dealer);
        return $this->CData->GetEmail($Email, $Dealer);
    }

    public function DeleteEmail($Email, $Dealer)
    {
        if(!$Dealer)
            $Dealer = User::Init()->GetPartnerId();
        PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_EMAILS_MANAGEMENT, $Dealer);
        $this->CData->DeleteEmail($Email, $Dealer);
    }

    public function GetEmails($Dealer = null)
    {
        $Dealer = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TO_EMAILS_MANAGEMENT, PERMISSIONS_TO_EMAILS_READY), $Dealer);
        return $this->CData->GetEmails(WS::Init()->GetBrandId(), $Dealer);
    }

    public function GetCampaignForMember($Member)
    {
        return $this->CData->GetCampaignForMember($Member);
    }


    public function GetTOModels()
    {
        return $this->CData->GetTOModels();
    }


    public function PrepareVINs()
	{
		return;
		$this->CData->LoadVINs();
	}
	
	
	public function AddSpatialServiceCampaign($Title, $Description, $File)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_TO_SPATIAL_CAMPAIGN_MANAGEMENT);
		if($File)
		{
			$File = file_get_contents($File);
			$File = iconv(CHARSET_WIN1251, CHARSET."//IGNORE", $File);
			$VINs = array();
			if(preg_match_all('/(\'|")([^\1]+)\1;/i', $File, $Z))
			{
				foreach($Z as $v)
				{
					$VINs[] = $v[2];
				}
			}
		}
		$Campaign = $this->CData->AddSpatialServiceCampaign(WS::Init()->GetBrandId(), $Title, $Description, $VINs);
		return $this->CData->GetSpatialServiceCampaign(WS::Init()->GetBrandId(), $Campaign);
	}
	
	public function GetSpatialServiceCampaign()
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_TO_SPATIAL_CAMPAIGN_MANAGEMENT);
		return $this->CData->GetSpatialServiceCampaign(WS::Init()->GetBrandId());
	}
	
	
	public function GetDealerData($Dealer)
	{
		return array(
			"userActions"			=> $this->CData->GetUserActions2($Dealer),
			"toTypes"				=> $this->CData->GetTOTypes($Dealer),
			"toTypesFacilities"		=> $this->CData->GetTOTypes($Dealer, true),
			"facilities"			=> FacilitiesProcessor::Init()->GetFacilities(null, $Dealer),
			//"dealersFormSets"		=> FacilitiesFormsProcessor::Init()->GetDealersFormSetForOld(17, 1, $Dealer)
		);
	}
	
	public function DeleteMemeber($Member)
	{
		$Data = $this->CData->GetMember($Member);
		PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_EMAILS_MANAGEMENT, $Data["partnerId"]);
		$this->CData->DeleteMemeber($Member);
	}
}