<?php
class DownloadsProcessor extends Processor
{
	/**
	 * 
	 * @var DownloadsProcessor
	 */
	protected static $Inst = false;
	
	
	/**
	 * 
	 * Инициализирует класс
	 * 
	 * @return DownloadsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}
	
	protected function __construct()
	{
		parent::__construct();
	}
	
	
	public function Download($File)
	{
		$F = mb_substr($File, 0, 1) == "/" ? mb_substr($File, 1) : $File;
		if(!in_array(array_shift(explode("/", $F)), array("images", "gallery", "coupons")))
			$this->Forbidden();
$this->Dump($File, $F);
		WS::Init()->FileName = $F;
	}
}