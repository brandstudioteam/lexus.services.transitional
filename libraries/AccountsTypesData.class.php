<?php
Class AccountsTypesData extends Data
{
    public function GetAccountsTypes($Type)
    {
        $Query = "SELECT DISTINCT
	b.`users_type_id` AS userTypeId,
	b.`name` AS userTypeName,
	b.`description` AS userTypeDescription
FROM `references`.`users_types_access` AS a
LEFT JOIN `references`.`users_types` AS b ON b.`users_type_id`=a.`access_users_type_id`
WHERE a.`users_type_id`=".$Type."
ORDER BY b.`name`;";
        $R = $this->Get($Query);
        return $R;
    }

    public function CreateAccountType($Name, $Description = null)
    {
        $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_types`
    (`name`,
    `description`)
VALUES
    (".$this->Esc($Name, true, false, true).",
    ".$this->Esc($Description).");");
        return $this->DB->GetLastID();
    }

    public function SaveAccountType($AccountType, $Name = null, $Description = null)
    {
//``,
        $U = array();
        if($Name)
            $U[] = "`name`=".$this->Exec($Name);
        if($Description !== null)
            $U[] = "`description`=".$this->Exec($Description);
        return sizeof($U) ? $this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`users_types`
SET ".implode(",", $U))."
WHERE `users_type_id`=".$AccountType.";" : 0;
    }

    public function DeleteAccountType($AccountType)
    {
        return $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_types`
WHERE `users_type_id`=".$AccountType.";");
    }

    public function LinkAccountsTypesWithRolesTypes($Links)
    {
        $D = array();
        $A = array();
        foreach($Links as $v)
        {
            switch($v["operation"])
            {
                case 2: //Удаление
                    $D[] = "(`users_type_id`=".$v["userTypeId"]." AND `roles_type_id`=".$v["roleTypeId"].")";
                    break;
                default :
                    $A[] = "(".$v["userTypeId"].", ".$v["roleTypeId"].")";
            }
        }
        if(sizeof($D))
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_types_access`
WHERE ".implode(" OR ", $D));
        if(sizeof($A))
            $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_types_access`
(`users_type_id`,
`roles_type_id`)
VALUES ".implode(", ", $A));
    }


    public function GetAccountsTypesPartnersTypes($Type)
    {
        return $this->Get("SELECT
	b.`users_type_id` AS userTypeId,
	b.`partners_type_id` AS partnerTypeId
FROM `references`.`users_types_access` AS a
LEFT JOIN `references`.`users_types_partners_types` AS b ON b.`users_type_id`=a.`access_users_type_id`
WHERE a.`users_type_id`=".$Type.";");
    }
}