<?php
require_once(PATH_LIBS.'Debug.class.php');
require_once(PATH_LIBS.'dmtErrException.class.php');
require_once(PATH_LIBS.'dmtException.class.php');

Debug::Init()->SetContext();
/**
 * 
 * Возвращает объект Debug. Используется в текущей версии для совместимости
 * @return Debug
 */
function GetDebug() {return Debug::Init();}
/**
 * 
 * Возвращает объект WS. Используется в текущей версии для совместимости 
 */
function &GetWorkSpace() {return WS::Init();}

function ExcError($Number, $Message, $File, $Line)
{
	try {throw new dmtErrException($Message, $Number, $File, $Line);}
	catch (dmtErrException $e) 
	{
		throw new dmtException($Message, 5550, true);
	}
}
set_error_handler('ExcError', E_ALL); // & ~E_NOTICE &~ E_USER_NOTICE | E_STRICT

function NoCatchingException($e)
{
	GetDebug()->Dump("No catching exception ".get_class($e).", file '".$e->getFile()."' in line ".$e->getLine()." with message: '".$e->getMessage()."'"." with code: '".$e->getCode()."'", $e->getTrace());
}

set_exception_handler("NoCatchingException");

function is_empty($Variable)
{
	return (bool) ($Variable === dmtEMPTY); 
} 

function FatalError()
{
    $R = error_get_last();
    if ($R && ($R['type'] == E_ERROR ||
        $R['type'] == E_CORE_ERROR ||
        $R['type'] == E_COMPILE_ERROR ||
        $R['type'] == E_USER_ERROR))
	{
		debug_print_backtrace();
        try {throw new dmtErrException($R['message'], $R['type'], $R['file'], $R['line']);}
		catch (dmtException $e) {};
		return true;
	}
}
register_shutdown_function('FatalError');

function __autoload($ClassName)
{
	if(file_exists(PATH_LIBS.$ClassName.".class.php")) require_once(PATH_LIBS.$ClassName.".class.php");
	//elseif(file_exists(PATH_LIBS.$ClassName.".interface.php"))  require_once(PATH_LIBS.$ClassName.".interface.php");
	//elseif(file_exists(PATH_SERVICES.$ClassName.".class.php")) require_once(PATH_SERVICES.$ClassName.".class.php");
	elseif(file_exists(PATH_LIBS."fpdf") && file_exists(PATH_LIBS."fpdf/".$ClassName.".class.php")) require_once(PATH_LIBS."fpdf/".$ClassName.".class.php");
 	else {
GetDebug()->Dump(__METHOD__.": ".__LINE__, $ClassName);
		
 		if(PHPExcel_Autoloader::Load($ClassName) === false)
 			throw new dmtException("File of class \"".$ClassName.".class.php"."\" is not found.");
 	}
}

function base64_decode_sp($data)
{
	return base64_decode(mb_str_replace(" ", "+", $data));
}

if(!function_exists("mb_str_replace"))
{
	function mb_str_replace($needle, $replacement, $haystack, $CharSet = null)
	{
		if(!$CharSet) $CharSet = "UTF-8";
		if(!is_array($needle)) $needle = array($needle);
		if(!is_array($replacement)) $replacement = array($replacement);
		
		for($j = 0; $j < sizeof($needle); $j++)
		{
			$rep = array_key_exists($j, $replacement) ? $replacement[$j] : $replacement[sizeof($replacement) - 1];
		    $needle_len = mb_strlen($needle[$j], $CharSet);
		    $replacement_len = mb_strlen($rep, $CharSet);
		    $pos = mb_strpos($haystack, $needle[$j], 0, $CharSet);
			 while ($pos !== false)
		    {
		        $haystack = mb_substr($haystack, 0, $pos, $CharSet) . $rep
		                . mb_substr($haystack, $pos + $needle_len, mb_strlen($haystack, $CharSet) - $pos - $needle_len, $CharSet);
				$pos = mb_strpos($haystack, $needle[$j], $pos + $replacement_len, $CharSet);
		    }
		}
	    return $haystack;
	}
}