<?php
class UsersPollingData extends Data
{
    public function AnswerPolling($Code, $Ansver, $Comment)
    {
        $Member = $this->Count("SELECT
	`member_id` AS Cnt
FROM `references`.`to_members`
WHERE `polling_code`=".$this->Esc($Code).";");

        if(!$Member)
            throw new dmtException("Code is error");
        try
        {
            $this->Begin();
            $this->Exec("INSERT INTO `references`.`user_polling_answer`
	(`member_id`,
	`answer_id`,
	`comment`,
	`create`)
VALUES
	(".$Member.",
    ".$Ansver.",
    ".$this->Esc($Comment).",
    NOW());");

            $R = $this->DB->GetLastID();

            $this->Exec("UPDATE `references`.`to_members`
SET `polling_answer_id`=".$R.",
    `polling_code`=''
WHERE `member_id`=".$Member.";");

            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
        return $R;
    }

    public function GetPollings($Member, $Dealers, $DateStart, $DateEnd)
    {
        $Where = array();

        if($Member)
            $Where[] = "`member_id`".$this->PrepareValue($Member);
        else
        {
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealers);
            if($DateStart)
                $Where[] = "m.`create` >=".$this->Esc($DateStart);
            if($DateEnd)
                $Where[] = "m.`create` <=".$this->Esc($DateEnd);
        }

        return $this->Get("SELECT
    a.`member_id` AS memberId,
	a.`partners_division_id` AS partnerId,
    a.`first_name` AS firstName,
    a.`last_name` AS lastName,
    a.`middle_name` AS middleName,
    a.`email` AS email,
    CONCAT('+7', a.`phone`) AS phone,
    a.`register` AS registredU,
    DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i') AS registred,
    a.`visit_date` AS visitDateU,
	DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS visitDate,
    a.`visit_time` AS visitTime,
    a.`user_action_id` AS userActionId,
    a.`model_years` AS year,
    a.`model` AS modelId,
    a.`vin` AS vin,
    a.`mileage` AS mileage,
    a.`reg_number` AS regNumber,
    a.`to_type_id` AS serviceTypeId,
    IF(a.`is_owner_site`=1, 'Да', 'Нет') AS isOwner,
	CONCAT(c.`name`, ' ', b.`name`) AS modelName,
	a.`user_action_name` AS userActionName,
	e.`name` AS serviceTypeName,
	f.`name` AS partnerName,
	m.`polling_answer_id` AS pollingAnswerId,
	m.`answer_id` AS answerId,
	m.`comment` AS answerComment,
	m.`create` AS answerDate,
	p.`name` AS answerName
FROM `references`.`user_polling_answer` AS m
LEFT JOIN `references`.`pollings_answers` AS p ON p.`answer_id`=m.`answer_id`
LEFT JOIN `references`.`to_members` AS a ON a.`member_id`=m.`member_id`
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model`
LEFT JOIN `references`.`tl_brands` AS c ON c.`brand_id`=b.`brand_id`
LEFT JOIN `references`.`to_types` AS e ON e.`to_type_id`=a.`to_type_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS f ON f.`partners_division_id`=a.`partners_division_id`".$this->PrepareWhere($Where));
    }

    public function SendPolling($Member, $Code)
    {
        if($this->Count("SELECT
    `polling_code` IS NOT NULL AS Cnt
FROM `references`.`to_members`
WHERE `member_id`=".$Member.";"))
            throw new dmtException("Already exists");

        return $this->Exec("UPDATE `references`.`to_members`
SET `polling_code`=".$this->Esc($Code)."
WHERE `member_id`=".$Member.";");
    }
}