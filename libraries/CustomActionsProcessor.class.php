<?php
define('TO_EMAIL',												'BatchAx@toyota-motor.ru');

define("ACTIONS_TEST_DRIVES",											1);
define("ACTIONS_MODEL_ORDERS",											2);
define("ACTIONS_MODEL_ORDERS_SPEC",										3);
define("ACTIONS_CAR_WASH",												4);
define("ACTIONS_PROMO",                                                 6);
define("ACTIONS_TEST_DRIVES_SPATIAL",									7);

define("TEST_DRIVES_SERVICE_ID",										11);
define("ORDER_MODEL_SERVICE_ID",										13);
define("RESERVATION_MODEL_SERVICE_ID",									18);

class CustomActionsProcessor extends Processor
{
	/**
	 *
	 * @var CustomActionsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var CustomActionsData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return CustomActionsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new CustomActionsData();
	}


	public function GetSourceInfoList($Action)
	{
		return $this->CData->GetSourceInfoList($Action);
	}


	/**
	 *
	 * Возвращает список городов, в которых есть дилеры, участвующие в заказе
	 * @param integer $Model - Идентификатор модели
	 */
	public function GetCitiesForDealersTD($Model)
	{
		return $this->CData->GetCitiesForDealers(ACTIONS_TEST_DRIVES, $Model);
	}

	/**
	 *
	 * Возвращает список городов, в которых есть дилеры, участвующие в заказе
	 * @param integer $Model - Идентификатор модели
	 */
	public function GetCitiesForDealersOM($Model)
	{
		return $this->CData->GetCitiesForDealers(ACTIONS_MODEL_ORDERS, $Model);
	}

	/**
	 *
	 * Возвращает список городов, в которых есть дилеры, участвующие в заказе
	 * @param integer $Model - Идентификатор модели
	 */
	public function GetCitiesForDealersBM($Model)
	{
		return $this->CData->GetCitiesForDealers(ACTIONS_MODEL_ORDERS_SPEC, $Model);
	}

	/**
	 *
	 * Возвращает список городов, в которых есть дилеры, участвующие в акции
	 * @param integer $Model - Идентификатор модели
	 * @param integer $Action - Идентификатор акции
	 */
	public function GetCitiesForDealers($Action, $Model)
	{
		return $this->CData->GetCitiesForDealers($Action, $Model);
	}


	/**
	 *
	 * Возвращает список дирлеров, участвующие в заказе для заданного города
	 * @param integer $Model - Идентификатор модели
	 * @param integer $City - Идентификатор города
	 */
	public function GetDealersForCityTD($Model, $City)
	{
		return $this->CData->GetDealersForCity(ACTIONS_TEST_DRIVES, $Model, $City);
	}
	/**
	 *
	 * Возвращает список дирлеров, участвующие в заказе для заданного города
	 * @param integer $Model - Идентификатор модели
	 * @param integer $City - Идентификатор города
	 */
	public function GetDealersForCityOM($Model, $City)
	{
		return $this->CData->GetDealersForCity(ACTIONS_MODEL_ORDERS, $Model, $City);
	}
	/**
	 *
	 * Возвращает список дирлеров, участвующие в заказе для заданного города
	 * @param integer $Model - Идентификатор модели
	 * @param integer $City - Идентификатор города
	 */
	public function GetDealersForCityBM($Model, $City)
	{
		return $this->CData->GetDealersForCity(ACTIONS_MODEL_ORDERS_SPEC, $Model, $City);
	}
	/**
	 *
	 * Возвращает список дирлеров, участвующие в акции для заданного города
	 * @param integer $Model - Идентификатор модели
	 * @param integer $City - Идентификатор города
	 * @param integer $Action - Идентификатор акции
	 */
	public function GetDealersForCity($Action, $Model, $City)
	{
		return $this->CData->GetDealersForCity($Action, $Model, $City);
	}



	/**
	 *
	 * Возвращает список моделей, участвующих в заказах
	 */
	public function GetModelsInOrders()
	{

	}

	/**
	 *
	 * Возвращает список моделей, участвующих в акциях
	 */
	public function GetModelsForAction($Action, $Dealer = null, $WitchDealers = null)
	{
		return $this->CData->GetModelsForAction($Action,$Dealer, $WitchDealers);
	}

    public function GetSubmodelsForAction($Action, $Dealer = null, $WitchDealers = null)
    {
        return $this->CData->GetSubmodelsForAction($Action, $Dealer, $WitchDealers);
    }

    /**
	 *
	 * Возвращает список дилеров, участвующих в заказе модели
	 * @param integer $Model - Идентификатор модели
	 */
	public function GetDealersForOrder($Model)
	{

	}

	/**
	 *
	 * Возвращает список дилеров, не участвующих в заказе модели или всех дилеров, которые могут участвовать в заказе моделей
	 * @param integer $Model - Идентификатор модели
	 */
	public function GetDealersNotInOrder($Model)
	{

	}

	/**
	 *
	 * Удаляет модель из списка заказов
	 * @param integer $Model - Идентификатор модели
	 */
	public function DeleteModel($Model)
	{

	}

	/**
	 *
	 * Возвращает список моделей, не участвующих в заказах
	 */
	public function GetModelNotInOrders()
	{

	}

	/**
	 *
	 * Создает заказ
	 * @param integer $Model - Идентификатор модели
	 * @param array $Dealers - Индексный массив идентификаторов дилеров
	 */
	public function CreateOrder($Model, $Dealers)
	{

	}

	/**
	 *
	 * Сохраняет изменения в заказе
	 * @param integer $Model - Идентификатор модели
	 * @param array $Dealers - Индексный массив идентификаторов дилеров
	 */
	public function SaveOrder($Model, $Dealers)
	{

	}

	/**
	 *
	 * Сохраняет изменения дилеров (электронные адреса) для (всех) заказов
	 * @param unknown_type $Dealer
	 * @param unknown_type $Emails
	 */
	public function SaveDealer($Dealer, $Emails)
	{

	}

    public function AddMembersSpatialTD($FormType, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $ContactMethod, $Gender, $Age, $Subscribe = null, $IsOwner = null,
                                $Type = null, $VizitDate = null, $VizitTime = null, $FromDealerSite = null, $FromMobile = null, $ActionId = null)
	{
		return $this->AddMembers( $ActionId ? $ActionId : ACTIONS_TEST_DRIVES_SPATIAL, $FormType, $Model, $Dealer, $LastName, $FirstName, null, $Email, $Phone, $ContactMethod, $Gender,
                                $Age, $Subscribe, $IsOwner, null, $Type, null, null, null, null, $VizitDate, $VizitTime, null, null, null, $FromDealerSite, $FromMobile);
	}

	public function AddMembersTD($Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe = null, $IsOwner = null, $Type = null, $FromDealerSite = null, $FromMobile = null)
	{
                                //($Action, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe = null, $IsOwner = null, $Spec = null, $Color = null, $ContactMethod = null)
		return $this->AddMembers(ACTIONS_TEST_DRIVES, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe, $IsOwner, null, null, null, $Type, $FromDealerSite, $FromMobile);
	}
	public function AddMembersOM($Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe = null, $IsOwner = null, $CurrentModel = null, $FromDealerSite = null, $FromMobile = null, $Type = null, $FormType = null)
	{
		return $this->AddMembers(ACTIONS_MODEL_ORDERS, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe, $IsOwner, null, null, null, $Type, $FromDealerSite, $FromMobile, $CurrentModel, $FormType);
	}

	public function AddMembersBM($Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe = null, $IsOwner = null, $Spec = null, $Color = null, $ContactMethod = null)
	{
		return $this->AddMembers(ACTIONS_MODEL_ORDERS_SPEC, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe, $IsOwner, $Spec, $Color, $ContactMethod);
	}


	/**
	 *
	 * Сохраняет заявку на тест-драйв
	 * @param integer $Model - Идентификатор модели
	 * @param integer $Dealer - Идентификатор дилера
	 * @param string $LastName - Фамилия
	 * @param string $FirstName - Имя
	 * @param string $Email - Адрес электронной почты
	 * @param string $Phone - Телефон
	 * @param integer $Gender - Пол
	 * @param integer $Age - Возраст
	 * @param integer $Subscribe Флаг подписки
	 * @param integer $IsOwner - Флаг владельца тойоты
	 */
	public function AddMembers($Action, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe = null, $IsOwner = null, $Spec = null, $Color = null, $ContactMethod = null, $Type = null, $FromDealerSite = null, $FromMobile = null, $CurrentModel = null, $FormType = null)
	{
        if(!$FormType)
            $FormType = 1;
		$RCode = null;
		try
        {
			$Member = $this->CData->AddMember($Action, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $IsOwner, $Spec ? "Комплектация: ".$Spec."\nЦвет: ".$Color : null, $Type === null ? 1 : $Type, WS::Init()->GetOuterURL(), $FromDealerSite, $FromMobile, $_SERVER["HTTP_USER_AGENT"], $CurrentModel, $FormType, WS::Init()->IsTest());
            $DealerData = DealersProcessor::Init()->GetDealerData($Dealer);
            $RCode = $DealerData["RCode"];
            $Member = $this->CData->GetMembersForEmail($Member);
			if($ContactMethod)
				$Member["contactMethod"] = $ContactMethod;
            if(SITE_CURRENT == SITE_LEXUS)
            {
                $DealerData["partnerName"] = mb_str_replace("&ndash;", "—", $DealerData["partnerName"]);
                $ModelData = ModelsProcessor::Init()->GetSubmodelData($Model);
            }
            else
            {
                $ModelData = ModelsProcessor::Init()->GetModelData($Model);
            }
			if($Spec)
				$ModelData["spec"] = $Spec;
			if($Color)
				$ModelData["color"] = $Color;
            $this->Send($Action, $FormType, $DealerData, $ModelData, $Member);
        }
        catch(dmtException $e)
        {
			$this->Dump("++++++++++++++++", $e->getCode(), $e->getCode() == 50000);
			if($e->getCode() == 50000)
			{
				$ActionName = $this->CData->GetActionName($Action);
				$Mailer = new MailDriver(false, true);
				$Mailer->From = SITE_CURRENT == SITE_LEXUS ? "support@lexus.ru" : "support@toyota.ru";
				$Mailer->FromName = SITE_CURRENT == SITE_LEXUS ? "Lexus" : "Toyota";
				$Mailer->Subject = (SITE_CURRENT == SITE_LEXUS ? "Дубликат записи на Lexus " : "Дубликат записи на Toyota ").$ActionName;
				$Mailer->AddAddress("lexus.support@bstd.ru");
				$Mailer->Body = "Зафиксирована попытка записи дубликата. С формы ".$ActionName.($FormType == 2 ? " Заказ обратного звонка" : "").". Referrer:".WS::Init()->GetOuterURL()."; UserAgent: ".$_SERVER["HTTP_USER_AGENT"]."; SourceFlag: ".$Type;
				$Mailer->Send();
				throw new dmtException("MembersAlreadyExists", 80002, true);
			}
			throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
		if($Subscribe)
		{
			try
			{
				SubscribeDriver::Subscribe($FirstName, $LastName, null, $Gender, $Email, $Age, null, $Dealer, null, $Model);
			}
			catch (dmtException $e)
			{

			}
		}
		return array("dealerRcode" => $RCode);
	}

	public function GetActionInfoTD($Model = null, $Dealer = null)
	{
		return $this->GetActionInfo(ACTIONS_TEST_DRIVES, $Model, $Dealer);
	}
	public function GetActionInfoOM($Model = null, $Dealer = null)
	{
		return $this->GetActionInfo(ACTIONS_MODEL_ORDERS, $Model, $Dealer);
	}
	public function GetActionInfoBM($Model = null, $Dealer = null)
	{
		return $this->GetActionInfo(ACTIONS_MODEL_ORDERS_SPEC, $Model, $Dealer);
	}

    /**
	 *
	 * Возвращает информацию для построения формы заказа
	 * @param integer $Model - Идентификатор модели
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetActionFullInfo($Action, $Dealer = null, $Model = null)
    {
        $Services = array(
            1   => 11,
            2   => 13
        );
        $R = array();
        $R["partners"] = $Dealer ? DealersProcessor::Init()->GetDealersListFull($Dealer) : DealersProcessor::Init()->FindDealers(null, null, null, null, null, null, null, null, null, null, $Action, $Model);
		$R["models"] = $this->CData->GetModelsForAction($Action, null, null, $Services[$Action]);
        $R["cities"] = $Dealer ? array() : GeoProcessor::Init()->GetDealersCities(null, null, null, null, $Action, $Model);
        $R["partnersModels"] = $this->CData->GetDealersModels($Action, $Dealer);
        if(SITE_CURRENT == SITE_LEXUS)
        {
            $R["submodels"] = $this->CData->GetSubmodelsForAction($Action);//, $Dealer
        }
        return $R;
    }

	/**
	 *
	 * Возвращает информацию для построения формы заказа
	 * @param integer $Model - Идентификатор модели
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetActionInfo($Action, $Model = null, $Dealer = null)
	{
		$R = array();

		switch($Action)
		{
			case ACTIONS_TEST_DRIVES:
				if($Model)
				{
					$R["modelId"] = $Model;
					$R["modelName"] = ModelsProcessor::Init()->GetModelName($Model);
					$R["modelCheck"] = 0;
				}
				else
				{
					$R["modelCheck"] = 1;
					$R["modelId"] = 0;
				}
				$ServiceId = TEST_DRIVES_SERVICE_ID;
				if($Dealer)
				{
					if(is_array($Dealer))
					{
						if(sizeof($Dealer) > 1)
						{
							$R["partnerCheck"] = 1;
							$R["partnerId"] = 0;
							$R["partners"] = DealersProcessor::Init()->GetDealers($Dealer);
							$R["models"] = $this->CData->GetModels($Action, $ServiceId);
						}
						else
						{
							$Dealer = $Dealer[0];
							$R["partnerCheck"] = 2;
							$R["partnerId"] = $Dealer;
							$R["partnerName"] = DealersProcessor::Init()->GetDealerName($Dealer);
							$R["models"] = $this->CData->GetModels($Action, $ServiceId, $Dealer);
						}
					}
					else
					{
						$R["partnerCheck"] = 2;
						$R["partnerId"] = $Dealer;
						$R["partnerName"] = DealersProcessor::Init()->GetDealerName($Dealer);
						$R["models"] = $this->CData->GetModels($Action, $ServiceId, $Dealer);
					}
				}
				else
				{
					$R["partnerCheck"] = 3;
					$R["partnerId"] = 0;
					$R["models"] = $this->CData->GetModels($Action, $ServiceId);
				}
				break;
			case ACTIONS_MODEL_ORDERS:
				$ServiceId = ORDER_MODEL_SERVICE_ID;
				if($Dealer)
				{
					if(is_array($Dealer))
					{
						if(sizeof($Dealer) > 1)
						{
							$R["partnerCheck"] = 1;
							$R["partnerId"] = 0;
							$R["partners"] = DealersProcessor::Init()->GetDealers($Dealer);
						}
						else
						{
							$Dealer = $Dealer[0];
							$R["partnerCheck"] = 2;
							$R["partnerId"] = $Dealer;
							$R["partnerName"] = DealersProcessor::Init()->GetDealerName($Dealer);
						}
					}
					else
					{
						$R["partnerCheck"] = 2;
						$R["partnerId"] = $Dealer;
						$R["partnerName"] = DealersProcessor::Init()->GetDealerName($Dealer);
					}
				}
				else
				{
					$R["partnerCheck"] = 3;
					$R["partnerId"] = 0;
					$R["cities"] = $this->CData->GetCitiesForDealers($Action, $Model);
				}
				$R["modelId"] = $Model;
				$R["modelName"] = ModelsProcessor::Init()->GetModelName($Model);
				break;
		}
		return $R;
	}

    public function Send($Action, $FormType, $Dealer, $Model, $Member)
	{
        $Tempaltes = $this->CData->GetTemplates($Action, $FormType);
        $TmplData = array(
            "member"        => $Member,
            "model"         => $Model,
            "dealer"        => $Dealer
        );
        $PhoneInfo = UtilsProcessor::Init()->GetPhoneInfo($TmplData["member"]["phone"]);
		/*
        if($PhoneInfo["error"] == 0)
        {
            $TmplData["member"]["phone"] = $PhoneInfo["phone"];
            $TmplData["member"]["code"] = $PhoneInfo["code"];
        }
        else $TmplData["member"]["code"] = "";
		 */
		$TmplData["member"]["code"] = "";
        if($Tempaltes["dealerSend"])
        {
            $Emails = $this->CData->GetDealerAndUniversalEmail($Action, $FormType, $Dealer["partnerId"], WS::Init()->IsTest());
            $this->SendEmail($Emails,
                            $TmplData,
                            $Tempaltes["dealerMailForm"],
                            $Tempaltes["dealerMailFormName"],
                            $Tempaltes["dealerMailSubject"],
                            $Tempaltes["dealerMailTemplate"],
                            $Tempaltes["dealerAsHTML"],
                            $Tempaltes["dealerCharset"]);
        }
        if($Tempaltes["tmrSend"])
        {
			$Emails = $this->CData->GetUniversalEmail($Action, $FormType, WS::Init()->IsTest(), array(1, 2));
            if($Emails)
	            $this->SendEmail($Emails,
								$TmplData,
								$Tempaltes["tmrMailForm"],
								$Tempaltes["tmrMailFormName"],
								$Tempaltes["tmrMailSubject"],
								$Tempaltes["tmrMailTemplate"],
								$Tempaltes["tmrAsHTML"],
								$Tempaltes["tmrCharset"],
								$Tempaltes["tmrAttachmentMailTemplate"],
								$Tempaltes["tmrAttachmentMailTemplateName"]);
        }
        if($Tempaltes["userSend"] && $Member["email"])
        {
            if(isset($TmplData["member"]["genderId"]))
                $TmplData["member"]["name"] = ($TmplData["member"]["genderId"] ? "Уважаемая " : "Уважаемый ").$TmplData["member"]["firstName"];
            $this->SendEmail($Member["email"],
							$TmplData,
							$Tempaltes["userMailForm"],
							$Tempaltes["userMailFormName"],
							$Tempaltes["userMailSubject"],
							$Tempaltes["userMailTemplate"],
							$Tempaltes["userAsHTML"],
							$Tempaltes["userCharset"]);
        }
	}

    protected function SendEmail($Emails, $Data, $From, $FromName, $Subject, $Template, $AsHTML, $Charset = null, $AttachmentTemplate = null, $AttachmentName = null)
    {
        if(!$Emails)
            throw new dmtException("Email is not found", 5);
        $Mailer = new MailDriver(false, true);
        $Mailer->From = $From;
        $Mailer->FromName = $FromName;
        $Mailer->Subject = Templater2::Init()->Merge(null, $Data, null, null, $Subject);
        if(is_array($Emails))
            foreach($Emails as $v)
			{
				switch($v["sendAs"])
				{
					case 1:
						$Mailer->AddBCC($v["email"]);
						break;
					case 2:
						$Mailer->AddCC($v["email"]);
						break;
					default:
						$Mailer->AddAddress($v["email"]);
				}
			}
        else $Mailer->AddAddress($Emails);
        $Message = Templater2::Init()->Merge(null, $Data, null, null, $Template);
        if($Charset && $Charset != CHARSET)
            $Message    = iconv(CHARSET, $Charset.'//IGNORE', $Message);
        if($AsHTML)
        {
            $Mailer->MsgHTML($Message);
        }
        else $Mailer->Body = $Message;
        $Attachment = null;
        if($AttachmentTemplate)
        {
            $Attachment = Templater2::Init()->Merge(null, $Data, null, null, $AttachmentTemplate);
            if($Charset != CHARSET)
                $Attachment = iconv(CHARSET, $Charset.'//IGNORE', $Attachment);
            $Mailer->AddStringAttachment($Attachment, $AttachmentName ? $AttachmentName : 'file.txt');
        }
        $Mailer->Send();
    }

	public function SendForAllDealers($Type)
	{
		$Mailer = new PHPMailer();
		$Mailer->isSMTP();

		$Mailer->Encoding = "quoted-printable";
		$Mailer->CharSet = 'UTF-8';
		$Mailer->From = 'mailer@toyota.ru';

		if($Type == ACTIONS_TEST_DRIVES)
		{
			$Mailer->FromName = 'LEXUS test drive';
			$Mailer->Subject = 'LEXUS test drive';
		}
		elseif($Type == ACTIONS_MODEL_ORDERS)
		{
			$Mailer->FromName = 'LEXUS';
			$Mailer->Subject = 'Заявка на заказ ';
		}
		elseif($Type == ACTIONS_MODEL_ORDERS_SPEC)
		{
			$Mailer->FromName = 'LEXUS';
			$Mailer->Subject = "Локатор ";
		}
                $Mailer->Body = "Просьба обратным письмом подтвердить получение. Спасибо!";
        if(!WS::Init()->IsTest())
        {
            $Mailer->AddAddress(TO_EMAIL);
            $Emails = $this->CData->GetDealerEmails($Type);
            foreach ($Emails as $v)
            {
                $Mailer->AddBCC($v["email"]);
            }
		}
$Mailer->AddBCC("formresult@bstd.ru");
		$Mailer->Send();
	}

    public function GetMembersCSV($Action, $FormType, $Dealer, $Model = null, $RegisterStart = null, $RegisterEnd = null)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT);
        if($Dealer)
        {
            PartnersProcessor::Init()->CheckPartnersHierarchy($Dealer, true);
            $Scope = $Dealer;
        }
        else
        {
            $Scope = PartnersProcessor::Init()->GetScope();
            if(!$Scope)
                throw new dmtException("Scope access denid", 1005);
        }
        if(!$FormType)
            $FormType = 1;
        $R = $this->CData->GetMembersForReport($Action, $FormType, $Scope != -10 ? $Scope : null, $Model, $RegisterStart, $RegisterEnd);
        switch($Action)
        {
            case ACTIONS_TEST_DRIVES:
                array_unshift(  $R,
                                array(
                                    "Фамилия",
                                    "Имя",
                                    "Пол",
                                    "Возраст",
                                    "Дилерский центр",
                                    "Модель",
                                    "Электронная почта",
                                    "Телефон",
                                    "Заявка получена с сайта дилера",
                                    "Дата отправки заявки",
									"Источник трафика",
									"Идентификатор заявки"
                                ));
                                break;
            case ACTIONS_MODEL_ORDERS:
                if($FormType == 1)
                    array_unshift(  $R,
                                    array(
                                        "Фамилия",
                                        "Имя",
                                        "Пол",
                                        "Возраст",
                                        "Дилерский центр",
                                        "Модель",
                                        "Электронная почта",
                                        "Телефон",
                                        "Заявка получена с сайта дилера",
                                        "Автомобиль в настоящее время",
                                        "Дата отправки заявки"
                                    ));
                else array_unshift(  $R,
                                    array(
                                        "Фамилия",
                                        "Имя",
                                        "Пол",
                                        "Дилерский центр",
                                        "Модель",
                                        "Электронная почта",
                                        "Телефон",
                                        "Заявка получена с сайта дилера",
                                        "Дата отправки заявки"
                                    ));
                break;
            case ACTIONS_MODEL_ORDERS_SPEC:
                array_unshift(  $R,
                                array(
                                    "Фамилия",
                                    "Имя",
                                    "Пол",
                                    "Дилерский центр",
                                    "Модель",
                                    "Электронная почта",
                                    "Телефон",
                                    "Заявка получена с сайта дилера",
                                    "Дата отправки заявки"
                                ));
                break;
        }
        WS::Init()->FileOriginalName = $this->CData->GetActionName($Action)
                                        .($FormType == 2 ? "_Обратный звонок" : "").".csv";
        return $R;
    }

	public function ReSendTestDrives()
	{
		ignore_user_abort(true);
		set_time_limit(0);

		$Tempaltes = $this->CData->GetTemplates(1, 1);
		$Emails = $this->CData->GetUniversalEmail(1, 1, WS::Init()->IsTest(), array(1, 2));
		$R = $this->CData->GetForReSendTestDrives();
		foreach($R as $v)
		{
			$DealerData = DealersProcessor::Init()->GetDealerData($v["partnerId"]);
            if(SITE_CURRENT == SITE_LEXUS)
            {
                $ModelData = ModelsProcessor::Init()->GetSubmodelData($v["modelId"]);
            }
            else
            {
                $ModelData = ModelsProcessor::Init()->GetModelData($v["modelId"]);
            }

			$TmplData = array(
				"member"        => $v,
				"model"         => $ModelData,
				"dealer"        => $DealerData
			);
        	$TmplData["member"]["code"] = "";
			$this->SendEmail($Emails,
							$TmplData,
							$Tempaltes["tmrMailForm"],
							$Tempaltes["tmrMailFormName"],
							$Tempaltes["tmrMailSubject"],
							$Tempaltes["tmrMailTemplate"],
							$Tempaltes["tmrAsHTML"],
							$Tempaltes["tmrCharset"],
							$Tempaltes["tmrAttachmentMailTemplate"],
							$Tempaltes["tmrAttachmentMailTemplateName"]);

		}
	}
	
	public function EventAcceptTest()
	{
		
	}
}