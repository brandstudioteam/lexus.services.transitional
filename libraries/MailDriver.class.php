<?php
class MailDriver extends PHPMailer
{
	public function __construct($exceptions = false, $isDefault = null)
	{
		parent::__construct($exceptions);
		if($isDefault)
		{
			if(MAIL_USE_SMTP)
			{
				$this->isSMTP();
				$this->SMTPAuth = MAIL_SMTP_IS_AUTH;
				$this->Username = MAIL_SMTP_USER;
				$this->Password = MAIL_SMTP_PASSWORD;
				$this->Host = MAIL_SMTP_HOST;
				$this->Port = MAIL_SMTP_PORT;
			}
			$this->Encoding = "quoted-printable";
			$this->CharSet = MAIL_CAHRSET;
		}
	}
}