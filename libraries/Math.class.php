<?php
class Math extends BaseStatic
{
	/**
	 * 
	 * Возвращает наибольший общий делитель
	 * @param integer $Value1 Первое число 
	 * @param integer $Value2 Второе число
	 */
	static public function NOD($Value1, $Value2)
	{
		while($Value1 !== 0 && $Value2 !== 0)
			if($Value1 > $Value2) $Value1 -= $Value2;
			else $Value2 -= $Value1;
		return $Value1 + $Value2;
	}
	
	/**
	 * 
	 * Возвращает разрядность целой части числа
	 * @param number $Value Число для определения разрядности
	 */
	static public function Dec($Value)
	{
		return mb_strlen(floor($Value), mb_internal_encoding());
	}
	
	/**
	 * 
	 * Возвращает разрядность десятичной части числа
	 * @param number $Value
	 */
	static public function DecD($Value)
	{
		$Value -= floor($Value);
		return $Value > 0 ? mb_strlen($Value, mb_internal_encoding()) - 2 : 0;
	}
	
	/**
	 * 
	 * Возвращает результат деления первого числа на второе в виде массива целочисленных значений целой и дробной частей
	 * @param number $Delitet
	 * @param number $Dividend
	 * @return array массив первый элемент которого - значение целой части результата деления, вторая - целочисленное значение дробной.
	 */
	static public function D($Delitet, $Dividend) //division
	{
		$Div = $Delitet / $Dividend;
		$Delitet = floor($Div);
		if($Delitet != $Div)
		{
			$Dividend = $Div - $Delitet;
			$Dividend *= pow(10, self::DecD($Dividend));
		}
		else $Dividend = 0;
		return array($Div, $Dividend);
	}
	
	static public function DONS($SrcLen, $NeedLen)
	{
		if($SrcLen < $NeedLen) return $SrcLen;
		$L0 = $NeedLen;
		while($SrcLen % $L0)
			$L0++;
		$L1 = $NeedLen - 1;
		while($SrcLen % $L1)
			$L1 --;
		return ($L0 - $NeedLen) > ($NeedLen - $L1) ? $L0 : $L1;
	}
	
	static function PrepareFloat($Number)
	{
		if(preg_match('/^(\s*(-?)\s*(\d+)(\s*[,.]{1}\s*(\d*))?\s*)?$/', $Number, $R))
		{
			$Number = floatval($R[2].$R[3].(isset($R[5]) ? ".".$R[5] : ""));
		}
		else $Number = false;
		return $Number;
	}
	
	static function PrepareUnsignedFloat($Number)
	{
		if(preg_match('/^(\s*(\d+)(\s*[,.]{1}\s*(\d*))?\s*)?$/', $Number, $R))
		{
			$Number = floatval($R[2].(isset($R[4]) ? ".".$R[4] : ""));
		}
		else $Number = false;
		return $Number;
	}
	
	static function PrepareSignedFloat($Number)
	{
		if(preg_match('/^(\s*(-{1})\s*(\d+)(\s*[,.]{1}\s*(\d*))?\s*)?$/', $Number, $R))
		{
			$Number = floatval($R[2].$R[3].(isset($R[5]) ? ".".$R[5] : ""));
		}
		else $Number = false;
		return $Number;
	}
	
	
	static function PrepareInteger($Number)
	{
		if(preg_match('~^(\s*(-?)\s*(\d+)\s*){1}$~', $Number, $R))
		{
			$Number = intval($R[2].$R[3]);
		}
		else $Number = false;
		return $Number;
	}
	
	static function PrepareUnsignedInteger($Number)
	{
		if(preg_match('~^(\s*(\d+)\s*){1}$~', $Number, $R))
		{
			$Number = intval($R[2]);
		}
		else $Number = false;
		return $Number;
	}
	
	static function PrepareSignedInteger($Number)
	{
		if(preg_match('~^(\s*(-{1})\s*(\d+)\s*){1}$~', $Number, $R))
		{
			$Number = intval($R[2].$R[3]);
		}
		else $Number = false;
		return $Number;
	}
	
	static public function PrepareColor($Data)
	{
		if(preg_match('/^(\s*((#?)\s*(([\da-f]{3})|([\da-f]{6}))){1}|(\(?\s*(\d|\d{2}|(1[0-9]{2}|(2[0-4]\d)|25[0-5]))\s*[.,]\s*){2}(\d|\d{2}|(1[0-9]{2}|(2[0-4]\d)|25[0-5])){1}\s*\)?\s*)$/ui', $Data, $R))
		{
			if(sizeof($R) < 8)
			{
				$Data = array();
				if(strlen($R[4]) == 3)
				{
					$Data[] = hexdec($R[4]{0}.$R[4]{0});
					$Data[] = hexdec($R[4]{1}.$R[4]{1});
					$Data[] = hexdec($R[4]{2}.$R[4]{2});
				}
				else
				{
					$Data[] = hexdec($R[4]{0}.$R[4]{1});
					$Data[] = hexdec($R[4]{2}.$R[4]{3});
					$Data[] = hexdec($R[4]{4}.$R[4]{5});
				}
			}
			else $Data = array($R[8], $R[7], $R[10]);
		}
		else $Data = false;
		return $Data;
	}
	
	static public function PrepareMileage($Value)
	{
		if(preg_match('/^\d{1,3}(?:[ .,]?\d{3})*$/u', $Value) && $Value >= 100)
			$Value = preg_replace('/[ .,]/', "", $Value);
		else $Value = false;
		return $Value;
	}
	
	static public function ConverToNumeric($Value, $DecSep = null)
	{
		if(preg_match('/^(\d{1,3}(?:[ '.($DecSep == "," ? "." : ",").']?\d{3})+)(?:'.($DecSep == "," ? "," : ".").'(\d+))?$/u', $Value, $Z))
		{
			$Value = preg_replace('/[ .]/', "", $Z[1]);
			if(mb_strlen($Z[2]))
				$Value = floatval($Value.".".$Z[2]);
			else $Value = intval($Value);
		}
		else $Value = false;
		return $Value;
	}
	
	static public function PrepareTradeInOrder($Value)
	{
		if(preg_match('/^(\d{1,3}(?:[ ]?\d{3})+)(?:,\d+)?$/u', $Value, $Z))
			$Value = explode(" ", $Z[1]);
		else $Value = null;
		return $Value;
	}
}