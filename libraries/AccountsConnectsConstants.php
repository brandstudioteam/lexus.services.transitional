<?php
if(SITE_CURRENT == SITE_LEXUS)
{
	if(SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_PRODACTION)
	{
		define("ACCOUNTS_CLIENT_CONNECTIONS_URL",									"http://accounts.lexus.ru/auth/");

		define("ACCOUNTS_CLIENT_CONNECTIONS_URL_MAPS",								"http://accounts.lexus.ru/_auth/");
		
		define("ACCOUNTS_CLIENT_CONNECTIONS_URL_IFRAME",							"http://accounts.lexus.ru/i_auth/");

		define("ACCOUNTS_CONNECTIONS_HOST",											"accounts.lexus.ru");

		define("ACCOUNTS_CONNECTIONS_PORT",											"80");

		define("ACCOUNTS_CONNECTIONS_METHOD",										"POST");

		define("ACCOUNTS_CONNECTIONS_PROTOCOL",										"http:");

		define("URL_ADMIN_SYSTEM",													"http://content.lexus-russia.ru/admin/");
	}
	else
	{
		define("ACCOUNTS_CLIENT_CONNECTIONS_URL",									"http://accounts.lexus.dev.bstd.ru/auth/");

		define("ACCOUNTS_CLIENT_CONNECTIONS_URL_MAPS",								"http://accounts.lexus.dev.bstd.ru/_auth/");
		
		define("ACCOUNTS_CLIENT_CONNECTIONS_URL_IFRAME",							"http://accounts.lexus.dev.bstd.ru/i_auth/");

		define("ACCOUNTS_CONNECTIONS_HOST",											"accounts.lexus.dev.bstd.ru");

		define("ACCOUNTS_CONNECTIONS_PORT",											"80");

		define("ACCOUNTS_CONNECTIONS_METHOD",										"POST");

		define("ACCOUNTS_CONNECTIONS_PROTOCOL",										"http:");

		define("URL_ADMIN_SYSTEM",													"http://current_services.lexus.dev.bstd.ru/admin/");
	}
}
else
{
	if(SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_PRODACTION)
	{
		define("ACCOUNTS_CLIENT_CONNECTIONS_URL",									"http://service.toyota.ru/auth/");

		define("ACCOUNTS_CONNECTIONS_HOST",											"service.toyota.ru");

		define("ACCOUNTS_CONNECTIONS_PORT",											"80");

		define("ACCOUNTS_CONNECTIONS_METHOD",										"POST");

		define("ACCOUNTS_CONNECTIONS_PROTOCOL",										"http:");

		define("URL_ADMIN_SYSTEM",													"http://contacts.toyota.ru/admin/");
	}
	else
	{
		define("ACCOUNTS_CLIENT_CONNECTIONS_URL",									"http://accounts.toyota.dev.bstd.ru/auth/");

		define("ACCOUNTS_CONNECTIONS_HOST",											"accounts.toyota.dev.bstd.ru");

		define("ACCOUNTS_CONNECTIONS_PORT",											"80");

		define("ACCOUNTS_CONNECTIONS_METHOD",										"POST");

		define("ACCOUNTS_CONNECTIONS_PROTOCOL",										"http:");

		define("URL_ADMIN_SYSTEM",													"http://current_services.toyota.dev.bstd.ru/admin/");
	}
}

define("ACCOUNTS_CONNECTIONS_HEADER",										"User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0\r\nX-TOYOTA:a18Y04o3120T21\r\nX-Requested-With:XMLHttpRequest");

define("ACCOUNTS_CONNECTIONS_PATH_CHECK_TOKEN",								"checktoken");

define("ACCOUNTS_CONNECTIONS_PATH_GET_AUTH_TOKEN",							"gat");

define("ACCOUNTS_CONNECTIONS_PATH_GET_USER_DATA",							"userdata");

define("ACCOUNTS_CONNECTIONS_PATH_EXIT",									"exit");