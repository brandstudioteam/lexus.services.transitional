<?php
/**
 *
 * Типы пользователей
 */
define("ACCOUNT_TYPE_GUEST",				1);
define("ACCOUNT_TYPE_USER",					2);
define("ACCOUNT_TYPE_PARTNER",				3);
define("ACCOUNT_TYPE_DEALER",				5);
define("ACCOUNT_TYPE_TMR",					6);
define("ACCOUNT_TYPE_ADMINS",				7);
define("ACCOUNT_TYPE_DEVELOPERS",			8);
define("ACCOUNT_TYPE_GENERAL_DEALER",		10);
define("ACCOUNT_TYPE_GENERAL_PARTNER",		11);



/**
 *
 * Роли
 */
define("ROLE_GUEST",						1);
define("ROLE_USER",							2);
define("ROLE_SUPER_PARTNER",				7);
define("ROLE_TMR",							4);
define("ROLE_PARTNER",						5);
define("ROLE_DEALER",						6);
define("ROLE_ADMIN",						3);
define("ROLE_SPATIAL",						8);
/**
 *
 * Администратор Акции мойки машин Дилеры
 * @var integer
 */
define("ROLE_CARWASH_DEALER",				30);
/**
 *
 * Администратор Акции мойки машин ТМР
 * @var integer
 */
define("ROLE_CARWASH_TMR",					31);


/**
 *
 * Типы партнеров
 */
/**
 * Дилер
 */
define("PARTNERS_TYPE_DEALER",				1);
/**
 * Агентство
 */
define("PARTNERS_TYPE_AGENCY",				2);
/**
 * ТМР
 */
define("PARTNERS_TYPE_GENERAL",				3);
/**
 * Главное агентство
 */
define("PARTNERS_TYPE_SUPERAGENCY",			4);
define("PARTNERS_TYPE_GENERAL_AGENCY",		4);

/**
 * Специальный партнер
 */
define("PARTNERS_TYPE_SPATIAL",				5);
/**
 * Уполномоченный партнер
 */
define("PARTNERS_TYPE_AUTHORIZED",			6);
/**
 * Разработчики
 */
define("PARTNERS_TYPE_DEVELOPMENT",         7);
/**
 * Головное предприятие дилера
 */
define("PARTNERS_TYPE_GENERAL_DEALER",		8);


/**
 *
 * Статусы кампаний
 */
define("CAMPAIGN_STATUS_NEW",				1);
define("CAMPAIGN_STATUS_REGISTER",			2);
define("CAMPAIGN_STATUS_EXECUTE",			3);
define("CAMPAIGN_STATUS_ARCHIVE",			4);

/*
 * Сущности
 */
define("ESS_TO_MEMBERS",					1);
define("ESS_FORMS_MEMBERS",					2);
define("ESS_FORMS_SCHEDULER",				3);