<?php
class ClientsData extends Data
{
    public function Register()
	{
        return $this->Count("SELECT `references`.`RegisterClient`(
                        ".$this->Esc(User::Init()->GetSessionId()).",
                        ".(microtime(true)*1000).",
                       ".DB_VARS_ACCOUNT_CURRENT.") AS Cnt;");
	}

    public function Unregister($CUID)
	{
        $R = $this->Count("SELECT `references`.`UnregisterClient`(
                        ".$this->Esc($CUID).",
                        ".$this->Esc(User::Init()->GetSessionId()).",
                        ".DB_VARS_ACCOUNT_CURRENT.") AS Cnt");
	}

    public function SetCUID($CUID)
    {
		$Id = false;
        if($CUID)
		{
			//throw new dmtException("Client UID is error", 81112); //throw new dmtException("Client UID is error", 81111);
			$Id = $this->GetClientByCode($CUID);
			if($Id) $this->Set(DB_VARS_CLIENT_CURRENT, $Id);
		}
        return $Id;
    }

    public function GetClientByCode($CUID)
    {
        return $this->Count("SELECT
	`client_id` AS Cnt
FROM `references`.`users_clients`
WHERE `client_code`=".$this->Esc($CUID)."
	AND `user_id`=".DB_VARS_ACCOUNT_CURRENT.";");
    }
}