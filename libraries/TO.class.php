<?php
define("TO_DEALERS_ACTIOS_FILE_PATH",                    PATH_DOCROOT."to_dealers_actions_files/");

class TO extends Controller
{
	/**
	 *
	 * @var TO
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return TO
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}




	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"brandId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "memberId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"modelId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"captcha"				=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_CAPTCHA),
					"verifier"	=> array("Captcha", "CheckCode")
				),
				"isOwner"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
				),
				"email"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"phone"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
				),
				"gender"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
				),
				"lastName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
				"firstName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
                "middleName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"memberName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"lastNameWEn"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME_WEN),
					"notempty"	=> true
				),
				"firstNameWEn"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME_WEN),
					"notempty"	=> true
				),
				"visitDate"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"visitTime"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TIME),
				),
				"userActionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"year"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_YEAR),
				),
				"vin"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_VIN),
				),
				"mileage"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"regNumber"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_AUTO_REGNUMBER_SIMPLY),
				),
				"serviceType"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "captcha"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_CAPTHCA)
				),
                "usersActionId"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "usersActionName"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
                "usersActionDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"info"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "statusId"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "comment"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "visitDateStart"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "visitDateEnd"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerStart"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerEnd"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registred"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "dealersActionFile"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 5242880, 5242880),
						"mime"			=> array(
							"accept"		=> array("pdf", "jpg", "png")
						),
    					"name"			=> md5(microtime(true)),
						"savepath"		=> TO_DEALERS_ACTIOS_FILE_PATH,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
                "dealersActionName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "dealersActionMileageMin"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "dealersActionMileageMax"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "dealersActionYearMin"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_YEAR),
				),
                "dealersActionYearMax"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_YEAR),
				),
                "dealersActionModels"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "dealersActionStatus"		=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1)),
				),
                "dealersActionStart"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "dealersActionEnd"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "dealersActionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "partnersDivisionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "partnerDivisionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "partnerDivisionEmail"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
                "type"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"fds"		=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
				),
				"vinsFile"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("csv")
						),
    					"name"			=> "_auto",
						"savepath"		=> PATH_SERVICES_UPLOAD_FILE,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
				"scTitle"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"scDescription"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
			)
		);
		$this->Modes = array(
			//Возвращает список брендов
			"a"		=> array(
				"exec"		=> array("TOProcessor", "GetBrand")
			),
			//Возвращает список моделей
			"b"		=> array(
				"exec"		=> array("TOProcessor", "GetModels"),
				"TplVars"		=> array("brandId" => 1)
			),
            //Возвращает список моделей с поколением
			"с"		=> array(
				"exec"		=> array("TOProcessor", "GetModelsFullName"),
				"TplVars"		=> array("brandId" => 1)
			),
			//Возвращает список Типов обслуживания
			"d"		=> array(
				"exec"		=> array("TOProcessor", "GetServicesType"),
				"TplVars"		=> array("partnerId" => 0)
			),
            //Возвращает список действий пользователя
			"e"		=> array(
				"exec"		=> array("TOProcessor", "GetUserActions")
			),
            //Возвращает список действий пользователя
			"e2"		=> array(
				"exec"		=> array("TOProcessor", "GetUserActions2"),
                "TplVars"		=> array("partnerId" => 1)
			),
            //Возвращает список действий пользователя
			"e3"		=> array(
				"exec"		=> array("TOProcessor", "GetUserActions3"),
                "TplVars"		=> array("partnerId" => 0)
			),
            //Возвращает модель по VIN
			"f"		=> array(
				"exec"		=> array("TOProcessor", "GetModelsByVIN"),
				"TplVars"		=> array("vin" => 1)
			),
           //Записывает пользователя на техобслуживание
            //$FName, $MName, $LName, $Email, $Phone, $VisitDate, $VisitTime, $UserAction, $Model, $Year, $VIN, $Mileage, $Number, $TOType, $Dealer
			"g"		=> array(
				"exec"		=> array("TOProcessor", "SaveMember"),
				"TplVars"		=> array("firstName" => 2, "middleName" => 0, "lastName" => 2, "email" => 0, "phone" => 2, "visitDate" => 2, "visitTime" => 2,
                                        "userActionId" => 2, "modelId" => 2, "year" => 2, "vin" => 0, "mileage" => 2, "regNumber" => 0, "serviceType" => 2,
                                        "partnerId" => 2, "fds" => 0, "info" => 0, "type" => 0, "partnersDivisionId" => 0)
			),
            //Возвращает список Типов обслуживания
			"h"		=> array(
				"exec"		=> array("TOProcessor", "AddUserActions"),
				"TplVars"		=> array("partnerId" => 0, "usersActionName" => 2, "usersActionDescription" => 0)
			),
            //Возвращает список Типов обслуживания
			"i"		=> array(
				"exec"		=> array("TOProcessor", "SaveUserActions"),
				"TplVars"		=> array("usersActionId" => 2, "usersActionName" => 0, "usersActionDescription" => 0)
			),
            //Возвращает список Типов обслуживания
			"j"		=> array(
				"exec"		=> array("TOProcessor", "DeleteUserActions"),
				"TplVars"		=> array("usersActionId" => 2)
			),
            //Возвращает список действий пользователя
			"k"		=> array(
				"exec"		=> array("TOProcessor", "GetUserActionsFromDealer")
			),
            "l"		=> array(
				"exec"		=> array("TOProcessor", "GetMembers"),
                "TplVars"		=> array("partnerId" => 0)
			),
            "m"		=> array(
				"exec"		=> array("TOProcessor", "ChangeStatus"),
                "TplVars"		=> array("memberId" => 2, "statusId" => 2, "comment" => 0)
			),
            "n"		=> array(
				"exec"		=> array("TOProcessor", "GetStatuses"),
                "TplVars"		=> array()
			),
            "o"		=> array(
				"exec"		=> array("TOProcessor", "GetMembersCSV"),
                "TplVars"		=> array("partnerId" => 0, "modelId" => 0, "registred" => 0, "registerEnd" => 0, "visitDateStart" => 0, "visitDateEnd" => 0)// "registerStart" => 0,
			),
            "p"		=> array(
				"exec"		=> array("TOProcessor", "CheckVIN"),
                "TplVars"		=> array("vin" => 2, "captcha" => 2)
			),
            "r"     => array(
                "TplVars"		=> array("dealersActionFile" => 2)
			),
            "s"     => array(
                "exec"		=> array("TOProcessor", "AddDealersAction"),
                "TplVars"		=> array("dealersActionName" => 2, "dealersActionMileageMin" => 0, "dealersActionMileageMax" => 0, "dealersActionYearMin" => 0, "dealersActionYearMax" => 0,
                                        "dealersActionStart" => 0, "dealersActionEnd" => 0, "dealersActionModels" => 2),
                "getFilesProps"		=> "dealersActionFile",
            ),
            "t"		=> array(
				"exec"		=> array("TOProcessor", "GetCampaignForMember"),
                "TplVars"		=> array("memberId" => 2)
			),
            "u"     => array(
                "exec"		=> array("TOProcessor", "GetDealersActionsList"),
                                        //$Dealers = null, $Status = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null
                "TplVars"		=> array("partnersDivisionId" => 0)
            ),
            "v"     => array(
                "exec"		=> array("TOProcessor", "GetDealersActionsModels"),
                                        //$Dealers = null, $Status = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null
                "TplVars"		=> array("partnersDivisionId" => 0)
            ),
            "w"     => array(
                "exec"		=> array("TOProcessor", "ChangeStatusDealersAction"),
                                        //$Dealers = null, $Status = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null
                "TplVars"		=> array("dealersActionId" => 2, "dealersActionStatus" => 0)
            ),
            "x"     => array(
                "exec"		=> array("TOProcessor", "GetTOModels"),
            ),

            "y"     => array(
                "exec"		=> array("TOProcessor", "GetEmails"),
                "TplVars"		=> array("partnerDivisionId" => 0)
            ),
            "z"     => array(
                "exec"		=> array("TOProcessor", "AddEmail"),
                "TplVars"		=> array("partnerDivisionEmail" => 2, "partnerDivisionId" => 0)
            ),
            "a1"     => array(
                "exec"		=> array("TOProcessor", "DeleteEmail"),
                "TplVars"		=> array("partnerDivisionEmail" => 2, "partnerDivisionId" => 0)
            ),
			"a2"     => array(
                "exec"		=> array("TOProcessor", "GetDealerData"),
                "TplVars"		=> array("partnerDivisionId" => 2)
            ),
			"a3"     => array(
                "exec"		=> array("TOProcessor", "AddSpatialServiceCampaign"),
                "TplVars"		=> array("scTitle" => 2, "scDescription" => 2),
                "getFiles"		=> "vinsFile",
            ),
			"a4"	=> array(
				"TplVars"		=> array("vinsFile" => 1)
			),
			"a5"     => array(
                "exec"		=> array("TOProcessor", "GetSpatialServiceCampaign")
            ),
			"a6"     => array(
                "exec"		=> array("TOProcessor", "DeleteMemeber"),
                "TplVars"		=> array("memberId" => 2)
            ),

			"zzz000"     => array(
                "exec"		=> array("TOProcessor", "PrepareVINs"),
            ),
		);
	}
}