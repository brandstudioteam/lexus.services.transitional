<?php
class Clients extends Controller
{
	/**
	 *
	 * @var Clients
	 */
	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Clients
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				//Тип
				"cuid"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_MD5),
				)
			)
		);

		$this->Modes = array(
			//Заругистрироавть клиент
			"a"	=> array(
				"exec"			=> array("ClientsProcessor", "Register")
			),
			//Удалить клиент
			"b"	=> array(
				"exec"				=> array("ClientsProcessor", "Unregister"),
				"TplVars"			=> array("cuid" => 2)
			)
		);
	}
}