<?php
class EventDriver extends BaseDebug
{
	static public function CreateEvent($Type, $Category, $Parent, $Name, $Description, $Users)
	{
		if(EVENTS_DRIVER_HOST == mb_strtolower($_SERVER["HTTP_HOST"]))
			$R = EventProcessor::Init()->CreateEvent($Type, $Category, $Parent, $Name, $Description, $Users);
		else
		{
			$Driver = new InetDriver();
			$Data = array(
				"a" => "x",
				"b"	=> "a", 
				"d"	=> array(
					"eventType"				=> $Type,
					"eventCategory"			=> $Category,
					"eventParent"			=> $Parent,
					"eventName"				=> $Name,
					"eventDescription"		=> $Description,
					"users"					=> $Users
				)
			);
			
			$Driver->Request("prq=".json_encode($Data), EVENTS_DRIVER_HOST, EVENTS_DRIVER_PATH, null, "POST", null, CHARSET_UTF8, array(	"X-REQUESTED-WITH:XMLHttpRequest",
																																"X-REQUESTED-SDCFAONJYWNHUWQ:1g3Qn91-u_E10-t-r_58"), true);
			
			$R = $Driver->Body;
			
			if(!$R)
			{
				throw new dmtException("Undefined Error", 1);
			}
			$R = json_decode($R, true);
			if($R["error"])
			{
				throw new dmtException($R["error"], 2);
			}
			$R = $R["result"] ? $R["result"] : true;
		}
		return $R;
	}
}