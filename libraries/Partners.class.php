<?php
class Partners extends Controller
{
	/**
	 *
	 * @var Partners
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return Partners
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"partnerDivisionId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerTypeId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerUserDataType"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerDivisionName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerDivisionOfficialName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerDivisionAddress"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerDivisionEmail"				=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_EMAIL),
				),
				"partnerTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"regionId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"cityId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"findText"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
                "children"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "roleId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "parent"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"childrenPartners"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"gLn"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT),
				),
				"gLg"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT),
				),
				"partnerDivisionSite"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_URL),
				),
				"partnerId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerDivisionPhone"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
				),
				"partnerDivisionRCode"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_RCODE),
				),
				"mapImage"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("gif", "jpeg", "jpg", "png")
						),
    					"name"			=> "_auto",
						"savepath"		=> PATH_PARTNERS_MAP_IMAGE,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
			)
		);
		$this->Modes = array(
			//Создает партнера
			"a"		=> array(
				"exec"		=> array("PartnersProcessor", "CreatePartner"),
								//($PartnerType, $UsersDataType, $Name2, $OfficialName, $Address, $Email)
				"TplVars"		=> array("partnerTypeId" => 2, "partnerId" => 0, "partnerDivisionName" => 2, "partnerDivisionSite" => 0, "cityId" => 2, "partnerDivisionAddress" => 0, "gLn" => 0, "gLg" => 0, "partnerDivisionPhone" => 0, "partnerDivisionRCode" => 0, "childrenPartners" => 0),
				"getFiles"		=> array("mapImage" => 0)
			),
			//Сохраняет изменения партнера
			"b"	=> array(
				"exec"		=> array("PartnersProcessor", "SavePartner"),
									//SavePartner($Partner, $PartnerType, $UsersDataType, $Name, $OfficialName, $Address, $Email)
				"TplVars"		=> array("partnerDivisionId" =>2, "partnerTypeId" => 0, "partnerId" => 0, "partnerDivisionName" => 0, "partnerDivisionSite" => 0, "partnerDivisionAddress" => 0, "gLn" => 0, "gLg" => 0, "partnerDivisionPhone" => 0, "partnerDivisionRCode" => 0, "childrenPartners" => 0),
				"getFiles"		=> array("mapImage" => 0)
			),
			//Возвращает список партнеров
			"c"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnersList"),
				"TplVars"		=> array("partnerTypeId" => 0, "regionId" => 0, "findText" => 0)
			),
			//Возвращает информацию об одном партнере
			"d"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnerF"),
				"TplVars"		=> array("partnerId" => 1)
			),
			//Создает тип партнера
			"e"	=> array(
				"exec"		=> array("PartnersProcessor", "CreatePartnerType"),
										//$Name, $Description
				"TplVars"		=> array("partnerTypeName" => 1, "partnerTypeDescription" => 0)
			),
			//Возвращает список партнеров с дополнительной информацией
			"f"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnersListF"),
				"TplVars"		=> array("partnerTypeId" => 0, "regionId" => 0, "findText" => 0)
			),
			//Возвращает список типов партнеров
			"g"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnersTypeList")
			),

            //Возвращает список подчиненных подразделений и ролей для них
			"i"	=> array(
				"exec"		=> array("PartnersProcessor", "GetChildrenRoles")
			),
            //Сохраняет список подчиненных подразделений и ролей для них
			"j"	=> array(
				"exec"		=> array("PartnersProcessor", "SaveChildrenRoles"),
                "TplVars"		=> array("userId" => 2, "children" => 2, "roleId" => 2)
			),
            //Сохраняет список подчиненных подразделений и ролей для них
			"k"	=> array(
				"exec"		=> array("PartnersProcessor", "GetSlavePartnersTypes")
			),
            "l"	=> array(
				"exec"		=> array("PartnersProcessor", "GetHierarchy"),
                "TplVars"		=> array("parent" => 0)
			),


            //Сохраняет список подчиненных подразделений и ролей для них
			"m"	=> array(
				"exec"		=> array("PartnersProcessor", "GetAccessiblePartners")
			),
            //Сохраняет список подчиненных подразделений и ролей для них
			"n"	=> array(
				"exec"		=> array("PartnersProcessor", "GetAccessiblePartnersParents")
			),
			"o"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnersListFullNEW"),
				"TplVars"		=> array("partnerTypeId" => 0, "regionId" => 0)
			),
			"p"	=> array(
				"exec"		=> array("PartnersProcessor", "GetPartnersCSV"),
				"TplVars"		=> array("partnerTypeId" => 0, "regionId" => 0, "findText" => 0)
			),
			"r"	=> array(
				"exec"		=> array("PartnersProcessor", "SaveChildrenPartners"),
				"TplVars"		=> array("partnerDivisionId" => 2, "childrenPartners" => 0)
			),
			"q"	=> array(
				"TplVars"		=> array("mapImage" => 2)
			),
		);
	}
}