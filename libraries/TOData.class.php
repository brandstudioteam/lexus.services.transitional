<?php
class TOData extends Data
{
    public function GetBrand()
    {
        return $this->Get("SELECT
	`brand_id` AS brandId,
	`name` AS brandName,
	`manufacturer_reference_id` AS manufacturerReferenceId
FROM `references`.`brands`;");
    }

    public function GetModels($Brand = null)
    {
        return $this->Get("SELECT
	`model_id` AS modelId,
	`brand_id` AS brandId,
	`name` AS modelName,
	`status` AS modelStatus,
	`category` AS modelCategory,
	`car_references_id` AS carReferencesId
FROM `references`.`tl_models`".($Brand ? "
WHERE `brand_id`".$this->PrepareValue($Brand) : ""));
    }

    public function GetModelsFullName($Brand = null)
    {
        return $this->Get("SELECT
	a.`model_id` AS modelId,
	a.`brand_id` AS brandId,
	a.`name` AS modelName,
	a.`status` AS modelStatus,
	a.`category` AS modelCategory,
	a.`car_references_id` AS carReferencesId,
	b.`name` AS brandName
FROM `references`.`tl_models` AS a
LEFT JOIN `references`.`brands` AS b ON b.`brand_id`=a.`brand_id`".($Brand ? "
WHERE a.`brand_id`".$this->PrepareValue($Brand) : ""));
    }

     public function GetModelName($Model)
    {
        return $this->Count("SELECT
	CONCAT(b.`name`, ' ', a.`name`) AS Cnt
FROM `references`.`tl_models` AS a
LEFT JOIN `references`.`brands` AS b ON b.`brand_id`=a.`brand_id`
WHERE a.`model_id`=".$Model.";");
    }

	public function DeleteMember($Member)
	{
		/*
		$this->Exec("DELETE FROM `references`.`to_members`
WHERE `member_id`=".$Member.";");
		 */
	}





    public function GetServicesType($Dealer = null, $Brand = null)
    {
        if($Dealer)
            $R = $this->Get("SELECT
	b.`to_type_id` AS serviceTypeId,
	b.`name` AS serviceTypeName,
	b.`description` AS serviceTypeDescription
FROM `references`.`to_types_partners_divisions` AS a
LEFT JOIN `references`.`to_types` AS b ON b.`to_type_id`=a.`to_type_id`
WHERE a.`partners_division_id`=".$Dealer."
	AND a.`brand_id`=".$Brand."
ORDER BY b.`name`;");
        else $R = $this->Get("SELECT
	`to_type_id` AS serviceTypeId,
	`name` AS serviceTypeName,
	`description` AS serviceTypeDescription
FROM `references`.`to_types`
ORDER BY `name`;");
        return $R;
    }

    public function GetServiceTypeName($Type)
    {
        return $this->Count("SELECT
	`name` AS Cnt
FROM `references`.`to_types_partners_divisions`
WHERE `to_partners_type_id`=".$Type.";");
    }

    public function GetUserActions()
    {
        $Query = "SELECT
	`user_action_id` AS usersActionId,
	`name` AS usersActionName,
	`description` AS usersActionDescription
FROM `references`.`users_actions`";
        return $this->Get($Query."
ORDER BY `name`;");
    }

    public function GetUserActions2($Dealer = null)
    {
        $Query = "SELECT
	a.`user_action_id` AS usersActionId,
    a.`partners_division_id` AS partnerId,
	a.`name` AS usersActionName,
	a.`description` AS usersActionDescription,
	b.`name` AS partnerName
FROM `references`.`users_actions_dealers` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`new_partners_division_id`
WHERE b.`brand_id`=".(SITE_CURRENT == SITE_LEXUS ? 2 : 1)."
    AND b.`old_id`".$this->PrepareValue($Dealer);
        return $this->Get($Query."
ORDER BY a.`name`, b.`name`;");
    }

    public function GetUserActions3($Dealer = null)
    {
        $Query = "SELECT
	a.`user_action_id` AS usersActionId,
    a.`partners_division_id` AS partnerId,
	a.`name` AS usersActionName,
	a.`description` AS usersActionDescription,
	b.`name` AS partnerName
FROM `references`.`users_actions_dealers` AS a
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE a.`brand_id`=".WS::Init()->GetBrandId()."
    AND a.`new_partners_division_id`".$this->PrepareValue($Dealer);
        return $this->Get($Query."
ORDER BY a.`name`, b.`name`;");
    }

    public function GetUserActionOne($Action)
    {
        $Query = "SELECT
	a.`user_action_id` AS usersActionId,
    a.`partners_division_id` AS partnerId,
	a.`name` AS usersActionName,
	a.`description` AS usersActionDescription,
	b.`name` AS partnerName
FROM `references`.`users_actions_dealers` AS a
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE a.`user_action_id`=".$Action.";";
        return $this->Get($Query, true);
    }

    public function GetUserActionName($UserAction)
    {
        return $this->Count("SELECT
	`name` AS Cnt
FROM `references`.`users_actions_dealers`
WHERE `user_action_id`=".$UserAction.";");
    }

    public function GetUserActionName2($UserAction)
    {
        return $this->Count("SELECT
	`name` AS Cnt
FROM `references`.`users_actions_dealers`
WHERE `user_action_id`=".$UserAction.";");
    }

    public function AddUserActions($Dealer, $Name, $Description)
    {
        $OldDealer = $this->GetOldDealer($Dealer);
        $this->Exec("INSERT INTO `references`.`users_actions_dealers`
	(`partners_division_id`,
    `brand_id`,
	`name`,
	`description`,
    `new_partners_division_id`)
VALUES
	(".$OldDealer.",
    ".WS::Init()->GetBrandId().",
    ".$this->Esc($Name).",
    ".$this->Esc($Description).",
    ".$Dealer.");");
        return $this->DB->GetLastID();
    }

    public function SaveUserActions($UserAction, $Name, $Description)
    {
        $A = array();
        if($Name)
            $A[] = "`name`=".$this->Esc($Name);
        if($Description)
            $A[] = "`description`=".$this->Esc($Description);

        $this->Exec("UPDATE `references`.`users_actions_dealers`
SET ".implode(", ", $A)."
WHERE `user_action_id`=".$UserAction.";");
    }

    public function DeleteUserActions($UserAction)
    {
        return $this->Exec("DELETE FROM `references`.`users_actions_dealers`
WHERE `user_action_id`=".$UserAction.";");
    }

    public function GetDealerForUserAction($Action)
    {
        return $this->Count("SELECT
    `new_partners_division_id` AS Cnt
FROM `references`.`users_actions_dealers`
WHERE `user_action_id`=".$Action."
    AND `brand_id`=".WS::Init()->GetBrandId().";");
    }




    public function GetModelsByVIN($WMI, $VDS, $VDS2)
    {
        $R = $this->Get("SELECT
	a.`generation` AS modelGeneration,
	a.`start_year` AS modelYearStart,
	a.`end_year` AS modelYearEnd,
	b.`model_id` AS modelId,
	b.`brand_id` AS brandId,
	b.`name` AS modelName,
	c.`name` AS brandName
FROM `references`.`tl_models_codes` AS a
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model_id`
LEFT JOIN `references`.`brands` AS c ON c.`brand_id`=b.`brand_id`
WHERE a.`wmi`=".$this->Esc($WMI)."
	AND a.`vds`=".$this->Esc($VDS).";", true);
        if(!$R)
            $R = $this->Get("SELECT
	a.`generation` AS modelGeneration,
	a.`start_year` AS modelYearStart,
	a.`end_year` AS modelYearEnd,
	b.`model_id` AS modelId,
	b.`brand_id` AS brandId,
	b.`name` AS modelName,
	c.`name` AS brandName
FROM `references`.`tl_models_codes` AS a
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model_id`
LEFT JOIN `references`.`brands` AS c ON c.`brand_id`=b.`brand_id`
WHERE a.`wmi`=".$this->Esc($WMI)."
	AND a.`vds`=".$this->Esc($VDS2).";", true);
        return $R;
    }

    public function GetModelDataByVIN($WDI, $VDS)
    {
        return $this->Get("SELECT
	`modelcode` AS `modelcode`,
	`engine` AS `engine`,
	`eng_volum_round` AS `engVolume`,
    `transmission` AS `transmission`
FROM `references`.`vins_temp`
WHERE `wmi`=".$this->Esc($WDI)."
	AND `vds`=".$this->Esc($VDS).";", true);
    }



    public function SaveMember($FName, $MName, $LName, $Email, $Phone, $VisitDate, $VisitTime, $UserAction, $Model, $Year, $VIN, $Mileage, $Number, $TOType, $Brand, $Dealer, $IsOwner, $Info, $UserActionName, $TOCampaigns, $Type, $NewDealer, $Referrer, $IsTest = null)
    {
        //$NewDealer = $this->GetNewDealer($Dealer);
        $this->Begin();
        try
        {
            $this->Exec("INSERT INTO `references`.`to_members`
    (`first_name`,
    `last_name`,
    `middle_name`,
    `email`,
    `phone`,
    `register`,
    `visit_date`,
    `visit_time`,
    `user_action_id`,
    `model_years`,
    `model`,
    `vin`,
    `mileage`,
    `reg_number`,
    `to_type_id`,
    `brand_id`,
    `partners_division_id`,
    `is_owner_site`,
	`info`,
    `user_action_name`,
	`current_status_record_id`,
    `to_campaigns`,
    `advanced_flag`,
	`referrer`,
    `old_dealer_id`,
    `new_dealer_id`)
VALUES
	(".$this->Esc($FName).",
    ".$this->Esc($LName).",
    ".$this->Esc($MName).",
    ".$this->Esc($Email).",
    ".$this->Esc($Phone).",
    NOW(),
    ".$this->Esc($VisitDate).",
    ".$this->Esc($VisitTime).",
    ".$UserAction.",
    ".$Year.",
    ".$Model.",
    ".$this->Esc($VIN).",
    ".$Mileage.",
    ".$this->Esc($Number).",
    ".$TOType.",
    ".$this->CheckNull($Brand).",
    ".$this->CheckNull($Dealer).",
    ".($IsOwner ? 1 : 0).",
	".$this->Esc($Info).",
    ".$this->Esc($UserActionName).",
	".($IsTest ? 2 : 1).",
    ".($TOCampaigns ? 1 : 0).",
    ".$Type.",
	".$this->Esc($Referrer).",
    ".$this->CheckNull($Dealer).",
    ".$this->CheckNull($NewDealer).");");
            $Member = $this->DB->GetLastID();
            if($TOCampaigns)
            {
                $A = array();
                foreach($TOCampaigns as $v)
                    $A[] = "(".$Member.", ".$v.", 0".")";
                if(sizeof($A))
                    $this->Exec("INSERT INTO `references`.`to_members_to_campaigns`
	(`member_id`,
	`to_campaign_id`,
	`status`)
VALUES ".implode(", ", $A).";");
            }
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }

        return $this->GetMember($Member);
    }



    public function GetMembers($Dealer = null, $Brand = null)
    {
        $Query = "SELECT
    a.`member_id` AS memberId,
	a.`partners_division_id` AS partnerId,
    a.`first_name` AS firstName,
    a.`last_name` AS lastName,
    a.`middle_name` AS middleName,
    a.`email` AS email,
    CONCAT('+7', a.`phone`) AS phone,
    a.`register` AS registredU,
    DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i') AS registred,
    a.`visit_date` AS visitDateU,
	DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS visitDate,
    a.`visit_time` AS visitTime,
    a.`user_action_id` AS userActionId,
    a.`model_years` AS year,
    a.`model` AS modelId,
	a.`info` AS advancedInfo,
    a.`vin` AS vin,
    a.`mileage` AS mileage,
    a.`reg_number` AS regNumber,
    a.`to_type_id` AS serviceTypeId,
    IF(a.`is_owner_site`=1, 'Да', 'Нет') AS isOwner,
    a.`user_action_name` AS userActionName,
	a.`polling_answer_id` AS pollingAnswerId,
    IF(i.`count` IS NULL, 0, i.`count`) AS commentsCount,
	CONCAT(c.`name`, ' ', b.`name`) AS modelName,
	a.`current_status_record_id` AS statusId,
    a.`to_campaigns` AS isToCampaigns,
	a.`referrer` AS referrer,
	DATE_FORMAT(k.`change_datetime`, '%d.%m.%Y %H:%i%s') AS statusChangeDate,
	INET_NTOA(k.`ip`) AS statusChangeUserIP,
	l.`first_name` AS userFirstName,
	l.`middle_name` AS userMiddleName,
	l.`last_name` AS userLastName,
	m.`text` AS statusChangeComment,
    g.`name` AS statusName,
	e.`name` AS serviceTypeName,
	f.`name` AS partnerName,
	h.`comment` AS pollingAnswerComment,
	DATE_FORMAT(h.`create`, '%d.%m.%Y %H:%i:%s') AS pollingAnswerCreate,
	j.`name` AS pollingAnswerName,
	-- CONCAT('<div class=\"polling-date\">', IF(h.`create` IS NULL, '', DATE_FORMAT(h.`create`, '%d.%m.%Y')), '</div><div class=\"polling-name\">', j.`name`, '</div><div class=\"polling-comment\">', IF(h.`comment` IS NULL, '', h.`comment`), '</div>') AS pollingAnswer
    CONCAT('<div class=\"polling-date\">', IF(h.`create` IS NULL, '', DATE_FORMAT(h.`create`, '%d.%m.%Y')), '</div><div class=\"polling-name\">', j.`name`, '</div>') AS pollingAnswer
FROM `references`.`to_members` AS a
LEFT JOIN `references`.`to_members_statuses_history` AS k ON k.`record_id`=a.`current_status_record_id`
LEFT JOIN `references`.`users` AS l ON l.`user_id`=k.`user_id`
LEFT JOIN `references`.`comments` AS m ON m.`comment_id`=k.`comment_id`
LEFT JOIN `references`.`comments_objects_statistics` AS i ON (i.`ess_id`=1 AND i.`object_id`=a.`member_id`)
LEFT JOIN `references`.`to_members_statuses` AS g ON g.`status_id`=a.`current_status_record_id`
LEFT JOIN `references`.`user_polling_answer` AS h ON h.`polling_answer_id`=a.`polling_answer_id`
LEFT JOIN `references`.`pollings_answers` AS j ON j.`answer_id`=h.`answer_id`
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model`
LEFT JOIN `references`.`brands` AS c ON c.`brand_id`=b.`brand_id`
LEFT JOIN `references`.`to_types_partners_divisions` AS e ON e.`to_partners_type_id`=a.`to_type_id`
LEFT JOIN `references`.`partners_divisions` AS f ON f.`partners_division_id`=a.`new_dealer_id`
WHERE a.`brand_id`".$this->PrepareValue($Brand).($Dealer ? "
    AND a.`new_dealer_id`".$this->PrepareValue($Dealer) : "")."
ORDER BY a.`register` DESC;";

        return $this->Get($Query);
    }



    public function GetMember($Member)
    {
        $Query = "SELECT
    a.`member_id` AS memberId,
	a.`partners_division_id` AS partnerId,
    a.`first_name` AS firstName,
    a.`last_name` AS lastName,
    a.`middle_name` AS middleName,
    a.`email` AS email,
    CONCAT('+7', a.`phone`) AS phone,
    a.`register` AS registredU,
    DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i') AS registred,
    a.`visit_date` AS visitDateU,
	DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS visitDate,
    a.`visit_time` AS visitTime,
    a.`user_action_id` AS userActionId,
    a.`model_years` AS year,
    a.`model` AS modelId,
    a.`vin` AS vin,
    a.`mileage` AS mileage,
    a.`reg_number` AS regNumber,
    a.`to_type_id` AS serviceTypeId,
    IF(a.`is_owner_site`=1, 'Да', 'Нет') AS isOwner,
    a.`user_action_name` AS userActionName,
	a.`polling_answer_id` AS pollingAnswerId,
    IF(i.`count` IS NULL, 0, i.`count`) AS commentsCount,
	CONCAT(c.`name`, ' ', b.`name`) AS modelName,
	a.`current_status_record_id` AS statusId,
    DATE_FORMAT(k.`change_datetime`, '%d.%m.%Y %H:%i:%s') AS statusChangeDate,
	INET_NTOA(k.`ip`) AS statusChangeUserIP,
	l.`first_name` AS userFirstName,
	l.`middle_name` AS userMiddleName,
	l.`last_name` AS userLastName,
	m.`text` AS statusChangeComment,
    g.`name` AS statusName,
	e.`name` AS serviceTypeName,
	f.`name` AS partnerName,
	h.`comment` AS pollingAnswerComment,
	DATE_FORMAT(h.`create`, '%d.%m.%Y %H:%i:%s') AS pollingAnswerCreate,
	j.`name` AS pollingAnswerName
FROM `references`.`to_members` AS a
LEFT JOIN `references`.`to_members_statuses_history` AS k ON k.`record_id`=a.`current_status_record_id`
LEFT JOIN `references`.`users` AS l ON l.`user_id`=k.`user_id`
LEFT JOIN `references`.`comments` AS m ON m.`comment_id`=k.`comment_id`
LEFT JOIN `references`.`comments_objects_statistics` AS i ON (i.`ess_id`=1 AND i.`object_id`=a.`member_id`)
LEFT JOIN `references`.`to_members_statuses` AS g ON g.`status_id`=a.`current_status_record_id`
LEFT JOIN `references`.`user_polling_answer` AS h ON h.`polling_answer_id`=a.`polling_answer_id`
LEFT JOIN `references`.`pollings_answers` AS j ON j.`answer_id`=h.`answer_id`
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model`
LEFT JOIN `references`.`brands` AS c ON c.`brand_id`=b.`brand_id`
LEFT JOIN `references`.`to_types_partners_divisions` AS e ON e.`to_partners_type_id`=a.`to_type_id`
LEFT JOIN `references`.`partners_divisions` AS f ON f.`partners_division_id`=a.`partners_division_id`
WHERE a.`member_id`=".$Member.";";

        return $this->Get($Query, true);
    }




    public function ChangeStatus($Member, $Brand, $Status, $Code, $IP, $Comment = null)
    {
        try
        {
            $this->Begin();
            $this->Exec("INSERT INTO `references`.`to_members_statuses_history`
	(`member_id`,
	`change_datetime`,
	`status_id`,
	`brand_id`,
	`user_id`,
	`ip`,
	`comment_id`)
VALUES
	(".$Member.",
    NOW(),
    ".$Status.",
    ".$Brand.",
    ".DB_VARS_ACCOUNT_CURRENT.",
    INET_ATON(".$this->Esc($IP)."),
    ".$this->CheckNull($Comment).");");

            $this->Exec("UPDATE `references`.`to_members`
SET `current_status_record_id`=".$Status."
WHERE `member_id`=".$Member.";");

            $this->Commit();
        }
        catch (dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }

    public function GetStatuses()
    {
        return $this->Get("SELECT
	`status_id` AS statusId,
	`name` AS statusName,
	`description` AS statusDescription
FROM `references`.`to_members_statuses`
ORDER BY `status_id`;");
    }


    public function GetMembersForReport($Brand, $Dealer, $Model = null, $RegisterStart = null, $RegisterEnd = null, $ViziteStart = null, $ViziteEnd = null)
    {
        $Where = array();

        $Where[] = "a.`brand_id`".$this->PrepareValue($Brand);
        if($Dealer)
            $Where[] = "a.`new_dealer_id`".$this->PrepareValue($Dealer);
        if($Model)
            $Where[] = "a.`model`".$this->PrepareValue($Model);
        if($RegisterStart)
            $Where[] = "DATE(a.`register`) >= DATE(".$this->Esc($RegisterStart).")";
        if($RegisterEnd)
            $Where[] = "DATE(a.`register`) <= DATE(".$this->Esc($RegisterEnd).")";
        if($ViziteStart)
            $Where[] = "DATE(a.`visit_date`) >= DATE(".$this->Esc($ViziteStart).")";
        if($ViziteEnd)
            $Where[] = "DATE(a.`visit_date`) <= DATE(".$this->Esc($ViziteEnd).")";

        $Query = "SELECT
    REPLACE(f.`name`, '&ndash;', '-') AS partnerName,
	a.`last_name` AS lastName,
    a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
    a.`email` AS email,
    CONCAT('+7', a.`phone`) AS phone,
    DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i') AS registred,
	DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS visitDate,
    a.`visit_time` AS visitTime,
    CONCAT(c.`name`, ' ', b.`name`) AS modelName,
    a.`model_years` AS year,
    a.`vin` AS vin,
    a.`mileage` AS mileage,
    a.`reg_number` AS regNumber,
    a.`user_action_name` AS userActionName,
	e.`name` AS serviceTypeName,
	-- a.`info` AS advancedInfo,
	g.`name` AS statusName,
    m.`text` AS statusChangeComment,
    DATE_FORMAT(k.`change_datetime`, '%d.%m.%Y %H:%i:%s') AS statusChangeDate,
	CONCAT_WS(' ', l.`last_name`, l.`first_name`, l.`middle_name`) AS userName,
    INET_NTOA(k.`ip`) AS statusChangeUserIP,
    j.`name` AS pollingAnswerName,
	h.`comment` AS pollingAnswerComment,
	DATE_FORMAT(h.`create`, '%d.%m.%Y') AS pollingAnswerCreate,
    IF(a.`is_owner_site`=1, 'Да', 'Нет') AS isOwner,
	a.`referrer` AS referrer
FROM `references`.`to_members` AS a
LEFT JOIN (
	SELECT
		`member_id`,
		MAX(`change_datetime`),
		`change_datetime`,
		`status_id`,
		`user_id`,
		`comment_id`,
		`ip`
	FROM `references`.`to_members_statuses_history`
	GROUP BY `member_id`) AS k ON k.`member_id`=a.`member_id`
LEFT JOIN `references`.`users` AS l ON l.`user_id`=k.`user_id`
LEFT JOIN `references`.`comments` AS m ON m.`comment_id`=k.`comment_id`
LEFT JOIN `references`.`comments_objects_statistics` AS i ON (i.`ess_id`=1 AND i.`object_id`=a.`member_id`)
LEFT JOIN `references`.`to_members_statuses` AS g ON g.`status_id`=a.`current_status_record_id`
LEFT JOIN `references`.`user_polling_answer` AS h ON h.`polling_answer_id`=a.`polling_answer_id`
LEFT JOIN `references`.`pollings_answers` AS j ON j.`answer_id`=h.`answer_id`
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model`
LEFT JOIN `references`.`brands` AS c ON c.`brand_id`=b.`brand_id`
LEFT JOIN `references`.`to_types_partners_divisions` AS e ON e.`to_partners_type_id`=a.`to_type_id`
LEFT JOIN `references`.`partners_divisions` AS f ON f.`partners_division_id`=a.`partners_division_id`";

        return $this->Get($Query.$this->PrepareWhere($Where));
    }


    public function CheckVIN($VIN)
    {
        return $VIN ? $this->Get("SELECT
    a.`vin` AS VIN,
	c.`to_campaign_id` AS toCampaignId,
	c.`name` AS toCampaignName,
	c.`description` AS toCampaignDescription
FROM `references`.`to_campaigns_vins` AS a
LEFT JOIN `references`.`to_campaigns_vins_related` AS b ON b.`vin_id`=a.`vin_id`
LEFT JOIN `references`.`to_campaigns` AS c ON c.`to_campaign_id`=b.`to_campaign_id`
WHERE a.`brand_id`=".WS::Init()->GetBrandId()."
	AND a.`vin`=".$this->Esc($VIN).";") : null;
    }




    public function AddDealersAction($Name, $MileageMin, $MileageMax, $ManufacturedYearMin, $ManufacturedYearMax, $ActionStart, $ActionEnd, $Models, $File, $FileName, $MimeType)
    {
        $this->Begin();
        try
        {
            $this->Exec("INSERT INTO `references`.`to_dealers_actions`
	(`name`,
    `partners_division_id`,
    `brand_id`,
    `status`,
    `file`,
    `file_name`,
    `file_mime_type`,
    `mileage_min`,
    `mileage_max`,
    `manufactured_year_min`,
    `manufactured_year_max`,
    `start_date`,
    `end_date`,
    `created`)
VALUES (
    ".$this->Esc($Name).",
    ".DB_VARS_PARTNER_CURRENT.",
    ".WS::Init()->GetBrandId().",
    1,
    ".$this->Esc($File).",
    ".$this->Esc($FileName).",
    ".$this->Esc($MimeType).",
    ".$MileageMin.",
    ".$MileageMax.",
    ".$this->Esc($ManufacturedYearMin).",
    ".$this->Esc($ManufacturedYearMax).",
    ".$this->Esc($ActionStart).",
    ".$this->Esc($ActionEnd).",
    NOW());");
            $Action = $this->DB->GetLastID();

            $A = array();
            foreach($Models as $v)
                $A[] = "(".$Action.", ".$v.")";
            $this->Exec("INSERT INTO `references`.`to_dealers_actions_models`
    (`dealers_action_id`,
    `model_id`)
VALUES ".implode(",", $A));
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }

        return $Action;
    }

    protected function SaveDealerActionsModels($Action, $Models)
    {
        $A = array();
        foreach($Models as $v)
            $A[] = "(".$Action.", ".$v.")";
        $this->Exec("INSERT INTO `references`.`to_dealers_actions_models`
    (`dealers_action_id`,
    `model_id`)
VALUES ".implode(", ", $A));
    }


    public function SaveDealersAction($Action, $Status = null, $Name = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null, $File = null, $FileName = null, $MimeType = null)
    {
        $A = array();
        if($this->IsValue($Name))
            $A[] = "`name`=".$this->Esc($Name);
        if($this->IsValue($Status))
            $A[] = "`status`=".$Status;
        if($this->IsValue($File))
            $A[] = "`file`=".$this->Esc($File);
        if($this->IsValue($FileName))
            $A[] = "`file_name`=".$this->Esc($FileName);
        if($this->IsValue($MimeType))
            $A[] = "`file_mime_type`=".$this->Esc($MimeType);
        if($this->IsValue($MileageMin))
            $A[] = "`mileage_min`=".$MileageMin;
        if($this->IsValue($MileageMax))
            $A[] = "`mileage_max`=".$MileageMax;
        if($this->IsValue($ManufacturedYearMin))
            $A[] = "`manufactured_year_min`=".$this->Esc($ManufacturedYearMin);
        if($this->IsValue($ManufacturedYearMax))
            $A[] = "`manufactured_year_max`=".$this->Esc($ManufacturedYearMax);
        if($this->IsValue($ActionStart))
            $A[] = "`start_date`=".$this->Esc($ActionStart);
        if($this->IsValue($ActionEnd))
            $A[] = "`end_date`=".$this->Esc($ActionEnd);

        $this->Begin();
        try
        {
            if(sizeof($A))
                $this->Exec("UPDATE `references`.`to_dealers_actions`
SET ".implode(", ", $A)."
WHERE `dealers_action_id`=".$Action.";");
            if($Models)
            {
                $this->Exec("DELETE FROM `references`.`to_dealers_actions_models`
WHERE `dealers_action_id`=".$Action.";");
                $this->SaveDealerActionsModels($Action, $Models);
            }
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }


     public function CheckDealersAction($Dealer, $Mileage, $ManufacturedYear, $Model)
    {
        return $this->Get("SELECT
	a.`dealers_action_id` AS dealersActionId,
	a.`name` AS dealersActionName,
    a.`file` AS dealersActionFile,
	a.`file_name` AS dealersActionFileName,
	a.`file_mime_type` AS dealersActionFileMime
FROM `references`.`to_dealers_actions` AS a
LEFT JOIN `references`.`to_dealers_actions_models` AS b ON b.`dealers_action_id`=a.`dealers_action_id`
WHERE a.`status`=1
    AND a.`partners_division_id`=".$Dealer."
	AND a.`start_date` <= NOW()
	AND a.`end_date` >= NOW()
	AND b.`model_id`=".$Model."
	AND a.`mileage_min` <= ".$Mileage."
	AND a.`mileage_max` >= ".$Mileage."
	AND a.`manufactured_year_min` <= ".$this->Esc($ManufacturedYear)."
	AND a.`manufactured_year_max` >= ".$this->Esc($ManufacturedYear).";");
    }


    public function GetDealersAction($Action)
    {
        return $this->Get("SELECT
	`dealers_action_id` AS dealersActionId,
	`name` AS dealersActionName,
	`partners_division_id` AS partnersDivisionId,
	`brand_id` AS brandId,
    `status` AS dealersActionStatus,
	`file` AS dealersActionFileName,
	`file_name` AS dealersActionOriginFileName,
	`file_mime_type` AS dealersActionFileMime,
	`mileage_min` AS dealersActionMileageMin,
	`mileage_max` AS dealersActionMileageMax,
	`manufactured_year_min` AS dealersActionYearMin,
	`manufactured_year_max` AS dealersActionYearMax,
	`created` AS dealersActionCreate,
    `start_date` AS dealersActionStart,
    `end_date` AS dealersActionEnd
FROM `references`.`to_dealers_actions`
WHERE `dealers_action_id`=".$Action.";", true);
    }

    public function GetDealersActionsList($Brand, $Dealers, $Status  = null, $MileageMin = null, $MileageMax = null, $ManufacturedYearMin = null, $ManufacturedYearMax = null, $ActionStart = null, $ActionEnd = null, $Models = null)
    {
        $Where = array();
        $Where[] = "a.`partners_division_id`".$this->PrepareValue($Dealers);
		$Where[] = "a.`brand_id`=".$Brand;
        if($this->IsValue($Status))
            $Where[] = "a.`status`".$this->PrepareValue($Status);
        if($MileageMin)
            $Where[] = "a.`mileage_min`".$this->PrepareValue($MileageMin);
        if($MileageMax)
            $Where[] = "a.`mileage_max`".$this->PrepareValue($MileageMax);
        if($ManufacturedYearMin)
            $Where[] = "a.`manufactured_year_min`".$this->PrepareValue($ManufacturedYearMin);
        if($ManufacturedYearMax)
            $Where[] = "a.`manufactured_year_max`".$this->PrepareValue($ManufacturedYearMax);
        if($ActionStart)
            $Where[] = "a.`start_date`".$this->PrepareValue($ActionStart);
        if($ActionEnd)
            $Where[] = "a.`end_date`".$this->PrepareValue($ActionEnd);


        if($Models)
        {
            $Where[] = "b.`model_id`".$this->PrepareValue($Models);
            $Query = "SELECT DISTINCT
	a.`dealers_action_id` AS dealersActionId,
	a.`name` AS dealersActionName,
	a.`partners_division_id` AS partnersDivisionId,
	a.`brand_id` AS brandId,
    a.`status` AS dealersActionStatus,
    IF(a.`status`=1, 'Активная', 'Заблокированная') AS dealersActionStatusName,
	`file` AS dealersActionFileName,
	a.`file_name` AS dealersActionOriginFileName,
	a.`file_mime_type` AS dealersActionFileMime,
	a.`mileage_min` AS dealersActionMileageMin,
	a.`mileage_max` AS dealersActionMileageMax,
	a.`manufactured_year_min` AS dealersActionYearMin,
	a.`manufactured_year_max` AS dealersActionYearMax,
	a.`created` AS dealersActionCreate,
    a.`start_date` AS dealersActionStart,
    a.`end_date` AS dealersActionEnd,
	c.`name` AS partnersDivisionName
FROM `references`.`to_dealers_actions` AS a
LEFT JOIN `references`.`to_dealers_actions_models` AS b ON b.`dealers_action_id`=a.`dealers_action_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`";
        }
        else
        {
            $Query = "SELECT
	a.`dealers_action_id` AS dealersActionId,
	a.`name` AS dealersActionName,
	a.`partners_division_id` AS partnersDivisionId,
	a.`brand_id` AS brandId,
    a.`status` AS dealersActionStatus,
    IF(a.`status`=1, 'Активная', 'Заблокированная') AS dealersActionStatusName,
	`file` AS dealersActionFileName,
	a.`file_name` AS dealersActionOriginFileName,
	a.`file_mime_type` AS dealersActionFileMime,
	a.`mileage_min` AS dealersActionMileageMin,
	a.`mileage_max` AS dealersActionMileageMax,
	a.`manufactured_year_min` AS dealersActionYearMin,
	a.`manufactured_year_max` AS dealersActionYearMax,
	a.`created` AS dealersActionCreate,
    a.`start_date` AS dealersActionStart,
    a.`end_date` AS dealersActionEnd,
	c.`name` AS partnersDivisionName
FROM `references`.`to_dealers_actions` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`";
        }

        return $this->Get($Query.$this->PrepareWhere($Where));
    }

    public function GetModelsForDealersAction($Actions)
    {
        return $this->Get("SELECT DISTINCT
	`dealers_action_id`,
	`model_id`
FROM `references`.`to_dealers_actions_models`
WHERE `dealers_action_id`".$this->PrepareValue($Actions).";");
    }

    public function GetModelsForDealersActionFull($Actions)
    {
        return $this->Get("SELECT DISTINCT
	a.`dealers_action_id` AS dealersActionId,
	a.`model_id` AS modelId,
	CONCAT(c.`name`, ' ', b.`name`) AS modelName
FROM `references`.`to_dealers_actions_models` AS a
LEFT JOIN `references`.`tl_models` AS b ON b.`model_id`=a.`model_id`
LEFT JOIN `references`.`brands` AS c ON c.`brand_id`=b.`brand_id`
WHERE `dealers_action_id`".$this->PrepareValue($Actions).";");
    }

    public function GetTOModels()
    {
        return $this->Get("SELECT
	a.`model_id` AS modelId,
	b.`brand_id` AS brandId,
	CONCAT(b.`name`, ' ', a.`name`) AS modelName
FROM `references`.`tl_models` AS a
LEFT JOIN `references`.`brands` AS b ON b.`brand_id`=a.`brand_id`
WHERE a.`brand_id`=".WS::Init()->GetBrandId().";");
    }

    public function GetDealerForDealersAction($Action)
    {
        return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `references`.`to_dealers_actions`
WHERE `dealers_action_id`=".$Action.";");
    }

    public function ChangeStatusDealersAction($Action, $Status)
    {
        $this->Begin();
        try
        {
            $this->Exec("DELETE FROM `references`.`to_dealers_actions_models`
WHERE `dealers_action_id`=".$Action.";");
            $this->Exec("DELETE FROM `references`.`to_dealers_actions`
WHERE `dealers_action_id`=".$Action.";");
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
        /*
        if($this->Count(""))
            $this->Exec("DELETE FROM `references`.`to_dealers_actions`
WHERE `dealers_action_id`=".$Action.";");
        else $this->Exec("UPDATE `references`.`to_dealers_actions`
SET `status`=".($Status ? 1 : 0)."
WHERE `dealers_action_id`=".$Action.";");
         */
    }


    public function SaveEmails($Emails, $Dealer = null)
    {
        if(!is_array($Emails))
            $Emails = array($Emails);
        $A = array();
        $Dealer = $Dealer ? $Dealer : DB_VARS_PARTNER_CURRENT;
        foreach($Emails as $v)
            $A[] = "(".($Dealer).", ".TOSERVICE_FACILITY_ID.", ".$this->Esc($v).")";
        if(sizeof($A))
        {
            $this->Begin();
            try
            {
                $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`=".$Dealer."
    AND `facility_id`=".TOSERVICE_FACILITY_ID.";");
                $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
	(`partners_division_id`,
	`facility_id`,
	`email`)
VALUES ".implode(",", $A).";");

                $this->Commit();
            }
            catch(dmtException $e)
            {
                $this->Rollback();
                throw new dmtException($e->getMessage(), $e->getCode(), true);
            }
        }
    }


    public function AddEmail($Brand, $Email, $Dealer)
    {
        $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
	(`partners_division_id`,
	`facility_id`,
	`brand_id`,
	`email`)
VALUES
	(".$Dealer.",
    ".TOSERVICE_FACILITY_ID.",
	".$Brand.",
    ".$this->Esc($Email).");");
    }

    public function DeleteEmail($Email, $Dealer)
    {
        $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`=".$Dealer."
    AND `facility_id`=".TOSERVICE_FACILITY_ID."
    AND `email`=".$this->Esc($Email).";");
    }

    public function GetEmail($Email, $Dealer)
    {
        return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
	a.`email` AS partnerDivisionEmail,
	b.`name` AS partnerDivisionName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE a.`facility_id`=".TOSERVICE_FACILITY_ID."
    AND a.`partners_division_id`=".$Dealer."
    AND a.`email`=".$this->Esc($Email).";", true);
    }

    public function GetEmails($Brand, $Dealer)
    {
        return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
	a.`email` AS partnerDivisionEmail,
	b.`name` AS partnerDivisionName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE a.`facility_id`=".TOSERVICE_FACILITY_ID."
	-- AND a.`brand_id`=".$Brand."
    AND a.`partners_division_id`".$this->PrepareValue($Dealer).";");
    }


    public function GetCampaignForMember($Member)
    {
        return $this->Get("SELECT
	a.`member_id` AS memberId,
	b.`to_campaign_id` AS toCampaignId,
	b.`name` AS toCampaignName,
	b.`description` AS toCampaignDescription
FROM `references`.`to_members_to_campaigns` AS a
LEFT JOIN `references`.`to_campaigns` AS b ON b.`to_campaign_id`=a.`to_campaign_id`
WHERE a.`member_id`=".$Member.";");
    }



/*
    public function PrepareVINs()
    {
$this->Dump("1 ----------------------------------");
        $Start =
        $R = $this->Get("SELECT
	`id`,
	`vin`,
	`campaigns`
FROM `tvins`.`vins`;");
$this->Dump("2 ----------------------------------");
        foreach($R as $v)
        {
            $this->Begin();
            try
            {
                $this->Exec("INSERT INTO `references`.`to_campaigns_vins`
    (`brand_id`,
    `vin`)
VALUES (1, ".$this->Esc($v["vin"]).")");
                $Vin = $this->DB->GetLastID();
                $Rel = array();
                $Campaigns = explode(",", $v["campaigns"]);
                foreach($Campaigns as $c)
                    $Rel[] = "(".$c.", ".$Vin.")";
                $this->Exec("INSERT INTO `references`.`to_campaigns_vins_related`
    (`to_campaign_id`,
    `vin_id`)
VALUES ".implode(",", $Rel));
                $this->Commit();
            }
            catch(dmtException $e)
            {
                $this->Rollback();
            }
        }
    }


	public function PrepareVINsLexus()
    {
$this->Dump("1 ----------------------------------");
		$DB = new DBi("localhost", "lservice", "TOhSkypR", "lservice");

		$DB->OpenQuery("SELECT
	`id`,
	`vin`,
	`campaigns`
FROM `lservice`.`vins`;");
		do
		{
$this->Dump("2 ----------------------------------");
			$Data = $DB->GetCurrent();
			$this->Begin();
			try
			{
				$this->Exec("INSERT INTO `references`.`to_campaigns_vins`
    (`brand_id`,
    `vin`)
VALUES (2, ".$this->Esc($Data["vin"]).")");
				$Vin = $this->DB->GetLastID();
				$Rel = array();
				$Campaigns = explode(",", $Data["campaigns"]);
				foreach($Campaigns as $c)
				{
					$Rel[] = "(".$c.", ".$Vin.")";
				$this->Exec("INSERT INTO `references`.`to_campaigns_vins_related`
    (`to_campaign_id`,
    `vin_id`)
SELECT
	`to_campaign_id`,
	".$Vin."
FROM `references`.`to_campaigns`
WHERE `ext_company_id`=".$c."
	AND `brand_id`=2;");
				}
				$this->Commit();
			}
			catch(dmtException $e)
			{
				$this->Rollback();
			}
        }
		while($DB->MoveNext());
    }
*/


    public function GetNewDealer($Dealer)
    {
        return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `references`.`partners_divisions`
WHERE `old_id`=".$Dealer."
    AND `brand_id`=".WS::Init()->GetBrandId().";");
    }

    public function GetOldDealer($Dealer)
    {
        return $this->Count("SELECT
	`old_id` AS Cnt
FROM `references`.`partners_divisions`
WHERE `partners_division_id`=".$Dealer.";");
    }


    public function GetEmailsForFacilities($Dealers, $Facilities)
    {
        return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`facility_id` AS facilityId,
	`email` AS email
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`".$this->PrepareValue($Dealers)."
	AND `facility_id`".$this->PrepareValue($Facilities).";");
    }

    public function GetDealerData($Dealer)
    {
		return $this->Get("SELECT
	d.`partners_division_id` AS partnerId,
	d.`partners_type_id` AS partnerTypeId,
	d.`name` AS partnerName,
	d.`site` AS partnerSite,
	d.`city_id` AS cityId,
	d.`latitude` AS gLt,
	d.`longitude` AS gLg,
	d.`address` AS partnerAddress,
	d.`phones` AS partnerPhone,
	d.`rcode` AS partnerRCode,
	d.`status` AS partnerStatus,
	d.`map_image` AS  partnerMap
FROM `".DBS_UNIVERSAL_REFERENCES."`.`dealers` AS d
WHERE d.`old_id`=". $Dealer.";", true);
    }

	public function GetTOTypes($Dealer, $FromFacility = null)
	{
		if($FromFacility)
		{
			$Query = "SELECT
	a.`to_partners_type_id` AS serviceTypeId,
	a.`partners_division_id` AS partnerDivisionId,
	a.`name` AS serviceTypeName,
	a.`facility_id` AS facilityId,
	a.`description` AS serviceTypeDescription
FROM `references`.`to_types_partners_divisions` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE b.`old_id`=".$Dealer."
	AND b.`brand_id`=".WS::Init()->GetBrandId()."
	AND a.`facility_id` IS NOT NULL;";
		}
		else
		{
			$Query = "SELECT
	a.`to_partners_type_id` AS serviceTypeId,
	a.`partners_division_id` AS partnerDivisionId,
	a.`name` AS serviceTypeName,
	a.`description` AS serviceTypeDescription
FROM `references`.`to_types_partners_divisions` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE `old_id`=".$Dealer."
	AND b.`brand_id`=".WS::Init()->GetBrandId()."
	AND `facility_id` IS NULL;";
		}
		return $this->Get($Query);
	}


	public function DeleteMemeber($Member)
	{
		$this->Begin();
		try
		{
			$this->Exec("DELETE FROM `references`.`comments`
WHERE `ess_id`=".ESS_TO_MEMBERS."
	AND `object_id`=".$Member.";");
			$this->Exec("DELETE b FROM `references`.`to_members` AS a
LEFT JOIN `references`.`pollings_answers` AS b ON b.`answer_id`=a.`polling_answer_id`
WHERE a.`member_id`=".$Member.";");
			$this->Exec("DELETE FROM `references`.`to_members_statuses_history`
WHERE `member_id`=".$Member.";");
			$this->Exec("DELETE FROM `references`.`to_members_to_campaigns`
WHERE `member_id`=".$Member.";");
			$this->Exec("DELETE FROM `references`.`to_members`
WHERE `member_id`=".$Member.";");
			$this->Commit();
		}
		catch(dmtException $e)
		{
			$this->Rollback();
			throw new dmtException($e->getMessage(), $e->getCode(), true);
		}
	}

	public function AddSpatialServiceCampaign($Brand, $Title, $Description, $VINs)
	{
		$this->Begin();
		try
		{
			$this->Exec("INSERT INTO `references`.`to_campaigns`
	(`brand_id`,
	`name`,
	`description`,
	`status`)
VALUES
	(".$Brand.",
	".$this->Esc($Description)."
	".$this->Esc($Title).",
	1);");
			$Campaign = $this->DB->GetLastID();
			foreach($VINs as $v)
			{
				$VId = $this->Count("SELECT
	`vin_id` AS Cnt
FROM `references`.`to_campaigns_vins`
WHERE `brand_id`=".$Brand."
	AND `vin`=".$this->Esc($v));
				if(!$VId)
				{
					$this->Exec("INSERT INTO `references`.`to_campaigns_vins`
	(`vin`,
	`brand_id`)
VALUES
	(".$this->Esc($v).",
	".$Brand.");");
					$VId = $this->DB->GetLastID();
				}
				$this->Exec("INSERT IGNORE INTO `references`.`to_campaigns_vins_related`
	(`to_campaign_id`,
	`vin_id`)
VALUES
	(".$Campaign.",
	".$VId.");");
			}
			
			
			$this->Rollback();
//$this->Commit();
			return $Campaign;
		}
		catch(dmtException $e)
		{
			$this->Rollback();
			throw new dmtException($e->getMessage(), $e->getCode(), true);
		}
	}
	
	public function GetSpatialServiceCampaign($Brand, $Campaign = null)
	{
		return $this->Get("SELECT
	`to_campaign_id` AS scCampaignId,
    `ext_company_id` AS scCampaignExtId,
    `brand_id` AS brandId,
    `name` AS scCampaignName,
	`description` AS scCampaignDescription,
    `status` AS scCampaignStatus
FROM `references`.`to_campaigns`
WHERE `brand_id`=".$Brand.($Campaign ? "
	AND `to_campaign_id`=".$Campaign : "").";", (boolean) $Campaign);
	}
}