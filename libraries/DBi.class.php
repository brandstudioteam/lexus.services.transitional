<?php
/**
 *
 * Унифицированный класс работы с СУБД Mysql на основе библиотеки mysqli.
 *
 * Базируется на классе mysqli
 * Интерфейсно соотетствует классу DB.
 * Расширения интерфейса:
 * Добавлены методы управления именованными блокировками
 * SetNamedLock и UnsetNamedLock.
 * Метод OpenQuery, вызванный с параметром $AsArray = true, возвратит массив всех записей без необходимости вызова метода GetAllRecords.
 * Если при работе с данными базой данных будут выброшены пользовательские исключения при SQLSTAT в диапазоне от 50000 до 99999,
 * будет выборошено исключение с кодом, соответсвующим SQLSTAT.
 * Добавлена возможность чтения SQLSTAT.
 * Класс поддерживает работу с несколькими результатами выборки. Для этого необходимо явно использовать имена запросов отличные от 0.
 * Например,
 * $DB = new dmtDBi();
 * $DB->OpenQuery("SELECT `field_1`, `field_2` FROM `table_1`", false, "SelTab1");
 * $Res = array();
 * do {
 * 		$Curr = $DB->GetCurrent("SelTab1");
 * 		$DB->OpenQuery("SELECT `field_1`, `field_2` FROM `table_2` WHERE `field_1`=".$Curr["field_1"], true, "SelTab2");
 * 		if($DB->GetCount())
 * 			$Res[] = $DB->GetCurrent("SelTab2");
 * } while($DB->MoveNext("SelTab1"))
 * $DB->Close("SelTab2");
 * $DB->Close("SelTab1");
 *
 * или
 *
 * $DB->OpenQuery("SELECT `field_1`, `field_2` FROM `table_1`", false);
 * $Res = array();
 * do {
 * 		$DB->OpenQuery("SELECT `field_1`, `field_2` FROM `table_2` WHERE `field_1`=".$DB->Current["field_1"], false, "SelTab2");
 * 		$Res[] = $DB->Current;
 * 		if($DB->GetCount())
 * 			do{
 * 				$Res[] = $DB->GetCurrent("SelTab2");
 * 			} whele($DB->MoveNext("SelTab2"))
 * } while($DB->MoveNext())
 * $DB->Close("SelTab2");
 *
 * @author Игорь
 * @version 1.0 (расширенный интерфейс dmtDB версии 6.2)
 * @package dmtBasicEngine
 *
 * @todo Подлючить модуль отладки работы с БД и логирования выполнения завросов (к версии 1.1)
 */
class DBi extends mysqli
{
	/**
	 *
	 * Название текущей базы данных
	 * @var string
	 */
	private $DB;

	/**
	 *
	 * Текущий запрос на выборку
	 * @var string
	 */
	private $Query;

	/**
	 *
	 * Результат выполнения запроса.
	 * Объект типа mysqli_result.
	 *
	 * @var mysqli_result
	 */
	private $Result;

	/**
	 *
	 * Текущая запись.
	 *
	 * Принимает значения после успешного выполнения текущего запроса на выборку
	 *
	 * @var array
	 */
	private $Current;

	/**
	 *
	 * Указатель записи в наборе записей, возвращенных запросом на выборку
	 * @var integer
	 */
	private $Index;

	/**
	 *
	 * Флаг открытой транзакции
	 *
	 * @var boolean
	 */
	private $IsTransaction	= false;

	/**
	 *
	 * Флаг установленной табличной блокировки
	 *
	 * @var boolean
	 */
	private $IsLocked		= false;

	/**
	 *
	 * Массив приватных свойств, которые можно читать публично
	 *
	 * @var array
	 */
	private $Getting		= array("db", "table", "transactionscount", "islocked", "query", "isolation");


    /**
     *
     * Инициализирует класс, устанавливает соединение с базой данных.
     *
     * @param string $Host - имя хостаю Необязательный
     * Если параметр опущен, то будет использовано имя хоста, определенное константой <b>DB_HOSTNAME</b>.
     * @param integer $Port - номер порта. Необязательный
     * Если параметр опущен, то будет использован номер порта, определенный константой <b>DB_PORT</b>.
     * @param string $User - имя пользователя. Необязательный
     * Если параметр опущен, то будет использовано имя пользователя, определенное константой <b>DB_USERNAME</b>.
     * @param string $Pass - пароль. Необязательный
     * Если параметр опущен, то будет использован пароль, определенный константой <b>DB_PASSWORD</b>.
     * @param string $DB - название базы данных. Необязательный
     * Если параметр опущен, то будет открыта БД по умолчанию, определенная константой <b>DB_DBNAME</b>.
     * @param $DebugMode - флаг установки режима отладки.
     * Если параметр опущен, режим отладки будет отключен.
     * Если параметр эквивалентен true, режим отладки будет включен, если константа DB_DEBUGING эквивалентна true.
     * Зарезервирован. Используется с версии 1.1.
     * @throws dmtException при возникновении ошибки подключения будет выброшено исключение с кодом <b>80000</b>
     */
	public function __construct($Host = null, $User = null, $Pass = null, $DB = null, $Port = null, $DebugMode = false)
	{
		if(!$Host) $Host = DB_HOSTNAME;
		if(!$Port) $Port = DB_PORT;
		if(!$User) $User = DB_USERNAME;
		if(!$Pass) $Pass = DB_PASSWORD;
		$this->DB = $DB ? $DB : DB_DBNAME;
		parent::__construct($Host, $User, $Pass, $this->DB, intval($Port));
		if($this->connect_errno)
			throw new dmtException("DB connect is error with number ".$this->connect_errno.".\r\n".$this->connect_error, 80000);
	}

	/**
	 *
	 * Завершает работу с объектом, снимает блокировки, закрыват подключения
	 */
	public function __destruct()
	{
		if($this->IsTransaction)
		{
			$this->Rollback();
			throw new dmtException("Close dmtDBi class with opened transaction. Transaction is rollback.");
		}
		if($this->IsLocked) $this->UnLockTable();
		if(sizeof($this->Result)) foreach ($this->Result as $v) if(is_object($v)) $v->close();
		parent::close();
	}

	public function __get($Name)
	{
		$Name = strtolower($Name);
		if(array_key_exists($Name, get_class_vars(__CLASS__))) return $this->$Name;
		elseif($Name == "err") return $this->errno;
		elseif($Name == "error") return $this->error;
		elseif($Name == "sqlstate") return $this->sqlstate;
		elseif($Name == "charset") return $this->character_set_name();
		elseif($Name == "current") return isset($this->Current[0]) ? $this->Current[0] : null;
		elseif($Name == "index") return isset($this->Index[0]) ? $this->Index[0] : null;
		elseif($Name == "query") return $this->Query;
		elseif($Name == "count") return $this->GetCount(0);
		else throw new dmtException("Property \"".$Name."\" not found in class \"".__CLASS__."\"");
    }

    /**
     *
     * Устанавливает соединение с базой данных.
     *
     * @param string $Host - имя хостаю Необязательный
     * Если параметр опущен, то будет использовано имя хоста, определенное константой <b>DB_HOSTNAME</b>.
     * @param integer $Port - номер порта. Необязательный
     * Если параметр опущен, то будет использован номер порта, определенный константой <b>DB_PORT</b>.
     * @param string $User - имя пользователя. Необязательный
     * Если параметр опущен, то будет использовано имя пользователя, определенное константой <b>DB_USERNAME</b>.
     * @param string $Pass - пароль. Необязательный
     * Если параметр опущен, то будет использован пароль, определенный константой <b>DB_PASSWORD</b>.
     * @param string $DB - название базы данных. Необязательный
     * Если параметр опущен, то будет открыта БД по умолчанию, определенная константой <b>DB_DBNAME</b>.
     * @throws dmtException при возникновении ошибки будет выброшено исключение с кодом <b>80000</b>
     */
    //$host = NULL, $user = NULL, $password = NULL, $database = NULL, $port = NULL, $socket = NULL
	public function connect($host = null, $user = null, $password = null, $database = null, $port = null, $socket = null)
	{
		$this->Reset();
		if(!$host) $host = DB_HOSTNAME;
		if(!$port) $port = DB_PORT;
		if(!$user) $user = DB_USERNAME;
		if(!$password) $password = DB_PASSWORD;
		$this->DB = $database ? $database : DB_DBNAME;
		parent::connect($host, $user, $password, $this->DB, $port, $socket);
		if($this->connect_errno)
			throw new dmtException("DB connect is error with number ".$this->connect_errno.".\r\n".$this->connect_error, 80000);
	}

	/**
	 *
	 * Открывает базу данных.
	 *
	 * @param string $DB название базы данных. Необязательный.
	 * Если параметр опущен, то будет открыта БД по умолчанию, определенная константой <b>DB_DBNAME</b>.
	 * @throws dmtException при возникновении ошибки будет выброшено исключение с кодом <b>80005</b>
	 */
	public function OpenDB($DB = null)
	{
		if($this->DgMode) $this->Dg->SetTimer("DB:openDB");
		$this->DB = $DB ? $DB : DB_DBNAME;
		if(!$this->select_db($this->DB))
			throw new dmtException("Open Database ".$this->DB." is error with number ".$this->errno.".\r\n".$this->error, 80005);
		$this->SetCharSet();
	}

	/**
	 *
	 * Устанавливает кодировку БД
	 *
	 * @param string $CharSet - кодировка. Необязательный.
	 * Если параметр опущен, будет использована кодировка по-умолчанию, указанная в константе <b>DB_CHARSET</b>.
	 * @throws dmtException при возникновении ошибки будет выброшено исключение с кодом <b>80009</b>
	 */
	public function SetCharSet($CharSet = null)
	{
		if(!$CharSet) $CharSet=DB_CHARSET;
        /*
		if (!$this->set_charset($CharSet));
			throw new dmtException("Set charset is error with number ".$this->errno.".\r\n".$this->error, 80009);
         *
         */
	}

	public function CheckNamedLock($Name)
	{
		$Query = "SELECT IS_FREE_LOCK('".$this->Esc($Name)."') AS Cnt;";
		
		$R = $this->Execute($Query);
		if(!$R) $this->SetError($Query);
		$R = $R->fetch_assoc();
		return !$R["Cnt"];
	}
	
	/**
	 *
	 * Устанавливает именную блокировку
	 * @param string $Name - имя блокировки
	 * @param integer $Timeuot - время ожидания установки именованной блокировки в секундах. Необязательный.
	 * Если параметр не установлен, то будет использовано значение из константы <b>DB_TIMEOUT_NAMED_LOCKS</b>
	 * @throws dmtException В случае возникновения ошибки при выполнении запроса, будет выброшено исключение с кодом <b>80009</b>.
	 * В случае неудачной попытки установить блокировку будет выброшено исключение с кодом <b>80012</b>.
	 */
	public function SetNamedLock($Name, $Timeuot = null)
	{
		if($Timeuot === null) $Timeuot = DB_TIMEOUT_NAMED_LOCKS;
		$Query = "SELECT GET_LOCK('".$this->Esc($Name)."', ".$Timeuot.") AS Cnt;";
		
		$R = $this->Execute($Query);
		if(!$R) $this->SetError($Query);
		$R = $R->fetch_assoc();
		return $R["Cnt"];
	}

	/**
	 *
	 * Снимает именную блокировку
	 * @param string $Name - имя блокировки
	 * @throws dmtException В случае возникновения ошибки при выполнении запроса, будет выброшено исключение с кодом <b>80009</b>.
	 */
	public function UnsetNamedLock($Name)
	{
		$Query = "SELECT RELEASE_LOCK('".$this->Esc($Name)."') AS Cnt;";
	
		$R = $this->Execute($Query);
		if(!$R) $this->SetError($Query);
		$R = $R->fetch_assoc();
		return $R["Cnt"];
	}

    /**
     *
     * Устанавливает блокировку таблиц
     *
     * ВНИМАНИЕ! Эту блокировку необходимо с осторожностью использовать для транзакционных таблиц.
     *
     * @param mixed $Tables - строка название таблицы или массив строк названий таблиц
     * @param mixed $ReadOrWrite - уровень блокировки. Необязательный.
     * 1 или write для блокировки записи, другое значение для блокировки чтения (по-умолчанию).
	 * @throws dmtException В случае возникновения ошибки при выполнении запроса, будет выброшено исключение с кодом <b>80009</b>.
     */
	public function LockTable($Tables, $ReadOrWrite = null)
	{
		$Tables = explode(",", $Tables);
		$ReadOrWrite = ($ReadOrWrite && ($ReadOrWrite == 1 || strtolower($ReadOrWrite) == "write")) ? " WRITE" : " READ";
		if(!parent::query("LOCK TABLES ".implode($ReadOrWrite.",", $Tables).$ReadOrWrite.";"))
			throw new dmtException("Lock tebles ".implode(", ", $Tables)." is error with number ".$this->errno.".\r\n".$this->error, 80009);
		$this->IsLocked = true;
	}

	/**
	 *
	 * Cнимает блокировку таблиц
	 *
	 * @throws dmtException В случае возникновения ошибки при выполнении запроса, будет выброшено исключение с кодом <b>80009</b>.
	 */
	public function UnLockTable()
	{
		if(!parent::query("UNLOCK TABLES;"))
			 throw new dmtException("Unlock tebles is error with number ".$this->errno.".\r\n".$this->error, 80009);
	}

	/**
	 * Открывает транзакцию
	 *
	 * @param integer $Isolation - уровень изоляции:
	 * REPEATABLE-READ  1
	 * READ-UNCOMMITTED 2
	 * READ-COMMITTED   3
	 * SERIALIZABLE     4
	 * @throws dmtException в случае ошибки выбрасывае исключение с кодом <b>80009</b>.
	 */
	public function Begin($Isolation = null)
	{
		if(!$Isolation) $Isolation = DB_ISOLATION_LEVEL;
		$Isolation = ($Isolation == 2) ? "READ-UNCOMMITTED" : ($Isolation == 3) ? "READ-COMMITTED" : ($Isolation == 4) ? "SERIALIZABLE" : "REPEATABLE-READ";
		if(!parent::query( "SET SESSION TRANSACTION ISOLATION LEVEL ".$Isolation.";") || !$this->autocommit(false))
			throw new dmtException("Begin transaction is failed. Error number ".$this->errno.".\r\n".$this->error, 80009);
		$this->IsTransaction = true;
	}

	/**
	 *
	 * Фиксирует транзакцию
	 *
	 * @throws dmtException в случае ошибки выбрасывае исключение с кодом <b>80009</b>.
	 */
	public function Commit($flags = null, $name = null)
	{
		if(!parent::commit($flags, $name))
			throw new dmtException("Commit transaction is failed. Error number ".$this->errno.".\r\n".$this->error, 80009);
		$this->IsTransaction = false;
		$this->autocommit(true);
	}

	/**
	 *
	 * Отменяет транзакцию
	 *
	 * @throws dmtException в случае ошибки выбрасывае исключение с кодом <b>80009</b>.
	 */
	public function Rollback($flags = null, $name = null)
	{
		if(!parent::rollback($flags, $name))
			throw new dmtException("Rollback transaction is failed. Error number ".$this->errno.".\r\n".$this->error, 80009);
		$this->IsTransaction = false;
		$this->autocommit(true);
	}


	/**
	 *
	 * Открывает запрос
	 *
	 * @param string $Query Строка SQL-запроса
	 * @param bollean $SetCurr - установить текущую запись
	 * @param string $Name - имя запроса
	 */
	public function OpenQuery($Query, $AsArray = false, $Name = 0)
	{
		$this->Reset($Name);
		$this->Query[$Name] = $Query;
			$this->Result[$Name] = $this->Execute($Query);
		if(!$this->Result[$Name]) $this->SetError($Query);
		if($AsArray) return $this->GetAllRecords($Name);
		$this->MoveFirst($Name);
	}

	/**
	 *
	 * Сбрасывает значения переменных наборов записей
	 * @param string $Name - имя запроса
	 */
	private function Reset($Name = 0)
	{
		if(isset($this->Result[$Name]) && is_object($this->Result[$Name])) $this->Result[$Name]->close();
		$this->Result[$Name]	= array();
		$this->Current[$Name]	= array();
		$this->Index[$Name]	= null;
	}

	/**
	 *
	 * Возвращает количество записей в результате
	 * @param string $Name - имя запроса
	 */
	public function GetCount($Name = 0)
	{
		return (isset($this->Result[$Name]) && is_object($this->Result[$Name])) ? $this->Result[$Name]->num_rows : 0;
	}

	/**
	 *
	 * Возвращает текущую запись
	 * @param string $Name - имя запроса
	 */
	public function GetCurrent($Name = 0)
	{
		if(!isset($this->Result[$Name])) return null;
		if(is_object($this->Result[$Name]) && $this->Result[$Name]->num_rows)
		{
			if(!$this->Current[$Name]) $this->MoveFirst($Name);
		}
		else $this->Current[$Name] = null;
		return $this->Current[$Name];
	}

	/**
	 *
	 * Возвращает набор записей в виде массива
	 * @param string $Name - имя запроса
	 * @return array
	 */
	public function GetAllRecords($Name = 0)
	{
		$R = array();
		if(isset($this->Result[$Name]) && is_object($this->Result[$Name]))
			while (($Ri = $this->Result[$Name]->fetch_assoc()))
				$R[] = $Ri;
		return $R;
	}

	/**
	 *
	 * Выполняет запросы, не возвращающие записей
	 * @param string $Query - SQL-запрос
	 * @throws выбрасывается исключение при пустом значении $Query и в случае ошибки (через метод SetError);
	 */
	public function Exec($Query)
	{
		$this->Reset();
		$this->Query = null;
		$R = $this->Execute($Query);
		if($R === false)
			$this->SetError($Query);
		return $this->affected_rows;
	}


	protected function Execute($Query)
	{
		try {
			$this->set_charset(DB_CHARSET);
			if(!$Query) return null;
			return $this->query($Query);
		}
		catch (dmtException $e)
		{
			preg_match('~.*?\((\d+)\/~ui', $e->getMessage(), $Z);
			if($Z)
			{
				$SQLState = $Z[1];
				if($SQLState > 50000)
					throw new dmtException("", $SQLState);
			}
		}
	}

	private function SetError($Query = null)
	{
		$Err = 5000;
		if($this->sqlstate)
		{
			switch (intval($this->errno))
			{
				case 1062:
					if($this->sqlstate != "23000") break;
					$Err = 70002;
					//$Field = dmtString::Cut($this->error, "key '", "'");
					break;
				case 1064:
					if($this->sqlstate != "HY000") break;
					$Err = 70005;
					//$Field = dmtString::Cut($this->error, "Field '", "'");
					break;
				case 1366:
					if($this->sqlstate != "HY000") break;
					$Err = 70006;
					//$Field = dmtString::Cut($this->error, "Field '", "'");
					break;
				case 1032:
					if($this->sqlstate != "HY000") break;
					$Err = 70003;
					break;
				case 1054:
					if($this->sqlstate != "42S22") break;
					$Err = 70004;
					break;
				case 1451:
					if($this->sqlstate != "23000") break;
					$Err = 70007;
					break;
				default:
					$Err = 50000;
			}
			throw new dmtException("Database Error: ".$this->Error."\r\nQuery = \"".($Query ? $Query : $this->Query)."\" Error number = ".$this->errno."; sqlstate = ".$this->sqlstate, $Err);
		}
	}

	/**
	 *
	 * Перемещает указатель на запись с индексом $Index и выбирает ее в $this->Current
	 *
	 * @param integer $Index - индекс записи в пределах от 0 до количества записей минус 1.
	 * @param string $Name - имя запроса
	 * @return boolean true - в случае успешного перемещения указателя,
	 * и false - в случае, если индекс выходит за пределы области определения, т.е. больше количества записей минус 1, или меньше 0.
	 */
	public function Move($Index, $Name = 0)
	{
		if(isset($this->Result[$Name]) && is_object($this->Result[$Name]) && $this->Result[$Name]->data_seek($Index))
		{
			$this->Index[$Name] = $Index;
			$this->Current[$Name] = $this->Result[$Name]->fetch_assoc();
			$R = true;
		}
		else $R = false;
		return $R;
	}

	/**
	 *
	 * Перемещает указатель на первую запись и выбирает ее в $this->Current
	 * @param string $Name - имя запроса
	 */
	public function MoveFirst($Name = 0)
	{
		return $this->Move(0, $Name);
	}

	/**
	 *
	 * Перемещает указатель на последнюю запись и выбирает ее в $this->Current
	 * @param string $Name - имя запроса
	 */
	public function MoveLast($Name = 0)
	{
		return  $this->Move($this->GetCount($Name) - 1, $Name);
	}

	/**
	 *
	 * Перемещает указатель на следующую запись и выбирает ее в $this->Current
	 * @param string $Name - имя запроса
	 */
	public function MoveNext($Name = 0)
	{
		return $this->Move($this->Index[$Name] + 1, $Name);
	}

	/**
	 *
	 * Перемещает указатель на предыдущую запись и выбирает ее в $this->Current
	 * @param string $Name - имя запроса
	 */
	public function MovePrevious($Name = 0)
	{
		return $this->Move($this->Index[$Name] - 1, $Name);
	}

	/**
	 *
	 * Возвращает последний вставленный идентификатор в поле auto_increment
	 */
	public function GetLastID()
	{
		$R = $this->insert_id;
		if(!$R) $R = null;
		return $R;
	}

	/**
	 *
	 * Экранирует спецсимволы в строках.
	 *
	 * @param string $Value
	 */
	public function Esc($Value)
	{
		return $this->escape_string($Value);
	}

	/**
	 *
	 * Возвращает текущий указатель записи в наборе возвращенных записей
	 * @param string $Name - имя запроса
	 */
	public function GetIndex($Name = 0)
	{
		if(!isset($this->Index[$Name])) return null;
		return $this->Index[$Name];
	}

	/**
	 *
	 * Возвращает последний запрос
	 * @param string $Name - имя запроса
	 */
	public function GetQuery($Name = 0)
	{
		if(!isset($this->Query[$Name])) return null;
		return $this->Query[$Name];
	}
}
?>