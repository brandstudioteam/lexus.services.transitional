<?php
/**
 *
 * Максимальный размер загружаемого файла
 * @var integer
 * Текущее значение равно 100 Мб
 */
define("CONTROLLER_UPLOADED_FILE_MAX_SIZE",			104857600);

/**
 * Методы обращения к серверу
 */
define("REQUEST_METHOD_POST",				1);
define("REQUEST_METHOD_GET",				2);
define("REQUEST_METHOD_HEAD",				3);
define("REQUEST_METHOD_PUT",				4);
define("REQUEST_METHOD_PATCH",				5);
define("REQUEST_METHOD_DELETE",				6);
define("REQUEST_METHOD_TRACE",				7);
define("REQUEST_METHOD_LINK",				8);
define("REQUEST_METHOD_UNLINK",				9);
define("REQUEST_METHOD_CONNECT",			10);
define("REQUEST_METHOD_OPTIONS",			11);


/**
 * Критические ошибки
 */
define("ERRORS_NOT_FOUND",					1);
define("ERRORS_ERROR_ACCESS_DENID",			2);


/**
 * Константы конфигуратора
 */

/**
 * Константы действий по умолчанию
 */
define("WSCONFIG_ACTIONS_RELOAD",			1);
define("WSCONFIG_ACTIONS_REDIRECT",			2);
define("WSCONFIG_ACTIONS_ERROR",			3);
define("WSCONFIG_ACTIONS_USE_TEMPLATE",		4);
define("WSCONFIG_ACTIONS_AUTORIZATION",		5);
define("WSCONFIG_ACTIONS_EXEC",				6);
define("WSCONFIG_ACTIONS_SCRIPT",			7);
define("WSCONFIG_ACTIONS_EXIT",				8);


/**
 *
 * Константы типов отправки данных
 */
define("SEND_TYPE_JSON",					1);
define("SEND_TYPE_TEXT",					2);
define("SEND_TYPE_FILE",					3);
define("SEND_TYPE_UPLOAD",					4);
define("SEND_TYPE_BINARY",					5);
define("SEND_TYPE_HTML",					6);


/**
 * Кодировки текста
 */

define("CHARSET_UTF8",						"utf-8");
define("CHARSET_WIN1251",					"windows-1251");
define("CHARSET_KOI8R",						"koi8r");



define("dmtEMPTY",							"");


/**
 *
 * Разрешить компрессию отправляемых данных
 * @var boolean
 */
define("COMPRESSED_ENABLE",					true);

/**
 * Настройки кеширования
 */

/**
 *
 * Разрешить использование кеширования
 * @var boolean
 */
define("CACHE_ENABLE",						false);

/**
 *
 * Путь к дерeктории кеша
 * @var strinig
 */
define("CACHE_PATH",						PATH_ROOT."/cache/");



/**
 * Константы данных
 */

/**
 * Константы ограничений (limit)
 */
define("LIMITS_TYPE_CHAR", 					1);
define("LIMITS_TYPE_DIGITAL",				2);
define("LIMITS_TYPE_BYTE", 					3);


/**
 *
 * Константы типов фильтров
 */

/**
 *
 * Использование массива сопоставления значений входящих переменных используемым значениям
 */
define("FILTER_TYPE_ALIASES",				2);

/**
 * Использование массива возможных значений
 */
define("FILTER_TYPE_VALUES",				3);

define("FILTER_TYPE_FREE",					4);

/**
 * Использование регуляного выражения
 */
define("FILTER_TYPE_REGEXP",				5);

/**
 * Использование определения типа. Типы описываюся константами фильтров типов данных
 */
define("FILTER_TYPE_TYPE",					6);


/**
 * Фильтры типов данных
 */
define("FILTER_TT_INTEGER",					"Math::PrepareInteger");
define("FILTER_TT_INTEGER_UNSIGNED",		"Math::PrepareUnsignedInteger");
define("FILTER_TT_INTEGER_SIGNED",			"Math::PrepareSignedInteger");
define("FILTER_TT_FLOAT",					"Math::PrepareFloat");
define("FILTER_TT_FLOAT_UNSIGNED",			"Math::PrepareUnsignedFloat");
define("FILTER_TT_FLOAT_SIGNED",			"Math::PrepareSignedFloat");
//define("FILTER_TT_SECUND",					"");
//define("FILTER_TT_TIMESTAMP",				"");
define("FILTER_TT_TIME",					"DTs::PrepareTime");
define("FILTER_TT_DATE",					"DTs::PrepareDate");
//define("FILTER_TT_HOUR",					7);
//define("FILTER_TT_MOUNT",					8);
define("FILTER_TT_YEAR",					"DTs::PrepareYear");
define("FILTER_TT_YEAR_B",					"DTs::PrepareYearB");
define("FILTER_TT_DATETIME",				"DTs::PrepareDatetime");
define("FILTER_TT_DATETIME_UNIX",			"DTs::PrepareUnixDatetime");
define("FILTER_TT_MILEAGE",					"Math::PrepareMileage");



define("FILTER_TT_COLOR",					"Math::PrepareColor");
define("FILTER_TT_TEXT_B64",				"Strings::PrepareTextB64");
define("FILTER_TT_TEXT_B64_ONLY",			"Strings::PrepareTextB64O");
define("FILTER_TT_TEXT",					"Strings::PrepareText");
define("FILTER_TT_TEXT_B64_FULL",			"Strings::PrepareTextB64");
define("FILTER_TT_TEXT_B64_FOR_EDITOR",		"Strings::PrepareTextB64E");
define("FILTER_TT_TEXT_B64_FOR_STRING",		"Strings::PrepareTextB64S");
define("FILTER_TT_TEXT_B64_FOR_FIND",		"Strings::PrepareTextB64Find");
define("FILTER_TT_TEXT_HTML",				"Strings::PrepareTextHTML");

define("FILTER_TT_EMAIL_ONE",				"Strings::ValidateEmailOne");
define("FILTER_TT_EMAIL_MULTI",				"Strings::PrepareEmailMulti");
define("FILTER_TT_PHONE_ONE",				"Strings::ValidatePhoneOne");
define("FILTER_TT_PHONE_ONE2",				"Strings::ValidatePhoneOne2");
define("FILTER_TT_RCODE",					"Strings::ValidateRCode");
define("FILTER_TT_PHONE_ADVANCED",			"Strings::ValidatePhoneAdvances");
//define("FILTER_TT_PHONE_MULTI",				Strings::ValidatePhoneOne);
define("FILTER_TT_PHONE_TOYOTA",			"Strings::ValidatePhoneForToyotaDealerTmp");
define("FILTER_TT_LASTNAME",				"Strings::PrepareLastName");
define("FILTER_TT_NAME",					"Strings::PrepareFirstAndMiddleName");
define("FILTER_TT_URL_PATH",				"Strings::PrepareURLPath");
define("FILTER_TT_OGRN",					"StateCodeValidate::OGRN");
define("FILTER_TT_OGRNIP",					"StateCodeValidate::OGRNIP");
define("FILTER_TT_OGRNUL",					"StateCodeValidate::OGRNUL");
define("FILTER_TT_INN",						"StateCodeValidate::INN");
define("FILTER_TT_INNIP",					"StateCodeValidate::INNIP");
define("FILTER_TT_INNUL",					"StateCodeValidate::INNUL");
define("FILTER_TT_IP_STRING",				"Strings::ValidateIP");
define("FILTER_TT_FIND_STRING",				"Strings::PrepareFindString");
define("FILTER_TT_VIN",						"Strings::ValidateVIN");
define("FILTER_TT_LASTNAME_WEN",			"Strings::PrepareLastNameWEn");
define("FILTER_TT_NAME_WEN",				"Strings::PrepareFirstAndMiddleNameWEn");
define("FILTER_TT_FIO",						"Strings::PrepareFirstAndMiddleAndLansName");
define("FILTER_TT_FIO_WEN",					"Strings::PrepareFirstAndMiddleAndLansNameWEn");
define("FILTER_TT_PROMOCODE",				"Strings::ValidatePromoCode");
define("FILTER_TT_AUTO_REGNUMBER",			"Strings::ValidateAutoRegNumber");
define("FILTER_TT_AUTO_REGNUMBER_SIMPLY",	"Strings::ValidateAutoRegNumberSimply");
define("FILTER_TT_TEXT0",					"Strings::PrepareText0");
define("FILTER_TT_CAPTHCA",                 "Captcha::CheckCaptchaCode");
define("FILTER_TT_LOGIN_PASSWORD",          "Strings::ValidateLoginAndPassword");
define("FILTER_TT_FULL_TEXT",               "Strings::PrepareFullText");
define("FILTER_TT_USER_POSITION",			"Strings::PrepareUserPosition");
define("FILTER_TT_URL",						"Strings::ValidateURL");
define("FILTER_TT_GA_ACCOUNT",				"Strings::ValidateGAAccount");

/**
 *
 * Константы регулярных выражений для фильтров
 */
define("FILTER_EMAIL",						'^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)+$');
define("FILTER_SMALLTEXT",					'^[\%\+?! " / \*!\(\)@#\№:\.,a-zA-Z0-9а-яА-ЯЁё\-_=]*$');
define("FILTER_SIMPLEPHRASE",				'^[ .,-?!:a-zA-Zа-яА-Я]*$');
define("FILTER_SIMPLEPHRASE_RU",			'^[ .,-?!:а-яА-ЯёЁ]*$');
define("FILTER_SIMPLEPHRASE_EN",			'^[ .,-?!:a-zA-Z]*$');
define("FILTER_ONEDIGITAL",					'^[0-9]{1,1}$');
define("FILTER_DIGITALS",					'^[-.,0-9]*$');
define("FILTER_DATE",						'^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$');
define("FILTER_WORD",						'^[a-zA-Zа-яА-ЯёЁ]*$');
define("FILTER_WORD_RU",					'^[а-яА-ЯёЁ]*$');
define("FILTER_WORD_EN",					'^[a-zA-Z]*$');
define("FILTER_CHAR",						'^[a-zA-Zа-яА-ЯёЁ]{1,1}$');
define("FILTER_CHAR_RU",					'^[а-яА-ЯёЁ]{1,1}$');
define("FILTER_CHAR_EN",					'^[a-zA-Z]{1,1}$');
define("FILTER_CHARANDDIGITAL",				'^[0-9a-zA-Z]*$');
define("FILTER_COLORHEX",					'^[0-9abcdefABCDEF]{3,6}$');
define("FILTER_PASSWORD",					'^[?!\(\)@#\№:\.\+\=,a-zA-Z0-9а-яА-ЯёЁ\-_]*$');
define("FILTER_QUESTION",					FILTER_SIMPLEPHRASE);
define("FILTER_ANSWER",						FILTER_SIMPLEPHRASE);
define("FILTER_CAPTCHA",					'^[0-9a-zA-Z]*$');
define("FILTER_CONFIRMCODE",				'^[0-9a-zA-Z]*$');
define("FILTER_SEGURL",						'^[\-_0-9a-zA-Z]*$');
define("FILTER_DOMAIN",						'^[-_0-9a-zA-Z]*$');
define("FILTER_DIGITAL",					'^[0-9]*$');
define("FILTER_URL",						'^[a-zA-Z0-9.\-_~:\/?&=+;#]*$');
define("FILTER_FLOAT",						'^[0-9.]*$');
define("FILTER_FILENAME",					'^[ .,-?!()=+a-zA-Zа-яА-ЯёЁ]*$');
define("FILTER_FILENAME_LATIN",				'^[\-_0-9a-zA-Z]{1,64}(\.[0-9a-zA-Z]{1,3})?$');
define("FILTER_SESSION",					'^[\d\w]{20,32}');
define("FILTER_LOGIN",						'^[\%?!\(\)@#\№:\.,a-zA-Z0-9а-яА-ЯёЁ\-_]*$');
define("FILTER_SMALLHTMLTEXT",				'^[\?! " < > = \s \/ \*!\(\)\[\]\$\+\'\`\&%«»\{\}@#\№:;\.,a-zA-Z0-9а-яА-ЯЁё\-_]*$');
define("FILTER_SEARCH",						'^[\ 0-9a-zA-Zа-яА-ЯёЁ\-]*$');
define("FILTER_MD5", 						'^[a-zA-Z0-9]{32}$');
define("FILTER_BASE64",						'^[a-zA-Z0-9\+= /]*$');
define("FILTER_SHA1",						'^[a-zA-Z0-9]{40}$');
define("FILTER_SIMPLEEMAIL",				'^[\w\d_\-+.]?@[\w\d_\-]{2,}(?:\.[\w\d_\-]{2,})?$');
define("FILTER_FORM_UID",					'^[\w\d_\-]{10,25}');
define("FILTER_TOYOTA_CONFIGURATOR_ID",		'^[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{12}$');


/**
 *
 * Именительный (номинатив)
 *  @var integer
 */
define("TEXT_CASES_NOMINATIVE",				1);
/**
 *
 * Родительный (генитив)
 *  @var integer
 */
define("TEXT_CASES_GENITIVE",				2);
/**
 *
 * Дательный (датив)
 * @var integer
 */
define("TEXT_CASES_DATIVE",					3);
/**
 *
 * Винительный (аккузатив)
 *  @var integer
 */
define("TEXT_CASES_ACCUSATIVE",				4);
/**
 *
 * Творительный (инструменталис)
 *  @var integer
 */
define("TEXT_CASES_ABLATIVE",				5);
/**
 *
 * Предложный (препозитив)
 *  @var integer
 */
define("TEXT_CASES_PREPOSITIONAL",			6);


