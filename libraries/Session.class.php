<?php
/**
 * Класс работы с сессиями. Работает с вессиями сохраняемыми в файлах и в БД.
 * Поддерживает одновременную работу с несколькими сессиями. TODO: реализовать полнофункциональную поддержку
 */
//TODO: Доработать методы класса для сохранения данных сессии в БД
class Session extends Base
{
	private static $IsInit = false;
	private $Data;
	private $Session;
	protected $SessionNames = array();
	protected $SessionIds = array();
	//Местоположения идентификатора сессии: POST, GET, COOCKIE;
	private $SessionIn;

	protected $IsSessionStarted = false;

	protected function __construct()
	{
		parent::__construct();
		if(!(parent::CalledClalssFrom() instanceof User)) throw new dmtException("This class \"".__CLASS__."\" can by initialized from class \"User\" only.");
		if(self::$IsInit)
		{
$this->Dump(debug_backtrace());
			throw new dmtException("Class ".__CLASS__." is already inicialized.");
		}
		self::$IsInit = true;
		if(SESSION_TYPE == SESSION_TYPE_DATABASE)
		{
			session_set_save_handler(array($this, "ReadForDB"),
									array($this, "WriteFromDB"),
									array($this, "ReadForDB"),
									array(&$this, "WriteFromDB"),
									array($this, "Destroy"),
									array($this, "GC"));
			$this->Data = new SessionData();
		}
		//$this->CheckSession(SESSION_NAME_USER);
	}

    protected function Check()
	{if(!self::$IsInit) throw new dmtException("Class \"".__CLASS__."\" is not initialized.");}

	/**
	 *
	 * Закрывает сессию (не уничтожает ее)
	 */
	public function Close()
	{
		$this->Check();
		session_write_close();
	}

	/**
	 *
	 * Восстанавливает данные сессии из БД, если сессии сохраняются в БД
	 * @param string $SessionId - идентификатор сессии
	 */
	public function ReadForDB($SessionId)
	{
		$this->Check();
		if(SESSION_TYPE == SESSION_TYPE_DATABASE)
			$this->Data->GetSession($SessionId ? $SessionId : null, $this->GetId() ? $this->GetId() : null);
	}

	/**
	 *
	 * Сохраняет данные сесии в БД, если сессии сохраняются в БД
	 * @param string $SessionId - идентификатор сессии
	 */
	public function WriteFromDB($SessionId)
	{
		$this->Check();
		if(SESSION_TYPE == SESSION_TYPE_DATABASE)
			$R = $this->Data->SaveSession($SessionId ? $SessionId : null, $this->GetId() ? $this->GetId() : null);
	}

	/**
	 *
	 * Удаляет данные сессии из базы данных, если сессии сохраняются в БД
	 * @param unknown_type $SessionId
	 */
	public function DeleteForDB($SessionId)
	{
		$this->Check();
		if(SESSION_TYPE == SESSION_TYPE_DATABASE)
			$R = $this->Data->Delete($SessionId);
	}

	/**
	 *
	 * Очищает данные устаревших сессий
	 */
	public function GC()
	{
		$this->Check();
		if(SESSION_TYPE == SESSION_TYPE_DATABASE)
			$this->Data->Clear();
	}

	public function __destruct()
	{
		if(SESSION_TYPE == SESSION_TYPE_DATABASE)
			if($this->IsSessionStarted) $this->Data->SaveSession($this->GetSessionId(), $this->GetId() ? $this->GetId() : null);
	}

    //Работа с сессиями

	/**
	 * Запускает сессию. Если сессия уже запущена и имя сессии, переданное в аргументе, отличается от имени текущей сессии,
	 * то текущая сессиия будет закрыта и запустится новая сессия
	 *
	 * @param string $SessionName - Имя сессии.
	 */
	public function SessionStart($SessionName = null, $SessionId = null)
	{
		$this->Check();
		if($SessionName)
		{
			if($this->IsSessionStarted && $this->GetSessionName() == $SessionName) return;
			else
			{
				if($this->IsSessionStarted)
					$this->SessionClose();
				$this->SetSessionName($SessionName);
			}
		}
		if($SessionId)
		{
			if($this->IsSessionStarted && $this->GetSessionId() == $SessionId) return;
			if($this->IsSessionStarted)
				$this->SessionClose();
			session_id($SessionId);
		}
		/*
		elseif(!$SessionName && !$this->SessionName)
		{
			if(SESSION_NAME_DEFAULT) $this->SessionName = session_name();
			else throw new dmtException("Bad session name;");
		}
		*/

		session_set_cookie_params(0, '/', ".".$_SERVER["HTTP_HOST"]/*URL_BASE*/);
		session_start();
		if(WS::Init()->IsMobileApp())
			HTTPHeaders::SetMobileAppSession(session_id());
		else setcookie($SessionName ? $SessionName : session_name(), session_id(), null, '/', ".".$_SERVER["HTTP_HOST"]/*URL_BASE*/,  WS::Init()->Secure ? 1 : null);

		//$this->SetCoockies();
		$this->IsSessionStarted = true;
	}

	public function GetSessionName()
	{
		$this->Check();
		return session_name();
	}

	public function GetSessionId()
	{
		$this->Check();
		return session_id();
	}

	/**
	 *
	 * Закрывает текущую сессию
	 */
	public function SessionClose()
	{
		$this->Check();
		if($this->IsSessionStarted)
		{
			session_write_close();
			$this->IsSessionStarted = false;
		}
	}

	/**
	 * Уничтожает сессию
	 *
	 */
	public function Destroy()
	{
		$this->Check();
		if($this->IsSessionStarted)
		{
			$this->UnsetCoockies();
			session_destroy();
		}
		$this->IsSessionStarted = false;
	}

	public function SetCoockies()
	{
		$this->Check();
		setcookie($this->GetSessionName(), $this->GetSessionId(), null, '/', ".".$_SERVER["HTTP_HOST"]/*URL_BASE*/,  WS::Init()->Secure ? 1 : null);
	}

	public function UnsetCoockies()
	{
		$this->Check();
		setcookie($this->GetSessionName(), $this->GetSessionId(), time() - SESSION_TIME_DEFAULT, '/', ".".$_SERVER["HTTP_HOST"]/*URL_BASE*/,  WS::Init()->Secure ? 1 : null);
	}

	public function SessionId()
	{
		return $this->GetSessionId();
	}


	public function CheckSession($SessionName)
	{
		$this->Check();
		if($this->IsSessionStarted)
		{
			if($this->GetSessionName() == $SessionName) return;
			else $this->SessionClose();
		}
		session_name($SessionName);
		session_start();

		$R = session_id();
		if(!mb_eregi("^[a-zA-Z0-9]{1,32}", $R))
		{
			$this->Destroy();
			throw new dmtException("Bad session Id.");
		}
		$this->IsSessionStarted = true;
	}

	public function SetSessionName($SessionName)
	{
		$this->Check();
		session_name($SessionName);
	}

	/*
	 /**
	 *
	 * Возвращает данные сессии
	 */
	/*
	protected function &GetSessionArray()
	{
		$this->Check();
		if(SESSION_TYPE == SESSION_TYPE_FILE) $R = $_SESSION;
		elseif(SESSION_TYPE == SESSION_TYPE_DATABASE)
			$R = $this->Data->GetSession(null, $this->GetId());
		return $R;
	}
	public function Set($Path, $Value)
    {
    	$this->Check();
    	$Path = explode("/", $Path);
    	foreach ($Path as $v)
    	{
    		//if(array_key_exists($v, $this->Session))
    	}
    }

	public function GetSessionData($SessionName, $Path)
	{
		$this->Check();
		$t = false;
		$R = array();
		$Path = explode("/", $Path);
		if(array_key_exists($SessionName, $this->SessionNames) && array_key_exists("Data", $this->SessionNames[$SessionName]))
		{
			$R = $this->SessionNames[$SessionName]["Data"];
			foreach($Path as $v)
			{
				if(array_key_exists($v, $R)) $R = $R[$v];
				else
				{
					$t = true;
					break;
				}
			}
			if($t) $R = null;
		}
		else $R = null;
		return $R;
	}


	private function SetSessions($WithData = false)
	{
		$this->SessionNames[$this->SessionName] = array();
		$this->SessionNames[$this->SessionName]["Id"] = $this->SessionId;
		if($WithData) $this->SessionNames[$this->SessionName]["Data"] = $_SESSION;
	}

	private function DeleteSessions($SessionName = null)
	{
		if(!$SessionName) $SessionName = $this->SessionName;
		unset($this->SessionNames[$SessionName]);
	}
	*/
}
?>