<?php
class Tools
{
	public static function ArrayToPath($Array, $Prefix = null, $Postfix = null)
	{
		return self::ArrayToPathEx($Array, $Prefix, $Postfix);
	}

	protected static function ArrayToPathEx($Values, &$Prefix = null, &$Postfix = null, $Key = null, &$Path = null)
	{
		if(!is_array($Path))
		{
			$Path = array(
				"keys"		=> array(),
				"values"	=> array()
			);
		}
		foreach ($Values as $k => $v)
		{
			if(is_array($v))
				self::ArrayToPathEx($v, $Prefix, $Postfix, ($Key ? $Key.".".$k : $k), $Path);
			else
			{
				$Path["keys"][] = ($Prefix ? $Prefix : "").($Key ? $Key.".".$k : $k).($Postfix ? $Postfix : "");
				$Path["values"][] = $v;
			}
		}
		return $Path;
	}

	public static function js_urlencode($str)
	{
	    $str = mb_convert_encoding($str, 'UTF-16', 'UTF-8');
	    $out = '';
	    for ($i = 0; $i < mb_strlen($str, 'UTF-16'); $i++)
	        $out .= '%u'.bin2hex(mb_substr($str, $i, 1, 'UTF-16'));
	    return $out;
	}

    public static function GetMime($Extend)
    {
        $MimeTypes = array(
		    'txt' => 'text/plain',
		    'htm' => 'text/html',
		    'html' => 'text/html',
		    'php' => 'text/html',
		    'css' => 'text/css',
		    'js' => 'application/javascript',
		    'json' => 'application/json',
		    'xml' => 'application/xml',
		    'swf' => 'application/x-shockwave-flash',
		    'flv' => 'video/x-flv',

		    // images
		    'png' => 'image/png',
		    'jpe' => 'image/jpeg',
		    'jpeg' => 'image/jpeg',
		    'jpg' => 'image/jpeg',
		    'gif' => 'image/gif',
		    'bmp' => 'image/bmp',
		    'ico' => 'image/vnd.microsoft.icon',
		    'tiff' => 'image/tiff',
		    'tif' => 'image/tiff',
		    'svg' => 'image/svg+xml',
		    'svgz' => 'image/svg+xml',

		    // archives
		    'zip' => 'application/zip',
		    'rar' => 'application/x-rar-compressed',
		    'exe' => 'application/x-msdownload',
		    'msi' => 'application/x-msdownload',
		    'cab' => 'application/vnd.ms-cab-compressed',

		    // audio/video
		    'mp3' => 'audio/mpeg',
		    'qt' => 'video/quicktime',
		    'mov' => 'video/quicktime',
			'mp4'	=> 'video/mp4',

		    // adobe
		    'pdf' => 'application/pdf',
		    'psd' => 'image/vnd.adobe.photoshop',
		    'ai' => 'application/postscript',
		    'eps' => 'application/postscript',
		    'ps' => 'application/postscript',

		    // ms office
		    'doc' => 'application/msword',
		    'rtf' => 'application/rtf',
		    'xls' => 'application/vnd.ms-excel',
		    'ppt' => 'application/vnd.ms-powerpoint',

		    // open office
		    'odt' => 'application/vnd.oasis.opendocument.text',
		    'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		);

        return isset($MimeTypes[$Extend]) ? $MimeTypes[$Extend] : 'application/octet-stream';
    }

	public static function MimeContentType($FileName, $FullFilename=false, $AsFile=false, $ReturnShort=false)
	{
		$MimeTypes = array(
		    'txt' => 'text/plain',
		    'htm' => 'text/html',
		    'html' => 'text/html',
		    'php' => 'text/html',
		    'css' => 'text/css',
		    'js' => 'application/javascript',
		    'json' => 'application/json',
		    'xml' => 'application/xml',
		    'swf' => 'application/x-shockwave-flash',
		    'flv' => 'video/x-flv',

		    // images
		    'png' => 'image/png',
		    'jpe' => 'image/jpeg',
		    'jpeg' => 'image/jpeg',
		    'jpg' => 'image/jpeg',
		    'gif' => 'image/gif',
		    'bmp' => 'image/bmp',
		    'ico' => 'image/vnd.microsoft.icon',
		    'tiff' => 'image/tiff',
		    'tif' => 'image/tiff',
		    'svg' => 'image/svg+xml',
		    'svgz' => 'image/svg+xml',

		    // archives
		    'zip' => 'application/zip',
		    'rar' => 'application/x-rar-compressed',
		    'exe' => 'application/x-msdownload',
		    'msi' => 'application/x-msdownload',
		    'cab' => 'application/vnd.ms-cab-compressed',

		    // audio/video
		    'mp3' => 'audio/mpeg',
		    'qt' => 'video/quicktime',
		    'mov' => 'video/quicktime',
			'mp4'	=> 'video/mp4',

		    // adobe
		    'pdf' => 'application/pdf',
		    'psd' => 'image/vnd.adobe.photoshop',
		    'ai' => 'application/postscript',
		    'eps' => 'application/postscript',
		    'ps' => 'application/postscript',

		    // ms office
		    'doc' => 'application/msword',
		    'rtf' => 'application/rtf',
		    'xls' => 'application/vnd.ms-excel',
		    'ppt' => 'application/vnd.ms-powerpoint',

		    // open office
		    'odt' => 'application/vnd.oasis.opendocument.text',
		    'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		);

		$MimeTypes2 = array(
		    'text/plain'										=> 'txt',
		    'text/html'											=> 'html',
		    'text/css'											=> 'css',
		    'application/javascript'							=> 'js',
		    'application/json'									=> 'json',
		    'application/xml'									=> 'xml',
		    'application/x-shockwave-flash'						=> 'swf',
		    'video/x-flv'										=> 'flv',

		    // images
		    'image/png'											=> 'png',
		    'image/jpeg'										=> 'jpg',
		    'image/gif'											=> 'gif',
		    'image/bmp'											=> 'bmp',
			'image/vnd.microsoft.icon'							=> 'ico',
			'image/tiff'										=> 'tiff',
			'image/tiff'										=> 'tif',
			'image/svg+xml'										=> 'svg',
			'image/svg+xml'										=> 'svgz',
			// archives
			'application/zip'									=> 'zip',
			'application/x-rar-compressed'						=> 'rar',
			'application/x-msdownload'							=> 'exe',
			'application/x-msdownload'							=> 'msi',
			'application/vnd.ms-cab-compressed'					=> 'cab',
			// adobe
			'application/pdf'									=> 'pdf',
			'image/vnd.adobe.photoshop'							=> 'psd',
			'application/postscript'							=> 'ai',
			'application/postscript'							=> 'eps',
			'application/postscript'							=> 'ps',
			// ms office
			'application/msword'								=> 'doc',
			'application/rtf'									=> 'rtf',
			'application/vnd.ms-excel'							=> 'xls',
			'application/vnd.ms-powerpoint'						=> 'ppt',
			// open office
			'application/vnd.oasis.opendocument.text'			=> 'odt',
			'application/vnd.oasis.opendocument.spreadsheet'	=> 'ods',
		);



		if(!$AsFile)
		{
			$Ext = strtolower(array_pop(explode('.', $FileName)));
			if (array_key_exists($Ext, $MimeTypes))
				return $ReturnShort ? $Ext : $MimeTypes[$Ext];
			elseif (function_exists('finfo_open'))
			{
			    $Finfo = finfo_open(FILEINFO_MIME);
			    $MimeType = explode(";",finfo_file($Finfo, $FileName));
			    $MimeType = trim($MimeType[0]);
			    finfo_close($Finfo);
			    $R = ($ReturnShort) ? (array_key_exists($MimeType, $MimeTypes2) ? $MimeTypes2[$MimeType] : 'application/octet-stream') : $MimeType;
			    return $R;
			}
			else return (!$ReturnShort) ? 'application/octet-stream' : false;
		}
		else
		{
			if (function_exists('finfo_open') && $FullFilename)
			{
			    if(!file_exists($FullFilename))
			    	throw new dmtException("File name \"".$FullFilename."\" is error.");
				$Finfo = finfo_open(FILEINFO_MIME);
			    $MimeType = explode(";", finfo_file($Finfo, $FullFilename));
			    $MimeType = trim($MimeType[0]);
			    finfo_close($Finfo);
			    $R = ($ReturnShort) ? (array_key_exists($MimeType, $MimeTypes2) ? $MimeTypes2[$MimeType] : 'application/octet-stream') : $MimeType;
				return $R;
			}
			else return false;
		}
	}
}