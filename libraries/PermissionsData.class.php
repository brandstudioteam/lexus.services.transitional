<?php
class PermissionsData extends Data
{
	/**
	 *
	 * Проверяет право у пользователя
	 * @param integer $Permission - Идентификатор права
	 * @param integer $User - Идентификатор пользователя
	 */
	public function CheckAccess($Permission, $Partner = null, $AsBoolean = null)
	{
        if(!$this->Count("SELECT
	COUNT(*) AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS b ON b.`role_id`=a.`role_id`
WHERE a.`user_id`=".DB_VARS_ACCOUNT_CURRENT."
	AND a.`partners_division_id`=".($Partner ? $Partner : DB_VARS_PARTNER_CURRENT)."
	AND b.`permission_id`".$this->PrepareValue($Permission).";"))
		{
			if(!$AsBoolean)
				$this->AccessDenid();
			else return false;
		}
		return true;
	}

    public function GetScopeAccess($Permission, $Partner = null)
    {
        $Query = "SELECT
	a.`partners_division_id` AS partnerDivisionId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS b ON b.`role_id`=a.`role_id`
WHERE a.`user_id`=".DB_VARS_ACCOUNT_CURRENT."
	AND b.`permission_id`".$this->PrepareValue($Permission).($Partner ? "
    AND a.`partners_division_id`".$this->PrepareValue($Partner) : "").";";

        $this->Dump(__METHOD__.": ".__LINE__, "+++++++++", $Query, $this->Count("SELECT ".DB_VARS_ACCOUNT_CURRENT." AS Cnt;"));

        $R = $this->Get($Query);
        return $R;
    }

    public function GetScopeAccessForService($Service)
    {
        if($Service)
            $Query = "SELECT DISTINCT
	a.`partners_division_id` AS partnerDivisionId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS b ON b.`role_id`=a.`role_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS c ON c.`permission_id`=b.`permission_id`
WHERE a.`user_id`=".DB_VARS_ACCOUNT_CURRENT."
	AND c.`service_id`".$this->PrepareValue($Service).";";
        else
            $Query = "SELECT DISTINCT
	a.`partners_division_id` AS partnerDivisionId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles` AS a
WHERE a.`user_id`=".DB_VARS_ACCOUNT_CURRENT.";";
        $this->Dump(__METHOD__.": ".__LINE__, "+++++++++", $Query, $this->Count("SELECT ".DB_VARS_ACCOUNT_CURRENT." AS Cnt;"));

        $R = $this->Get($Query);
        return $R;
    }

	/**
	 *
	 * Возвращает сумму кодов прав персоны для заданного сервиса
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $User - Идентификатор пользователя
	 */
	public function GetAccess($Service, $User)
	{
		return $this->Get("SELECT
	p.`service_id` AS serviceId,
	SUM(p.`code`) AS serviceAccess
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_permissions_distinct` AS up
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=up.`permission_id`
WHERE up.`user_id`=".$User."
	AND p.`service_id`=".$Service.";", true);
	}

	/**
	 *
	 * Возвращает список прав для заданного сервиса или заданного сервиса и заданного пользователя
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $User - Идентификатор пользователя
	 */
	public function GetPermissions()
	{
        return $this->Get("SELECT
	`service_id` AS serviceId,
	`permission_id` AS permissionId,
	`name` AS permissionName,
	`description` AS permissionDescription
FROM `".DBS_UNIVERSAL_REFERENCES."`.`permissions`;");

        /*
		//TODO: Проверить права доступа к правам
        $Where = array();
        if($Service)
            $Where[] = "p.`service_id`=".$Service;
        if($User)
            $Where[] = "up.`user_id`=".$User;
		return $this->Get("SELECT
	p.`service_id` AS serviceId,
	p.`permission_id` AS permissionId,
	p.`name` AS permissionName,
	p.`description` AS permissionDescription
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_permissions_distinct` AS up
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=up.`permission_id`".$this->PrepareWhere($Where).";");
*/
	}

    /**
	 *
	 * Возвращает список прав для заданного сервиса или заданного сервиса и заданного пользователя
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $User - Идентификатор пользователя
	 */
	public function GetPermissionsList($Service = null, $User = null)
	{
		//TODO: Проверить права доступа к правам
        $Where = array();
        if($Service)
            $Where[] = "p.`service_id`=".$Service;
        if($User)
            $Where[] = "up.`user_id`=".$User;
		return $this->Get("SELECT
	p.`service_id` AS serviceId,
	p.`permission_id` AS permissionId,
	p.`name` AS permissionName,
	p.`description` AS permissionDescription
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_permissions_distinct` AS up
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=up.`permission_id`".$this->PrepareWhere($Where).";");

	}

    public function GetPermissionsForClient($Service = null)
    {
        return $this->Get("SELECT DISTINCT
	p.`service_id` AS serviceId,
	SUM(p.`code`) AS permissions
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_permissions_distinct` AS up
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=up.`permission_id`
WHERE up.`user_id`=".DB_VARS_ACCOUNT_CURRENT.($Service ? "
	AND p.`service_id`".$this->PrepareValue($Service) : "")."
GROUP BY p.`service_id`", !!$Service);
    }

    public function GetPartnersHierarchy($Type, $NotType)
    {
        if($Type)
            $R = $this->Get("SELECT
	`partners_division_id` AS partnersId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partner_id` IN (
		SELECT
			`partner_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
		WHERE `partners_division_id`=".User::Init()->GetPartnerId()."
	)
    AND `partners_type_id`".$this->PrepareValue($Type).";");
        else $R = $this->Get("SELECT
	`partners_division_id` AS partnersId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partners_type_id`".$this->PrepareValue($NotType, 2).";");
        return $R;
    }
}