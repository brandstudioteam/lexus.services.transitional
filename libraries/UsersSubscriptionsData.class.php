<?php
class UsersSubscriptionsData extends Data
{
    private $Member;


    public function __construct()
	{
		parent::__construct();
	}

	public function Subscribe(	$Brand, $LastName, $FirstName, $MiddleName, $Gender, $Email, $Age = null, $MaritalStatus = null,
								$CompanyType = null, $Occupation = null, $CarType = null, $Dealer = null, $Themes = null, $Model = null,
								$UCode = null, $ECode = null, $IP = null)
	{
		$Member = $this->GetSubscribe($Email);
        $this->Begin();
        try
        {
            if($Member)
            {
                $Edit = true;
            }
            else
            {
                $Edit = false;
                $this->Exec("INSERT INTO `references`.`users_subscriptions`
	(`brand_id`,
	`partners_division_id`,
	`email`,
	`last_name`,
	`first_name`,
	`middle_name`,
	`age`,
	`gender`,
	`unsubscribe_code`,
	`edit_subscribe_code`,
    `status`,
    `ip`,
    `register`,
    `update`,
    `password_type`)
VALUES
	(".$Brand.",
	".$this->CheckNull($Dealer).",
	".$this->Esc($Email).",
	".$this->Esc($LastName).",
	".$this->Esc($FirstName).",
	".$this->Esc($MiddleName).",
	".$this->CheckNull($Age).",
	".$this->CheckNull($Gender).",
	".$this->Esc($UCode).",
	".$this->Esc($ECode).",
    1,
	INET_ATON(".$this->Esc($IP)."),
	NOW(),
	NOW(),
	1);");

                $Member = $this->DB->GetLastID();
				$this->Set(DB_VARS_SUBSCRIBTION_MEMBER, $Member);
				
				if($MaritalStatus || $CompanyType || $Occupation || $CarType)
					$this->SetAdvancedData($MaritalStatus, $CompanyType, $Occupation, $CarType);
            }
            if(!$Themes)
            {
                $T = $this->GetSubscribeTheme($Brand);
                $Themes = array();
                foreach($T as $v)
                    $Themes[] = $v["subscriptionThemeId"];
            }
            $this->SetCurrentMember($Member);
            $this->SaveThemes($Themes);
            $Themes = $this->GetThemesForMember();

            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getCode(), $e->getMessage(), true);
        }
		//Support old system
        $this->OldTableAU($Email, $Email, $LastName, $Themes, $IP, $UCode, $ECode);

        return $Edit ? null : $Member;
	}

    public function SaveThemes($Themes)
    {
		if(!is_array($Themes))
			$Themes = array($Themes);
		$A = array();
		foreach ($Themes as $v)
			$A[] = "(".DB_VARS_SUBSCRIBTION_MEMBER.", ".$v.")";
		if(sizeof($A))
        {
            $this->Exec("DELETE FROM `references`.`users_subscriptions_users_subscriptions_themes`
WHERE `subscription_id`=".DB_VARS_SUBSCRIBTION_MEMBER.";");
			$this->Exec("INSERT IGNORE INTO `references`.`users_subscriptions_users_subscriptions_themes`
	(`subscription_id`,
	`subscription_theme_id`)
VALUES ".implode(",", $A));
        }
    }

    public function UpdateUserData($LastName, $FirstName, $MiddleName, $Gender, $Email, $Age, $IP, $MaritalStatus, $CompanyType, $Occupation, $CarType, $Dealer = null, $Themes = null, $Model = null, $Stop = null)
    {
        $Set = array();
        $this->Begin();
        $MemberData = $this->GetMember();
        try
        {
            if($Email)
            {
                $Check = $this->Count("SELECT `subscription_id` AS Cnt
FROM `references`.`users_subscriptions`
WHERE `email`=".$this->Esc($Email, true, false, true));
                if(!$Check)
                    $Set[] = "`email`=".$this->Esc($Email, true, false, true);
                elseif($Check != $MemberData["memberId"])
                    throw new dmtException("Email is already exists", 5);
            }
            if($LastName)
                $Set[] = "`last_name`=".$this->Esc($LastName, true, false, true);
            if($FirstName)
                $Set[] = "`first_name`=".$this->Esc($FirstName, true, false, true);
            if($MiddleName)
                $Set[] = "`middle_name`=".$this->Esc($MiddleName, true, false, true);
            if($Gender !== null && !is_empty($Gender))
                $Set[] = "`gender`=".$Gender;
            if($Age)
                $Set[] = "`age`=".$this->CheckNull($Age);
            if($Stop !== null && !is_empty($Stop))
                $Set[] = "`status`=".$Stop;
            if(sizeof($Set))
            {
                $Set[] = "`age`=".$this->Esc($Age, true, false, true);
                $this->Exec("UPDATE `references`.`users_subscriptions`
SET ".implode(", ", $Set)."
WHERE `subscription_id`=".DB_VARS_SUBSCRIBTION_MEMBER.";");
            }
            $this->SetAdvancedData($MaritalStatus, $CompanyType, $Occupation, $CarType);
            $this->SaveThemes($Themes);

            $Themes = $this->GetThemesForMember();
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getCode(), $e->getMessage(), true);
        }
        $this->OldTableAU($MemberData["memberEmail"], $Email, $LastName, $Themes, $IP, null, null, $Stop);
    }

    public function SetAdvancedData($MaritalStatus, $CompanyType, $Occupation, $CarType)
    {
        $this->Exec("INSERT INTO `references`.`users_subscriptions_adv_data`
	(`subscription_id`,
	`marital_status`,
	`company_type`,
	`occupation`,
	`car_type`)
VALUES
	(".DB_VARS_SUBSCRIBTION_MEMBER.",
    ".$this->Esc($MaritalStatus).",
    ".$this->Esc($CompanyType).",
    ".$this->Esc($Occupation).",
    ".$this->Esc($CarType).")
ON DUPLICATE KEY UPDATE
    `marital_status`=VALUES(`marital_status`),
	`company_type`=VALUES(`company_type`),
	`occupation`=VALUES(`occupation`),
	`car_type`=VALUES(`car_type`);");
    }

    public function GetAdvancedData($Member)
    {
        return $this->Get("SELECT
	`subscription_id` AS memberId,
	`marital_status` AS memberMaritalStatus,
	`company_type` AS memberCompanyType,
	`occupation` AS memberOccupation,
	`car_type` AS memberCarType
FROM `references`.`users_subscriptions_adv_data`
WHERE `subscription_id`=".$Member.";");
    }

    public function GetFullMemberData()
    {
        return $this->Get("SELECT
	a.`subscription_id` AS memberId,
	a.`partners_division_id` AS partnerDivisionId,
	a.`email` AS memberEmail,
	a.`last_name` AS memberLastName,
	a.`first_name` AS memberFirstName,
	a.`middle_name` AS memberMiddleName,
	a.`age` AS memberAge,
	a.`gender` AS memberGender,
    a.`status` AS memberStatus,
	-- a.`unsubscribe_code` AS memberUnsubscribeCode,
	-- a.`edit_subscribe_code` AS memberEditCode,
	b.`marital_status` AS memberMaritalStatus,
	b.`company_type` AS memberCompanyType,
	b.`occupation` AS memberOccupation,
	b.`car_type` AS memberCarType
FROM `references`.`users_subscriptions` AS a
LEFT JOIN `references`.`users_subscriptions_adv_data` AS b ON b.`subscription_id`=a.`subscription_id`
WHERE a.`subscription_id`=".DB_VARS_SUBSCRIBTION_MEMBER.";", true);
    }

	public function UnSubscribe($Email, $UCode)
	{
        $Member = $this->Count("SELECT
    `subscription_id` AS Cnt
FROM `references`.`users_subscriptions`
WHERE `email`=".$this->Esc($Email)."
    AND `unsubscribe_code`=".$this->Esc($UCode).";");
        if(!$Member)
            throw new dmtException("Record is not found");

        $this->Begin();
        try
        {
            $this->Exec("DELETE FROM `references`.`users_subscriptions_adv_data`
WHERE `subscription_id`=".$Member.";");
            $this->Exec("DELETE FROM `references`.`users_subscriptions_users_subscriptions_themes`
WHERE `subscription_id`=".$Member.";");
            $this->Exec("DELETE FROM `references`.`users_subscriptions`
WHERE `subscription_id`=".$Member.";");

            $this->Commit();
        }
        catch(Exception $e)
        {
            $this->Rollback();
            throw new dmtException($e->getCode(), $e->getMessage(), true);
        }
        $this->OldUnsubscribe($Email, $UCode);

        /*
        $this->Exec("UPDATE `references`.`users_subscriptions`
SET `status`=0,
    `unregister`=NOW()
WHERE `subscription_id`=".$Member.";");

        //Support old system
        $this->OldUnsubscribe($Email, $UCode);
         */
	}

	public function GetSubscribe($Email)
	{
		return $this->Count("SELECT
	`subscription_id` AS Cnt
FROM `references`.`users_subscriptions`
WHERE `email`=".$this->Esc($Email));
	}

	public function GetThemesIdForModel($Model)
	{
		return $this->Get("SELECT
	`subscription_theme_id` AS i
FROM `references`.`users_subscriptions_themes_models`
WHERE `model_id`=".$Model.";");
	}

	public function GetThemesForModel($Model)
	{
		return $this->Get("SELECT
	a.`subscription_theme_id` AS subscriptionThemeId,
	b.`name` AS subscriptionThemeName,
	b.`description` AS subscriptionThemeDescription
FROM `references`.`users_subscriptions_themes_models` AS a
LEFT JOIN `references`.`users_subscriptions_themes` AS b ON b.`subscription_theme_id`=a.`subscription_theme_id`
WHERE `model_id`=".$Model.";");
	}

    public function GetSubscribeTheme($Brand, $Status = null)
    {
$this->Dump(__METHOD__);
        return $this->Get("SELECT
	`subscription_theme_id` AS subscriptionThemeId,
	`status` AS subscriptionThemeStatus,
	`alias` AS subscriptionThemeAlias,
	`name` AS subscriptionThemeName,
	`description` AS subscriptionThemeDescription
FROM `references`.`users_subscriptions_themes`
WHERE `brand_id`=".$Brand."
	AND `status`=".($Status ? $this->PrepareValue($Status) : 1).";");
    }


    public function GetMember()
    {
        return $this->Get("SELECT
	`subscription_id` AS memberId,
	`partners_division_id`,
	`email` AS memberEmail,
	`last_name` AS memberLastName,
	`first_name` AS memberFirstName,
	`middle_name` AS memberMiddleName,
	`age` AS memberAge,
	`gender` AS memberGender,
	`unsubscribe_code` AS memberUnsubscribeCode,
	`edit_subscribe_code` AS memberEditCode
FROM `references`.`users_subscriptions`
WHERE `subscription_id`=".DB_VARS_SUBSCRIBTION_MEMBER.";", true);
    }

    public function GetThemesForMember()
    {
        return $this->Get("SELECT
	b.`subscription_theme_id` AS subscriptionThemeId,
	b.`alias` AS subscriptionThemeAlias,
	b.`name` AS subscriptionThemeName,
	b.`name_genitive` AS subscriptionThemeNameGen
FROM `references`.`users_subscriptions_users_subscriptions_themes` AS a
LEFT JOIN `references`.`users_subscriptions_themes` AS b ON b.`subscription_theme_id`=a.`subscription_theme_id`
WHERE b.`status`=1
	AND `subscription_id`=".DB_VARS_SUBSCRIBTION_MEMBER.";");
    }

    public function GetThemesCount()
    {
        return $this->Get("SELECT COUNT(*)
FROM `references`.`users_subscriptions_themes`
WHERE `status`=1;");
    }





    public function MemberAuth($Email, $Password)
    {
        $R = $this->Get("SELECT
	a.`subscription_id` AS memberId,
	a.`email` AS memberEmail,
	a.`last_name` AS memberLastName,
	a.`first_name` AS memberFirstName,
	a.`middle_name` AS memberMiddleName,
	a.`age` AS memberAge,
	a.`gender` AS memberGender,
	a.`status` AS memberStatus,
	b.`marital_status` AS memberMaritalStatus,
	b.`company_type` AS memberCompanyType,
	b.`occupation` AS memberOccupation,
	b.`car_type` AS memberCarType
FROM `references`.`users_subscriptions` AS a
LEFT JOIN `references`.`users_subscriptions_adv_data` AS b ON b.`subscription_id`=a.`subscription_id`
WHERE `email`=".$this->Esc($Email)."
	AND `edit_subscribe_code`=".$this->Esc($Password).";", true);

        if(!$R)
            throw new dmtException("Login or passwors is error", 5);
		/*
        if($R["memberStatus"] == 0)
            throw new dmtException("Member unsubscribed", 5);
		 */
        $this->SetCurrentMember($R["memberId"]);
        return $R;
    }

    public function SetCurrentMember($Member)
    {
        $this->Member = $Member;
        $this->Set(DB_VARS_SUBSCRIBTION_MEMBER, $Member);
    }

    public function ResetCurrentMember()
    {
        if($this->Member)
            $this->Set(DB_VARS_SUBSCRIBTION_MEMBER, $this->Member);
    }

    public function GetMemberThemes()
    {
        return $this->Get("SELECT
	`subscription_theme_id` AS subscriptionThemeId
FROM `references`.`users_subscriptions_users_subscriptions_themes` AS a
WHERE `subscription_id`=".DB_VARS_SUBSCRIBTION_MEMBER.";");
    }

    public function RestorPassword($Member, $ECode, $Email)
    {
        $this->Exec("UPDATE `references`.`users_subscriptions`
SET `edit_subscribe_code`=".$this->Esc($ECode)."
WHERE `subscription_id`=".$Member.";");
        $this->OldRestorePassword($Email, $ECode);
    }





    /**
     * Support old system
     */
    protected function OldConnect()
    {
        $this->DB = new DBi("localhost", DB_SUBSCRIBE_USERNAME, DB_SUBSCRIBE_PASSWORD, DBS_SUBSCRIBE_TMP);
		$this->DB->Exec("SET NAMES CP1251;");
    }

    protected function OldDisconnect()
    {
        $this->DB = new DBi();
        $this->ResetCurrentMember();
    }


    protected function OldTableAU($OldEmail, $Email, $LastName, $Themes, $IP, $UCode = null, $ECode = null, $Stop = null)
    {
		$this->OldConnect();

        $Member = $this->Count("SELECT
	`id` AS Cnt
FROM `".DBS_SUBSCRIBE_TMP."`.`users` WHERE email=".$this->Esc($OldEmail));

        $Thms = array();
        foreach($Themes as $v)
            $Thms[$v["subscriptionThemeAlias"]] = 1;
        $Thms = serialize($Thms);
		if($Member)
		{
            $Set = array();
            $Set[] = "`name`=".$this->Esc($LastName);
            if($OldEmail != $Email)
                $Set[] = "`email`=".$this->Esc($Email);
            $Set[] = "`ip`=".$this->Esc($IP);
            $Set[] = "`sections`=".$this->Esc($Thms);
            //$Set[] = "`subdate`=NOW()";
            if($Stop !== null)
            {
                $Set[] = "`disabled`=".($Stop == 2 ? 1 : 0);
                //$Set[] = "`subdate`=NOW()";
            }
            $this->Exec("UPDATE `".DBS_SUBSCRIBE_TMP."`.`users`
SET ".implode(", ", $Set)."
WHERE `id`=".$Member.";");
        }
        else
        {
            $this->Exec("INSERT INTO `".DBS_SUBSCRIBE_TMP."`.`users`
	(`name`,
	`email`,
	`ip`,
	`salt`,
	`sections`,
	`unsubsalt`,
	`subdate`)
VALUES
	(".$this->Esc($LastName).",
	".$this->Esc($Email).",
	".$this->Esc($IP).",
	".$this->Esc($ECode).",
	".$this->Esc($Thms).",
	".$this->Esc($UCode).",
	NOW())");
        }
        $this->OldDisconnect();
    }

    protected function OldUnsubscribe($Email, $UCode)
    {
        $this->OldConnect();
        /*
        $this->Exec("UPDATE `".DBS_SUBSCRIBE_TMP."`.`users`
SET `disabled`=1,
    `subdate`=NOW()
WHERE `email`=".$this->Esc($Email)."
    AND `unsubsalt`=".$this->Esc($UCode));
         */

        $this->Exec("DELETE FROM `".DBS_SUBSCRIBE_TMP."`.`users`
WHERE `email`=".$this->Esc($Email)."
    AND `unsubsalt`=".$this->Esc($UCode).";");

        $this->OldDisconnect();
    }

    protected function OldRestorePassword($Email, $Password)
    {
        $this->OldConnect();
        $this->Exec("UPDATE `".DBS_SUBSCRIBE_TMP."`.`users`
SET `salt`=".$this->Esc($Password)."
WHERE `email`=".$this->Esc($Email).";");
        $this->OldDisconnect();
    }



    public function Test()
    {
        $Limit = 1000;
        $Start = 0;
        $ThemesA = array(
            "sect2"     => 1,
            "sect3"     => 2,
            "sect4"     => 3,
            "sect5"     => 4,
            "sect6"     => 5,
            "sect7"     => 6,
            "sect8"     => 7,
            "sect9"     => 8
        );
        while(1)
        {
            $this->OldConnect();
            $Data = $this->Get("SELECT *
FROM `".DBS_SUBSCRIBE_TMP."`.`users`
WHERE `email`<>''
LIMIT ".$Start.", ".$Limit.";");
            $this->OldDisconnect();
            if(!is_array($Data) || !sizeof($Data))
                break;
            foreach($Data as $v)
            {
                $Themes = array();
                $Sections = unserialize($v["sections"]);
                if(!$Sections)
                    $Themes = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
                elseif(array_key_exists("sect1", $Sections))
                    $Themes = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
                else
                {
                    foreach($Themes as $k => $t)
                    {
                        $Themes[] = $ThemesA[$k];
                    }
                }
                $this->Begin();
                try
                {
                    $this->Exec("INSERT INTO `references`.`users_subscriptions`
	(`email`,
	`last_name`,
	`first_name`,
	`middle_name`,
	`age`,
	`gender`,
	`unsubscribe_code`,
	`edit_subscribe_code`,
    `status`,
    `ip`,
    `register`,
    `update`,
    `password_type`)
VALUES
	(".$this->Esc($v["email"]).",
	".$this->Esc($v["name"]).",
	".$this->Esc($v["firstname"]).",
	".$this->Esc($v["patname"]).",
	".$this->CheckNull($v["age"]).",
	".($v["sex"] == "F" ? 0 : 1).",
	".$this->Esc($v["unsubsalt"]).",
	".$this->Esc($v["salt"]).",
    ".($v["disabled"] == 1 ? 0 : 1).",
	INET_ATON(".$this->Esc($v["ip"])."),
	".$this->Esc($v["subdate"]).",
	".$this->Esc($v["unsubdate"]).",
	2)
ON DUPLICATE KEY UPDATE
    `email`=VALUES(`email`),
    `last_name`=VALUES(`last_name`),
    `first_name`=VALUES(`first_name`),
    `middle_name`=VALUES(`middle_name`),
    `age`=VALUES(`age`),
    `gender`=VALUES(`gender`),
    `unsubscribe_code`=VALUES(`unsubscribe_code`),
    `edit_subscribe_code`=VALUES(`edit_subscribe_code`),
    `status`=VALUES(`status`),
    `ip`=VALUES(`ip`),
    `register`=VALUES(`register`),
    `update`=VALUES(`update`),
    `password_type`=VALUES(`password_type`);");

                    $Member = $this->DB->GetLastID();

                    if(!$Member)
                        $Member = $this->Count("SELECT
    `subscription_id` AS Cnt
FROM `references`.`users_subscriptions`
WHERE `email`=".$this->Esc($v["email"]).";");
                    if(!$Member)
                        continue;
                    $this->SetCurrentMember($Member);

                    //$this->SetAdvancedData(iconv( 'cp1251', 'utf-8//IGNORE', $v["familystatus"]), iconv( 'cp1251', 'utf-8//IGNORE', $v["company"]), iconv( 'cp1251', 'utf-8//IGNORE', $v["occupation"]), iconv( 'cp1251', 'utf-8//IGNORE', $v["haveauto"]));
                    $this->SetAdvancedData($v["familystatus"], $v["company"], $v["occupation"], $v["haveauto"]);
                    $this->SaveThemes($Themes);

                    $this->Commit();
                }
                catch (dmtException $e)
                {
                    $this->Rollback();
                }
            }
            $Start += $Limit;
        }

/*
        $this->Exec("SET character_set_results='cp1251';");
        return $this->Get("SELECT * FROM `references`.`users_subscriptions`
ORDER BY `subscription_id` DESC
LIMIT 10;");
 */
    }
}