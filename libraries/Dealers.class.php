<?php
class Dealers extends Controller
{
	/**
	 *
	 * @var Dealers
	 */
 	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Dealers
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->IgnoreTemplateVariables = true;
		$this->TemplateVariablesStart = "{";
		$this->TemplateVariablesEnd = "}";

		$this->Tpls		= array(
			"TplVars"		=> array(
				"count"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"actionId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"promoId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"citiesCount"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"cityId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"findText"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FIND_STRING)
				),
				"dealersId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FIND_STRING)
				),
				"excludeDealersIds"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FIND_STRING)
				),
				"facilityId"	=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"partnerTypeId"	=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"partnerId"	=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"regionId"	=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"countryId"	=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"federalDistrictId"	=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"facilityAlias"	=> array(
					"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_SEGURL)
				),
				"facilityAliasPool"	=> array(
					"pool"	=> array(
						"dataPath"	=> CONTROLLER_DATA_PATH_ROOT,
						"type"		=> CONTROLLER_POOL_TYPE_KEYS,
						"key"		=> array(
							"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_SEGURL)
						)
					)
				),
                "fStatus"	=> array(
					"filter"		=> array(FILTER_TYPE_VALUES, array("0", "1"))
				),
				"ref"			=>  array(
					"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_URL)
				),
				"refi"			=>  array(
					"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_URL)
				),
				//Долгота
				"gLg"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				//Широта
				"gLt"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
                //Статус
				"status"			=>  array(
					"filter"		=> array(FILTER_TYPE_VALUES, array("0", "1", "2", "3"))
				),
			)
		);

		$this->Modes = array(
			//Получить список всех дилеров, дилеров для заданного города или поиск дилера по строке
			"a"	=> array(
				"exec"			=> array("DealersProcessor", "GetDealersListFull"),
				"TplVars"		=> array("dealersId" => 0, "excludeDealersIds" => 0,  "cityId" => 0, "findText" => 0, "facilityId" => 0, "partnerId" => 0, "status" => 0, "fStatus" => 0)
			),

			//Получить список ближайших к пользователю дилеров
			"b"	=> array(
				"exec"			=> array("DealersProcessor", "GetNearestDealers"),
				"TplVars"		=> array("count" => 0, "citiesCount" => 0, "facilityId" => 0, "gLt" => 0, "gLg" => 0)
			),
			//Получить список ближайших к пользователю дилеров
			"c"	=> array(
				"exec"			=> array("DealersProcessor", "FindDealers"),
									//$DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null, $FacilityAlias = null
				"TplVars"		=> array("partnerTypeId" => 0, "cityId" => 0, "regionId" => 0, "federalDistrictId" => 0, "countryId" => 0, "facilityId" => 0,
											"gLt" => 0, "gLg" => 0, "count" => 0, "citiesCount" => 0, "actionId" => 0, "modelId" => 0, "promoId" => 0, "ref" => 0, "refi" => 0, "status" => 0, "facilityAliasPool" => 0)
			),
            //Получить список ближайших к пользователю дилеров
			"c1"	=> array(
				"exec"			=> array("DealersProcessor", "FindDealers2"),
									//$DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null, $FacilityAlias = null
				"TplVars"		=> array("partnerTypeId" => 0, "cityId" => 0, "regionId" => 0, "federalDistrictId" => 0, "countryId" => 0, "facilityId" => 0,
											"gLt" => 0, "gLg" => 0, "count" => 0, "citiesCount" => 0, "actionId" => 0, "modelId" => 0, "promoId" => 0, "ref" => 0, "refi" => 0, "status" => 0, "facilityAliasPool" => 0)
			),
			"d"	=> array(
				"exec"			=> array("DealersProcessor", "GetDealersListWithGeoFull"),
				"TplVars"		=> array("dealersId" => 0)
			),
			"e"	=> array(
				"exec"			=> array("DealersProcessor", "GetNotRelatedFacilities"),
				"TplVars"		=> array("dealersId" => 1)
			),
			"f"	=> array(
				"exec"			=> array("DealersProcessor", "GetDealersTypes"),
			),
			"g"	=> array(
				"exec"			=> array("DealersProcessor", "GetRelatedFacilities"),
				"TplVars"		=> array("dealersId" => 0)
			),
			"h"	=> array(
				"exec"			=> array("DealersProcessor", "GetRelatedFacilitiesFull"),
				"TplVars"		=> array("dealersId" => 0)
			),
			"i"	=> array(
				"exec"			=> array("DealersProcessor", "GetNotRelatedFacilitiesFull"),
				"TplVars"		=> array("dealersId" => 1)
			),
			//Получить список ближайших к пользователю дилеров
			"j"	=> array(
				"exec"			=> array("DealersProcessor", "FindDealersWN"),
									//$DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
									//$Latitude = null, $Longitude = null, $Ref = null, $RefFrameId = null, $FacilityAlias = null
				"TplVars"		=> array("partnerTypeId" => 0, "cityId" => 0, "regionId" => 0, "federalDistrictId" => 0, "countryId" => 0, "facilityId" => 0,
										"gLt" => 0, "gLg" => 0, "count" => 0, "citiesCount" => 0, "actionId" => 0, "modelId" => 0, "promoId" => 0, "ref" => 0, "refi" => 0, "status" => 0, "facilityAliasPool" => 0)
			),
			//Получить список всех дилеров
			"k"	=> array(
				"exec"			=> array("DealersProcessor", "FindDealers"),
			),
		);
	}
}