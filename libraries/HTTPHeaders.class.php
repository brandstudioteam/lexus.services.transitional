<?php
/**
 *
 * @author Игорь Борщенков
 * @copyright Igor Borschenkov, 2009-2015
 * @version 2.1
 * @package basic
 * @subpackage utils
 *
 */
class HTTPHeaders
{
	private static $LifeTime;

	public static function NotFound()
	{
		header("http/1.0 404 not found");
	}

	public static function Forbidden()
	{
		header("http/1.0 403 forbidden");
	}

	public static function PartialContent()
	{
		header("http/1.1 206 partial content");
	}

	public static function Warning($ErrorMessage)
	{
		header("warning: 99# ".$ErrorMessage);
	}

	/**
	 *
	 * Устанавливает заголовки, запрещающие кешировать данные
	 */
	public static function NoCache()
	{
		header("Cache-Control: no-store, no-cache, max-age=0, must-revalidate");
	}

	public static function CasheRevalidate()
	{
		header("Cache-Control: private, must-revalidate");
	}

	/**
	 *
	 * Устанавливает заголовки, разрешающие кеширование данных
	 */
	public static function Cache()
	{
		self::CashControl(0);
		header("Cache-Control: max-age=".self::GetLifeTime(), "no-transform");
	}

	/**
	 *
	 * Устанваливает в заголовке тип используемого кеша данных: 0 - приватный, 1 - публичный
	 * @param int $ControlType - тип используемого кеша
	 */
	public static function CashControl($ControlType)
	{
		$CT = array("private", "public");
		if(is_int($ControlType))
		{
			if(isset($CT[$ControlType])) $ControlType = $CT[$ControlType];
		}
		elseif(!in_array(strtolower($ControlType, $CT))) $ControlType = $CT[0];
		header("Cache-Control: ".$ControlType);
	}

	/**
	 *
	 * Устанавливает заголовок Last-Modified, даты последнего изменения данных
	 * @param unknown_type $DateTime
	 */
	public static function SetModified($LastUpdate)
	{
		self::CasheRevalidate();
		header("Last-Modified: ".gmdate("D, d M Y H:i:s", $LastUpdate)." GMT");
	}

	/**
	 *
	 * Устанавливае время актуальности кешированных данных
	 * @param int $Time - время актуальности данных.
	 */
	public static function SetLifeTime($Time)
	{
		self::$LifeTime = $Time;
	}

	public static function GetLifeTime()
	{
		if(!self::$LifeTime) self::$LifeTime = SESSION_TIME_DEFAULT;
		return self::$LifeTime;
	}

	/**
	 *
	 * Устанавливает заголовок Not Modified.
	 */
	public static function NotModified()
	{
		header("HTTP/1.1 304 Not Modified");
	}

	/**
	 *
	 * Сравнивает время последнего изменения данных с временем из запроса
	 * @param int $LastModified - время последнего изменения данных в формате timestamp
	 */
	public static function IsModified($LastModified)
	{
		if (isset($_SERVER["HTTP_IF_MODIFIED_SINCE"]))
			$MT = strtotime(preg_replace("/;.*$/", "", $_SERVER["HTTP_IF_MODIFIED_SINCE"]));
		else $MT = 0;
		return (boolean) ($LastModified > $MT);
	}

	public static function CheckModified($LastModified)
	{
		$R = self::IsModified($LastModified);
		self::SetModified($LastModified);
		if(!$R) self::NotModified();
		return $R;
	}

	public static function SetExpires($DateTime)
	{
		header("Expires: ".gmdate("D, d M Y H:i:s", $DateTime)." GMT");
	}

	/**
	 *
	 * Устанавливает заголовки mime-типа
	 */
	public static function SetContent($Type, $Charset = null)
	{
		header("Content-type: ".$Type.($Charset !== false ? "; charset=".($Charset ? $Charset : CHARSET) : ""));
	}

	public static function Location($URL, $Secure = false)
	{
		if(mb_strpos($URL, "http://") === false && mb_strpos($URL, "https://") === false) $URL = ($Secure ? "https://" : "http://").$URL;
		header("Location: ".$URL);
	}

	/**
	 *
	 * Устанавливает заголовки для сжатого содержимого
	 * @param integer $Lenght - размер отправляемых данных
	 * @param string $Type - тип сжатия: gzip или x-gzip
	 */
	public static function Compressed($Lenght, $Type = null)
	{
		if(!$Type) $Type = "gzip";
		header("content-encoding: ".$Type);
		header("vary: accept-encoding");
		header("content-length: ".$Lenght);
	}

	/**
	 *
	 * Устанавливает заголовки при загрузке файлов с сервера
	 * @param string $FileName - имя файла
	 * @param integer $Length - размер файла
	 * @param timestamp $LastUpdate - дата последнего обновления
	 * @param string $Mime - mime-тип файла
	 * @param string $Range - начало диапазона отправляемых байт
	 */
	public static function SendFile($FileName, $Length, $Mime = null, $Range = null)
	{
		$Mime = (preg_match('~MSIE ([0-9].[0-9]{1,2})~ui', $_SERVER['HTTP_USER_AGENT'])) ? //preg_match('~Opera(/| )([0-9].[0-9]{1,2})~ui', $_SERVER['HTTP_USER_AGENT']) ||
					'application/octetstream' :
					($Mime ? $Mime : 'application/octet-stream');

		header("Content-Type: ".$Mime."; name=\"".urldecode($FileName)."\"");
		header("Cache-Control: private");
		header("Content-Length: " .($Length - $Range));
		header("Content-Transfer-Encoding: binary");
		header("Content-Disposition: attachment; filename=\"".$FileName."\"");

		if($Range)
		{
			self::PartialContent();
			header("accept-ranges: bytes");
			header("content-length: ".($Length - $Range));
			header("content-range: bytes ".$Range."-".($Length - 1)."/".$Range);
		}
	}

	public static function AccessRanges()
	{
		header("accept-ranges: bytes");
	}

	public static function ContentLength($Length)
	{
		header("Content-length: ".$Length, true);
	}

	public static function ContentRange($From, $To)
	{
		header("Content-Range: bytes ".$From."-".$To);
	}

	public static function ConnectionClose()
	{
		header('Connection: close', true);
	}

	public static function ContentDisposition($FileName)
	{
		header('Content-Disposition: attachment; filename="'.$FileName.'";');
	}

	public static function ContentTransferEncoding()
	{
		header("Content-Transfer-Encoding: binary");
	}

	public static function SetMobileAppSession($Session)
	{
		header("X-Maps-Id: ".$Session);
	}
}