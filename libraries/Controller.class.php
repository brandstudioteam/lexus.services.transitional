<?php
/**
 *
 * Базовый класс классов-контроллеров.
 * Изменения в версии 6.0.0.1 базового класса контроллеров:
 * Классы контроллеры должны иметь метод Sets, в котором задаются настройки контроллера.
 * Настройки контроллера записываются в массив $this->Modes в соответствии с документом controller_sets_definition.
 *
 * В данной версии предполагается следующая структура возвращаемого массива:
 * array(	"result" => <returned_data>,
 * 			["message" => <message_text>],
 * 			["errors" => array("variable_name" => <error_message>, ...)],
 * 			[<key_advanced_data> => <value_advanced_data>, ...]
 *
 *
 * @author Игорь (igor@borschenkov.ru)
 * @version 6.0.0.2
 * @package BasicEngine
 *
 */


define("CONTROLLER_POOL_TYPE_KEYS",											1);
define("CONTROLLER_POOL_TYPE_VARIABLES",									2);
define("CONTROLLER_POOL_TYPE_VALUES",										3);

define("CONTROLLER_DATA_PATH_ROOT",											1);
define("CONTROLLER_DATA_PATH_CURRENT",										2);

class Controller extends Base
{
	private $Gettitg = array("Mode", "ContentType", "Template", "ErrorTemplate", "SendType", "IsError", "IsFileUpload", "Errors",
							"Message", "Returned", "Result", "RetData");
	protected $Classes = array();
	/**
	 *
	 * Массив настроек режимов
	 * @var array
	 */
	protected $Modes = array();
	/**
	 *
	 * Обработанные клиентские данные
	 * @var array
	 */
	protected $ClnData = array();
	protected $Data;
	protected $Z;
	protected $FileID;

	/**
	 *
	 * Кеш текстовых элементов
	 * @var array
	 */
	protected $langCache = array();
	/**
	 *
	 * Текущий режим
	 * @var string
	 */
	protected $Mode;
	/**
	 *
	 * Специфические сообщения об ошибке для каждой входящей переменной
	 * @var array
	 */
	protected $Errors = array();
	/**
	 *
	 * Флаг наличия ошибки
	 * @var boolean
	 */
	protected $IsError = false;
	/**
	 *
	 * Общее сообщение для пользователя
	 * @var string
	 */
	protected $Message;
	/**
	 *
	 * Массив возвращаемых клиенту данных
	 * @var array
	 */
	protected $Returned = array();
	/**
	 *
	 * Код результата выполнения
	 * @var integer
	 */
	protected $Result = null;

	protected $RetData;

	protected $SendType;
	protected $ContentType;

	protected $Template;
	protected $ErrorTemplate;

	/**
	 *
	 * Директория хранения временных файлов при загрузке
	 * @var string
	 */
	private $UsedTmpDir;

	private $CurrDef;

	protected $Tpls;

	protected $IgnoreTemplateVariables;

	protected $TemplateVariablesStart = "{";

	protected $TemplateVariablesEnd = "}";

	protected $FormUID = "";

    protected $SysVariables = array(
        "__captcha"               => array(
            "filter"    => array(FILTER_TYPE_TYPE, FILTER_TT_CAPTHCA),
            "required"  => 2,
            "__type"    => "sys"
        ),
        "__files"               => 1,
        "__fileName"            => 1,
        "__fileOriginName"      => 1,
        "__fileMimeType"        => 1
    );

    protected $DataSetStart;
    protected $DataSetEnd;

    /**
	 *  Диспетчер. Управляет процессом базовой обработки клиентских данных
	 *
	 * @param array $Data клиентские данные
	 * @return Массив вида:
	 * array(	"result" => <result_code>,
 	 * 			["message" => <message_text>],
	 * 			["errors" => array("variable_name" => <error_message>, ...)],
	 * 			["data" => array("variable_name" => [<value> || array(<value>, ...) || array(<key> => <value>, ...) || array(<key> => array(<key> => <value>, ...), ...) || array(array(<key> => <value>, ...), ...)], ...)],
	 * 			["Items" => <array>,]
	 * 			["Form" => <array_as_form_definition>,]
	 * 			[<key_advanced_data> => <value_advanced_data>, ...]
	 * 			["Sets" => <array_as_sets_definition>,]
	 * 			["ItemsDef" => <array_as_IObjects_definition>,]
	 * 			["Report" => <debug_report_text>])
	 *
	 * Код результата ("result") должен быть выставлен всегда.
	 *
	 * Общее сообщение ("message"), устанавливается в случае необходимости.
	 *
	 * Массив сообщений об ошибках для соответсвующих входящих переменных (полей данных).
	 * Устанавливается в случае возникновения ошибок. В противном случае параметр не устанавливается.
	 * При дополнительной обработке данных в случае возникновения ошибки, сообщения об ошибках добавляются в массив $this-Errors.
	 * Если для ошибки нет специфического сообщения или ошибка не привязана к конкретной входящей переменной поднимается флаг $this->IsError;
	 * ВНИМАНИЕ!
	 * Подстановка массива ошибок в возвращаемый массив производится автоматически методом Dispatcher при наличии элементов в массиве $this->Errors.
	 *
	 * Массив значений входящих данных для соответствующих входящих переменных (полей данных).
	 * Устанавливается в случае, если имеется необходимость вернуть клиенту полученные от клиента данные (например при работе с формой).
	 * Управляется параметром "IsForm".
	 * ВНИМАНИЕ!
	 * Подстановка данных в возвращаемый массив производится автоматически методом Dispatcher на основании управляющего параметра.
	 *
	 * Параметр "Report" устанавливается в случае, если обмен данными между клиентом и сервером осуществляется ассинхронно (через ajax), разрешена отладка сайта и разрешено при отладке использовать консоль отладки
	 * ВНИМАНИЕ!
	 * Подстановка параметра в возвращаемый массив производится автоматически методом Dispatcher.
	 *
	 */
	final public function Dispatcher($Mode, $Data)
	{
		$this->Sets();
		if($Mode == "p")
			$this->Dump("ACTIVATION ", $Data);
		try {
			$this->Mode = $Mode;
			if(!$Mode || $Data && !is_array($Data)) throw new dmtException("Argument \"Data\" is error");
			if(!is_array($Data))
				$Data = array();
			$this->Data = &$Data;
			if(!isset($this->Modes[$this->Mode]))
			{
				throw new dmtException("Command is not found or access denid. Mode = '".$this->Mode."'");
			}
			if(array_key_exists("z", $Data))
			{
				$this->Z = $Data["z"];
				unset($Data["z"]);
			}
			$this->FormUID = WS::Init()->GetFormUID();
			$this->CurrDef = $this->Modes[$this->Mode];
			$this->Prepare($this->CurrDef, $Data);
			if(isset($this->CurrDef["Variables"]))
			{
				if(isset($this->CurrDef["AsArray"]) && is_array($Data))
				{
					foreach($Data as $d)
						$this->CheckClientData($d, $this->CurrDef["Variables"], $this->Errors, $this->ClnData);
				}
				else
				{
					$this->CheckClientData($Data, $this->CurrDef["Variables"], $this->Errors, $this->ClnData);
				}
			}
			$this->SendType = isset($this->CurrDef["SendType"]) ? $this->CurrDef["SendType"] : SEND_TYPE_JSON;
			if(array_key_exists("ContentType", $this->CurrDef))
				$this->ContentType = $this->CurrDef["ContentType"];
			if($this->IsError)
			{
				if(array_key_exists("ErrorMethod", $this->CurrDef))
					call_user_func(array($this, $this->CurrDef["ErrorMethod"]));
				$this->Result = 1;
				if(!$this->Message && array_key_exists("iserror", $this->CurrDef))
					$this->Message = $this->CurrDef["iserror"];
				if(array_key_exists("IsForm", $this->CurrDef) && $this->CurrDef["IsForm"])
					$this->RetData = $Data;
				$this->SendType = isset($this->CurrDef["ErrorSendType"]) ? $this->CurrDef["ErrorSendType"] : (isset($this->CurrDef["SendType"]) ? $this->CurrDef["SendType"] : SEND_TYPE_JSON);
				$this->ContentType = (array_key_exists("ErrorContentType", $this->CurrDef))? $this->CurrDef["ErrorContentType"] :
											((array_key_exists("ContentType", $this->CurrDef))? $this->CurrDef["ContentType"] : "text/plain");
				if(isset($this->CurrDef["ErrorTemplate"]))
					$this->ErrorTemplate = $this->CurrDef["ErrorTemplate"];
			}
			else
			{
				try {
					if(isset($this->CurrDef["getFiles"]))
					{
						$Fs = $this->CurrDef["getFiles"];
						$Keep = isset($this->CurrDef["keepFiles"]) && $this->CurrDef["keepFiles"];
						if(is_array($Fs))
						{
							foreach($Fs as $k => $v)
							{
								$Files = self::GetCachedFiles(WS::Init()->GetFormUID().$k, $Keep);
								if(is_array($Files) && sizeof($Files))
								{
									$this->ClnData[$k] = $Files;
								}
								else
								{
									if($v) {
										//TODO: Set Error
									}
									else {
										$this->ClnData[$k] = null;
									}
								}
							}
						}
						else $this->ClnData[] = self::GetCachedFiles(WS::Init()->GetFormUID().$Fs, $Keep);
					}
                    elseif(isset($this->CurrDef["getFilesProps"]))
					{
						$Fs = $this->CurrDef["getFilesProps"];
						$Keep = isset($this->CurrDef["keepFiles"]) && $this->CurrDef["keepFiles"];
						if(is_array($Fs))
						{
							foreach($Fs as $k => $v)
							{
								$Files = self::GetCachedFiles(WS::Init()->GetFormUID().$k, $Keep, true);
								if(is_array($Files) && sizeof($Files))
								{
									$this->ClnData[$k] = $Files;
								}
								else
								{
									if($v) {
										//TODO: Set Error
									}
									else {
										$this->ClnData[$k] = null;
									}
								}
							}
						}
						else $this->ClnData[] = self::GetCachedFiles(WS::Init()->GetFormUID().$Fs, $Keep, true);
					}
					if(array_key_exists("Method", $this->CurrDef))
						call_user_func(array($this, $this->CurrDef["Method"]));
					elseif(array_key_exists("exec", $this->CurrDef))
						$this->Returned = $this->CallFunct($this->CurrDef["exec"], $this->ClnData);
					$this->SetResult(1);
					if(isset($this->CurrDef["Template"]))
						$this->Template = $this->CurrDef["Template"];
					if(isset($this->CurrDef["redirect"]))
						WS::Init()->Redirect($this->CurrDef["redirect"]);
				}
				catch (dmtException $e)
				{
					if($e->getCode() == 77777 || $e->getCode() == 77775)
					{
						throw new dmtFailException($e->getMessage(), $e->getCode());
					}
					$this->SetResult(2, $e->getCode());
					if(array_key_exists("IsForm", $this->CurrDef) && $this->CurrDef["IsForm"])
					{
						$this->RetData = $Data;
					}
					$this->SendType = isset($this->CurrDef["ErrorSendType"]) ? $this->CurrDef["ErrorSendType"] : (isset($this->CurrDef["SendType"]) ? $this->CurrDef["SendType"] : SEND_TYPE_JSON);
					$this->ContentType = (array_key_exists("ErrorContentType", $this->CurrDef))? $this->CurrDef["ErrorContentType"] :
												((array_key_exists("ContentType", $this->CurrDef))? $this->CurrDef["ContentType"] : "text/plain");
$this->Dump(__CLASS__."::".__METHOD__.", Exception_1", isset($this->CurrDef["ContentType"]) ? $this->CurrDef["ContentType"] : "", $this->ContentType, isset($this->CurrDef["SendType"]) ? $this->CurrDef["SendType"] : "", $this->SendType);
					if(isset($this->CurrDef["ErrorTemplate"]))
						$this->ErrorTemplate = $this->CurrDef["ErrorTemplate"];
				}
			}
			$this->ConvMCode();
		}
		catch (dmtException $e)
		{
			$this->SendType = is_array($this->CurrDef) && array_key_exists("ErrorContentType", $this->CurrDef) ?  $this->CurrDef["ErrorContentType"] : (
								isset($this->CurrDef["SendType"]) ? $this->CurrDef["SendType"] : SEND_TYPE_JSON);
			$this->SetResult(0);
		}
		if($this->Z)
		{
			if(!is_array($this->Returned))
				$this->Returned = array($this->Returned);
			$this->Returned["z"] = $this->Z;
		}
	}

	/**
	 *
	 * Выполняет валидацию входящих даных
	 * @param array $Data входящие данные.
	 * @throws dmtException в случае несоответствия формата данных или описания данных
	 */
	private function CheckClientData(&$Data, $Define, &$Errors, &$Values, $Rec = null)
	{
		foreach($Define as $k => $v)
		{
			$Err = false;
			if($v === null)
			{
				$Values[$k] = null;
				continue;
			}
			$Values[$k] = array();
			if(!isset($v['isfile']) || !$v['isfile']) //Если приходящие данные не являются файлом
			{
				if(array_key_exists("pool", $v))
				{
					$Pool = array();
					if(isset($v["pool"]["dataPath"]) && $v["pool"]["dataPath"] == CONTROLLER_DATA_PATH_ROOT)
					{
						$CurrData = &$this->Data;
						$CurrValues = &$this->ClnData;
					}
					else
					{
						$CurrData = &$Data;
						$CurrValues = &$Values;
					}
					
					$Exclude = array();
					$Include = null;
					if(isset($v["pool"]["enableValues"]) && is_array($v["pool"]["enableValues"]))
					{
						$Include = $v["pool"]["enableValues"];
					}
					if(isset($v["pool"]["enableMethod"]) && is_array($v["pool"]["enableMethod"]))
					{
						$Include = $this->CallFunct($v["pool"]["enableMethod"]);
					}
					else
					{
						//Удаляем из данных описанные переменные
						foreach($Define as $checkK => $checkV)
						{
							$Exclude[] = $checkK;
							if(isset($v["alias"]))
							{
								if(!is_array($v["alias"]))
									$v["alias"] = array($v["alias"]);
								foreach($v["alias"] as $aliasK => $aliasV)
								{
									$Exclude[] = $aliasK;
								}
							}
						}
					}
					
					$E = null;
					foreach($CurrData as $ck => $cv)
					{
						if($Include && !in_array($ck, $Exclude) || !$Include && (in_array($ck, $Exclude) || array_key_exists($ck, $CurrValues))) {
							continue;
						}
						if($this->IgnoreTemplateVariables)
						{
							if($cv == $this->TemplateVariablesStart.$ck.$this->TemplateVariablesEnd)
								continue;
						}
						switch($v["pool"]["type"])
						{
							case CONTROLLER_POOL_TYPE_KEYS:
								if($this->CheckFilter($ck, array_key_exists("key", $v["pool"]) ? $v["pool"]["key"] : $v["pool"], $E, $ck, false, $k, true) !== false)
									$Pool[] = $ck;
								break;
							case CONTROLLER_POOL_TYPE_VALUES:
								if($this->CheckFilter($cv, $v["pool"], $E, $cv, false, $k, true) !== false)
									$Pool[] = $cv;
								break;
							case CONTROLLER_POOL_TYPE_VARIABLES:
								$this->CheckFilter($ck, $v["pool"]["key"], $E, $ck, false, $k, true);
								$this->CheckFilter($cv, $v["pool"], $E, $cv, false, $k, true);
								if($ck !== false && $cv !== false)
									$Pool[$ck] = $cv;
								break;
							default:
								throw new dmtException("Pool type error");
						}
					}
					$Values[$k] = $Pool;
					return;
				}

				$CurrentKey = null;
				if(array_key_exists($k, $Data) || isset($v["alias"]))
				{
					if(array_key_exists($k, $Data))
					{
						$CurrentKey = $k;
					}
					else
					{
						if(!is_array($v["alias"]))
							$v["alias"] = array($v["alias"]);
						foreach($v["alias"] as $aliasV)
						{
							if(array_key_exists($aliasV, $Data))
							{
								$CurrentKey = $aliasV;
								break;
							}
						}
					}
				}
				if($CurrentKey !== null)
				{
					if($this->IgnoreTemplateVariables)
					{
						if($Data[$CurrentKey] == $this->TemplateVariablesStart.$CurrentKey.$this->TemplateVariablesEnd)
							continue;
					}

					if(array_key_exists("Variables", $v) || array_key_exists("TplVars", $v))
					{
						//Подготовить переменные к обработке
						$this->Prepare($v, $Data); //Реконфигурировать метод для работы с рекурсивными данными

						if(is_array($Data[$CurrentKey]) && array_key_exists(0, $Data[$CurrentKey]))
						{
							foreach ($Data[$CurrentKey] as $j => $jv)
							{
								$Values[$k][$j] = array();
								$this->CheckClientData($jv, $v["Variables"], $Err, $Values[$k][$j], true);
							}
						}
						else
						{
							$this->CheckClientData($Data[$CurrentKey], $v["Variables"], $Err, $Values[$k], true);
						}

					}
					else
					{
						if(is_array($Data[$CurrentKey])) $D = $Data[$CurrentKey];
						else $D = array($Data[$CurrentKey]);
						foreach ($D as $j => $jv)
						{
							$Val = null;
											//$Data, $Define, &$Error, &$Values, $NotEmpty = null, $Key = null
							$this->CheckFilter($jv, $v, $Err, $Val, false, $k);
							$this->CheckLimit($Val, $v, $Err);
							if($Err) $Errors[$CurrentKey] = $Err;
							else
							{
								$Val = $this->CallBack($v, $Val);
								if(!$this->IsError)
								{
									if(is_array($Data[$CurrentKey])) $Values[$k][$j] = $Val;
									else $Values[$k] = $Val;
								}
							}
						}
					}
				}
				elseif(array_key_exists("required", $v) && $v["required"])
				{
					$this->SetError($Errors[$k], isset($v["iserror"]) ? $v["iserror"] : "ErrRequired", $Values[$k]);
				}
				elseif(array_key_exists("default", $v))
				{
					/*
					 * Проверка необходимости установки значения по умолчанию из функции или метода некоторого класса
					 */
			 		if(is_array($v["default"]) && array_key_exists("exec", $v["default"]))
			 			$Values[$k] = $this->CallFunct($v["default"]["exec"]);
			 		else $Values[$k] = $v['default'];
				}
				else $Values[$k] = null;
			}
			//Ожидается файл
			else
			{
				if($Rec) throw new dmtException("Error configuration mode \"".$this->Mode."\".");
				if(!isset($_FILES) || !array_key_exists($k, $_FILES) || (is_array($_FILES[$k]["name"]) && sizeof($_FILES[$k]["name"]) == 0))
				{
					$this->IsError = true;
					if(isset($v["iserror"])) $Errors[$k] = $v["iserror"]; //Language::Init()->GetText($v["iserror"]);
				}
				else
				{
					if(is_array($_FILES[$k]["name"])) $D=$_FILES[$k];
					else $D=array("name"	=> array($_FILES[$k]["name"]),
								"type"		=> array($_FILES[$k]["type"]),
								"size"		=> array($_FILES[$k]["size"]),
								"error"		=> array($_FILES[$k]["error"]),
								"tmp_name"	=> array($_FILES[$k]["tmp_name"])
								);
					$FileDef = $v['isfile'];
					$FilesKey = $this->FormUID.$k;
					foreach($D["name"] as $j => $jv)
					{
						if(!is_uploaded_file($D['tmp_name'][$j]))
						{
							if(!is_array($Errors)) $Errors = array();
							$this->SetError($Errors[$k], isset($FileDef["iserror"]) ? $FileDef["iserror"] : "ErrFileUpload", $Values[$k], $FilesKey, $_FILES[$k], $j);
							continue;
						}
						$Error = null;
						if(isset($v["filter"]))
						{
							if(isset($v["filter"]["type"]) && $v["filter"]["type"] == FILTER_TYPE_REGEXP || isset($v["filter"][0]) == FILTER_TYPE_REGEXP)
							{
								if(!$this->CheckFilter($jv, $v, $Err, $jv, true, $k."::".$j))
								{
									if($Err) $Errors[$k] = $Err;
									continue;
								}
							}
						}
						if(array_key_exists('limits', $FileDef))
						{
							$Curr = $FileDef['limits'];
							$FilesCount = $Curr[0] == 0 ? 0 : (intval($Curr[0]) > 0 ? $Curr[0] : 1);
							$FileMaxSize = array_key_exists(1, $Curr) ? ($Curr[1] == 0 ? 0 : (intval($Curr[1]) > 0 ? $Curr[1] : CONTROLLER_UPLOADED_FILE_MAX_SIZE)) : CONTROLLER_UPLOADED_FILE_MAX_SIZE;
							$FileAllMaxSize = array_key_exists(2, $Curr) ? ($Curr[2] == 0 ? 0 : (intval($Curr[2]) > 0 ? $Curr[2] : $FilesCount * CONTROLLER_UPLOADED_FILE_MAX_SIZE)) : $FilesCount * CONTROLLER_UPLOADED_FILE_MAX_SIZE;
							if($FilesCount > $this->GetCachedFilesCount($FilesKey) + 1)
							{
								if(!is_array($Errors)) $Errors = array();
								$this->SetError($Errors[$k], "ErrFileCountLimit", $Values[$k], $FilesKey, $_FILES[$k], $j);

								continue;
							}
							if($D["size"][$j] > $FileMaxSize)
							{
								if(!is_array($Errors)) $Errors = array();
								$this->SetError($Errors[$k], "ErrFileFLimit", $Values[$k], $FilesKey, $_FILES[$k], $j);
								continue;
							}
							if($this->GetCachedFilesSize($FilesKey) + $D["size"][$j] > $FileAllMaxSize)
							{
								if(!is_array($Errors)) $Errors = array();
								$this->SetError($Errors[$k], "ErrFileLimit", $Values[$k], $FilesKey, $_FILES[$k], $j);
								continue;
							}
						}
						else
						{
							if(array_key_exists('limit', $FileDef))
							{
$this->Dump(__METHOD__.": ".__LINE__, "Used depricated definitions - section 'limit' in isfile partition in file ".__FILE__);
								if(!array_key_exists("value", $FileDef['limit']))
									$FileDef['limit']['value'] = $FileDef['limit'][0];
								if(!array_key_exists("iserror", $FileDef['limit']) && isset($FileDef['limit'][1]))
									$FileDef['limit']["iserror"] = $FileDef['limit'][1];
								if($D["size"][$j] > $FileDef['limit']['value'])
								{
									if(!is_array($Errors)) $Errors = array();
									$this->SetError($Errors[$k], isset($FileDef['limit']["iserror"]) ? $FileDef['limit']["iserror"] : "ErrFileLimit", $Values[$k], $FilesKey, $_FILES[$k], $j);
									continue;
								}
							}
							//else throw new dmtException("Error configuration mode \"".$this->Mode."\".");

							if(array_key_exists('flimit', $FileDef))
							{
$this->Dump(__METHOD__.": ".__LINE__, "Used depricated definitions - section 'flimit' in isfile partition in file ".__FILE__);
								if(!array_key_exists("value", $FileDef['flimit']))
									$FileDef['flimit']['value'] = $FileDef['flimit'][0];
								if(!array_key_exists("iserror", $FileDef['flimit']) && isset($FileDef['flimit'][1]))
									$FileDef['flimit']["iserror"] = $FileDef['flimit'][1];
								if($FileDef['flimit']['value'] > $this->GetCachedFilesSize($FilesKey) + $D["size"][$j])
								{
									if(!is_array($Errors)) $Errors = array();
									$this->SetError($Errors[$k], isset($FileDef['flimit']["iserror"]) ? $FileDef['flimit']["iserror"] : "ErrFileFLimit", $Values[$k], $FilesKey, $_FILES[$k], $j);
									continue;
								}
							}
						}
						if(array_key_exists('mime', $FileDef))
						{
							/*
							$mimetype = FileSys::CheckMIME($D['tmp_name'][$j],
														array_key_exists('accept', $FileDef['mime']) ? $FileDef['mime']['accept'] : null,
														array_key_exists('except', $FileDef['mime']) ? $FileDef['mime']["except"] : null);
							*/
							$mimetype = FileSys::CheckMIME2(FileSys::GetExtFile($D["name"][$j]),
														array_key_exists('accept', $FileDef['mime']) ? $FileDef['mime']['accept'] : null,
														array_key_exists('except', $FileDef['mime']) ? $FileDef['mime']["except"] : null);
							if(!$mimetype)
							{
								if(!is_array($Errors)) $Errors = array();
								$this->SetError($Errors[$k], isset($FileDef['mime']["iserror"]) ? $FileDef['mime']["iserror"] : "ErrFileMIME", $Values[$k], $FilesKey, $_FILES[$k], $j);
								continue;
							}

							/*
							$mimetype = Tools::MimeContentType(false, $D['tmp_name'][$j], true);
							if(!$mimetype && !$D['type'][$j]) $mimetype = Tools::MimeContentType($D['tmp_name'][$j]);
							elseif($D['type'][$j])
							{
								$mimetype = explode("/", $D['type'][$j]);
								$mimetype = $mimetype[1];
							}
							if(!$mimetype || array_key_exists('accept', $FileDef['mime']) && !in_array($mimetype, $FileDef['mime']['accept'])
									|| array_key_exists('except', $FileDef['mime']) && in_array($mimetype, $FileDef['mime']['except']))
							{
								if(!is_array($Errors)) $Errors = array();
								$this->SetError($Errors[$k], isset($FileDef['mime']["iserror"]) ? $FileDef['mime']["iserror"] : "ErrFileMIME", $Values[$k], $FilesKey, $_FILES[$k], $j);
								continue;
							}
							*/
						}
						if(array_key_exists('named', $FileDef))
						{
							if(mb_strpos(mb_strtolower($FileDef['named']), "auto") == 0)
							{
								$Name = FileSys::GetExtFile($D["name"][$j]);
								$t = mb_strpos($FileDef['named'], "(");
								if($t !== false) $t = mb_substr($FileDef['named'], $t+1, mb_strpos($FileDef['named'], ")") - $t);
								else $t = null;
								if(is_int($t)) $t = $t - mb_strlen($Name);
								if(is_null($t) || $t >= 32) $Name = WS::Init()->GetUID(16, true, User::Init()->GetSessionId().$j).($Name ? ".".$Name : "");
								else $Name = WS::Init()->GetUID($t-1, false, "f").($Name ? ".".$Name : "");
							}
							else $Name = call_user_func_array(isset($FileDef['named'][0]) ? $FileDef['named'][0] : $FileDef['named'], isset($FileDef['named'][1]) ? $FileDef['named'][1] : null);
						}
						elseif(isset($FileDef["name"]))
						{
							$Name = $FileDef["name"] == "_auto" ? md5(microtime(true)) : $FileDef['name'];
						}
						else $Name = $D["name"][$j];

						if(!FileSys::GetExtFile($Name)) {
							$Name .= ".".FileSys::GetExtFile($D["name"][$j]);
						}
$this->Dump(__METHOD__.": ".__LINE__, "+++ 1");
						if(array_key_exists('savepath', $FileDef) || array_key_exists("move", $FileDef) && $FileDef["move"])
						{
							$fn = ((array_key_exists('savepath', $FileDef)) ?
									((substr($FileDef['savepath'],-1) == "/") ? $FileDef['savepath'] : $FileDef['savepath']."/") :
									($this->UsedTmpDir ? ((substr($this->UsedTmpDir,-1) == "/") ? $this->UsedTmpDir : $this->UsedTmpDir."/") :
									null)).$Name;
							if($fn)
							{
								if(!move_uploaded_file($D['tmp_name'][$j], $fn)) throw new dmtException("File Upload Error");
								if(array_key_exists('base64', $FileDef) || array_key_exists('call', $v))
								{
									$tmp_file = file_get_contents($fn);
									if(array_key_exists('corrected', $FileDef) && $FileDef['corrected']) $tmp_file = $this->BinFileCorrected($tmp_file);
									if(array_key_exists('call', $v)) $tmp_file = $this->CallBack($v, $tmp_file);
									if(array_key_exists('base64', $FileDef) && $FileDef['base64']) $tmp_file = base64_encode($tmp_file);
									file_put_contents($fn, $tmp_file);
									unset($tmp_file);
								}
							}
						}
						$this->SetCachedFiles($FilesKey, $Name, $D["size"][$j], $D["name"][$j], $D["type"][$j], $mimetype, $FileDef['savepath']);
						if(array_key_exists("autoreply", $FileDef))
						{
							if(!is_array($this->Returned))
								$this->Returned = array();
							if(!isset($this->Returned[$k]))
								$this->Returned[$k] = array();
							if(!isset($this->Returned[$k]["files"]))
								$this->Returned[$k]["files"] = array();
							$this->Returned[$k]["files"][] = $D["name"][$j];
							$this->Returned[$k]["count"] = $this->GetCachedFilesCount($FilesKey);
							$this->Returned[$k]["size"] = $this->GetCachedFilesSize($FilesKey);
							if($this->FormUID)
								$this->Returned[$k]["_fid"] = $this->FormUID;
						}
						else
						{
							$Values[$k][$j] = array();
							if($Name) $Values[$k][$j]["name"]	= $Name;
							$Values[$k][$j]["originalName"]		= $D["name"][$j];
							$Values[$k][$j]["size"]				= $D["size"][$j];
							$Values[$k][$j]["type"]				= $D["type"][$j];
							$Values[$k][$j]["mime"]				= $mimetype;
							$Values[$k][$j]["tmp_name"]			= $D["tmp_name"][$j];
							$Values[$k][$j]["key"]				= $FilesKey;
							if(array_key_exists('savepath', $FileDef))
								$Values[$k][$j]['path']				= $FileDef['savepath'];
							if(isset($tmp_file) && $tmp_file)
								$Values[$k][$j]['Data']				= $tmp_file;
							if(isset($tmp_file)) unset($tmp_file);
						}
					}
 				}
			}
		}
 	}


	private function Prepare(&$Define, $Data = null)
	{
		/*
		 * Условная установка описания переменных
		 */
		if(isset($Define["VarsCondition"]) && $Data)
		{
			if(!isset($Define["VarsCondition"]["variableName"]) || !isset($Define["VarsCondition"]["conditions"]))
				throw new dmtException("varsCondition definition error");
			$VarName = $Define["VarsCondition"]["variableName"];
			if(!isset($Define["VarsCondition"]["type"]))
				$Define["VarsCondition"]["type"] = "value";
			$VarValue = isset($Data[$VarName]) ? $Data[$VarName] : null;
			switch($Define["VarsCondition"]["type"])
			{
				case "value":
					$CondVars = null;
					if(isset($Define["VarsCondition"]["conditions"][$VarValue]))
						$CondVars = $Define["VarsCondition"]["conditions"][$VarValue];
					elseif(isset($Define["VarsCondition"]["conditions"]["default"]))
						$CondVars = $Define["VarsCondition"]["conditions"]["default"];
					else throw new dmtException("Not found variables definitions in varsCondition definition");
					if(isset($CondVars["Variables"]) || isset($CondVars["TplVars"]))
					{
						if(isset($CondVars["TplVars"]))
							$Define["TplVars"] = $CondVars["TplVars"];
						if(isset($CondVars["Variables"]))
							$Define["Variables"] = $CondVars["Variables"];
						if(isset($CondVars["getFiles"]))
						{
							$Define["getFiles"] = $CondVars["getFiles"];
						}
						if(isset($CondVars["getFilesProps"]))
						{
							$Define["getFilesProps"] = $CondVars["getFilesProps"];
						}
						if(isset($CondVars["keepFiles"]))
						{
							$Define["keepFiles"] = $CondVars["keepFiles"];
						}
					}
					elseif(isset($CondVars["getFiles"]) || isset($CondVars["getFilesProps"]))
					{
						if(isset($CondVars["getFiles"]))
						{
							$Define["getFiles"] = $CondVars["getFiles"];
						}
						if(isset($CondVars["getFilesProps"]))
						{
							$Define["getFilesProps"] = $CondVars["getFilesProps"];
						}
						if(isset($CondVars["keepFiles"]))
						{
							$Define["keepFiles"] = $CondVars["keepFiles"];
						}
					}
					else throw new dmtException("varsCondition definition error");
					break;
				case "range":
					break;
			}
			unset($Define["VarsCondition"]);
		}
		if(isset($Define["TplVars"]))
		{
			if(!isset($this->Tpls["TplVars"])) throw new dmtException("Mode configuration is error. Mode=".$this->Mode);
			if(!isset($Define["Variables"])) $Define["Variables"] = array();
			$CV = &$Define["TplVars"];
			$TV = &$this->Tpls["TplVars"];

			try {
				$Ks = array_keys($CV);
			}
			catch (dmtException $e)
			{
				$this->Dump($Define, $CV);
			}
			$CurrDef = array();
			foreach($Ks as $k)
			{
				if($CV[$k] === null)
				{
					$CurrDef[$k] = null;
					continue;
				}
				if(!array_key_exists($k, $TV))
                {
                    if(array_key_exists($k, $this->SysVariables))
                    {
                        if(is_array($this->SysVariables[$k]))
                        {
                            $CurrDef[$k] = $TV[$k];
                            $CurrDef[$k]["required"] = $this->SysVariables[$k]["required"] ? $this->SysVariables[$k]["required"] : $CV[$k];
                        }
                        else
                        {
                            $CurrDef[$k] = "sys";
                        }
                    }
                    else
                    {
                        $this->Dump(__METHOD__.": ".__LINE__, $TV, $Ks, $k, "Mode", $this->Mode);
                        throw new dmtException("Mode configuration is error. Mode=".$this->Mode);
                    }
                }
				else
				{
					$CurrDef[$k] = $TV[$k];
					$CurrDef[$k]["required"] = $CV[$k];
				}
			}
			if(isset($Define["Variables"]))
				//TODO: Добавить функцию рекурсивного слияния массивов с перезаписыванием значений ключа и использовать здесь ее вместо array_merge_recursive
				$Define = array_merge_recursive(array("Variables" => $CurrDef), $Define);
			else $Define["Variables"] = $CurrDef;
		}
	}


 	/**
 	 *
 	 * Проверяет данные на соответствие фильтру
 	 * @param mixed $Value данные, подлежащие проверке
 	 * @param array $Define фрагмет описания входящих данных
 	 */
 	private function CheckFilter($Data, $Define, &$Error, &$Values, $NotEmpty = null, $Key = null, $Ignore = null)
 	{
 		if(is_empty($Data))
 		{
 			if($NotEmpty) $Values = null;
 			elseif(array_key_exists("required", $Define) && $Define["required"])
 			{
 				if(array_key_exists("notempty", $Define) && $Define["notempty"])
 					$this->SetError($Error, isset($Define["iserror"]) ? $Define["iserror"] : "ErrNotEmpty", $Values);
 				else $Values = dmtEMPTY;
 			}
 			else $Values = dmtEMPTY;
 		}
 		else
 		{
 			$OriginalData = $Data;

	 		if(!array_key_exists("filter", $Define))
	 			throw new dmtException("Error variable definition");
	 		$DF = array();
	 		if(isset($Define["filter"]["type"]))	$DF[0] = $Define["filter"]["type"];
	 		if(isset($Define["filter"]["filter"]))	$DF[1] = $Define["filter"]["filter"];
	 		if(isset($Define["filter"]["iserror"]))	$DF[2] = $Define["filter"]["iserror"];
	 		if($DF) $Define = $DF;
	 		else $DF = $Define["filter"];

	 		/*
	 		 * Проверка необходимости установки значений фильтра из функции или метода некоторого класса
	 		 */
	 		if(array_key_exists("exec", $Define["filter"]))
	 		{
	 			$R = $this->CallFunct($Define["filter"]["exec"]);
	 			$DF[2] = $DF[1];
	 			$DF[1] = is_array($R) ? $R : array($R);
	 		}
	 		try
		 	{
		 		switch ($DF[0])
		 		{
		 			case FILTER_TYPE_ALIASES:
		 				if(array_key_exists($Data,  $DF[1]))
		 					$Values = $DF[1][$Data];
		 				else throw new dmtException("Data is not valide. Data=".$OriginalData);
		 				break;
		 			case FILTER_TYPE_VALUES:
		 				$tmp = array();
		 				foreach($DF[1] as $d)
		 				{
		 					$tmp[$d] = $d;
		 				}
		 				if(array_key_exists($Data, $tmp))
		 					$Values = $tmp[$Data];
		 				else throw new dmtException("Data is not valide. Data=".$OriginalData);
		 				break;
		 			case FILTER_TYPE_REGEXP:
		 				if(WS::Init()->Filtering($Data,  $DF[1]))
		 					$Values = $Data;
		 				else
			 				throw new dmtException("Data is not valide. FilterType = ".$DF[0].", FilterValue=".$DF[1].", Value='".$OriginalData."', action='".$this->Mode."', VariableName='".$Key."'", null, $NotEmpty, null, $NotEmpty);
		 				break;
		 			case FILTER_TYPE_FREE:
		 					if(is_array($DF[1]) && isset($$DF[1][2]))
		 					{
		 						$Arg = $DF[1][2];
		 						array_pop($DF[1]);
		 					}
		 					else $Arg = array();
							array_unshift($Arg, $Data);
							$Values = $this->CallFunct($DF[1], $Arg);
							if($Values === false)
								throw new dmtException("Data is not valide. Data ='".$Values."'", null, $NotEmpty);
		 				break;
		 			case FILTER_TYPE_TYPE:
		 				switch ($DF[1])
		 				{
			 				case "int":
			 					$Data = Math::PrepareInteger($Data);
			 					break;
			 				case "intu":
			 					$Data = Math::PrepareUnsignedInteger($Data);
			 					break;
			 				case "ints":
			 					$Data = Math::PrepareSignedInteger($Data);
			 					break;
			 				case "float":
			 					$Data = Math::PrepareFloat($Data);
			 					break;
			 				case "floatu":
			 					$Data = Math::PrepareUnsignedFloat($Data);
			 					break;
			 				case "floats":
			 					$Data = Math::PrepareSignedFloat($Data);
			 					break;
			 				case "date":
			 					$Data = DTs::PrepareDate($Data);
			 					break;
			 				case "time":
			 					$Data = DTs::PrepareTime($Data);
			 					break;
			 				case "datetime":
			 					$Data = DTs::PrepareDatetime($Data);
			 					break;
			 				default:
								try {
			 						$Data = call_user_func($DF[1], $Data);
								}
								catch (dmtException $e)
								{
$this->Dump(__METHOD__.": ".__LINE__, $DF[1], $Data);
									throw new dmtException($e->getMessage(), $e->getCode(), true);
								}
		 				}
		 				if($Data === false)
			 				throw new dmtException("Data is not valide. FilterType = ".$DF[0].", FilterValue=".$DF[1].", Value='".$OriginalData."', action='".$this->Mode."', VariableName='".$Key."'", null, $NotEmpty);
		 				$Values = $Data;
		 				break;
		 		}

			 	if(isset($Define["verifier"]))
			 	{
			 		try {
			 			$Values = $this->CallFunct($Define["verifier"], $Values);
			 		}
			 		catch (dmtException $e)
			 		{
			 			$Values = null;
						$this->SetError($Error, $e->getMessage(), $Values);
			 		}
			 	}

		 	}
		 	catch (dmtException $e)
		 	{
		 		if(!$Ignore)
		 		{
			 		$Values = null;
					$this->SetError($Error, isset($DF[2]) ? $DF[2] : "NotValideChars", $Values);
		 		}
		 	}

 		}
		return $Values;
 	}

 	/**
 	 *
 	 * Проверяет данные на соответствие граничным значениям
 	 * @param mixed $Value данные, подлежащие проверке
 	 * @param array $Define фрагмет описания входящих данных
 	 */
 	private function CheckLimit(&$Data, $Define, &$Error)
 	{
 		if($Data == null) return;
 		if(array_key_exists("limit", $Define))
 		{
 			$Define = $Define["limit"];
	 		$DF = array();
 			if(!isset($Define["max"]) && !isset($Define["min"]))
 			{
		 		if(isset($Define["type"])) $DF[0] = $Define["type"];
		 		if(isset($Define["filter"])) $DF[1] = $Define["filter"];
		 		if(isset($Define["iserror"])) $DF[2] = $Define["iserror"];
		 		if($DF) $Define = $DF;
 				if($Define[0] == 'char' && mb_strlen($Data, CHARSET) > $Define[1]
				|| $Define[0] == 'digital' && intval($Data) != $Define[1]
				|| $Define[0] == 'byte' && mb_strlen($Data, "windows-1251") * (mb_strtolower(CHARSET) == "utf-8" ? 2 : 1) != $Define[1])
					$this->SetError($Error, isset($Define[2]) ? $Define[2] : "ErrLimit", $Data);
 			}
 			else
 			{
	 			if(isset($Define["max"]))
	 			{
			 		if(isset($Define["max"]["type"])) $DF["max"][0] = $Define["max"]["type"];
			 		if(isset($Define["max"]["filter"])) $DF["max"][1] = $Define["max"]["filter"];
			 		if(isset($Define["max"]["iserror"])) $DF["max"][2] = $Define["max"]["iserror"];
			 		if($DF) $Define["max"] = $DF;
			 		if(array_key_exists("max", $Define) && ($Define["max"][0] == 'char' && mb_strlen($Data, CHARSET) > $Define["max"][1]
					|| $Define["max"][0] == 'digital' && intval($Data) > $Define["max"][1]
					|| $Define["max"][0] == 'byte' && mb_strlen($Data, CHARSET) * (mb_strtolower(CHARSET) == "utf-8" ? 2 : 1) > $Define["max"][1]))
						$this->SetError($Error, isset($Define["max"][2]) ? $Define["max"][2] : "ErrLimitMax", $Data);
	 			}
	 			if(isset($Define["min"]))
	 			{
	 				$DF = array();
			 		if(isset($Define["min"]["type"])) $DF["min"][0] = $Define["min"]["type"];
			 		if(isset($Define["min"]["filter"])) $DF["min"][1] = $Define["min"]["filter"];
			 		if(isset($Define["min"]["iserror"])) $DF["min"][2] = $Define["min"]["iserror"];
			 		if($DF) $Define["min"] = $DF;
					if(array_key_exists("min", $Define) &&
					($Define["min"][0] == 'char' && mb_strlen($Data, CHARSET) < $Define["min"][1]
					|| $Define["min"][0] == 'digital' && intval($Data) > $Define["min"][1]
					|| $Define["min"][0] == 'byte' && mb_strlen($Data, "windows-1251") * (mb_strtolower(CHARSET) == "utf-8" ? 2 : 1) < $Define["min"][1]
					))
						$this->SetError($Error, isset($Define["min"][2]) ? $Define["min"][2] : "ErrLimitMin", $Data);
	 			}
 			}
 		}
 	}


	private function SetError(&$Error, $MnCode, &$Values, $FileKey = null, $FilesData = null, $Index = null)
	{
		if(WS::Init()->IsCrossdomain() && $FileKey)
		{
			$this->SetCachedFiles($FileKey, null, isset($FilesData["size"]) ? $FilesData["size"] : null,
													isset($FilesData["name"]) ? $FilesData["name"] : null,
													isset($FilesData["type"]) ? $FilesData["type"] : null,
													null, null, $MnCode, $Index);
		}
		$this->IsError = true;
		$Error = $MnCode;
		$Values = null;
	}


	private function ConvMCode()
	{
		$MCCache = array();
		if($this->Errors)
			$MCCache = $this->ExtrMCode($this->Errors);
		if($this->Message)
			$MCCache[] =  &$this->Message;
	}

	private function ExtrMCode(&$Variable)
	{
		static $MCode = array();
		if($MCode === null) $MCode = array();
		if(is_array($Variable))
		{
			foreach($Variable as &$v)
				if(is_array($v)) $this->ExtrMCode($v);
				else $MCode[] = &$v;
		}
		else $MCode[] = &$Variable;
		return $MCode;
	}

	/**
	 * Проверяет и реализует вызов функции для входящих данных
	 *
	 * @param array $v - массив описания текущей входящей переменной для текущего режима массива описания режимов
	 * @param mixed $Value - значение текущей переменной
	 * @return mixed Результат выполнения вызванной и примененной функции в формате, возвращаемом вызванной функцией
	 */
	private function CallBack($v, $Value)
	{
		if($Value === null) return $Value;
		if(isset($v['call']))
		{
			if(is_array($v["call"]) && isset($v['call'][2]))
			{
				$Arg = $v['call'][2];
				array_pop($v['call']);
			}
			else $Arg = array();
			array_unshift($Arg, $Value);
			$Value = $this->CallFunct($v['call'], $Arg);
		}
		return $Value;
	}

	private function CallFunct($Called, $Args = null)
	{
		if(is_array($Called))
		{
            $IArgs = array();
            if(sizeof($Called) == 3)
            {
                if(!is_array($Called[2]))
                    $Called[2] = array($Called[2]);
                foreach($Called[2] as $v)
                    $IArgs[] = array_key_exists($v, $this->ClnData) ? $this->ClnData[$v] : $v;
                unset($Called[2]);
            }
			if(sizeof($Called) == 2)
			{
				if(!is_object($Called[0]) && method_exists($Called[0], "Init"))
					$Called[0] = call_user_func(array($Called[0], "Init"));
				elseif(method_exists($Called[0], "__construct")) $Called[0] = new $Called[0]();
			}
			else $Called = $Called[0];
		}
        if($Args === null)
            $Args = array();
		elseif(!is_array($Args))
			$Args = array($Args);
        $Merged = array_merge($Args, $IArgs);
		return call_user_func_array($Called, $Merged);
	}

	/**
	 *
	 * Заменяет неправильно добавленный символ нуля в случаях чтения содержимого файла с шестнадцатиразрядными данными.
	 * @param string $Content - строка для корректировки.
	 * @return string Результирующая строка.
	 */
	private function BinFileCorrected($Content)
	{
		return str_replace(chr(0), "", $Content);
	}

	final public function __get($Name)
	{
		if(!in_array($Name, $this->Gettitg))
		{
			$this->Trace();
			throw new dmtException("Property \"".$Name."\" can not be obtained or not found in class \"".__CLASS__."\"");
		}
		return $this->$Name;
	}
	/**
	 *
	 * Возвращает директорию временного хранения загруженных фалов.
	 */
	final protected function GetTempPath()
	{
		return $this->UsedTmpDir;
	}

	/**
	 *
	 * Возвращает список загруженных файлов
	 *
	 * @param string $Key - иденфикатор блока (формы)
	 */
	final static public function GetCachedFiles($Key, $NoClear = null, $GetProp = null)
	{
		$R = isset($_SESSION["Controller"]) && isset($_SESSION["Controller"]["Files"]) && isset($_SESSION["Controller"]["Files"][$Key]) ? ($GetProp ? $_SESSION["Controller"]["Files"][$Key]["FilesProps"] : $_SESSION["Controller"]["Files"][$Key]["List"]) : array();
		if(!$NoClear)
			unset($_SESSION["Controller"]["Files"][$Key]);
		return $R;
	}

	/**
	 *
	 * Добавляет файл в кеш загруженных фалов
	 *
	 * @param string $Key - иденфикатор блока (формы)
	 * @param string $FileName - название переменной (название поля формы)
	 * @param mixed $FileName строковое или целочисленное значение идентификатора записи
	 */
	final protected function SetCachedFiles($Key, $FileName, $FileSize, $OriginalName, $Type, $Mime, $SavePath, $Error = null, $Index = null)
	{
		if(!isset($_SESSION["Controller"]))
			$_SESSION["Controller"] = array();
		if(!isset($_SESSION["Controller"]["Files"]))
			$_SESSION["Controller"]["Files"] = array();
		if(!isset($_SESSION["Controller"]["Files"][$Key]))
		{
			$_SESSION["Controller"]["Files"][$Key] = array(
				"List"          => array(),
				"Size"          => 0,
				"Count"         => 0,
				"AllCount"		=> 0,
				"FilesProps"    => array()
			);
		}
		if($Index == null) {
			$Index = $_SESSION["Controller"]["Files"][$Key]["Count"];
		}
		$_SESSION["Controller"]["Files"][$Key]["FilesProps"][$Index] = array(
			"key"           => $Key,
			"name"          => $FileName,
			"size"          => $FileSize,
			"originalName"  => $OriginalName,
			"type"          => $Type,
			"mime"          => $Mime,
			"savepath"      => $SavePath,
			"error"			=> $Error
		);
		if(!$Error)
		{
			$_SESSION["Controller"]["Files"][$Key]["List"][$Index] = $FileName;
			$_SESSION["Controller"]["Files"][$Key]["Size"] += $FileSize;
			$_SESSION["Controller"]["Files"][$Key]["Count"] ++;
		}
		$_SESSION["Controller"]["Files"][$Key]["AllCount"] ++;
	}

	/**
	 *
	 * Возвращает количество загруженных файлов для формы и названия переменной (названия поля формы) и идентификатора записи
	 *
	 * @param string $Key - иденфикатор блока (формы)
	 */
	final protected function GetCachedFilesSize($Key)
	{
		return isset($_SESSION["Controller"]) && isset($_SESSION["Controller"]["Files"]) && isset($_SESSION["Controller"]["Files"][$Key]) ? $_SESSION["Controller"]["Files"][$Key]["Size"] : 0;
	}

	/**
	 *
	 * Возвращает количество загруженных файлов для формы и названия переменной (названия поля формы) и идентификатора записи
	 *
	 * @param string $Key - иденфикатор блока (формы)
	 */
	final protected function GetCachedFilesCount($Key)
	{
		return isset($_SESSION["Controller"]) && isset($_SESSION["Controller"]["Files"]) && isset($_SESSION["Controller"]["Files"][$Key]) ? $_SESSION["Controller"]["Files"][$Key]["AllCount"] : 0;
	}

	/**
	 *
	 * Очищает кеш данных загруженных файлов
	 *
	 * @param string $FormId - иденфикатор формы
	 * @param string $VariableName - название переменной (название поля формы)
	 * @param mixed $RecId строковое или целочисленное значение идентификатора записи
	 * @param integer $Index - индекс или идентификатор файла
	 */
	final static public function ClearCachedFiles($Key)
	{
		if(isset($_SESSION["Controller"]) && isset($_SESSION["Controller"]["Files"]) && isset($_SESSION["Controller"]["Files"][$Key]))
			unset($_SESSION["Controller"]["Files"][$Key]);
	}

	final static public function GetFileSatatus($FieldName, $Index)
	{
		$Key = WS::Init()->GetFormUID().$FieldName;
		$Returned = array();
		$Returned[$FieldName] = array();
		$Returned[$FieldName]["files"] = array();
		$Returned[$FieldName]["files"][$Index] = array();
		$Errors = null;
		if(isset($_SESSION["Controller"]) && isset($_SESSION["Controller"]["Files"]) && isset($_SESSION["Controller"]["Files"][$Key]) && isset($_SESSION["Controller"]["Files"][$Key]["FilesProps"]) && isset($_SESSION["Controller"]["Files"][$Key]["FilesProps"][$Index]))
		{
			if($_SESSION["Controller"]["Files"][$Key]["FilesProps"][$Index]["error"])
			{
				$Errors = array();
				$Errors[$FieldName] = array();
				$Errors[$FieldName][$Index] = $_SESSION["Controller"]["Files"][$Key]["FilesProps"][$Index]["error"];
			}
			else
			{
				$Returned[$FieldName]["file"] = $_SESSION["Controller"]["Files"][$Key]["FilesProps"][$Index]["name"];
				$Returned[$FieldName]["count"] = $_SESSION["Controller"]["Files"][$Key]["AllCount"];
				$Returned[$FieldName]["size"] = $_SESSION["Controller"]["Files"][$Key]["Size"];
				if(WS::Init()->GetFormUID())
					$Returned[$FieldName]["_fid"] = WS::Init()->GetFormUID();
			}
		}
		return array(
			"returned"	=> $Returned,
			"errors"	=> $Errors
		);
	}


	private function SetResult($Type, $ExcCode = null)
	{
		$Msg = null;
		if($Type == 0)
		{
			if(isset($this->CurrDef["Results"]) && isset($this->CurrDef["Results"]["error"]))
			{
				if($this->Result === null) $this->Result = isset($this->CurrDef["Results"]["error"]["number"]) ? $this->CurrDef["Results"]["error"]["number"] : $this->CurrDef["Results"]["error"][0];
				if(!$this->Message) $Msg = isset($this->CurrDef["Results"]["error"]["message"]) ? $this->CurrDef["Results"]["error"]["message"] : sizeof($this->CurrDef["Results"]["error"]) == 2 ? $this->CurrDef["Results"]["error"][1] : null;
			}
			else $this->Result = 1;
		}
		elseif($Type == 1)
		{
			if(isset($this->CurrDef["Results"]) && isset($this->CurrDef["Results"]["full"]) && $this->Returned)
			{
				if($this->Result === null) $this->Result = isset($this->CurrDef["Results"]["full"]["number"]) ? $this->CurrDef["Results"]["full"]["number"] : $this->CurrDef["Results"]["full"][0];
				if(!$this->Message) $Msg = isset($this->CurrDef["Results"]["full"]["message"]) ? $this->CurrDef["Results"]["full"]["message"] : sizeof($this->CurrDef["Results"]["full"]) == 2 ? $this->CurrDef["Results"]["full"][1] : null;
			}
			elseif(isset($this->CurrDef["Results"]) && isset($this->CurrDef["Results"]["empty"]) && !$this->Returned)
			{
				if($this->Result === null) $this->Result = isset($this->CurrDef["Results"]["empty"]["number"]) ? $this->CurrDef["Results"]["empty"]["number"] : $this->CurrDef["Results"]["empty"][0];
				if(!$this->Message) $Msg = isset($this->CurrDef["Results"]["empty"]["message"]) ? $this->CurrDef["Results"]["empty"]["message"] : sizeof($this->CurrDef["Results"]["empty"]) == 2 ? $this->CurrDef["Results"]["empty"][1] : null;
			}
			elseif(isset($this->CurrDef["Results"]) && isset($this->CurrDef["Results"]["succes"]))
			{
				if($this->Result === null) $this->Result = isset($this->CurrDef["Results"]["succes"]["number"]) ? $this->CurrDef["Results"]["succes"]["number"] : $this->CurrDef["Results"]["succes"][0];
				if(!$this->Message) $Msg = isset($this->CurrDef["Results"]["succes"]["message"]) ? $this->CurrDef["Results"]["succes"]["message"] : sizeof($this->CurrDef["Results"]["succes"]) == 2 ? $this->CurrDef["Results"]["succes"][1] : null;
			}
			elseif($this->Returned)
			{
				if($this->Result === null) $this->Result = 7;
			}
			elseif($this->Result === null) $this->Result = !$this->Returned && $this->Returned !== null ? 5 : 7;
		}
		elseif($Type == 2)
		{
			if(isset($this->CurrDef["Results"]) && isset($this->CurrDef["Results"]["exceptions"]))
			{
				if($this->Result === null)
				{
					$this->Result = $ExcCode;
				}
				if(!$this->Message)
				{
					if(isset($this->CurrDef["Results"]["exceptions"]["message"]))
					{
						$Msg = $ExcCode !== null && array_key_exists($ExcCode, $this->CurrDef["Results"]["exceptions"]) ?
						 	$this->CurrDef["Results"]["exceptions"]["message"][$ExcCode] :
							$this->CurrDef["Results"]["exceptions"]["message"][0];
					}
					elseif(isset($this->CurrDef["Results"]["exceptions"][1]))
					{
						$Msg = $ExcCode !== null && array_key_exists($ExcCode, $this->CurrDef["Results"]["exceptions"][1]) ?
						 	$this->CurrDef["Results"]["exceptions"][1][$ExcCode] :
							(isset($this->CurrDef["Results"]["exceptions"][1][0]) ? $this->CurrDef["Results"]["exceptions"][1][0] : "");
					}
					else $Msg = "ErrUnspecific";
				}
			}
			else
			{
				$Msg = "ErrUnspecific";
				$this->Result = 1;
			}
		}
		if($Msg) $this->Message = $Msg;
	}


    public function GetDataSetStart()
    {
        return $this->DataSetStart;
    }

    public function GetDataSetEnd()
    {
        return $this->DataSetEnd;
    }
}