<?php
if(!defined("GEO_SERVICE_ID"))
	define("GEO_SERVICE_ID",												5);



class GeoProcessor extends Processor
{
	/**
	 *
	 * @var GeoProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var GeoData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return GeoProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new GeoData();
	}

	public function GetNearestDealers($Count = null, $CityCount = null, $IP = null, $Facility = null, $Latitude = null, $Longitude = null, $FacilityAlias = null)
	{
		if(!$Count && !$CityCount)
			$Count = 1;
		return $this->CData->GetNearestDealers(ip2long($IP ? $IP : $_SERVER["REMOTE_ADDR"]), $Count, $CityCount, $Facility, $Latitude, $Longitude);
	}


	public function GetNearestDealersCity($Count = null, $IP = null, $Latitude = null, $Longitude = null, $Facility = null, $FacilityAlias = null, $Action = null, $Model = null, $Promo = null)
	{
		if(!$Count)
			$Count = 3;
		return $this->CData->GetNearestDealersCity(ip2long($IP ? $IP : $_SERVER["REMOTE_ADDR"]), $Count, $Latitude, $Longitude, $Facility, $FacilityAlias, $Action, $Model, $Promo);
	}

	public function GetDistance($lat1, $lng1, $lat2, $lng2, $miles = true)
	{
		$pi80 = M_PI / 180;
		$lat1 *= $pi80;
		$lng1 *= $pi80;
		$lat2 *= $pi80;
		$lng2 *= $pi80;
		$r = 6372.797; // mean radius of Earth in km
		$dlat = $lat2 - $lat1;
		$dlng = $lng2 - $lng1;
		$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));

		$km = $r * $c;
		return ($miles ? ($km * 0.621371192) : $km);
	}




	/**
	 *
	 * Получить список всех стран, поиск страны по строке
	 * @param string $FindText - Строка поиска
	 */
	public function GetCountries($FindText = null)
	{
		return $this->CData->GetCountries($FindText);
	}

	/**
	 *
	 * Получить список стран, в которых есть дилеры, поиск страны, в которой есть дилеры, по строке
	 * @param string $FindText - Строка поиска
	 */
	public function GetDealersCountries($FindText = null)
	{
		return $this->CData->GetDealersCountries($FindText);
	}

	/**
	 *
	 * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
	 * @param integer $Country - Идентификатор страны
	 * @param string $FindText - Строка поиска
	 */
	public function GetRegion($Country = null, $FindText = null)
	{
		if(!$Country && !$FindText)
			throw new dmtException("Arguments error");
		return $this->CData->GetRegion($Country, $FindText);
	}

	/**
	 *
	 * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
	 * @param integer $Country - Идентификатор страны
	 * @param string $FindText - Строка поиска
	 */
	public function GetDealersRegion($Country = null, $FindText = null)
	{
		return $this->CData->GetDealersRegion($Country, $FindText);
	}


	/**
	 *
	 * Получить список всех городов для заданной страны, поиск города по строке
	 * @param integer $Region - Идентификатор региона
	 * @param string $FindText - Строка поиска
	 */
	public function GetCities($Region = null, $FindText = null, $Country = null)
	{
		if(!$Region && !$FindText && !$Country)
			throw new dmtException("Arguments error");

		return $this->CData->GetCities($Region, $FindText, $Country);
	}

	/**
	 *
	 * Получить список всех городов для заданной страны, поиск города по строке
	 * @param integer $Country - Идентификатор страны
	 * @param string $FindText - Строка поиска
	 */
									//"regionId" => 0, "countryId" => 0, "findText" => 0, "facilityId" => 0, "actionId" => 0, "modelId" => 0, "fStatus" => 0, "dealerStatus" => 0, "facilityAliasPool" => 0
	public function GetDealersCities($Region = null, $Country = null, $FindText = null, $Facility = null, $Action = null, $Model = null, $FacilityStatus = null, $DealerStatus = null, $FacilityPool = null)
	{
		return $this->CData->GetDealersCities($Region, $Country, $FindText, $Facility, $Action, $Model, $FacilityStatus, $DealerStatus, $FacilityPool);
	}

	/**
	 *
	 * Получить список всех городов для заданной страны, поиск города по строке
	 * @param integer $Country - Идентификатор страны
	 * @param string $FindText - Строка поиска
	 */
									//"regionId" => 0, "countryId" => 0, "findText" => 0, "facilityId" => 0, "modelId" => 0, "fStatus" => 0, "dealerStatus" => 0, "facilityAliasPool" => 0
	public function GetDealersCitiesNew($Region = null, $Country = null, $FindText = null, $Facility = null, $Model = null, $FacilityStatus = null, $DealerStatus = null, $FacilityPool = null)
	{
		return $this->CData->GetDealersCitiesNew(WS::Init()->GetBrandId(), $Region, $Country, $FindText, $Facility, $Model, $FacilityStatus, $DealerStatus, $FacilityPool);
	}

	public function GetFederalDistrict($Country = null)
	{
		return $this->CData->GetFederalDistrict();
	}

	public function GetDealersFederalDistrict()
	{
		return $this->CData->GetDealersFederalDistrict();
	}

	public function GetCityName($City)
	{
		return $this->CData->GetCityName($City);
	}

	public function GetCityByCoordinates($Latitude, $Longitude)
	{
		return $this->CData->GetCityByCoordinates($Latitude, $Longitude);
	}
}