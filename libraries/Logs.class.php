<?php
class Logs extends Base
{
	private $File;
	private $Start;
	private $StringSeparator = "\n";
	private $Prefix = "";
	private $AddTimestamp;

	public function __construct($Name, $Path = null, $AddTimestamp = null, $Replace = null, $StringSeparator = null, $Prefix = null)
	{
		parent::__construct();
		if(!$Path)
		{
			$Path = PATH_ROOT."debug/";
		}
		$FileName = $Path.$Name;
		if($StringSeparator)
		{
			$this->StringSeparator = $StringSeparator;
		}
		if($Prefix)
		{
			$this->Prefix = $Prefix;
		}
		try
		{
			$this->File = fopen($FileName, $Replace ? "w" : "a");
			$this->Start = microtime(true);
			if($AddTimestamp)
			{
				$this->AddTimestamp = true;
				fwrite($this->File, "--- Начало записи: ".date("d.m.Y H:i:s", $this->Start)."; метка времени: ".$this->Start.$this->StringSeparator);
			}
		}
		catch(dmtException $e) {}
	}
	
	public function SetPrefix($Prefix)
	{
		$this->Prefix = $Prefix;
	}
	
	public function SetStringSeparator($StringSeparator)
	{
		$this->StringSeparator = $StringSeparator;
	}
	
	public function Log($Message, $NewLineCount = null)
	{
		if($this->File)
		{
			fwrite($this->File, $this->Prefix.$Message);
			if($NewLineCount)
			{
				$this->Line($NewLineCount);
			}
		}
	}
	
	public function Line($Count = null)
	{
		if($this->File)
		{
			$Count = intval($Count);
			fwrite($this->File, str_repeat($this->StringSeparator, $Count ? $Count : 1));
		}
	}
	
	public function Close()
	{
		if($this->File)
		{
			fclose($this->File);
		}
		$this->File = null;
	}
	
	public function __destruct()
	{
		if($this->AddTimestamp)
		{
			$End = microtime(true);
			fwrite($this->File, "--- Окончание записи: ".date("d.m.Y H:i:s", $End)."; метка времени: ".$End."; время выполнения (с): ".($End - $this->Start));
		}
		$this->Line(3);
		$this->Close();
	}
}