<?php
class ClientsProcessor extends Processor
{
	/**
	 *
	 * @var ClientsProcessor
	 */
	protected static $Inst = false;

    protected $CUID;
    protected $Id;

    /**
	 *
	 * Класс данных
	 * @var ClientsData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return ClientsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new ClientsData();
	}


	public function Register()
	{
		return $this->CData->Register();
	}

    public function Unregister($CUID)
	{
		$this->CData->Unregister($CUID);
	}

    public function SetCUID($CUID)
    {
        $this->Id = $this->CData->SetCUID($CUID);
		return $this->Id;
    }

    public function GetClientId()
    {
        return $this->Id;
    }
}