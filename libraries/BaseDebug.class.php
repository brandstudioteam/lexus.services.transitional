<?php
class BaseDebug
{
	protected function __construct() {}
	
	/**
	 * 
	 * Транслирует протоколирование
	 * @param string $Text текст, для протоколирования
	 */
	protected function Debug($Text)
	{
		Debug::Init()->Debug($Text);
	}
	
	protected function Trace()
	{
		Debug::Init()->Trace();
	}
	
	protected function Dump($Var)
	{
		call_user_func_array(array(Debug::Init(), "Dump"), func_get_args());
	}
	
	/**
	 * 
	 * Возвращает текст протокола
	 * @param bool $ForHTML - флаг, означающий необходимость перевода спецсимволов в html-сущности
	 * @return srting Строка текста протокола
	 */	
	protected function GetDebug()
	{
		return Debug::Init()->GetInfo();
	}
}