<?php
class dmtException extends Exception
{
    public function __construct($Message, $Code = 0, $IsTraslate = false, $File = null, $Line = null, $IsDB = null, $Trace = null)
    {
        parent::__construct($Message, $Code);
        if(!$IsTraslate)
        	$this->SetDebugInfo(null, null, null, null, true);
    }
    
    protected function SetDebugInfo($Message = null, $Code = null, $File = null, $Line = null, $Trace = null)
    {
    	$Tr = false;
    	if($Line || $Trace)
    	{
    		$Tr = debug_backtrace();
    		unset($Tr[0]);
    	}
    	GetDebug()->SaveException($Message ? $Message : $this->getMessage(), $Code ? $Code : $this->getCode(), $File ? $File : $this->getFile(), $Line ? $Line : $this->getLine(), $Tr ? $Tr : null/*$this->getTrace()*/, null);
    }
}