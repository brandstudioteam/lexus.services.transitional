<?php
class CommentsProcessor extends Processor
{
	/**
	 *
	 * @var CommentsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var CommentsData
	 */
	private $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return CommentsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new CommentsData();
	}


    public function CreateComment($Ess, $Object, $Text)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->CreateComment($Ess, $Object, WS::Init()->GetBrandId(), $_SERVER["REMOTE_ADDR"], $Text);
    }

    public function UpdateComment($Comments, $Text)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return array("update" => $this->CData->UpdateComment($Comments, WS::Init()->GetBrandId(), $Text));
    }

    public function GetCommentsWUsers($Ess, $Object, $User = null, $DateStart = null, $DateEnd = null, $IP = null, $Comments = null)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->GetCommentsWUsers($Ess, $Object, WS::Init()->GetBrandId(), $User, $DateStart, $DateEnd, null, $Comments);
    }

    public function GetCommentsByIdWUsers($Comments)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->GetCommentsByIdWUsers($Comments);
    }

    public function GetComments($Ess, $Object, $User = null, $DateStart = null, $DateEnd = null, $IP = null, $Comments = null)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->GetComments($Ess, $Object, WS::Init()->GetBrandId(), $User, $DateStart, $DateEnd, $IP, $Comments);
    }

    public function GetCommentsById($Comments)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->GetCommentsById($Comments);
    }

    public function GetComment($Comment)
    {
        //PermissionsProcessor::Init()->CheckAccess();
        return $this->CData->GetComment($Comment);
    }

	public function DeleteComment($Ess, $Object, $Comment = null)
	{
		 return $this->CData->DeleteComment($Ess, $Object, $Comment);
	}
}