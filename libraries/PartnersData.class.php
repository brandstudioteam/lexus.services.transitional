<?php
class PartnersData extends Data
{
	/**
	 *
	 * Создает партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param string $Name - Наименование
	 * @param string $OfficialName - Официальное наименование
	 * @param string $Address - Адрес
	 * @param string $Email - Адрес электронной почты
	 */
	public function CreatePartner($Brand, $PartnerType, $Parent, $Name, $Site, $City, $Address, $Latitude, $Longitude, $Phone, $RCode, $MapImage)
	{
		if($PartnerType == 6 || $PartnerType == 1)
		{
			$this->Exec("INSERT INTO `".DBS_REFERENCES."`.`partners_divisions`
	(`partner_id`,
	`partners_type_id`,
	`name`,
	`site`,
	`city_id`,
	`latitude`,
	`longitude`,
	`address`,
	`phones`,
	`rcode`,
	`rcode_hash`,
	`map_image`)
VALUES
	(".$this->CheckNull($Parent).",
	".$PartnerType.",
	".$this->Esc($Name, true, false, true).",
	".$this->Esc($Site).",
	".$City.",
	".$this->Esc($Latitude).",
	".$this->Esc($Longitude).",
	".$this->Esc($Address).",
	".$this->Esc($Phone).",
	".$this->Esc($RCode).",
	".$this->Esc(md5($RCode)).",
	".$this->Esc($MapImage).");");
			$Partner = $this->DB->GetLastID();
			$this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `old_id`=".$Partner.";");
		}
		else
		{
			$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
	(`partner_id`,
	`partners_type_id`,
	`brand_id`,
	`name`,
	`site`,
	`city_id`,
	`latitude`,
	`longitude`,
	`address`,
	`phones`,
	`rcode`,
	`rcode_hash`,
	`map_image`)
VALUES
	(".$this->CheckNull($Parent).",
	".$PartnerType.",
	".$Brand.",
	".$this->Esc($Name, true, false, true).",
	".$this->Esc($Site).",
	".$City.",
	".$this->Esc($Latitude).",
	".$this->Esc($Longitude).",
	".$this->Esc($Address).",
	".$this->Esc($Phone).",
	".$this->Esc($RCode).",
	".$this->Esc(md5($RCode)).",
	".$this->Esc($MapImage).");");
			$Partner = $this->DB->GetLastID();
		}
		return $Partner;
	}

	/**
	 *
	 * Сохраняет изменения партнера
	 * @param integer $Partner - Идентификатор партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param integer $UsersDataType - Тип пользовательских данных (тип анкеты пользователя)
	 * @param string $Name - Наименование
	 * @param string $OfficialName - Официальное наименование
	 * @param string $Address - Адрес
	 * @param string $Email - Адрес электронной почты
	 */
	public function SavePartner($Partner, $PartnerType, $Parent, $Name, $Site, $Address, $Latitude, $Longitude, $Phone, $RCode, $MapImage)
	{
		$U = array();
		$U2 = array();
		
		$OldId = $this->Count("SELECT
	`old_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partners_division_id`=".$Partner.";");
		
		if($PartnerType)
		{
			$U[] = "`partners_type_id`=".$PartnerType;
			$U2[] = "`partners_type_id`=".$PartnerType;
		}
		if($Parent)
		{
			$ParentOldId = $this->Count("SELECT
	`old_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partners_division_id`=".$Parent.";");
			$U[] = "`partner_id`=".$this->Esc($Parent);
			$U2[] = "`partner_id`=".$this->Esc($ParentOldId);
		}
		if($Name)
		{
			$U[] = "`name`=".$this->Esc($Name, true, false, true);
			$U2[] = "`name`=".$this->Esc($Name, true, false, true);
		}
		if($Site !== null)
		{
			$U[] = "`site`=".$this->Esc($Site);
			$U2[] = "`site`=".$this->Esc($Site);
		}
		if($Latitude !== null)
		{
			$U[] = "`latitude`=".$this->Esc($Latitude);
			$U2[] = "`latitude`=".$this->Esc($Latitude);
		}
		if($Longitude !== null)
		{
			$U[] = "`longitude`=".$this->Esc($Longitude);
			$U2[] = "`longitude`=".$this->Esc($Longitude);
		}
		if($Address)
		{
			$U[] = "`address`=".$this->Esc($Address);
			$U2[] = "`address`=".$this->Esc($Address);
		}
		if($Phone !== null)
		{
			$U[] = "`phones`=".$this->Esc($Phone);
			$U2[] =  "`phones`=".$this->Esc($Phone);
		}
		if($RCode)
		{
			$U[] = "`rcode`=".$this->Esc($RCode);
			$U[] = "`rcode_hash`=".$this->Esc(md5($RCode));
			$U2[] =  "`rcode`=".$this->Esc($RCode);
			$U2[] =  "`rcode_hash`=".$this->Esc(md5($RCode));
		}
		if($MapImage)
		{
			$U[] = "`map_image`=".$this->Esc($MapImage);
			$U2[] = "`map_image`=".$this->Esc($MapImage);
		}
		if(sizeof($U))
		{
			$this->Begin();
			try
			{
				$this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
SET ".implode(",", $U)."
WHERE `partners_division_id`=".$Partner.";");
				if($OldId)
					$this->Exec("UPDATE `".DBS_REFERENCES."`.`partners_divisions`
SET ".implode(",", $U2)."
WHERE `partners_division_id`=".$OldId.";");
				
				$this->Commit();
			}
			catch(dmtException $e)
			{
				$this->Rollback();
				throw new dmtException($e->getMessage(), $e->getCode(), true);
			}
		}
	}

	/**
	 *
	 * Возвращает список партнеров
	 */
	public function GetPartnersList($PartnerType = null, $Region = null, $FindText = null)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerDivisionId,
	`partners_type_id` AS partnerTypeId,
	`name` AS partnerDivisionName,
	`brand_name` AS partnerDivisionOfficialName,
	`address` AS partnerDivisionAddress
FROM `".DBS_REFERENCES."`.`partners`
ORDER BY `name`");
	}

	public function GetPartnersListF($PartnerType = null, $Region = null, $City = null, $FederalDistrict = null, $FindText = null)
	{
		$Query = "SELECT
	p.`partners_division_id` AS partnerDivisionId,
    p.`partner_id` AS partnerId,
	p.`partners_type_id` AS partnerTypeId,
	p.`name` AS partnerDivisionName,
	p.`brand_name` AS partnerDivisionOfficialName,
	p.`address` AS partnerDivisionAddress,
	p.`phones` AS partnerDivisionPhone,
	p.`city_id` AS cityId,
	p.`city_name` AS cityName,
	p.`region_id` AS regionId,
	p.`region_name` AS regionName,
	p.`federal_district_id` AS federalDistrictId,
	p.`federal_district_name` AS federalDistrictName,
	p.`partners_type_name` AS partnerTypeName
FROM `".DBS_REFERENCES."`.`partnersGeoFull` AS p";

		$W = array();
		if($Region)
			$W[] = "p.`region_id`".$this->PrepareValue($Region);

		if($City)
			$W[] = "p.`city_id`".$this->PrepareValue($City);

		if($FederalDistrict)
			$W[] = "p.`federal_district_id`".$this->PrepareValue($FederalDistrict);

		if($FindText)
			$W[] = "(p.`name` LIKE ".$this->Esc("%".$FindText."%")." OR p.`brand_name` LIKE ".$this->Esc("%".$FindText."%").")";

		if($PartnerType)
			$W[] = "p.`partners_type_id`".$this->PrepareValue($PartnerType);

		$Query .= (sizeof($W) ? "
WHERE ".implode(" AND ", $W) : "")."
ORDER BY p.`name`;";
		return $this->Get($Query);
	}
	
	
	public function GetPartnersListFullNEW($PartnerType = null, $Region = null, $City = null, $FederalDistrict = null, $FindText = null)
	{
		$Query = "SELECT
	p.`partners_division_id` AS partnerDivisionId,
    -- p.`partner_id` AS partnerId,
	p.`partners_type_id` AS partnerTypeId,
	p.`name` AS partnerDivisionName,
	p.`name` AS partnerDivisionOfficialName,
	p.`address` AS partnerDivisionAddress,
	p.`phones` AS partnerDivisionPhone,
	p.`city_id` AS cityId,
	p.`city_name` AS cityName,
	p.`region_id` AS regionId,
	p.`region_name` AS regionName,
	p.`federal_district_id` AS federalDistrictId,
	p.`federal_district_name` AS federalDistrictName,
	p.`partners_type_name` AS partnerTypeName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partnersGeoFull` AS p";

		$W = array();
		$W[] = "`brand_id`=".WS::Init()->GetBrandId();
		if($Region)
			$W[] = "p.`region_id`".$this->PrepareValue($Region);

		if($City)
			$W[] = "p.`city_id`".$this->PrepareValue($City);

		if($FederalDistrict)
			$W[] = "p.`federal_district_id`".$this->PrepareValue($FederalDistrict);

		if($FindText)
			$W[] = "(p.`name` LIKE ".$this->Esc("%".$FindText."%")." OR p.`brand_name` LIKE ".$this->Esc("%".$FindText."%").")";

		if($PartnerType)
			$W[] = "p.`partners_type_id`".$this->PrepareValue($PartnerType);

		$Query .= (sizeof($W) ? "
WHERE ".implode(" AND ", $W) : "")."
ORDER BY p.`name`;";
		$this->Dump(__METHOD__, $Query);
		return $this->Get($Query);
	}

	/**
	 *
	 * Возвращает информацию об одном партнере
	 * @param integer $Partner
	 */
	public function GetPartner($Partner)
	{
		return $this->Get("SELECT
	p.`partners_division_id` AS partnerDivisionId,
	p.`partners_type_id` AS partnerTypeId,
	p.`name` AS partnerDivisionName,
	p.`brand_name` AS partnerDivisionOfficialName,
	p.`address` AS partnerDivisionAddress,
	p.`phones` AS partnerDivisionPhone,
	p.`city_id` AS cityId,
	p.`city_name` AS cityName,
	p.`region_id` AS regionId,
	p.`region_name` AS regionName,
	p.`federal_district_id` AS federalDistrictId,
	p.`federal_district_name` AS federalDistrictName,
	p.`partners_type_name` AS partnerTypeName
FROM `".DBS_REFERENCES."`.`partnersGeoFull` AS p
WHERE p.`partner_id`=".$Partner.";", true);
	}

	/**
	 *
	 * Возвращает информацию об одном партнере
	 * @param integer $Partner
	 */
	public function GetPartnerF($Partner)
	{
		return $this->Get("SELECT
	p.`partners_division_id` AS partnerDivisionId,
	p.`partners_type_id` AS partnerTypeId,
	p.`name` AS partnerDivisionName,
	p.`brand_name` AS partnerDivisionOfficialName,
	p.`address` AS partnerDivisionAddress,
	p.`partners_type_name` AS partnerTypeName
FROM `".DBS_REFERENCES."`.`partnersGeoFull` AS p
WHERE p.`partner_id`=".$Partner.";", true);
	}


	/**
	 *
	 * Создает тип партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function CreatePartnerType($Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_CREATE);

		$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`partners_types`
	(`name`,
	`description`)
VALUES
	(".$this->Esc($Name, true, false, true).",
	".$this->Esc($Description).");");
		return $this->DB->GetLastID();
	}

	/**
	 *
	 * Сохраняет изменения типа партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function SavePartnerType($PartnerType, $Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_EDIT);

		$U = array();

		if($Name)
			$U[] = "`name`=".$this->Esc($Name);
		if($Description !== null)
			$U[] = "`description`=".$this->Esc($Description);
		if(sizeof($U))
			$this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`partners_types`
SET ".implode(", ", $U)."
WHERE `partners_type_id`=".$PartnerType.";");
	}

	/**
	 *
	 * Возвращает список типов партнеров
	 */
	public function GetPartnersTypeList()
	{
		return $this->Get("SELECT
	`partners_type_id` AS partnerTypeId,
	`name` AS partnerTypeName,
	`description` AS partnerTypeDescription
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_types`
ORDER BY `name`");
	}





    public function GetPartnersHierarchy($Type, $NotType = null)
    {
        if($Type)
            $Query = "SELECT
	`partners_division_id` AS partnersId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partner_id` IN (
		SELECT
			`partner_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
		WHERE `partners_division_id`=".User::Init()->GetPartnerId()."
	)
    AND `partners_type_id`".$this->PrepareValue($Type).";";
        else $Query = "SELECT
	`partners_division_id` AS partnersId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partners_type_id`".$this->PrepareValue($NotType, false, 2).";";

        return $this->Get($Query);
    }

    public function CheckPartnersHierarchy($Partners, $Type, $NotType)
    {
        if($Type)
            $R = $this->Get("SELECT
	`partners_division_id` AS partnersId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partner_id` IN (
		SELECT
			`partner_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
		WHERE `partners_division_id`=".User::Init()->GetPartnerId()."
	)
	AND `partners_division_id`=".$this->PrepareValue($Partners)."
    AND `partners_type_id`".$this->PrepareValue($Type).";");
        else $R = $this->Get("SELECT
	`partners_division_id` AS partnersId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partners_division_id`".$this->PrepareValue($Partners)."
    AND `partners_type_id`".$this->PrepareValue($NotType, false, 2).";");
        return $R;
    }




    public function GetChildren()
    {
        return $this->Get("SELECT
	`user_id` AS userId,
	`partners_division_id` AS partnerDivisionId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions`;");
    }

    public function SaveChildren($User, $Partners)
    {
        $A = array();
        if(!is_array($Partners) && !is_empty($Partners))
            throw new dmtException("Partners is error", 10);
        $this->Begin();
        try
        {
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions`
WHERE `user_id`=".$User.";");
            if(!is_empty($Partners))
            {
                foreach($Partners as $v)
                {
                    $this->CheckChild($User, $v);
                    $A[] = "(".$User.", ".$v.")";
                }

                if(sizeof($A))
                    $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions`
	(`user_id`,
	`partners_division_id`)
VALUES ".implode(", ", $A).";");
            }
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }

    protected function CheckChild($User, $Partner)
    {
        if(!$this->Count("SELECT
	COUNT(*) AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS b ON b.`partner_id`=a.`partner_id`
WHERE a.`user_id`=".$User."
	AND b.`partners_division_id`=".$Partner.""))
            throw new dmtException("Partner is not child", 11);
    }


    public function SaveChildrenRoles($User, $Partners, $Roles)
    {
        $A = array();
        if(!is_array($Partners) && !is_empty($Partners))
            throw new dmtException("Partners is error", 10);
        $this->Begin();
        try
        {
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
WHERE `user_id`=".$User.";");
            if(!is_empty($Partners))
            {
                foreach($Partners as $v)
                {
                    $this->CheckChild($User, $v);
                    foreach($Roles as $r)
                    {
                        $A[] = "(".$User.", ".$v.", ".$r.")";
                    }
                }

                if(sizeof($A))
                    $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
	(`user_id`,
	`partners_division_id`,
	`role_id`)
VALUES ".implode(", ", $A).";");
            }
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }

    public function GetChildrenRoles()
    {
        return $this->Get("SELECT
	`user_id` AS userId,
	`partners_division_id` AS partnerDivisionId,
	`role_id` AS roleId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`;");
    }


    public function GetSlavePartnersTypes()
    {
        return $this->Get("SELECT
	`partners_type_id` AS partnerTypeId,
	`slave_partners_type_id` AS slavePartnerTypeId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_types_slave_partners_types`;");
    }

    public function GetHierarchy($Parent)
    {
        return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
	a.`parent_partners_division_id` AS parentPartnerDivisionId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_parent` AS a
WHERE a.`parent_partners_division_id`".$this->PrepareValue($Parent).";");
    }







	public function GetNewParentId($Partner)
    {
        return $this->Count("SELECT
	`old_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions`
WHERE `partners_division_id`=".$Partner.";", true);
    }
	
	public function GetPartnerNew($Partner)
    {
        return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
    a.`partner_id` AS partnerId,
	a.`partners_type_id` AS partnerTypeId,
	a.`name` AS partnerDivisionName,
	a.`site` AS partnerDivisionSite,
	a.`city_id` AS cityId,
	a.`latitude` AS gLn,
	a.`longitude` AS gLg,
	a.`address` AS partnerDivisionAddress,
	a.`phones` AS partnerDivisionPhone,
	a.`rcode` AS partnerDivisionRCode,
	a.`status` AS partnerDivisionStatus,
	IF(a.`status`=1,'Активный','Заблокирован') AS partnerDivisionStatusName,
	a.`map_image` AS mapImage,
	a.`partners_type_name` AS partnerTypeName,
	a.`city_name` AS cityName,
    a.`region_id` AS regionId,
    a.`region_name` AS regionName,
	a.`federal_district_id` AS federalDistrictId,
	a.`federal_district_name` AS federalDistrictName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partnersGeoFull` AS a
WHERE a.`partners_division_id`=".$Partner.";", true);
    }

    public function GetAccessiblePartners()
    {
        return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
    a.`partner_id` AS partnerId,
	a.`partners_type_id` AS partnerTypeId,
	a.`name` AS partnerDivisionName,
	a.`site` AS partnerDivisionSite,
	a.`city_id` AS cityId,
	a.`latitude` AS gLn,
	a.`longitude` AS gLg,
	a.`address` AS partnerDivisionAddress,
	a.`phones` AS partnerDivisionPhone,
	a.`rcode` AS partnerDivisionRCode,
	a.`status` AS partnerDivisionStatus,
	IF(a.`status`=1,'Активный','Заблокирован') AS partnerDivisionStatusName,
    a.`map_image` AS mapImage,
	a.`partners_type_name` AS partnerTypeName,
	a.`city_name` AS cityName,
    a.`region_id` AS regionId,
    a.`region_name` AS regionName,
	a.`federal_district_id` AS federalDistrictId,
	a.`federal_district_name` AS federalDistrictName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partnersGeoFull` AS a
WHERE a.`partners_division_id` IN (
		SELECT DISTINCT
			`partners_division_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
		WHERE `user_id`=".DB_VARS_ACCOUNT_CURRENT."
	);");
    }

    public function GetAccessiblePartnersParents()
    {
        return $this->Get("SELECT
	`partners_division_id` AS partnerDivisionId,
    `parent_partners_division_id` AS parentPartnerDivisionId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_parent`
WHERE `partners_division_id` IN (
		SELECT DISTINCT
			`partners_division_id`
		FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
		WHERE `user_id`=".DB_VARS_ACCOUNT_CURRENT."
	);");
    }
	
	
		
	public function GetPartnersCSV($Brand, $Scope, $PartnerType, $Region, $FindText)
	{
		$Query = "SELECT
	-- p.`partners_division_id` AS partnerDivisionId,
    p.`old_id` AS partnerDivisionId,
    p.`name` AS partnerDivisionName,
    p.`partners_type_name` AS partnerTypeName,
    p.`status` AS partnerDivisionStatus,
    p.`rcode` AS partnerDivisionRCode,
    p.`site` AS partnerDivisionSite,
    p.`latitude` AS partnerDivisionLatitude,
    p.`longitude` AS partnerDivisionLongitude,
	p.`address` AS partnerDivisionAddress,
	p.`phones` AS partnerDivisionPhone,
	p.`city_name` AS cityName,
	p.`region_name` AS regionName,
	p.`federal_district_name` AS federalDistrictName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partnersGeoFull` AS p";

		$W = array();
		$W[] = "p.`brand_id`=".$Brand;
		$W[] = "p.`partners_division_id`".$this->PrepareValue($Scope);
		if($Region)
			$W[] = "p.`region_id`".$this->PrepareValue($Region);
		if($FindText)
			$W[] = "(p.`name` LIKE ".$this->Esc("%".$FindText."%")." OR p.`brand_name` LIKE ".$this->Esc("%".$FindText."%").")";
		if($PartnerType)
			$W[] = "p.`partners_type_id`".$this->PrepareValue($PartnerType);

		return $this->Get($Query.$this->PrepareWhere($W)."
ORDER BY p.`name`;");
	}
	
	public function SaveChildrenPartners($Partner, $Children)
	{
		$A = array();
		if(is_array($Children))
		{
			foreach($Children as $v)
				$A[] = "(".$v.", ".$Partner.")";
		}
		$this->Begin();
		try
		{
			$this->Exec("DELETE FROM `references`.`partners_divisions_parent`
WHERE `parent_partners_division_id`=".$Partner.";");
			if(sizeof($A))
				$this->Exec("INSERT INTO `references`.`partners_divisions_parent`
	(`partners_division_id`,
	`parent_partners_division_id`)
VALUES ".implode(",", $A));
			$this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
	}
	
	
	
}