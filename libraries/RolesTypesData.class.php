<?php
class RolesTypesData extends Data
{
    public function GetRolesTypes()
    {
        return $this->Get("SELECT
	`roles_type_id` AS roleTypeId,
	`name` AS roleTypeName,
	`description` AS roleTypeDescription
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_types`;");
    }

    public function CreateRoleTypes()
    {

    }

    public function SaveRoleTypes()
    {

    }

    public function DeleteRoleTypes()
    {

    }

    public function GetRolesTypesUsersTypes()
    {

    }
}