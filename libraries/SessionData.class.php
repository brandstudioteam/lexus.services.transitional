<?php
class SessionData extends Data
{
	/**
	 * 
	 * Возвращает данные сессии.
	 * @param int $SessionId - идентификатор сессии
	 */
	public function GetSession($SessionId)
	{
		$Query  = "SELECT sess.`".SESSION_FIELDS_ID."` AS Id, sess.`".SESSION_FIELDS_DATA."` AS Data FROM `".SESSION_TABLENAME."` AS sess WHERE sess.`".SESSION_FIELDS_ID."`='".$SessionId."' LIMIT 1;";
		$R = $this->Get($Query, true);
		session_decode($R);
	}
	
	/**
	 * 
	 * Сохраняет данные сессии
	 * @param идентификатор сессии $SessionId
	 * @param string $Data - строка данных сесси. Ожидатся сериализованные данные сессии.
	 * @param int $UserId - идентификатор пользователя
	 */
	public function SaveSession($SessionId, $UserId)
	{
		$Data = mysql_real_escape_string(session_encode());
		if(!$this->CheckSession($SessionId)) $Query  = "INSERT INTO `".SESSION_TABLENAME."` (`".SESSION_FIELDS_ID."`, `".SESSION_FIELDS_DATA."`, `".SESSION_FIELDS_USER."`, `".SESSION_FIELDS_TIMEATAMP."`) VALUES ('".addslashes($SessionId)."', ".($Data ? "'".$Data."'" : "null").", ".($UserId ? $UserId : "null").", NOW());";
		else $Query  = "UPDATE `".SESSION_TABLENAME."` SET `".SESSION_FIELDS_DATA."` = ".($Data ? "'".$Data."'" : "null")." ".($UserId ? ", `".SESSION_FIELDS_USER."`=".$UserId : "").", `".SESSION_FIELDS_TIMEATAMP."`=NOW() WHERE `".SESSION_FIELDS_ID."`='".addslashes($SessionId)."' LIMIT 1;";
		$this->DB->Exec($Query);
	}
	
	/**
	 * 
	 * Проверят существование сессии для идентификатора сессии или идентификатора пользователя
	 * @param int $UserId - идентификатор пользователя
	 */
	public function CheckSession($SessionId)
	{
		if($SessionId) $Query  = "SELECT COUNT(`".SESSION_FIELDS_ID."`) AS Exs FROM `".SESSION_TABLENAME."` AS sess WHERE sess.`".SESSION_FIELDS_ID."`='".addslashes($SessionId)."' LIMIT 1;";
		return $this->Check($Query);
	}
	
	/**
	 * 
	 * Проверят существование сессии для идентификатора сессии или идентификатора пользователя
	 * @param int $UserId - идентификатор пользователя
	 */
	public function CheckForUser($UserId)
	{
		$Query  = "SELECT COUNT(`".SESSION_FIELDS_ID."`) AS Exs FROM `".SESSION_TABLENAME."` AS sess WHERE sess.`".SESSION_FIELDS_USER."`=".$UserId." LIMIT 1;";
		return $this->Check($Query);
	}
	
	/**
	 * 
	 * Удалаяет запсь сессии
	 * @param int $SessionId - идентификатор сессии
	 */
	public function Delete($SessionId)
	{
		$Query  = "DELETE FROM `".SESSION_TABLENAME."` WHERE `".SESSION_FIELDS_ID."`='".$SessionId."' LIMIT 1;";
		$this->DB->Exec($Query);
	}
	
	/**
	 * 
	 * Удаляет записи просроченных сессий
	 */
	public function Clear()
	{
		$Query  = "DELETE FROM `".SESSION_TABLENAME."` WHERE `".SESSION_FIELDS_TIMEATAMP."` < NOW() + ".SESSION_TIME_DEFAULT.";";
		$this->DB->Exec($Query);
	}
}
?>