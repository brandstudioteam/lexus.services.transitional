<?php
if(!defined("TEMPLATER_DEFAULT"))
    define("TEMPLATER_DEFAULT", "Templater2");
class WSConfig extends Base
{
	/**
	 *
	 * @var WSConfig
	 */

	protected $Config;

	protected $ClientCallback;
	protected $SystemEvents = array();
	protected $Controller;
	protected $Action;
	protected $Arguments = array();
	protected $DefaultLoad;
	protected $DefaultError;
	protected $WSAction;
	protected $Template;
    protected $DefaultTemplate;
	protected $AdvActions;
	protected $KeyData;
	protected $Templater;
    protected $setCookies = array();
	protected $AddArgument;

    protected $AdvHeaders;

    protected $Convert;
    protected $RetData;
	protected $Message;
	protected $Errors;

	protected $FileName;
	protected $FileOriginalName;

	protected $BackURL;
    protected $SetFormUID;
    /**
	 *
	 * Параметры отправки данных
	 */
	protected $Compressed;
    protected $DefaultContentType;
	protected $ContentType;
	protected $UseContentType;
    protected $DefaultSendType;
	protected $SendType;

private $CurrentDef;

	protected $Returned;
	protected $ClnData;

	protected $OutCharSet = CHARSET;

	protected $CurrentControllers;
	protected $CurrentAccess;

	protected $Controllers = array(
        "_a"    => "Auth",
		"a"     => "Accounts",
		"b"     => "Campaigns",
		"c"     => "Partners",
		"d"     => "Dealers",
		"e"     => "Admin",
		"f"     => "Cars",
		"g"     => "Clients",
		"h"     => "PromoSite",
		"i"     => "Geo",
		"j"     => "Roles",
		"k"     => "Permissions",
		"l"     => "Models",
		"m"     => "Facilities",

		"n"     => "TDCampaigns",
		"o"     => "Galery",
		"p"     => "CustomActions",
		"r"     => "carWashAction",
		"s"     => "CarValuations",
        "t"     => "TradeInProcessor",
        "u"     => "TradeInImportProcessor",
        "v"     => "TO",

		"x"     => "Event",
		"y"     => "UsersSubscriptions",
        "y0"	=> "UsersSubscriptions",
		"z"     => "Captcha",

		"aa"	=> "AccountsTypes",
		"ab"	=> "UsersPolling",
		"ac"	=> "Comments",
        "ad"    => "Insurance",
        "ae"    => "RolesTypes",
        "af"    => "Contest",
        "ag"    => "CarOrdersState",
        "ah"    => "CarOrdersState",
		"ai"	=> "FacilitiesForms",
		"aj"	=> "Complectations",
		"ak"	=> "SchedulerUsers",

		"zz"    => "OrderAndTestdriveImportEmails",
        "yy"    => "_DataModel",
        "z0T9y5_WW" => "SMSSender",
        "mias"  => "Mias"
	);


	protected function Sets()
	{
		$this->Config = WScfg::Sets();
	}

	/**
	 *
	 * Метод, запускает обработку режимов
	 * @param string $URL - полная входящая строка запроса
	 */
	public function Dispatcher()
	{
		$Path = parse_url($this->URL);
		$Path = isset($Path["path"]) ? (mb_substr($Path["path"], 0, 1) == "/" ? mb_substr($Path["path"], 1) : $Path["path"]) : "";
		$Path = $Path ? explode("/", mb_substr($Path, -1) == "/" ? mb_substr($Path, 0, -1) : $Path) : null;

		//Проверяем альтернативные домены
		$Domain = explode(".", $this->CheckAlternativeDomain($_SERVER["HTTP_HOST"]));
		$SuperDomain = array_pop($Domain);

		if(defined("URL_SA"))
		{
			while(end($Domain) != URL_SA)
			{
				array_pop($Domain);
			}
		}
		if(!sizeof($Domain))
			throw new dmtException("Start domain configuration error");

		$this->PrepareDomain($this->Config, $Domain, $Path);
		if($this->CheckAccess())
		{
			if($this->DefaultError)
			{
				$this->SetError();
			}
			if($this->AdvHeaders)
			{
				foreach($this->AdvHeaders as $h)
					header($h);
			}
			if($this->GetFileStatus !== null && !is_empty($this->GetFileStatus) && $this->FileStatusName)
			{
				$this->ModulesSwitcher();
			}
			else
			{
				$this->GetClientData();
				$R = true;
				if($this->AddArgument)
				{
					foreach($this->AddArgument as $v)
					{
						$this->GetArgumet($v);
					}
				}
				if($this->Controller)
				{
					$this->CheckController();
					$this->CheckAction();
					$this->ModulesSwitcher();
				}
				else $R = $this->CheckDefaultAction();
				if(!$this->SendType)
					$this->SendType = $this->DefaultSendType;
				if(!$this->ContentType)
					$this->ContentType = $this->DefaultContentType;
				if(is_array($this->setCookies))
				{
					foreach($this->setCookies as $v)
					{
						$CVal = $this->GetValue($v[1], isset($v[2]) ? $v[2] : null);
						if(!$CVal)
							if(isset($v[3]))
								$CVal = $this->GetValue($v[3]);
						setcookie($v[0], $CVal, time() + SESSION_TIME_DEFAULT, '/', $_SERVER["HTTP_HOST"],  WS::Init()->Secure ? 1 : null);
					}
				}
				if($R)
				{
					if(!$this->Template)
						$this->Template = $this->DefaultTemplate;
					if($this->KeyData)
						$this->Returned = array($this->KeyData => $this->Returned);
					if(is_array($this->AdvActions) && sizeof($this->AdvActions) || $this->SetFormUID)
					{
						if($this->Returned && !is_array($this->Returned))
						{
							$this->Returned = array($this->Returned);
						}
						elseif(!$this->Returned) $this->Returned = array();
						if($this->SetFormUID && !isset($this->Returned["_fid"]))
						{
							$this->Returned["_fid"] = md5(microtime(true));
						}
						foreach ($this->AdvActions as $k => $v)
						{
							$this->Returned[$k] = $this->GetValue($v);
						}
					}
					if(is_array($this->Convert) && sizeof($this->Convert))
					{
						$Fn = $this->Convert[0];
						$Fn[] = $this->Returned;
						//$this->Returned = $this->CallFunction($Fn, isset($this->Convert[1]) ? $this->Convert[1] : null);
						$this->Returned = call_user_func_array($this->Convert[0], array($this->Returned));
					}
					if($this->Template)
					{
						if($this->BackURL)
							$this->PrepareBackURL();
						$Templater = $this->Templater ? $this->Templater : (defined("TEMPLATER_DEFAULT") ? TEMPLATER_DEFAULT : "Templater");
						$this->Returned = $Templater::Init()->Merge($this->Template, $this->Returned, $this->Message, $this->Errors);
					}
				}
			}
		}
		else
		{
			if($this->IsAJAX)
			{
				$this->SendType = SEND_TYPE_JSON;
				$this->BuildAnswer(1, null);
			}
		}
	}

    protected function GetArgumet($Def)
    {
        $Value = null;
        if(is_array($Def))
        {
            switch($Def[1])
            {
                case "cookies":
                    $Value = isset($_COOKIE[$Def[2]]) ? $_COOKIE[$Def[2]] : null;
            }
        }
        $this->ClnData[$Def[0]] = $Value;
    }

    /**
	 *
	 * Читает описания конфигурации режимов и выбирает текущие значения для работы
	 * @param array $CurrentDef - Массив описания конфигурации. См. WSConfigDefinition.definition.php
	 * @param array $Domains - Массив доменов, в котором каждая составная часть полного доменного имени без имени домена первого уровня находится как отдельный элемент.
	 * @param array $Path - Массив путей, в котором каждая составная часть полного пути находится как отдельный элемент.
	 * @param boolean $IsFirst
	 */
	protected function PrepareDomain($CurrentDef, $Domains, $Path, $IsFirst = null)
	{
		$DebugSets = null;
		//Анализ доменов
		if(is_array($Domains))
		{
			if(!array_key_exists("domains", $CurrentDef))
				throw new dmtException("Configuration error. ".$_SERVER["REQUEST_URI"], 77770);
			$TmpDef = $CurrentDef["domains"];
$this->Dump(__METHOD__.": ".__LINE__, $Domains);
			if(sizeof($Domains))
			{
				$CurrDomain = array_pop($Domains);
				if(array_key_exists($CurrDomain, $TmpDef))
				{
					$TmpDef = $TmpDef[$CurrDomain];
					if(array_key_exists("access", $TmpDef))
					{
						$this->CurrentAccess = $TmpDef["access"];
					}
					if(array_key_exists("controllers", $TmpDef))
					{
						$this->CurrentControllers = $TmpDef["controllers"];
					}
					if(array_key_exists("useTemplater", $TmpDef))
					{
						$this->Templater = $TmpDef["useTemplater"];
					}
					if(array_key_exists("debugSets", $TmpDef))
					{
						$DebugSets = $TmpDef["debugSets"];
					}
					if(array_key_exists("headers", $TmpDef))
					{
						$this->AdvHeaders = $TmpDef["headers"];
					}
					$this->PrepareDomain($TmpDef, $Domains, $Path);
				}
				else
				{
					if(array_key_exists("default", $TmpDef))
					{
						$TmpDef = $TmpDef["default"];
						if(array_key_exists("access", $TmpDef))
						{
							$this->CurrentAccess = $TmpDef["access"];
						}
						if(array_key_exists("controllers", $TmpDef))
						{
							$this->CurrentControllers = $TmpDef["controllers"];
						}
						if(array_key_exists("useTemplater", $TmpDef))
						{
							$this->Templater = $TmpDef["useTemplater"];
						}
						if(array_key_exists("debugSets", $TmpDef))
						{
							$DebugSets = $TmpDef["debugSets"];
						}
						if(array_key_exists("headers", $TmpDef))
						{
							$this->AdvHeaders = $TmpDef["headers"];
						}
						$this->PrepareDomain($TmpDef, null, $Path);
					}
					else throw new dmtException("Configuration error. ".$_SERVER["REQUEST_URI"], 77770);
				}
			}
			else
			{
				if($IsFirst)
					throw new dmtException("Configuration error. ".$_SERVER["REQUEST_URI"], 77770);

				if(array_key_exists("domains", $CurrentDef))
					$TmpDef = $CurrentDef["domains"];
				if(array_key_exists("empty", $TmpDef))
				{
					$TmpDef = $TmpDef["empty"];
				}
				elseif(array_key_exists("default", $TmpDef))
				{
					$TmpDef = $TmpDef["default"];
				}
				else throw new dmtException("Configuration error", 77770);
				if(array_key_exists("access", $TmpDef))
				{
					$this->CurrentAccess = $TmpDef["access"];
				}
				if(array_key_exists("controllers", $TmpDef))
				{
					$this->CurrentControllers = $TmpDef["controllers"];
				}
				if(array_key_exists("useTemplater", $TmpDef))
				{
					$this->Templater = $TmpDef["useTemplater"];
				}
				if(array_key_exists("debugSets", $TmpDef))
				{
					$DebugSets = $TmpDef["debugSets"];
				}
				if(array_key_exists("headers", $TmpDef))
				{
					$this->AdvHeaders = $TmpDef["headers"];
				}
				$this->PrepareDomain($TmpDef, null, $Path);
			}
		}
		//Анализ путей
		else
		{
			if($IsFirst)
				throw new dmtException("Configuration error. ".$_SERVER["REQUEST_URI"], 77770);
			if(!array_key_exists("parts", $CurrentDef))
				throw new dmtException("Configuration error. ".$_SERVER["REQUEST_URI"], 77770);
			$TmpDef = $CurrentDef["parts"];
			if(is_array($Path) && sizeof($Path))
			{
				$CurrPath = array_shift($Path);
$this->Dump(__METHOD__.": ".__LINE__, "CurrPath = ".$CurrPath);
				if(array_key_exists($CurrPath, $TmpDef))
				{
					$TmpDef = $TmpDef[$CurrPath];
					if(array_key_exists("access", $TmpDef))
					{
						$this->CurrentAccess = $TmpDef["access"];
					}
                    if(array_key_exists("setCookies", $TmpDef))
                    {
                        $this->setCookies[] = $TmpDef["setCookies"];
                    }
                    if(array_key_exists("setFormUID", $TmpDef))
                    {
                        $this->SetFormUID = $TmpDef["setFormUID"];
                    }
					if(array_key_exists("controllers", $TmpDef))
					{
						$this->CurrentControllers = $TmpDef["controllers"];
					}
					if(array_key_exists("useController", $TmpDef))
					{
						$this->Controller = $TmpDef["useController"];
					}
					if(array_key_exists("useAction", $TmpDef))
					{
						$this->Action = $TmpDef["useAction"];
					}
                    if(array_key_exists("dataKey", $TmpDef))
                    {
                        $this->KeyData = $TmpDef["dataKey"];
                    }
					if(array_key_exists("useArgument", $TmpDef))
					{
						$this->Arguments[$TmpDef["useArgument"]] = $CurrPath;
					}
					if(array_key_exists("defaultAction", $TmpDef))
					{
						$this->WSAction = $TmpDef["defaultAction"];
					}
					if(array_key_exists("backURL", $TmpDef))
					{
						$this->BackURL = $TmpDef["backURL"];
					}
					if(array_key_exists("useTemplater", $TmpDef))
					{
						$this->Templater = $TmpDef["useTemplater"];
					}
					if(array_key_exists("debugSets", $TmpDef))
					{
						$DebugSets = $TmpDef["debugSets"];
					}
					if(array_key_exists("headers", $TmpDef))
					{
						$this->AdvHeaders = $TmpDef["headers"];
					}
                    if(array_key_exists("addArgument", $TmpDef))
					{
						$this->AddArgument = $TmpDef["addArgument"];
					}
                    if(array_key_exists("Convert", $TmpDef))
                    {
						$this->Convert = $TmpDef["Convert"];
                    }
                    if(array_key_exists("template", $TmpDef))
                    {
                        $this->Template = $TmpDef["template"];
                    }
                    if($this->IsAJAX())
                    {
                        if(array_key_exists("isAJAX", $TmpDef))
                        {
                            if(array_key_exists("addArgument", $TmpDef["isAJAX"]))
                            {
                                $this->AddArgument = $TmpDef["isAJAX"]["addArgument"];
                            }
                            if(array_key_exists("setCookies", $TmpDef["isAJAX"]))
                            {
                                $this->setCookies[] = $TmpDef["isAJAX"]["setCookies"];
                            }
                            if(array_key_exists("headers", $TmpDef["isAJAX"]))
                            {
                                $this->AdvHeaders = $TmpDef["isAJAX"]["headers"];
                            }
                            if(array_key_exists("setFormUID", $TmpDef["isAJAX"]))
                            {
                                $this->SetFormUID = $TmpDef["isAJAX"]["setFormUID"];
                            }
                            if(array_key_exists("access", $TmpDef["isAJAX"]))
                            {
                                $this->CurrentAccess = $TmpDef["isAJAX"]["access"];
                            }
                            if(array_key_exists("controllers", $TmpDef["isAJAX"]))
                            {
                                $this->CurrentControllers = $TmpDef["isAJAX"]["controllers"];
                            }
                            if(array_key_exists("useController", $TmpDef["isAJAX"]))
                            {
                                $this->Controller = $TmpDef["isAJAX"]["useController"];
                            }
                            if(array_key_exists("useAction", $TmpDef["isAJAX"]))
                            {
                                $this->Action = $TmpDef["isAJAX"]["useAction"];
                            }
                            if(array_key_exists("useArgument", $TmpDef["isAJAX"]) && $CurrPath !== null)
                            {
                                $this->Arguments[$TmpDef["isAJAX"]["useArgument"]] = $CurrPath;
                            }
                            if(array_key_exists("defaultAction", $TmpDef["isAJAX"]))
                            {
                                $this->WSAction = $TmpDef["isAJAX"]["defaultAction"];
                            }
                            if(array_key_exists("dataKey", $TmpDef["isAJAX"]))
                            {
                                $this->KeyData = $TmpDef["isAJAX"]["dataKey"];
                            }
                            if(array_key_exists("useTemplater", $TmpDef["isAJAX"]))
                            {
                                $this->Templater = $TmpDef["isAJAX"]["useTemplater"];
                            }
                            if(array_key_exists("debugSets", $TmpDef["isAJAX"]))
                            {
                                $DebugSets = $TmpDef["isAJAX"]["debugSets"];
                            }
                            if(array_key_exists("data", $TmpDef["isAJAX"]))
                            {
                                $this->AdvActions = $TmpDef["isAJAX"]["data"];
                            }
                            if(array_key_exists("fileOriginalName", $TmpDef["isAJAX"]))
                            {
                                $this->FileOriginalName = $TmpDef["isAJAX"]["fileOriginalName"];
                            }
                            if(array_key_exists("fileName", $TmpDef["isAJAX"]))
                            {
                                $this->FileName = $TmpDef["isAJAX"]["fileName"];
                            }
                            if(array_key_exists("OutCharSet", $TmpDef["isAJAX"]))
                            {
                                $this->OutCharSet = $TmpDef["isAJAX"]["OutCharSet"];
                            }
                            if(array_key_exists("SendType", $TmpDef["isAJAX"]))
                            {
                                $this->SendType = $TmpDef["isAJAX"]["SendType"];
                            }
                            if(array_key_exists("Convert", $TmpDef["isAJAX"]))
                            {
                                $this->Convert = $TmpDef["isAJAX"]["Convert"];
                            }
                            if(array_key_exists("template", $TmpDef["isAJAX"]))
                            {
                                $this->Template = $TmpDef["isAJAX"]["template"];
                            }
                        }
                    }
                    else
                    {
                        if(array_key_exists("isNotAJAX", $TmpDef))
                        {
                            if(array_key_exists("addArgument", $TmpDef["isNotAJAX"]))
                            {
                                $this->AddArgument = $TmpDef["isNotAJAX"]["addArgument"];
                            }
                            if(array_key_exists("setCookies", $TmpDef["isNotAJAX"]))
                            {
                                $this->setCookies[] = $TmpDef["isNotAJAX"]["setCookies"];
                            }
                            if(array_key_exists("headers", $TmpDef["isNotAJAX"]))
                            {
                                $this->AdvHeaders = $TmpDef["isNotAJAX"]["headers"];
                            }
                            if(array_key_exists("setFormUID", $TmpDef["isNotAJAX"]))
                            {
                                $this->SetFormUID = $TmpDef["isNotAJAX"]["setFormUID"];
                            }
                            if(array_key_exists("access", $TmpDef["isNotAJAX"]))
                            {
                                $this->CurrentAccess = $TmpDef["isNotAJAX"]["access"];
                            }
                            if(array_key_exists("controllers", $TmpDef["isNotAJAX"]))
                            {
                                $this->CurrentControllers = $TmpDef["isNotAJAX"]["controllers"];
                            }
                            if(array_key_exists("useController", $TmpDef["isNotAJAX"]))
                            {
                                $this->Controller = $TmpDef["isNotAJAX"]["useController"];
                            }
                            if(array_key_exists("useAction", $TmpDef["isNotAJAX"]))
                            {
                                $this->Action = $TmpDef["isNotAJAX"]["useAction"];
                            }
                            if(array_key_exists("useArgument", $TmpDef["isNotAJAX"]) && $CurrPath !== null)
                            {
                                $this->Arguments[$TmpDef["isNotAJAX"]["useArgument"]] = $CurrPath;
                            }
                            if(array_key_exists("defaultAction", $TmpDef["isNotAJAX"]))
                            {
                                $this->WSAction = $TmpDef["isNotAJAX"]["defaultAction"];
                            }
                            if(array_key_exists("dataKey", $TmpDef["isNotAJAX"]))
                            {
                                $this->KeyData = $TmpDef["isNotAJAX"]["dataKey"];
                            }
                            if(array_key_exists("useTemplater", $TmpDef["isNotAJAX"]))
                            {
                                $this->Templater = $TmpDef["isNotAJAX"]["useTemplater"];
                            }
                            if(array_key_exists("debugSets", $TmpDef["isNotAJAX"]))
                            {
                                $DebugSets = $TmpDef["isNotAJAX"]["debugSets"];
                            }
                            if(array_key_exists("data", $TmpDef["isNotAJAX"]))
                            {
                                $this->AdvActions = $TmpDef["isNotAJAX"]["data"];
                            }
                            if(array_key_exists("fileOriginalName", $TmpDef["isNotAJAX"]))
                            {
                                $this->FileOriginalName = $TmpDef["isNotAJAX"]["fileOriginalName"];
                            }
                            if(array_key_exists("fileName", $TmpDef["isNotAJAX"]))
                            {
                                $this->FileName = $TmpDef["isNotAJAX"]["fileName"];
                            }
                            if(array_key_exists("OutCharSet", $TmpDef["isNotAJAX"]))
                            {
                                $this->OutCharSet = $TmpDef["isNotAJAX"]["OutCharSet"];
                            }
                            if(array_key_exists("SendType", $TmpDef["isNotAJAX"]))
                            {
                                $this->SendType = $TmpDef["isNotAJAX"]["SendType"];
                            }
                            if(array_key_exists("Convert", $TmpDef["isNotAJAX"]))
                            {
                                $this->Convert = $TmpDef["isNotAJAX"]["Convert"];
                            }
                            if(array_key_exists("template", $TmpDef["isNotAJAX"]))
                            {
                                $this->Template = $TmpDef["isNotAJAX"]["template"];
                            }
                            $this->AdvActions = $TmpDef["isNotAJAX"];
                        }
                    }
					$this->PrepareDomain($TmpDef, null, $Path);
				}
				elseif(array_key_exists("*", $TmpDef))
				{
                    if(array_key_exists("addArgument", $TmpDef))
                    {
                        $this->AddArgument = $TmpDef["addArgument"];
                    }
                    if(array_key_exists("setCookies", $TmpDef))
                    {
                        $this->setCookies[] = $TmpDef["setCookies"];
                    }
					if(array_key_exists("useArgument", $TmpDef) && $CurrPath !== null)
					{
						$this->Arguments[$TmpDef["useArgument"]] = $CurrPath;
					}
                    if(array_key_exists("dataKey", $TmpDef))
                    {
                        $this->KeyData = $TmpDef["dataKey"];
                    }
                    if(array_key_exists("setFormUID", $TmpDef))
                    {
                        $this->SetFormUID = $TmpDef["setFormUID"];
                    }
					if(array_key_exists("useController", $TmpDef))
					{
						$this->Controller = $TmpDef["useController"];
					}
					if(array_key_exists("useAction", $TmpDef))
					{
						$this->Action = $TmpDef["useAction"];
					}
					if(array_key_exists("useTemplater", $TmpDef))
					{
						$this->Templater = $TmpDef["useTemplater"];
					}
					if(array_key_exists("debugSets", $TmpDef))
					{
						$DebugSets = $TmpDef["debugSets"];
					}
					if(array_key_exists("headers", $TmpDef))
					{
						$this->AdvHeaders = $TmpDef["headers"];
					}
                    if(array_key_exists("Convert", $TmpDef))
                    {
						$this->Convert = $TmpDef["Convert"];
                    }
                    if(array_key_exists("template", $TmpDef))
                    {
                        $this->Template = $TmpDef["template"];
                    }
                    if($this->IsAJAX())
                    {
                        if(array_key_exists("isAJAX", $TmpDef))
                        {
                            if(array_key_exists("addArgument", $TmpDef["isAJAX"]))
                            {
                                $this->AddArgument = $TmpDef["isAJAX"]["addArgument"];
                            }
                            if(array_key_exists("setCookies", $TmpDef["isAJAX"]))
                            {
                                $this->setCookies[] = $TmpDef["isAJAX"]["setCookies"];
                            }
                            if(array_key_exists("headers", $TmpDef["isAJAX"]))
                            {
                                $this->AdvHeaders = $TmpDef["isAJAX"]["headers"];
                            }
                            if(array_key_exists("setFormUID", $TmpDef["isAJAX"]))
                            {
                                $this->SetFormUID = $TmpDef["isAJAX"]["setFormUID"];
                            }
                            if(array_key_exists("access", $TmpDef["isAJAX"]))
                            {
                                $this->CurrentAccess = $TmpDef["isAJAX"]["access"];
                            }
                            if(array_key_exists("controllers", $TmpDef["isAJAX"]))
                            {
                                $this->CurrentControllers = $TmpDef["isAJAX"]["controllers"];
                            }
                            if(array_key_exists("useController", $TmpDef["isAJAX"]))
                            {
                                $this->Controller = $TmpDef["isAJAX"]["useController"];
                            }
                            if(array_key_exists("useAction", $TmpDef["isAJAX"]))
                            {
                                $this->Action = $TmpDef["isAJAX"]["useAction"];
                            }
                            if(array_key_exists("useArgument", $TmpDef["isAJAX"]) && $CurrPath !== null)
                            {
                                $this->Arguments[$TmpDef["isAJAX"]["useArgument"]] = $CurrPath;
                            }
                            if(array_key_exists("defaultAction", $TmpDef["isAJAX"]))
                            {
                                $this->WSAction = $TmpDef["isAJAX"]["defaultAction"];
                            }
                            if(array_key_exists("dataKey", $TmpDef["isAJAX"]))
                            {
                                $this->KeyData = $TmpDef["isAJAX"]["dataKey"];
                            }
                            if(array_key_exists("useTemplater", $TmpDef["isAJAX"]))
                            {
                                $this->Templater = $TmpDef["isAJAX"]["useTemplater"];
                            }
                            if(array_key_exists("debugSets", $TmpDef["isAJAX"]))
                            {
                                $DebugSets = $TmpDef["isAJAX"]["debugSets"];
                            }
                            if(array_key_exists("data", $TmpDef["isAJAX"]))
                            {
                                $this->AdvActions = $TmpDef["isAJAX"]["data"];
                            }
                            if(array_key_exists("fileOriginalName", $TmpDef["isAJAX"]))
                            {
                                $this->FileOriginalName = $TmpDef["isAJAX"]["fileOriginalName"];
                            }
                            if(array_key_exists("fileName", $TmpDef["isAJAX"]))
                            {
                                $this->FileName = $TmpDef["isAJAX"]["fileName"];
                            }
                            if(array_key_exists("OutCharSet", $TmpDef["isAJAX"]))
                            {
                                $this->OutCharSet = $TmpDef["isAJAX"]["OutCharSet"];
                            }
                            if(array_key_exists("SendType", $TmpDef["isAJAX"]))
                            {
                                $this->SendType = $TmpDef["isAJAX"]["SendType"];
                            }
                            if(array_key_exists("Convert", $TmpDef["isAJAX"]))
                            {
                                $this->Convert = $TmpDef["isAJAX"]["Convert"];
                            }
                            if(array_key_exists("template", $TmpDef["isAJAX"]))
                            {
                                $this->Template = $TmpDef["isAJAX"]["template"];
                            }
                        }
                    }
                    else
                    {
                        if(array_key_exists("isNotAJAX", $TmpDef))
                        {
                            if(array_key_exists("addArgument", $TmpDef["isNotAJAX"]))
                            {
                                $this->AddArgument = $TmpDef["isNotAJAX"]["addArgument"];
                            }
                            if(array_key_exists("setCookies", $TmpDef["isNotAJAX"]))
                            {
                                $this->setCookies[] = $TmpDef["isNotAJAX"]["setCookies"];
                            }
                            if(array_key_exists("headers", $TmpDef["isNotAJAX"]))
                            {
                                $this->AdvHeaders = $TmpDef["isNotAJAX"]["headers"];
                            }
                            if(array_key_exists("setFormUID", $TmpDef["isNotAJAX"]))
                            {
                                $this->SetFormUID = $TmpDef["isNotAJAX"]["setFormUID"];
                            }
                            if(array_key_exists("access", $TmpDef["isNotAJAX"]))
                            {
                                $this->CurrentAccess = $TmpDef["isNotAJAX"]["access"];
                            }
                            if(array_key_exists("controllers", $TmpDef["isNotAJAX"]))
                            {
                                $this->CurrentControllers = $TmpDef["isNotAJAX"]["controllers"];
                            }
                            if(array_key_exists("useController", $TmpDef["isNotAJAX"]))
                            {
                                $this->Controller = $TmpDef["isNotAJAX"]["useController"];
                            }
                            if(array_key_exists("useAction", $TmpDef["isNotAJAX"]))
                            {
                                $this->Action = $TmpDef["isNotAJAX"]["useAction"];
                            }
                            if(array_key_exists("useArgument", $TmpDef["isNotAJAX"]) && $CurrPath !== null)
                            {
                                $this->Arguments[$TmpDef["isNotAJAX"]["useArgument"]] = $CurrPath;
                            }
                            if(array_key_exists("defaultAction", $TmpDef["isNotAJAX"]))
                            {
                                $this->WSAction = $TmpDef["isNotAJAX"]["defaultAction"];
                            }
                            if(array_key_exists("dataKey", $TmpDef["isNotAJAX"]))
                            {
                                $this->KeyData = $TmpDef["isNotAJAX"]["dataKey"];
                            }
                            if(array_key_exists("useTemplater", $TmpDef["isNotAJAX"]))
                            {
                                $this->Templater = $TmpDef["isNotAJAX"]["useTemplater"];
                            }
                            if(array_key_exists("debugSets", $TmpDef["isNotAJAX"]))
                            {
                                $DebugSets = $TmpDef["isNotAJAX"]["debugSets"];
                            }
                            if(array_key_exists("data", $TmpDef["isNotAJAX"]))
                            {
                                $this->AdvActions = $TmpDef["isNotAJAX"]["data"];
                            }
                            if(array_key_exists("fileOriginalName", $TmpDef["isNotAJAX"]))
                            {
                                $this->FileOriginalName = $TmpDef["isNotAJAX"]["fileOriginalName"];
                            }
                            if(array_key_exists("fileName", $TmpDef["isNotAJAX"]))
                            {
                                $this->FileName = $TmpDef["isNotAJAX"]["fileName"];
                            }
                            if(array_key_exists("OutCharSet", $TmpDef["isNotAJAX"]))
                            {
                                $this->OutCharSet = $TmpDef["isNotAJAX"]["OutCharSet"];
                            }
                            if(array_key_exists("SendType", $TmpDef["isNotAJAX"]))
                            {
                                $this->SendType = $TmpDef["isNotAJAX"]["SendType"];
                            }
                            if(array_key_exists("Convert", $TmpDef["isNotAJAX"]))
                            {
                                $this->Convert = $TmpDef["isNotAJAX"]["Convert"];
                            }
                            if(array_key_exists("template", $TmpDef["isNotAJAX"]))
                            {
                                $this->Template = $TmpDef["isNotAJAX"]["template"];
                            }
                        }
                    }
					$this->PrepareDomain($TmpDef, null, $Path);
				}
				else
				{
					if(array_key_exists("default", $TmpDef))
					{
						$TmpDef = $TmpDef["default"];
                        if(array_key_exists("addArgument", $TmpDef))
                        {
                            $this->AddArgument = $TmpDef["addArgument"];
                        }
                        if(array_key_exists("setCookies", $TmpDef))
                        {
                            $this->setCookies[] = $TmpDef["setCookies"];
                        }
						if(array_key_exists("access", $TmpDef))
						{
							$this->CurrentAccess = $TmpDef["access"];
						}
                        if(array_key_exists("setFormUID", $TmpDef))
                        {
                            $this->SetFormUID = $TmpDef["setFormUID"];
                        }
						if(array_key_exists("controllers", $TmpDef))
						{
							$this->CurrentControllers = $TmpDef["controllers"];
						}
						if(array_key_exists("useController", $TmpDef))
						{
							$this->Controller = $TmpDef["useController"];
						}
						if(array_key_exists("useAction", $TmpDef))
						{
							$this->Action = $TmpDef["useAction"];
						}
						if(array_key_exists("useArgument", $TmpDef) && $CurrPath !== null)
						{
							$this->Arguments[$TmpDef["useArgument"]] = $CurrPath;
						}
						if(array_key_exists("defaultAction", $TmpDef))
						{
							$this->WSAction = $TmpDef["defaultAction"];
						}
						if(array_key_exists("setInDataTypeAsAJAX", $TmpDef) && $TmpDef["setInDataTypeAsAJAX"])
						{
							$this->IsAJAX = true;
						}
						if(array_key_exists("backURL", $TmpDef))
						{
							$this->BackURL = $TmpDef["backURL"];
						}
						if(array_key_exists("useTemplater", $TmpDef))
						{
							$this->Templater = $TmpDef["useTemplater"];
						}
						if(array_key_exists("debugSets", $TmpDef))
						{
							$DebugSets = $TmpDef["debugSets"];
						}
						if(array_key_exists("headers", $TmpDef))
						{
							$this->AdvHeaders = $TmpDef["headers"];
						}
                        if(array_key_exists("dataKey", $TmpDef))
						{
							$this->KeyData = $TmpDef["dataKey"];
						}
						if(array_key_exists("fileOriginalName", $TmpDef))
						{
							$this->FileOriginalName = $TmpDef["fileOriginalName"];
						}
						if(array_key_exists("fileName", $TmpDef))
						{
							$this->FileName = $TmpDef["fileName"];
						}
						if(array_key_exists("OutCharSet", $TmpDef))
						{
							$this->OutCharSet = $TmpDef["OutCharSet"];
						}
						if(array_key_exists("SendType", $TmpDef))
                        {
							$this->SendType = $TmpDef["SendType"];
                        }
                        if(array_key_exists("Convert", $TmpDef))
                        {
							$this->Convert = $TmpDef["Convert"];
                        }
                        if(array_key_exists("template", $TmpDef))
                        {
							$this->Template = $TmpDef["template"];
                        }
						if($this->IsAJAX())
						{
							if(array_key_exists("isAJAX", $TmpDef))
							{
                                if(array_key_exists("addArgument", $TmpDef["isAJAX"]))
                                {
                                    $this->AddArgument = $TmpDef["isAJAX"]["addArgument"];
                                }
                                if(array_key_exists("setCookies", $TmpDef["isAJAX"]))
                                {
                                    $this->setCookies[] = $TmpDef["isAJAX"]["setCookies"];
                                }
								if(array_key_exists("headers", $TmpDef["isAJAX"]))
								{
									$this->AdvHeaders = $TmpDef["isAJAX"]["headers"];
								}
                                if(array_key_exists("setFormUID", $TmpDef["isAJAX"]))
                                {
                                    $this->SetFormUID = $TmpDef["isAJAX"]["setFormUID"];
                                }
								if(array_key_exists("access", $TmpDef["isAJAX"]))
								{
									$this->CurrentAccess = $TmpDef["isAJAX"]["access"];
								}
								if(array_key_exists("controllers", $TmpDef["isAJAX"]))
								{
									$this->CurrentControllers = $TmpDef["isAJAX"]["controllers"];
								}
								if(array_key_exists("useController", $TmpDef["isAJAX"]))
								{
									$this->Controller = $TmpDef["isAJAX"]["useController"];
								}
								if(array_key_exists("useAction", $TmpDef["isAJAX"]))
								{
									$this->Action = $TmpDef["isAJAX"]["useAction"];
								}
								if(array_key_exists("useArgument", $TmpDef["isAJAX"]) && $CurrPath !== null)
								{
									$this->Arguments[$TmpDef["isAJAX"]["useArgument"]] = $CurrPath;
								}
								if(array_key_exists("defaultAction", $TmpDef["isAJAX"]))
								{
									$this->WSAction = $TmpDef["isAJAX"]["defaultAction"];
								}
								if(array_key_exists("dataKey", $TmpDef["isAJAX"]))
								{
									$this->KeyData = $TmpDef["isAJAX"]["dataKey"];
								}
								if(array_key_exists("useTemplater", $TmpDef["isAJAX"]))
								{
									$this->Templater = $TmpDef["isAJAX"]["useTemplater"];
								}
								if(array_key_exists("debugSets", $TmpDef["isAJAX"]))
								{
									$DebugSets = $TmpDef["isAJAX"]["debugSets"];
								}
                                if(array_key_exists("data", $TmpDef["isAJAX"]))
                                {
                                    $this->AdvActions = $TmpDef["isAJAX"]["data"];
                                }
                                if(array_key_exists("dataKey", $TmpDef["isAJAX"]))
                                {
                                    $this->KeyData = $TmpDef["isAJAX"]["dataKey"];
                                }
                                if(array_key_exists("fileOriginalName", $TmpDef["isAJAX"]))
                                {
                                    $this->FileOriginalName = $TmpDef["isAJAX"]["fileOriginalName"];
                                }
                                if(array_key_exists("fileName", $TmpDef["isAJAX"]))
                                {
                                    $this->FileName = $TmpDef["isAJAX"]["fileName"];
                                }
                                if(array_key_exists("OutCharSet", $TmpDef["isAJAX"]))
                                {
                                    $this->OutCharSet = $TmpDef["isAJAX"]["OutCharSet"];
                                }
                                if(array_key_exists("SendType", $TmpDef["isAJAX"]))
                                {
                                    $this->SendType = $TmpDef["isAJAX"]["SendType"];
                                }
                                if(array_key_exists("Convert", $TmpDef["isAJAX"]))
                                {
                                    $this->Convert = $TmpDef["isAJAX"]["Convert"];
                                }
                                if(array_key_exists("template", $TmpDef["isAJAX"]))
                                {
                                    $this->Template = $TmpDef["isAJAX"]["template"];
                                }
							}
						}
						else
						{
							if(array_key_exists("isNotAJAX", $TmpDef))
							{
                                if(array_key_exists("addArgument", $TmpDef["isNotAJAX"]))
                                {
                                    $this->AddArgument = $TmpDef["isNotAJAX"]["addArgument"];
                                }
                                if(array_key_exists("setCookies", $TmpDef["isNotAJAX"]))
                                {
                                    $this->setCookies[] = $TmpDef["isNotAJAX"]["setCookies"];
                                }
								if(array_key_exists("headers", $TmpDef["isNotAJAX"]))
								{
									$this->AdvHeaders = $TmpDef["isNotAJAX"]["headers"];
								}
                                if(array_key_exists("setFormUID", $TmpDef["isNotAJAX"]))
                                {
                                    $this->SetFormUID = $TmpDef["isNotAJAX"]["setFormUID"];
                                }
								if(array_key_exists("access", $TmpDef["isNotAJAX"]))
								{
									$this->CurrentAccess = $TmpDef["isNotAJAX"]["access"];
								}
								if(array_key_exists("controllers", $TmpDef["isNotAJAX"]))
								{
									$this->CurrentControllers = $TmpDef["isNotAJAX"]["controllers"];
								}
								if(array_key_exists("useController", $TmpDef["isNotAJAX"]))
								{
									$this->Controller = $TmpDef["isNotAJAX"]["useController"];
								}
								if(array_key_exists("useAction", $TmpDef["isNotAJAX"]))
								{
									$this->Action = $TmpDef["isNotAJAX"]["useAction"];
								}
								if(array_key_exists("useArgument", $TmpDef["isNotAJAX"]) && $CurrPath !== null)
								{
									$this->Arguments[$TmpDef["isNotAJAX"]["useArgument"]] = $CurrPath;
								}
								if(array_key_exists("defaultAction", $TmpDef["isNotAJAX"]))
								{
									$this->WSAction = $TmpDef["isNotAJAX"]["defaultAction"];
								}
								if(array_key_exists("dataKey", $TmpDef["isNotAJAX"]))
								{
									$this->KeyData = $TmpDef["isNotAJAX"]["dataKey"];
								}
								if(array_key_exists("useTemplater", $TmpDef["isNotAJAX"]))
								{
									$this->Templater = $TmpDef["isNotAJAX"]["useTemplater"];
								}
								if(array_key_exists("debugSets", $TmpDef["isNotAJAX"]))
								{
									$DebugSets = $TmpDef["isNotAJAX"]["debugSets"];
								}
                                if(array_key_exists("data", $TmpDef["isNotAJAX"]))
                                {
                                    $this->AdvActions = $TmpDef["isNotAJAX"]["data"];
                                }
                                if(array_key_exists("fileOriginalName", $TmpDef["isNotAJAX"]))
                                {
                                    $this->FileOriginalName = $TmpDef["isNotAJAX"]["fileOriginalName"];
                                }
                                if(array_key_exists("fileName", $TmpDef["isNotAJAX"]))
                                {
                                    $this->FileName = $TmpDef["isNotAJAX"]["fileName"];
                                }
                                if(array_key_exists("OutCharSet", $TmpDef["isNotAJAX"]))
                                {
                                    $this->OutCharSet = $TmpDef["isNotAJAX"]["OutCharSet"];
                                }
                                if(array_key_exists("SendType", $TmpDef["isNotAJAX"]))
                                {
                                    $this->SendType = $TmpDef["isNotAJAX"]["SendType"];
                                }
                                if(array_key_exists("Convert", $TmpDef["isNotAJAX"]))
                                {
                                    $this->Convert = $TmpDef["isNotAJAX"]["Convert"];
                                }
                                if(array_key_exists("template", $TmpDef["isNotAJAX"]))
                                {
                                    $this->Template = $TmpDef["isNotAJAX"]["template"];
                                }
							}
						}
					}
					else
					{
						throw new dmtException("Configuration error. Not find default section. ".$_SERVER["REQUEST_URI"], 77770);
					}
				}
			}
			else
			{
				if(array_key_exists("empty", $TmpDef))
				{
					$TmpDef = $TmpDef["empty"];
				}
				elseif(array_key_exists("default", $TmpDef))
				{
					$TmpDef = $TmpDef["default"];
				}
				else
				{
					throw new dmtException("Configuration error", 77770);
				}
				if(array_key_exists("access", $TmpDef))
                {
                    $this->CurrentAccess = $TmpDef["access"];
                }
                if(array_key_exists("controllers", $TmpDef))
                {
                    $this->CurrentControllers = $TmpDef["controllers"];
                }
                if(array_key_exists("useController", $TmpDef))
                {
                    $this->Controller = $TmpDef["useController"];
                }
                if(array_key_exists("useAction", $TmpDef))
                {
                    $this->Action = $TmpDef["useAction"];
                }
                if(array_key_exists("setCookies", $TmpDef))
                {
                    $this->setCookies[] = $TmpDef["setCookies"];
                }
                /*
                if(array_key_exists("useArgument", $TmpDef) && $CurrPath !== null)
                {
                    $this->Arguments[$TmpDef["useArgument"]] = $CurrPath;
                }
                 */
                if(array_key_exists("defaultAction", $TmpDef))
                {
                    $this->WSAction = $TmpDef["defaultAction"];
                }
                if(array_key_exists("setInDataTypeAsAJAX", $TmpDef) && $TmpDef["setInDataTypeAsAJAX"])
                {
                    $this->IsAJAX = true;
                }
                if(array_key_exists("backURL", $TmpDef))
                {
                    $this->BackURL = $TmpDef["backURL"];
                }
                if(array_key_exists("useTemplater", $TmpDef))
                {
                    $this->Templater = $TmpDef["useTemplater"];
                }
                if(array_key_exists("debugSets", $TmpDef))
                {
                    $DebugSets = $TmpDef["debugSets"];
                }
                if(array_key_exists("headers", $TmpDef))
                {
                    $this->AdvHeaders = $TmpDef["headers"];
                }
                if(array_key_exists("dataKey", $TmpDef))
                {
                    $this->KeyData = $TmpDef["dataKey"];
                }
                if(array_key_exists("fileOriginalName", $TmpDef))
                {
                    $this->FileOriginalName = $TmpDef["fileOriginalName"];
                }
                if(array_key_exists("fileName", $TmpDef))
                {
                    $this->FileName = $TmpDef["fileName"];
                }
                if(array_key_exists("OutCharSet", $TmpDef))
                {
                    $this->OutCharSet = $TmpDef["OutCharSet"];
                }
                if(array_key_exists("SendType", $TmpDef))
                {
                    $this->SendType = $TmpDef["SendType"];
                }
                if(array_key_exists("Convert", $TmpDef))
                {
                    $this->Convert = $TmpDef["Convert"];
                }
                if(array_key_exists("setFormUID", $TmpDef))
                {
                    $this->SetFormUID = $TmpDef["setFormUID"];
                }
                if(array_key_exists("template", $TmpDef))
                {
                    $this->Template = $TmpDef["template"];
                }
                if(array_key_exists("addArgument", $TmpDef))
                {
                    $this->AddArgument = $TmpDef["addArgument"];
                }
                if($this->IsAJAX())
                {
                    if(array_key_exists("isAJAX", $TmpDef))
                    {
                        if(array_key_exists("addArgument", $TmpDef["isAJAX"]))
                        {
                            $this->AddArgument = $TmpDef["isAJAX"]["addArgument"];
                        }
                        if(array_key_exists("setCookies", $TmpDef["isAJAX"]))
                        {
                            $this->setCookies[] = $TmpDef["isAJAX"]["setCookies"];
                        }
                        if(array_key_exists("headers", $TmpDef["isAJAX"]))
                        {
                            $this->AdvHeaders = $TmpDef["isAJAX"]["headers"];
                        }
                        if(array_key_exists("template", $TmpDef["isAJAX"]))
                        {
                            $this->Template = $TmpDef["isAJAX"]["template"];
                        }
                        if(array_key_exists("setFormUID", $TmpDef))
                        {
                            $this->SetFormUID = $TmpDef["isAJAX"]["setFormUID"];
                        }
                        if(array_key_exists("access", $TmpDef["isAJAX"]))
                        {
                            $this->CurrentAccess = $TmpDef["isAJAX"]["access"];
                        }
                        if(array_key_exists("controllers", $TmpDef["isAJAX"]))
                        {
                            $this->CurrentControllers = $TmpDef["isAJAX"]["controllers"];
                        }
                        if(array_key_exists("useController", $TmpDef["isAJAX"]))
                        {
                            $this->Controller = $TmpDef["isAJAX"]["useController"];
                        }
                        if(array_key_exists("useAction", $TmpDef["isAJAX"]))
                        {
                            $this->Action = $TmpDef["isAJAX"]["useAction"];
                        }
                        /*
                        if(array_key_exists("useArgument", $TmpDef["isAJAX"]) && $CurrPath !== null)
                        {
                            $this->Arguments[$TmpDef["isAJAX"]["useArgument"]] = $CurrPath;
                        }
                         */
                        if(array_key_exists("defaultAction", $TmpDef["isAJAX"]))
                        {
                            $this->WSAction = $TmpDef["isAJAX"]["defaultAction"];
                        }
                        if(array_key_exists("dataKey", $TmpDef["isAJAX"]))
                        {
                            $this->KeyData = $TmpDef["isAJAX"]["dataKey"];
                        }
                        if(array_key_exists("useTemplater", $TmpDef["isAJAX"]))
                        {
                            $this->Templater = $TmpDef["isAJAX"]["useTemplater"];
                        }
                        if(array_key_exists("debugSets", $TmpDef["isAJAX"]))
                        {
                            $DebugSets = $TmpDef["isAJAX"]["debugSets"];
                        }
                        if(array_key_exists("data", $TmpDef["isAJAX"]))
                        {
                            $this->AdvActions = $TmpDef["isAJAX"]["data"];
                        }
                        if(array_key_exists("dataKey", $TmpDef["isAJAX"]))
                        {
                            $this->KeyData = $TmpDef["isAJAX"]["dataKey"];
                        }
                        if(array_key_exists("fileOriginalName", $TmpDef["isAJAX"]))
                        {
                            $this->FileOriginalName = $TmpDef["isAJAX"]["fileOriginalName"];
                        }
                        if(array_key_exists("fileName", $TmpDef["isAJAX"]))
                        {
                            $this->FileName = $TmpDef["isAJAX"]["fileName"];
                        }
                        if(array_key_exists("OutCharSet", $TmpDef["isAJAX"]))
                        {
                            $this->OutCharSet = $TmpDef["isAJAX"]["OutCharSet"];
                        }
                        if(array_key_exists("SendType", $TmpDef["isAJAX"]))
                        {
                            $this->SendType = $TmpDef["isAJAX"]["SendType"];
                        }
                        if(array_key_exists("Convert", $TmpDef["isAJAX"]))
                        {
                            $this->Convert = $TmpDef["isAJAX"]["Convert"];
                        }
                    }
                }
                else
                {
                    if(array_key_exists("isNotAJAX", $TmpDef))
                    {
                        if(array_key_exists("addArgument", $TmpDef["isNotAJAX"]))
                        {
                            $this->AddArgument = $TmpDef["isNotAJAX"]["addArgument"];
                        }
                        if(array_key_exists("setCookies", $TmpDef["isNotAJAX"]))
                        {
                            $this->setCookies[] = $TmpDef["isNotAJAX"]["setCookies"];
                        }
                        if(array_key_exists("headers", $TmpDef["isNotAJAX"]))
                        {
                            $this->AdvHeaders = $TmpDef["isNotAJAX"]["headers"];
                        }
                        if(array_key_exists("template", $TmpDef["isNotAJAX"]))
                        {
                            $this->Template = $TmpDef["isNotAJAX"]["template"];
                        }
                        if(array_key_exists("setFormUID", $TmpDef))
                        {
                            $this->SetFormUID = $TmpDef["isNotAJAX"]["setFormUID"];
                        }
                        if(array_key_exists("access", $TmpDef["isNotAJAX"]))
                        {
                            $this->CurrentAccess = $TmpDef["isNotAJAX"]["access"];
                        }
                        if(array_key_exists("controllers", $TmpDef["isNotAJAX"]))
                        {
                            $this->CurrentControllers = $TmpDef["isNotAJAX"]["controllers"];
                        }
                        if(array_key_exists("useController", $TmpDef["isNotAJAX"]))
                        {
                            $this->Controller = $TmpDef["isNotAJAX"]["useController"];
                        }
                        if(array_key_exists("useAction", $TmpDef["isNotAJAX"]))
                        {
                            $this->Action = $TmpDef["isNotAJAX"]["useAction"];
                        }
                        if(array_key_exists("defaultAction", $TmpDef["isNotAJAX"]))
                        {
                            $this->WSAction = $TmpDef["isNotAJAX"]["defaultAction"];
                        }
                        if(array_key_exists("dataKey", $TmpDef["isNotAJAX"]))
                        {
                            $this->KeyData = $TmpDef["isNotAJAX"]["dataKey"];
                        }
                        if(array_key_exists("useTemplater", $TmpDef["isNotAJAX"]))
                        {
                            $this->Templater = $TmpDef["isNotAJAX"]["useTemplater"];
                        }
                        if(array_key_exists("debugSets", $TmpDef["isNotAJAX"]))
                        {
                            $DebugSets = $TmpDef["isNotAJAX"]["debugSets"];
                        }
                        if(array_key_exists("data", $TmpDef["isNotAJAX"]))
                        {
                            $this->AdvActions = $TmpDef["isNotAJAX"]["data"];
                        }
                        if(array_key_exists("dataKey", $TmpDef["isNotAJAX"]))
                        {
                            $this->KeyData = $TmpDef["isNotAJAX"]["dataKey"];
                        }
                        if(array_key_exists("fileOriginalName", $TmpDef["isNotAJAX"]))
                        {
                            $this->FileOriginalName = $TmpDef["isNotAJAX"]["fileOriginalName"];
                        }
                        if(array_key_exists("fileName", $TmpDef["isNotAJAX"]))
                        {
                            $this->FileName = $TmpDef["isNotAJAX"]["fileName"];
                        }
                        if(array_key_exists("OutCharSet", $TmpDef["isNotAJAX"]))
                        {
                            $this->OutCharSet = $TmpDef["isNotAJAX"]["OutCharSet"];
                        }
                        if(array_key_exists("SendType", $TmpDef["isNotAJAX"]))
                        {
                            $this->SendType = $TmpDef["isNotAJAX"]["SendType"];
                        }
                        if(array_key_exists("Convert", $TmpDef["isNotAJAX"]))
                        {
                            $this->Convert = $TmpDef["isNotAJAX"]["Convert"];
                        }
                    }
                }
			}
		}
		if($DebugSets)
		{
			Debug::Init()->SetDebugParams($DebugSets);
		}
	}

	protected function GetFuntion($Funtion)
	{
		return method_exists($Funtion, "Init") ? $Funtion::Init() : $Funtion;
	}

	protected function CallFunction($Funtion, $Arguments = null)
	{
		$M = null;
		$A = null;
		if(is_array($Funtion))
		{
			switch (sizeof($Funtion))
			{
				case 1:
					$M = $Funtion[0];
					break;
				case 2:
					if(is_array($Funtion[0]))
					{
						$M = array(
							$this->GetFuntion($Funtion[0][0]),
							$Funtion[0][1]
						);
						$A = $Funtion[1];
					}
					elseif(is_array($Funtion[1]))
					{
						$M = $Funtion[0];
						$A = $Funtion[1];
					}
					else
					{
						$M = array(
							$this->GetFuntion($Funtion[0]),
							$Funtion[1]
						);
					}
					break;
				case 3:
					$M = array(
						$this->GetFuntion($Funtion[0]),
						$Funtion[1]
					);
					$A = $Funtion[2];
					break;
				default:
					throw new dmtException("Configuration error", 77770);
			}
			if(!$A) $A = array();
		}
		else $M = $Funtion;
		if($Arguments)
		{
			if($A)
			{
				if(!is_array($A))
					$A = array($A);
				if(is_array($Arguments))
				{
					$A = $A + $Arguments;
				}
				else
				{
					array_push($A, $Arguments);
				}
			}
			else
			{
				$A = is_array($Arguments) ? $Arguments : array($Arguments);
			}
		}
		return call_user_func_array($M, $A);
	}

	protected function GetValue($Def, $Arguments = null)
	{
		if(!is_array($Def))
			return $Def;
		switch($Def[0])
		{
			case "exec":
				if($Arguments)
				{
					if(!is_array($Arguments))
						$Arguments = array($Arguments);
				}
				else $Arguments = array();
				if(isset($Def[2]))
				{
					if(is_array($Def[2]))
					{
						switch($Def[2][0])
						{
							case "names":
								$Arguments[] = $this->ClnData[$Def[2][1]];
								break;
							default:
								$Arguments = $Arguments + $Def[2];
						}
					}
					else
						array_push($Arguments, $Def[2]);
				}
				$Value = $this->CallFunction($Def[1], $Arguments);
				break;
			case "value":
				$Value = $Def[1];
				break;
			default:
				throw new dmtException("Configuration error", 77770);
		}
		return $Value;
	}


	protected function CheckCondition($Condition)
	{
		if(!is_array($Condition))
			return $Condition;
		if(!is_array($Condition[1]))
			throw new dmtException("Condition definition error");
		$ControllValue = $this->GetValue($Condition[0]);

		foreach ($Condition[1] as $v)
		{
			if(is_array($v[0]))
			{
				if(in_array($ControllValue, $v[0]))
				{
					$R = $v[1];
					break;
				}
			}
			else
			{
				if($ControllValue == $v[0])
					$R = $v[1];
					break;
			}
		}

		return is_array($R) ? $this->GetValue($R) : $R;
	}

	protected function PrepareAdvancedData()
	{
		$R = array();
		if(array_key_exists("data", $this->AdvActions))
		{
			if(array_key_exists("setFormUID", $this->AdvActions["data"]))
			{
				$R["_fid"] = md5(microtime(true));
			}
			foreach ($this->AdvActions["data"] as $k => $v)
			{
				$R[$k] = $this->GetValue($v);
			}
		}
		if($this->KeyData)
			$this->Returned = array($this->KeyData => $this->Returned);
		elseif(!$this->Returned) $this->Returned = array();
		elseif(!$this->IsAJAX)
		{
			throw new dmtException("Configuration error", 77770);
		}
		$this->Returned = array_merge($this->Returned, $R);
		if(array_key_exists("SendType", $this->AdvActions))
			$this->SendType = $this->AdvActions["SendType"];
		elseif(array_key_exists("defaultSendType", $this->AdvActions) && !$this->SendType)
			$this->SendType = $this->AdvActions["defaultSendType"];
		if(array_key_exists("ContentType", $this->AdvActions))
			$this->ContentType = $this->AdvActions["ContentType"];
		elseif(array_key_exists("defaultContentType", $this->AdvActions) && !$this->ContentType)
			$this->ContentType = $this->AdvActions["defaultContentType"];
		if(array_key_exists("template", $this->AdvActions))
			$this->Template = $this->AdvActions["template"];
		elseif(array_key_exists("defaultTemplate", $this->AdvActions) && !$this->Template)
			$this->Template = $this->AdvActions["defaultTemplate"];
		if(array_key_exists("backURL", $this->AdvActions))
			$this->BackURL = $this->AdvActions["backURL"];
	}


	/**
	 *
	 * Проверяет альтернативные названия доменов. Не реализовано в текущей версии
	 * @param string $Domain - Полное доменное имя
	 */
	protected function CheckAlternativeDomain($Domain)
	{
		//TODO: Реализовать метод
		return $Domain;
	}

	protected function _redirectAuth($Token)
	{
		$this->Dump(__METHOD__.":".__LINE__);
		$_SESSION["token"] = $Token;
		if($this->IsAJAX)
		{
			$this->AddSystemEvent("requestAuth",
								array(
									"url"	=> ($this->IsMobileApp ? ACCOUNTS_CLIENT_CONNECTIONS_URL_MAPS : ACCOUNTS_CLIENT_CONNECTIONS_URL_IFRAME).$Token
								));
$this->Dump(__METHOD__.":".__LINE__, array(
									"url"	=> ($this->IsMobileApp ? ACCOUNTS_CLIENT_CONNECTIONS_URL_MAPS : ACCOUNTS_CLIENT_CONNECTIONS_URL_IFRAME).$Token
								));
		}
		else WS::Init()->Redirect(ACCOUNTS_CLIENT_CONNECTIONS_URL.$Token);
	}
	
	protected function AddSystemEvent($Type, $Detail = null)
	{
		$this->SystemEvents[] = array(
			"eventType" =>	$Type,
			"detail"	=> $Detail
		);
	}


	protected function RedirectAuth()
	{
		$this->Dump(__METHOD__.":".__LINE__);
		$Ret = false;
		$Url = explode("?", WS::Init()->Protocol.$_SERVER["HTTP_HOST"].(mb_substr($_SERVER["REQUEST_URI"], 0, 1) == "/" ? "" : "/").$_SERVER["REQUEST_URI"]);
		$R = AccountsDriver::GetAuthToken(User::Init()->GetSessionId(), $_SERVER["HTTP_HOST"], $Url[0]);
		if($R["depricated"])
		{
			$this->_redirectAuth($R["token"]);
		}
		elseif($R["userId"])
		{
			User::Init()->LoadUser();
			$Ret = true;
		}
		else
		{
			$this->Dump(__METHOD__.":".__LINE__);
			$this->_redirectAuth($R["token"]);
		}
		$this->Dump(__METHOD__.":".__LINE__, "Ret = ".($Ret ? "true" : "false"));
		return $Ret;
	}


	/**
	 *
	 * Проверяет права доступа на основе секций access описания режимов
	 */
	protected function CheckAccess()
	{
		$Ret = false;
		if($this->CurrentAccess)
		{
			if(array_key_exists("method", $this->CurrentAccess))
			{
				if(in_array(WS::Init()->GetMethod(), $this->CurrentAccess["method"]))
					$Ret = true;
				else $this->Forbidden();
			}
			if(array_key_exists("onlyAJAX", $this->CurrentAccess) && $this->CurrentAccess["onlyAJAX"])
			{
				if(WS::Init()->IsAJAX())
					$Ret = true;
				else $this->Forbidden();
			}
			if(array_key_exists("ip", $this->CurrentAccess))
			{
				if(in_array(WS::Init()->GetIP(), $this->CurrentAccess["ip"]))
					$Ret = true;
				else $this->Forbidden();
			}
			if(array_key_exists("headers", $this->CurrentAccess))
			{
				foreach ($this->CurrentAccess["headers"] as $k => $v)
				{
					$k = "HTTP_".mb_str_replace("-", "_", mb_strtoupper($k));
					if(isset($_SERVER[$k]))
						$Ret = true;
					else $this->Forbidden();

					switch($v[0])
					{
						case "exec":
							$Arguments = array($_SERVER[$k]);
							if(isset($v[2]))
							{
								if(is_array($v[2]))
									$Arguments = $Arguments + $v[2];
								else array_push($Arguments, $v[2]);
							}
							if($this->CallFunction($v[1], $Arguments) === false)
								$this->Forbidden();
							$Ret = true;
							break;
						case "value":
							if($_SERVER[$k] == $v[1])
								$Ret = true;
							else $this->Forbidden();
							break;
						default:
							throw new dmtException("Configuration error", 77770);
					}
				}
			}
			if(array_key_exists("onlyAutorized", $this->CurrentAccess))
			{
				if(User::Init()->IsLogged())
				{
					if(isset($_SESSION["token"]))
					{
						try
						{
							$R = AccountsDriver::CheckToken($_SESSION["token"]);
							if(!$R["depricated"] && $R["userId"])
								$Ret = true;
							else
							{
								User::Init()->ResetSessionData();
								$Ret = $this->RedirectAuth();
							}
						}
						catch (dmtException $e)
						{
							User::Init()->ResetSessionData();
							$Ret = $this->RedirectAuth();
						}
					}
					else
					{
						User::Init()->ResetSessionData();
						$Ret = $this->RedirectAuth();
					}
				}
				else
				{
					if(isset($_SESSION) && isset($_SESSION["token"]))
					{
						try
						{
							$R = AccountsDriver::CheckToken($_SESSION["token"]);
							if($R["depricated"])
							{
								User::Init()->ResetSessionData();
								$Ret = $this->RedirectAuth();
							}
							elseif($R["userId"])
							{
								User::Init()->LoadUser();
								$Ret = true;
							}
							else
							{
								User::Init()->ResetSessionData();
								$Ret = $this->RedirectAuth();
							}
						}
						catch (dmtException $e)
						{
							User::Init()->ResetSessionData();
							$Ret = $this->RedirectAuth();
						}
					}
					else
					{
						$Ret = $this->RedirectAuth();
					}
				}
			}
			elseif(array_key_exists("permissions", $this->CurrentAccess))
			{
				PermissionsProcessor::Init()->CheckAccess($this->CurrentAccess["permissions"]);
				$Ret = true;
			}
		}
		else $Ret = true;

		$this->Dump(__METHOD__.":".__LINE__, "Ret = ".($Ret ? "true" : "false"));

		return $Ret;
	}

	/**
	 *
	 * Получает имя контроллера по его псевдониму и сохраняет в свойство Controller
	 * @param string $Controller - Псевдоним контроллера
	 */
	protected function GetController($Controller)
	{
		if($Controller)
		{
			if(array_key_exists($Controller, $this->Controllers))
				$this->Controller = $this->Controllers[$Controller];
			else $this->Forbidden();
		}
	}

	/**
	 *
	 * Проверяет разрешение использования контроллера
	 * @param string $Controller - Имя контроллера
	 */
	protected function CheckController($Controller = null)
	{
		if($this->CurrentControllers)
		{
			if(!$Controller)
				$Controller = $this->Controller;
			if(!array_key_exists($Controller, $this->CurrentControllers))
				$this->Forbidden();
		}
	}

	/**
	 *
	 * Проверяет разрешение использования акции
	 * @param string $Action - Имя акции
	 */
	protected function CheckAction($Action = null)
	{
		if($this->CurrentControllers)
		{
			if(!$Action)
				$Action = $this->Action;
			if(!in_array($Action, $this->CurrentControllers[$this->Controller]))
				$this->Forbidden();
		}
	}

	/**
	 *
	 * Проверяет наличие действий по умолчанию и при их наличии реализует их
	 * @throws dmtException
	 */
	protected function CheckDefaultAction()
	{
        $R = true;
		if($this->WSAction)
		{
			$R = true;
			switch ($this->WSAction[0])
			{
				case WSCONFIG_ACTIONS_RELOAD:
					$this->Returned = file_get_contents($this->WSAction[1]);
					$this->SendType = isset($this->WSAction[2]) ? $this->WSAction[2] : SEND_TYPE_HTML;
                    $R = false;
					break;
				case WSCONFIG_ACTIONS_REDIRECT:
					$URL = $this->WSAction[1] ? $this->WSAction[1] : URL_BASE_FULL.$_SERVER["REQUEST_URI"];
					$this->Redirect($URL);
                    $R = false;
				case WSCONFIG_ACTIONS_ERROR:
					$this->SetError($this->WSAction[1]);
                    $R = false;
					break;
				case WSCONFIG_ACTIONS_USE_TEMPLATE:
					$this->Template = $this->WSAction[1];
					$this->SendType = isset($this->WSAction[2]) ? $this->WSAction[2] : SEND_TYPE_HTML;
					break;
				case WSCONFIG_ACTIONS_EXEC:
					$this->SendType = isset($this->WSAction[2]) ? $this->WSAction[2] : SEND_TYPE_HTML;
					if(!isset($this->WSAction[1]))
						throw new dmtException("Configuration error", 77770);
					call_user_func($this->WSAction[1]);
                    $R = false;
					break;
				case WSCONFIG_ACTIONS_SCRIPT:
					require_once $this->WSAction[1];
					break;
				case WSCONFIG_ACTIONS_EXIT:
					if(User::Init()->IsLogged())
					{
						AccountsDriver::Logout($_SESSION["token"]);
					}
					User::Init()->End();
					if($this->IsAJAX)
						$this->RedirectAuth();
					else WS::Init()->Redirect(URL_ADMIN_SYSTEM);
                    $R = false;
					break;
				default:
					throw new dmtException("Configuration error", 77770);
			}
		}
        return $R;
	}

	/**
	 *
	 * Обрабатывает глобальные ошибки
	 * @param integer $Error - Идентификатор типа ошибки (ERRORS_NOT_FOUND | ERRORS_ERROR_ACCESS_DENID)
	 */
    protected function SetError($Error = null)
    {
    	if(!$Error)
    		$Error = $this->DefaultError;
    	switch ($Error)
    	{
    		case ERRORS_NOT_FOUND:
    			$this->NotFound();
    			//break;
    		case ERRORS_ERROR_ACCESS_DENID:
    			$this->Forbidden();
    			//break;
    	}
    }

	/**
	 *
	 * Получает и подготавливает входящие данные для последующей передачи их контроллерам
	 */
	private function GetClientData()
	{
     	//Получение данных переданных через AJAX
     	if($this->IsAJAX)
		{
			if(isset($_POST["prq"]))
			{
				try {
					$Data = json_decode($_POST["prq"], true);
				}
				catch (dmtException $e)
				{
					$this->Forbidden();
				}
			}
		}
		else
		{
			if(sizeof($_REQUEST))
				$Data = $_REQUEST;
		}
		if(!$this->Controller)
		{
			//Если контроллер не задан в конфигурации путей, то пытаемся получить контроллер из входящих данных
			if(isset($Data["a"]))
			{
				if(array_key_exists($Data["a"], $this->Controllers))
					$this->Controller = $this->Controllers[$Data["a"]];
				unset($Data["a"]);
			}
		}
        if(!$this->Action && isset($Data) && isset($Data["b"]))
        {
            $this->Action = $Data["b"];
            unset($Data["b"]);
        }

		if(isset($Data["d"]))
		{
			$this->ClnData = $Data["d"];
			$this->PrepareVariables($this->ClnData);
		}
		else $this->ClnData = $_REQUEST;
		if(!is_array($this->ClnData))
			$this->ClnData = array();
		foreach ($this->Arguments as $k => $v)
		{
			$this->ClnData[$k] = $v;
		}

$this->Dump(__METHOD__.": ".__LINE__, $this->Controller, $this->Action, $this->ClnData);
	}

	protected function BuildAnswer($Result, $Returned, $Errors = null, $Message = null, $RetData = null)
	{
		$this->Returned = array();
		$this->Returned["error"] = $Result;
		$this->Returned["result"] = $Result != 0 ? false : $Returned;
		if($this->IsMobileApp)
			$this->Returned["maps"] = User::Init ()->GetSessionId();
		if(sizeof($this->SystemEvents))
			$this->Returned["systemEvents"] = $this->SystemEvents;
		if($Message)
			$this->Returned["message"] = $Message;
		if(is_array($Errors) && sizeof($Errors))
		{
			$this->Returned["errors"] = $Errors;
			if($RetData)
				$this->Returned["data"] = $RetData;
		}
	}

	/**
	 *
	 * Подключает необходимый контроллер и передает ему управление, собирает данные подготовленные контроллером
	 * @throws dmtException
	 */
	private function ModulesSwitcher()
	{
		if($this->GetFileStatus !== null && !is_empty($this->GetFileStatus) && $this->FileStatusName)
		{
			$this->Dump("************************* -----------------------------------------------");
			$M = Controller::GetFileSatatus($this->FileStatusName, $this->GetFileStatus);
			$this->BuildAnswer($M["errors"] ? 0 : 1, $M["returned"], $M["errors"]);
		}
		else
		{
			if(!$this->Action)
			{
				if(!$this->AdvActions)
					throw new dmtException("Configuration error", 77770);
				return;
			}
			$M = $this->Controller;
			$this->Controller = $M::Init();
			$this->Controller->Dispatcher($this->Action, $this->ClnData);
			if(!$this->SendType && $this->Controller->SendType)
				$this->SendType = $this->Controller->SendType;
			if($this->Controller->ContentType) $this->ContentType = $this->Controller->ContentType;
			if($this->IsAJAX)
			{
				try {
					if(!$this->SendType)
						$this->SendType = END_TYPE_JSON;
					$this->ContentType	= $this->Controller->ContentType;
					//Получаем результат выполнения
					if($this->SendType == SEND_TYPE_UPLOAD || $this->SendType == SEND_TYPE_TEXT)
					{
						$this->Returned = $this->Controller->Returned;
					}
					else
					{
						$this->BuildAnswer(	$this->Controller->Result == 7 || $this->Controller->Result == 5 ? 0 : $this->Controller->Result,
										($this->Controller->Result == 7 || $this->Controller->Result == 5 ? 0 : $this->Controller->Result) ? false : $this->Controller->Returned,
										sizeof($this->Controller->Errors) ? $this->Controller->Errors : null,
										$this->Controller->Message,
										$this->Controller->RetData);
					}
				}
				catch (dmtException $e)
				{
					$this->BuildAnswer(1, false);
					$this->SendType = SEND_TYPE_JSON;
				}
				//Если разрешена отправка отладочной информации, собираем и отправляем ее
				/*
				if(DEBUG_ENABLE && (DEBUG_REPORT_JSCONSOLE || DEBUG_REPORT_BOTH))
					$this->Returned[0]["Debug"] = $this->GetDebug();
				*/
			}
			else
			{
				if($this->Controller->ErrorTemplate)
				{
					$this->Template = $this->Controller->ErrorTemplate;
				}
				elseif($this->Controller->Template)
				{
					$this->Template = $this->Controller->Template;
				}
				$this->Returned = $this->Controller->IsError ? $this->Controller->RetData : $this->Controller->Returned;
				$this->Message = $this->Controller->Message;
				$this->Errors = $this->Controller->Errors;
			}
		}
	}

	private function PrepareBackURL()
	{
		$R = $this->CheckCondition($this->BackURL);
		$this->Returned["backURL"] = Templater::Init()->Merge(null, $this->Returned, null, null, $R);
	}

    public function GetCurrentController()
    {
        return $this->Controller;
    }

    /**
	 *
	 * Конструктор
	 */
	protected function __construct()
	{
		parent::__construct();
		$this->Sets();
	}
}