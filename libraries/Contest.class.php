<?php
class Contest extends Controller
{
  	/**
	 *
	 * @var Contest
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return Contest
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"carId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"email"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"phone"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
				),
				"gender"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
				),
				"lastName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
				),
				"firstName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"age"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"cityId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"comment"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				)
			)
		);
		$this->Modes = array(
			//Возвращает
			"a"	=> array(
				"exec"		=> array("ContestProcessor", "AddMember"),
				"TplVars"		=> array("lastName" => 2, "firstName" => 2, "cityId" => 2, "carId" => 2, "email" => 2, "gender" => 2, "age" => 2, "comment" => 2)
			)
		);
	}
}