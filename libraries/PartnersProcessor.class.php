<?php
define("PARTNERS_SERVICE_ID",												4);

class PartnersProcessor extends Processor
{
	/**
	 *
	 * @var PartnersProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var PartnersData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return PartnersProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new PartnersData();
	}

	/**
	 *
	 * Создает партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param integer $UsersDataType - Тип пользовательских данных (тип анкеты пользователя)
	 * @param string $Name - Наименование
	 * @param string $OfficialName - Официальное наименование
	 * @param string $Address - Адрес
	 * @param string $Email - Адрес электронной почты
	 */
	public function CreatePartner($PartnerType, $Parent, $Name, $Site, $City, $Address, $Latitude, $Longitude, $Phone, $RCode, $Children, $MapImage)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_PARTNER_MANAGEMENT);
		$Name = htmlentities($Name);
	
		$Partner = $this->CData->CreatePartner(WS::Init()->GetBrandId(), $PartnerType, $Parent, $Name, $Site, $City, $Address, $Latitude, $Longitude, $Phone, $RCode, $MapImage);
		if($Children)
			$this->SaveChildrenPartners($Partner, $Children);
		return $this->CData->GetPartnerNew($Partner);
	}

	/**
	 *
	 * Сохраняет изменения партнера
	 * @param integer $Partner - Идентификатор партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param integer $UsersDataType - Тип пользовательских данных (тип анкеты пользователя)
	 * @param string $Name - Наименование
	 * @param string $OfficialName - Официальное наименование
	 * @param string $Address - Адрес
	 * @param string $Email - Адрес электронной почты
	 */
								//"partnerDivisionId" => 1, "partnerTypeId" => 1, "partnerId" => 0, "partnerDivisionName" => 1, "partnerDivisionAddress" => 1, "gLn" => 0, "gLg" => 0, "partnerDivisionPhone" => 0, "partnerDivisionRCode" => 0, "childrenPartners" => 0
	public function SavePartner($Partner, $PartnerType, $Parent, $Name, $Site, $Address, $Latitude, $Longitude, $Phone, $RCode, $Children, $MapImage)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_PARTNER_MANAGEMENT);
		$Name = htmlentities($Name);
		
$this->Dump(__METHOD__.": ".__LINE__, $MapImage);
		if($MapImage)
			$MapImage = URL_PARTNERS_MAP_IMAGE.$MapImage[0];
		
$this->Dump(__METHOD__.": ".__LINE__, $MapImage);

		$this->CData->SavePartner($Partner, $PartnerType, $Parent, $Name, $Site, $Address, $Latitude, $Longitude, $Phone, $RCode, $MapImage);
		if($Children)
			$this->SaveChildrenPartners($Partner, $Children);
		return $this->CData->GetPartnerNew($Partner);
	}

	/**
	 *
	 * Возвращает список партнеров
	 */
	public function GetPartnersList($PartnerType = null, $Region = null, $FindText = null)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_PARTNER_READY);
		return $this->CData->GetPartnersList($PartnerType,$Region, $FindText);
	}

	public function GetPartnersListF($PartnerType = null, $Region = null, $FindText = null)
	{
		//PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_PARTNER_READY);

		$R = $this->CData->GetPartnersListF($PartnerType, $Region, $FindText);
//$this->Dump(__METHOD__."   LINE = ".__LINE__, "PartnerType = ", $PartnerType, $R);
		return $R;
	}
	
	public function GetPartnersListFullNEW($PartnerType = null, $Region = null)
	{
		return $this->CData->GetPartnersListFullNEW($PartnerType, $Region);
	}


	/**
	 *
	 * Возвращает информацию об одном партнере
	 * @param integer $Partner
	 */
	public function GetPartner($Partner)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_PARTNER_READY);
		return $this->CData->GetPartner($Partner);
	}

	/**
	 *
	 * Возвращает информацию об одном партнере
	 * @param integer $Partner
	 */
	public function GetPartnerF($Partner)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_PARTNER_READY);
		return $this->CData->GetPartnerF($Partner);
	}

	/**
	 *
	 * Создает тип партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function CreatePartnerType($Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_MANAGEMENT);
		$R = $this->CData->CreatePartnerType($Name, $Description);
		return $R;
	}

	/**
	 *
	 * Сохраняет изменения типа партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function SavePartnerType($PartnerType, $Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_MANAGEMENT);
		$this->CData->SavePartnerType($PartnerType, $Name, $Description);
	}

	/**
	 *
	 * Возвращает список типов партнеров
	 */
	public function GetPartnersTypeList()
	{
		/*
		PermissionsProcessor::Init()->CheckAccess(array(
													PERMISSIONS_PARTNERS_TYPE_READY,
													PERMISSIONS_PARTNERS_READY,
													PERMISSIONS_PARTNERS_CTREATE,
													PERMISSIONS_PARTNERS_SAVE
												));
		*/
		return $this->CData->GetPartnersTypeList();
	}



    public function GetScope()
    {
        $R = false;
        switch(User::Init()->GetType())
        {
            case ACCOUNT_TYPE_DEVELOPERS:
            case ACCOUNT_TYPE_ADMINS:
                $R = -10;
                break;
            case ACCOUNT_TYPE_TMR:
                $R = $this->CData->GetPartnersHierarchy(null, array(PARTNERS_TYPE_DEVELOPMENT));
                $R = $this->PrepareArray($R);
                break;
            case ACCOUNT_TYPE_DEALER:
            case ACCOUNT_TYPE_PARTNER:
                $R = User::Init()->GetPartnerId();
                break;
            case ACCOUNT_TYPE_GENERAL_DEALER:
                $R = $this->PrepareArray($this->CData->GetPartnersHierarchy(array(PARTNERS_TYPE_GENERAL_DEALER)));
                break;
            case ACCOUNT_TYPE_GENERAL_PARTNER:
                $R = $this->PrepareArray($this->CData->GetPartnersHierarchy(array(PARTNERS_TYPE_GENERAL_AGENCY)));
                break;
        }
        return $R;
    }



    public function CheckPartnersHierarchy($CheckedPartner, $AsExeption = null)
    {
        $R = false;
        switch(User::Init()->GetType())
        {
            case ACCOUNT_TYPE_DEVELOPERS:
            case ACCOUNT_TYPE_ADMINS:
                $R = $CheckedPartner;
                break;
            case ACCOUNT_TYPE_TMR:
                $R = $this->PrepareArray($this->CData->CheckPartnersHierarchy($CheckedPartner, null, array(ACCOUNT_TYPE_DEVELOPERS)));
            case ACCOUNT_TYPE_DEALER:
            case ACCOUNT_TYPE_PARTNER:
                if(is_array($CheckedPartner))
                    $CheckedPartner = $CheckedPartner[0];
                if($CheckedPartner == User::Init()->GetPartnerId())
                    $R = $CheckedPartner;
                break;
            case ACCOUNT_TYPE_GENERAL_DEALER:
                $R = $this->PrepareArray($this->CData->CheckPartnersHierarchy($CheckedPartner, array(PARTNERS_TYPE_GENERAL_DEALER)));
                break;
            case ACCOUNT_TYPE_GENERAL_PARTNER:
                $R = $this->PrepareArray($this->CData->CheckPartnersHierarchy($CheckedPartner, array(PARTNERS_TYPE_GENERAL_AGENCY)));
                break;
        }
        if(!$R && $AsExeption)
            throw new dmtException("Scope error", 77000);
        return $R;
    }




    public function GetChildren()
    {
        return $this->CData->GetChildren();
    }

    public function SaveChildren($User, $Partners)
    {
        $this->CData->SaveChildren($User, $Partners);
    }


     public function SaveChildrenRoles($User, $Partners, $Roles)
     {
         return $this->CData->SaveChildrenRoles($User, $Partners, $Roles);
     }

     public function GetChildrenRoles()
     {
         return $this->CData->GetChildrenRoles();
     }

     public function GetSlavePartnersTypes()
     {
         return $this->CData->GetSlavePartnersTypes();
     }

     public function GetHierarchy($Parent = null)
     {
		 PermissionsProcessor::Init()->GetScopeAccess(array(
										PERMISSIONS_PARTNERS_PARTNER_READY,
										PERMISSIONS_PARTNERS_PARTNER_MANAGEMENT,
										PERMISSIONS_PARTNERS_READY,
										PERMISSIONS_PARTNERS_CTREATE,
										PERMISSIONS_PARTNERS_SAVE,
										PERMISSIONS_PARTNERS_DELETE,
										PERMISSIONS_USERS_VIEW,
										PERMISSIONS_USERS_EDIT,
										PERMISSIONS_USERS_CREATE,
										PERMISSIONS_USERS_DELETE
									), $Parent);
         return $this->CData->GetHierarchy($Parent);
     }




     public function GetAccessiblePartners()
     {
         return $this->CData->GetAccessiblePartners();
     }

     public function GetAccessiblePartnersParents()
     {
         return $this->CData->GetAccessiblePartnersParents();
     }
	 
	 
	public function GetPartnersCSV($PartnerType = null, $Region = null, $FindText = null)
    {
$this->Dump(__METHOD__, "**************");
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_PARTNERS_PARTNER_MANAGEMENT);
        $R = $this->CData->GetPartnersCSV(WS::Init()->GetBrandId(), $Scope, $PartnerType, $Region, $FindText);
        array_unshift(  $R,
                        array(
							"Идентификатор",
                            "Название",
                            "Тип",
                            "Статус",
                            "R-код",
                            "Сайт",
                            "Широта",
                            "Долгота",
                            "Адрес",
                            "Телефон",
                            "Город",
                            "Область (регион)",
                            "Федеральный округ"
                        ));
        return $R;
    }
	
	public function SaveChildrenPartners($Partner, $Children)
	{
		$this->CData->SaveChildrenPartners($Partner, $Children);
	}
	
	public function GetNewParentId($Partner)
	{
		return $this->CData->GetNewParentId($Partner);
	}
}