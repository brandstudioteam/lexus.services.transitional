<?php
class CarOrdersStateProcessor extends Processor
{
	/**
	 *
	 * @var CarOrdersStateProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var CarOrdersStateData
	 */
	protected $CData;

    /**
	 *
	 * Инициализирует класс
	 *
	 * @return CarOrdersStateProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new CarOrdersStateData();
	}

    public function CheckState($Order, $Password)
    {
        return $this->CData->CheckState($Order, md5(sha1(mb_strtolower($Password))));
    }
}