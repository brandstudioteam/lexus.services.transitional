<?php
//TODO: Доработать класс
class dmtErrException extends Exception 
{
    public function __construct($Message, $Code, $File, $Line)
    {
    	switch($Code)
	    {
			case E_NOTICE:
				$Message = "NOTICE: ".$Message;
				break;
			case E_USER_NOTICE:
				$sev = "NOTICE: ".$Message;
				break;
			case E_WARNING:
				$sev = "WARNING: ".$Message;
				break;
			case E_USER_WARNING:
				$sev = "WARNING: ".$Message;
				break;
			case E_USER_ERROR:
				$sev = "FATAL: ".$Message;
				break;
			case E_STRICT:
				$sev = "STRICT: ".$Message;
				break;
			default:
		}
		$Message = "ERROR! ".$Message;
		parent::__construct($Message, $Code);
		if($File) $this->SetDebugInfo($Message, $Code, $File, $Line);
        else $this->SetDebugInfo();
    }
    
    protected function SetDebugInfo($Message, $Code, $File, $Line)
    {
   		$Tr = debug_backtrace();
   		unset($Tr[0]);
   		$R = GetDebug();
//print_r(debug_backtrace());
    	$R->SaveException($Message, $Code, $File, $Line, $Tr, null);
    }
}
?>