<?php
final class Debug
{
	/**
	 *
	 * @var Debug
	 */
	private static $Inst;
	private static $StartTm;

	private $Mode = false;
	private $Enabled = false;
	private $FileMode;
	private $FileName;

	private $Info = array();
	private $DBDebuger;
	private $ExtTrc;
	private $ExtExc;
	private $File;
	private $Timers = array();

	private function __construct($Enabled, $Mode, $FileMode)
	{
		$this->Enabled = $Enabled === null ? SYSTEM_ON_DEBUG : (boolean) $Enabled;
		$this->Mode = $Mode === null ? DEBUG_MODE : ($Mode == DEBUG_REPORT_BOTH ? DEBUG_REPORT_BOTH : ($Mode == DEBUG_REPORT_JSCONSOLE ? DEBUG_REPORT_JSCONSOLE : DEBUG_REPORT_FILE));
		$this->FileMode = $FileMode === null ? DEBUG_FILE_MODE : ($FileMode == DEBUG_FILE_MODE_SESSION ? DEBUG_FILE_MODE_SESSION : DEBUG_FILE_MODE_SITE);

		$this->FileName = ($this->FileMode == DEBUG_FILE_MODE_SESSION) ? DEBUG_FILES_PATH.session_id() : DEBUG_FILE;
	}

	/**
	 *
	 * Инициализирует класс
	 * 
	 * @return Debug
	 */
	public static function Init($Enabled = null, $Mode = null, $FileMode = null)
	{
		if(!self::$Inst)
		{
			self::$StartTm = (float) microtime(true);
			$C = __CLASS__;
			self::$Inst = new $C($Enabled, $Mode, $FileMode);
		}
		return self::$Inst;
	}

	private function CheckFile()
	{
		if($this->Mode == DEBUG_REPORT_FILE || $this->Mode == DEBUG_REPORT_BOTH)
		{
			if(!$this->File)
			{
				//$this->FileName = ($this->FileMode == DEBUG_FILE_MODE_SESSION) ? DEBUG_FILES_PATH.session_id() : DEBUG_FILE;
				if(file_exists($this->FileName) && filesize($this->FileName) > DEBUG_FILE_SIZE_MAX)
				{
					unlink($this->FileName);
				}
				$this->File = fopen($this->FileName, file_exists($this->FileName) ? "a" : "w");
			}
		}
	}

	private function Check()
	{
		if(!self::$Inst) throw new Exception("Class '".__CLASS__."' is not inicialized");
		return !$this->Enabled;
	}

	/**
	 *
	 * Производит трассировку вызова
	 */
	public function Trace()
	{
		if($this->Check()) return;
		if(!$this->Enabled) return;
		$tr = debug_backtrace();
		unset($tr[0]);

		if(DEBUG_EXTERNAL_FORMATER_TRACE)
		{
			if(!is_object($this->ExtTrc))
			{
				$C = DEBUG_EXTERNAL_FORMATER_TRACE;
				$this->ExtTrc = new $C();
			}
			$R = $this->ExtTrc->Format($tr);
		}
		else $R = $this->FormatTrace($tr);
		$this->Debug($R);
	}

	/**
	 *
	 * Устанавливает начальную метку времени исполнения (таймер)
	 *
	 * @param string $Name - имя таймера
	 * @param string $Comment - комментарий к таймеру
	 */
	public function SetTimer($Name, $Comment = null, $Params = null, $Fixed = false)
	{
		if($Fixed && isset($this->Timers[$Name]))
			$this->FixesTimer($Name);
		$this->Timers[$Name] = array();
		$this->Timers[$Name]["s"] = (float) microtime(true);
		$this->Timers[$Name]["c"]  = $Comment;
	}

	/**
	 *
	 * Фиксирует время исполнения
	 *
	 * @param string $Name - имя таймера
	 */
	public function FixesTimer($Name)
	{
		if(!isset($this->Timers[$Name]))
			$this->Debug("Остановка таймера исполнения с именем \"".$Name."\" без его инициализации");
		else $this->Debug(strval($this->Timers[$Name]["c"])."\r\n".((float) microtime(true) - $this->Timers[$Name]["s"]). "c");
	}

	/**
	 *
	 * Сохраняет информацию об исключении
	 * @param string $Message
	 * @param int $Code
	 * @param int $Line
	 * @param array $Trace
	 * @param array $DBInfo
	 */
	public function SaveException($Message, $Code, $File, $Line, $Trace, $DBInfo = null)
	{
		$t = debug_backtrace();
		if($t[1]["object"] instanceof dmtException || $t[1]["object"] instanceof dmtErrException)
		{
			if(DEBUG_EXTERNAL_FORMATER_EXCEPTION)
			{
				if(!is_object($this->ExtExc))
				{
					$C = DEBUG_EXTERNAL_FORMATER_EXCEPTION;
					$this->ExtExc = new $C();
				}
				$R = $this->ExtExc->Format($Message, $Code, $File, $Line, $Trace, $DBInfo);
			}
			else $R = $this->VD($Trace);
			$R = "";
			$this->Debug("Выброшено исключение ".(is_numeric($Code) ? "с кодом ".$Code." " : "")." и сообщением: \"".$Message."\". Файл \"".$File."\", строка ".$Line."\nТрассировка:\n".$R);
		}
		else throw new dmtException("This method (\"".__METHOD__."\") can only be called from the classes \"dmtException\" or \"dmtErrException\"");
	}

	private function FormatTrace($Trace)
	{
		$R = $this->VD($Trace); //TODO: Дописать форматированиt информации о трассировке
		return $R;
	}

	private function FormatExc($Trace)
	{
		$R = $this->VD($Trace); //TODO: Дописать форматированиt информации об исключении
		return $R;
	}

	/**
	 *
	 * Сохраняет дамп переданных переменных.
	 * Dump(mixed expression [, mixed expression [, ...]]])
	 * @param mixed $Var - переменная, дамп которой нужно сохранить
	 */
	public function Dump($Var)
	{
		if($this->Check()) return;
		if(!$this->Enabled) return;
		ob_start();
			call_user_func_array("var_dump", func_get_args());
		$Var = ob_get_contents();
		ob_end_clean();
		$this->Save($Var);
	}

	/**
	 *
	 * Сохраняет значения суперглобальных массивов
	 */
	public function SetContext()
	{
		$this->Dump("------------------------ ".date("d.m.Y H:i:s"));
		$this->Dump("SERVER", $_SERVER, "COOCKIES", $_COOKIE, "POST", $_POST, "GET", $_GET);
		/*
		$this->Info[] = "SERVER = \n".$this->VD($_SERVER);
		if(isset($_COOKIE)) $this->Info[] = "COOCKIES = \n".$this->VD($_COOKIE);
		if(isset($_POST)) $this->Info[] = "POST = \n".$this->VD($_POST);
		if(isset($_GET)) $this->Info[] = "GET = \n".$this->VD($_GET);
		if(isset($_FILES)) $this->Info[] = "FILES = \n".$this->VD($_FILES);
		 */
	}

	/**
	 * 
	 * Ведет протоколирование
	 * @param string $Text текст, для протоколирования
	 */
	public function Debug($Text)
	{
		if($this->Check()) return;
		if(!$this->Enabled) return;
		$R = (is_string($Text) || is_numeric($Text)) ? $Text : $this->VD($Text, false);
		$this->Save($R);
	}

	private function Save($Value)
	{
		if(DEBUG_MODE == DEBUG_REPORT_FILE || DEBUG_MODE == DEBUG_REPORT_BOTH)
		{
			$this->CheckFile();
			foreach(func_get_args() as $a)
				fwrite($this->File, var_export($a, true)."\r\n");
		}
		/*
		if(DEBUG_MODE == DEBUG_REPORT_JSCONSOLE || DEBUG_MODE == DEBUG_REPORT_BOTH)
			$this->Info[] = $Value;
		*/
	}

	private function VD($Var, $IsDump = true)
	{
		ob_start();
			if($IsDump) var_dump($Var);
			else print_r($Var);
			$Var = ob_get_contents();
		ob_end_clean();
		return $Var;
	}

	/**
	 *
	 * Возвращает текст протокола
	 * @param bool $ForHTML - флаг, означающий необходимость перевода спецсимволов в html-сущности
	 * @return srting Строка текста протокола
	 */
	public function GetInfo()
	{
		if($this->Check()) return;
		if(sizeof($this->Info) == 0) $R = "";
		else
		{
			$R = array();
			foreach ($this->Info as $v)
				$R[] = htmlentities(mb_str_replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;", nl2br($v)));
			$R = "<p>".implode("<br /><br />", $R)."</p>";
		}
		$R = "<p>Начало работы скрипта: ".date("d.m.y H:i:s", self::$StartTm)."<br>Время работы скрипта: ".((microtime(true) - self::$StartTm))." с.</p>";
		return $R;
	}

	public function SetDebugParams($Params)
	{
		if(!is_array($Params) || !sizeof($Params)) return;
		if(array_key_exists("enabled", $Params) && $this->Enabled != $Params["enabled"])
		{
			if($this->Enabled)
				$this->CloseFile();
			$this->Enabled = (boolean) $Params["enabled"];
		}
		if(array_key_exists("mode", $Params) && $this->Mode != $Params["mode"])
		{
			if($Params["mode"] == DEBUG_REPORT_JSCONSOLE && ($this->Mode == DEBUG_REPORT_FILE || $this->Mode == DEBUG_REPORT_BOTH))
				$this->CloseFile();
			$this->Mode = $Params["mode"] == DEBUG_REPORT_FILE ? DEBUG_REPORT_FILE : ($Params["mode"] == DEBUG_REPORT_JSCONSOLE ? DEBUG_REPORT_JSCONSOLE : DEBUG_REPORT_BOTH);
		}
		if(array_key_exists("fileMode", $Params) && $this->FileMode != $Params["fileMode"])
		{
			$this->CloseFile();
			$this->FileMode = $Params["fileMode"] == DEBUG_FILE_MODE_SESSION ? DEBUG_FILE_MODE_SESSION : DEBUG_FILE_MODE_SITE;
		}
		if(array_key_exists("file", $Params) && $this->FileName != $Params["file"])
		{
			$this->CloseFile();
			$this->FileName = $Params["file"];
		}
		else $this->FileName = ($this->FileMode == DEBUG_FILE_MODE_SESSION) ? DEBUG_FILES_PATH.session_id() : DEBUG_FILE;
	}

	private function CloseFile()
	{
		if($this->File)
		{
			fclose($this->File);
			$this->File = null;
		}
	}
}