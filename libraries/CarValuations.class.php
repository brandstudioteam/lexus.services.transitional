<?php
class CarValuations extends Controller
{
	/**
	 *
	 * @var CarValuations
	 */
 	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return CarValuations
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				"cityId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"partnerId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "partnersDivisionId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "modelId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "tradeInModelId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"memberName"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FIO)
				),
                "memberId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"phone"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE)
				),
				"email"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE)
				),
				"manufacturers"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				//Долгота
				"car"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				//Широта
				"mileage"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_MILEAGE)
				),
				//Широта
				"manufactured"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_YEAR_B)
				),
				//Широта
				"description"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT)
				),
				//Широта
				"photo"					=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				//Широта
				"photo2"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				//Широта
				"photo3"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				//Широта
				"photo4"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				//Широта
				"photo5"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				"isMobile"			=>  array(
					"filter"		=> array(FILTER_TYPE_VALUES, array(0, 1)),
					"default"		=> 0
				),
                "isOwner"			=>  array(
					"filter"		=> array(FILTER_TYPE_VALUES, array(0, 1)),
					"default"		=> 0
				),
				"tradeInModel"		=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"captcha"			=> array(
					"filter"		=> array(FILTER_TYPE_VALUES, array(Captcha::GetCCode()))
				),
                "code"              => array(
                    "filter"          => array(FILTER_TYPE_VALUES, array("__0_5_w79-10-Gn-t801__"))
                ),
                "type"		=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "cost"		=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"costMax"		=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
                "c"		=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT_B64_ONLY)
				),
				"photo"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("gif", "jpeg", "jpg", "png")
						),
    					"name"			=> md5(microtime(true)),
						"savepath"		=> PATH_CAR_VALAUATION_UPLOAD,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
                "registred"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerStart"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerEnd"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
			)
		);

		$this->Modes = array(
			//Отправить заявку на оценку
			"a"	=> array(
				"exec"			=> array("CarValuationsProcessor", "Send"),
				"getFiles"		=> "photo",
				"TplVars"		=> array("partnerId" => 1, "memberName" => 1, "phone" => 1, "email" => 1, "manufacturers" => 1, "car" => 1, "mileage" =>  1, "manufactured" =>  1, "description" => 0, "tradeInModel" => 0, "isMobile" => 0, "isOwner" => 0, "type" => 0, "c" => 0)//, "captcha" => 1
			),
			"b"	=> array(
				"TplVars"		=> array("photo" => 1)
			),
            "c" => array(
                "exec"			=> array("CarValuationsProcessor", "ReSend"),
                "TplVars"		=> array("code" => 1)
            ),
            "d" => array(
                "exec"			=> array("CarValuationsProcessor", "TestSOAP"),
            ),
            "e" => array(
                "exec"			=> array("CarValuationsProcessor", "GetMembers"),
                "TplVars"		=> array("partnersDivisionId" => 0)
            ),
            "f" => array(
                "exec"			=> array("CarValuationsProcessor", "GetMembersCSV"),
                "TplVars"		=> array("partnersDivisionId" => 0, "modelId" => 0, "registred" => 0, "registerEnd" => 0, "tradeInModelId" => 0)
            ),
            "g" => array(
                "exec"			=> array("CarValuationsProcessor", "AddCost"),
                "TplVars"		=> array("memberId" => 2, "cost" => 2, "costMax" => 2)
            ),
			"h" => array(
                "exec"			=> array("CarValuationsProcessor", "SendMailWitchCost"),
                "TplVars"		=> array("memberId" => 2)
            ),
			"i" => array(
                "exec"			=> array("CarValuationsProcessor", "GetAllMembersCSV")
            ),
        );
	}
}