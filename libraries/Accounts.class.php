<?php
class Accounts extends Controller
{
	protected static $Inst;

	/**
	 *
	 * Класс данных
	 * @var AccountsProcessor
	 */
	private $Process;

	/**
	 *
	 * Инициализирует класс
	 * @return Accounts
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				//Логин
				"lg"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
					"limit"		=> array(
						"max"		=> array("char", 30),
						"min"		=> array("char", 4)
					)
				),
				//Пароль
				"pw"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
					"limit"		=> array(
						"max"		=> array("char", 32),
						"min"		=> array("char", 6)
					)
				),
				"_o_s"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_MD5),
				),
				"sessionId"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_CHARANDDIGITAL),
				),
				"token"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_SHA1),
				),
				"domain"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_URL),
				),
				"url"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_URL),
				),
				//Логин
				"userLogin"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				/*
					"limit"		=> array(
						"max"		=> array("char", 30),
						"min"		=> array("char", 5)
					),
					*/
					//"verifier"	=> array("AccountsProcessor", "IsLoginNotExists")
				),
				//Логин
				"userLogin2"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Пароль
				"userPassword"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Имя
				"userFirstName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Отчество
				"userMiddleName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Фамилия
				"userLastName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Идентификатор
				"userId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				//Идентификатор партнера
				"partnerDivisionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				//Электронная почта
				"userEmail"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_MULTI),
					"limit"		=> array(
						"max"		=> array("char", 255),
						"min"		=> array("char", 7)
					),
					//"verifier"	=> array("AccountsProcessor", "IsEmailNotExists")
				),
				//Электронная почта
				"userEmail2"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_MULTI),
					"limit"		=> array(
						"max"		=> array("char", 255),
						"min"		=> array("char", 7)
					),
				),
				//Тип пользователя
				"userTypeId"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"roleId"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerDivisionId"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),

                "usersFile"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("csv")
						),
    					"name"			=> md5(microtime(true)),
						"savepath"		=> PATH_ROOT."debug/",
						"autoreply"		=> 1
					),
					"required"		=> true
				),
                "children"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "childrenRoles"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"userPosition"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_USER_POSITION),
				),
				"userAdvPhone"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ADVANCED),
				),
				"userPhone"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
				),
				/*
				//Капча
				"cpi"	=> array(
					"filter" => array(FILTER_TYPE_VALUES, "exec" => array("Captcha", "GetCode"))
				)
				*/
			)
		);

		$this->Modes = array(
			//Создание учетной записи
			"a"	=> array(
				"exec"			=> array("AccountsProcessor", "CreateAccount"),
									//$Type, $Email, $FName, $MName, $LName, $Login, $Partner, $Password = null, $Role = null
				"TplVars"		=> array("userTypeId" => 2, "userFirstName" => 2, "userMiddleName" => 0, "userLastName" => 0, "partnerDivisionId" => 2, "roleId" => 0, "children" => 0, "childrenRoles" => 0),
                "Variables"  => array(
                    "userLogin"		=> array(
                        "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
                        "limit"		=> array(
                            "max"		=> array("char", 30),
                            "min"		=> array("char", 4)
                        ),
                        "verifier"	=> array("AccountsProcessor", "IsLoginNotExists"),
                        "required"  => 2
                    ),
                    //Электронная почта
                    "userEmail"	=> array(
                        "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_MULTI),
                        "limit"		=> array(
                            "max"		=> array("char", 255),
                            "min"		=> array("char", 7)
                        ),
                        "verifier"	=> array("AccountsProcessor", "IsEmailNotExists")
                    ),
                ),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					),
				),
			),
            "b"	=> array(
				"exec"			=> array("AccountsProcessor", "DeleteAccount"),
				"TplVars"		=> array("userId" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
			//Смена пароля
			"d"	=> array(
				"exec"			=> array("AccountsProcessor", "ChangePassword"),
				"TplVars"		=> array("pw" => 1, "cpw" => 1, "cpi" => 0)
			),
			//Получить список пользователей
			"e"	=> array(
				"exec"			=> array("AccountsProcessor", "GetUsersList"),
				"TplVars"		=> array("userTypeId" => 0)
			),
			//Получить список пользователей с дополнительной информацией
			"f"	=> array(
				"exec"			=> array("AccountsProcessor", "GetUsersListF"),
				"TplVars"		=> array("userTypeId" => 0)
			),
			//Получить информацию о единичном пользователе
			"g"	=> array(
				"exec"			=> array("AccountsProcessor", "GetUser"),
				"TplVars"		=> array("userId" => 1)
			),

			//Сохранение изменений учетной записи
			"j"	=> array(
				"exec"			=> array("AccountsProcessor", "Save"),
									//  $Account,       $Type = null,       $FName = null,          $MName = null,      $LName = null,      $Partner = null,        $Role = null,    $Children = null, $ChildrenRoles = null, $Login = null, $Email = null
				"TplVars"		=> array("userId" => 1, "userTypeId" => 0, "userFirstName" => 0, "userMiddleName" => 0, "userLastName" => 0, "partnerDivisionId" => 0, "roleId" => 0, "children" => 0, "childrenRoles" => 0),
//TODO: реализовать проверку логини и электронной почты при смене
                "Variables"  => array(
                    "userLogin"		=> array(
                        "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
                        "limit"		=> array(
                            "max"		=> array("char", 30),
                            "min"		=> array("char", 4)
                        ),
                        "verifier"	=> array("AccountsProcessor", "CheckLoginForUpdate", "userId"),
                        "required"  => 2
                    ),
                    //Электронная почта
                    "userEmail"	=> array(
                        "filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_MULTI),
                        "limit"		=> array(
                            "max"		=> array("char", 255),
                            "min"		=> array("char", 7)
                        ),
                        "verifier"	=> array("AccountsProcessor", "CheckEmailForUpdate", "userId")
                    )
                ),
				"Results"		=> array(
					"exceptions"		=>  array(1, array(
							11 => "AddedDealersIsNotChild"
						)
					),
				),
			),

			//Удаляет роль пользователя
			"i"	=> array(
				"exec"			=> array("AccountsProcessor", "RoleDelete"),
									//
				"TplVars"		=> array("userId" => 1, "roleId" => 1),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					),
					"succes"			=> array(7, "RegisterSucTxt")
				)
			),
			//Добавляет роль пользователю
			"l"	=> array(
				"exec"			=> array("AccountsProcessor", "RoleAdd"),
									//
				"TplVars"		=> array("userId" => 1, "roleId" => 1),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					),
					"succes"			=> array(7, "RegisterSucTxt")
				)
			),
			//Возвращает список ролей указанного пользователя
			"m"	=> array(
				"exec"			=> array("AccountsProcessor", "GetRoles"),
									//
				"TplVars"		=> array("userId" => 0),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					),
				)
			),
            "n"	=> array(
				"exec"			=> array("AccountsProcessor", "GetCurrentData")
			),
			"o"	=> array(
				"exec"			=> array("AccountsProcessor", "SaveFIO"),
				"TplVars"		=> array("userFirstName" => 0, "userMiddleName" => 0, "userLastName" => 0, "userPosition" => 0, "userPhone" => 0, "userAdvPhone" => 0),
			),

			"p"		=> array(
				"exec"			=> array("AccountsProcessor", "GetUsersChildrenParents"),
				//"TplVars"		=> array("userFirstName" => 0, "userMiddleName" => 0, "userLastName" => 0),
			),
			"q"		=> array(
				"exec"			=> array("AccountsProcessor", "GetUsersChildrenRoles"),
				//"TplVars"		=> array("userFirstName" => 0, "userMiddleName" => 0, "userLastName" => 0),
			),


            "z"	=> array(
				"exec"			=> array("AccountsProcessor", "CreateAccountFromEmail"),//"CreateAccountsFromFile"
                "getFiles"		=> "usersFile",
                //"TplVars"		=> array("usersFile" => 0)
			),
            "z0"	=> array(
				"TplVars"		=> array("usersFile" => 2)
			),
		);
	}
}