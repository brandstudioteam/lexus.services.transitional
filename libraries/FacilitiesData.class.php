<?php
class FacilitiesData extends Data
{
	/**
	 *
	 * Возвращает список возможностей
	 */
	public function GetFacilities($FiltersOnly = null, $FilterType = null, $Dealer = null)
	{
		$Query = "SELECT
	a.`facility_id` AS facilityId,
	a.`name` AS facilityName,
	a.`description` AS facilityDescription,
	a.`icon` AS facilityIcon,
	a.`use_as_filter` AS facilityAsFilter,
	a.`alias` AS facilityAlias,
	a.`advanced_info_ref` AS facilityAdvInfo
FROM `".DBS_REFERENCES."`.`facilities` AS a";

		$Where = array();
		if($Dealer)
		{
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS b ON b.`facility_id`=a.`facility_id`";
			$Where[] = "b.`partners_division_id`=".$Dealer;
		}
		if($FilterType)
		{
			$Where[] = "a.`use_as_filter`".$this->PrepareValue($FilterType);
		}
		elseif($FiltersOnly)
		{
			$Where[] = "a.`use_as_filter`=1";
		}
		return $this->Get($Query.$this->PrepareWhere($Where));
	}
	
	/**
	 *
	 * Возвращает список возможностей
	 */
	public function GetFacilitiesNew($Brand, $Category = null, $FilterType = null, $Dealer = null)
	{
		$Query = "SELECT
	a.`facility_id` AS facilityId,
	a.`name` AS facilityName,
	a.`description` AS facilityDescription,
	a.`icon` AS facilityIcon,
	a.`facilities_type_id` AS facilityAsFilter,
	a.`alias` AS facilityAlias,
	a.`advanced_info_ref` AS facilityAdvInfo
FROM `".DBS_UNIVERSAL_REFERENCES."`.`facilities` AS a
LEFT JOIN `references`.`facilities_brands` AS a1 ON a1.`facility_id`=a.`facility_id`";

		$Where = array();
		$Where[] = "a1.`brand_id`=".$Brand;
		if($Dealer)
		{
			$Query .= "
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities` AS b ON b.`facility_id`=a.`facility_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS c ON c.`partners_division_id`=b.`partners_division_id`";
			$Where[] = "c.`partners_division_id`=".$Dealer;
			$Where[] = "c.`brand_id`=".WS::Init()->GetBrandId();
		}
		if($FilterType)
		{
			$Where[] = "a.`facilities_type_id`".$this->PrepareValue($FilterType);
		}
		/*
		elseif($FiltersOnly)
		{
			$Where[] = "a.`facilities_type_id`=1";
		}
		 */
		return $this->Get($Query.$this->PrepareWhere($Where));
	}


	/**
	 *
	 * Создает возможность
	 * @param string $Name - Наименование возможности
	 * @param string $Desc - Необязательный. Описание возможности
	 * @param string $Icon - Необязательный. Путь к миниатюре
	 */
	public function AddFacility($Name, $AsFilter, $Desc = null, $Icon = null, $AdvancedInfo = null)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_FACILITIES_CREATE, User::Init()->GetAccount());

		$this->Exec("INSERT INTO `".DBS_REFERENCES."`.`facilities`
	(`name`,
	`description`,
	`icon`)
VALUES
	(".$this->Esc($Name, true, false, true).",
	".$this->Esc($Desc).",
	".$this->Esc($Icon).");");

		return $this->DB->GetLastID();
	}

	/**
	 *
	 * Сохраняет изменения возможности
	 * @param integer $Facility - Идентификатор возможности
	 * @param string $Name - Наименование возможности
	 * @param string $Desc - Необязательный. Описание возможности
	 * @param string $Icon - Необязательный. Путь к миниатюре
	 */
	public function SaveFacility($Facility, $Name = null, $AsFilter = null, $Desc = null, $Icon = null, $AdvancedInfo = null)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_FACILITIES_EDIT, User::Init()->GetAccount());

		$A = array();

		if($Name)
			$A[] = "`name`=".$this->Esc($Name);
		if($Desc)
			$A[] = "`description`=".$this->Esc($Desc);
		if($Icon)
			$A[] = "`icon`=".$this->Esc($Icon);
		if(sizeof($A))
			$this->Exec("UPDATE `".DBS_REFERENCES."`.`facilities`
SET ".implode(", ", $A)."
WHERE `facility_id`=".$Facility.";");
	}

	/**
	 *
	 * Удаляет возможность
	 * @param integer $Facility - Идентификатор возможности
	 */
	public function DeleteFacility($Facility)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_FACILITIES_DELETE, User::Init()->GetAccount());

		$this->Exec("DELETE FROM `".DBS_REFERENCES."`.`facilities`
WHERE `facility_id`=".$Facility.";");
	}


	public function GetDealersState($Facility)
	{
		return $this->Get("SELECT
    `partners_division_id` AS partnerDivisionId,
    `facility_id` AS facilityId,
    `status` AS facilityPartnerDivisionState
FROM `toyota_references`.`partners_divisions_facilities`
WHERE `facility_id`".$this->PrepareValue($Facility).";");
	}
	
	public function SaveDealersModelsAll($Brand, $AllStatus)
	{
		foreach($AllStatus as $v)
		{
			try
			{
				$this->Exec("INSERT INTO `references`.`facilities_partners_models`
	(`partners_division_id`,
	`brand_id`,
	`facility_id`,
	`model_id`,
	`status`)
VALUES
	(".$v["partnerDivisionId"].",
	".$Brand.",
	".$v["facilityId"].",
	".$v["modelId"].",
	".($v["partnerDivisionModelStatus"] ? 1 : 0).")
ON DUPLICATE KEY UPDATE
	`status`=VALUES(`status`);");
			}
			catch(dmtException $e)
			{

			}
		}
	}
	
	public function SaveDealersModelsCountAll($Brand, $AllModel)
	{
		foreach($AllModel as $v)
		{
			$this->Exec("UPDATE `references`.`facilities_partners_models`
SET `count`=".($v["partnerDivisionModelCount"] ? $v["partnerDivisionModelCount"] : 0)."
WHERE `partners_division_id`=".$v["partnerDivisionId"]."
	AND `brand_id`=".$Brand."
	AND `facility_id`".$v["facilityId"]."
	AND `model_id`=".$v["modelId"]."
	AND `status`=1;");
		}
	}

	public function GetDealersModelsAll($Brand, $Facility, $Status = null)
	{
		$Query = "SELECT
	`partners_division_id` AS partnerDivisionId,
	`facility_id` AS facilityId,
	`model_id` AS modelId,
	`status` AS partnerDivisionModelStatus,
	`count` AS partnerDivisionModelCount
FROM `references`.`facilities_partners_models`
WHERE `facility_id`=".$Facility."
	AND `brand_id`=".$Brand."
	AND `status`".$this->PrepareValue($Status).";";
		return $this->Get($Query);
	}
	
	public function GetDealersModelsCountAll($Brand, $Facility)
	{
		$Query = "SELECT
	`partners_division_id` AS partnerDivisionId,
	`facility_id` AS facilityId,
	`model_id` AS modelId,
	`status` AS partnerDivisionModelStatus,
	`count` AS partnerDivisionModelCount
FROM `references`.`facilities_partners_models`
WHERE `facility_id`=".$Facility."
	AND `brand_id`=".$Brand."
	AND `status`=1;";
		return $this->Get($Query);
	}

	public function SaveDealersFacilitiesAll($Brand, $AllStatus)
	{
		$t = $this->Get("SELECT
	a.`facility_id` AS facilityId,
    a.`old_id` AS oldId
FROM `references`.`facilities` AS a
LEFT JOIN `references`.`facilities_brands` AS b ON b.`facility_id`=a.`facility_id`
LEFT JOIN `lexus_references`.`facilities` AS c ON c.`facility_id`=a.`old_id`
WHERE b.`brand_id`=".$Brand."
	AND c.`facility_id` IS NOT NULL;");
		
		$FA = array();
		foreach($t as $v)
		{
			if($v["facilityId"] && $v["oldId"])
				$FA[$v["facilityId"]] = $v["oldId"];
		}
		
		$t = $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
    b.`partners_division_id` AS oldId
FROM `references`.`partners_divisions` AS a
LEFT JOIN `lexus_references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`old_id`
WHERE a.`brand_id`=".$Brand."
	AND a.`partners_type_id` IN (1, 6)
    AND b.`partners_division_id` IS NOT NULL;");
		
		$DA = array();
		foreach($t as $v)
		{
			if($v["partnerDivisionId"] && $v["oldId"])
				$DA[$v["partnerDivisionId"]] = $v["oldId"];
		}
		
		$A = array();
		$B = array();
		foreach($AllStatus as $v)
		{
			$A[] = "(".$v["partnerDivisionId"].",
	".$v["facilityId"].",
	".$v["partnerDivisionFacilityStatus"].")";
			if(isset($DA[$v["partnerDivisionId"]]) && isset($FA[$v["facilityId"]]))
				$B[] = "(".$DA[$v["partnerDivisionId"]].",
	".$FA[$v["facilityId"]].",
	".$v["partnerDivisionFacilityStatus"].")";
		}
		
		if(sizeof($A))
		{
			$this->Begin();
			try
			{
				$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities`
	(`partners_division_id`,
	`facility_id`,
	`status`)
VALUES
	".implode(",", $A)."
ON DUPLICATE KEY UPDATE
	`status`=VALUES(`status`);");
				if(sizeof($B))
				{
					$this->Exec("INSERT INTO `".DBS_REFERENCES."`.`partners_divisions_facilities`
	(`partners_division_id`,
	`facility_id`,
	`status`)
VALUES
	".implode(",", $B)."
ON DUPLICATE KEY UPDATE
	`status`=VALUES(`status`);");
				}
				$this->Commit();
			}
			catch(dmtException $e)
			{
				$this->Rollback();
				throw new dmtException($e->getMessage(), $e->getCode(), true);
			}
		}
	}
	
	public function GetRelatedFacilities($Brand)
	{
		return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
	a.`facility_id` AS facilityId,
	a.`status` AS partnerDivisionFacilityStatus
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities` AS a
LEFT JOIN `references`.`facilities_brands` AS b ON b.`facility_id`=a.`facility_id`
WHERE b.`brand_id`=".$Brand.";");
	}
	
	public function ImportDealersModels($Brand, $Data, $Facility)
	{
		$this->Begin();
		try
		{
			$this->Exec("UPDATE `references`.`facilities_partners_models`
SET `count`=0
WHERE `facility_id`=".$Facility."
	AND `brand_id`=".$Brand.";");
			$this->Exec("INSERT INTO `references`.`facilities_partners_models`
	(`partners_division_id`,
	`facility_id`,
	`model_id`,
	`count`)
VALUES ".implode(",", $Data)."
ON DUPLICATE KEY UPDATE
`status`=VALUES(`status`);");
			$this->Commit();
		}
		catch(dmtException $e)
		{
			$this->Rollback();
			throw new dmtException($e->getMessage(), $e->getCode(), true);
		}
	}
	
	public function GetFacilityName($Facility)
	{
		return $this->Count("SELECT 
	`name` AS Cnt
FROM `references`.`facilities`
WHERE `facility_id`=".$Facility.";");
	}
}