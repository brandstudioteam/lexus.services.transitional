<?php
class PermissionsTypes extends Controller
{
	/**
	 *
	 * @var PermissionsTypes
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return PermissionsTypes
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"permissionsTypeId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"permissionsTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"permissionsTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
			)
		);
		$this->Modes = array(
			//Возвращает
			"a"	=> array(
				"exec"		=> array("PermissionsTypesProcessor", "GetPermissionsTypesList")
			),
            //Создает
			"b"	=> array(
				"exec"		=> array("PermissionsTypesProcessor", "CreatePermissionsType"),
				"TplVars"		=> array("permissionsTypeName" => 1, "permissionsTypeDescription" => 0)
			),
             //Сохраняет
			"c"	=> array(
				"exec"		=> array("PermissionsTypesProcessor", "SavePermissionsType"),
				"TplVars"		=> array("pertnerTypeId" => 1, "permissionsTypeName" => 0, "permissionsTypeDescription" => 0)
			),
			//Удаляет
			"d"	=> array(
				"exec"		=> array("PermissionsTypesProcessor", "DeletePermissionsType"),
                "TplVars"		=> array("pertnerTypeId" => 1)
			)
		);
	}
}