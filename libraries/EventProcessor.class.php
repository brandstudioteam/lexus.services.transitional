<?php
class EventProcessor extends Processor
{
	/**
	 *
	 * @var EventProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var EventData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return EventProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new EventData();
	}

	/**
	 * Формирует событие для пользователей или клиентов пользователей
	 * @param integer $Service - Идентификатор сервиса
	 * @param integer $Type - Идентификатор типа события
	 * @param integer $Category - Категория события
	 * @param array $Detail - Детали события
	 * @param integer $Dealer - Идентификатор дилера, связанного с событием
	 * @param mixed $ExclUser - Идентификатор или массив идентификаторов пользователя, исключаемого из адресатов рассылки события
	 * @param string $ExclCUID - Идентификатор клиента, исключаемого из адресатов рассылки события
	 */
	public function CreateEvent($Service, $Type, $Category, $Detail, $Dealer, $ExclUser = null, $ExclCUID = null)
	{
		if($Category == EVENTS_CATEGORY_NOTICE_CONFIRMED)
		$this->CData->CreateEvent($Service, $Type, $Category, json_encode($Detail), $Dealer, $ExclUser, $ExclCUID);
	}
	
	/**
	 * Возвращает события для текущего пользователя и текущего клиента
	 * @param integer $Timestamp - Метка времени последнего запроса в секундах
	 * @return array
	 */
	public function GetEvents($Timestamp)
	{
		return array(
            "events"        => $this->CData->GetEvents($Timestamp),
            "timestamp"     => time()
        );
	}

	public function DeliveredEvent($Event)
	{
		$this->CData->DeliveredEvent($Event);
	}

	public function ConfirmedEvent($Event)
	{
		$this->CData->ConfirmedEvent($Event);
	}

	public function WorkedEvent($Event)
	{
		$this->CData->WorkedEvent($Event);
	}
	
	/**
	 * Удаляет события-уведомления с подтверждением прочтения для текущего пользователя
	 * @param integer $Event - Идентификатор события
	 */
	public function ClosedEvent($Event)
	{
		$this->CData->ClosedEvent($Event);
	}
	
	/**
	 * Рекурсивно проверяет соответствие полей деталей подписки и деталей события
	 * @param array $Data - Детали события
	 * @param array $Keys - Секция keys деталей подписки
	 * @param string $Type - Тип сравнения
	 * @param mixed $Value - Значения сравнения. Строковые, числовые или масив строковых или числовых значений
	 * @return boolean
	 */
	protected function CompareRecursive($Data, $Keys, $Type, $Value = null)
	{
		if($Keys)
		{
			foreach($Keys as $k => $v)
			{
				if(!array_key_exists($k, $Data))
				{
					$R = false;
					break;
				}
				if(!$Type)
					$Type = "and";
				if($Type == "and")
				{
					$R = true;
					if(!$this->CompareRecursive($Data[$k], isset($v["keys"]) ? $v["keys"] : null, isset($v["type"]) ? $v["type"] : null, isset($v["value"]) ? $v["value"] : null))
					{
						$R = false;
						break;
					}
				}
				elseif($Type == "or")
				{
					$R = false;
					if($this->CompareRecursive($Data[$k], isset($v["keys"]) ? $v["keys"] : null, isset($v["type"]) ? $v["type"] : null, isset($v["value"]) ? $v["value"] : null))
					{
						$R = true;
						break;
					}
				}
			}
		}
		elseif($Value)
		{
			if(is_array($Value))
			{
				if(!$Type)
					$Type = "and";
				if(!is_array($Data))
					$Data = array($Data);
				if($Type == "and")
				{
					$R = !(bool) (sizeof(array_diff($Value, $Data)) || sizeof(array_diff($Data, $Value)));
				}
				elseif($Type == "or")
				{
					$R = (bool) sizeof(array_intersect($Value, $Data));
				}
			}
			else
			{
				if(is_array($Data))
				{
					$R = false;
					if($Type == "or")
					{
						$R = (bool) sizeof(array_intersect(array($Value), $Data));
					}
				}
				else
				{
					$R = $Value == $Data;
				}
			}
		}
		else $R = false;
		
		return $R;
	}

	/**
	 * Формирует системное сибытие для подписанных сервисов
	 * @param integer $EventType - Идентификатор типа события
	 * @param array $Detail - Детали события
	 */
	public function CreateSystemEvent($EventType, $Detail)
	{
		$Subscr = $this->CData->GetServicesSubscription($EventType);
		if(is_array($Subscr))
		{
			foreach($Subscr as $s)
			{
				if($s["detail"])
				{
					$s["detail"] = json_decode($s["detail"], true);
					if(!$this->CompareRecursive($Detail, $s["detail"]["keys"], $s["detail"]["type"]))
						break;
				}
				$this->CallFunction(array($s["className"], $s["methodName"]), array($Detail));
			}
		}
	}
	
	
	public function AutoUpdate($Script)
	{
		return $this->CData->AutoUpdate($Script);
	}
	
	public function DateDiffAutoUpdate($Script)
	{
		return $this->CData->DateDiffAutoUpdate($Script);
	}
	
	public function GetLastAutoUpdate($Script)
	{
		return $this->CData->GetLastAutoUpdate($Script);
	}
}