<?php
class PartnersTypesData extends Data
{
	/**
	 *
	 * Создает тип партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function CreatePartnerType($Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_CREATE, User::Init()->GetAccount());

		$this->Exec("INSERT INTO `".DBS_REFERENCES."`.`partners_types`
	(`name`,
	`description`)
VALUES
	(".$this->Esc($Name, true, false, true).",
	".$this->Esc($Description).");");
		return $this->DB->GetLastID();
	}

	/**
	 *
	 * Сохраняет изменения типа партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function SavePartnerType($PartnerType, $Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_EDIT, User::Init()->GetAccount());

		$U = array();

		if($Name)
			$U[] = "`name`=".$this->Esc($Name);
		if($Description !== null)
			$U[] = "`description`=".$this->Esc($Description);
		if(sizeof($U))
			$this->Exec("UPDATE `".DBS_REFERENCES."`.`partners_types`
SET ".implode(", ", $U)."
WHERE `partners_type_id`=".$PartnerType.";");
	}

	/**
	 *
	 * Возвращает список типов партнеров
	 */
	public function GetPartnersTypeList()
	{
		return $this->Get("SELECT
	`partners_type_id` AS partnerTypeId,
	`name` AS partnerTypeName,
	`description` AS partnerTypeDescription
FROM `".DBS_REFERENCES."`.`partners_types`
ORDER BY `name`");
	}
}