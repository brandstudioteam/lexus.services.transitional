<?php
class UsersPolling extends Controller
{
    /**
     *
     * @var UserEvaluationDealers
     */
	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return UserEvaluationDealers
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				//Логин
				"partnerDivisionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "code"		=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_MD5),
				),
                "answerId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "memberId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "comment"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "dateStart"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATETIME),
				),
                "dateEnd"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATETIME),
				),
			)
		);

		$this->Modes = array(
			//Создание
			"a"	=> array(
				"exec"			=> array("UsersPollingProcessor", "AnswerPolling"),
				"TplVars"		=> array("code" => 2, "answerId" => 2, "comment" => 0),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "b"	=> array(
				"exec"			=> array("UsersPollingProcessor", "GetPollings"),
                                        //$Member, $Dealers, $DateStart, $DateEnd
				"TplVars"		=> array("memberId" => 0, "partnerDivisionId" => 2, "dateStart" => 0, "dateStart" => 0),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "c"	=> array(
				"exec"			=> array("UsersPollingProcessor", "SendPolling"),
				"TplVars"		=> array("memberId" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			)
		);
	}
}