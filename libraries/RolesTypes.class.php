<?php
class RolesTypes extends Controller
{
	/**
	 *
	 * @var RolesTypes
	 */
	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return RolesTypes
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				//Пароль
				"roleTypeId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"roleTypeName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"roleTypeDescription"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				)
			)
		);

		$this->Modes = array(
			//Получить список типов ролей
			"a"	=> array(
				"exec"			=> array("RolesProcessor", "GetRolesTypes"),
			),
			//Создать тип роли
			"b"	=> array(
				"exec"				=> array("RolesProcessor", "CreateRoleTypes"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleTypeId" => 1, "roleName" => 1, "roleDescription" => 0)
			),
			//Изменить данные типа роли
			"c"	=> array(
				"exec"				=> array("RolesProcessor", "SaveRoleTypes"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1, "roleName" => 0, "roleDescription" => 0)
			),
			//Удалить тип роли
			"d"	=> array(
				"exec"				=> array("RolesProcessor", "DeleteRoleTypes"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
			//Получить соответствие типов пользователей типам ролей
			"e"	=> array(
				"exec"				=> array("RolesProcessor", "GetRolesTypesUsersTypes"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			)
		);
	}
}