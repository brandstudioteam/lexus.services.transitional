<?php
class CarOrdersStateData extends Data
{
    public function CheckState($Order, $Password)
    {
        $R = $this->Get("SELECT
    `order_id` AS orderId,
	`status` AS orderStatus,
    CASE `status`
        WHEN 1 THEN 'ожидание заказа в производство'
        WHEN 2 THEN 'автомобиль производится'
        WHEN 3 THEN 'доставка из производства'
        WHEN 4 THEN 'ожидайте, Вам позвонит дилер в ближайшее время' END AS orderStatusName,
	`partners_division_id` AS partnerDivisionId,
	`user_type` AS userType,
	`user_name` AS userName,
	`submodel_id` AS submodelId,
	`create` AS created,
	`update` AS updated,
	`submodel_name` AS submodelName,
	`partners_division_name` AS partnerDivisionName,
	`partners_division_address` AS partnerDivisionAddress,
	`partners_division_phones` AS partnerDivisionPhone,
	`partners_division_sites` AS partnerDivisionSite
FROM `lexus_references`.`orders_cars_statuses_f`
WHERE `order_id`=".$Order."
	AND `password_md5`=".$this->Esc($Password).";", true);
        if(!$R)
            throw new dmtException("NotFound", 2);
        return $R;
    }
}