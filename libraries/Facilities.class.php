<?php
class Facilities extends Controller
{
	/**
	 *
	 * @var Facilities
	 */
 	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Facilities
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				"facilityName"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0)
				),
				"facilityDescription"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0)
				),
				"facilityId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"partnerDivisionId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"type"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"facilityIcon"		=> array(
					"isfile"	=> array(),
					"required"	=> true
				),
				"partnerDivisionModelStatus"		=> array(
					"filter"		=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1))
				),
				"partnerDivisionModelCount"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"partnerDivisionModelStatusAll"	=> array(
					"TplVars"		=> array("facilityId" => 2, "modelId" => 2, "partnerDivisionId" => 2, "partnerDivisionModelStatus" => 2)
				),
				"partnerDivisionModelCountAll"	=> array(
					"TplVars"		=> array("facilityId" => 2, "modelId" => 2, "partnerDivisionId" => 2, "partnerDivisionModelCount" => 2)
				),
				"partnerDivisionFacilityStatus"		=> array(
					"filter"		=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1))
				),
				"partnerDivisionFacilityStatusAll"	=> array(
					"TplVars"		=> array("facilityId" => 2, "partnerDivisionId" => 2, "partnerDivisionFacilityStatus" => 2)
				),
				"file"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 5242880, 5242880),
						"mime"			=> array(
							"accept"		=> array("csv")
						),
    					"name"			=> md5(microtime(true)),
						"savepath"		=> PATH_SERVICES_UPLOAD_FILE,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
			)
		);

		$this->Modes = array(
			//Получить список возможностей дилеров
			"a"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetFacilities"),
				"TplVars"		=> array("type" => 0)
			),

			//Создает возможность
			"b"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetNearestDealers"),
				"TplVars"		=> array("count" => 0, "citiesCount" => 0)
			),

			"c"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetDealersState"),
				"TplVars"		=> array("facilityId" => 2)
			),
			"d"	=> array(
				"exec"			=> array("FacilitiesProcessor", "SaveDealersModelsAll"),
				//"TplVars"		=> array("facilityId" => 2, "modelId" => 2, "partnerDivisionId" => 2, "partnerDivisionModelStatus" => 2)
				"TplVars"		=> array("partnerDivisionModelStatusAll" => 2)
			),
			"d0"	=> array(
				"exec"			=> array("FacilitiesProcessor", "SaveDealersModelsCountAll"),
				//"TplVars"		=> array("facilityId" => 2, "modelId" => 2, "partnerDivisionId" => 2, "partnerDivisionModelStatus" => 2)
				"TplVars"		=> array("partnerDivisionModelStatusAll" => 2)
			),
			"e"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetDealersModelsAll"),
				"TplVars"		=> array("facilityId" => 2, "partnerDivisionModelStatus" => 0)
			),
			"e0"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetDealersModelsCountAll"),
				"TplVars"		=> array("facilityId" => 2)
			),
			"f"	=> array(
				"exec"			=> array("FacilitiesProcessor", "SaveDealersFacilitiesAll"),
				"TplVars"		=> array("partnerDivisionFacilityStatusAll" => 2)
			),
			"g"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetRelatedFacilities")
			),
			"h"	=> array(
				"exec"			=> array("FacilitiesProcessor", "ImportDealersModels"),
				"getFiles"		=> "file",
				"TplVars"		=> array("facilityId" => 2)
			),
			"i"	=> array(
				"TplVars"		=> array("file" => 2)
			),
			
			//Получить список возможностей дилеров
			"a0"	=> array(
				"exec"			=> array("FacilitiesProcessor", "GetFacilitiesNew"),
				"TplVars"		=> array("type" => 0)
			),
		);
	}
}