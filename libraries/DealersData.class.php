<?php

class DealersData extends Data
{
	/**
	 *
	 * Возвращает название дилера (партнера типа 1 или 6)
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetDealerName($Dealer)
	{
		return $this->Count("SELECT
	`name` AS Cnt
FROM `".DBS_REFERENCES."`.`dealers`
WHERE `partners_division_id`=".$Dealer.";");
	}

	/**
	 *
	 * Возвращает RCode дилера (партнера типа 1 или 6)
	 * @param integer $Dealer - Идентификатор дилера
	 */
	public function GetDealerRCode($Dealer)
	{
		return $this->Count("SELECT
	`rcode` AS Cnt
FROM `".DBS_REFERENCES."`.`dealers`
WHERE `partners_division_id`=".$Dealer.";");
	}



	/**
	 *
	 * Возвращает основную информацию об одном или нескольких дилерах
	 * @param mixed $Dealer - Идентификатор дилера или массив идентификаторов дилеров или строка идентификаторов дилеров, разделенных запятыми
	 */
	public function GetDealers($Dealer)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`name` AS partnerName
FROM `".DBS_REFERENCES."`.`dealers`
WHERE `partners_division_id`".$this->PrepareValue($Dealer).";");
	}

    public function GetDealerData($Dealer)
    {
        return $this->Get("SELECT
	d.`partners_division_id` AS partnerId,
	d.`partners_type_id` AS partnerTypeId,
	d.`name` AS partnerName,
	d.`site` AS partnerSite,
	d.`city_id` AS cityId,
	d.`latitude` AS gLt,
	d.`longitude` AS gLg,
	d.`address` AS partnerAddress,
	d.`phones` AS partnerPhone,
	d.`rcode` AS partnerRCode,
	d.`status` AS partnerStatus,
	d.`map_image` AS  partnerMap,
    d.`rcode` AS RCode,
	d.`phones_2` AS partnerPhone2
FROM `".DBS_REFERENCES."`.`dealers` AS d
WHERE d.`partners_division_id`=".$Dealer.";", true);
    }

	/**
	 *
	 * Возвращает основную информацию об одном или нескольких дилерах
	 * @param mixed $Dealer - Идентификатор дилера или массив идентификаторов дилеров или строка идентификаторов дилеров, разделенных запятыми
	 */
	public function GetDealersListFull($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null, $FacilityStatus = null)
	{
		$Fields = "SELECT
	d.`partners_division_id` AS partnerId,
	d.`partners_type_id` AS partnerTypeId,
	d.`name` AS partnerName,
	d.`site` AS partnerSite,
	d.`city_id` AS cityId,
	d.`latitude` AS gLt,
	d.`longitude` AS gLg,
	d.`address` AS partnerAddress,
	d.`phones` AS partnerPhone,
	d.`rcode` AS partnerRCode,
	d.`status` AS partnerStatus,
	d.`map_image` AS  partnerMap,
	d.`phones_2` AS partnerPhone2,
	b.`partners_division_id` AS partnersDivisionId";
$Query = "
FROM `".DBS_REFERENCES."`.`dealers` AS d
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS b ON b.`old_id`=d.`partners_division_id` AND b.`brand_id`=".WS::Init()->GetBrandId();
		$Where = array();
		$IsOne = false;
		if($Dealer)
		{
			$Where[] = "d.`partners_division_id`".$this->PrepareValue($Dealer);
			$IsOne = !$this->CheckArg($Dealer);
		}
		if($Only)
		{
			$Where[] = "d.`partners_division_id`".$this->PrepareValue($Only);
		}
        if(!$Dealer && !$Only)
        {
            if($Status === null || is_empty($Status))
                $Where[] = "d.`status`=1";
            else $Where[] = "d.`status`=".$Status;
        }
		if($Exclude)
		{
			$Where[] = "d.`partners_division_id`".$this->PrepareValue($Exclude, false, 2);
		}
		if($City)
		{
			$Where[] = "d.`city_id`".$this->PrepareValue($City);
		}
		if($Find)
		{
			$Where[] = "d.`name` LIKE ".$this->Esc("%".$Find."%");
		}
		if($Facility)
		{
			$Fields .= ",
	df.`status` AS partnerFacilityStatus";
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`";

			$Where[] = "df.`facility_id`".$this->PrepareValue($Facility);
            if($FacilityStatus)
                $Where[] = "df.`status`".$this->PrepareValue($FacilityStatus);
			else $Where[] = "df.`status`=1";
		}
		if(sizeof($Where))
		{
			$Query .= "
WHERE ".implode(" AND ", $Where);
		}
		$Query .= "
ORDER BY cityId, partnerName";





		return $this->Get($Fields.$Query, $IsOne);
	}

	/**
	 *
	 * Возвращает основную информацию об одном или нескольких дилерах
	 * @param mixed $Dealer - Идентификатор дилера или массив идентификаторов дилеров или строка идентификаторов дилеров, разделенных запятыми
	 */
	public function GetDealersListWithGeoFull($Only = null, $Exclude = null, $City = null, $Find = null, $Facility = null, $Dealer = null, $Status = null)
	{



		$Query = "SELECT
	d.`partners_division_id` AS partnerId,
	d.`partners_type_id` AS partnerTypeId,
	d.`name` AS partnerName,
	d.`brand_name` AS partnerOfficialName,
	d.`site` AS partnerSite,
	d.`latitude` AS gLt,
	d.`longitude` AS gLg,
	d.`address` AS partnerAddress,
	d.`phones` AS partnerPhone,
	d.`rcode` AS partnerRCode,
	IF(d.`status`, 'Активен', 'Не активен') AS partnerStatusName,
	d.`status` AS partnerStatus,
	d.`rcode_hash` AS partnerHash,
	d.`map_image` AS  partnerMap,
	d.`partners_type_name` AS partnerTypeName,
	d.`city_id` AS cityId,
	d.`city_name` AS cityName,
	d.`region_id` AS regionId,
	d.`region_name` AS regionName,
	d.`country_id` AS countryId,
	d.`country_name` AS countryName,
	d.`federal_district_id` AS frderalDistrictId,
	d.`federal_district_name` AS frderalDistrictName,
	d.`phones_2` AS partnerPhone2
FROM `".DBS_REFERENCES."`.`dealersGeoFull` AS d";
		$Where = array();
        if(!$Dealer && !$Only)
        {
            if($Status === null || is_empty($Status))
                $Where[] = "d.`status`=1";
            else $Where[] = "d.`status`=".$Status;
        }
		if($Dealer)
			$Where[] = "d.`partners_division_id`=".$Dealer;
		if($Only)
		{
			$Where[] = "d.`partners_division_id`".$this->PrepareValue($Only);
		}
		if($Exclude)
		{
			$Where[] = "d.`partners_division_id`".$this->PrepareValue($Exclude, false, 2);
		}
		if($City)
		{
			$Where[] = "`d.city_id`".$this->PrepareValue($City);
		}
		if($Find)
		{
			$Where[] = "d.`name` LIKE ".$this->Esc("%".$Find."%");
		}
		if($Facility)
		{
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`";

			$Where[] = "d.`facility_id`".$this->PrepareValue($Facility);
		}
		if(sizeof($Where))
		{
			$Query .= "
WHERE ".implode(" AND ", $Where);
		}
		$Query .= "
ORDER BY cityId, partnerName";
		return $this->Get($Query);
	}

	public function GetCitiesForPartners($PartnerType = null, $Only = null, $Exclude = null, $Status = null)
	{
		$Query = "SELECT DISTINCT
	c.`city_id` AS cityId,
	c.`name` AS cityName
FROM `".DBS_REFERENCES."`.`dealers` AS pd
LEFT JOIN `references`.`cities` AS c ON c.`city_id`=pd.`city_id`";
		$Where = array();
        if($Status === null || is_empty($Status))
            $Where[] = "pd.`status`=1";
        else $Where[] = "pd.`status`=".$Status;
		if($PartnerType)
		{
			$Where[] = "pd.`partners_type_id`".$this->PrepareValue($PartnerType);
		}
		if($Only)
		{
			$Where[] = "c.`city_id`".$this->PrepareValue($Only);
		}
		if($Exclude)
		{
			$Where[] = "c.`city_id`".$this->PrepareValue($Exclude, false, 2);
		}
		if(sizeof($Where))
		{
			$Query .= "
WHERE ".implode(" AND ", $Where);
		}
		return $this->Get($Query);
	}

	public function GetPartnersForCity($City, $Status = null)
	{
        $Where = array();
        if($Status === null || is_empty($Status))
            $Where[] = "`status`=1";
        else $Where[] = "`status`=".$Status;
        $Where[] = "`city_id`=".$City;
		return $this->Get("SELECT DISTINCT
	`partners_division_id` AS partnerId,
	`name` AS partnerName,
	`site` AS partnerSite,
	`city_id` AS cityId,
	`latitude` AS gLt,
	`longitude` AS gLg,
	`address` AS partnerAddress,
	`phones` AS partnerPhone,
	`rcode` AS partnerRCode,
	`status` AS partnerStatus,
	`map_image` AS  partnerMap,
	`phones_2` AS partnerPhone2
FROM `".DBS_REFERENCES."`.`dealers` ".$this->PrepareWhere($Where));
	}

    public function GetDealerPhone($Dealer)
    {
        return $this->Count("SELECT
    `phones` AS Cnt
FROM `".DBS_REFERENCES."`.`partners_divisions`
WHERE `partners_division_id`=".$Dealer.";");
    }


    public function FindDealers($DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $FacilityAlias = null, $Status = null)
	{
		$Distinct = false;

		$Fields = array("d.`partners_division_id` AS partnerId",
"d.`partners_type_id` AS partnerTypeId",
"d.`name` AS partnerName",
"d.`site` AS partnerSite",
"d.`latitude` AS gLt",
"d.`longitude` AS gLg",
"d.`address` AS partnerAddress",
"d.`phones` AS partnerPhone",
"d.`rcode` AS partnerRCode",
"d.`status` AS partnerStatus",
"d.`map_image` AS  partnerMap",
"d.`city_id` AS cityId",
"d.`region_id` AS regioonId",
"d.`region_id` AS regionId",
"d.`country_id` AS countryId",
"d.`federal_district_id` AS federalDistrictId",
"d.`phones_2` AS partnerPhone2");

		$Query = " FROM `".DBS_REFERENCES."`.`dealersGeoFull` AS d";

		$Where = array();
        if($Status === null || is_empty($Status))
            $Where[] = "d.`status`=1";
        else $Where[] = "d.`status`=".$Status;
		if($DealerType)
		{
			$Where[] = "d.`partners_type_id`".$this->PrepareValue($DealerType);
		}
		if($City)
		{
			$Where[] = "d.`city_id`".$this->PrepareValue($City);
		}
		if($Region)
		{
			$Where[] = "d.`region_id`".$this->PrepareValue($Region);
		}
		if($FederalDistrict)
		{
			$Where[] = "d.`federal_district_id`".$this->PrepareValue($FederalDistrict);
		}
		if($Country)
		{
			$Where[] = "d.`country_id`".$this->PrepareValue($Country);
		}

		if($Facility)
		{
			if(is_array($Facility))
			{
				$Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "df.`facility_id`".$this->PrepareValue($Facility);
			}
			else
			{
				$Fields[] = "dfsp.`facility_id` AS facilityId,
	dfsp.`url` AS partnerSite";
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfsp ON dfsp.`facility_id`=df.`facility_id` AND dfsp.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "df.`facility_id`=".$Facility;
			}
		}
		elseif($FacilityAlias)
		{
			if(is_array($FacilityAlias) && sizeof($FacilityAlias) > 1)
			{
				$Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`";
				$Where[] = "f.`alias`".$this->PrepareValue($FacilityAlias, true);
			}
			else
			{
				if(is_array($FacilityAlias))
					$FacilityAlias = $FacilityAlias[0];
				$Fields[] = "f.`old_id` AS facilityId,
	dfsp.`url` AS partnerSite";
				$Query .= "
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS npd ON npd.`old_id`=d.`partners_division_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=npd.`partners_division_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`facilities_brands` AS fb ON fb.`facility_id`=f.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfsp ON dfsp.`facility_id`=npd.`old_id` AND dfsp.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "f.`alias`=".$this->Esc($FacilityAlias, true);
				$Where[] = "fb.`brand_id`=".WS::Init()->GetBrandId();
				$Where[] = "npd.`brand_id`=".WS::Init()->GetBrandId();
				$Where[] = "df.`status`=1";
			}
		}

		if($Action)
		{
            $Distinct = true;
			if($Model)
			{
				$Where[] = "capm.`model_id`=".$Model;
			}
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`custom_actions_partners_models` AS capm ON capm.`partners_division_id`=d.`partners_division_id`";
			$Where[] = "capm.`action_id`=".$Action;
			$Where[] = "capm.`status`=1";
		}
		elseif($Promo)
		{
			$Fields[] = "padm.`page` AS partnerActionPage";
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`promo_actions_dealers` AS padm ON padm.`partners_division_id`=d.`partners_division_id`";
			$Distinct = true;
			$Where[] = "padm.`promo_action_id`=".$Promo;
			if($Model)
			{
				$Where[] = "padm.`model_id`=".$Model;
			}
		}
		$Query = "SELECT ".($Distinct ? "DISTINCT " : "").implode(",", $Fields)." ".$Query.(sizeof($Where) ? " WHERE ".implode(" AND ", $Where) : "")." ORDER BY d.`name`;";

$this->Dump(__METHOD__, "+++++++++++++", $Query);

		return $this->Get($Query);
	}



	public function FindDealers2($FacilityType, $DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $FacilityAlias = null, $Status = null)
	{
		$Distinct = false;

		$Fields = array("d.`partners_division_id` AS partnerId",
"d.`partners_type_id` AS partnerTypeId",
"d.`name` AS partnerName",
"d.`site` AS partnerSite",
"d.`latitude` AS gLt",
"d.`longitude` AS gLg",
"d.`address` AS partnerAddress",
"d.`phones` AS partnerPhone",
"d.`rcode` AS partnerRCode",
"d.`status` AS partnerStatus",
"d.`map_image` AS  partnerMap",
"d.`city_id` AS cityId",
"d.`region_id` AS regioonId",
"d.`region_id` AS regionId",
"d.`country_id` AS countryId",
"d.`federal_district_id` AS federalDistrictId",
"d.`phones_2` AS partnerPhone2");

		$Query = " FROM `".DBS_REFERENCES."`.`dealersGeoFull` AS d";

		$Where = array();
        if($Status === null || is_empty($Status))
            $Where[] = "d.`status`=1";
        else $Where[] = "d.`status`=".$Status;
		if($DealerType)
		{
			$Where[] = "d.`partners_type_id`".$this->PrepareValue($DealerType);
		}
		if($City)
		{
			$Where[] = "d.`city_id`".$this->PrepareValue($City);
		}
		if($Region)
		{
			$Where[] = "d.`region_id`".$this->PrepareValue($Region);
		}
		if($FederalDistrict)
		{
			$Where[] = "d.`federal_district_id`".$this->PrepareValue($FederalDistrict);
		}
		if($Country)
		{
			$Where[] = "d.`country_id`".$this->PrepareValue($Country);
		}

		if($Facility || $FacilityType)
		{
			if(is_array($Facility))
			{
				$Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "df.`facility_id`".$this->PrepareValue($Facility);
			}
            elseif($FacilityType)
            {
                $Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`";
				$Where[] = "f.`use_as_filter`=".$FacilityType;
            }
			else
			{
				$Fields[] = "dfsp.`facility_id` AS facilityId,
	dfsp.`url` AS partnerSite";
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfsp ON dfsp.`facility_id`=df.`facility_id` AND dfsp.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "df.`facility_id`=".$Facility;
			}
		}
		elseif($FacilityAlias)
		{
			if(is_array($FacilityAlias) && sizeof($FacilityAlias) > 1)
			{
				$Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`";
				$Where[] = "f.`alias`".$this->PrepareValue($FacilityAlias, true);
			}
			else
			{
				if(is_array($FacilityAlias))
					$FacilityAlias = $FacilityAlias[0];
				$Fields[] = "dfsp.`facility_id` AS facilityId,
	dfsp.`url` AS partnerSite";
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfsp ON dfsp.`facility_id`=df.`facility_id` AND dfsp.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "f.`alias`=".$this->Esc($FacilityAlias, true);
			}
		}

		if($Action)
		{
            $Distinct = true;
			if($Model)
			{
				$Where[] = "capm.`model_id`=".$Model;
			}
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`custom_actions_partners_models` AS capm ON capm.`partners_division_id`=d.`partners_division_id`";
			$Where[] = "capm.`action_id`=".$Action;
			$Where[] = "capm.`status`=1";
		}
		elseif($Promo)
		{
			$Fields[] = "padm.`page` AS partnerActionPage";
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`promo_actions_dealers` AS padm ON padm.`partners_division_id`=d.`partners_division_id`";
			$Distinct = true;
			$Where[] = "padm.`promo_action_id`=".$Promo;
			if($Model)
			{
				$Where[] = "padm.`model_id`=".$Model;
			}
		}

		$Query = "SELECT ".($Distinct ? "DISTINCT " : "").implode(",", $Fields)." ".$Query.(sizeof($Where) ? " WHERE ".implode(" AND ", $Where) : "")." ORDER BY d.`name`;";
		return $this->Get($Query);
	}




	public function FullFindDealers($DealerType = null, $City = null, $Region = null, $FederalDistrict = null, $Country = null, $Facility = null,
								$Latitude = null, $Longitude = null, $Count = null, $CityCount = null, $Action = null, $Model = null, $Promo = null,
								$Ref = null, $RefFrameId = null, $FacilityAlias = null, $Status = null)
	{
		$Distinct = false;

		$Fields = array("d.`partners_division_id` AS partnerId",
"d.`partners_type_id` AS partnerTypeId",
"d.`name` AS partnerName",
"d.`site` AS partnerSite",
"d.`latitude` AS gLt",
"d.`longitude` AS gLg",
"d.`address` AS partnerAddress",
"d.`phones` AS partnerPhone",
"d.`rcode` AS partnerRCode",
"d.`status` AS partnerStatus",
"d.`map_image` AS  partnerMap",
"d.`city_id` AS cityId",
"d.`region_id` AS regioonId",
"d.`region_id` AS regionId",
"d.`country_id` AS countryId",
"d.`federal_district_id` AS federalDistrictId",
"d.`phones_2` AS partnerPhone2");

		$Query = " FROM `".DBS_REFERENCES."`.`dealersGeoFull` AS d";

		$Where = array();
        if($Status === null || is_empty($Status))
            $Where[] = "d.`status`=1";
        else $Where[] = "d.`status`=".$Status;
		if($DealerType)
		{
			$Where[] = "d.`partners_type_id`".$this->PrepareValue($DealerType);
		}
		if($City)
		{
			$Where[] = "d.`city_id`".$this->PrepareValue($City);
		}
		if($Region)
		{
			$Where[] = "d.`region_id`".$this->PrepareValue($Region);
		}
		if($FederalDistrict)
		{
			$Where[] = "d.`federal_district_id`".$this->PrepareValue($FederalDistrict);
		}
		if($Country)
		{
			$Where[] = "d.`country_id`".$this->PrepareValue($Country);
		}

		if($Facility)
		{
			if(is_array($Facility))
			{
				$Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "df.`facility_id`".$this->PrepareValue($Facility);
			}
			else
			{
				$Fields[] = "dfsp.`facility_id` AS facilityId,
	dfsp.`url` AS partnerSite";
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfsp ON dfsp.`facility_id`=df.`facility_id` AND dfsp.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "df.`facility_id`=".$Facility;
			}
		}
		elseif($FacilityAlias)
		{
			if(is_array($FacilityAlias) && sizeof($FacilityAlias) > 1)
			{
				$Distinct = true;
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`";
				$Where[] = "f.`alias`".$this->PrepareValue($FacilityAlias, true);
			}
			else
			{
				if(is_array($FacilityAlias))
					$FacilityAlias = $FacilityAlias[0];
				$Fields[] = "dfsp.`facility_id` AS facilityId,
	dfsp.`url` AS partnerSite";
				$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df ON df.`partners_division_id`=d.`partners_division_id`
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfsp ON dfsp.`facility_id`=df.`facility_id` AND dfsp.`partners_division_id`=d.`partners_division_id`";
				$Where[] = "f.`alias`=".$this->Esc($FacilityAlias);
			}
		}
		if($Action)
		{
			if($Model)
			{
				$Where[] = "capm.`model_id`=".$Model;
			}
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`custom_actions_partners_models` AS capm ON capm.`partners_division_id`=d.`partners_division_id`";
			$Where[] = "capm.`action_id`=".$Action;
			$Where[] = "capm.`status`=1";
		}
		elseif($Promo)
		{
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`promo_actions_dealers` AS pad ON pad.`partners_division_id`=d.`partners_division_id`";
			$Where[] = "pad.`promo_action_id`=".$Promo;
		}

		$Query = "SELECT ".($Distinct ? "DISTINCT " : "").implode(",", $Fields)." ".$Query.(sizeof($Where) ? " WHERE ".implode(" AND ", $Where) : "")." ORDER BY d.`name`;";
		return $this->Get($Query);
	}


	public function GetDealersModelsForPromo($Promo)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`model_id` AS modelId
FROM `".DBS_REFERENCES."`.`promo_actions_dealers_models`
WHERE `promo_action_id`=".$Promo.";");
	}



	public function GetRelatedFacilities($Dealer = null, $Type = null)
	{
        if($Type)
        {
            $sql = "SELECT
	a.`partners_division_id` AS partnerId,
	a.`facility_id` AS facilityId,
	a.`status` AS partnerDivisionFacilityStatus
FROM `".DBS_REFERENCES."`.`partners_divisions_facilities` AS a
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS b ON b.`facility_id`=a.`facility_id`
WHERE a.`status`=1
	AND b.`use_as_filter`=".$Type.";";
        }
        else
        {
            $sql = "SELECT
	`partners_division_id` AS partnerId,
	`facility_id` AS facilityId,
	`status` AS partnerDivisionFacilityStatus
FROM `".DBS_REFERENCES."`.`partners_divisions_facilities`
WHERE `status`=1".($Dealer ? "
	AND `partners_division_id`".$this->PrepareValue($Dealer) : "").";";
        }

        //echo $sql;

        $R = $this->Get($sql);

        return $R;
	}


	public function GetRelatedFacilitiesNT($Brand)
	{
		$Query = "SELECT
	c.`old_id` AS partnerId,
	b.`old_id` AS facilityId,
	a.`status` AS partnerDivisionFacilityStatus
FROM `references`.`partners_divisions_facilities` AS a
LEFT JOIN `references`.`facilities` AS b ON b.`facility_id`=a.`facility_id`
LEFT JOIN `references`.`facilities_brands` AS d ON d.`facility_id`=a.`facility_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`
WHERE a.`status`=1
	AND d.`brand_id`=".$Brand."
	AND c.`brand_id`=".$Brand."
	AND b.`facilities_type_id`=1";
        return $this->Get($Query);
	}



	public function GetRelatedFacilitiesFull($Dealer = null)
	{
		return $this->Get("SELECT
	df.`partners_division_id` AS partnerId,
	f.`facility_id` AS facilityId,
	f.`name` AS facilityName,
	f.`description` AS facilityDescription,
	f.`icon` AS facilityIcon,
	f.`use_as_filter` AS facilityAsFilter,
	f.`alias` AS facilityAlias,
	f.`advanced_info_ref` AS facilityAdvInfo,
	dfs.`url` AS facilityDealerUrl
FROM `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfs ON ((dfs.`facility_id`=df.`facility_id`) AND (dfs.`partners_division_id`=df.`partners_division_id`))".($Dealer ? "
WHERE df.status = 1 and df.`partners_division_id`".$this->PrepareValue($Dealer) : "")."
ORDER BY partnerId, facilityName;");
	}

	public function GetNotRelatedFacilities($Dealer)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`facility_id` AS facilityId
FROM `".DBS_REFERENCES."`.`partners_divisions_facilities`
WHERE `partners_division_id`".$this->PrepareValue($Dealer, false, 2).";");
	}

	public function GetNotRelatedFacilitiesFull($Dealer)
	{
		return $this->Get("SELECT
	df.`partners_division_id` AS partnerId,
	f.`facility_id` AS facilityId,
	f.`name` AS facilityName,
	f.`description` AS facilityDescription,
	f.`icon` AS facilityIcon,
	f.`use_as_filter` AS facilityAsFilter,
	f.`alias` AS facilityAlias,
	f.`advanced_info_ref` AS facilityAdvInfo,
	dfs.`url` AS facilityDealerUrl
FROM `".DBS_REFERENCES."`.`partners_divisions_facilities` AS df
LEFT JOIN `".DBS_REFERENCES."`.`facilities` AS f ON f.`facility_id`=df.`facility_id`
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities_sites_partitions` AS dfs ON ((dfs.`facility_id`=df.`facility_id`) AND (dfs.`partners_division_id`=df.`partners_division_id`))
WHERE df.`partners_division_id`
ORDER BY partnerId, facilityName".$this->PrepareValue($Dealer, false, 2).";");
	}

	public function GetDealersTypes()
	{
		return $this->Get("SELECT
	`partners_type_id`,
	`name`,
	`description`
FROM `".DBS_REFERENCES."`.`partners_types`
WHERE `partners_type_id` IN (1, 6);");
	}


	public function GetCityFromDealers($Dealers)
	{
		return $this->Count("SELECT
	`city_id` AS Cnt
FROM `".DBS_REFERENCES."`.`dealers`
WHERE `partners_division_id`=".$Dealers.";");
	}

	public function GetEmailsForFacilities($Dealers, $Facilities)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`facility_id` AS facilityId,
	`email` AS email
FROM `".DBS_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`".$this->PrepareValue($Dealers)."
	AND `facility_id`".$this->PrepareValue($Facilities).";");
	}

	public function GetEmailsForFacilitiesNew($Dealers, $Facilities)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`facility_id` AS facilityId,
	`email` AS email
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`".$this->PrepareValue($Dealers)."
	AND `facility_id`".$this->PrepareValue($Facilities).";");
	}



	public function GetDealersNew($Brand, $Status = null, $Dealer = null)
	{
		$Query = "SELECT
	a.`partners_division_id` AS partnerDivisionId,
	a.`partners_type_id` AS partnerTypeId,
	a.`name` AS partnerName,
	a.`site` AS partnerSite,
	a.`latitude` AS gLt,
	a.`longitude` AS gLg,
	a.`address` AS partnerAddress,
	a.`phones` AS partnerPhone,
	a.`rcode` AS partnerRCode,
	a.`status` AS partnerStatus,
	a.`map_image` AS  partnerMap,
	a.`city_id` AS cityId,
	a.`region_id` AS regioonId,
	a.`region_id` AS regionId,
	a.`country_id` AS countryId,
	a.`federal_district_id` AS federalDistrictId,
	a.`phones_2` AS partnerPhone2,
	a.`rcode` AS RCode,
	a.`old_id` AS partnerDivisionOldId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`dealersGeoFull` AS a
WHERE a.`status`".$this->PrepareValue($Status !== null && !is_empty($Status) ? $Status : 1)."
	AND a.`brand_id`=".$Brand.($Dealer ? "
	AND a.`partners_division_id`=".$Dealer : "");
		return $this->Get($Query." ORDER BY a.`name`;", (boolean) $Dealer);
	}

	public function GetDealersNewForImport($Brand)
	{
		return $this->Get("SELECT
	a.`partners_division_id` AS partnerDivisionId,
	a.`rcode` AS partnerRCode
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions` AS a
WHERE a.`partners_type_id` IN (1,6)
	AND a.`brand_id`=".$Brand.";");
	}

	public function GetOldId($Dealer)
	{
		return $this->Count("SELECT
	`old_id` AS Cnt
FROM `references`.`partners_divisions`
WHERE `partners_division_id`=".$Dealer.";");
	}
}