<?php
class AccountsTypesProcessor extends Processor
{
	/**
	 *
	 * @var AccountsTypesProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var AccountsTypesData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return AccountsTypesProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new AccountsTypesData();
	}


    public function GetAccountsTypes()
    {
        return $this->CData->GetAccountsTypes(User::Init()->GetType());
    }

    public function CreateAccountType($Name, $Description = null)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_TYPES_CREATE);
        return $this->CData->CreateAccountType($Name, $Description);
    }

    public function SaveAccountType($AccountType, $Name = null, $Description = null)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_TYPES_EDIT);
        return array("updated" => $this->CData->SaveAccountType($AccountType, $Name, $Description));
    }

    public function DeleteAccountType($AccountType)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_TYPES_DELETE);
        return array("deleted" => $this->CData->DeleteAccountType($AccountType));
    }

    public function LinkAccountsTypesWithRolesTypes($Links)
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_TYPES_LINK_ROLES_TYPES);
        if(!is_array($Links))
            throw new dmtException("Links type error", 100);
        return array("linked" => $this->CData->LinkAccountsTypesWithRolesTypes($Links));
    }

    public function GetAccountsTypesPartnersTypes()
    {
        return $this->CData->GetAccountsTypesPartnersTypes(User::Init()->GetType());
    }
}