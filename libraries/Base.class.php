<?php
class Base extends BaseDebug
{
	protected function CalledClalssFrom()
	{
		$t = debug_backtrace();
		return $t[2]["object"];
	}
	
	protected function Forbidden()
	{
		throw new dmtFailException("Access denid", 77777);
	}
	
	protected function NotFound()
	{
		throw new dmtFailException("NotFound", 77775);
	}
	
	private function GetObject($Callback)
	{
		return method_exists($Callback, "Init") ? $Callback::Init() : $Callback;
	}

	protected function CallFunction($Callback, $Arguments = null)
	{
		$M = null;
		$A = null;
		if(is_array($Callback))
		{
			switch (sizeof($Callback))
			{
				case 1:
					$M = $Callback[0];
					break;
				case 2:
					if(is_array($Callback[0]))
					{
						$M = array(
							$this->GetObject($Callback[0][0]),
							$Callback[0][1]
						);
						$A = $Callback[1];
					}
					elseif(is_array($Callback[1]))
					{
						$M = $Callback[0];
						$A = $Callback[1];
					}
					else
					{
						$M = array(
							$this->GetObject($Callback[0]),
							$Callback[1]
						);
					}
					break;
				case 3:
					$M = array(
						$this->GetObject($Callback[0]),
						$Callback[1]
					);
					$A = $Callback[2];
					break;
				default:
					throw new dmtException("Configuration error", 77770);
			}
			if(!$A) $A = array();
		}
		else $M = $Callback;
		if($Arguments)
		{
			if($A)
			{
				if(!is_array($A))
					$A = array($A);
				if(is_array($Arguments))
				{
					$A = $A + $Arguments;
				}
				else
				{
					array_push($A, $Arguments);
				}
			}
			else
			{
				$A = is_array($Arguments) ? $Arguments : array($Arguments);
			}
		}
		return call_user_func_array($M, $A);
	}
}