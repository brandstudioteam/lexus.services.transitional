<?php
/**
 *
 * Права системы администрирования
 */

/**
 *
 * Доступ к системе администрирования
 * @var integer
 */
define("PERMISSIONS_ADMIN_ACCESS",										1);



/**
 *
 * Права сервиса Пользователи
 */
/**
 *
 * Доступ к управлению типами пользователей
 * @var integer
 */
define("PERMISSIONS_USERS_TYPES_ADMIN_ACCESS",                          2);

/**
 * Создавать тип пользователей
 */
define("PERMISSIONS_USERS_TYPES_CREATE",                                3);
/**
 * Изменять тип пользователей
 */
define("PERMISSIONS_USERS_TYPES_EDIT",                                  4);
/**
 * Удалять тип пользователей
 */
define("PERMISSIONS_USERS_TYPES_DELETE",                                5);
/**
 * Связывать тип пользователя с типом роли
 */
define("PERMISSIONS_USERS_TYPES_LINK_ROLES_TYPES",                      6);


/**
 *
 * Доступ к управлению пользователями
 * @var integer
 */
define("PERMISSIONS_USERS_ADMIN_ACCESS",								7);
/**
 * Просматривать пользователей
 */
define("PERMISSIONS_USERS_VIEW",                                        8);
/**
 * Создавать пользователя
 */
define("PERMISSIONS_USERS_CREATE",                                      9);
/**
 * Изменять данные пользователя
 */
define("PERMISSIONS_USERS_EDIT",                                        10);
/**
 * Удалять пользователя
 */
define("PERMISSIONS_USERS_DELETE",                                      11);
/**
 * Управлять правами пользователя
 */
define("PERMISSIONS_USERS_ACCESS",                                      21);
/**
 * Изменять пароли
 */
define("PERMISSIONS_USERS_PASSWORD",                                    14);






/**
 * Доступ к управлению типами ролей
 */
define("PERMISSIONS_ROLES_TYPES_ADMIN_ACCESS",                          15);
/**
 *
 * Чтение типов ролей
 * @var integer
 */
define("PERMISSIONS_ROLES_TYPES_READY",                                 16);
/**
 *
 * Создание типа ролей
 * @var integer
 */
define("PERMISSIONS_ROLES_TYPES_CREATE",                                17);
/**
 *
 * Изменение типа ролей
 * @var integer
 */
define("PERMISSIONS_ROLES_TYPES_SAVE",                                  18);
/**
 *
 * Удаление типа ролей
 * @var integer
 */
define("PERMISSIONS_ROLES_TYPES_DELETE",                                19);




/**
 * Управление ролями
 */
/**
 *
 * Доступ к управлению ролями
 * @var integer
 */
define("PERMISSIONS_ROLES_ADMIN_ACCESS",                                20);
/**
 *
 * Чтение ролей
 * @var integer
 */
        define("PERMISSIONS_ROLES_READY",                               21);
/**
 *
 * Создание роли
 * @var integer
 */
define("PERMISSIONS_ROLES_CREATE",                                      22);
/**
 *
 * Изменение роли
 * @var integer
 */
define("PERMISSIONS_ROLES_SAVE",                                        23);
/**
 *
 * Удаление роли
 * @var integer
 */
define("PERMISSIONS_ROLES_DELETE",                                      24);
/**
 *
 * Связывание роли с правами
 * @var integer
 */
define("PERMISSIONS_ROLES_LINK",                                        25);





/**
 * Управление типами прав
 */
/**
 *
 * Доступ к управлению типами прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_ADMIN_ACCESS",					26);
/**
 *
 * Чтение типов прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_READY",							27);
/**
 *
 * Создание типа прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_CREATE",							28);
/**
 *
 * Изменение типа прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_SAVE",							29);
/**
 *
 * Удаление типа прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_DELETE",							30);




/**
 * Управление правами
 */
/**
 *
 * Доступ к управлению правами
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_ADMIN_ACCESS",							31);
/**
 *
 * Чтение прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_READY",                                 32);
/**
 *
 * Создание права
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_CREATE",                            	33);
/**
 *
 * Изменение права
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_SAVE",                  				34);
/**
 *
 * Удаление права
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_DELETE",                    			35);
/**
 *
 * Чтение прав для работы клиента
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_READY_FOR_CLIENT",						36);









/**
 *
 * Доступ к управлению типами партнеров
 */
define("PERMISSIONS_PARTNERS_TYPE_ADMIN_ACCESS",						37);
/**
 *
 * Чтение типов партнеров
 */
define("PERMISSIONS_PARTNERS_TYPE_READY",                               38);
/**
 *
 * Создание типа партнеров
 */
define("PERMISSIONS_PARTNERS_TYPE_CTREATE",                             39);
/**
 *
 * Изменение типа партнеров
 */
define("PERMISSIONS_PARTNERS_TYPE_SAVE",                                40);
/**
 *
 * Удаление типа партнеров
 */
define("PERMISSIONS_PARTNERS_TYPE_DELETE",                              41);




/**
 *
 * Доступ к управлению партнерами
 */
define("PERMISSIONS_PARTNERS_ADMIN_ACCESS",                 			42);
/**
 *
 * Чтение партнеров
 */
define("PERMISSIONS_PARTNERS_READY",                                    43);
/**
 *
 * Создание партнера
 */
define("PERMISSIONS_PARTNERS_CTREATE",                                  44);
/**
 *
 * Изменение партнера
 */
define("PERMISSIONS_PARTNERS_SAVE",                                     45);
/**
 *
 * Удаление партнера
 */
define("PERMISSIONS_PARTNERS_DELETE",                                   46);
/**
 *
 * Связывание дилеров с возможностфми дилеров
 */
define("PERMISSIONS_DEALERS_FACILITYES_RELATE",							47);








/**
 * Управление мероприятиями X-Country
 */
/**
 *
 * Доступ к управлению кампаниями тест-драйвов
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_ADMIN_ACCESS",                			48);
/**
 *
 * Управление кампаниями тест-драйвов: создание, редактирование, удаление
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_MANAGEMENT",                    		49);
/**
 *
 * Просмотр полной информации об активной кампании тест-драйвов
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_ACTIVE_READY",                      	50);
/**
 *
 * Просмотр полной информации о любой кампании тест-драйвов
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_READY",                             	51);
/**
 *
 * Доступ к отчетам активной кампании
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_ACTIVE_REPORTS",						52);
/**
 *
 * Доступ ко всем отчетам
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_REPORTS",       						53);
/**
 *
 * Управление участниками заездов активной кампании
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_ACTIVE_MEMBERS_MANAGEMENT",				54);
/**
 *
 * Управление всеми участниками заездов
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_MEMBERS_MANAGEMENT",        			55);
/**
 *
 * Просмотр информации об участниках тест-драйва активной кампании
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_ACTIVE_MEMBERS_READY",					56);
/**
 *
 * Просмотр информации об участниках всех тест-драйвов
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_MEMBERS_READY",                     	57);
/**
 *
 * Импорт данных
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_DATA_IMPORT",							58);
/**
 *
 * Просмотр информации о предварительно зарегистрированных пользователях
 * для участия в тест-драйва активной кампании
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_PREREGISTRED_MEMBERS_READY",        	59);
/**
 *
 * Управление предварительно зарегистрированными пользователями
 * для участия в тест-драйва активной кампании
 * @var integer
 */
define("PERMISSIONS_TDCAMPAIGNS_PREREGISTRED_MEMBERS_MANAGEMENT",		60);
/**
 * Экспорт данных
 */
define("PERMISSIONS_TDCAMPAIGNS_DATA_EXPORT",                           61);
/**
 * Рассылка SMS
 */
define("PERMISSIONS_TDCAMPAIGNS_DELIVERY_SMS",                          62);
/**
 * Рассылка электронных писем
 */
define("PERMISSIONS_TDCAMPAIGNS_DELIVERY_EMAIL",                        63);
/**
 * Рассылка электронных писем
 */
define("PERMISSIONS_TDCAMPAIGNS_GALERY_MANAGEMENT",                     65);
/**
 * Рассылка электронных писем
 */
define("PERMISSIONS_TDCAMPAIGNS_GOOGLE_ANALITICS_READY",                66);


/**
 * Управление записью на техническое обслуживание
 */
/**
 *
 * Доступ к управлению записью на техническое обслуживание
 * @var integer
 */
define("PERMISSIONS_TO_ADMIN_ACCESS",                       			87);
/**
 *
 * Управление действиями пользователя во время технического обслуживания
 * @var integer
 */
define("PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT",                        88);
/**
 *
 * Просмотр заявок на техническое обслуживание
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_READY",                                  89);
/**
 *
 * Управление заявками на техническое обслуживание
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_MANAGEMENT",                             90);


define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_MANAGEMENT",		91);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_READY",        	92);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT",		93);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY",           94);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_MANAGEMENT",		95);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_READY",       		96);