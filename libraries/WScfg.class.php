<?php
if(!defined("USERS_EVENTS_DRIVER_HEADER_NAME"))
    define("USERS_EVENTS_DRIVER_HEADER_NAME", "X-REQUESTED-SDCFAONJYWNHUWQ");
if(!defined("USERS_EVENTS_DRIVER_HEADER_VALUE"))
    define("USERS_EVENTS_DRIVER_HEADER_VALUE", "1g3Qn91-u_E10-t-r_58");

class WScfg
{
	static public function Sets()
	{
		return array(
			/**
			 * Определения: <имя_поддомена>.[<имя_поддомена>....]<имя_домена>.<имя_супердомена>/<имя_раздела>/[<имя_раздела>/...]
			 */
			"domains"		=> array(
				//"lexus_masterclass" => array(
				URL_SA		=> array(
					"domains"	=> array(
						"default"		=> array(
							"access"	=> array(),
							"controllers"	=> array(),
							"parts"	=> array(
								"exit"		=> array(
									"parts"		=> array(
										"default"	=> array(
											"defaultAction" => array(WSCONFIG_ACTIONS_EXIT),
										),
									)
								),
								"getSession"		=> array(
									"parts"		=> array(
										"default"	=> array(
											"useController"	=> "Admin",
											"useAction"		=> "e",
										),
									)
								),
								"admin"		=> array(
									"access"	=> array(
										"permissions"		=> array(
											PERMISSIONS_ADMIN_ACCESS
										),
										"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
									),
									"parts"		=> array(
                                        "getScope" => array(
                                            "access"	=> array(
                                                "onlyAJAX"      => true,
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "isAJAX"		=> array(
                                                        "useController"	=> "Permissions",
                                                        "useAction"		=> "e",
                                                    )
                                                )
                                            )
                                        ),
                                        "data" => array(
                                            "access"	=> array(
                                                "onlyAJAX"      => true,
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "isAJAX"		=> array(
                                                        "useController"	=> "Accounts",
                                                        "useAction"		=> "n",
                                                    )
                                                )
                                            )
                                        ),
                                        "getPermissions" => array(
                                            "access"	=> array(
                                                "onlyAJAX"      => true,
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "isAJAX"		=> array(
                                                        "useController"	=> "Permissions",
                                                        "useAction"		=> "f",
                                                    )
                                                )
                                            )
                                        ),
										"users"	=> array(
											"access"	=> array(
												"permissions"		=> array(
                                                    PERMISSIONS_USERS_ADMIN_ACCESS
                                                ),
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Accounts"
												),
												"types"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "AccountsTypes"
														)
													)
												),
											)
										),
//Парнтеры::НАЧАЛО
										"partners"	=> array(
											"access"	=> array(
												"permissions"		=> array(
													PERMISSIONS_ADMIN_ACCESS,
													PERMISSIONS_PARTNERS_ADMIN_ACCESS
												),
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Partners",
												),
												"types"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "PartnersTypes"
														)
													)
												),
												"csv"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "Partners",
															"useAction"		=> "p",
															"Convert"		=> array(array("Converter", "ArrayToCSV")),
															"fileOriginalName"		=> "Партнеры.csv",
															"OutCharSet"	=> CHARSET_WIN1251,
															"SendType"		=> SEND_TYPE_UPLOAD,
														)
													)
												)
											)
										),
//Парнтеры::КОНЕЦ


                                        "permissions"	=> array(
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "isAJAX"		=> array(
                                                        "SendType"		=> SEND_TYPE_JSON
                                                    ),
                                                    "isNotAJAX"		=> array(
                                                        //"SendType"		=> SEND_TYPE_HTML
                                                        "SendType"		=> SEND_TYPE_JSON
                                                    )
                                                ),
                                            )
                                        ),


//Роли::НАЧАЛО
										"roles"	=> array(
											"access"	=> array(
												"permissions"		=> array(
													PERMISSIONS_ADMIN_ACCESS,
													PERMISSIONS_ROLES_ADMIN_ACCESS
												),
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Roles"
												)
											)
										),
//Роли::КОНЕЦ

//Акции::НАЧАЛО
										/**
										 * Акции
										 */
										"carorders"	=> array(
											"access" => array(
												"onlyAJAX" => true
											),
											"parts"		=> array(
												"empty"	=> array(
													"useArgument"	=> "partnerId",
													"useController"	=> "Partners",
													"useAction"		=> "f",

													"isAJAX"		=> array(
														"SendType"	=> SEND_TYPE_JSON
													)
												)
											)
										),
										"testdrives"	=> array(
											"access"	=> array(
												"permissions"		=> array(
													PERMISSIONS_TESTDIVESORDERS_ADMIN_ACCESS
												),
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION)
											),
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "FacilitiesForms"
												)
											)
										),
																				/**
										 * Акции
										 */
										"orders"	=> array(
											"access" => array(
												"onlyAJAX" => true
											),
											"parts"		=> array(
												"empty"	=> array(
													"useArgument"	=> "partnerId",
													"useController"	=> "Partners",
													"useAction"		=> "f",

													"isAJAX"		=> array(
														"SendType"	=> SEND_TYPE_JSON
													)
												)
											)
										),


										"scheduler"	=> array(
											"access"	=> array(
												"permissions"		=> array(
                                                    PERMISSIONS_USERS_ADMIN_ACCESS
                                                ),
												"onlyAutorized"	=> array(WSCONFIG_ACTIONS_AUTORIZATION),
												"onlyAJAX" => true
											),
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "SchedulerUsers"
												),
												//Получить расписание
												"getTimeTables"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "SchedulerUsers",
															"useAction"		=> "a"
														),
													)
												),
												"getModelsTimeTables"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "SchedulerUsers",
															"useAction"		=> "h"
														),
													)
												),
												"getTimeTablesForModel"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "SchedulerUsers",
															"useAction"		=> "h"
														),
													)
												),
											)
										),


										"facilities"	=> array(

											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Facilities"
												),
												"forms"	=> array(
													"parts"		=> array(
														"default"	=> array(
															"useController"	=> "FacilitiesForms"
														),
														"csv"	=> array(
                                                            "parts"		=> array(
                                                                "default"	=> array(
                                                                    "useController"	=> "FacilitiesForms",
                                                                    "useAction"		=> "h",
                                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                                    "fileOriginalName"		=> "Otchet_o_zayavkah.csv",
                                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                                    "SendType"		=> SEND_TYPE_UPLOAD,
																	/*
																	"debugSets"		=> array(
																		"enabled"		=> true
																	)
																	 */
                                                                )
                                                            )
														),
													)
												),
											)
										),
//Акции::КОНЕЦ
                                        /**
										 * Акции
										 */
										"cars"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Cars"
												)
											)
										),
										"models"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Models"
												)
											)
										),
										"tradein"	=> array(
                                            "parts"		=> array(
                                               "evaluation"	=> array(
                                                    "parts"		=> array(
                                                        "default"	=> array(
															"useController"	=> "CarValuations",
                                                        ),
                                                        "csv"	=> array(
                                                            "parts"		=> array(
                                                                "default"	=> array(
                                                                    "useController"	=> "CarValuations",
                                                                    "useAction"		=> "f",
                                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                                    "fileOriginalName"		=> "Заявки_на_оценку.csv",
                                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                                    "SendType"		=> SEND_TYPE_UPLOAD,
                                                                )
                                                            )
														),
														"all_csv"	=> array(
                                                            "parts"		=> array(
                                                                "default"	=> array(
                                                                    "useController"	=> "CarValuations",
                                                                    "useAction"		=> "i",
                                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                                    "fileOriginalName"		=> "Все_заявки_на_оценку_за_весь_период.csv",
                                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                                    "SendType"		=> SEND_TYPE_UPLOAD,
                                                                )
                                                            )
														)
                                                    )
                                                ),
                                                "default"	=> array(
                                                )
                                            )
                                        ),
                                        "techservice"	=> array(
                                            "parts"		=> array(
                                                "users_actions"	=> array(
                                                    "parts"		=> array(
                                                        "default"	=> array(
                                                            "dataKey"		=> "admin",
                                                            "useTemplater"	=> "Templater2",
                                                            "isNotAJAX"		=> array(
                                                                "template" => PATH_TEMPLATES."admin/_techservice.html",
                                                                "data"			=> array(
                                                                    "menu"		=> array("exec", array("AdminProcessor", "GetServicesActions"), 23),
                                                                ),
                                                                "SendType"		=> SEND_TYPE_HTML,
                                                            )
                                                        )
                                                    )
                                                ),
                                               "members"	=> array(
                                                    "parts"		=> array(
                                                        "default"	=> array(
                                                            "dataKey"		=> "admin",
                                                            "useTemplater"	=> "Templater2",
                                                            "isNotAJAX"		=> array(
                                                                "template" => PATH_TEMPLATES."admin/_techserviceMembers_new.html",
                                                                "data"			=> array(
                                                                    "menu"		=> array("exec", array("AdminProcessor", "GetServicesActions"), 23),
                                                                ),
                                                                "SendType"		=> SEND_TYPE_HTML,
                                                            )
                                                        ),
                                                        "csv"	=> array(
                                                            "parts"		=> array(
                                                                "default"	=> array(
                                                                    "useController"	=> "TO",
                                                                    "useAction"		=> "o",
                                                                    //"dataKey"		=> "members",
                                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                                    "fileOriginalName"		=> "Записавшиеся_на_техобслуживание.csv",
                                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                                    "SendType"		=> SEND_TYPE_UPLOAD,
                                                                )
                                                            )
														),
                                                    )
                                                ),
                                                "default"	=> array(
                                                    "dataKey"		=> "admin",
                                                    "useTemplater"	=> "Templater2",
                                                    "isNotAJAX"		=> array(
                                                        "template" => PATH_TEMPLATES."admin/_techservice.html",
                                                        "data"			=> array(
															"menu"		=> array("exec", array("AdminProcessor", "GetServicesActions"), 23),
														),
														"SendType"		=> SEND_TYPE_HTML,
                                                    )
                                                )
                                            )
                                        ),

										"mias_members_csv"	=> array(
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "useController"	=> "Mias",
                                                    "useAction"		=> "x",
                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                    "SendType"		=> SEND_TYPE_UPLOAD,
                                                )
                                            )
                                        ),
										"mias_test_drives_members_csv"	=> array(
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "useController"	=> "Mias",
                                                    "useAction"		=> "x1",
                                                    //"dataKey"		=> "members",
                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                    //"fileOriginalName"		=> "Записавшиеся_на_техобслуживание.csv",
                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                    "SendType"		=> SEND_TYPE_UPLOAD,
                                                )
                                            )
                                        ),

                                        "action_members_csv"	=> array(
                                            "parts"		=> array(
                                                "default"	=> array(
                                                    "useController"	=> "CustomActions",
                                                    "useAction"		=> "x",
                                                    //"dataKey"		=> "members",
                                                    "Convert"		=> array(array("Converter", "ArrayToCSV")),
                                                    //"fileOriginalName"		=> "Записавшиеся_на_техобслуживание.csv",
                                                    "OutCharSet"	=> CHARSET_WIN1251,
                                                    "SendType"		=> SEND_TYPE_UPLOAD,
                                                )
                                            )
                                        ),
										"default"	=> array(
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."admin/adminsNew2.html",
												"SendType"		=> SEND_TYPE_HTML,
											)
										),
									)
								),

/*КОНЕЦ АДМИНКИ*/




								/**
								 * Запись на тест-драйв
								 */
								"testdrives"	=> array(
									"parts"	=> array(
										"info"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"SendType"		=> SEND_TYPE_JSON
												)
											)
										),
										"default"	=> array(
											"useArgument"	=> "modelId",
											"useController"	=> "CustomActions",
											"useAction"		=> "o",
											"dataKey"		=> "info",
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."testdrive.html",
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),
                                /**
								 * Запись на тест-драйв
								 */
								"testdrives_mias"	=> array(
									"parts"	=> array(
										"info"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"SendType"		=> SEND_TYPE_JSON
												)
											)
										),
										"default"	=> array(
											"useArgument"	=> "modelId",
											"useController"	=> "CustomActions",
											"useAction"		=> "o",
											"dataKey"		=> "info",
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."testdrive_mias.html",
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),
                                /**
								 * Запись на тест-драйв
								 */
								"testdrive_test"	=> array(
									"parts"	=> array(
										"info"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"SendType"		=> SEND_TYPE_JSON
												)
											)
										),
										"default"	=> array(
											"useArgument"	=> "modelId",
											"useController"	=> "CustomActions",
											"useAction"		=> "o",
											"dataKey"		=> "info",
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."testdrive_test.html",
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),
								/**
								 * Запись на тест-драйв
								 */
								"testdrives_t"	=> array(
									"parts"	=> array(
										"info"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"SendType"		=> SEND_TYPE_JSON
												)
											)
										),
										"default"	=> array(
											"useArgument"	=> "modelId",
											"useController"	=> "CustomActions",
											"useAction"		=> "o",
											"dataKey"		=> "info",
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."testdrive_t.html",
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),
								/**
								 * Предварительный заказ
								 */
								"spec_orders"	=> array(
									"parts"	=> array(
										"access" => array(
											"headers"	=> array(
												"X-LEXUS" => array("value", "a18Y04o3120T21")
											),
										),
										"info"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"SendType"		=> SEND_TYPE_JSON
												)
											)
										),
										"default"	=> array(
											"useArgument"	=> "modelId",
											"useController"	=> "CustomActions",
											"useAction"		=> "n",
											"setInDataTypeAsAJAX"		=> true,
											"SendType"		=> SEND_TYPE_JSON
										)
									)
								),
								/**
								 * Test :: Запись на тест-драйв
								 */
								"test"	=> array(
									"parts"	=> array(
										"default"	=> array(
											"useController"	=> "Dealers",
											"useAction"		=> "c1",
										)
									)
								),

								"findPage"	=> array(
									"parts"		=> array(
										"default"	=> array(
											"useController"	=> "Dealers",
											"useAction"		=> "k",
											"dataKey"		=> "dealers",
											"useTemplater"	=> "Templater2",
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."findDealers.html",
												"data"			=> array(
													"facilities"			=> array("exec", array("FacilitiesProcessor", "GetFacilitiesAsFilter")),
													"cities"				=> array("exec", array("GeoProcessor", "GetDealersCities")),
													"dealersFacilities"		=> array("exec", array("DealersProcessor", "GetRelatedFacilitiesNT")),
													//"title"			=> array("value", "Система администитрирование портала Toyota")
												),
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),

								"facilities"	=> array(
									"headers" => array("Access-Control-Allow-Origin: *"),
									"parts"		=> array(
										"dealers_related" => array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Dealers",
													"useAction"		=> "h",
													"useArgument"	=> "dealersId",
													"dataKey"		=> "data"
												)
											)
										),
										"dealers_not_related" => array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Dealers",
													"useAction"		=> "i",
													"useArgument"	=> "dealersId",
													"dataKey"		=> "data"
												)
											)
										),
										"default"	=> array(
											"useController"	=> "Facilities",
											"useAction"		=> "a",
											"dataKey"		=> "data"
										)
									)
								),


                                /**
                                 * ВНЕШНЕЕ API
                                 */
                                "geo" => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        /**
                                         * Получить список всех стран или поиск страны.
                                         */
                                        "countries"              => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "a",
                                                    "dataKey"       => "data",
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список стран, в которых есть дилеры
                                         */
                                        "dealerCountries"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "b",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список  регионов для заданной страны, поиск региона по строке
                                         */
                                        "regions"                => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "c",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "countryId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
                                         */
                                        "dealerRegions"          => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "d",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "countryId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список всех городов для заданного региона, поиск города по строке
                                         */
                                        "cities"                 => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "e",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список городов, в которых есть дилеры для заданной страны или региона, поиск города, в котором есть дилеры по строке
                                         */
                                        "dealerCities"           => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "f",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        "action"                 => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "CustomActions",
                                                    "useAction"     => "a1",
                                                    "dataKey"       => "data",
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список ближайших к пользователю городов, в которых есть дилеры
                                         */
                                        "nearcity"               => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "g",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список федеральных округов
                                         */
                                        "federalDistrict"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "h",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список федеральных округов, в которых есть дилеры
                                         */
                                        "dealersFederalDistrict" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "i",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        "default"                => array(
                                            "dataKey" => "data"
                                        )
                                    )
                                ),
                                "dealers"    => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "nearest"  => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "b",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "types"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "f",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "findPage" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "b",
                                                    "dataKey"       => "data",
                                                    "isNotAJAX" => array(
                                                        "template" => PATH_TEMPLATES."findDealers.html",
                                                        "data"     => array(
                                                            "facilities" => array("exec", array("FacilitiesProcessor", "GetFacilitiesAsFilter")),
                                                            "cities"     => array("exec", array("GeoProcessor", "GetCities")),
                                                        //"title"			=> array("value", "Система администитрирование портала Toyota")
                                                        ),
                                                        "SendType" => SEND_TYPE_HTML,
                                                    )
                                                )
                                            )
                                        ),
                                        "ffind"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "j",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "cityId",
                                                    "SendType"      => SEND_TYPE_JSON,
                                                )
                                            )
                                        ),
                                        "action"   => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useAction"     => "a2",
                                                    "useController" => "CustomActions",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "default"  => array(
                                            "dataKey"       => "data",
                                            "useController" => "Dealers",
                                            "useAction"     => "a",
                                            "useArgument"   => "cityId"
                                        )
                                    )
                                ),
                                "partners"   => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "types" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Partners",
                                                    "useAction"     => "f",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "get"   => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Partners",
                                                    "useAction"     => "d",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "partnerId"
                                                )
                                            )
                                        ),
										"new"   => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Partners",
                                                    "useAction"     => "o",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "partnerId"
                                                )
                                            )
                                        ),
                                        "default" => array(
                                            "dataKey"       => "data",
                                            "useController" => "Partners",
                                            "useAction"     => "f",
                                            "useArgument"   => "cityId"
                                        )
                                    )
                                ),
                                "cars"       => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "manufacturers" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Cars",
                                                    "useAction"     => "a",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "models"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Cars",
                                                    "useAction"     => "b",
                                                    "dataKey"       => "data",
                                                    "useArgument"   => "manufacturerId"
                                                )
                                            )
                                        ),
                                        "submodels"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Models",
                                                    "useAction"     => "b",
                                                    "dataKey"       => "data",
                                                )
                                            )
                                        ),
                                        "action"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "CustomActions",
                                                    "useAction"     => "a3",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "default"       => array(
                                            "dataKey"       => "data",
                                            "useController" => "Models",
                                            "useAction"     => "a",
                                        )
                                    )
                                ),
                                "facilities" => array(
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "dealers_related"     => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "h",
                                                    "useArgument"   => "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        "dealers_not_related" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "i",
                                                    "useArgument"   => "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
										"forms" => array(
											"parts"		=> array(
												"default"	=> array(
													 "isAJAX"		=> array(
                                                        "SendType"		=> SEND_TYPE_JSON
                                                    )
												)
											)
										),
                                        "default"             => array(
                                            "useController" => "Facilities",
                                            "useAction"     => "a",
                                            "dataKey"       => "data"
                                        )
                                    )
                                ),
                                "techservice" => array(
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        //Возвращает список брендов
                                        "brands"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "a",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список моделей
                                        "models"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "b",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список Типов обслуживания
                                        "servicesTypes" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "d",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список действий пользователя
                                        "usersActions"  => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "e",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список действий пользователя
                                        "usersActions2" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "e2",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает модель по VIN
                                        "checkVin"      => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "f",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Записывает пользователя на техобслуживание
                                        "register"      => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "g",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
										 //Возвращает список действий пользователя
										"getDealersData" => array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "TO",
													"useAction"		=> "a2",
													"dataKey"		=> "data"
												)
											)
										),
                                        //Записывает пользователя на техобслуживание
                                        "to"            => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."to_record_v3.html"),
                                                )
                                            )
                                        ),
										//Записывает пользователя на техобслуживание
                                        "to-test"            => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."to_record_v3.html"),
                                                )
                                            )
                                        ),
                                         //Записывает пользователя на техобслуживание
                                        "mias" => array(
											"parts"		=> array(
												"default"	=> array(
                                                    "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."to_record_mias.html"),
                                                )
											)
										),
                                        "default"       => array(
                                            "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."to_record.html"),
                                        )
                                    )
                                ),
                                "techservice2" => array(
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        //Возвращает список брендов
                                        "brands"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "a",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список моделей
                                        "models"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "b",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список Типов обслуживания
                                        "servicesTypes" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "d",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список действий пользователя
                                        "usersActions"  => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "e",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает список действий пользователя
                                        "usersActions2" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "e2",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Возвращает модель по VIN
                                        "checkVin"      => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "f",
                                                    //"useArgument"	=> "dealersId",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Записывает пользователя на техобслуживание
                                        "register"      => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "TO",
                                                    "useAction"     => "g",
                                                    "dataKey"       => "data"
                                                )
                                            )
                                        ),
                                        //Записывает пользователя на техобслуживание
                                        "to"            => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."to_record_v2.html"),
                                                )
                                            )
                                        ),
                                        "default"       => array(
                                            "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."to_record.html"),
                                        )
                                    )
                                ),
                                /**
                                 * Запись на тест-драйв
                                 */
                                "td-s_8-156548-0sad"   => array(//
                                    "access" => array(
                                        "headers" => array(
                                            "X-V3D9ODC0MSZOCWNKL" => array("value", "0NFEB1YUMQOCLU9KYR1")
                                        ),
                                    ),
                                    "parts"  => array(
                                        "info"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "SendType" => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
                                        "default" => array(
                                            "isNotAJAX" => array(
                                                "useArgument"   => "modelId",
                                                "useController" => "CustomActions",
                                                "useAction"     => "o",
                                                "dataKey"       => "info",
                                                "template"      => PATH_TEMPLATES."testdrive.html",
                                                "SendType"      => SEND_TYPE_HTML,
                                            )
                                        )
                                    )
                                ),
                                /**
                                 * Запись на тест-драйв
                                 */
                                "td-s_8-156548-0sadie" => array(//
                                    "access" => array(
                                    ),
                                    "parts"  => array(
                                        "info"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "SendType" => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
                                        "default" => array(
                                            "useArgument"   => "modelId",
                                            "useController" => "CustomActions",
                                            "useAction"     => "o",
                                            "dataKey"       => "info",
                                            "isNotAJAX"     => array(
                                                "template" => PATH_TEMPLATES."testdrive.html",
                                                "SendType" => SEND_TYPE_HTML,
                                            )
                                        )
                                    )
                                ),
                                /**
								 * Запись на тест-драйв
								 */
								"testdrives"	=> array(
									"parts"	=> array(
										"default"	=> array(
											"isAJAX"		=> array(
												"useController"	=> "CustomActions",
                                                "useAction"		=> "l"
											),
                                            "isNotAJAX"		=> array(
                                                "dataKey"		=> "info",
                                                "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."testdrive.html"),
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),
                                /**
								 * Запись на тест-драйв
								 */
								"testdrives_test"	=> array(
									"parts"	=> array(
										"default"	=> array(
											"isAJAX"		=> array(
												"useController"	=> "CustomActions",
                                                "useAction"		=> "l"
											),
                                            "isNotAJAX"		=> array(
                                                "dataKey"		=> "info",
                                                "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."testdrive-test.html"),
												"SendType"		=> SEND_TYPE_HTML,
											)
										)
									)
								),
                                /**
                                 * Предварительный заказ
                                 */
                                "orders" => array(
                                    "parts" => array(
                                        "info"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "SendType" => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
                                        "call"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"useArgument"	=> "modelId",
                                                    "useController"	=> "CustomActions",
                                                    "useAction"		=> "p",
                                                    "dataKey"		=> "info",
                                                    "isNotAJAX"		=> array(
                                                        "template"		=> PATH_TEMPLATES."orders_call.html",
                                                        "SendType"		=> SEND_TYPE_HTML,
                                                    )
												)
											)
										),
                                        "call2"	=> array(
											"parts"		=> array(
												"default"	=> array(
													"useArgument"	=> "modelId",
                                                    "useController"	=> "CustomActions",
                                                    "useAction"		=> "p",
                                                    "dataKey"		=> "info",
                                                    "isNotAJAX"		=> array(
                                                        "template"		=> PATH_TEMPLATES."orders_orders_call.html",
                                                        "SendType"		=> SEND_TYPE_HTML,
                                                    )
												)
											)
										),
                                        "default" => array(
                                            "isNotAJAX"     => array(
                                                "template" => PATH_TEMPLATES."orders.html",
                                                "SendType" => SEND_TYPE_HTML,
                                            )
                                        )
                                    )
                                ),
                                /**
                                 * Предварительный заказ
                                 */
                                "spec_orders" => array(
                                    "parts" => array(
                                        "access"  => array(
                                            "headers" => array(
                                                "X-TOYOTA" => array("value", "a18Y04o3120T21")
                                            ),
                                        ),
                                        "info"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "SendType" => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
                                        "default" => array(
                                            "useArgument"         => "modelId",
                                            "useController"       => "CustomActions",
                                            "useAction"           => "n",
                                            "setInDataTypeAsAJAX" => true,
                                            "SendType"            => SEND_TYPE_JSON
                                        )
                                    )
                                ),
                                /**
                                 * Test :: Запись на тест-драйв
                                 */
                                "test"        => array(
                                    "parts" => array(
                                        "default" => array(
                                            "useController" => "Dealers",
                                            "useAction"     => "c1",
                                        )
                                    )
                                ),
                                "findPage" => array(
                                    "parts" => array(
                                        "default" => array(
                                            "useController" => "Dealers",
                                            "useAction"     => "c",
                                            "dataKey"       => "dealers",
                                            "useTemplater"  => "Templater2",
                                            "isNotAJAX"     => array(
                                                "template" => PATH_TEMPLATES."findDealers.html",
                                                "data"     => array(
                                                    "facilities"        => array("exec", array("FacilitiesProcessor", "GetFacilitiesAsFilter")),
                                                    "cities"            => array("exec", array("GeoProcessor", "GetDealersCities")),
                                                    "dealersFacilities" => array("exec", array("DealersProcessor", "GetRelatedFacilitiesNT")),
                                                //"nearestDealer"			=> array("exec", array("DealersProcessor", "GetRelatedFacilities"))
                                                //"title"			=> array("value", "Система администитрирование портала Toyota")
                                                ),
                                                "SendType" => SEND_TYPE_HTML,
                                            )
                                        )
                                    )
                                ),
								"findPage-test" => array(
                                    "parts" => array(
                                        "default" => array(
                                            "useController" => "Dealers",
                                            "useAction"     => "c",
                                            "dataKey"       => "dealers",
                                            "useTemplater"  => "Templater2",
                                            "isNotAJAX"     => array(
                                                "template" => PATH_TEMPLATES."findDealers-test.html",
                                                "data"     => array(
                                                    "facilities"        => array("exec", array("FacilitiesProcessor", "GetFacilitiesAsFilter")),
                                                    "cities"            => array("exec", array("GeoProcessor", "GetDealersCities")),
                                                    "dealersFacilities" => array("exec", array("DealersProcessor", "GetRelatedFacilities")),
                                                //"nearestDealer"			=> array("exec", array("DealersProcessor", "GetRelatedFacilities"))
                                                //"title"			=> array("value", "Система администитрирование портала Toyota")
                                                ),
                                                "SendType" => SEND_TYPE_HTML,
                                            )
                                        )
                                    )
                                ),
								"findPage2" => array(
                                    "parts" => array(
                                        "default" => array(
                                            "useController" => "Dealers",
                                            "useAction"     => "c",
                                            "dataKey"       => "dealers",
                                            "useTemplater"  => "Templater2",
                                            "isNotAJAX"     => array(
                                                "template" => PATH_TEMPLATES."findDealers2.html",
                                                "data"     => array(
                                                    "facilities"        => array("exec", array("FacilitiesProcessor", "GetFacilitiesAsFilter2")),
                                                    "cities"            => array("exec", array("GeoProcessor", "GetDealersCities")),
                                                    "dealersFacilities" => array("exec", array("DealersProcessor", "GetRelatedFacilities2")),
                                                //"nearestDealer"			=> array("exec", array("DealersProcessor", "GetRelatedFacilities"))
                                                //"title"			=> array("value", "Система администитрирование портала Toyota")
                                                ),
                                                "SendType" => SEND_TYPE_HTML,
                                            )
                                        )
                                    )
                                ),



								/**
                                 * ВНЕШНЕЕ API
                                 */
                                "geo_n" => array(
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        /**
                                         * Получить список всех стран или поиск страны.
                                         */
                                        "countries"              => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "a0"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список стран, в которых есть дилеры
                                         */
                                        "dealerCountries"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "b0"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список  регионов для заданной страны, поиск региона по строке
                                         */
                                        "regions"                => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "c0",
                                                    "useArgument"   => "countryId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
                                         */
                                        "dealerRegions"          => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "d0",
                                                    "useArgument"   => "countryId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список всех городов для заданного региона, поиск города по строке
                                         */
                                        "cities"                 => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "e",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список городов, в которых есть дилеры для заданной страны или региона, поиск города, в котором есть дилеры по строке
                                         */
                                        "dealerCities"           => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "f0",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список ближайших к пользователю городов, в которых есть дилеры
                                         */
                                        "nearcity"               => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "g0"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список федеральных округов
                                         */
                                        "federalDistrict"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "h0",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        /**
                                         * Получить список федеральных округов, в которых есть дилеры
                                         */
                                        "dealersFederalDistrict" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Geo",
                                                    "useAction"     => "i0",
                                                    "useArgument"   => "regionId"
                                                )
                                            )
                                        ),
                                        "default"                => array(
                                            "dataKey" => "data"
                                        )
                                    )
                                ),
                                "dealers_n"    => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "nearest"  => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "b0"
                                                )
                                            )
                                        ),
                                        "types"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "f0"
                                                )
                                            )
                                        ),
                                        "findPage" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "b0",
                                                    "isNotAJAX" => array(
                                                        "template" => PATH_TEMPLATES."findDealers.html",
                                                        "data"     => array(
                                                            "facilities" => array("exec", array("FacilitiesProcessor", "GetFacilitiesAsFilter")),
                                                            "cities"     => array("exec", array("GeoProcessor", "GetCities")),
                                                        //"title"			=> array("value", "Система администитрирование портала Toyota")
                                                        ),
                                                        "SendType" => SEND_TYPE_HTML,
                                                    )
                                                )
                                            )
                                        ),
                                        "ffind"    => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "j0",
                                                    "useArgument"   => "cityId",
                                                    "SendType"      => SEND_TYPE_JSON,
                                                )
                                            )
                                        ),
                                        "default"  => array(
                                            "useController" => "Dealers",
                                            "useAction"     => "a",
                                            "useArgument"   => "cityId"
                                        )
                                    )
                                ),
                                "partners_n"   => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "types" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Partners",
                                                    "useAction"     => "f0"
                                                )
                                            )
                                        ),
                                        "get"   => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Partners",
                                                    "useAction"     => "d0",
                                                    "useArgument"   => "partnerId"
                                                )
                                            )
                                        ),
										"new"   => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Partners",
                                                    "useAction"     => "o",
                                                    "useArgument"   => "partnerId"
                                                )
                                            )
                                        ),
                                        "default" => array(
                                            "useController" => "Partners",
                                            "useAction"     => "f0",
                                            "useArgument"   => "cityId"
                                        )
                                    )
                                ),
                                "cars_n"       => array(
                                    /*
                                      "access"	=> array(
                                      "onlyAJAX"			=> true,
                                      ),
                                     */
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "manufacturers" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Cars",
                                                    "useAction"     => "a"
                                                )
                                            )
                                        ),
                                        "other_models"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Cars",
                                                    "useAction"     => "b",
                                                    "useArgument"   => "manufacturerId"
                                                )
                                            )
                                        ),
                                        "models"        => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Models",
                                                    "useAction"     => "b0"
                                                )
                                            )
                                        ),
                                        "default"       => array(
                                            "useController" => "Models",
                                            "useAction"     => "a",
                                        )
                                    )
                                ),
                                "facilities_n" => array(
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "dealers_related"     => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "h",
                                                    "useArgument"   => "dealersId"
                                                )
                                            )
                                        ),
                                        "dealers_not_related" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "Dealers",
                                                    "useAction"     => "i",
                                                    "useArgument"   => "dealersId"
                                                )
                                            )
                                        ),
										"getFullData" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "FacilitiesForms",
                                                    "useAction"     => "e"
                                                )
                                            )
                                        ),
                                        "default"             => array(
                                            "useController" => "Facilities",
                                            "useAction"     => "a0"
                                        ),
										"contactingThemes" => array(
                                            "parts" => array(
                                                "default" => array(
                                                    "useController" => "FacilitiesForms",
                                                    "useAction"     => "f"
                                                )
                                            )
                                        ),
										"dealersFormsSets" => array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "FacilitiesForms",
													"useAction"		=> "a"
												)
											)
										),
										"getDealersFormsSetsNew" => array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "FacilitiesForms",
													"useAction"		=> "a"
												)
											)
										),
										"dealersState" => array(
											"parts"		=> array(
												"default"	=> array(
													"useController"	=> "Facilities",
													"useAction"		=> "c",
													"dataKey"		=> "data"
												)
											)
										)
                                    )
                                ),
								"fforms" => array(
                                    "headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"   => array(
                                        "default"             => array(
                                            "useController" => "FacilitiesForms",
											"debugSets"		=> array(
												"enabled"		=> true
											)
                                        )
                                    )
                                ),






                                "source_info"	=> array(
									"parts"		=> array(
										"default"	=> array(
											"useController"	=> "CustomActions",
											"useAction"		=> "r",
											//"dataKey"		=> "dealers",
											"isAJAX"		=> array(
												"SendType"	=> SEND_TYPE_JSON
											)
										)
									)
								),
								"__users-events_951-for-driver"	=> array(
									"access" => array(
										"headers"	=> array(
											USERS_EVENTS_DRIVER_HEADER_NAME => array("value", USERS_EVENTS_DRIVER_HEADER_VALUE)
										),
										"onlyAJAX" => true
									),
									"parts"	=> array(
										"empty"	=> array(
											"SendType"		=> SEND_TYPE_JSON,
										)
									)
								),
                                "nx_contest"	=> array(
									"parts"	=> array(
										"empty"	=> array(
											"SendType"		=> SEND_TYPE_JSON,
										)
									)
								),
								"captcha"	=> array(
									"parts"	=> array(
										"default"	=> array(
											"useController"	=> "Captcha",
											"useAction"		=> "c",
											"SendType"		=> SEND_TYPE_BINARY
										)
									)
								),
								"check_captcha"	=> array(
									"parts"	=> array(
										"default"	=> array(
											"useController"	=> "Captcha",
											"useAction"		=> "a",
											"SendType"		=> SEND_TYPE_JSON
										)
									)
								),
                                "actions"	=> array(
                                    //"headers" => array("Access-Control-Allow-Origin: *"),
                                    "parts"		=> array(
                                        "default"	=> array(
                                            "dataKey"		=> "data",
                                            "useController"	=> "CustomActions",
                                            "dataKey"		=> "data",
                                            "useAction"		=> "a0",
                                        )
                                    )
                                ),
								"valuation-test"	=> array(
									"parts"		=> array(
										"default"	=> array(
											//"useTemplater"	=> "Templater2",
											"isNotAJAX"		=> array(
												"template"		=> PATH_TEMPLATES."Evaluation2-test.html",
												"SendType"		=> SEND_TYPE_HTML,
											),
											"isAJAX"		=> array(
												"SendType"		=> SEND_TYPE_JSON,
											),
										),
										"tmpl"		=> array(
											"parts"		=> array(
												"default"	=> array(
													"template"		=> PATH_TEMPLATES."Evaluation2-test.html",
												)
											)
										),
										"photo"		=> array(
											"parts"		=> array(
												"default"	=> array(
													"SendType"		=> SEND_TYPE_JSON,
												)
											)
										)
									)
								),
                                "mias"      => array(
                                    "parts" => array(
                                        "default" => array(
                                            "useController" => "Mias",
                                            //"useAction"     => "forms",
                                            "dataKey"       => "data"
                                        )
                                    )
                                ),
                                "check_order"      => array(
                                    "parts" => array(
                                        "default" => array(
                                            "isAJAX"		=> array(
                                                "useController" => "CarOrdersState",
                                                "useAction"     => "a"
                                            ),
                                            "isNotAJAX"		=> array(
                                                "template"		=> PATH_TEMPLATES."checkOrderStatus.html",
                                                "SendType"		=> SEND_TYPE_HTML
                                            )
                                        )
                                    )
                                ),
								"clients"	=> array(
                                    "access"	=> array(
										"onlyAJAX"			=> true,
									),
									"parts"	=> array(
                                        "unregister"	=> array(
                                            "parts"	=> array(
                                                "default"	=> array(
                                                    "useController"		=> "Clients",
                                                    "useAction"			=> "b",
                                                    //"addArgument"       => array(array("cuid", "cookies", "__u_cl_u753")),
                                                    "SendType"      	=> SEND_TYPE_JSON
                                                )
                                            )
                                        ),
										"default"	=> array(
											"useController"		=> "Clients",
											"useAction"			=> "a",
											"SendType"      	=> SEND_TYPE_JSON
										)
									)
								),
								"uevents"	=> array(
									"access" => array(
										"headers"	=> array(
											USERS_EVENTS_DRIVER_HEADER_NAME => array("value", USERS_EVENTS_DRIVER_HEADER_VALUE)
										),
										"onlyAJAX" => true
									),
									"parts"	=> array(
										"default"	=> array(
											"SendType"		=> SEND_TYPE_JSON,
										)
									)
								),

								/**
								 * Предварительный заказ
								 */
								"___re_send_test_drives_members___"	=> array(
									"parts"	=> array(
										"default"	=> array(
											"useArgument"	=> "code",
											"useController"	=> "CustomActions",
											"useAction"		=> "z100",
											"setInDataTypeAsAJAX"		=> true,
											"SendType"		=> SEND_TYPE_JSON
										)
									)
								),

								"forms"	=> array(
									"parts"	=> array(
										"testdrive"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"isNotAJAX"		=> array(
														"template"		=> PATH_TEMPLATES."Lexus_Test_Drive.html",
														"SendType"		=> SEND_TYPE_HTML,
													)
												)
											)
										),
										"to"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"isNotAJAX"		=> array(
														"template"		=> PATH_TEMPLATES."Lexus_TO.html",
														"SendType"		=> SEND_TYPE_HTML,
													)
												)
											)
										),
										"to-test"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"isNotAJAX"		=> array(
														"template"		=> PATH_TEMPLATES."Lexus_TO-test.html",
														"SendType"		=> SEND_TYPE_HTML,
													)
												)
											)
										),
										"eval"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"template"		=> PATH_TEMPLATES."Lexus_Trade.html",
													"SendType"		=> SEND_TYPE_HTML,
												)
											)
										),
										"eval-test"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"template"		=> PATH_TEMPLATES."Lexus_Trade-test.html",
													"SendType"		=> SEND_TYPE_HTML,
												)
											)
										),
										"subscription"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"template"		=> PATH_TEMPLATES."News_Subscriptions.html",
													"SendType"		=> SEND_TYPE_HTML,
												)
											)
										),
										"support"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"template"		=> PATH_TEMPLATES."Support.html",
													"SendType"		=> SEND_TYPE_HTML,
												)
											)
										),
										"callback"	=> array(
											"parts"	=> array(
												"default"	=> array(
													"template"		=> PATH_TEMPLATES."Callback.html",
													"SendType"		=> SEND_TYPE_HTML,
												)
											)
										),
									)
								),
								/**
								 * Подписка
								 */
								"subscribe"	=> array(
									"parts"	=> array(
                                        "themes"	=> array(
                                            "parts"	=> array(
                                                "default"	=> array(
                                                    "useController"		=> "UsersSubscriptions",
                                                    "useAction"			=> "c",
                                                    "SendType"          => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
                                        "cabinet"	=> array(
                                            "parts"	=> array(
                                                "auth"	=> array(
                                                    "parts"	=> array(
                                                        "default"	=> array(
                                                            "isAJAX"		=> array(
                                                                "SendType"          => SEND_TYPE_JSON
                                                            ),
                                                            "isNotAJAX"		=> array(
                                                                "useController"		=> "UsersSubscriptions",
                                                                "useAction"			=> "d",
                                                                "template"          => PATH_TEMPLATES."subsciption/subscribeCabinetAuth.html",
                                                                "SendType"          => SEND_TYPE_HTML
                                                            )
                                                        )
                                                    )
                                                ),
                                                "exit"	=> array(
                                                    "parts"	=> array(
                                                        "default"	=> array(
                                                            "useController"		=> "UsersSubscriptions",
                                                            "useAction"			=> "i",
                                                            "SendType"          => SEND_TYPE_JSON
                                                        )
                                                    )
                                                ),
                                                "data"	=> array(
                                                    "parts"	=> array(
                                                        "default"	=> array(
                                                            "useController"		=> "UsersSubscriptions",
                                                            "useAction"			=> "e",
                                                            "SendType"          => SEND_TYPE_JSON
                                                        )
                                                    )
                                                ),
                                                "restore"	=> array(
                                                    "parts"	=> array(
                                                        "default"	=> array(
                                                            "useController"		=> "UsersSubscriptions",
                                                            "useAction"			=> "j",
                                                            "SendType"          => SEND_TYPE_JSON
                                                        )
                                                    )
                                                ),
                                                "default"	=> array(
                                                    "isAJAX"		=> array(
                                                        "SendType"          => SEND_TYPE_JSON
                                                    ),
                                                    "isNotAJAX"		=> array(
                                                        "useController"		=> "UsersSubscriptions",
                                                        "useAction"			=> "h",
                                                        "template"          => PATH_TEMPLATES."News_Subscriptions.html",
                                                        "SendType"          => SEND_TYPE_HTML
                                                    )
                                                )
                                            )
                                        ),
                                        "add"	=> array(
                                            "parts"	=> array(
                                                "default"	=> array(
                                                    "useController"		=> "UsersSubscriptions",
                                                    "useAction"			=> "a",
                                                    "SendType"          => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
                                        "test"	=> array(
                                            "parts"	=> array(
                                                "default"	=> array(
                                                    "useController"		=> "UsersSubscriptions",
                                                    "useAction"			=> "z",
                                                    //"SendType"          => SEND_TYPE_JSON
                                                )
                                            )
                                        ),
										"default"	=> array(
                                            //"isNotAJAX"		=> array(
                                                "defaultAction" => array(WSCONFIG_ACTIONS_RELOAD, PATH_TEMPLATES."News_Subscriptions.html")
                                            //)
										)
									)
								),
								"empty"	=> array(
									"isNotAJAX"		=> array(
										"template"		=> PATH_TEMPLATES."testdrive.html",
										"SendType"		=> SEND_TYPE_HTML
									)
								)
							)
						)
					)
				)
			)
		);
	}
}