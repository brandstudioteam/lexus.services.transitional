<?php
class Geo extends Controller
{
	/**
	 *
	 * @var Geo
	 */
 	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Geo
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				"count"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"cityCount"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"findText"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FIND_STRING)
				),
				"countryId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"actionId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"modelId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"promoId"			=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"regionId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"facilityId"		=> array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"facilityAlias"	=> array(
					"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_SEGURL)
				),
				"facilityAliasPool"	=> array(
					"pool"	=> array(
						"dataPath"	=> CONTROLLER_DATA_PATH_ROOT,
						"type"		=> CONTROLLER_POOL_TYPE_KEYS,
						"key"		=> array(
							"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_SEGURL)
						)
					)
				),
				//Долгота
				"gLg"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				//Широта
				"gLt"			=>  array(
					"filter"		=> array(FILTER_TYPE_TYPE, FILTER_TT_FLOAT)
				),
				"ref"			=>  array(
					"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_URL)
				),
				"refi"			=>  array(
					"filter"		=> array(FILTER_TYPE_REGEXP, FILTER_URL)
				),
                "fStatus"	=> array(
					"filter"		=> array(FILTER_TYPE_VALUES, array("0", "1"))
				),
				"dealerStatus"	=> array(
					"filter"		=> array(FILTER_TYPE_VALUES, array("0", "1", "2"))
				),
			)
		);

		$this->Modes = array(
			//Получить список всех стран, поиск страны по строке
			"a"	=> array(
				"exec"			=> array("GeoProcessor", "GetCountries"),
				"TplVars"		=> array("findText" => 0)
			),
			//Получить список стран, в которых есть дилеры, поиск страны, в которой есть дилеры, по строке
			"b"	=> array(
				"exec"			=> array("GeoProcessor", "GetDealersCountries"),
				"TplVars"		=> array("findText" => 0)
			),
			//Получить список  регионов для заданной страны, поиск региона по строке
			"c"	=> array(
				"exec"			=> array("GeoProcessor", "GetRegion"),
				"TplVars"		=> array("countryId" => 0, "findText" => 0)
			),
			//Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
			"d"	=> array(
				"exec"			=> array("GeoProcessor", "GetDealersRegion"),
				"TplVars"		=> array("countryId" => 0, "findText" => 0)
			),
			//Получить список всех городов для заданного региона, поиск города по строке
			"e"	=> array(
				"exec"			=> array("GeoProcessor", "GetCities"),
				"TplVars"		=> array("regionId" => 0, "findText" => 0, "countryId" => 0)
			),
			//Получить список городов, в которых есть дилеры, поиск города, в котором есть дилеры по строке
			"f"	=> array(
				"exec"			=> array("GeoProcessor", "GetDealersCities"),
				"TplVars"		=> array("regionId" => 0, "countryId" => 0, "findText" => 0, "facilityId" => 0, "actionId" => 0, "modelId" => 0, "fStatus" => 0, "dealerStatus" => 0, "facilityAliasPool" => 0)
			),
			//Получить список городов, в которых есть дилеры, поиск города, в котором есть дилеры по строке
			"f0"	=> array(
				"exec"			=> array("GeoProcessor", "GetDealersCitiesNew"),
				"TplVars"		=> array("regionId" => 0, "countryId" => 0, "findText" => 0, "facilityId" => 0, "actionId" => 0, "modelId" => 0, "fStatus" => 0, "dealerStatus" => 0, "facilityAliasPool" => 0)
			),
			//Получить список городов, в которых есть дилеры для заданной страны или региона, поиск города, в котором есть дилеры по строке
			"g"	=> array(
				"exec"			=> array("GeoProcessor", "GetNearestDealers"),
				"TplVars"		=> array("count" => 0, "cityCount" => 0, "facilityId" => 0, "gLt" => 0, "gLg" => 0)
			),
			//Получить список федеральных округов
			"h"	=> array(
				"exec"			=> array("GeoProcessor", "GetFederalDistrict"),
				"TplVars"		=> array("countryId" => 0)
			),
			//Получить список федеральных округов, в которых есть дилеры
			"i"	=> array(
				"exec"			=> array("GeoProcessor", "GetDealersFederalDistrict"),
				"TplVars"		=> array("countryId" => 0)
			),



			"j"	=> array(
				"exec"			=> array("GeoProcessor", "GetNearestDealersCity"),
				"TplVars"		=> array("count" => 0, "cityCount" => 0, "gLt" => 0, "gLg" => 0)
			)

		);
	}
}