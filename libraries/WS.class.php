<?php
/**
 * Зарезервированные имена входящих переменных:
 * ___view_error - при значении 1 включает режим отладки
 * ___debug_log - имя файла протокола отладчика
 * prq - в переменной передается строка json для интерпретации
 * callback - в переменной передается имя фукнкции для обмена данными способом JSONP
 * pjb - при передаче данных способом JSONP значение, эквивалентное true, указывает, что данные закодированы base64
 * ___dev_test - Если значение, эквивалентное true, указывает, что входящие данные должны интерпретироваться как тестовые
 * _ - принимает случайное значение для избегания кеширования ответов сервера. Не интерпритируется.
 * t - при значении "fu" указывает, что входящие данные содаржат файл и должны интерпритироваться как отправленные асинхронно
 * _bd - содержит идентификатор бренда, от имени которого необходимо интерпритировать данные и осуществлять выбор БД и т.п.
 * cuid - содержит идентификатор клиента
 * _fid - содержит уникальный идентификатор экземпляра формы
 * __id -  Если значение, эквивалентное true, указывает, что клиент работает в режиме драйвера. Т.е. при запросе событий, запрос производит только одно окно браузера
 * __gfs - Индекс файла формы для выставления его состояния при асинхронной загрузке файлов
 * __gfn - Имя файла формы для выставления его состояния при асинхронной загрузке файлов
 * __iCD - При значении "cd" указывает, что производится кроссдоменный запрос, или работа осуществляется в контексте стороннего домена
 * ref - При запросах из iframe'ов содержит ссылку на родительскую страницу. Используется на клиенте для обмена данными между фреймом и родитльской страницей
 * refi - При запросах из iframe'ов содержит идентификатор фрейма. Используется на клиенте для обмена данными между фреймом и родитльской страницей
 * refu - При запросах из iframe'ов содержит фрагмент url-фрейма. Используется на клиенте для обмена данными между фреймом и родитльской страницей
 * _gaA - При запросах из iframe'ов содержит счетчик Google Analitics
 * _ga - При запросах из iframe'ов содержит служебные данные Google Analitics
 * utm_... Данные, начинающиеся с utm_ при запросах из iframe'ов содержит служебные данные Google Analitics
 *
 * Указанные переменные уничтожаются во время подготовки контекста и не передаются контроллерам
 * Переменные имеют описанные значения только при передаче отдельными параметрами, т.е. не входящими в получаемые JSON-данные в переменной prq.
 */

class WS extends WSConfig
{
	/**
	 *
	 * @var WS
	 */
	protected static $Inst;

	protected $Getting = array("Protocol", "Secure", "Superdomain", "SendType", "IsThisServer", "IsAJAX", "Method", "DB", "OutHost", "OutURL", "IsTest", "Controller", "BrandId", "ClientUID", "ClientModeIsDriver");
	protected $Setting = array("Compressed", "FileOriginalName", "FileName", "SendType", "ContentType", "UseContentType");

	/**
	 *
	 * Объект класса DBi
	 * @var DBi
	 */
	protected $DB;

	/**
	 *
	 * Параметры входящих данных
	 */
	protected $IsThisServer;
	protected $IsAJAX;
	protected $IsMobileApp;
	protected $MobileAppSession;
	protected $URL;
    protected $IsTest;
	protected $IsDebugMode;
	protected $Protocol = "http://";
	protected $Method;
	protected $Secure;
	protected $OutURL;
    protected $OutHost;
    protected $BrandId;
    protected $ClientUID;
	protected $ClientRestore;
	protected $ClientModeIsDriver;
	protected $Configs;
	protected $FormUID;
	protected $IsCrossdomain;
	protected $GetFileStatus;
	protected $FileStatusName;

	public function Dispatcher()
	{
		$this->CheckProtocol();
     	$this->AnaliseContext();
     	try
     	{
	     	User::Init();

            if($this->ClientUID)
			{
				if(ClientsProcessor::Init()->SetCUID( $this->ClientUID))
				{
					parent::Dispatcher();
				}
				else
				{
					if($this->IsAJAX)
					{
						if(User::Init()->IsLogged())
						{
							$this->AddSystemEvent("clientRegisterRequest");
							$this->BuildAnswer(1, null);
							$this->SendType = SEND_TYPE_JSON;
						}
						else
						{
							if($this->RedirectAuth())
							{
								$this->AddSystemEvent("clientRegisterRequest");
								$this->BuildAnswer(1, null);
								$this->SendType = SEND_TYPE_JSON;
							}
							else
							{
								$this->BuildAnswer(1, null);
								$this->SendType = SEND_TYPE_JSON;
							}
						}
					}
					else throw new dmtException("Client UID is error", 81112);
				}
			}
			else parent::Dispatcher();
			switch ($this->SendType)
			{
				case SEND_TYPE_HTML:

					$this->Dump(__METHOD__.":".__LINE__, "SendType = SEND_TYPE_HTML");

					HTTPHeaders::SetContent("text/html", "utf-8");
                    if(is_array($this->Returned))
                    {
                        if($this->KeyData)
                            $this->Returned = $this->Returned[$this->KeyData];
                        else $this->Returned = reset($this->Returned);
                    }
					echo $this->Returned;
					exit();
					break;
				case SEND_TYPE_BINARY:

					$this->Dump(__METHOD__.":".__LINE__, "SendType = SEND_TYPE_BINARY");

					HTTPHeaders::SetContent($this->ContentType ? $this->ContentType : "application/octet-stream");
					echo $this->Returned;
					exit();
					break;
				case SEND_TYPE_UPLOAD:

					$this->Dump(__METHOD__.":".__LINE__, "SendType = SEND_TYPE_UPLOAD");

					if($this->FileName)
					{
						if(!file_exists($this->FileName))
							$this->NotFound();
						if(!$this->FileOriginalName)
							$this->FileOriginalName = array_pop(explode("/", $this->FileName));
						if(preg_match('~MSIE ([0-9].[0-9]{1,2})~ui', $_SERVER['HTTP_USER_AGENT']))
							$this->FileOriginalName = Strings::TranclitRE($this->FileOriginalName);
						HTTPHeaders::SendFile(	$this->FileOriginalName,
												filesize($this->FileName),
			     								$this->ContentType ? $this->ContentType : FileSys::GetMIME($this->FileName));
			     		//TODO: Добавить отправку по частям
			     		echo file_get_contents($this->FileName);
					}
					else
					{
						if($this->KeyData && is_array($this->Returned) && array_key_exists($this->KeyData, $this->Returned))
						{
							$this->Returned = $this->Returned[$this->KeyData];
						}
						elseif(is_array($this->Returned))
						{
							if(array_key_exists("result", $this->Returned))
								$this->Returned = $this->Returned["result"];
							else $this->Returned = array_shift($this->Returned);
						}
						$this->CheckCharSet();
						if(preg_match('~MSIE ([0-9].[0-9]{1,2})~ui', $_SERVER['HTTP_USER_AGENT']))
							$this->FileOriginalName = Strings::TranclitRE($this->FileOriginalName);
						HTTPHeaders::SendFile(	$this->FileOriginalName,
												mb_strlen($this->Returned, CHARSET_WIN1251),
			     								$this->UseContentType ? $this->UseContentType : ($this->ContentType ? $this->ContentType : "application/octet-stream"));
			     		echo $this->Returned;
					}
		     		exit;
				case SEND_TYPE_TEXT:

					$this->Dump(__METHOD__.":".__LINE__, "SendType = SEND_TYPE_TEXT");

					break;
				case SEND_TYPE_JSON:

					$this->Dump(__METHOD__.":".__LINE__, "SendType = SEND_TYPE_JSON");

                    if(is_array($this->Returned))
                    {
                        if($this->KeyData && isset($this->Returned[$this->KeyData]))
                            $this->Returned = $this->Returned[$this->KeyData];
                    }
					$this->Returned = $this->JSPack($this->Returned);
					if($this->ClientCallback)
						$this->Returned = $this->ClientCallback."(".$this->Returned.")";
					break;
			}
			HTTPHeaders::SetContent($this->ContentType ? $this->ContentType : "text/plain");
			if($this->Compressed)
				$this->Compressed($this->Returned);
			
			$this->Dump($this->Returned);
			
			echo $this->Returned;
     	}
     	catch(dmtException $e)
     	{
     		switch ($e->getCode())
     		{
     			case 77777:
					HTTPHeaders::Forbidden();
					break;
     			case 77775:
		     		HTTPHeaders::NotFound();
		     		break;
     		}
			exit;
     	}
		catch(dmtErrException $e)
     	{
     		switch ($e->getCode())
     		{
     			case 77777:
					HTTPHeaders::Forbidden();
					break;
     			case 77775:
		     		HTTPHeaders::NotFound();
		     		break;
     		}
			exit;
     	}
     	catch(dmtFailException $e)
     	{
     		switch ($e->getCode())
     		{
     			case 77777:
					HTTPHeaders::Forbidden();
					break;
     			case 77775:
		     		HTTPHeaders::NotFound();
		     		break;
     		}
			exit;
     	}
	}

	private function ParseHref($Href)
	{
		$R = false;
		//if(Strings::ValidateURL($Href))
		//{
			$URL = parse_url($Href);
			if(isset($URL['host']) && $URL['host'] != $_SERVER['HTTP_HOST'])
			{
				$this->IsThisServer = false;
				$this->OutURL = $Href;
				$this->OutHost = $URL['host'];
				$R = true;
			}
			/*
		}
		else
		{
			$this->Dump(__METHOD__.": ".__LINE__, "======= ERORR REFERER VALIDATE. REFERER = \"".$Href."\"");
		}
			 */
		return $R;
	}


	private function AnaliseContext()
	{
		$this->IsThisServer = true;
		if(isset($_SERVER['HTTP_REFERER']))
		{
			if(!$this->ParseHref($_SERVER['HTTP_REFERER']))
			{
				if(isset($_SERVER["HTTP_X_D_C_R_F"]))
				{
					$this->ParseHref($_SERVER['HTTP_X_D_C_R_F']);
					unset($_SERVER['HTTP_X_D_C_R_F']);
				}
				$_SERVER['HTTP_REFERER'] = null;
			}
		}
		elseif(isset($_SERVER["HTTP_X_D_C_R_F"]))
		{
			$this->ParseHref($_SERVER['HTTP_X_D_C_R_F']);
			unset($_SERVER['HTTP_X_D_C_R_F']);
		}

		if(!isset($_SERVER["HTTP_USER_AGENT"]))
		{
			$_SERVER["HTTP_USER_AGENT"] = null;
		}
		switch(mb_strtoupper($_SERVER["REQUEST_METHOD"]))
		{
			case "POST":
				$this->Method = REQUEST_METHOD_POST;
				break;
			case "GET":
				$this->Method = REQUEST_METHOD_GET;
				break;
			case "HEAD":
				$this->Method = REQUEST_METHOD_HEAD;
				break;
			case "PATCH":
				$this->Method = REQUEST_METHOD_PATCH;
				break;
			case "PUT":
				$this->Method = REQUEST_METHOD_PUT;
				break;
			case "DELETE":
				$this->Method = REQUEST_METHOD_DELETE;
				break;
			case "LINK":
				$this->Method = REQUEST_METHOD_LINK;
				break;
			case "UNLINK":
				$this->Method = REQUEST_METHOD_UNLINK;
				break;
			/*
			case "TRACE":
				$this->Method = REQUEST_METHOD_TRACE;
				break;
			case "CONNECT":
				$this->Method = REQUEST_METHOD_CONNECT;
				break;
			*/
			case "OPTIONS":
				if(isset($_SERVER["HTTP_ORIGIN"]) && (preg_match('/(?:[\w\d-_]+\.)?'.(SITE_CURRENT == SITE_TOYOTA ? "toyota.ru" : "lexus.ru").'/', $_SERVER["HTTP_ORIGIN"]) || preg_match('/(?:[\w\d-_]+\.)?dev.bstd.ru/', $_SERVER["HTTP_ORIGIN"]) || preg_match('/mias.ampmstd.ru/', $_SERVER["HTTP_ORIGIN"])))
				{
					header('Access-Control-Allow-Origin: *');
					header('Access-Control-Allow-Methods: GET, POST');
					if($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"])
                    {
						header('Access-Control-Allow-Headers: '.$_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]);
					}
					exit;
				}
				$this->Forbidden();
			default:
				$this->Forbidden();
		}
		$this->URL = $_SERVER["REQUEST_URI"];
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']))
		{
			if(mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest")
				$this->IsAJAX = true;
			elseif(mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttpmobilerequest")
			{
				$this->IsAJAX = true;
				$this->IsMobileApp = true;
				if(isset($_SERVER['HTTP_X_MAPS_ID']))
				{
					$this->MobileAppSession = $_SERVER['HTTP_X_MAPS_ID'];
				}
			}
		}
		elseif(isset($_REQUEST["___flap"]))
		{
			$this->Dump(__METHOD__.": ".__LINE__, "SET Flash applications MODE");
			$this->IsAJAX = true;
			$this->IsMobileApp = true;
			if(isset($_REQUEST['___mapsId']))
			{
				if($this->Filtering($_REQUEST['___mapsId'], FILTER_SESSION))
					$this->MobileAppSession = $_REQUEST['___mapsId'];

				$this->Dump(__METHOD__.": ".__LINE__, "SET Flash applications SESSION. SessionId='".$_REQUEST['___mapsId']."'");

				unset($_REQUEST['___mapsId']);
				if(isset($_GET["___mapsId"]))
					unset($_GET["___mapsId"]);
				if(isset($_POST["___mapsId"]))
					unset($_POST["___mapsId"]);
			}
			unset($_REQUEST["___flap"]);
			if(isset($_GET["___flap"]))
				unset($_GET["___flap"]);
			if(isset($_POST["___flap"]))
				unset($_POST["___flap"]);
		}

		if(isset($_REQUEST["___view_error"]) && $_REQUEST["___view_error"] == "1")
		{
			$this->IsDebugMode = true;
			if(isset($_REQUEST["___debug_log"]) && $this->Filtering($_REQUEST["___debug_log"], FILTER_FILENAME_LATIN))
			{
				Debug::Init()->SetDebugParams(array(
					"enabled"	=> true,
					"file"		=> DEBUG_FILES_PATH.$_REQUEST["___debug_log"]
				));
				Debug::Init()->SetContext();
				unset($_REQUEST["___debug_log"]);
				if(isset($_GET["___debug_log"]))
				{
					unset($_GET["___debug_log"]);
				}
				if(isset($_POST["___debug_log"]))
				{
					unset($_POST["___debug_log"]);
				}
			}
			unset($_REQUEST["___view_error"]);
			if(isset($_GET["___view_error"]))
            {
                unset($_GET["___view_error"]);
            }
            if(isset($_POST["___view_error"]))
            {
                unset($_POST["___view_error"]);
            }
		}

        if(isset($_GET["callback"]))
		{
			$this->ClientCallback = $_GET["callback"];
			unset($_GET["callback"], $_REQUEST["callback"]);
			if(isset($_GET["pjb"]))
            {
				if(isset($_GET["prq"]))
                {
					$_POST["prq"] = base64_decode(mb_str_replace(" ", "+", $_GET["prq"]));
					unset($_GET["prq"]);
				}
				$this->IsAJAX = true;
				unset($_GET["pjb"], $_REQUEST["pjb"]);
			}
		}
        else
        {
           if(isset($_REQUEST["pjb"]))
            {
				if(isset($_REQUEST["prq"]))
                {
					$_POST["prq"] = base64_decode(mb_str_replace(" ", "+", $_REQUEST["prq"]));
					unset($_REQUEST["prq"]);
				}
				$this->IsAJAX = true;
				unset($_REQUEST["pjb"]);
                if(isset($_GET["pjb"]))
                    unset($_GET["pjb"]);
                if(isset($_POST["pjb"]))
                    unset($_POST["pjb"]);
			}
        }
        if(array_key_exists("___dev_test", $_REQUEST))
        {
            $this->IsTest = true;
            unset($_REQUEST["___dev_test"]);
			if(isset($_GET["___dev_test"]))
				unset($_GET["___dev_test"]);
			if(isset($_POST["___dev_test"]))
				unset($_POST["___dev_test"]);
        }
		if(isset($_GET["_"]))
		{
			unset($_GET["_"]);
		}
		if(isset($_REQUEST["_"]))
		{
			unset($_REQUEST["_"]);
		}
		if(isset($_POST["t"]) && $_POST["t"] = "fu") {
			unset($_POST["t"]);
			$this->IsAJAX = true;
		}
        if(isset($_REQUEST["_bd"]) && mb_strtolower($_REQUEST["_bd"]) != "lexus")
        {
            $this->BrandId = $_REQUEST["_bd"];
            unset($_REQUEST["_bd"]);
            if(isset($_GET["_bd"]))
            {
                unset($_GET["_bd"]);
            }
            if(isset($_POST["_bd"]))
            {
                unset($_POST["_bd"]);
            }
        }
        else $this->BrandId = preg_match('/lexus/iu', $_SERVER['HTTP_HOST']) ? BRAND_LEXUS : BRAND_TOYOTA;
        BrandManager::Init($this->BrandId);
        if(isset($_REQUEST["cuid"]))
        {
            if($this->Filtering($_REQUEST["cuid"], FILTER_MD5))
            {
                $this->ClientUID = $_REQUEST["cuid"];
				unset($_REQUEST["cuid"]);
				if(isset($_GET["cuid"]))
				{
					unset($_GET["cuid"]);
				}
				if(isset($_POST["cuid"]))
				{
					unset($_POST["cuid"]);
				}
            }
            else
            {
                throw new dmtException("Invalid Client UID", 81112);
            }
        }
		if(isset($_REQUEST["_fid"]) && $this->Filtering($_REQUEST["_fid"], FILTER_FORM_UID))
		{
			$this->FormUID = $_REQUEST["_fid"];
			unset($_REQUEST["_fid"]);
			if(isset($_GET["_fid"]))
            {
                unset($_GET["_fid"]);
            }
            if(isset($_POST["_fid"]))
            {
                unset($_POST["_fid"]);
            }
		}
		if(isset($_REQUEST["__id"]) && $_REQUEST["__id"] == 1)
		{
			$this->ClientModeIsDriver = true;
			unset($_REQUEST["__id"]);
			if(isset($_GET["__id"]))
            {
                unset($_GET["__id"]);
            }
            if(isset($_POST["__id"]))
            {
                unset($_POST["__id"]);
            }
		}
		if(isset($_REQUEST["__iCD"]) && $_REQUEST["__iCD"] == "cd")
		{
			$this->IsCrossdomain = true;
			unset($_REQUEST["__iCD"]);
			if(isset($_GET["__iCD"]))
            {
                unset($_GET["__iCD"]);
            }
            if(isset($_POST["__iCD"]))
            {
                unset($_POST["__iCD"]);
            }
		}
		if(isset($_REQUEST["__gfs"]) && isset($_REQUEST["__gfn"]) && $this->Filtering($_REQUEST["__gfn"], FILTER_DOMAIN))
		{
			$tmp = Math::PrepareUnsignedInteger($_REQUEST["__gfs"]);
			if($tmp !== false)
			{
				$this->GetFileStatus = $tmp;
				unset($_REQUEST["__gfs"]);
				if(isset($_GET["__gfs"]))
				{
					unset($_GET["__gfs"]);
				}
				if(isset($_POST["__gfs"]))
				{
					unset($_POST["__gfs"]);
				}
				$this->FileStatusName = $_REQUEST["__gfn"];
				unset($_REQUEST["__gfn"]);
				if(isset($_GET["__gfn"]))
				{
					unset($_GET["__gfn"]);
				}
				if(isset($_POST["__gfn"]))
				{
					unset($_POST["__gfn"]);
				}
				$this->IsAJAX = true;
			}
			else
			{
			}
		}
		if(isset($_REQUEST["ref"]))
		{
			unset($_REQUEST["ref"]);
			if(isset($_GET["ref"]))
            {
                unset($_GET["ref"]);
            }
            if(isset($_POST["ref"]))
            {
                unset($_POST["ref"]);
            }
		}
		if(isset($_REQUEST["refi"])) {
			unset($_REQUEST["refi"]);
			if(isset($_GET["refi"]))
            {
                unset($_GET["refi"]);
            }
            if(isset($_POST["refi"]))
            {
                unset($_POST["refi"]);
            }
		}
		if(isset($_REQUEST["refu"])) {
			unset($_REQUEST["refu"]);
			if(isset($_GET["refu"]))
            {
                unset($_GET["refu"]);
            }
            if(isset($_POST["refu"]))
            {
                unset($_POST["refu"]);
            }
		}
		/*
		 * Убиваем служебные метки Google Analitics
		 */
		if(isset($_REQUEST["_gaA"]))
		{
			unset($_REQUEST["_gaA"]);
			if(isset($_GET["_gaA"]))
            {
                unset($_GET["_gaA"]);
            }
            if(isset($_POST["_gaA"]))
            {
                unset($_POST["_gaA"]);
            }
		}
		if(isset($_REQUEST["_ga"]))
		{
			unset($_REQUEST["_ga"]);
			if(isset($_GET["_ga"]))
            {
                unset($_GET["_ga"]);
            }
            if(isset($_POST["_ga"]))
            {
                unset($_POST["_ga"]);
            }
		}
		foreach($_REQUEST as $k => $v) {
			if(mb_strpos($k, "utm_") !== false)
			{
				unset($_REQUEST[$k]);
				if(isset($_GET[$k]))
				{
					unset($_GET[$k]);
				}
				if(isset($_POST[$k]))
				{
					unset($_POST[$k]);
				}
			}
		}
	}

	public function IsMobileApp()
	{
		return $this->IsMobileApp;
	}

	public function GetMobileAppSession()
	{
		return $this->MobileAppSession;
	}

	protected function PrepareVariables($Data)
	{
		if(!is_array($Data))
		{
			return;
		}
		if(array_key_exists("___dev_test", $Data))
        {
            $this->IsTest = true;
            unset($Data["___dev_test"]);
        }
        if(isset($Data["_bd"]) && mb_strtolower($Data["_bd"]) != "lexus")
        {
            $this->BrandId = $Data["_bd"];
            unset($Data["_bd"]);
			BrandManager::Init($this->BrandId);
        }
        else
		{
			$this->BrandId = preg_match('/lexus/iu', $_SERVER['HTTP_HOST']) ? BRAND_LEXUS : BRAND_TOYOTA;
			BrandManager::Init($this->BrandId);
		}
        if(isset($Data["cuid"]))
        {
            if($this->Filtering($Data["cuid"], FILTER_MD5))
            {
                $this->ClientUID = $Data["cuid"];
				unset($Data["cuid"]);
            }
            else
            {
                throw new dmtException("Invalid Client UID", 81112);
            }
        }
		if(isset($Data["_fid"]) && $this->Filtering($Data["_fid"], FILTER_FORM_UID))
		{
			$this->FormUID = $Data["_fid"];
			unset($Data["_fid"]);
		}
		if(isset($Data["__id"]) && $Data["__id"] == 1)
		{
			$this->ClientModeIsDriver = true;
			unset($Data["__id"]);
		}
		if(isset($Data["__iCD"]) && $Data["__iCD"] == "cd")
		{
			$this->IsCrossdomain = true;
			unset($Data["__iCD"]);
		}
		if(isset($Data["__gfs"]) && isset($Data["__gfn"]) && $this->Filtering($Data["__gfn"], FILTER_DOMAIN))
		{
			$tmp = Math::PrepareUnsignedInteger($Data["__gfs"]);
			if($tmp !== false)
			{
				$this->GetFileStatus = $tmp;
				unset($Data["__gfs"]);
				$this->FileStatusName = $Data["__gfn"];
				unset($Data["__gfn"]);
				$this->IsAJAX = true;
			}
		}
		if(isset($Data["___view_error"]) && $Data["___view_error"] == "1")
		{
			$this->IsDebugMode = true;
			Debug::Init()->SetDebugParams(array(
				"enabled"	=> true,
				"file"		=> DEBUG_FILES_PATH.$Data["___debug_log"]
			));
			Debug::Init()->SetContext();

			unset($Data["___view_error"]);
			if(isset($Data["___debug_log"]) && $this->Filtering($Data["___debug_log"], FILTER_FILENAME_LATIN))
			{
				$this->DebugLog = $Data["___debug_log"];
				unset($Data["___debug_log"]);
			}
		}
	}


    /**
	 *
	 * Выполняет компрессию отсылаемых данных
	 *
	 * Сжатие будет выполнено только в случае, если клиент подтверждает поддержку формата сжатия заголовком ACCEPT_ENCODING
	 *
	 * @param string $Type - параметр принудительного сжатия данных. В текущей версии может принимать значения: "gzip" или "x-gzip".
	 * Рекомендуется использовать только в случаях, когда доподлинно известно, что клиент принимает сжатые данные с использованием gzip.
	 * @return boolean true - в случае успешного сжатия, false - если данные не были сжаты.
	 */
	public function Compressed($Type = null)
	{
		if(!COMPRESSED_ENABLE) return false;
		if($_SERVER["HTTP_ACCEPT_ENCODING"] || $Type)
		{
			if(!$Type)
			{
				$Type = explode(",", $_SERVER["HTTP_ACCEPT_ENCODING"]);
				$Type = in_array("gzip", $Type) ? "gzip" : (in_array("x-gzip", $Type) ? "x-gzip" : false);
			}
			elseif($Type != "gzip" && $Type != "x-gzip") $Type = false;
			if($Type)
			{
				$this->Returned = gzencode($this->Returned);
				HTTPHeaders::Compressed(mb_strlen($this->Returned, "windows-1251"), $Type);
			}
		}
		return (boolean) $Type;
	}

	public function CheckProtocol()
	{
     	if(PROTOCOL_ENABLE_SSL)
     	{
	    	if(isset($_SERVER["HTTPS"]) && $this->User->IsLogged() && $this->User->IsSecurity())
	    	{
	    		$this->Secure = true;
	    		$this->Protocol = "https://";
	    	}
	    	else
	    	{
	    		$this->Secure = false;
	    		$this->Protocol = "http://";
	    	}
     	}
     	else $this->Protocol = "http://";
	}

	/**
	 * Если пришел тестовый запрос
	 * @return boolean
	 */
	public function IsTest()
    {
        return $this->IsTest;
    }
	/**
	 * Если включен режим отладки
	 * @return boolean
	 */
	public function IsDebugMode()
	{
		return $this->IsDebugMode;
	}
	/**
	 * Возвращает IP адрес запроса
	 * @return string
	 */
	public function GetIP()
	{
		return $_SERVER["REMOTE_ADDR"];
	}
	/**
	 * Возвращает идентификатор формы, с которой пришел запрос
	 * @return string
	 */
	public function GetFormUID()
	{
		return $this->FormUID;
	}
	/**
	 * Если пришел кросс-доменный запрос
	 * @return boolean
	 */
	public function IsCrossdomain()
	{
		return $this->IsCrossdomain;
	}

	public function IsGetFileStatus()
	{
		return $this->GetFileStatus;
	}

	public function GetFileStatusName()
	{
		return $this->FileStatusName;
	}
	/**
	 * Возвращает внитренний идентификатор метода запроса
	 * @return integer
	 */
	public function GetMethod()
	{
		return $this->Method;
	}
	/**
	 * Возвращает имя хоста при кросс-доменном запросе или из referrer
	 * @return string
	 */
    public function GetOuterHost()
    {
        return $this->OutHost;
    }
	/**
	 * Если асинхронный запрос (AJAX)
	 * @return boolean
	 */
	public function IsAJAX()
	{
		return $this->IsAJAX;
	}
	/**
	 * Возвращает идентификатор клиента
	 * @return string
	 */
	public function GetClientUID()
	{
		return $this->ClientUID;
	}
	/**
	 * Если клиент работает в режиме драйвера
	 * @return boolean
	 */
	public function ClientModeIsDrive()
	{
		return $this->ClientModeIsDriver;
	}
	/**
	 * Упаковывает данные в JSON
	 * @param array $Data
	 * @return string
	 */
	public function JSPack($Data)
	{
		return is_array($Data) ? json_encode($Data) : $Data;
	}
	/**
	 * Перенаправляет запрос на указанный адрес
	 * @param string $URL
	 */
	public function Redirect($URL)
	{
		HTTPHeaders::Location($URL);
		exit;
	}
	/**
	 * Возвращает URL при кросс-доменном запросе или из referrer
	 * @return string
	 */
    public function GetOuterURL()
    {
        return $this->OutURL;
    }
	/**
	 * Устанавливает текущий шаблон
	 * @param string $Template
	 */
	public function SetTemplate($Template)
	{
		$this->Template = $Template;
	}
	/**
	 * Возвращает идентификатор бренда
	 * @return integer
	 */
	public function GetBrandId()
	{
		return $this->BrandId;
	}

	public function Filtering($Data, $FilterType)
	{
		return (boolean)(mb_eregi($FilterType, $Data));
	}

	private function GlobalError($Error)
	{
		//TODO: Отписать действия в случае возникновения глобальной ошибки
		exit();
	}

	protected function CheckCharSet()
	{
		if($this->OutCharSet && $this->OutCharSet != CHARSET)
		{
			$this->Returned = iconv(CHARSET, $this->OutCharSet."//IGNORE", $this->Returned);
		}
	}


	function __get($Name)
	{
     	if(in_array($Name, $this->Getting)) return $this->$Name;
     	else throw new dmtException("Property \"".$Name."\" can not be obtained or not found in class \"".__CLASS__."\"");
	}

	function __set($Name, $Value)
	{
     	if(in_array($Name, $this->Setting)) $this->$Name = $Value;
		else
        {
            //GetDebug()->Trace();
            throw new dmtException("Property \"".$Name."\" can not be obtained or not found in class \"".__CLASS__."\"");
        }
	}

	public function __call($Metod, $Params=false)
	{
		if($this->DB && method_exists($this->DB, $Metod))
			$this->DB->$Metod($Params);
		else
		{
			throw new dmtException("Metod \"".$Metod."\" not found.");
		}
	}

	/**
	 *
	 * Инициализирует класс
	 * @return WS
	 */
	public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

    protected function __construct()
    {
    	parent::__construct();
    	//Подключение к БД
		$this->DB = new DBi();
    }
}