<?php
class FacilitiesForms extends Controller
{
	/**
	 *
	 * @var FacilitiesForms
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return FacilitiesForms
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"facilityId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerDivisionId"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"modelId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"currentModelId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"currentManufacturerId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"captcha"				=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_CAPTCHA),
					"verifier"	=> array("Captcha", "CheckCode")
				),
				"subscribe"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1, "true" => 1, "false" => 0)),
				),
				"isOwner"				=> array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1, "true" => 1, "false" => 0))
				),
				"email"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"age"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"phone"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
				),
				"gender"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
				),
				"lastName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_FIO),
				),
				"firstName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"middleName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"memberName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
				),
				"lastNameWEn"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME_WEN),
					"notempty"	=> true
				),
				"firstNameWEn"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME_WEN),
					"notempty"	=> true
				),
				"partnerUserDataType"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerOfficialName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerAddress"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerEmail"				=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_EMAIL),
				),
				"pertnerTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"pertnerTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"emails"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"testDriveCarImage"	=> array(
					"isfile"	=> array(),
					"required"	=> true
				),
				"cityId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"modelSpec"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"modelColor"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"contactMethod"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
                "submodelId"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "witchDealers"    => array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
				),
                "type"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "pdf"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
                "formType"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "contactMethod"   => array(
					"filter"	=> array(FILTER_TYPE_ALIASES, array("0" => 0, "1" => 1, "2" => 2)),
				),
                "fds"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
					"default"	=> 0
				),
                "im"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
					"default"	=> 0
				),
                "visitDate"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
					"default"	=> 0
				),
                "visitTime"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TIME),
					"default"	=> 0
				),
                "registerStart"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerEnd"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"code"    => array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("_asd-AS-adasdwer-r-rrt5436--df-srf3-54345")),
				),
				"complectationId"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"source"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"agreementTitle"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"agreementText"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"advancedInfo"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"manufactured"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_YEAR),
				),
				"miliage"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"regNumber"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_AUTO_REGNUMBER),
				),
				"sourceInfo"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"vin"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_VIN),
				),
				"configurationId"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_VIN),
				),
				"color"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"userAction"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"toTypeId"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"contactingThemeId"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"photo"			=> array(
					"isfile"		=> array(
						"limits"		=> array(1, 2097152, 2097152),
						"mime"			=> array(
							"accept"		=> array("gif", "jpeg", "jpg", "png")
						),
    					"name"			=> md5(microtime(true)),
						"savepath"		=> PATH_CAR_VALAUATION_UPLOAD,
						"autoreply"		=> 1
					),
					"required"		=> true
				),
				"customCity"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
				"visitDateStart"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"visitDateEnd"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"gaAccount"		    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_GA_ACCOUNT),
				),
			)
		);
		$this->Modes = array(
			//Возвращает дилерские настройки форм
			"a"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "GetDealersFormSet"),
				"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 0)
			),
			//Возвращает дилерские настройки форм для системы управления
			"b"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "GetDealersFormSetForAdmin"),
				"TplVars"		=> array("partnerDivisionId" => 0)
			),
			//Сохраняет дилерские настройки форм
			"c"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "SaveDealersFormSet"),
				"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 2, "agreementTitle" => 0, "agreementText" => 0, "gaAccount" => 0)
			),
			"d"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "AddMember"),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						array(
							5 => "AlreadyExists"
						)
					),
				),
				"VarsCondition"	=> array(
					"variableName"	=> "facilityId",
					"type"			=> "value",
					"conditions"	=> array(
						"50"	=>	array(
							"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 2, "modelId" => 2, "lastName" => 2, "firstName" => 2,
											"middleName" => 0, "email" => 2, "phone" => 2, "contactMethod" => 0, "gender" => 2, "age" => 0, "isOwner" => 0, "advancedInfo" => 0,
											"type" => 2, "currentModelId" => 0, "currentManufacturerId" => 0, "manufactured" => 0, "miliage" => 0, "regNumber" => 0,
											"cityId" => 0, "sourceInfo" => 0, "visitDate" => 0, "visitTime" => 0, "vin" => 0, "configurationId" => 0, "color" => 0,
											"userAction" => 0, "toTypeId" => 0, "fds" => 0, "im" => 0, "subscribe" => 0)
						),
						"52"	=>	array(
							"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 0, "modelId" => 0, "lastName" => 2, "firstName" => 2,
											"middleName" => 0, "email" => 2, "phone" => 2, "contactMethod" => 0, "gender" => 0, "age" => 0, "isOwner" => 0, "advancedInfo" => 2,
											"type" => 2, "currentModelId" => 0, "currentManufacturerId" => 0, "manufactured" => 0, "miliage" => 0, "regNumber" => 0,
											"cityId" => 0, "sourceInfo" => 0, "visitDate" => 0, "visitTime" => 0, "vin" => 0, "configurationId" => 0, "color" => 0,
											"userAction" => 0, "toTypeId" => 0, "fds" => 0, "im" => 0, "subscribe" => 0, "contactingThemeId" => 2, "customCity" => 2)
						),
						"53"	=>	array(
							"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 2, "modelId" => 0, "lastName" => 2, "firstName" => 2,
											"middleName" => 0, "email" => 0, "phone" => 2, "contactMethod" => 0, "gender" => 0, "age" => 0, "isOwner" => 0, "advancedInfo" => 2,
											"type" => 2, "currentModelId" => 0, "currentManufacturerId" => 0, "manufactured" => 0, "miliage" => 0, "regNumber" => 0,
											"cityId" => 0, "sourceInfo" => 0, "visitDate" => 2, "visitTime" => 2, "vin" => 0, "configurationId" => 0, "color" => 0,
											"userAction" => 0, "toTypeId" => 0, "fds" => 0, "im" => 0, "subscribe" => 0, "contactingThemeId" => 2, "customCity" => 0)
						),
						"76"	=>	array(
							"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 2, "modelId" => 0, "lastName" => 0, "firstName" => 2,
											"middleName" => 0, "email" => 2, "phone" => 2, "contactMethod" => 0, "gender" => 0, "age" => 0, "isOwner" => 0, "advancedInfo" => 0,
											"type" => 2, "currentModelId" => 2, "currentManufacturerId" => 2, "manufactured" => 0, "miliage" => 0, "regNumber" => 0,
											"cityId" => 0, "sourceInfo" => 0, "visitDate" => 2, "visitTime" => 2, "vin" => 0, "configurationId" => 0, "color" => 0,
											"userAction" => 0, "toTypeId" => 0, "fds" => 0, "im" => 0),
						),
						"77"	=>	array(
							"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 2, "modelId" => 0, "lastName" => 0, "firstName" => 2,
											"middleName" => 0, "email" => 2, "phone" => 2, "contactMethod" => 0, "gender" => 0, "age" => 0, "isOwner" => 0, "advancedInfo" => 0,
											"type" => 2, "currentModelId" => 2, "currentManufacturerId" => 2, "manufactured" => 0, "miliage" => 0, "regNumber" => 0,
											"cityId" => 0, "sourceInfo" => 0, "visitDate" => 2, "visitTime" => 2, "vin" => 0, "configurationId" => 0, "color" => 0,
											"userAction" => 0, "toTypeId" => 0, "fds" => 0, "im" => 0)
						),
						"default"	=>	array(
							"TplVars"		=> array("facilityId" => 2, "formType" => 2, "partnerDivisionId" => 0, "modelId" => 0, "lastName" => 0, "firstName" => 0,
											"middleName" => 0, "email" => 0, "phone" => 0, "contactMethod" => 0, "gender" => 0, "age" => 0, "isOwner" => 0, "advancedInfo" => 0,
											"type" => 2, "currentModelId" => 0, "currentManufacturerId" => 0, "manufactured" => 0, "miliage" => 0, "regNumber" => 0,
											"cityId" => 0, "sourceInfo" => 0, "visitDate" => 0, "visitTime" => 0, "vin" => 0, "configurationId" => 0, "color" => 0,
											"userAction" => 0, "toTypeId" => 0, "fds" => 0, "im" => 0, "subscribe" => 0, "contactingThemeId" => 0, "customCity" => 0),
							"getFiles"		=> array("photo" => 0)
						)
					)
				)
			),
			"e"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "GetFullDataForm"),
				"TplVars"		=> array("facilityId" => 2, "formType" => 0)
			),
			"f"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "GetContactingThemes"),
				"TplVars"		=> array("facilityId" => 2)
			),
			"g"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "GetMembers"),
				"TplVars"		=> array("facilityId" => 2, "formType" => 0, "partnerDivisionId" => 0, "modelId" => 0, "registerStart" => 0, "registerEnd" => 0,
										"visitDateStart" => 0, "visitDateEnd" => 0)
			),
			"h"		=> array(
				"exec"		=> array("FacilitiesFormsProcessor", "ExportMembers"),
				"TplVars"		=> array("facilityId" => 2, "formType" => 0, "partnerDivisionId" => 0, "modelId" => 0, "registerStart" => 0, "registerEnd" => 0,
										"visitDateStart" => 0, "visitDateEnd" => 0)
			),
		);
	}
}