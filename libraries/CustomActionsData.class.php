<?php
class CustomActionsData extends Data
{
	public function AddMember($Action, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $IsOwner = null, $AdvInfo = null, $Type = null, $Referrer = null, $FromDealerSite = null, $FromMobile = null, $UserAgent = null, $CurrentModel = null, $FormType = null, $IsTest = null)
	{
		$this->Exec("INSERT INTO `".DBS_REFERENCES."`.`custom_actions_members`
	(`partners_division_id`,
	`custom_actions_id`,
	`model_id`,
	`first_name`,
	`last_name`,
	`gender`,
	`age`,
	`email`,
	`phone`,
	`is_owner`,
	`register`,
	`other_info`,
    `advanced_flag`,
    `referrer`,
    `from_dealers_site`,
    `from_mobile`,
    `user_agent`,
    `current_model_id`,
    `form_type_id`,
	`is_test`)
VALUES
	(".$Dealer.",
	".$Action.",
	".$Model.",
	".$this->Esc($FirstName).",
	".$this->Esc($LastName).",
	".$this->CheckNull($Gender).",
	".$this->CheckNull($Age).",
	".$this->Esc($Email).",
	".$this->Esc($Phone).",
	".$this->CheckNull($IsOwner).",
	NOW(),
	".$this->Esc($AdvInfo).",
    ".$Type.",
    ".$this->Esc($Referrer).",
    ".($FromDealerSite ? 1 : 0).",
    ".($FromMobile ? 1 : 0).",
    ".$this->Esc($UserAgent).",
    ".$this->CheckNull($CurrentModel).",
    ".($FormType ? $FormType : 1).",
	".($IsTest ? 1 : 0).");");

        return $this->DB->GetLastID();
	}

	public function GetDealerEmails($Action, $Dealer = null, $FormType = null)
	{
        $Where = array();
        $Where[] = "`form_type_id`".$this->PrepareValue($FormType ? $FormType : 1);
        $Where[] = "`action_id`".$this->PrepareValue($Action);
        if($Dealer)
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealer);
        return $this->Get("SELECT
	`email` AS email
FROM `".DBS_REFERENCES."`.`custom_actions_partners_emails`".$this->PrepareWhere($Where).";");
	}

    public function GetDealerAndUniversalEmail($Action, $FormType, $Dealer, $Type)
    {
        $Where = array();
        $Where2 = array();
        $A = "`form_type_id`=".($FormType ? $FormType : 1);
        $Where[] = $A;
        $Where2[] = $A;
        $A = "`action_id`=".$Action;
        $Where[] = $A;
        $Where2[] = $A;
        $Where2[] = "`type` IN (".($Type ? 2 : 1).", 3)";
		$Where2[] = "`category`<>1";
        if($Dealer)
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealer);

        $Query = "SELECT
	`email` AS email,
	3 AS sendAs
FROM `".DBS_REFERENCES."`.`custom_actions_partners_emails`".$this->PrepareWhere($Where)."
UNION
SELECT
	`email`,
	`send_as`
FROM `".DBS_REFERENCES."`.`custom_actions_universal_emails`".$this->PrepareWhere($Where2).";";
        return $this->Get($Query);
    }

	public function GetAllEmails($Action)
	{
		return $this->Get("SELECT
	`email` AS email
FROM `".DBS_REFERENCES."`.`custom_actions_partners_emails`
WHERE `action_id`=".$Action.";");
	}

	public function CheckMember($Action, $Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe, $IsOwner, $AdvInfo, $ContactMethod, $Type, $FromDealerSite, $FromMobile, $CurrentModel, $FormType)
	{
		$Where = array();
		$Where[] = "`partners_division_id`=".$Dealer;
		$Where[] = "`custom_actions_id`=".$Action;
		$Where[] = "`form_type_id`=".$FormType;
		$Where[] = "`model_id`=".$this->CheckNull($Model);
		$Where[] = "`first_name`=".$this->Esc($FirstName);
		$Where[] = "`last_name`=".$this->Esc($LastName);
		if($Gender)
			$Where[] = "`gender`=".$Gender;
		if($Age)
			$Where[] = "`age`=".$Age;
		if($Email)
			$Where[] = "`email`=".$this->Esc($Email);
		if($Phone)
			$Where[] = "`phone`=".$this->Esc($Phone);


		if($IsOwner !== null && !is_empty($IsOwner))
			$Where[] = "`is_owner`=".$this->Esc($IsOwner);
		if($AdvInfo)
			$Where[] = "`other_info`=".$this->Esc($AdvInfo);
		if($CurrentModel)
			$Where[] = "`current_model_id`=".$this->CheckNull($CurrentModel);
		$Where[] = "`register` > DATE_ADD(NOW(), INTERVAL -1 DAY)";

		$this->Dump(__METHOD__.": ".__LINE__, "SELECT
	COUNT(*) AS Cnt
FROM `".DBS_REFERENCES."`.`custom_actions_members`".$this->PrepareWhere($Where));


		return $this->Count("SELECT
	COUNT(*) AS Cnt
FROM `".DBS_REFERENCES."`.`custom_actions_members`".$this->PrepareWhere($Where));
	}


	public function GetCitiesForDealers($Action, $Model)
	{
		return $this->Get("SELECT DISTINCT
	c.`city_id` AS cityId,
	c.`name` AS cityName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS ot
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS pd ON pd.`partners_division_id`=ot.`partners_division_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`cities` AS c ON c.`city_id`=pd.`city_id`
WHERE pd.`status`=1
	AND ot.`action_id`=".$Action."
	AND ot.`model_id`=".$Model."
	AND ot.`status`=1
ORDER BY cityName");
	}

	public function GetDealersForCity($Action, $Model, $City)
	{
		return $this->Get("SELECT DISTINCT
	pd.`partners_division_id` AS partnerId,
	pd.`name` AS partnerName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS ot
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS pd ON pd.`partners_division_id`=ot.`partners_division_id`
WHERE pd.`city_id`=".$City."
	AND pd.`status`=1
	AND ot.`action_id`=".$Action."
	AND ot.`model_id`=".$Model."
	AND ot.`status`=1");
	}

	public function GetDealersModel($Action)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`model_id` AS modelId
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models`
WHERE `action_id`=".$Action."
	AND `status`=1;");
	}

	public function GetModels($Action, $ServiceId, $Dealer = null)
	{
		if($Dealer)
			$Query = "SELECT
	tdm.`partners_division_id` AS partnerId,
	m.`model_id` AS modelId,
	m.`name` AS modelName,
	mi.`image` AS modelImage
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=tdm.`submodel_id`
LEFT JOIN `".DBS_REFERENCES."`.`models_images` AS mi ON mi.`model_id`=tdm.`submodel_id`
WHERE tdm.`partners_division_id`=".$Dealer."
	AND tdm.`action_id`=".$Action."
	AND tdm.`status`=1
	AND mi.`service_id`=".$ServiceId;
		else $Query = "SELECT
	m.`model_id` AS modelId,
	m.`name` AS modelName,
	mi.`image` AS modelImage
FROM `".DBS_REFERENCES."`.`custom_actions_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=tdm.`submodel_id`
LEFT JOIN `".DBS_REFERENCES."`.`models_images` AS mi ON mi.`model_id`=tdm.`submodel_id`
WHERE tdm.`action_id`=".$Action."
	AND mi.`service_id`=".$ServiceId;

		return $this->Get($Query);
	}


    public function GetSubmodelsForAction($Action, $Dealer = null, $WitchDealers = null)
    {
        if(SITE_CURRENT == SITE_LEXUS)
        {
            if($Dealer)
                $Query = "SELECT
	tdm.`partners_division_id` AS partnerId,
	m.`submodel_id` AS submodelId,
	m.`model_id` AS modelId,
	m.`name` AS modelName,
    tdm.`comment` AS modelComment
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`submodels` AS m ON m.`submodel_id`=tdm.`submodel_id`
WHERE tdm.`partners_division_id`=".$Dealer."
	AND tdm.`action_id`=".$Action."
	AND tdm.`status`=1";
            else
            {
                if($WitchDealers)
                {
                    $Query = "SELECT
	tdm.`partners_division_id` AS partnerId,
	m.`submodel_id` AS submodelId,
	m.`model_id` AS modelId,
	m.`name` AS modelName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`submodels` AS m ON m.`submodel_id`=tdm.`submodel_id`
WHERE tdm.`action_id`=".$Action."
	AND tdm.`status`=1";
                }
                else
                {
                    $Query = "SELECT
	m.`submodel_id` AS submodelId,
	m.`model_id` AS modelId,
	m.`name` AS modelName,
    tdm.`comment` AS modelComment
FROM `".DBS_REFERENCES."`.`custom_actions_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`submodels` AS m ON m.`submodel_id`=tdm.`submodel_id`
WHERE tdm.`action_id`=".$Action."
ORDER BY m.`order` ASC;";
                }
            }
            return $this->Get($Query);
        }
    }

    public function GetDealersModels($Action, $Dealer = null)
    {
        $Where = array();
        if(SITE_CURRENT == SITE_LEXUS)
        {
            $Query = "SELECT
    `partners_division_id` AS partnerId,
	`submodel_id` AS submodelId
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models`";
        }
        else
        {
            $Query = "SELECT
    `partners_division_id` AS partnerId,
	`model_id` AS modelId
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models`";
        }
        $Where[] = "`action_id`=".$Action;
		$Where[] = "`status`=1";
        if($Dealer)
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealer);
        return $this->Get($Query.$this->PrepareWhere($Where));
    }

	public function GetModelsForAction($Action, $Dealer = null, $WitchDealers = null, $Service = null)
	{
        if(SITE_CURRENT == SITE_LEXUS)
        {
             if($Dealer)
                $Query = "SELECT DISTINCT
    tdm.`partners_division_id` AS partnerId,
	m.`model_id` AS modelId,
	m.`name` AS modelName,
	mi.`image` AS modelImage
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`submodels` AS s ON s.`submodel_id`=tdm.`submodel_id`
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=s.`model_id`
LEFT JOIN `".DBS_REFERENCES."`.`models_images` AS mi ON mi.`model_id`=m.`model_id` AND mi.`service_id`=".$Service."
WHERE tdm.`partners_division_id`=".$Dealer."
	AND tdm.`action_id`=".$Action."
	AND tdm.`status`=1";
            else
            {
                if($WitchDealers)
                {
                    $Query = "SELECT DISTINCT
    tdm.`partners_division_id` AS partnerId,
	m.`model_id` AS modelId,
	m.`name` AS modelName,
	mi.`image` AS modelImage
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`submodels` AS s ON s.`submodel_id`=tdm.`submodel_id`
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=s.`model_id`
LEFT JOIN `".DBS_REFERENCES."`.`models_images` AS mi ON mi.`model_id`=m.`model_id` AND mi.`service_id`=".$Service."
WHERE tdm.`action_id`=".$Action."
	AND tdm.`status`=1";
                }
                else
                {
                    $Query = "SELECT DISTINCT
	m.`model_id` AS modelId,
	m.`name` AS modelName,
	mi.`image` AS modelImage,
    m.`comment` AS modelComment
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`submodels` AS s ON s.`submodel_id`=tdm.`submodel_id`
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=s.`model_id`
LEFT JOIN `".DBS_REFERENCES."`.`models_images` AS mi ON mi.`model_id`=m.`model_id` AND mi.`service_id`
WHERE tdm.`action_id`=".$Action."
	AND tdm.`status`=1
ORDER BY m.`order` ASC;";
                }
            }
        }
        else
        {
            if($Dealer)
                $Query = "SELECT
	tdm.`partners_division_id` AS partnerId,
	m.`model_id` AS modelId,
	m.`name` AS modelName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=tdm.`submodel_id`
WHERE tdm.`partners_division_id`=".$Dealer."
	AND tdm.`action_id`=".$Action."
	AND tdm.`status`=1";
            else
            {
                if($WitchDealers)
                {
                    $Query = "SELECT
	tdm.`partners_division_id` AS partnerId,
	m.`model_id` AS modelId,
	m.`name` AS modelName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=tdm.`submodel_id`
WHERE tdm.`action_id`=".$Action."
	AND tdm.`status`=1";
                }
                else
                {
                    $Query = "SELECT
	m.`model_id` AS modelId,
	m.`name` AS modelName
FROM `".DBS_REFERENCES."`.`custom_actions_models` AS tdm
LEFT JOIN `".DBS_REFERENCES."`.`models` AS m ON m.`model_id`=tdm.`submodel_id`
WHERE tdm.`action_id`=".$Action.";";
                }
            }
        }
		return $this->Get($Query);
	}



    public function SaveOldSubscript($LastName, $FirstName, $Email, $Gender, $Age)
    {
        $this->DB->connect("localhost", "lsub", "jYesAqhi", "lsub");
        $this->DB->SetCharSet("cp1251");
        $this->Exec("INSERT INTO `lsub`.`users`
	(`name`,
	`firstname`,
	`email`,
	`sex`,
	`age`,
	`sub_news`,
    `subdate`)
VALUES
	(".$this->Esc($FirstName).",
    ".$this->Esc($LastName).",
    ".$this->Esc($Email).",
    ".$this->Esc($Gender ? "F" : "M").",
    ".$this->Esc($Age).",
    1,
    NOW());");
    }


    public function GetTemplates($Action, $FormType)
    {
        return $this->Get("SELECT
	`action_id` AS actionId,
	`form_type_id` AS formTypeId,
	`file_name` AS fileName,
    `dealer_send` AS dealerSend,
	`dealer_mail_template` AS dealerMailTemplate,
    `dealer_send_as_html` AS dealerAsHTML,
    `dealer_charset` AS dealerCharset,
	`dealer_mail_subject` AS dealerMailSubject,
	`dealer_mail_form` AS dealerMailForm,
    `dealer_from_name` AS dealerMailFormName,
    `user_send` AS userSend,
	`user_mail_template` AS userMailTemplate,
    `user_send_as_html` AS userAsHTML,
    `user_charset` AS userCharset,
	`user_mail_subject` AS userMailSubject,
	`user_mail_form` AS userMailForm,
    `user_from_name` AS userMailFormName,
    `tmr_send` AS tmrSend,
	`tmr_mail_template` AS tmrMailTemplate,
    `tmr_send_as_html` AS tmrAsHTML,
    `tmr_charset` AS tmrCharset,
	`tmr_mail_subject` AS tmrMailSubject,
	`tmr_mail_form` AS tmrMailForm,
    `tmr_from_name` AS tmrMailFormName,
	`tmr_attachment_mail_template` AS tmrAttachmentMailTemplate,
    `tmr_attachment_mail_name` AS tmrAttachmentMailTemplateName
FROM `".DBS_REFERENCES."`.`custom_actions_forms`
WHERE `action_id`=".$Action."
	AND `form_type_id`=".$FormType.";", true);
    }

    public function GetUniversalEmail($Action, $FormType, $Type, $Category)
    {
        $Query = "SELECT
	`email`,
	`send_as` AS sendAs
FROM `".DBS_REFERENCES."`.`custom_actions_universal_emails`
WHERE `action_id`=".$Action."
	AND `form_type_id`=".$FormType."
    AND `type` IN (".($Type ? 2 : 1).", 3)
	AND `category`".$this->PrepareValue($Category).";";
        return $this->Get($Query);
    }


    public function GetMembers($Brand, $Action, $Dealer = null, $Member = null)
    {
        $Query = "SELECT
	a.`member_id` AS memberId,
	a.`custom_actions_id` AS actionId,
	a.`partners_division_id` AS partnerId,
	a.`model_id` AS modelId,
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	a.`gender` AS genderId,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
	a.`age` AS age,
	a.`email` AS email,
	IF(a.`phone` IS NOT NULL AND a.`phone`<>'', CONCAT('+7 ', a.`phone`), '-') AS phone,
	a.`is_owner`,
	IF(a.`is_owner`=1, 'Да', 'Нет') AS isOwner,
	a.`register` AS registredU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS registred,
	a.`status` AS `status`,
	a.`is_test` AS isTest,
	-- a.`middle_name`,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a";

        $Where = array();
        if($Member)
            $Where[] = "a.`member_id`=".$Member;
        else
        {
            $Where[] = "a.`custom_actions_id`=".$Action;
            if($Dealer)
                $Where[] = "a.`partners_division_id`".$this->PrepareValue($Dealer);
        }
        return $this->Get($Query.$this->PrepareWhere($Where), $Member);
    }

    public function GetMembersForEmail($Member)
    {
        return $this->Get("SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	a.`other_info` AS advancedInfo,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`advanced_flag` AS advancedFlag,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    DATE_FORMAT(a.`visit_time`, '%H:%i:%s') AS memberVisitTime,
	a.`is_test` AS isTest,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a
WHERE a.`member_id`=".$Member.";", true);
    }

    public function GetMembersForReport($Action, $FormType, $Dealers, $Model, $RegisterStart, $RegisterEnd)
    {
        switch($Action)
        {
            case ACTIONS_TEST_DRIVES:
                $Query = "SELECT
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
    a.`age` AS age,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName,
	a.`email` AS email,
	IF(a.`phone` IS NOT NULL AND a.`phone`<>'', CONCAT('+7 ', a.`phone`), '-') AS phone,
	IF(a.`is_owner`=1, 'Да', 'Нет') AS isOwner,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS registred,
	a.`advanced_flag` AS `type`,
	a.`member_id` AS memberId
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a";
                break;
            case ACTIONS_MODEL_ORDERS:
                if($FormType == 1)
                    $Query = "SELECT
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
    a.`age` AS age,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName,
	a.`email` AS email,
	IF(a.`phone` IS NOT NULL AND a.`phone`<>'', CONCAT('+7 ', a.`phone`), '-') AS phone,
	IF(a.`is_owner`=1, 'Да', 'Нет') AS isOwner,
    a.`current_model_name` AS currentModelName,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS registred
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a";
                else $Query = "SELECT
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName,
	a.`email` AS email,
	IF(a.`phone` IS NOT NULL AND a.`phone`<>'', CONCAT('+7 ', a.`phone`), '-') AS phone,
	IF(a.`is_owner`=1, 'Да', 'Нет') AS isOwner,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS registred
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a";
                break;
            case ACTIONS_MODEL_ORDERS_SPEC:
                $Query = "SELECT
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
	a.`partners_division_name` AS partnerName,
	a.`model_name` AS modelName,
	a.`email` AS email,
	IF(a.`phone` IS NOT NULL AND a.`phone`<>'', CONCAT('+7 ', a.`phone`), '-') AS phone,
	IF(a.`is_owner`=1, 'Да', 'Нет') AS isOwner,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS registred
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a";
                break;
        }


        $Where = array();
        $Where[] = "a.`custom_actions_id`=".$Action;
        $Where[] = "a.`form_type_id`=".$FormType;
		$Where[] = "a.`is_test`=0";
        if($Dealers)
            $Where[] = "a.`partners_division_id`".$this->PrepareValue($Dealers);
        if($RegisterStart)
            $Where[] = "DATE(a.`register`) >= ".$this->Esc($RegisterStart);
        if($RegisterEnd)
            $Where[] = "DATE(a.`register`) <= ".$this->Esc($RegisterEnd);
        if($Model)
            $Where[] = "a.`model_id`".$this->PrepareValue($Model);
        return $this->Get($Query.$this->PrepareWhere($Where));
    }

    public function GetActionName($Action)
    {
        return $this->Count("SELECT `name` AS Cnt
FROM `".DBS_REFERENCES."`.`custom_actions`
WHERE `action_id`=".$Action.";");
    }

	public function GetForReSendTestDrives()
	{
		 $Query = "SELECT
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
    a.`from_dealers_site` AS fromDealersSite,
    a.`from_mobile` AS fromMobile,
	a.`partners_division_id` AS partnerId,
	a.`model_id` AS modelId
FROM `".DBS_REFERENCES."`.`custom_actions_members_fn` AS a
WHERE a.`custom_actions_id`=1
	AND a.`is_test`=0
	AND DATE(a.`register`) >= '2014-09-22'
	AND DATE(a.`register`) <= '2014-09-30'";
        return $this->Get($Query);
	}
}