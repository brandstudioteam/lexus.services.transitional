<?php
class StateCodeValidate
{
	static public function INNUL($INN)
	{
		$INN = trim($INN);
		if(!preg_match("/^[0-9]{10,10}$/", $INN))
			$INN = false;
		else
		{
			$R = Strings::Left($INN, 9);
			$R = 2 * $R{0} + 4 * $R{1} + 10 * $R{2} + 3 * $R{3} + 5 * $R{4} + 9 * $R{5} + 4 * $R{6} + 6 * $R{7} + 8 * $R{8};
			$INN = (($R - floor($R / 11) * 11) == $INN{9}) ? $INN : false;
		}
		return $INN;
	}
	
	static public function INNIP($INN)
	{
		$INN = trim($INN);
		if(!preg_match("/^[0-9]{12,12}$/", $INN))
			$INN = false;
		else
		{
			$R = Strings::Left($INN, 10);
			$R = 7 * $R{0} + 4 * $R{1} + 4 * $R{2} + 10 * $R{3} + 3 * $R{4} + 5 * $R{5} + 9 * $R{6} + 4 * $R{7} + 6 * $R{8} + 8 * $R{9};
			if(($R - floor($R / 11) * 11) == $INN{11})
			{
				$R = Strings::Left($INN, 11);
				$R = 3 * $R{0} + 7 * $R{1} + 2 * $R{2} + 4 * $R{3} + 10 * $R{4} + 3 * $R{5} + 5 * $R{6} + 9 * $R{7} + 4 * $R{8} + 6 * $R{9} + 8 * $R{10};
				$INN = (($R - floor($R / 11) * 11) == $INN{11}) ? $INN : false;
			}
			else $INN = false;
		}
		return $INN;
	}
	
	static function INN($INN)
	{
		$R = strlen(trim($INN));
		if($R == 10)
			$R = self::INNUL($INN);
		elseif($R == 12)
			$R = self::INNIP($INN);
		else $R = false;
		return $R;
	}
	
	static public function OGRNUL($OGRN)
	{
		$OGRN = trim($OGRN);
		if(!preg_match("/^[0-9]{13,13}$/", $OGRN))
			$OGRN = false;
		else
		{
			if(in_array($OGRN{0}, array(1, 2, 3, 5)) && intval("20".$OGRN{1}.$OGRN{2}) <= intval(DTs::GetYear()))
			{
				$R = Strings::Left($OGRN, 12);
				$OGRN = (($R - floor($R / 11) * 11) == $OGRN{12}) ? $OGRN : false;
			}
			else $OGRN = false;
		}
		return $OGRN;
	}
	
	static public function OGRNIP($OGRN)
	{
		$OGRN = trim($OGRN);
		if(!preg_match("/^[0-9]{15,15}$/", $OGRN))
			$OGRN = false;
		else
		{
			if($OGRN{0} == "3" && intval("20".$OGRN{1}.$OGRN{2}) <= intval(DTs::GetYear()))
			{
				$R = Strings::Left($OGRN, 14);
				$OGRN = (($R - floor($R / 13) * 13) == $OGRN{14}) ? $OGRN : false;
			}
			else $OGRN = false;
		}
		return $OGRN;
	}
	
	static public function OGRN($OGRN)
	{
		$R = strlen(trim($OGRN));
		if($R == 13)
			$R = self::OGRNUL($OGRN);
		elseif($R == 15)
			$R = self::OGRNIP($OGRN);
		else $R = false;
		return $R;
	}
	
}