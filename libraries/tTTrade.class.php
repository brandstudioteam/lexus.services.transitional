<?php
class tTTrade extends  Controller
{
	/**
	 * 
	 * @var tTTrade
	 */
	private static $Inst = false;
 
	protected function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * 
	 * Инициализирует класс
	 * @return tTTrade
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"dealerRCodeHash"	=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_MD5),
				),
				"hash"	=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_MD5),
				),
				"start"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"end"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"site"	=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_WORD_EN),
				)
			)
		);
		$this->Modes = array(
			//Возвращает список городов, в которых есть дилеры, участвующие в заказе
			"a"		=> array(
				"exec"		=> array("tTTradeProcessor", "GetFile"),
				"TplVars"		=> array("hash" => 1, "start" => 1, "end" => 1, "site" => 0)
			),
			"b"		=> array(
				"exec"		=> array("tTTradeProcessor", "GetPage"),
				"TplVars"		=> array("hash" => 1, "site" => 0)
			),
		);
	}
}