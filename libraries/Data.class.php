<?php
/**
 *
 * Базовый класс классов данных
 *
 * Содержит методы-утилиты для упрощения типовой работ классов данных с Моделью данных,
 * а также методы-трансляторы.
 *
 * Расширения:
 * Добавлены методы-трансляторы: Exec, Next, Prew, Move, First, Last, Begin, Commit, Rollback, Esc.
 * Добавлены транслируемые свойства Count, Current, Index
 *
 * Получения массива записей без необходимости их последующей обработки, например, отправки клиенту как есть,
 * используется метод Get без параметров $OneValue и $NoReturned.
 * При необходимости обработки массива записей используется метод Get с параметрами $OneValue = false и $NoReturned = true.
 * В этом случае метод не будет возвращать данные. Для обработки записей классах объектной модели (логики) используется контрукция:
 * 		while ($this->CData->Next())
 *		{
 *			echo $this->CData->Current["field_name"];
 *		}
 *
 * @author Игорь (borschenkov@inbox.ru)
 * @version 1.2
 * Версия предназначена для использования совместно с унифицированными классами работы с БД
 * DB версии 1.1 и выше и DBi версии 1.0 и выше.
 * @package ObjectModelEngine
 *
 */
class Data extends Base
{
	/**
	 *
	 * Содержит объект DBi
	 * @var DBi
	 */
	protected $DB;

	public function __construct()
	{
		parent::__construct();
		$this->DB = WS::Init()->DB;
	}

	/**
	 *
	 * Подготавливает аргумент для использовании в запросе в части WHERE
	 *
	 * @param mixed $Value - значение, массив значений или срока значений, разделенных запятыми
	 * @param bool $IsString - флаг, означающий, что значения строковые
	 * @param integer $AddOp - определяет оператор сравнения.
	 * Возможные значения:
	 * 1 - оператор равенства ("=", "IN", "IS"),
	 * 2 - оператор неравенства ("<>", "NOT IN", "NOT IS"),
	 * @param boolean $IsNull - флаг, определяющий может ли параметр быть пустым.
	 * Если флаг установлен, то пустое значение или значение null будет преобразовано в строку NULL.
	 * Если флаг не установлен, то в массивах значений пустые значения или значения равные null будут опущены.
	 * При этом, если значение одно или все значения массива значений пустые или равны null будет выброшне исключение.
	 * @throws Если в параметре $Value нет ни одной строки или все строки пустые или имеют значение null и флаг $IsNull не установлен,
	 * будет выброшено исключение с номером 5000.
	 * @example
	 * Для числовых полей: "SELECT a.`field_1` AS fld1 FROM `table_1` AS a WHERE a.`field_2`".$this->PrepareValue($IntValues).";";
	 * Результат выполнения для одиночного значения $IntValues:
	 * "SELECT a.`field_1` AS fld1 FROM `table_1` AS a WHERE a.`field_2`=<IntValues>;",
	 * для множественных значений $IntValues:
	 * "SELECT a.`field_1` AS fld1 FROM `table_1` AS a WHERE a.`field_2` IN (<IntValues_1>, ... <IntValues_N>);".
	 *
	 * Для строковых полей: "SELECT a.`field_1` AS fld1 FROM `table_1` AS a WHERE a.`field_2`".$this->PrepareValue($StriingValues, true).";";
	 * Результат выполнения для одиночного значения $IntValues:
	 * "SELECT a.`field_1` AS fld1 FROM `table_1` AS a WHERE a.`field_2`='<StringValues>';",
	 * где <StringValues> - строка с экранированными спецсимволами;
	 * для множественных значений $IntValues:
	 * "SELECT a.`field_1` AS fld1 FROM `table_1` AS a WHERE a.`field_2` IN ('<StringValues>', ... '<StringValues>');",
	 * где <StringValues>, <StringValues> - строки с экранированными спецсимволами.
	 *
	 * ВНИМАНИЕ!
	 * Пустые строки и строки со значением null в параметре $Value будут удалены.
	 * Если в параметре $Value нет ни одной строки или все строки пустые или имеют значение null,
	 * будет выброшено исключение с номером 5000.
	 */
	final protected function PrepareValue($Value, $IsString = false, $AddOp = 1, $IsNull = false, $NotEmpty = null, $Quote = null)
	{
		if(($Value === null || is_empty($Value)) && !$IsNull) throw new dmtException("Argument value is error", 5000);
		if(!is_array($Value)) $Value = explode(",", $Value);
		$Value = array_unique($Value);
		$Val = array();
		foreach ($Value as $v)
			if(is_empty($v) && $v === null)
			{
				if($IsNull) $Val[] = "NULL";
			}
                                                //$Value, $AddQuote = true, $SetNull = true, $NotEmpty = false, $Trim = true, $Quote = null
			else $Val[] = $IsString ? $this->Esc($v, true, false, $NotEmpty, true, $Quote) : $v;
		if(!sizeof($Val))
			throw new dmtException("Argument value is error", 5000);
		if(sizeof($Val) > 1)
			$Value = (!$AddOp ? "" : ($AddOp == 1 ? " IN" : " NOT IN"))." (".implode(", ", $Val).") ";
		elseif($Val[0] == "NULL") $Value = ($AddOp == 1 ? " IS " : " IS NOT ").$Val[0];
		else $Value = ($AddOp == 1 ? "=" : "<>").$Val[0];
		return $Value;
	}


	final protected function PrepareWhere($Where, $ORConcat = null)
	{
		return is_array($Where) && sizeof($Where) ? "
WHERE ".implode($ORConcat ? " OR " : " AND ", $Where) : "";
	}

	/**
	 *
	 * Проверяет значение и если оно пустое или равно null возвращает строку "NULL", иначе неизменное значение.
	 *
	 * Данный метод используется только для единичных числовых значений
	 * в конструкциях INSERT и UPDATE в частях VALUES или SET, соответственно.
	 * В конструкции SELECT рекомендуется использовать только для вычисляемых значений с параметром $SetNull.
	 * В предложении WHERE для условия с единичним значением и возможным нулевым значением аргумента условия
	 * используется с параметром $SetNull.
	 * Для строковых значений используется метод Esc.
	 * Для предложения WHERE с возможными множественными значениями
	 * используется метод PrepareValue для числовых и строковых значений
	 *
	 * @param numeric $Value
	 * @param integer $AddOp - добавляет оператор сравнения.
	 * Результаты использования, если проверяемое значение пустое или равно null:
	 * 1 - добавляет оператор IS, возвращемое значение " IS NULL",
	 * 2 - добавляет опероторы IS NOT, возвращемое значение " IS NOT NULL",
	 * если параметр не установлен или эквивалентен null, вернется "NULL".
	 * Результаты использования, если проверяемое значение не пустое и не равно null:
	 * 1 - добавляет оператор =, возвращемое значение "=$Value",
	 * 2 - добавляет оперотор <>, возвращемое значение "<>$Value",
	 * если параметр не установлен или эквивалентен null, вернется $Value.
	 * @return mixed значение $Value или строковое значение "NULL".
	 */
	final protected function CheckNull($Value, $AddOp = null, $NotNull = false, $NotEmpty = false)
	{
		if(is_empty($Value) || $Value === null)
		{
			if($NotNull && $Value === null) throw new dmtException("Argument must be not null");
			if($NotEmpty && is_empty($Value)) throw new dmtException("Argument must be not empty");
			if($AddOp == 1)
				$Value = " IS NULL";
			elseif($AddOp == 2)
				$Value = " IS NOT NULL";
			else $Value = "NULL";
		}
		else
		{
			if($AddOp == 1)
				$Value = "=".$Value;
			elseif($AddOp == 2)
				$Value = "<>".$Value;
		}
		return $Value;
	}

	/**
	 *
	 * Экранирует спецсимволы строковых значений
	 *
	 * Рекомендуется использовать после разбиения строки значений, разделенных запятыми, т.е. после преобразования в массив.
	 *
	 * @param mixed $Value - строковое значение или массив строковых значений.
	 * @param boolean $AddQuote флаг заключения значения в одиночные кавычки.
	 * По-умолчанию значение заключается в кавычки.
	 * @param boolean $SetNull - флаг необходимости установки "NULL" вместо пустого или нулевого (null) значения строки.
	 * По-умолчанию устанавливается "NULL".
	 * @param boolean $NotEmpty - флаг необходимости удаления пустых строк.
	 * По-умолчанию строки не удаляются. Имеет более высокий приоритет, чем $SetNull.
	 * Рекомендуется использовать параметр со значение true для обработки значений в предложении WHERE.
	 * @param boolean $Trim  - Флаг удаления концевых пробелов. Необязательный. По умолчанию равен true
	 * @throws Если в параметре $Value нет ни одной строки или все строки пустые или имеют значение null и параметр $NotEmpty эквивалентен true,
	 * будет выброшено исключение с номером 5000.
	 */
	final protected function Esc($Value, $AddQuote = true, $SetNull = true, $NotEmpty = false, $Trim = true, $Quote = null)
	{
		$Arr = is_array($Value);
		if(!$Arr)
			$Value = array($Value);
		$V = array();
        if(!$Quote)
            $Quote = "'";
		for($j = 0; $j < sizeof($Value); $j++)
			if(!is_empty($Value) && $Value !== null)
			{
				if($Trim) $Value[$j] = trim($Value[$j]);
				$V[$j] = $this->DB->Esc($Value[$j]);
				if($AddQuote) $V[$j] = $Quote.$V[$j].$Quote;
			}
			elseif($NotEmpty) continue;
			elseif($SetNull) $V[$j] = "NULL";
		if(empty($V)) throw new dmtException("Parameter \$Value is empty", 5000);
		return $Arr ? $V : $V[0];
	}

	/**
	 *
	 * Возвращает результата запроса
	 * @param string $Query - строка запроса
	 * @param boolean $OneValue - флаг, означающий необходимо ли вернуть единичное значение (true) или массив (false).
	 * @param boolean $NoReturned - флаг, означающий, что метод не должен возвращать данные.
	 * Следует установить значение эквивалентное true при необходимости дальнейшей обработки массивы записей.
	 * По-умолчанию возвращается массив
	 */
	final protected function Get($Query, $OneValue = false, $NoReturned = false)
	{
		$R = $this->DB->OpenQuery($Query, !$OneValue && !$NoReturned);
		if(!$this->DB->Count) $R = $OneValue ? null : ($NoReturned ? null : array());
		elseif($OneValue) $R = $this->DB->Current;
		return $R;
	}

	/**
	 *
	 * Возвращает количество записей из запроса.
	 *
	 * Запрос должен содержать в части SELECT: COUNT(<field_name>) AS Cnt
	 * ВНИМАНИЕ! В запросах нельзя использовать LIMIT, кроме специяльных случаев!
	 *
	 * @param string $Query
	 */
	final protected function Check($Query)
	{
		$R = $this->Get($Query, true);
		return ($R["Exs"] > 0);
	}

	/**
	 *
	 * Возвращает количество записей из запроса.
	 *
	 * Запрос должен содержать в части SELECT:  COUNT(<field_name>) AS Cnt
	 * Данный метод можно использовать также для получения записи с максимальным значением поля, используя в запросе
	 * в части SELECT:  MAX(<field_name>) AS Cnt
	 * ВНИМАНИЕ! В запросах нельзя использовать LIMIT, кроме специяльных случаев!
	 *
	 * @param string $Query
	 */
	final protected function Count($Query)
	{
		$R = $this->Get($Query, true);
		return $R["Cnt"];
	}

	/**
	 *
	 * Преобразует единичное значение или строку значений, разделенную запятыми, в массив
	 * @param mixed $Arg - проверяемый аргумент
	 */
	final protected function GetArrArg($Arg)
	{
		if(!is_array($Arg))
		{
			if(mb_strpos($Arg, ",")) $Arg = explode(",", $Arg);
			else $Arg = array($Arg);
		}
		return $Arg;
	}

	/**
	 *
	 * Проверяет тип аргумента. Если аргумент массив или строка значений, разделенных запятыми, возвращает true, иначе - false.
	 * @param mixed $Arg - проверяемый аргумент
	 */
	final protected function CheckArg($Arg)
	{
		return (is_array($Arg) && sizeof($Arg) > 1 || !is_array($Arg) && mb_strpos($Arg, ",")) ? true : false;
	}

	/**
	 *
	 * Устанавливает произвольную пользовательскую пременную БД
	 * @param string $Name - наименование переменной
	 * @param mixed $Value - значение переменной
	 */
	final protected function Set($Name, $Value)
	{
		if(mb_strpos($Name, "@") === false) $Name = "@".$Name;
		$this->DB->Exec("SET ".$Name."=".($Value === null ? "NULL" : (is_int($Value) ? $Value : $this->Esc($Value))).";");
	}

	/**
	 *
	 * Устанавливает пользовательскую переменную БД, содержащую значение текущего времени
	 * @param datetime $Timestamp
	 */
	final protected function SetDT($Timestamp = null)
	{
		if($Timestamp) $Query = "SET ".DB_VARS_PREVIEW_DATE."=UNIX_TIMESTAMP('".$Timestamp."');";
		else $Query = "SET ".DB_VARS_CURRENT_DATE."=(NOW() + 0);";
		$this->DB->Exec($Query);
	}

	/**
	 *
	 * Проверяет значение, содержащее идентификатор пользователя, которое должно использоваться в предложении WHERE sql-запроса.
	 * Если значение пустое, вместо него подставляется переменная текущей сессии подключения @CurrUser, устанавливаемая классом User
	 *
	 * @param integer $User
	 */
	final protected function CheckAccount($Account)
	{
		return $Account ? $Account : DB_VARS_ACCOUNT_CURRENT;
	}

	/**
	 *
	 * Преобразует в формат времени числовое значение в секундах
	 * @param int $Time - время в секундах
	 * @param bool $AsString - вернуть как строку, если аргумент эквивалентен true
	 * @return mixed array Массив формата: array(0 => <hour>, 1 => <minutes>, 2 => <seconds>)
	 */
	final protected function FormatTime($Time, $AsString = true)
	{
		if(!is_int(intval($Time))) return false;
		$R = array();
		//Определяем часы
		if($Time >= 3600)
		{
			$T = $Time / 3600;
			$T = floor($T);
			$R[0] = $T;
			$Time = $Time - $T * 3600;
		}
		else $R[0] = 0;
		//Определяем минуты
		if($Time >= 60)
		{
			$T = $Time / 60;
			$T = floor($T);
			$R[1] = $T;
			$Time = $Time - $T * 60;
		}
		else $R[1] = 0;
		$R[2] = $Time;
		return $AsString ? date("H:i:s", mktime($R[0], $R[1], $R[2], 0, 0, 0)) : $R;
	}

	/**
	 *
	 * Возвращает текущий указатель записи в наборе возвращенных записей
	 * @param string $Name - имя запроса
	 */
	protected function GetIndex($Name = 0)
	{
		return $this->DB->GetIndex($Name);
	}

	/**
	 *
	 * Возвращает текущую запись
	 * @param string $Name - имя запроса
	 */
	protected function GetCurrent($Name = 0)
	{
		return $this->DB->GetCurrent($Name);
	}

	/**
	 *
	 * Возвращает количество записей в результате
	 * @param string $Name - имя запроса
	 */
	protected function GetCount($Name = 0)
	{
		return $this->DB->GetCount($Name);
	}

	/**
	 * Транслятор к методу MoveNext класса работы с СУБД.
	 *
	 * Устанавливает указатель на следующую запись и выбирает ее в Current
	 */
	final public function Next($Name = 0)
	{
		return $this->DB->MoveNext($Name);
	}

	/**
	 * Транслятор к методу MovePrevious класса работы с СУБД.
	 *
	 * Перемещает указатель на предыдущую запись и выбирает ее как текущую запись
	 */
	final public function Prew($Name = 0)
	{
		return $this->DB->MovePrevious($Name);
	}


	/**
	 * Транслятор к методу MoveNext класса работы с СУБД.
	 *
	 * Перемещает указатель на запись с индексом $Index и выбирает ее как текущую запись
	 *
	 * @param integer $Index - индекс записи в пределах от 0 до количества записей минус 1.
	 * @return boolean true - в случае успешного перемещения указателя,
	 * и false - в случае, если индекс выходит за пределы области определения, т.е. больше количества записей минус 1, или меньше 0.
	 */
	final public function Move($Index, $Name = 0)
	{
		return $this->DB->Move($Index, $Name);
	}


	/**
	 * Транслятор к методу MoveNext класса работы с СУБД.
	 *
	 * Перемещает указатель на первую запись и выбирает ее как текущую запись
	 */
	final public function First($Name = 0)
	{
		return $this->DB->MoveFirst($Name);
	}

	/**
	 * Транслятор к методу MoveNext класса работы с СУБД.
	 *
	 * Перемещает указатель на последнюю запись и выбирает ее как текущую запись
	 */
	final public function Last($Name = 0)
	{
		return $this->DB->MoveLast($Name);
	}

	/**
	 * Транслятор к методу Exec класса работы с СУБД.
	 *
	 * Выполняет запросы, не возвращающие записей
	 * @param string $Query - SQL-запрос
	 * @throws выбрасывается исключение при пустом значении $Query и в случае ошибки (через метод SetError класса работы с СУБД);
	 */
	final protected function Exec($Query)
	{
		if(!$Query) throw new dmtException("Query string is empty");
		return $this->DB->Exec($Query);
	}

	/**
	 *
	 * Транслятор к методу Begin класса работы с СУБД.
	 *
	 * Открывавет транзакцию.
	 */
	final protected function Begin($Isolation = 4)
	{
		$this->DB->Begin($Isolation);
	}

	/**
	 *
	 * Транслятор к методу Commit класса работы с СУБД.
	 *
	 * Фиксирует транзакцию.
	 */
	final protected function Commit()
	{
		$this->DB->Commit();
	}

	/**
	 *
	 * Транслятор к методу Rollback класса работы с СУБД.
	 *
	 * Отменяет транзакцию.
	 */
	final protected function Rollback()
	{
		$this->DB->Rollback();
	}

	final public function __get($Name)
	{
		$Name = mb_strtolower($Name);
		if($Name == "count")
			$R = $this->DB->GetCount();
		elseif($Name == "current")
			$R = $this->DB->GetCurrent();
		elseif($Name == "index")
			$R = $this->DB->GetIndex();
	}

	protected function AccessDenid()
	{
		throw new dmtException("Access denid", 77777);
	}

    protected function IsValue($Value)
    {
        return $Value !== null && $Value !== "";
    }
}