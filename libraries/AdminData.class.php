<?php
class AdminData extends Data
{
	public function GetServicesForAdmin($User = null)
	{
		return $this->Get("SELECT DISTINCT
	s.`service_id` AS serviceId,
	s.`name` AS serviceName,
	s.`description` AS serviceDescription,
	ss.`host` AS serviceHost,
	ss.`admin_path` AS serviceAdminPath
FROM `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS rp
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles` AS ur ON ur.`role_id`=rp.`role_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS p ON p.`permission_id`=rp.`permission_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services` AS s ON s.`service_id`=p.`service_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services_sets` AS ss ON ss.`service_id`=s.`service_id` AND ss.`brand_id`=".DB_VARS_BRAND_CURRENT."
WHERE ur.`user_id`=".($User ? $User : DB_VARS_ACCOUNT_CURRENT)."
	AND s.`service_id` <> 8
	AND s.`is_public`=1
ORDER BY serviceName");
	}

    public function GetServicesActions($Service, $Status = null)
    {
        return $this->Get("SELECT DISTINCT
	a.`service_action_id` AS serviceActionId,
	a.`service_id` AS serviceId,
	a.`name` AS serviceActionName,
	a.`description` AS serviceActionDescription,
	CONCAT(e.`admin_path`, a.`path`) AS serviceActionPath
FROM `".DBS_UNIVERSAL_REFERENCES."`.`services_actions` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS b ON b.`service_action_id`=a.`service_action_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS c ON c.`permission_id`=b.`permission_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_roles` AS d ON d.`role_id`=c.`role_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services` AS s ON s.`service_id`=a.`service_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services_sets` AS e ON e.`service_id`=a.`service_id` AND e.`brand_id`=".DB_VARS_BRAND_CURRENT."
WHERE d.`user_id`=".DB_VARS_ACCOUNT_CURRENT."
    AND a.`service_id`=".$Service."
	AND s.`is_public`=1
    AND a.`status`=".($Status ? $Status : 1).";");
    }

    public function GetServicesActionsForAdmin($Brand)
    {
        return $this->Get("SELECT DISTINCT
	a.`service_action_id` AS actionId,
	a.`service_id` AS serviceId,
	a.`name` AS actionName,
	a.`description` AS actionDescription,
	a.`path` AS actionPath,
    a.`default` AS actionDefault
FROM `".DBS_UNIVERSAL_REFERENCES."`.`services_actions` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`permissions` AS b ON b.`service_action_id`=a.`service_action_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_permissions` AS c ON c.`permission_id`=b.`permission_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles` AS d ON d.`role_id`=c.`role_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services` AS e ON e.`service_id`=a.`service_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`services_actions_brands` AS f ON f.`service_action_id`=a.`service_action_id`
WHERE d.`user_id`=".DB_VARS_ACCOUNT_CURRENT."
	AND e.`is_public`=1
    AND a.`status`=1
	AND f.`brand_id`".$this->PrepareValue($Brand).";");
    }
}