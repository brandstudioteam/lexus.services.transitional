<?php
class Templater extends Processor
{
	/**
	 * 
	 * @var Templater
	 */
	protected static $Inst;
	
	protected $Template;
	
	protected $Data;
	
	protected $Constructions;
	
	protected $Collections = array();
	
	protected $Templater;
	
	
	/**
	 * 
	 * Инициализирует класс
	 * @return Templater
	 */
    public static function Init($Template = null)
    {
		if(!self::$Inst) self::$Inst = new self($Template);
		return self::$Inst;
    }
    
    protected function __construct($Template = null)
    {
    	parent::__construct();
    	$this->Sets();
    	if($Template) $this->SetTemplate($Template);
    }
	
    /**
     * 
     * Enter description here ...
     */
    protected function Sets()
    {
    	$this->Constructions = array(
    		"item"			=> array("Templater", "Item"),
    		"selectoption"	=> array("Templater", "SelectOption"),
    		"condition"		=> array("Templater", "ConditionView"),
    	);
    }
    
    
    
    
    public function SetTemplate($Template)
    {
    	$this->Template = $Template;
    }
    
	public function Merge($Template = null, $Data = null, $Message = null, $Errors = null, $TemplateString = null)
	{
		if($TemplateString)
		{
			$Template = $TemplateString;
		}
		else
		{	
			if(!$Template) $Template = $this->Template;
			$Template = file_get_contents($Template);
		}
     	if(!$Data)
     		$Data = array();
     	elseif(!is_array($Data))
     		$Data = array($Data);
     	if($Message) $Data["SysMessage"] = $Message;
     	if($Errors) $Data["Errors"] = $Errors;
     	$this->Data = $Data;
     	$Template = $this->PrepareTemplate($Template);
     	$Data = self::ArrayToPath($Data, null, null, true);
     	
     	
     	
     	$Template = preg_replace_callback('/templater_info\s*::\s*start(.*?)templater_info\s*::\s*end/uims', array("Templater", "ConfigTemplater"), $Template);
     	if($this->Templater)
     	
		if($Data)
		{
     		//$Template = mb_str_replace($Data["keys"], $Data["values"], $Template);
     		
     		$PreparedTemplate = addcslashes($Template, '"');
			$PreparedTemplate = "\$Template=\"".preg_replace('/(?:%\{%){1}([^(%\}%)]+)(?:%\}%){1}/sui', "\".(isset(\$Data[\"$1\"]) ? \$Data[\"$1\"] : \"\").\"", $PreparedTemplate)."\";";

			eval($PreparedTemplate);
     		
     		
		}
     	$Template = preg_replace('/(?:%\{%){1}[^(%\{%)]+(?:%\}%){1}/sui', "", $Template);
     	$Template = preg_replace('/#%#START::([\w\d-_]+)((?::\w+=[_\w\d\-.*]+)*)#%#(.+?)#%#END::\1#%#/uims', "", $Template);
     	return $Template;
	}
    
	protected function ConfigTemplater($Text)
	{
		if(!$Text) return "";
$this->Dump(__METHOD__.": ".__LINE__, "Text = ", $Text);
		$Text = explode("\n", trim($Text));
		if(preg_match_all('/\s*TEMPLATER\s*=(.*?)\n/uims', $Text, $Z))
		{
			if(isset($Z[1][0]) && trim($Z[1][0]) == "Templater2")
			{
				$this->Templater = Templater2::Init();
			}
		}
	}
	
	
	public static function ArrayToPath($Array, $Prefix = null, $Postfix = null, $AsArray = null)
	{
		return self::ArrayToPathEx($Array, $Prefix, $Postfix, $AsArray);
	}
	
	protected static function ArrayToPathEx($Values, &$Prefix = null, &$Postfix = null, $AsArray = null, $Key = null, &$Path = null)
	{
		if(!is_array($Path))
		{
			if($AsArray)
			{
				$Path = array();
			}
			else
			{
				$Path = array(
					"keys"		=> array(),
					"values"	=> array()
				);
			}
		}
try {
		if($Values)
		{
			if($AsArray)
			{
				foreach ($Values as $k => $v)
				{
					if(is_array($v))
						self::ArrayToPathEx($v, $Prefix, $Postfix, $AsArray, ($Key ? $Key.".".$k : $k), $Path);
					else
						$Path[($Prefix ? $Prefix : "").($Key ? $Key.".".$k : $k).($Postfix ? $Postfix : "")] = $v;
				}
			}
			else
			{
				foreach ($Values as $k => $v)
				{
					if(is_array($v))
						self::ArrayToPathEx($v, $Prefix, $Postfix, $AsArray, ($Key ? $Key.".".$k : $k), $Path);
					else
					{
						$Path["keys"][] = ($Prefix ? $Prefix : "").($Key ? $Key.".".$k : $k).($Postfix ? $Postfix : "");
						$Path["values"][] = $v;
					}
				}
			}
		}			
}
catch(dmtException $e)
{
	GetDebug()->Dump($Values, $Key, $Path);
}
		return $Path;
	}
	
	
	protected function PrepareTemplate($Template)
	{
//$this->Dump(__METHOD__);
//$Start = microtime(true);
		$R = preg_replace_callback('/#%#START::([\w\d-_]+)((?::\w+=[_\w\d\-.*]+)*)#%#(.+?)#%#END::\1#%#/uims', array("Templater", "DoTemplate"), $Template);
//$this->Debug("Templater PREPATE TEMPLATE ALL = ".(microtime(true) - $Start));
		return $R;
	}
	
	protected function DoTemplate($Text)
	{
		$R = array();
		if($Text[2])
		{
			$Info = explode(":", $Text[2]);
			array_shift($Info);
			$Keys = array();
			foreach ($Info as $v)
			{
				if(mb_substr($v, 0, 1) == ":")
					$v = mb_substr($v, 1);
				$v = explode("=", $v);
				$Keys[mb_strtolower($v[0])] = $v[1];
			}
			if(array_key_exists("type", $Keys) && array_key_exists(mb_strtolower($Keys["type"]), $this->Constructions))
			{
				$Type = $this->Constructions[mb_strtolower($Keys["type"])];
				unset($Keys["type"]);
				$R[] = call_user_func($Type, $Text[3], $Keys);
			}
		}
		else throw new dmtException("Tmplate error in block ".$v[1]);
		return implode("", $R);
	}
	
	
	
	
	
	protected function Item($Template, $Options = null)
	{

		if(!$Options)
			throw new dmtException("Template error");
		$this->PrepareTemplate($Template);
		
//$Start = microtime(true);
		
		$KeyData = isset($Options["datakey"]) ? $Options["datakey"] : null;
		$Keep = isset($Options["keepdata"]) ? (boolean) $Options["keepdata"] : false;
		
		if(isset($Options["addcollection"]))
		{
			$AddCollection = true;
			$CollectionName = $Options["addcollection"];
			$CollectionKey = $Options["collectionkey"];
			$Group = $Options["group"];
			$this->Collections[$CollectionName] = array();
			$Collection = array();
			if(isset($Options["where"]))
			{
				$Where = explode("-", $Options["where"]);
				if(sizeof($Where) == 2)
				{
					$WhereKey = $Where[0];
					$WhereValue = trim($Where[1]);
					$Where = true;
				}
				else $Where = false;
			}
			else $Where = false;
		}
		else
		{
			$AddCollection = false;
			$CollectionName = null;
			$CollectionKey = null;
		}
		
		$R = array();
		if($KeyData && $this->Data)
		{
			$KeyData = explode(".", $KeyData);
			$Data = $this->Data;
//$this->Dump(__METHOD__);
			$TData = true;
//$StartPrepareData = microtime(true);
			foreach ($KeyData as $v)
			{
				if(array_key_exists($v, $Data))
				{
					$Data = &$Data[$v];
				}
				else
				{
					$TData = false;
					break;
				}
			}
//$this->Debug("Templater ITEM :: FIND DATA = ".(microtime(true) - $StartPrepareData));
			if($TData && $Data)
			{
				if(!is_array($Data))
					$Data = array($Data);
//$StartPrepareData = microtime(true);
//$Counter = 0;

				$PreparedTemplate = addcslashes($Template, '"');
				$PreparedTemplate = "\$z=\"".preg_replace('/(?:%\{%){1}([^(%\}%)]+)(?:%\}%){1}/sui', "\".(isset(\$v[\"$1\"]) ? \$v[\"$1\"] : \"\").\"", $PreparedTemplate)."\";";
//$this->Dump(__METHOD__, "+++++++++++++++++++++++++++++++++++++++++++");
				if($AddCollection) $URecNumber = array();
				else $URecNumber = 0;
				foreach ($Data as $k => $v)
				{
//$Counter ++;
//$StartIteration = microtime(true);
					$v["recordNumber"] = $k;
					if($AddCollection)
					{
						if($Where)
						{
							if(!(array_key_exists($WhereKey, $v) && $v[$WhereKey] == $WhereValue))
								continue;
						}
						$key = $v[$Group];
						if(!array_key_exists($key, $Collection))
								$Collection[$key] = "";
						if(!isset($URecNumber[$CollectionKey]))
							$URecNumber[$CollectionKey] = 0;
						$v["recordIndex"] = $URecNumber[$CollectionKey];
						$URecNumber[$CollectionKey] ++;
//$StartArray = microtime(true);
						//$p = self::ArrayToPath($v, "%{%", "%}%");
//$this->Debug("Templater ITEM :: ITERATION FOR COLLECTION :: ARRAY_TO_KEY = ".(microtime(true) - $StartArray));
						//$Collection[$key] .= preg_replace('/(?:%\{%){1}[^(%\}%)]+(?:%\}%){1}/sui', "", mb_str_replace($p["keys"], $p["values"], $Template));
//$this->Debug("Templater ITEM :: ITERATION FOR COLLECTION = ".(microtime(true) - $StartIteration));

						$z= "";
						eval($PreparedTemplate);
//$this->Dump("***************************", $PreparedTemplate, $v, eval($PreparedTemplate), $z);


						$Collection[$key] .= $z;
					}
					else
					{
						//$p = self::ArrayToPath($v, "%{%", "%}%");
						//$R[] = preg_replace('/(?:%\{%){1}[^(%\}%)]+(?:%\}%){1}/sui', "", mb_str_replace($p["keys"], $p["values"], $Template));
						$z = "";
//$this->Dump("---------------------------------------", $PreparedTemplate, $v, eval($PreparedTemplate), $z);
						$v["recordIndex"] = $URecNumber;
						//$URecNumber ++;
						eval($PreparedTemplate);
						$R[] = $z;
						
//$this->Debug("Templater ITEM :: ITERATION = ".(microtime(true) - $StartIteration));
					}
				}
//$this->Debug("Templater ITEM :: PREPARE COLLECTION = ".(microtime(true) - $StartPrepareData)."; ITERATION COUNT = ".$Counter);
				if($AddCollection)
				{
//$StartIteration = microtime(true);
					$AData = &$this->GetSubtreeData($CollectionName);
//$this->Debug("Templater ITEM :: GET SUBTREE DATA FOR COLLECTION = ".(microtime(true) - $StartIteration));
//$StartIteration = microtime(true);
					foreach ($AData as $k => $v)
					{
						if(isset($Collection[$v[$Group]]))
						{
							$AData[$k][$CollectionKey] = $Collection[$v[$Group]];
						}
					}
				}
//$this->Debug("Templater ITEM :: ADD DATA FOR COLLECTION = ".(microtime(true) - $StartIteration));
				if(!$Keep) unset($Data);
			}
		}
		if(!$AddCollection) $R = implode("", $R);
		else $R = " ";
//$this->Debug("Templater ITEM ALL = ".(microtime(true) - $Start));
		return $R;
	}
	
	
	protected function SelectOption($Template, $Options = null)
	{
		if(!$Options)
			throw new dmtException("Template error");
		
		$KeyData = isset($Options["datakey"]) ? $Options["datakey"] : null;
		$Selected = isset($Options["selecteddatakey"]) ? $Options["selecteddatakey"] : null;
		$Keep = isset($Options["keepdata"]) ? (boolean) $Options["keepdata"] : false;
		
		if(preg_match('~%\{%isSelected(?:[:#](.*?))?%\}%~', $Template, $SelectedTemplate))
		{
			$Substitute = (isset($SelectedTemplate[1]) && $SelectedTemplate[1]) ? $SelectedTemplate[1] : "selected=\"selected\"";
			$SelectedTemplate = $SelectedTemplate[0];
			$Template = preg_replace('~%\{%isSelected(?:[:#](.*?))?%\}%~', '%{%isSelected%}%', $Template);
		}
		elseif($Selected)
		{
			throw new dmtException("Not found key 'isSelected' in template. ".__METHOD__);
		}
		else
		{
			$SelectedTemplate = '%{%isSelected%}%';
			$Substitute = "selected=\"selected\"";
		}
		
		if($Selected)
		{
			$Selected = explode(".*.", $Selected);
			$SelectedSubKey = $Selected[0];
			if(sizeof($Selected) == 2)
			{
				$SelectedKey = $Selected[1];
				$SelectedAsArray = true;
			}
			else
			{
				$SelectedKey = $Selected[0];
				$SelectedAsArray = false;
			}
			
			//Получаем выделенный элемент
			$SelectedData = $this->GetSubtreeData($SelectedSubKey, !$SelectedAsArray);
			$SelectedKey = !$SelectedAsArray && isset($SelectedData[1]) ? $SelectedData[1] : $SelectedKey;
			$SelectedData = !$SelectedAsArray && isset($SelectedData[0]) ? $SelectedData[0] : $SelectedData;
		}
		else
		{
			$SelectedKey = null;
			$SelectedData = null;
			$SelectedAsArray = false;
		}
		
		$R = array();
		if($KeyData && $this->Data)
		{
			$Data = $this->GetSubtreeData($KeyData);
			if($Data)
			{
				if(!is_array($Data))
					$Data = array($Data);
				$SelectedData = $SelectedAsArray ? $this->GetKeysFromData($SelectedData, $SelectedKey) : array($SelectedData);
				foreach ($Data as $k => $v)
				{
					if($SelectedKey && isset($v[$SelectedKey]) && in_array($v[$SelectedKey], $SelectedData))
					{
						$v["isSelected"] = $Substitute;
					}
					$v = self::ArrayToPath($v, "%{%", "%}%");
					$R[] = preg_replace('/(?:%\{%){1}[^(%\{%)]+(?:%\}%){1}/sui', "", mb_str_replace($v["keys"], $v["values"], $Template));
				}
				if(!$Keep) unset($Data);
			}
		}
		return implode("", $R);
	}
	
	
	protected function GetKeysFromData($Data, $Key)
	{
		$R = array();
		if(is_array($Data) && !isset($Data[$Key]))
		{
			if(sizeof($Data))
			{
				foreach ($Data as $v)
				{
					$R[] = $v[$Key];
				}
			}
		}
		else
		{

		}
		return $R;
	}
	
	
	protected function ConditionView($Template, $Options = null)
	{
//$this->Dump(__METHOD__);
		if(!$Options)
			throw new dmtException(__METHOD__." Template error.");
		
		$R = "";
		if(isset($Options["datakey"]) && isset($Options["value"]))
		{
			if($this->Data)
			{
				$Data = $this->GetSubtreeData($Options["datakey"]);
				if($Data)
				{
					$Value = $this->GetValues($Options["value"]);
					if(is_array($Value) && in_array($Data, $Value) || !is_array($Value) && $Data == $Value)
					{
						//$R = $Template;
						$R = $this->PrepareTemplate($Template);
					}
				}
			}
		}
		else throw new dmtException("Error Value or DataKey option in ".__METHOD__);
		return $R;
	}
	
	
	
	/**
	 * 
	 * Возвращает поддерево данных из массива данных по заданному пути
	 * @param string $KeyData - Путь
	 * @param boolean $GetLastKey - При значении аргумента, эквивалентном true, вернется массив, первый элемент, которого - выбранные данные, второй - последный ключ
	 * @param array $Data - Необязательный. Массив данных.
	 * Если аргумент не задан, будет использоваться массив данных $this->Data. Запланировано. Не реализовано в текущей версии.
	 */
	protected function &GetSubtreeData($KeyData, $GetLastKey = null, $Data = null)
	{
		if(!$KeyData)
			throw new dmtException("Key data error");
		$TData = true;
//$this->Dump(__METHOD__, $KeyData, $this->Data);
		$KeyData = explode(".", $KeyData);
		$Data = &$this->Data;
		if(is_array($Data))
		{
			foreach ($KeyData as $v)
			{
				if(!$Data || !array_key_exists($v, $Data))
				{
					$TData = false;
					break;
				}
				$Data = &$Data[$v];
			}
		}
		else $TData = false;
		if($TData)
		{
			if($GetLastKey)
			{
				$R = array(0 => &$Data, 1 => $v);
			}
			else $R = &$Data;
		}
		else $R = array();
		$R = &$R;
//$this->Dump(__METHOD__, $R);
		return $R;
	}
	
	protected function GetValues($Value)
	{
		if(mb_substr($Value, 0, 1) == "[")
		{
			$Value = preg_split('/\s*,\s*/', mb_substr($Value, 1, -1));
			explode(",", mb_substr($Value, 1, -1));
		}
		return $Value;
	}
}