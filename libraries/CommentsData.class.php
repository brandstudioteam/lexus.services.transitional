<?php
class CommentsData extends Data
{
    public function CreateComment($Ess, $Object, $Brand, $IP, $Text)
    {
        $this->Begin();
        try
        {
            $this->Exec("INSERT INTO `references`.`comments`
    (`ess_id`,
    `object_id`,
    `user_id`,
    `brand_id`,
    `ip`,
    `text`,
    `update`,
    `create`)
VALUES
    (".$Ess.",
    ".$Object.",
    ".DB_VARS_ACCOUNT_CURRENT.",
    ".$Brand.",
    INET_ATON(".$this->Esc($IP)."),
    ".$this->Esc($Text).",
    NOW(),
    NOW());");
            $Comment = $this->DB->GetLastID();

            $this->Exec("INSERT INTO `references`.`comments_objects_statistics`
	(`ess_id`,
	`object_id`,
	`count`,
	`last_update`)
VALUES
	(".$Ess.",
    ".$Object.",
    1,
    NOW())
ON DUPLICATE KEY UPDATE
`count` = `count` + 1,
`last_update` = VALUES(`last_update`);");
            $this->Commit();
        }
        catch(dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
        $R = $this->GetCommentsWUsers(null, null, null, null, null, null, null, $Comment);
        return $R[0];
    }

    public function UpdateComment($Comments, $Brand, $Text)
    {
        if(!$this->Count("SELECT COUNT(*)
FROM `references`.`comments`
WHERE `comment_id`=".$Comments."
    AND `brand_id`=".$Brand."
    AND `user_id`=".DB_VARS_ACCOUNT_CURRENT.";"))
            $this->Forbidden();

        return $this->Exec("UPDATE `references`.`comments`
SET `text`=".$this->Exec($Text).",
    `update`= NOW()
WHERE `comment_id`=".$Comments.";");
    }

    public function GetCommentsWUsers($Ess, $Object, $Brand, $User = null, $DateStart = null, $DateEnd = null, $IP = null, $Comments = null)
    {
        $Where = array();
        if($Comments)
        {
            $Where[] = "a.`comment_id`".$this->PrepareValue($Comments);
        }
        else
        {
            $Where[] = "a.`ess_id`=".$Ess;
            $Where[] = "a.`object_id`=".$Object;
            $Where[] = "a.`brand_id`=".$Brand;
            if($User)
                $Where[] = "a.`user_id`=".$User;
            elseif($IP)
                $Where[] = "a.`ip`=".$IP;
            if($DateStart)
                $Where[] = "a.`update`>=".$DateStart;
            if($DateEnd)
                $Where[] = "a.`update`<=".$DateEnd;
        }
        return $this->Get("SELECT
	a.`comment_id` AS commentId,
	a.`ess_id` AS essId,
	a.`object_id` AS objectId,
	a.`user_id` AS userId,
	a.`brand_id` AS brandId,
	INET_NTOA(a.`ip`) AS IP,
	a.`text` AS commentText,
	DATE_FORMAT(a.`update`, '%d.%m.%Y %H:%i:%s') AS commentUpdateU,
    DATE_FORMAT(a.`update`, '%Y-%m-%d %H:%i:%s') AS commentUpdate,
	DATE_FORMAT(a.`create`, '%d.%m.%Y %H:%i:%s') AS commentCreate,
	b.`partners_division_id` AS partnersDivisionId,
	b.`first_name` AS userFirstName,
	b.`middle_name` AS userMiddleName,
	b.`last_name` AS userLastName
FROM `references`.`comments` AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users` AS b ON b.`user_id`=a.`user_id`".$this->PrepareWhere($Where));
    }

    public function GetCommentsByIdWUsers($Comments)
    {
        return $this->GetCommentsWUsers(null, null, null, null, null, null, null, $Comments);
    }

    public function GetComments($Ess, $Object, $Brand, $User = null, $DateStart = null, $DateEnd = null, $IP = null, $Comments = null)
    {
        $Where = array();
        $Where[] = "`ess_id`=".$Ess;
        $Where[] = "`object_id`".$this->PrepareValue($Object);
        $Where[] = "`brand_id`=".$Brand;
        if($User)
            $Where[] = "`user_id`=".$User;
        elseif($IP)
            $Where[] = "`ip`=".$IP;
        if($DateStart)
            $Where[] = "`update`>=".$DateStart;
        if($DateEnd)
            $Where[] = "`update`<=".$DateEnd;

        return $this->Get("SELECT
	`comment_id` AS commentId,
	`ess_id` AS essId,
	`object_id` AS objectId,
	`user_id` AS userId,
	`brand_id` AS brandId,
	`ip` AS IP,
	`text` AS commentText,
	`update` AS commentUpdate,
	`create` AS commentCreate
FROM `references`.`comments`".$this->PrepareWhere($Where));
    }

    public function GetCommentsById($Comments)
    {
        return $this->GetCommentsWUsers(null, null, null, null, null, null, null, $Comments);
    }

    public function GetComment($Comment)
    {
        return $this->Get("SELECT
	`comment_id` AS commentId,
	`ess_id` AS essId,
	`object_id` AS objectId,
	`user_id` AS userId,
	`brand_id` AS brandId,
	`ip` AS IP,
	`text` AS commentText,
	`update` AS commentUpdate,
	`create` AS commentCreate
FROM `references`.`comments`
WHERE `comment_id`=".$Comment, true);
    }

	public function DeleteComment($Ess, $Object, $Comment)
	{
		$Where = array();
		if($Comment)
			$Where[] = "a.`comment_id`".$this->PrepareValue ($Comment);
		else
		{
			$Where[] = "a.`ess_id`=".$Ess;
			$Where[] = "a.`object_id`".$this->PrepareValue($Object);
		}
		return $this->Exec("DELETE b, a FROM `references`.`comments` AS a
LEFT JOIN `references`.`comments_objects_statistics` AS b ON b.`ess_id`=a.`ess_id` AND b.`object_id`=a.`object_id`".$this->PrepareWhere($Where));
	}
}