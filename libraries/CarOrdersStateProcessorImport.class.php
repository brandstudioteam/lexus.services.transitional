<?php
define("CAR_ORDERS_STATE_FILE_LOAD_PATH",           "");
define("CAR_ORDERS_STATE_FILE_ARCHIVE_PATH",        "");

class CarOrdersStateProcessorImport extends Processor
{
	/**
	 *
	 * @var CarOrdersStateProcessorImport
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var CarOrdersStateDataImport
	 */
	protected $CData;

    protected $IsError = 0;
    protected $Count = 0;
    protected $LogFile;
    protected $CurrFile;

    /**
	 *
	 * Инициализирует класс
	 *
	 * @return CarOrdersStateProcessorImport
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new CarOrdersStateDataImport();
	}

    public function Import()
    {
        ignore_user_abort(true);
		set_time_limit(0); //10800
		ini_set('memory_limit', '2048M');

        foreach(glob("ContractStatus_*") as $this->CurrFile)
        {
            $this->InitLog();
            $this->PrepareData();
            $this->CloseLog();
        }
    }

    public function PrepareData()
    {
        foreach(file($this->CurrFile) as $Data)
        {
            $Data = trim($Data);
        }
        $Data = iconv("cp1251", CHARSET.'//IGNORE', $Data);
        $Dealer = $this->CData->GetDealer($Data[3]);
        $Model = $this->CData->GetModel($Data[6]);
        if(!$Dealer)
        {
            $this->Log("Не найден дилер для кода ".$Data[3].". Номер заказа ".$Data[0]);
            $this->IsError ++;
            return;
        }
        if(!$Model)
        {
            $this->Log("Не найдена модель для кода ".$Data[6].". Номер заказа ".$Data[0]);
            $this->IsError ++;
            return;
        }
        try
        {
            $this->CData->SaveState($Data[0], md5(sha1($Data[1])), $Data[2], $Dealer, $Data[4], $Data[5], $Model);
            $this->Count ++;
        }
        catch(dmtException $e)
        {
            $this->IsError ++;
            $this->Log("При сохранении данных произошла ошика. Код: ".$e->getCode().". Сообщение: ".$e->getMessage().". Номер заказа ".$Data[0]);
        }
    }


    protected function Log($Message)
    {
        error_log($Message."\r\n", 3, $this->LogFile);
    }

    protected function InitLog()
    {
        $this->LogFile = CAR_ORDERS_STATE_FILE_ARCHIVE_PATH."_".$this->CurrFile."_".time().".log";
        $this->Log("Импорт данных файла: ".$this->CurrFile.". ".date("d.m.Y H:i:s"));
    }

    protected function CloseLog()
    {
        $this->Log("Завершен импорт данных. ".date("d.m.Y H:i:s"));
        $this->Log("Импортировано ".$this->Count." записей.");
        if($this->IsError)
            $this->Log("Не записано в результате возникщих ошибок ".$this->IsError." записей.");
        $this->SendReport();
    }

    public function SendReport()
    {
//$this->Dump('ОТПРАВКА: НАЧАЛО');
        $M = new MailDriver(false, true);
        $M->Subject = "Состояние заказанных автомобилей";

        $M->AddBCC('ml@bstd.ru');
        $M->AddBCC('ib@bstd.ru');

        $M->MsgHTML('<h2>Загрузка файла "'.$this->CurrFile.'" завершена</h2><br />
            <div>Обработано <b>'.($this->IsError + $this->Count).'</b> записей</div><br />
            <div>Добавлено или обновлено <b>'.$this->Count.'</b> записей</div><br />
            <div>Возникло <b>'.$this->IsError.'</b> ошибок</div><br />');

        $M->AddAttachment($this->LogFile);
        $M->Send();
//$this->Dump('ОТПРАВКА: КОНЕЦ');
    }
}