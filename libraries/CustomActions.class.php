<?php
class CustomActions extends Controller
{
	/**
	 *
	 * @var CustomActions
	 */
	private static $Inst = false;

	protected function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Инициализирует класс
	 * @return CustomActions
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function Sets()
	{
		$this->Tpls = array(
			"TplVars"	=> array(
				"partnerId"					=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"modelId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
                "submodelId"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"captcha"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"verifier"	=> array("Captcha", "CheckCode")
				),
				"subscribe"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
				),
				"isOwner"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
				),
				"email"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"age"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"phone"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_PHONE_ONE),
					"notempty"	=> true
				),
				"gender"			=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array(0, 1)),
					"notempty"	=> true
				),
				"lastName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME),
					"notempty"	=> true
				),
				"firstName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME),
					"notempty"	=> true
				),
				"lastNameWEn"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_LASTNAME_WEN),
					"notempty"	=> true
				),
				"firstNameWEn"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_NAME_WEN),
					"notempty"	=> true
				),

				"partnerUserDataType"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"partnerName"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerOfficialName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerAddress"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"partnerEmail"				=> array(
					"filter"	=> array(FILTER_TYPE_REGEXP, FILTER_EMAIL),
				),
				"pertnerTypeName"			=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"pertnerTypeDescription"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"emails"	=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_EMAIL_ONE),
				),
				"testDriveCarImage"	=> array(
					"isfile"	=> array(),
					"required"	=> true
				),
				"cityId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
                "actionId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
					"notempty"	=> true
				),
				"modelSpec"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
					"notempty"	=> true
				),
				"modelColor"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
					"notempty"	=> true
				),
				"contactMethod"				=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
                "type"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "fds"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
					"default"	=> 0
				),
                "im"				=> array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("0", "1")),
					"default"	=> 0
				),
                "currentModel"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "formType"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "registerStart"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
                "registerEnd"    => array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATE),
				),
				"code"    => array(
					"filter"	=> array(FILTER_TYPE_VALUES, array("_asd-AS-adasdwer-r-rrt5436--df-srf3-54345")),
				),
			)
		);
		$this->Modes = array(
			//Возвращает список городов, в которых есть дилеры, участвующие в заказе
			"a"		=> array(
				"exec"		=> array("CustomActionsProcessor", "GetCitiesForDealersTD"),
				"TplVars"		=> array("modelId" => 1)
			),
			//Возвращает список городов, в которых есть дилеры, участвующие в тест-драйве
			"b"		=> array(
				"exec"		=> array("CustomActionsProcessor", "GetCitiesForDealersOM"),
				"TplVars"		=> array("modelId" => 1)
			),
			//Возвращает список городов, в которых есть дилеры, участвующие в бронировании
			"c"		=> array(
				"exec"		=> array("CustomActionsProcessor", "GetCitiesForDealersBM"),
				"TplVars"		=> array("modelId" => 1)
			),

			//Возвращает список дирлеров, участвующие в заказе для заданного города
			"d"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetDealersForCityTD"),
				"TplVars"		=> array("modelId" => 1, "cityId" => 1)
			),
			//Возвращает список дирлеров, участвующие в заказе для заданного города
			"e"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetDealersForCityOM"),
				"TplVars"		=> array("modelId" => 1, "cityId" => 1)
			),
			//Возвращает список дирлеров, участвующие в заказе для заданного города
			"f"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetDealersForCityBM"),
				"TplVars"		=> array("modelId" => 1, "cityId" => 1)
			),
            "a0"		=> array(
				"exec"		=> array("CustomActionsProcessor", "GetActionFullInfo"),
				"TplVars"		=> array("actionId" => 1, "partnerId" => 0, "modelId" => 0)
			),

			/*
			//Возвращает список моделей, участвующих в заказах
			"c"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetModelsInOrders"),
				"TplVars"		=> array()
			),
			//Возвращает список дилеров, участвующих в заказе модели
			"d"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetDealersForOrder"),
				"TplVars"		=> array("modelId" => 1)
			),
			//Возвращает список дилеров, не участвующих в заказе модели или всех дилеров, которые могут участвовать в заказе моделей
			"e"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetDealersNotInOrder"),
				"TplVars"		=> array("modelId" => 1)
			),
			//Удаляет модель из списка заказов
			"f"	=> array(
				"exec"		=> array("CustomActionsProcessor", "DeleteModel"),
				"TplVars"		=> array("modelId" => 1)
			),
			//Возвращает список моделей, не участвующих в заказах
			"g"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetModelNotInOrders"),
				"TplVars"		=> array()
			),
			//Удаляет дилера из заказа модели
			"h"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetPartner"),
				"TplVars"		=> array("partnerId" => 1)
			),

			//Создает заказ
			"i"	=> array(
				"exec"		=> array("CustomActionsProcessor", "CreateOrder"),
				"TplVars"		=> array("modelId" => 1, "partnerId" => 1)
			),
			//Сохраняет изменения в заказе
			"j"	=> array(
				"exec"		=> array("CustomActionsProcessor", "SaveOrder"),
				"TplVars"		=> array("modelId" => 1, "partnerId" => 1)
			),
			//Сохраняет изменения дилеров (электронные адреса) для (всех) заказов
			"k"	=> array(
				"exec"		=> array("CustomActionsProcessor", "SaveDealer"),
				"TplVars"		=> array("partnerId" => 1, "emails")
			),
			*/
			//Сохраняет заявку на тест-драйв
			"l"	=> array(
				"exec"		=> array("CustomActionsProcessor", "AddMembersTD"),
                                        //$Model,       $Dealer,            $LastName,      $FirstName,         $Email,     $Phone,         $Gender,    $Age,       $Subscribe = null, $IsOwner = null, $Type = null
				"TplVars"		=> array("modelId" => 1, "partnerId" => 1, "lastName" => 1, "firstName" => 1, "email" => 1, "phone" => 1, "gender" => 1, "age" => 1, "subscribe" => 0, "isOwner" => 0, "type" => 0, "fds" => 0, "im" => 0, "formType" => 0),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							80002 => "AlreadyExists"
						)
					)
				),
			),
			"m"	=> array(
				"exec"		=> array("CustomActionsProcessor", "AddMembersOM"),
                                        //$Model, $Dealer, $LastName, $FirstName, $Email, $Phone, $Gender, $Age, $Subscribe = null, $IsOwner = null, $CurrentModel = null, $FromDealerSite = null, $FromMobile = null, $Type = null
				"TplVars"		=> array("modelId" => 1, "partnerId" => 1, "lastName" => 1, "firstName" => 1, "email" => 0, "phone" => 1, "gender" => 0, "age" => 0, "subscribe" => 0, "isOwner" => 0, "currentModel" => 0, "fds" => 0, "im" => 0, "type" => 0, "formType" => 0)
			),
			"n"	=> array(
				"exec"		=> array("CustomActionsProcessor", "AddMembersBM"),
				"TplVars"		=> array("modelId" => 1, "partnerId" => 1, "lastNameWEn" => 1, "firstNameWEn" => 1, "email" => 0, "phone" => 0, "gender" => 1, "age" => 0, "subscribe" => 0, "isOwner" => 0, "modelSpec"	=> 1, "modelColor" => 1, "contactMethod" => 0, "formType" => 0)
			),

			//Возвращает форму заказа
			"o"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetActionInfoTD"),
				"TplVars"		=> array("modelId" => 0, "partnerId" => 0)
			),
			//Возвращает форму заказа
			"p"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetActionInfoOM"),
				"TplVars"		=> array("modelId" => 0, "partnerId" => 0)
			),
			//Возвращает форму заказа
			"q"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetActionInfoBM"),
				"TplVars"		=> array("modelId" => 0, "partnerId" => 0)
			),
			/*
			"n"	=> array(
				"exec"		=> array("CustomActionsProcessor", "OrderInfo"),
				"TplVars"		=> array("modelId" => 1, "testDriveCarImage" => 1)
			)
			*/
                        //Возвращает форму заказа
			"z"	=> array(
				"exec"		=> array("CustomActionsProcessor", "SendForAllDealers"),
				"TplVars"		=> array("actionId" => 1)
			),
            "x"	=> array(
				"exec"		=> array("CustomActionsProcessor", "GetMembersCSV"),
                                        //$Action, $FormType, $Dealer, $Model = null, $RegisterStart = null, $RegisterEnd
				"TplVars"		=> array("actionId" => 2, "formType" => 2, "partnerId" => 0, "modelId" => 0, "registerStart" => 0, "registerEnd" => 0)
			),
            "m5"	=> array(
				"exec"		=> array("CustomActionsProcessor", "SendLink"),
				"TplVars"		=> array("email" => 1, "firstName" => 1, "pdf" => 1)
			),
			"z100"	=> array(
				"exec"		=> array("CustomActionsProcessor", "ReSendTestDrives"),
				"TplVars"		=> array("code" => 2)
			),
		);
	}
}