<?php
class ContestProcessor extends Processor
{
	/**
	 *
	 * @var ContestProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var ContestData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return ContestProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new ContestData();
	}


    public function AddMember($LastName, $FirstName, $City, $Car, $Email, $Gender, $Age, $Comment)
    {
        $this->CData->AddMember($LastName, $FirstName, $City, $Car, $Email, $Gender, $Age, $Comment);

        $Mailer = new MailDriver(false, true);
		$Mailer->From = "mailer@lexus.ru";
		$Mailer->FromName = "LEXUS";
		$Mailer->Subject = "Конкурс NX";
        $Mailer->Body = "Имя: ".$FirstName."
Фамилия: ".$LastName."
Пол: ".($Gender ? "Женский" : "Мужской")."
Возраст: ".$Age."
Город: ".GeoProcessor::Init()->GetCityName($City)."
E-Mail: ".$Email."
Модель: ".CarsProcessor::Init()->GetCarFullName($Car)."
Комментарий: ".$Comment;

        if(WS::Init()->IsTest())
        {
            $Mailer->AddAddress("ka@bstd.ru");
            $Mailer->AddAddress("ib@bstd.ru");
        }
        else {
            $Mailer->AddAddress("contest@smartnewsolutions.ru");
            $Mailer->AddAddress("EKuplinova@lexus.ru");
            $Mailer->AddBCC("formresult@bstd.ru");
            $Mailer->AddBCC("ka@bstd.ru");
        }
        try
        {
            $Mailer->Send();
        }
        catch(Exception $e)
        {
            throw new dmtException("Send e-mail to dealers error", 2);
        }
        
        
        
        $Mailer = new MailDriver(false, true);
		$Mailer->From = "mailer@lexus.ru";
		$Mailer->FromName = "LEXUS";
		$Mailer->Subject = "Конкурс LEXUS «ВАШ БИЛЕТ НА ПРЕМЬЕРУ LEXUS NX»";
        $Mailer->MsgHTML("<p>Поздравляем Вас с успешной регистрацией в конкурсе LEXUS «ВАШ БИЛЕТ НА ПРЕМЬЕРУ LEXUS NX»!</p>
<p>Ваш отзыв принят.</p>
<p>Не забудьте поделиться условиями конкурса на своих страницах в социальных сетях, обязательно добавив хэштег #LexusNX</p>
<p>Сроки проведения конкурса: 12 – 24 августа<br />
Подробнее с правилами проведения конкурса можно ознакомиться по ссылке: <a href=\"http://www.lexus.ru/car-models/nx/nx-house/win-with-nx.tmex\" terget=\"_blank\">http://www.lexus.ru/car-models/nx/nx-house/win-with-nx.tmex</a></p>
<p>Желаем удачи,<br />
команда Lexus</p>");

            $Mailer->AddAddress($Email);
        try
        {
            $Mailer->Send();
        }
        catch(Exception $e)
        {
            throw new dmtException("Send e-mail to member error", 2);
        }
    }
}