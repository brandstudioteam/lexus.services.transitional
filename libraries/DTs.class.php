<?php
define("FORMAT_DATETIME_DEFAULT",			"Y-m-d H:i:s");
class DTs extends BaseStatic
{
	static protected $DT;
	static protected $Timestamp;
	
	static public function GetCurrent()
	{
		if(!self::$DT)
			self::$DT = self::Format(self::GetCTimestamp());
		return self::$DT;
	}
	
	static public function GetCTimestamp()
	{
		if(!self::$Timestamp) self::$Timestamp = time();
		return self::$Timestamp;
	}
	
	static public function Format($Timestamp, $Format = null)
	{
		return date($Format ? $Format : FORMAT_DATETIME_DEFAULT, $Timestamp);
	}
	
	static public function DateToTimestamp($Datetime)
	{
		$R = "";
		if(preg_match('~^\s*(\d{4})[\s,/\\.\-]([10]?\d)[\s,/\\.\-](\d|3[01]|[120]\d)[\s,/\\.\-]([012]?\d)[\s,/\\.\-:]([0-5]?\d)[\s,/\\.\-:]([0-5]?\d)\s*$~', $Datetime, $Z))
		{
			$R = mktime(($Z[4] < 10 ? "0".intval($Z[4]): $Z[4]), ($Z[5] < 10 ? "0".intval($Z[5]): $Z[5]), ($Z[6] < 10 ? "0".intval($Z[6]) : $Z[6]), ($Z[2] < 10 ? "0".intval($Z[2]) : $Z[2]), ($Z[3] < 10 ? "0".intval($Z[3]) : $Z[3]), $Z[1] );
		}
		return $R;
	}
	
	static public function PrepareDate($Date)
	{
		if(preg_match('~^(?:\s*([0-2]?\d|3[01])(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)(\d{4})|(\d{4})(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)([0-2]?\d|3[01])\s*)$~u', $Date, $Z))
		{
			$Z[3] = isset($Z[4]) && $Z[4] ? $Z[4] : $Z[3];
			$Z[2] = isset($Z[5]) && $Z[5] ? $Z[5] : $Z[2];
			$Z[1] = isset($Z[6]) && $Z[6] ? $Z[6] : $Z[1];
			$Date = $Z[3]."-".($Z[2] < 10 ? "0".intval($Z[2]) : $Z[2])."-".($Z[1] < 10 ? "0".intval($Z[1]) : $Z[1]);
		}
		else $Date = false;
		return $Date;
	}
	
	static public function PrepareTime($Time)
	{
		if(preg_match('/^\s*([01]?\d|2[0-3])(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d)(?:(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d))?\s*$/u', $Time, $Z))
			$Time = $Z[1].":".($Z[2] < 10 ? "0".intval($Z[2]): $Z[2]).":".(isset($Z[3]) ? ($Z[3] < 10 ? "0".intval($Z[3]) : $Z[3]) : "00");
		else $Time = false;
		return $Time;
	}
	
	static public function PrepareDatetime($Datetime)
	{
		if(preg_match('~^(?:\s*([0-2]?\d|3[01])(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)(\d{4})|(\d{4})(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)([0-2]?\d|3[01])\s*)\s*([01]?\d|2[0-3])(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d)(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d)$~u', $Datetime, $Z))
		{
			$Z[3] = isset($Z[4]) && $Z[4] ? $Z[4] : $Z[3];
			$Z[2] = isset($Z[5]) && $Z[5] ? $Z[5] : $Z[2];
			$Z[1] = isset($Z[6]) && $Z[6] ? $Z[6] : $Z[1];
			$Datetime = $Z[3]."-".($Z[2] < 10 ? "0".intval($Z[2]) : $Z[2])."-".($Z[1] < 10 ? "0".intval($Z[1]) : $Z[1])." ".
						($Z[7] < 10 ? "0".intval($Z[7]): $Z[7]).":".($Z[8] < 10 ? "0".intval($Z[8]): $Z[8]).":".(isset($Z[9]) ? ($Z[9] < 10 ? "0".intval($Z[9]) : $Z[9]) : "00");
		}
		else $Datetime = false;
		return $Datetime;
	}
	
	static public function PrepareUnixDatetime($Datetime)
	{
self::Dump(__METHOD__."   LINE = ".__LINE__, $Datetime, preg_match('~^\s*(\d{4})[\s,/\\.\-]([10]?\d)[\s,/\\.\-](\d|3[01]|[120]\d)[\s,/\\.\-]([012]?\d)[\s,/\\.\-:]([0-5]?\d)[\s,/\\.\-:]([0-5]?\d)\s*$~', $Datetime, $Z), $Z);
		if(preg_match('~^\s*(\d{4})[\s,/\\.\-]([10]?\d)[\s,/\\.\-](\d|3[01]|[120]\d)[\s,/\\.\-]([012]?\d)[\s,/\\.\-:]([0-5]?\d)[\s,/\\.\-:]([0-5]?\d)\s*$~', $Datetime, $Z))
		{
			$Datetime = $Z[1]."-".($Z[2] < 10 ? "0".intval($Z[2]) : $Z[2])."-".($Z[3] < 10 ? "0".intval($Z[3]) : $Z[3])." ".
				($Z[4] < 10 ? "0".intval($Z[4]): $Z[4]).":".($Z[5] < 10 ? "0".intval($Z[5]): $Z[5]).":".($Z[6] < 10 ? "0".intval($Z[6]) : $Z[6]);
		}
		else $Datetime = false;
		return $Datetime;
	}
	
	
	
	static public function GetYear($Date = null)
	{
		if(!$Date) $Date = time();
		return date("Y", $Date);
	}
	
	static public function ValidateDatetime($Datetime)
	{
		return preg_match('~^\s*((([0-2]?\d|3[01])([ ]+|\s*[-,.\/\\])\s*(0?\d|1[0-2])([ ]+|\s*[-,.\/\\])\s*(\d{4}))|((\d{4})([ ]+|\s*[-,.\/\\])\s*(0?\d|1[0-2])([ ]+|\s*[-,.\/\\])\s*([0-2]?\d|3[01])))\s*(([01]?\d|2[0-3])([ ]+|\s*[-,.\/\\])\s*(0?\d|[0-5]\d)([ ]+|\s*[-,.\/\\])\s*(0?\d|[0-5]\d)?)?\s*$~', $Datetime);
	}
	
	static public function ValidateDate($Date)
	{
		return (boolean) preg_match('~^(?:\s*([0-2]?\d|3[01])(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)(\d{4})|(\d{4})(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)([0-2]?\d|3[01])\s*)$~u', $Date);
	}
	
	static public function ValidateTime($Time)
	{
		return (boolean) preg_match('/\s*([01]?\d|2[0-3])(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d)(?: |\s*[-,:.\/]\s*)(0?\d|[0-5]\d)*/u', $Time);
	}
	
	static public function GetStringMonth($Month, $Case = null)
	{
		$M = array(
			1 => array(
				1 => "январь",
				2 => "февраль",
				3 => "март",
				4 => "апрель",
				5 => "май",
				6 => "июнь",
				7 => "июль",
				8 => "август",
				9 => "сентябрь",
				10 => "октябрь",
				11 => "ноябрь",
				12 => "декабрь"
			),
			2 => array(
				1 => "января",
				2 => "февраля",
				3 => "марта",
				4 => "апреля",
				5 => "мая",
				6 => "июня",
				7 => "июля",
				8 => "августа",
				9 => "сентября",
				10 => "октября",
				11 => "ноября",
				12 => "декабря"
			)
		);
		if(!$Case) $Case = 1;
		return $M[$Case][$Month];
	}
	
	static public function GetDateWithStringMonth($Date, $Case = null)
	{
		if(preg_match("~^\\s*((([0-2]?\\d{1}|3[01]{1})([ ]+|\\s*[-,.\\/])\\s*(0?\\d{1}|1[0-2]{1})([ ]+|\\s*[-,.\\/])\\s*(\\d{4}))|((\\d{4})([ ]+|\\s*[-,.\\/])\\s*(0?\\d{1}|1[0-2]{1})([ ]+|\\s*[-,.\\/])\\s*([0-2]?\\d{1}|3[01]{1})))\\s*$~ui", $Date, $Z))
		{
			$Z[7] = intval($Z[7] ? $Z[7] : $Z[9]);
			$Z[5] = intval($Z[5] ? $Z[5] : $Z[11]);
			$Z[3] = intval($Z[3] ? $Z[3] : $Z[13]);
			$Date = ($Z[3] < 10 ? "0".$Z[3] : $Z[3])." ".self::GetStringMonth(intval($Z[5]), $Case)." ".$Z[7];
		}
		else $Date = false;
		return $Date;
	}
	
	static public function PrepareYear($Year)
	{
		if(!preg_match('/(19[89]\d|20[01]\d)/u', $Year))
			$Year = false;
		return $Year;
	}

	static public function PrepareYearB($Year)
	{
		if(preg_match('/(19[89]\d|20[01]\d)/u', $Year))
		{
			if($Year > date("Y"))
				$Year = false;
		}
		else $Year = false;
		return $Year;
	}


    static public function ConvertDate($Date)
	{
		if(preg_match('~^(?:\s*([0-2]?\d|3[01])(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)(\d{4})|(\d{4})(?:[ ]+|\s*[-,.\/]\s*)(0?\d|1[0-2])(?:[ ]+|\s*[-,.\/]\s*)([0-2]?\d|3[01])\s*)$~u', $Date, $Z))
		{
			$Z[3] = isset($Z[4]) && $Z[4] ? $Z[4] : $Z[3];
			$Z[2] = isset($Z[5]) && $Z[5] ? $Z[5] : $Z[2];
			$Z[1] = isset($Z[6]) && $Z[6] ? $Z[6] : $Z[1];
			$Date = ($Z[1] < 10 ? "0".intval($Z[1]) : $Z[1]).".".($Z[2] < 10 ? "0".intval($Z[2]) : $Z[2]).".".$Z[3];
		}
		return $Date;
	}
	
	static public function Time2Sec($Time)
	{
		if(preg_match('/^([01]?\d|2[0-3]):(0?\d|[0-5]\d)(?::(0?\d|[0-5]\d))?$/', $Time, $Z))
			$Time = intval($Z[1]) * 3600 + intval($Z[2]) * 60 + intval($Z[2]);
		else $Time = 0;
		return $Time;
	}
	
	static public function Sec2Time($Time)
	{
		$T = floor($Time / 3600);
		$R = $T < 10 ? "0".$T : $T;
		$R .= ":";
		$Time = $Time % 3600;
		$T = floor($Time / 60);
		$R = $T < 10 ? "0".$T : $T;
		$R .= ":";
		$Time = $Time % 60;
		$R = $T < 10 ? "0".$T : $T;
		return $R;
	}
}