<?php
class Comments extends Controller
{
    /**
     *
     * @var Comments
     */
	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Comments
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				//Логин
				"commentId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "userId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "essId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "objectId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "brandId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "IP"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "commentText"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT0),
				),
                "dateStart"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATETIME),
				),
                "dateEnd"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_DATETIME),
				),
			)
		);

		$this->Modes = array(
			//Создание
			"a"	=> array(
				"exec"			=> array("CommentsProcessor", "CreateComment"),
				"TplVars"		=> array("essId" => 2, "objectId" => 2, "commentText" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "b"	=> array(
				"exec"			=> array("CommentsProcessor", "UpdateComment"),
				"TplVars"		=> array("commentId" => 2, "commentText" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "c"	=> array(
				"exec"			=> array("CommentsProcessor", "GetCommentsWUsers"),
				"TplVars"		=> array("essId" => 2, "objectId" => 2, "userId" => 0, "dateStart" => 0, "dateEnd" => 0),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "d"	=> array(
				"exec"			=> array("CommentsProcessor", "GetCommentsByIdWUsers"),
				"TplVars"		=> array("commentId" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "e"	=> array(
				"exec"			=> array("CommentsProcessor", "GetComments"),
				"TplVars"		=> array("essId" => 2, "objectId" => 2, "userId" => 0, "dateStart" => 0, "dateStart" => 0, "IP" => 0),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "f"	=> array(
				"exec"			=> array("CommentsProcessor", "GetCommentsById"),
				"TplVars"		=> array("commentId" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			),
            "g"	=> array(
                //GetComment($Comment)
				"exec"			=> array("CommentsProcessor", "GetComment"),
				"TplVars"		=> array("commentId" => 2),
				"Results"		=> array(
					"exceptions"		=>  array(1,
						1 => "EmailIsExists",
						2 => "LoginIsExists",
						3 => "NotConfirmPwsg",
						5 => "UnspecificError",
						7 => "AlreadyRegister"
					)
				)
			)
		);
	}
}