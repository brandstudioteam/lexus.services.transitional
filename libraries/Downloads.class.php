<?php
class Downloads extends Controller
{
	protected static $Inst;
 
	/**
	 * 
	 * Инициализирует класс
	 * @return Downloads
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }
	
	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
				"name"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_URL_PATH),
				),
			)
		);
		
		$this->Modes = array(
			//Выгрузка файла
			"a"	=> array(
				"exec"			=> array("DownloadsProcessor", "Download"),
				"TplVars"		=> array("name" => 1)
			)
		);
	}
}