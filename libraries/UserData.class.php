<?php
class UserData extends Data
{
	public function SetCurrentAccount($Account)
	{
		$this->Set(DB_VARS_ACCOUNT_CURRENT, $Account);
	}

	public function SetCurrentDateTime()
	{
		$this->Exec("SET ".DB_VARS_CURRENT_DATE."=NOW();");
	}

    public function SetCurrentPartner($Partner)
	{
		$this->Exec("SET ".DB_VARS_PARTNER_CURRENT."=".$Partner.";");
	}
}