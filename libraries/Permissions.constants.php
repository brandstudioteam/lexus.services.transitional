<?php
/**
 *
 * Права системы администрирования
 */

/**
 *
 * Доступ к системе администрирования
 * @var integer
 */
define("PERMISSIONS_ADMIN_ACCESS",										5);
/**
 *
 * Доступ к тестировнию
 * @var integer
 */
define("PERMISSIONS_ADMIN_ACCESS_TEST",									107);
/**
 *
 * Права промо-сайта
 */
/**
 *
 * Доступ к управлению промо-сайтом
 * @var integer
 */
define("PERMISSIONS_PROMO_ACCESS",										1);
/**
 *
 * Создание акции
 * @var integer
 */
define("PERMISSIONS_PROMO_ACTION_CREATE",								2);
/**
 *
 * Редактирование акции
 * @var integer
 */
define("PERMISSIONS_PROMO_ACTION_EDIT",									3);
/**
 *
 * Удаление акции
 * @var integer
 */
define("PERMISSIONS_PROMO_ACTION_DELETE",								4);

/**
 *
 * Права сервиса Партнеры
 */
/**
 *
 * Доступ к управлению партнерами
 * @var integer
 */
define("PERMISSIONS_PARTNERS_ADMIN_ACCESS",								9);


/**
 *
 * Права сервиса Пользователи
 */
/**
 *
 * Доступ к управлению пользователями
 * @var integer
 */
define("PERMISSIONS_ROLES_ADMIN_ACCESS",								7);

/**
 *
 * Права сервиса X-Country
 */
/**
 *
 * Доступ к управлению мероприятиями X-Country
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_ADMIN_ACCESS",								15);

/**
 *
 * Права сервиса тест-драйвов
 */
/**
 *
 * Доступ к управлению промо-сайтом
 * @var integer
 */
define("PERMISSIONS_TESTDIVESORDERS_ADMIN_ACCESS",						16);

define("PERMISSIONS_CUSTOM_ACTIONS_ADMIN_ACCESS",						16);

/**
 * Права сервиса возможностей дилеров
 */
/**
 *
 * Доступ к управлению возможностями дилеров
 * @var integer
 */
define("PERMISSIONS_FACILITIES_ADMIN_ACCESS",							37);

//define("PERMISSIONS_USERS_TYPES_ADMIN_ACCESS",                          62);




/**
 *
 * Права сервиса Пользователи
 */
/**
 *
 * Доступ к управлению типами пользователей
 * @var integer
 */
define("PERMISSIONS_USERS_TYPES_ADMIN_ACCESS",                          2);

/**
 * Создавать тип пользователей
 */
//define("PERMISSIONS_USERS_TYPES_CREATE",                                3);
/**
 * Изменять тип пользователей
 */
//define("PERMISSIONS_USERS_TYPES_EDIT",                                  4);
/**
 * Удалять тип пользователей
 */
//define("PERMISSIONS_USERS_TYPES_DELETE",                                5);
/**
 * Связывать тип пользователя с типом роли
 */
//define("PERMISSIONS_USERS_TYPES_LINK_ROLES_TYPES",                      6);


/**
 *
 * Доступ к управлению пользователями
 * @var integer
 */
define("PERMISSIONS_USERS_ADMIN_ACCESS",								6);
/**
 * Просматривать пользователей
 */
define("PERMISSIONS_USERS_VIEW",                                        84);
/**
 * Создавать пользователя
 */
define("PERMISSIONS_USERS_CREATE",                                      18);
/**
 * Изменять данные пользователя
 */
define("PERMISSIONS_USERS_EDIT",                                        19);
/**
 * Удалять пользователя
 */
define("PERMISSIONS_USERS_DELETE",                                      20);
/**
 * Управлять правами пользователя
 */
define("PERMISSIONS_USERS_ACCESS",                                      21);
/**
 * Изменять пароли
 */
define("PERMISSIONS_USERS_PASSWORD",                                    17);
/**
 * Пакетное создание пользователей из файла
 */
define("PERMISSIONS_USERS_CREATE_FROM_FILE",                            98);


/**
 * Создавать тип пользователей
 */
define("PERMISSIONS_USERS_TYPES_CREATE",                                22);
/**
 * Изменять тип пользователей
 */
define("PERMISSIONS_USERS_TYPES_EDIT",                                  23);
/**
 * Удалять тип пользователей
 */
define("PERMISSIONS_USERS_TYPES_DELETE",                                24);
/**
 * Связывать тип пользователя с типом роли
 */
define("PERMISSIONS_USERS_TYPES_LINK_ROLES_TYPES",                      83);


/**
 *
 * Управление партнерами: создание, изменение, удаление
 * @var integer
 * 2
 */
define("PERMISSIONS_PARTNERS_PARTNER_MANAGEMENT",							41);
/**
 *
 * Изменение партнеров
 * @var integer
 * 4
 */
define("PERMISSIONS_PARTNERS_PARTNER_EDIT",									42);
/**
 *
 * Чтение данных партнеров
 * @var integer
 * 8
 */
define("PERMISSIONS_PARTNERS_PARTNER_READY",								43);


/**
 *
 * Управление типами партнеров: создание, изменение, удаление
 * @var integer
 * 16
 */
define("PERMISSIONS_PARTNERS_TYPE_MANAGEMENT",								44);
/**
 *
 * Чтение типов партнеров
 * @var integer
 * 32
 */
define("PERMISSIONS_PARTNERS_TYPE_READY",									45);
/**
 *
 * Управление дилерами
 * @var integer
 * 64
 */
define("PERMISSIONS_DEALERS_MANAGEMENT",									46);
/**
 *
 * Изменения привязки возможностей дилеров к дилерам
 * @var integer
 * 128
 */
define("PERMISSIONS_DEALERS_FACILITYES_RELATE",								47);



/**
 * Управление мероприятиями X-Country
 */
/**
 *
 * Управление кампаниями
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_CAMPAIGNS_MANAGEMENT",						48);
/**
 *
 * Просмотр полной информации об активной кампании
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_CAMPAIGNS_ACTIVE_READY",					49);
/**
 *
 * Просмотр полной информации о любой кампании
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_CAMPAIGNS_READY",							64);
/**
 *
 * Управление мероприятиями
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_ACTIONS_MANAGEMENT",						50);
/**
 *
 * Просмотр полной информации о мероприятии
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_ACTIONS_READY",							51);
/**
 *
 * Управление тест-драйвами
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_TEST_DRIVES_MANAGEMENT",					52);
/**
 *
 * Просмотр полной информации о тест-драйве
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_TEST_DRIVES_READY",						53);
/**
 *
 * Просмотр полной информации о всех тест-драйвах
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_TEST_DRIVES_ALL_READY",					54);
/**
 *
 * Управление заездами и квотами
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_RACES_MANAGEMENT",							55);
/**
 *
 * Просмотр полной информации о заезде
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_RACES_READY",								56);
/**
 *
 * Доступ к отчетам
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_REPORTS_READY",							57);
/**
 *
 * Доступ ко всем отчетам
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_REPORTS_ALL_READY",						58);
/**
 *
 * Управление участниками заездов
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_MEMBERS_MANAGEMENT",						59);
/**
 *
 * Управление всеми участниками заездов
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_MEMBERS_ALL_MANAGEMENT",					60);
/**
 *
 * Просмотр информации об участниках тест-драйва
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_MEMBERS_READY",							61);
/**
 *
 * Просмотр информации об участниках всех тест-драйвов
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_MEMBERS_ALL_READY",						62);
/**
 *
 * Импорт данных
 * @var integer
 */
define("PERMISSIONS_XCOUNTRY_DATA_IMPORT",								63);





/**
 * Управление типами прав
 */
/**
 *
 * Доступ к управлению типами прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_ADMIN_ACCESS",					51);
/**
 *
 * Чтение типов прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_READY",							52);
/**
 *
 * Создание типа прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_CREATE",							53);
/**
 *
 * Изменение типа прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_SAVE",							54);
/**
 *
 * Удаление типа прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_TYPES_DELETE",							55);




/**
 * Управление правами
 */
/**
 *
 * Доступ к управлению правами
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_ADMIN_ACCESS",							56);
/**
 *
 * Чтение прав
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_READY",                                 57);
/**
 *
 * Создание права
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_CREATE",                            	58);
/**
 *
 * Изменение права
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_SAVE",                  				59);
/**
 *
 * Удаление права
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_DELETE",                    			60);
/**
 *
 * Чтение прав для работы клиента
 * @var integer
 */
define("PERMISSIONS_PERMISSIONS_READY_FOR_CLIENT",						61);




/**
 * Управление записью на техническое обслуживание
 */
/**
 *
 * Доступ к управлению записью на техническое обслуживание
 * @var integer
 */
define("PERMISSIONS_TO_ADMIN_ACCESS",                       			87);
/**
 *
 * Управление действиями пользователя во время технического обслуживания
 * @var integer
 */
define("PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT",                        88);
/**
 *
 * Просмотр заявок на техническое обслуживание
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_READY",                                  89);
/**
 *
 * Управление заявками на техническое обслуживание
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_MANAGEMENT",                             90);

/**
 *
 * Создавать комментарии к заявкам
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_COMMENTS_ADD",                           99);

/**
 *
 * Редактировать собственные комментарии к заявкам
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_COMMENTS_EDIT",                          100);

/**
 *
 * Просматривать комментарии к заявкам
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_COMMENTS_READY",                         101);

/**
 *
 * Изменение состояния заявок
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_STATUS_CHANGE",                          102);

/**
 *
 * Читать резальтаты опросов пользователей по заявкам
 * @var integer
 */
define("PERMISSIONS_TO_MEMBERS_POLLINGS_READY",                         103);

/**
 *
 * Читать резальтаты опросов пользователей по заявкам
 * @var integer
 */
define("PERMISSIONS_TO_DEALERS_ACTIONS_MANAGEMENT",                     106);

/**
 *
 * Читать резальтаты опросов пользователей по заявкам
 * @var integer
 */
define("PERMISSIONS_TO_DEALERS_ACTIONS_READY",                          108);

/**
 *
 * Читать резальтаты опросов пользователей по заявкам
 * @var integer
 */
define("PERMISSIONS_TO_EMAILS_MANAGEMENT",                              109);

/**
 *
 * Читать резальтаты опросов пользователей по заявкам
 * @var integer
 */
define("PERMISSIONS_TO_EMAILS_READY",                                   110);

/**
 *
 * Управление специальными сервисными кампаниями
 * @var integer
 */
define("PERMISSIONS_TO_SPATIAL_CAMPAIGN_MANAGEMENT",					146);




define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_MANAGEMENT",		91);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_EMAILS_READY",        	92);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_MANAGEMENT",		93);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MEMBERS_READY",           94);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_MANAGEMENT",		95);
define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_READY",       		96);


define("PERMISSIONS_CUSTOM_ACTIONS_TESTDRIVES_MODELS_COUNT_MANAGEMENT",	153);






/**
 * Создавать роль
 */
define("PERMISSIONS_ROLES_CREATE",									25);
/**
 * Изменять роль
 */
define("PERMISSIONS_ROLES_EDIT",									26);
/**
 * Удалять роль
 */
define("PERMISSIONS_ROLES_DELETE",									27);
/**
 * Получать список ролей
 */
define("PERMISSIONS_ROLES_READ",									28);
/**
 * Получать права доступа к роли
 */
define("PERMISSIONS_ROLES_GET_ACCESS",								29);
/**
 * Устанавливать права доступа к роли
 */
define("PERMISSIONS_ROLES_SET_ACCESS",								30);
/**
 * Получать список разрешений для роли
 */
define("PERMISSIONS_ROLES_GET_PERMISSIONS",							31);
/**
 * Устанавливать разрешения для роли
 */
define("PERMISSIONS_ROLES_SET_PERMISSIONS",							32);
/**
 * Получать список пользователей, для которых установлена данная роль
 */
define("PERMISSIONS_ROLES_GET_USERS",								33);
/**
 * Создавать тип роли
 */
define("PERMISSIONS_ROLESTYPE_CREATE",								34);
/**
 * Изменять тип роли
 */
define("PERMISSIONS_ROLESTYPE_EDIT",								35);
/**
 * Удалять тип роли
 */
define("PERMISSIONS_ROLESTYPE_DELETE",								36);









/**
 * Управление записью на техническое обслуживание
 */
/**
 *
 * Доступ к управлению заявками на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_ADMIN_ACCESS",                     111);
/**
 *
 * Просмотр заявок на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_READY",                    112);
/**
 *
 * Управление заявками на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_MANAGEMENT",               113);

/**
 *
 * Создавать комментарии к заявкам на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_COMMENTS_ADD",             114);

/**
 *
 * Редактировать собственные комментарии к заявкам на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_COMMENTS_EDIT",            115);

/**
 *
 * Просматривать комментарии к заявкам на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_COMMENTS_READY",           116);

/**
 *
 * Изменение состояния заявок на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_STATUS_CHANGE",            117);

/**
 *
 * Читать резальтаты опросов пользователей по заявкам на оценку
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_MEMBERS_POLLINGS_READY",           118);

/**
 * Получение отчетов за весь период по всем заявкам
 * @var integer
 */
define("PERMISSIONS_TIEVALUATION_ALL_MEMBERS_REPORT",				143);

/**
 * Управление формами, связанными с возможностями дилеров
 */
/**
 * Базовый доступ к управлению формами, связанными с возможностями дилеров
 */
define("PERMISSIONS_FACILITIES_FORMS_MANAGEMENT",					121);
/**
 * Управление дилерскими настройками форм
 */
define("PERMISSIONS_FACILITIES_FORMS_BASIC_ACCESS",					120);



/**
 * Базовый доступ к управлению моделями
 */
define("PERMISSIONS_MODELS_BASIC_ACCESS",							12);

define("PERMISSIONS_MODELS_FAMILIES_FACILITY_RELATED",				144);



/**
 * Доступ к расписаниям
 */
define("PERMISSIONS_SHADULER_ACCESS",						148);
/**
 * Чтение расписания
 */
define("PERMISSIONS_SHADULER_READY",						149);
/**
 * Управление расписанием
 */
define("PERMISSIONS_SHADULER_MANAGEMENT",					150);
/**
 * Чтение расписания моделей
 */
define("PERMISSIONS_SHADULER_MODELS",						151);
/**
 * Управление режимом работы
 */
define("PERMISSIONS_SHADULER_TIMETABLE_MANAGEMENT",			152);