<?php
class GeoData extends Data
{
	public function GetCoordinates($IP)
	{
		$this->DB->SetCharSet("cp1251");

		$R = $this->Get("SELECT
	`latitude` AS lt,
	`longitude` AS lg
FROM `tmap`.`blocks` AS b
INNER JOIN `tmap`.`location` AS l ON l.`locId` = b.`locId`
WHERE ".$IP." > `startIpNum` AND ".$IP." < `endIpNum`");

		return $R;
	}

	public function GetNearestDealers($IP, $Count = null, $CityCount = null, $Facility = null, $Latitude = null, $Longitude = null)
	{
		if(!$Count && !$CityCount)
			throw new dmtException("Arguments error");
//$this->Dump(__METHOD__.": ".__LINE__, $Latitude, $Longitude);
		if($Latitude && $Longitude)
		{
			$this->Set("@pLatitude", $Latitude);
			$this->Set("@pLongitude", $Longitude);
		}
		else $this->SetCoordinatesByIP($IP);

		if($Count)
		{
			$R = $this->Get("SELECT
	`partners_division_id` AS partnerId,
	`".DBS_REFERENCES."`.`GetDistance`(
		@pLatitude,
		@pLongitude,
		`latitude`,
		`longitude`) AS partnerDistance,
	`city_id` AS cityId,
	`name` AS partnerName,
	`latitude` AS gLt,
	`longitude` AS gLg,
	`site` AS partnerSite,
	`address` AS partnerAddress,
	`phones` AS partnerPhone,
	`rcode` AS partnerRCode,
	`status` AS partnerStatus,
	`city_id` AS cityId,
	`region_id` AS regionId,
	`country_id` AS countryId,
	`federal_district_id` AS federalDistrictId
FROM `".DBS_REFERENCES."`.`dealersGeoFull`
ORDER BY partnerDistance ASC
LIMIT ".$Count.";");
		}
		elseif($CityCount)
		{
			$Prefix = md5(microtime(true));
			$this->Exec("CREATE TEMPORARY TABLE `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_distance` (
	`dealer_id` INT UNSIGNED NOT NULL ,
	`distance` FLOAT NULL ,
	`city_id` INT UNSIGNED NOT NULL ,
	`name` VARCHAR(255),
	`site` varchar(45) DEFAULT NULL,
	`latitude` double DEFAULT NULL,
	`longitude` double DEFAULT NULL,
	`address` varchar(512) DEFAULT NULL,
	`phones` varchar(512) DEFAULT NULL,
	`rcode` varchar(5) DEFAULT NULL,
	`status` tinyint(3) unsigned DEFAULT NULL,
	`region_id` INT UNSIGNED NOT NULL ,
	`country_id` INT UNSIGNED NOT NULL ,
	`federal_district_id` INT UNSIGNED NOT NULL ,
	PRIMARY KEY (`dealer_id`))
SELECT
	`partners_division_id` AS `dealer_id`,
	`".DBS_REFERENCES."`.`GetDistance`(
		@pLatitude,
		@pLongitude,
		`latitude`,
		`longitude`) AS `distance`,
	`city_id` AS `city_id`,
	`name` AS `name`,
	`site`,
	`latitude`,
	`longitude`,
	`address`,
	`phones`,
	`rcode`,
	`status`,
	`region_id`,
	`country_id`,
	`federal_district_id`
FROM `".DBS_REFERENCES."`.`dealersGeoFull`;");

			$this->Exec("CREATE TEMPORARY TABLE `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_city` (
	`city_id` INT UNSIGNED NOT NULL ,
	PRIMARY KEY (`city_id`))
SELECT DISTINCT
	`city_id`
FROM `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_distance`
WHERE `distance` IS NOT NULL
ORDER BY `distance` ASC
LIMIT ".$CityCount.";");

			$R = $this->Get("SELECT
	`dealer_id` AS partnerId,
	`distance` AS partnerDistance,
	`city_id` AS cityId,
	`name` AS partnerName,
	`latitude` AS gLt,
	`longitude` AS gLg,
	`site` AS partnerSite,
	`address` AS partnerAddress,
	`phones` AS partnerPhone,
	`rcode` AS partnerRCode,
	`status` AS partnerStatus,
	`city_id` AS cityId,
	`region_id` AS regionId,
	`country_id` AS countryId,
	`federal_district_id` AS federalDistrictId
FROM `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_distance`
WHERE `city_id` IN (
	(SELECT
		`city_id`
	FROM `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_city`)
)
ORDER BY `distance` ASC;");
			$this->Exec("DROP TEMPORARY TABLE `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_distance`, `".DBS_UNIVERSAL_REFERENCES."`.`".$Prefix."_city`;");

		}

		return $R;
	}


	public function GetNearestDealersCity($IP, $Count, $Latitude = null, $Longitude = null, $Facility = null, $FacilityAlias = null, $Action = null, $Model = null, $Promo = null)
	{
		if($Latitude && $Longitude)
		{
			$this->Set("@pLatitude", $Latitude);
			$this->Set("@pLongitude", $Longitude);
		}
		else $this->SetCoordinatesByIP($IP);

		$Query = "SELECT
	c.`city_id` AS cityId,
	c.`name` AS cityName,
	a.`distance` AS cityDistance
FROM (
	SELECT DISTINCT
		d.`city_id`,
		`".DBS_REFERENCES."`.`GetDistance`(
			@pLatitude,
			@pLongitude,
			d.`latitude`,
			d.`longitude`) AS distance,
		1 AS gr";
		if($Facility)
		{
			$Query .= "
	FROM `".DBS_REFERENCES."`.`partners_divisions_facilities` AS pdf
	LEFT JOIN `".DBS_REFERENCES."`.`dealers` AS d ON d.`partners_division_id`=pdf.`partners_division_id`
	WHERE pdf.`facility_id`".$this->PrepareValue($Facility)."
		AND pdf.`status`=1
	ORDER BY distance ASC) AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`cities` AS c ON c.`city_id`=a.`city_id`
WHERE a.`distance` IS NOT NULL
LIMIT ".$Count.";";
		}
		elseif($FacilityAlias)
		{
			$Query .= "
	FROM `".DBS_REFERENCES."`.`facilities` AS f
	LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS pdf ON pdf.`facility_id`=f.`facility_id`
	LEFT JOIN `".DBS_REFERENCES."`.`dealers` AS d ON d.`partners_division_id`=pdf.`partners_division_id`
	WHERE f.`alias`".$this->PrepareValue($FacilityAlias)."
		AND pdf.`status`=1
	ORDER BY distance ASC) AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`cities` AS c ON c.`city_id`=a.`city_id`
WHERE a.`distance` IS NOT NULL
LIMIT ".$Count.";";
		}
		elseif($Promo)
		{
			$Query .= "
FROM (
	SELECT DISTINCT
		b.`city_id`,
		b.`latitude`,
		b.`longitude`
	FROM `".DBS_REFERENCES."`.`promo_actions_dealers_models` AS padm
	LEFT JOIN `".DBS_REFERENCES."`.`dealers` AS b ON b.`partners_division_id`=padm.`partners_division_id`
	WHERE padm.`promo_action_id`=".$Promo.") AS d) AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`cities` AS c ON c.`city_id`=a.`city_id`
WHERE a.`distance` IS NOT NULL
LIMIT ".$Count.";";
		}
		else
		{
			$Query .= "
	FROM `".DBS_REFERENCES."`.`dealers` AS d
	ORDER BY distance ASC
	LIMIT ".$Count.") AS a
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`cities` AS c ON c.`city_id`=a.`city_id`
WHERE a.`distance` IS NOT NULL";
		}
//$this->Dump($Query, $Latitude, $Longitude);
		return $this->Get($Query);
	}

	protected function SetCoordinatesByIP($IP)
	{
		$this->Exec("SELECT
	`latitude`,
	`longitude`
INTO
	@pLatitude,
	@pLongitude
FROM `".DBS_UNIVERSAL_REFERENCES."`.`ip_locations`
WHERE ".$IP.">=`ip_start`
	AND ".$IP."<=`ip_end`;");
	}


	/**
	 *
	 * Получить список всех стран, поиск страны по строке
	 * @param string $FindText - Строка поиска
	 */
	public function GetCountries($FindText = null)
	{
		return $this->Get("SELECT
	`country_id` AS countryId,
	`alfa2` AS countryCodeAlpha2,
	`alfa3` AS countryCodeAlpha3,
	`name` AS countryName,
	`short_name` AS countryShortName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`countries`".($FindText ? "
WHERE `name` LIKE ".$this->Esc("%".$FindText."%") : "")."
ORDER BY countryName;");
	}

	/**
	 *
	 * Получить список стран, в которых есть дилеры, поиск страны, в которой есть дилеры, по строке
	 * @param string $FindText - Строка поиска
	 */
	public function GetDealersCountries($FindText = null)
	{
		return $this->Get("SELECT
	`country_id` AS countryId,
	`alfa2` AS countryCodeAlpha2,
	`alfa3` AS countryCodeAlpha3,
	`name` AS countryName,
	`short_name` AS countryShortName
FROM `".DBS_REFERENCES."`.`dealersCountries`".($FindText ? "
WHERE `name` LIKE ".$this->Esc("%".$FindText."%") : "")."
ORDER BY countryName;");
	}


	/**
	 *
	 * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
	 * @param integer $Country - Идентификатор страны
	 * @param string $FindText - Строка поиска
	 */
	public function GetRegion($Country = null, $FindText = null)
	{
		if(!$Country && !$FindText)
			throw new dmtException("Arguments error");

		$Where = array();
		if($Country)
		{
			$Where[] = "`country_id`".$this->PrepareValue($Country);
		}
		if($FindText)
		{
			$Where[] = "`name` LIKE ".$this->Esc("%".$FindText."%");
		}
		return $this->Get("SELECT
	`region_id` AS regionId,
	`country_id` AS countryId,
	`name` AS regionName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`regions`
WHERE ".implode(" AND ", $Where)."
ORDER BY regionName;");
	}


	/**
	 *
	 * Получить список  регионов, в которых есть дилеры, для заданной страны, поиск региона, в котором есть дилеры, по строке
	 * @param integer $Country - Идентификатор страны
	 * @param string $FindText - Строка поиска
	 */
	public function GetDealersRegion($Country = null, $FindText = null, $Facilities = null, $FacilityAlias = null)
	{
		if(!$Country)
			$Country = 172;

		$Fields = "SELECT
	dr.`region_id` AS regionId,
	dr.`country_id` AS countryId,
	dr.`name` AS regionName";

		$Query = "
FROM `".DBS_REFERENCES."`.`dealersRegions` AS dr";

		$Where = array();
		if($Country)
		{
			$Where[] = "dr.`country_id`".$this->PrepareValue($Country);
		}
		if($FindText)
		{
			$Where[] = "dr.`name` LIKE ".$this->Esc("%".$FindText."%");
		}

		if($Facilities || $FacilityAlias)
			$Query .= "
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions_facilities` AS pdf ON pdf.``";

		$Query .= "
WHERE ".implode(" AND ", $Where)."
ORDER BY regionName";

		return $this->Get($Fields.$Query);
	}



	/**
	 *
	 * Получить список всех городов для заданной страны, поиск города по строке
	 * @param integer $Region - Идентификатор региона
	 * @param string $FindText - Строка поиска
	 */
	public function GetCities($Region = null, $FindText = null, $Country = null)
	{
		if(!$Region && !$FindText && !$Country)
			throw new dmtException("Arguments error");
		$Where = array();
		if($Region)
		{
			$Where[] = "`region_id`".$this->PrepareValue($Region);
		}
		if($Country)
		{
			$Where[] = "`country_id`".$this->PrepareValue($Country);
		}
		if($FindText)
		{
			$Where[] = "`name` LIKE ".$this->Esc("%".$FindText."%");
		}
		return $this->Get("SELECT
	`city_id` AS cityId,
	`country_id` AS countryId,
	`region_id` AS regionId,
	`name` AS cityName,
	`latitude` AS gLt,
	`longitude` AS gLg
FROM `".DBS_UNIVERSAL_REFERENCES."`.`cities`
WHERE ".implode(" AND ", $Where)."
ORDER BY cityName;");
	}

	/**
	 *
	 * Получить список всех городов для заданной страны, поиск города по строке
	 * @param integer $Country - Идентификатор страны
	 * string $FindText - Строка поиска
	 */
	public function GetDealersCities($Region = null, $Country = null, $FindText = null, $Facility = null, $Action = null, $Model = null, $FacilityStatus = null, $DealerStatus = null, $FacilityPool = null)
	{
        $Distinct = false;
		$Query = "
	`c`.`city_id` AS cityId,
	`c`.`country_id` AS countryId,
	`c`.`region_id` AS regionId,
	`c`.`name` AS cityName,
	`c`.`latitude` AS gLt,
	`c`.`longitude` AS gLg
FROM ";
		$Where = array();
		if($Region)
		{
			$Where[] = "`c`.`region_id`".$this->PrepareValue($Region);
		}
		elseif($Country)
		{
			$Where[] = "`c`.`country_id`".$this->PrepareValue($Country);
		}
		if($FindText)
		{
			$Where[] = "`c`.`name` LIKE ".$this->Esc("%".$FindText."%");
		}

        if($Action)
        {
            $Distinct = true;
             $Query .= "`".DBS_REFERENCES."`.`partners_divisions` AS  `pd`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`cities` `c` ON `c`.`city_id` = `pd`.`city_id`";
			 $Where[] = "`pd`.`status`".  $this->PrepareValue($DealerStatus !== null && !is_empty($DealerStatus) ? $DealerStatus : 1);
            if($Model)
            {
                 $Where[] = "`pd`.`partners_division_id` IN (
        SELECT DISTINCT
            `partners_division_id`
        FROM `".DBS_REFERENCES."`.`custom_actions_partners_models`
        WHERE `action_id`=".$Action."
            AND ".(SITE_CURRENT == SITE_LEXUS ? "`submodel_id`" : "`model_id`").$this->PrepareValue($Model)."
    )";
            }
            else
            {
                $Where[] = "`pd`.`partners_division_id` IN (
        SELECT DISTINCT
            `partners_division_id`
        FROM `".DBS_REFERENCES."`.`custom_actions_partners_models`
        WHERE `action_id`
    )";
            }
			$Where[] = "pd.`status`".($DealerStatus ? $this->PrepareValue($DealerStatus) : "=1");
        }
		elseif($Facility)
		{
            $Distinct = true;
			$Query .= "`".DBS_REFERENCES."`.`dealersCityFF` AS `c`";
			$Where[] = "`c`.`facility_id`".$this->PrepareValue($Facility);
			if($FacilityStatus)
                $Where[] = "`c`.`status`".$this->PrepareValue($FacilityStatus);
			else $Where[] = "`c`.`status`=1";
			$Where[] = "`c`.`partners_divisions_status`".  $this->PrepareValue($DealerStatus !== null && !is_empty($DealerStatus) ? $DealerStatus : 1);
		}
		else
		{
			$Query .= "`".DBS_REFERENCES."`.`dealersCity` AS `c`";
			$Where[] = "`c`.`partners_divisions_status`".  $this->PrepareValue($DealerStatus !== null && !is_empty($DealerStatus) ? $DealerStatus : 1);
		}
		$Where[] = "`c`.`city_id` IS NOT NULL";

        $Query = "SELECT ".($Distinct ? "DISTINCT " : "").$Query.$this->PrepareWhere($Where)."
ORDER BY `c`.`name`;";

		return $this->Get($Query);
	}

	/**
	 *
	 * Получить список всех городов для заданной страны, поиск города по строке
	 * @param integer $Country - Идентификатор страны
	 * string $FindText - Строка поиска
	 */
	public function GetDealersCitiesNew($Brand, $Region = null, $Country = null, $FindText = null, $Facility = null, $Model = null, $FacilityStatus = null, $DealerStatus = null, $FacilityPool = null)
	{
        $Distinct = false;
		$Query = "
	`c`.`city_id` AS cityId,
	`c`.`country_id` AS countryId,
	`c`.`region_id` AS regionId,
	`c`.`name` AS cityName,
	`c`.`latitude` AS gLt,
	`c`.`longitude` AS gLg
FROM ";
		$Where = array();
		$Where[] = "c.`brand_id`=".$Brand;
		if($Region)
		{
			$Where[] = "`c`.`region_id`".$this->PrepareValue($Region);
		}
		elseif($Country)
		{
			$Where[] = "`c`.`country_id`".$this->PrepareValue($Country);
		}
		if($FindText)
		{
			$Where[] = "`c`.`name` LIKE ".$this->Esc("%".$FindText."%");
		}
		$Where[] = "c.`partners_divisions_status`".$this->PrepareValue($DealerStatus !== null && !is_empty($DealerStatus) ? $DealerStatus : 1);
		if($Facility || $FacilityPool)
		{
            $Distinct = true;
			$Query .= "`".DBS_UNIVERSAL_REFERENCES."`.`dealersCityFF` AS `c`";

			if($Model)
            {
				$Query .= "
LEFT JOIN `references`.`facilities_models` AS d ON d.`facility_id`=c.`facility_id`
";
				$Where[] = "`c`.`model_id`".$this->PrepareValue($Model);
			}
			if($Facility)
				$Where[] = "`c`.`facility_id`".$this->PrepareValue($Facility);
			else $Where[] = "`c`.`alias`".$this->PrepareValue($FacilityPool, true);
			$Where[] = "`c`.`status`".$this->PrepareValue($FacilityStatus !== null && !is_empty($FacilityStatus) ? $FacilityStatus : 1);
			$Where[] = "`c`.`partners_divisions_status`".  $this->PrepareValue($DealerStatus !== null && !is_empty($DealerStatus) ? $DealerStatus : 1);
		}
		else
		{
			$Query .= "`".DBS_UNIVERSAL_REFERENCES."`.`dealersCity` AS `c`";
			$Where[] = "`c`.`partners_divisions_status`".  $this->PrepareValue($DealerStatus !== null && !is_empty($DealerStatus) ? $DealerStatus : 1);
		}
		$Where[] = "`c`.`city_id` IS NOT NULL";

        $Query = "SELECT ".($Distinct ? "DISTINCT " : "").$Query.$this->PrepareWhere($Where)."
ORDER BY `c`.`name`;";

		$this->Dump(__METHOD__.": ".__LINE__, $Query);
		return $this->Get($Query);
	}

	public function GetFederalDistrict()
	{
		return $this->Get("SELECT
	`federal_district_id` AS federalDistrictId,
	`name` AS federalDistrictName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`federal_districts`
ORDER BY federalDistrictName");
	}

	public function GetDealersFederalDistrict()
	{
		return $this->Get("SELECT
	`federal_district_id` AS federalDistrictId,
	`name` AS federalDistrictName
FROM `".DBS_REFERENCES."`.`dealersFederalDistricts`
ORDER BY federalDistrictName;");
	}

	public function GetCityName($City)
	{
		return $this->Count("SELECT
	`name` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`cities`
WHERE `city_id`=".$City.";");
	}

	public function GetCityByCoordinates($Latitude, $Longitude)
	{
		return $this->Count("SELECT
	`city_id` As Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`cities`
WHERE `latitude`=".$Latitude."
	AND `longitude`=".$Longitude.";");
	}
}