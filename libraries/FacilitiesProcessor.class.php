<?php
if(!defined("PARTNERS_SERVICE_ID"))
	define("PARTNERS_SERVICE_ID",												16);

/**
 *
 * Создавать возможности
 * @var integer
 */
define("PERMISSIONS_FACILITIES_CREATE",											38);
/**
 *
 * Изменять возможности
 * @var integer
 */
define("PERMISSIONS_FACILITIES_EDIT",											39);
/**
 *
 * Удалять возможности
 * @var integer
 */
define("PERMISSIONS_FACILITIES_DELETE",											40);


class FacilitiesProcessor extends Processor
{
	/**
	 *
	 * @var FacilitiesProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var FacilitiesData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return FacilitiesProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new FacilitiesData();
	}

	/**
	 *
	 * Возвращает список возможностей
	 */
	public function GetFacilities($Type = null, $Dealer = null)
	{
		return $this->CData->GetFacilities(null, $Type, $Dealer);
	}
	/**
	 *
	 * Возвращает список возможностей
	 */
	public function GetFacilitiesNew($Type = null, $Dealer = null)
	{
		return $this->CData->GetFacilitiesNew(WS::Init()->GetBrandId(), null, $Type, $Dealer);
	}
    /**
	 *
	 * Возвращает список возможностей
	 */
	public function GetFacilities2()
	{
		return $this->CData->GetFacilities(null, 2);
	}


	/**
	 *
	 * Создает возможность
	 * @param string $Name - Наименование возможности
	 * @param string $Desc - Необязательный. Описание возможности
	 * @param string $Icon - Необязательный. Путь к миниатюре
	 */
	public function AddFacility($Name, $Desc = null, $Icon = null)
	{
		//$Name, $AsFilter, $Desc = null, $Icon = null, $AdvancedInfo = null
		return array("facilityId" => $this->CData->AddFacility($Name, $Desc, $Icon));
	}

	/**
	 *
	 * Сохраняет изменения возможности
	 * @param integer $Facility - Идентификатор возможности
	 * @param string $Name - Наименование возможности
	 * @param string $Desc - Необязательный. Описание возможности
	 * @param string $Icon - Необязательный. Путь к миниатюре
	 */
	public function SaveFacility($Facility, $Name = null, $Desc = null, $Icon = null)
	{
		//$Facility, $Name = null, $AsFilter = null, $Desc = null, $Icon = null, $AdvancedInfo = null
		$this->CData->SaveFacility($Facility, $Name, $Desc, $Icon);
	}

	/**
	 *
	 * Сохраняет миниатюру возможности
	 * @param integer $Facility - Идентификатор возможности
	 * @param string $Icon - Необязательный. Путь к миниатюре
	 */
	public function SaveFacilityIcon($Facility, $Icon)
	{

	}

	/**
	 *
	 * Удаляет возможность
	 * @param integer $Facility - Идентификатор возможности
	 */
	public function DeleteFacility($Facility)
	{
		return $this->CData->DeleteFacility($Facility);
	}

	/**
	 *
	 * Возвращает список возможностей дилеров, помеченных для использования в качестве фильтра поиска
	 * @param mixed $Facility - Идентификатор или индексный массив идентификаторов возможностей
	 */
	public function GetFacilitiesAsFilter()
	{
		return $this->CData->GetFacilities(true);
	}
    /**
	 *
	 * Возвращает список возможностей дилеров, помеченных для использования в качестве фильтра поиска
	 * @param mixed $Facility - Идентификатор или индексный массив идентификаторов возможностей
	 */
	public function GetFacilitiesAsFilter2()
	{
		$R = $this->CData->GetFacilities(null, 2);
        return $R;
	}



	public function GetDealersState($Facility)
	{
$this->Dump(__METHOD__.": ".__LINE__, "--------------------------------------------------");
		return $this->CData->GetDealersState($Facility);
	}

	public function SaveDealersModelsAll($AllStatus)
	{
		$this->CData->SaveDealersModelsAll(WS::Init()->GetBrandId(), $AllStatus);
	}
	
	public function SaveDealersModelsCountAll($AllModel)
	{
		$this->CData->SaveDealersModelsCountAll(WS::Init()->GetBrandId(), $AllModel);
	}
	
	public function GetDealersModelsAll($Facility, $Status = null)
	{
		if($Status)
		{
			if(!is_array($Status))
				$Status = $Status ? 1 : 0;
		}
		else $Status = 1;
		return $this->CData->GetDealersModelsAll(WS::Init()->GetBrandId(), $Facility, $Status);
	}
	
	public function GetDealersModelsCountAll($Facility)
	{
		return $this->CData->GetDealersModelsCountAll(WS::Init()->GetBrandId(), $Facility);
	}

	public function SaveDealersFacilitiesAll($AllStatus)
	{
		$this->CData->SaveDealersFacilitiesAll(WS::Init()->GetBrandId(), $AllStatus);
	}
	
	public function GetRelatedFacilities()
	{
		return $this->CData->GetRelatedFacilities(WS::Init()->GetBrandId());
	}
	public function ImportDealersModels($Facility, $File)
	{
		PermissionsProcessor::Init()->CheckAccess(array(PERMISSIONS_TEST_DRIVES_DEALERS_MODELS_IMPORT));
	
		if(!$File)
			throw new dmtException("File is not found");

		$File = $File[0];
		$Models = array();
		$R = ModelsProcessor::Init()->GetModelsNewForImport();
		foreach($R as $v)
			$Models[$v["modelName"]] = $v["modelId"];
		$Dealers = array();
		$R = DealersProcessor::Init()->GetDealersNewForImport();
		foreach($R as $v)
			$Dealers[$v["partnerRCode"]] = $v["partnerDivisionId"];
		$F = fopen(PATH_SERVICES_UPLOAD_FILE.$File, "r");
		$R = array();
		while(($String = fgetcsv($F, null, ";")) !== false) {
			if(!isset($Dealers[$String[0]]) || !isset($Models[$String[1]]))
			{
				//TODO: LOG
				continue;
			}
			$R[] = "(".$Dealers[$String[0]].",".$Facility.",".$Models[$String[1]].",1".")";
			
		}
		if(sizeof($R))
			$this->CData->ImportDealersModels(WS::Init()->GetBrandId(), $R, $Facility);
	}
	
	public function GetFacilityName($Facility)
	{
		return $this->CData->GetFacilityName($Facility);
	}
}