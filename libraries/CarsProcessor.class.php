<?php
if(!defined("PARTNERS_SERVICE_ID"))
	define("PARTNERS_SERVICE_ID",												6);


class CarsProcessor extends Processor
{
	/**
	 * 
	 * @var CarsProcessor
	 */
	protected static $Inst = false;
	
	/**
	 * 
	 * Класс данных
	 * @var CarsData
	 */
	private $CData;
	
	
	/**
	 * 
	 * Инициализирует класс
	 * 
	 * @return CarsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}
	
	protected function __construct()
	{
		parent::__construct();
		$this->CData = new CarsData();
	}
	
	
	public function GetManufacturersList()
	{
		return $this->CData->GetManufacturersList();
	}
	
	public function GetCarsForManufacturer($Manufacturer)
	{
		return $this->CData->GetCarsForManufacturer($Manufacturer);
	}
	
	public function GetManufacturersList2()
	{
		return $this->CData->GetManufacturersList2();
	}
	
	public function GetCarsForManufacturer2($Manufacturer)
	{
		return $this->CData->GetCarsForManufacturer2($Manufacturer);
	}
	
	public function GetCarsF()
	{
		return $this->CData->GetCarsF();
	}
	
	public function GetCarFullName($Car)
	{
		return $this->CData->GetCarFullName($Car);
	}
}