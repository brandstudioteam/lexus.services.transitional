<?php
class FacilitiesFormsData extends Data
{
	private $Queries = array(
		"forEmail" => array(
			"50"	=> "SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
	a.`first_name` AS `name`,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`register` AS memberRegisterU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 3600), '%H:%i:%s'))) AS memberVisitTime,
	a.`from_dealers_site` AS fds,
    b.`name` AS modelName,
	b.`name` AS modelFullName,
    b.`alias` AS modelAlias,
    c.`name` AS partnersDivisionName,
	c.`name` AS partnerName,
	c.`phones` AS partnerPhone,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`models` AS b ON b.`model_id`=a.`model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`
WHERE a.`member_id`",

			"52"	=> "SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	a.`traffic_source_id` AS trafficSourceId,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`other_info` AS advancedInfo,
    a.`custom_city` AS customCity,
	a.`from_dealers_site` AS fds,
    b.`name` AS contactingThemeName
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`facilities_forms_contactings_themes` AS b ON b.`contactings_theme_id`=a.`contactings_theme_id`
WHERE a.`member_id`",

			"53"	=> "SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    DATE_FORMAT(a.`visit_time`, '%H:%i:%s') AS memberVisitTime,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`other_info` AS advancedInfo,
    a.`custom_city` AS customCity,
	a.`from_dealers_site` AS fds,
    b.`name` AS contactingThemeName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`facilities_forms_contactings_themes` AS b ON b.`contactings_theme_id`=a.`contactings_theme_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`
WHERE a.`member_id`",
			
			
			"76"	=> "SELECT
	a.`member_id` AS memberId,
	a.`partners_division_id` AS partnersDivisionId,
	a.`current_model_id` AS  currentModelId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`register` AS memberRegisterU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 10800), '%H:%i:%s'))) AS memberVisitTime,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    CONCAT(b.`manufacturer_name`, ' ', b.`model_name`) AS currentModelName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`cars_wm_f` AS b ON b.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`
WHERE a.`member_id`",
			
			"77"	=> "SELECT
	a.`member_id` AS memberId,
	a.`partners_division_id` AS partnersDivisionId,
	a.`current_model_id` AS  currentModelId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`register` AS memberRegisterU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 10800), '%H:%i:%s'))) AS memberVisitTime,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    CONCAT(b.`manufacturer_name`, ' ', b.`model_name`) AS currentModelName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`cars_wm_f` AS b ON b.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`
WHERE a.`member_id`",
		),
		
		
		"members" => array(
			"50"	=> "SELECT
	a.`member_id` AS memberId,
	a.`partners_division_id` AS partnersDivisionId,
	a.`model_id` modelId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`register` AS memberRegisterU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 3600), '%H:%i:%s'))) AS memberVisitTime,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    b.`name` AS modelName,
    b.`alias` AS modelAlias,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`models` AS b ON b.`model_id`=a.`model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",

			"52"	=> "SELECT
	a.`member_id` AS memberId,
	a.`partners_division_id` AS partnersDivisionId,
	a.`model_id` modelId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	a.`traffic_source_id` AS trafficSourceId,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`other_info` AS advancedInfo,
    a.`custom_city` AS customCity,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    b.`name` AS contactingThemeName
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`facilities_forms_contactings_themes` AS b ON b.`contactings_theme_id`=a.`contactings_theme_id`",

			"53"	=> "SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    DATE_FORMAT(a.`visit_time`, '%H:%i:%s') AS memberVisitTime,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`other_info` AS advancedInfo,
    a.`custom_city` AS customCity,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    b.`name` AS contactingThemeName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`facilities_forms_contactings_themes` AS b ON b.`contactings_theme_id`=a.`contactings_theme_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",
			
			"76"	=> "SELECT
	a.`member_id` AS memberId,
	a.`partners_division_id` AS partnersDivisionId,
	a.`current_model_id` AS  currentModelId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`register` AS memberRegisterU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 10800), '%H:%i:%s'))) AS memberVisitTime,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    CONCAT(b.`manufacturer_name`, ' ', b.`model_name`) AS currentModelName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`cars_wm_f` AS b ON b.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",
			
			"77"	=> "SELECT
	a.`member_id` AS memberId,
	a.`partners_division_id` AS partnersDivisionId,
	a.`current_model_id` AS  currentModelId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	a.`register` AS memberRegisterU,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 10800), '%H:%i:%s'))) AS memberVisitTime,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
    CONCAT(b.`manufacturer_name`, ' ', b.`model_name`) AS currentModelName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`cars_wm_f` AS b ON b.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",
		),
		
		
		"membersForReport" => array(
			"50"	=> "SELECT
	a.`first_name` AS firstName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS gender,
	a.`age` AS age,
	c.`name` AS partnersDivisionName,
	 b.`name` AS modelName,
	a.`email` AS email,
	a.`phone` AS phone,
	-- IF(a.`is_owner` IS NULL OR a.`is_owner`=0, 'Нет', 'Да') AS isOwner,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 3600), '%H:%i:%s'))) AS memberVisitTime,
	a.`traffic_source_id` AS trafficSourceId,
    -- DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
	a.`member_id` AS memberId
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`models` AS b ON b.`model_id`=a.`model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",

			"52"	=> "SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	a.`traffic_source_id` AS trafficSourceId,
	-- IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
	a.`other_info` AS advancedInfo,
    a.`custom_city` AS customCity,
    b.`name` AS contactingThemeName
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`facilities_forms_contactings_themes` AS b ON b.`contactings_theme_id`=a.`contactings_theme_id`",

			"53"	=> "SELECT
	a.`member_id` AS memberId,
	a.`first_name` AS firstName,
    a.`middle_name` AS middleName,
	a.`last_name` AS lastName,
	IF(a.`gender` = 1, 'F', 'M') AS gender,
	IF(a.`gender` = 1, 'Женский', 'Мужской') AS genderName,
	a.`age` AS age,
	a.`email` AS email,
	a.`phone` AS phone,
	DATE_FORMAT(a.`register`, '%d.%m.%Y') AS memberRegisterDate,
    DATE_FORMAT(a.`register`, '%H:%i:%s') AS memberRegisterTime,
	a.`traffic_source_id` AS trafficSourceId,
    DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
    DATE_FORMAT(a.`visit_time`, '%H:%i:%s') AS memberVisitTime,
	-- IF(a.`is_owner` IS NULL, 0, a.`is_owner`) AS isOwner,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
	a.`other_info` AS advancedInfo,
    a.`custom_city` AS customCity,
    b.`name` AS contactingThemeName,
    c.`name` AS partnersDivisionName,
    c.`rcode` AS RCode
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`facilities_forms_contactings_themes` AS b ON b.`contactings_theme_id`=a.`contactings_theme_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",
			
			
			"76"	=> "SELECT
	a.`first_name` AS firstName,
	c.`name` AS partnersDivisionName,
	CONCAT(b.`manufacturer_name`, ' ', b.`model_name`) AS currentModelName,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
	IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 10800), '%H:%i:%s'))) AS memberVisitTime,
	a.`traffic_source_id` AS trafficSourceId,
	a.`member_id` AS memberId
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`cars_wm_f` AS b ON b.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",
			
			"77"	=> "SELECT
	a.`first_name` AS firstName,
	c.`name` AS partnersDivisionName,
	CONCAT(b.`manufacturer_name`, ' ', b.`model_name`) AS currentModelName,
	a.`email` AS email,
	a.`phone` AS phone,
	IF(a.`from_dealers_site` = 1, 'Да', 'Нет') AS fds,
	DATE_FORMAT(a.`register`, '%d.%m.%Y %H:%i:%s') AS memberRegister,
	DATE_FORMAT(a.`visit_date`, '%d.%m.%Y') AS memberVisitDate,
	IF(a.`visit_time` ='00:00:00' OR a.`visit_time` IS NULL, 'В ближайшее возможное время', CONCAT(DATE_FORMAT(a.`visit_time`, '%H:%i:%s'), '-', DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(a.`visit_time`) + 10800), '%H:%i:%s'))) AS memberVisitTime,
	a.`traffic_source_id` AS trafficSourceId,
	a.`member_id` AS memberId
FROM `references`.`facilities_forms_members` AS a
LEFT JOIN `references`.`cars_wm_f` AS b ON b.`model_id`=a.`current_model_id`
LEFT JOIN `references`.`partners_divisions` AS c ON c.`partners_division_id`=a.`partners_division_id`",
		)
	);


	public function GetDealersFormSet($Facility, $FormType, $Dealer)
	{
		$Query = "SELECT
	a.`facility_id` AS facilityId,
	a.`brand_id` AS brandId,
	a.`partners_division_id` AS partnerDivisionId,
	a.`form_type_id` AS formType,
	a.`agreement_title` AS agreementTitle,
	a.`agreement_text` AS agreementText,
	a.`google_analitics_account_id` AS gaAccount
FROM `references`.`facilities_forms_partners_divisions_set` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`";
		$Where = array();
		$Where[] = "a.`facility_id`".$this->PrepareValue($Facility);
		$Where[] = "a.`form_type_id`".$this->PrepareValue($FormType);
		if($Dealer)
			$Where[] = "b.`partners_division_id`".$this->PrepareValue($Dealer);

		$this->Dump(__METHOD__.": ".__LINE__, $Query.$this->PrepareWhere($Where));

		return $this->Get($Query.$this->PrepareWhere($Where), $Dealer && (!is_array($Dealer) || sizeof($Dealer) == 1));
	}

	public function GetDealersFormSetForOld($Facility, $FormType, $Dealer)
	{
$this->Debug(__METHOD__.": ".__LINE__, $Facility, $FormType, $Dealer);

		$Query = "SELECT
	a.`facility_id` AS facilityId,
	a.`brand_id` AS brandId,
	b.`old_id` AS partnerDivisionId,
	a.`form_type_id` AS formType,
	a.`agreement_title` AS agreementTitle,
	a.`agreement_text` AS agreementText,
	a.`google_analitics_account_id` AS gaAccount
FROM `references`.`facilities_forms_partners_divisions_set` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`";
		$Where = array();
		$Where[] = "a.`facility_id`".$this->PrepareValue($Facility);
		$Where[] = "a.`form_type_id`".$this->PrepareValue($FormType);
		$Where[] = "b.`old_id`".$this->PrepareValue($Dealer);

		$this->Debug(__METHOD__.": ".__LINE__, $Query.$this->PrepareWhere($Where));
		return $this->Get($Query.$this->PrepareWhere($Where), true);
	}


	public function GetDealersFormSetForAdmin($Dealer, $Facility = null, $FormType = null)
	{
		$Query = "SELECT DISTINCT
	n.`facility_id` AS facilityId,
	n.`brand_id` AS brandId,
	n.`partners_division_id` AS partnerDivisionId,
	n.`form_type_id` AS formType,
    n.`name` AS partnerDivisionName,
	b.`agreement_title` AS agreementTitle,
	b.`agreement_text` AS agreementText,
	b.`google_analitics_account_id` AS gaAccount,
    d.`name` AS facilityName
FROM (
	SELECT DISTINCT
		a.`facility_id` AS `facility_id`,
        a.`form_type_id` AS `form_type_id`,
		c.`brand_id` AS `brand_id`,
		c.`partners_division_id` AS `partners_division_id`,
		c.`name` AS `name`
	FROM `references`.`partners_divisions` AS c
	LEFT JOIN `references`.`facilities_forms` AS a ON a.`brand_id`=c.`brand_id`
    WHERE a.`enable_dealers_sets`=1
		AND c.`partners_division_id`".$this->PrepareValue($Dealer)."
    ORDER BY `partners_division_id`, `facility_id`
) AS n
LEFT JOIN `references`.`facilities_forms_partners_divisions_set` AS b ON b.`facility_id`=n.`facility_id` AND b.`brand_id`=n.`brand_id` AND b.`form_type_id`=n.`form_type_id` AND b.`partners_division_id`=n.`partners_division_id`
LEFT JOIN `references`.`facilities` AS d ON d.`facility_id`=n.`facility_id`";
		$AsOne = false;
		$Where = array();
		if($Facility && $FormType)
		{
			$Where[] = "n.`facility_id`=".$Facility;
			$Where[] = "n.`form_type_id`=".$FormType;
			$AsOne = true;
		}
		return $this->Get($Query.$this->PrepareWhere($Where), $AsOne);
	}

	public function SaveDealersFormSet($Facility, $FormType, $Dealer, $AgreementTitle, $AgreementText, $GA)
	{
		return $this->Exec("INSERT INTO `references`.`facilities_forms_partners_divisions_set`
	(`facility_id`,
	`partners_division_id`,
	`form_type_id`,
	`brand_id`,
	`agreement_title`,
	`agreement_text`,
	`google_analitics_account_id`)
VALUES
	(".$Facility.",
	".$Dealer.",
	".$FormType.",
	".WS::Init()->GetBrandId().",
	".$this->Esc($AgreementTitle).",
	".$this->Esc($AgreementText).",
	".$this->Esc($GA).")
ON DUPLICATE KEY UPDATE
	`agreement_title`=VALUES(`agreement_title`),
	`agreement_text`=VALUES(`agreement_text`),
	`google_analitics_account_id`=VALUES(`google_analitics_account_id`);");
	}

	public function GetFullDataForm($Facility, $FormType, $Dealer = null)
	{
		$Query = "";
		$Where = array();
		$Where[] = "";
		$Where[] = "";
		if($Dealer)
			$Where[] = "".$this->PrepareValue($Dealer);
		return $this->Get($Query.$this->PrepareWhere($Where));
	}
















	public function GetSourceInfoList($Facility)
	{
		return $this->Get("SELECT
	`source_info_id` AS sourceInfoId,
	`name` AS sourceInfoName
FROM `".DBS_REFERENCES."`.`custom_actions_source_info_n`
WHERE `action_id`=".$Facility.";");
	}
                             //($Action, $Model, $Dealer, $LastName, $FirstName, $MiddleName, $Email, $Phone, $Gender, $Age, $IsOwner, $AdvInfo, $Type,
                             //$Referrer = null, $FromDealerSite = null, $FromMobile = null, $UserAgent = null, $CurrentModel = null, $FormType = null)
	public function AddMember(  $Brand, $Facility, $FormType, $Dealer, $Model, $LastName, $FirstName, $MiddleName, $Email, $Phone, $ContactMethod, $Gender, $Age,
								$IsOwner, $AdvancedInfo, $TrafficSource, $CurrentCar = null, $CurrentCarManufacturer = null, $CarYear = null, $Miliage = null,
								$RegNumber = null, $City = null, $SourceInfo = null, $VizitDate = null, $VizitTime = null, $VIN = null, $Status = null,
								$Configuration = null, $ModelColor = null, $ActionCode = null, $ActionStatus = null, $UserAction = null, $PhotoCount = null,
								$PhotosNames = null, $TOType = null, $Referrer = null, $FromDealerSite = null, $FromMobile = null, $UserAgent = null,
								$ContactingTheme = null, $CustomCity = null, $OldDealer = null)
	{
/*
		$this->Exec("UPDATE `references`.`facilities_forms`
SET `tmr_attachment_mail_template`=".$this->Esc('ID:<{%memberId%}>
Create date:<{%memberRegisterDate%}>
Create time:<{%memberRegisterTime%}>
First name: <{%firstName%}>
Last name: <{%lastName%}>
Sex: <{%gender%}>
Age: <{%age%}>
E-Mail: <{%email%}>
Dealer center: <{%RCode%}>
Contact phone: <{%phone%}>
Model: <{%modelAlias%}>
Lexus owner: <{%isOwner%}>
DesiredCallTime: <{%memberVisitTime%}>')."
WHERE `form_id`=1;");

*/
		$this->Exec("INSERT INTO `references`.`facilities_forms_members`
	(`brand_id`,
	`facility_id`,
	`form_type_id`,
	`partners_division_id`,
	`model_id`,
	`configuration_id`,
	`model_color`,
	`first_name`,
	`middle_name`,
	`last_name`,
	`gender`,
	`age`,
	`email`,
	`phone`,
	`contact_method_id`,
	`city_id`,
	`manufacturer_id`,
	`current_model_id`,
	`model_years`,
	`mileage`,
	`reg_number`,
	`vin`,
	`is_owner`,
	`register`,
	`other_info`,
	`visit_date`,
	`visit_time`,
	`traffic_source_id`,
	`action_status`,
	`action_code`,
	`source_info_id`,
	`from_dealers_site`,
	`from_mobile`,
	-- `user_action_id`,
	`to_type_id`,
	`user_action_name`,
	`photo_count`,
	`photos_names`,
	`referrer`,
	`user_agent`,
	`contactings_theme_id`,
	`custom_city`,
	`current_status_id`,
	`old_dealer_id`)
VALUES (".$Brand.",
	".$Facility.",
	".$FormType.",
	".$this->CheckNull($Dealer).",
	".$this->CheckNull($Model).",
	".$this->CheckNull($Configuration).",
	".$this->Esc($ModelColor).",
	".$this->Esc($FirstName).",
	".$this->Esc($MiddleName).",
    ".$this->Esc($LastName).",
	".$this->CheckNull($Gender).",
	".$this->CheckNull($Age).",
	".$this->Esc($Email).",
	".$this->Esc($Phone).",
	".$this->CheckNull($ContactMethod).",
	".$this->CheckNull($City).",
	".$this->CheckNull($CurrentCarManufacturer).",
	".$this->CheckNull($CurrentCar).",
	".$this->Esc($CarYear).",
	".$this->CheckNull($Miliage).",
	".$this->Esc($RegNumber).",
	".$this->Esc($VIN).",
	".$this->CheckNull($IsOwner).",
	NOW(),
	".$this->Esc($AdvancedInfo).",
	".$this->Esc($VizitDate).",
	".$this->Esc($VizitTime).",
	".$this->CheckNull($TrafficSource).",
	".$this->CheckNull($ActionStatus).",
	".$this->Esc($ActionCode).",
	".$this->CheckNull($SourceInfo).",
	".($FromDealerSite ? 1 : 0).",
	".($FromMobile ? 1 : 0).",
	-- ".$this->Esc($UserAction).",
	".$this->CheckNull($TOType).",
	".$this->Esc($UserAction).",
	".$this->CheckNull($PhotoCount).",
	".$this->Esc($PhotosNames).",
    ".$this->Esc($Referrer).",
    ".$this->Esc($UserAgent).",
	".$this->CheckNull($ContactingTheme).",
	".$this->Esc($CustomCity).",
	".$Status.",
	".$this->CheckNull($OldDealer).");");

        return $this->DB->GetLastID();
	}

	public function DeleteMember($Member)
	{

		$R = $this->Exec("UPDATE `references`.`facilities_forms_members`
SET `current_status_id`=5
WHERE `member_id`=".$Member.";");
	}

	public function SaveMember(	$Member, $Model = null, $LastName = null, $FirstName = null, $MiddleName = null, $Email = null, $Phone = null, $ContactMethod = null, $Gender = null,
								$Age = null, $IsOwner = null, $AdvancedInfo = null, $TrafficSource = null, $CurrentCar = null, $CurrentCarManufacturer = null, $CarYear = null,
								$Miliage = null, $RegNumber = null, $City = null, $SourceInfo = null, $VizitDate = null, $VizitTime = null, $VIN = null, $Configuration = null,
								$ModelColor = null, $UserAction = null, $TOType = null, $ContactingTheme = null, $CustomCity = null, $Status = null, $ForShaduler = null)
	{
		$U = array();
		if($Model)
			$U[] = "`model_id`=".$Model;
		if($LastName)
			$U[] = "`last_name`=".$this->Esc($LastName);
		if($FirstName)
			$U[] = "`first_name`=".$this->Esc($FirstName);
		if($MiddleName)
			$U[] = "`middle_name`=".$this->Esc($MiddleName);
		if($Email)
			$U[] = "`email`=".$this->Esc($Email);
		if($Phone)
			$U[] = "`phone`=".$this->Esc($Phone);
		if($ContactMethod)
			$U[] = "`contact_method_id`=".$ContactMethod;
		if($City)
			$U[] = "`city_id`=".$City;
		if($this->IsValue($Gender))
			$U[] = "`gender`=".$Gender;
		if($Age)
			$U[] = "`age`=".$Age;
		if($this->IsValue($IsOwner))
			$U[] = "`is_owner`=".$IsOwner;
		if($AdvancedInfo)
			$U[] = "`other_info`=".$this->Esc($AdvancedInfo);
		if($TrafficSource)
			$U[] = "`traffic_source_id`=".$TrafficSource;
		if($CurrentCar)
			$U[] = "`current_model_id`=".$CurrentCar;
		if($CurrentCarManufacturer)
			$U[] = "`manufacturer_id`=".$CurrentCarManufacturer;
		if($CarYear)
			$U[] = "`model_years`=".$this->Esc($CarYear);
		if($Miliage !== null)
			$U[] = "`mileage`=".($Miliage === 0 ? 0 : ($Miliage ? $Miliage : "NULL"));
		if($RegNumber)
			$U[] = "`reg_number`=".$this->Esc($RegNumber);
		if($SourceInfo)
			$U[] = "`source_info_id`=".$SourceInfo;
		if($VizitDate)
			$U[] = "`visit_date`=".$this->Esc($VizitDate);
		if($VizitTime)
			$U[] = "`visit_time`=".$this->Esc($VizitTime);
		if($VIN)
			$U[] = "`vin`=".$this->Esc($VIN);
		if($Configuration)
			$U[] = "`configuration_id`=".$Configuration;
		if($TOType)
			$U[] = "`to_type_id`=".$TOType;
		if($ContactingTheme)
			$U[] = "`contactings_theme_id`=".$ContactingTheme;
		if($CustomCity)
			$U[] = "`custom_city`=".$this->Esc($CustomCity);
		if($this->IsValue($Status))
			$U[] = "`current_status_id`=".$Status;


		return $this->Exec("UPDATE `references`.`facilities_forms_members`
	SET ".implode(", ", $U)."
	WHERE `member_id`=".$Member.";");
	}

	public function CheckMember($Brand, $Facility, $FormType, $Dealer, $Model, $LastName, $FirstName, $MiddleName, $Email, $Phone, $ContactMethod, $Gender, $Age,
								$IsOwner, $AdvancedInfo, $TrafficSource, $CurrentCar = null, $CurrentCarManufacturer = null, $CarYear = null, $Miliage = null,
								$RegNumber = null, $City = null, $SourceInfo = null, $VizitDate = null, $VizitTime = null, $VIN = null, $Status = null,
								$Configuration = null, $ModelColor = null, $ActionCode = null, $ActionStatus = null, $UserAction = null, $PhotoCount = null,
								$PhotosNames = null, $TOType = null, $Referrer = null, $FromDealerSite = null, $FromMobile = null, $UserAgent = null,
								$ContactingTheme = null, $CustomCity = null, $OldDealer = null)
	{
		$Where = array();
		$Where[] = "`partners_division_id`".$this->CheckNull($Dealer, 1);
		$Where[] = "`facility_id`=".$Facility;
		$Where[] = "`brand_id`=".$Brand;
		$Where[] = "`form_type_id`=".$FormType;
		$Where[] = "`model_id`".$this->CheckNull($Model, 1);
		$Where[] = "`first_name`=".$this->Esc($FirstName);
		$Where[] = "`last_name`=".$this->Esc($LastName);
		$Where[] = "`middle_name`=".$this->Esc($MiddleName);
		$Where[] = "`configuration_id`".$this->CheckNull($Configuration, 1);
		$Where[] = "`model_color`=".$this->Esc($ModelColor);
		$Where[] = "`gender`".$this->CheckNull($Gender, 1);
		$Where[] = "`age`".$this->CheckNull($Age, 1);
		$Where[] = "`email`=".$this->Esc($Email);
		$Where[] = "`phone`=".$this->Esc($Phone);
		$Where[] = "`contact_method_id`".$this->CheckNull($ContactMethod, 1);
		$Where[] = "`city_id`".$this->CheckNull($City, 1);
		$Where[] = "`manufacturer_id`".$this->CheckNull($CurrentCarManufacturer, 1);
		$Where[] = "`current_model_id`".$this->CheckNull($CurrentCar, 1);
		$Where[] = "`model_years`=".$this->Esc($CarYear);
		$Where[] = "`mileage`".$this->CheckNull($Miliage, 1);
		$Where[] = "`reg_number`=".$this->Esc($RegNumber);
		$Where[] = "`vin`=".$this->Esc($VIN);
		$Where[] = "`is_owner`".$this->CheckNull($IsOwner, 1);
		$Where[] = "`other_info`=".$this->Esc($AdvancedInfo);
		if($VizitDate)
			$Where[] = "`visit_date`=".$this->Esc($VizitDate);
		if($VizitTime)
			$Where[] = "`visit_time`=".$this->Esc($VizitTime);
		$Where[] = "`traffic_source_id`".$this->CheckNull($TrafficSource, 1);
		$Where[] = "`action_status`".$this->CheckNull($ActionStatus, 1);
		$Where[] = "`action_code`=".$this->Esc($ActionCode);
		$Where[] = "`source_info_id`".$this->CheckNull($SourceInfo, 1);
		$Where[] = "`from_dealers_site`=".($FromDealerSite ? 1 : 0);
		$Where[] = "`from_mobile`=".($FromMobile ? 1 : 0);
		$Where[] = "`to_type_id`".$this->CheckNull($TOType, 1);
		$Where[] = "`user_action_name`=".$this->Esc($UserAction);
		$Where[] = "`photo_count`".$this->CheckNull($PhotoCount, 1);
		$Where[] = "`photos_names`=".$this->Esc($PhotosNames);
		$Where[] = "`referrer`=".$this->Esc($Referrer);
		$Where[] = "`user_agent`=".$this->Esc($UserAgent);
		$Where[] = "`contactings_theme_id`".$this->CheckNull($ContactingTheme, 1);
		$Where[] = "`custom_city`=".$this->Esc($CustomCity);
		$Where[] = "`current_status_id`=".$Status;

		$Where[] = "`register` > DATE_ADD(NOW(), INTERVAL -1 DAY)";

		$this->Dump(__METHOD__, "************", "SELECT
	COUNT(*) AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`facilities_forms_members`".$this->PrepareWhere($Where));

		return $this->Count("SELECT
	COUNT(*) AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`facilities_forms_members`".$this->PrepareWhere($Where));
	}

    public function GetMembers($Brand, $Facility, $FormType = null, $Dealer = null, $Model = null, $RegisterStart = null, $RegisterEnd = null, $VisitStart = null, $VisitEnd = null, $Status = null)
    {
        $Query = $this->Queries["members"][$Facility];
        $Where = array();
        $Where[] = "a.`facility_id`=".$Facility;
        $Where[] = "a.`partners_division_id`".$this->PrepareValue($Dealer);
        $Where[] = "a.`form_type_id`=".$FormType;
		$Where[] = "a.`brand_id`=".$Brand;
		$Where[] = "a.`current_status_id`".($Status !== null && !is_empty($Status) ? $this->PrepareValue($Status) : "<>5");

		if($Model)
			$Where[] = "a.`model_id`=".$Model;
		if($RegisterStart)
			$Where[] = "DATE(a.`register`) >=".$this->Esc($RegisterStart);
		else $Where[] = "DATE(a.`register`) >= DATE_ADD(DATE(NOW()), INTERVAL -3 MONTH)";
		if($RegisterEnd)
			$Where[] = "DATE(a.`register`) <=".$this->Esc($RegisterEnd);
		if($VisitStart)
			$Where[] = "DATE(a.`visit_date`) >=".$this->Esc($VisitStart);
		if($VisitEnd)
			$Where[] = "DATE(a.`visit_date`) <=".$this->Esc($VisitEnd);

        return $this->Get($Query.$this->PrepareWhere($Where));
    }

    public function GetMembersForReport($Brand, $Facility, $FormType = null, $Dealer = null, $Model = null, $RegisterStart = null, $RegisterEnd = null, $VisitStart = null, $VisitEnd = null, $Status = null)
    {
        $Where = array();
        $Where[] = "a.`facility_id`=".$Facility;
        $Where[] = "a.`partners_division_id`".$this->PrepareValue($Dealer);
        $Where[] = "a.`form_type_id`=".$FormType;
		$Where[] = "a.`brand_id`=".$Brand;
		$Where[] = "a.`current_status_id`".($Status !== null && !is_empty($Status) ? $this->PrepareValue($Status) : "<>5");

		if($Model)
			$Where[] = "a.`model_id`=".$Model;
		if($RegisterStart)
			$Where[] = "DATE(a.`register`) >=".$this->Esc($RegisterStart);
		else $Where[] = "DATE(a.`register`) >= DATE_ADD(DATE(NOW()), INTERVAL -3 MONTH)";
		if($RegisterEnd)
			$Where[] = "DATE(a.`register`) <=".$this->Esc($RegisterEnd);
		if($VisitStart)
			$Where[] = "DATE(a.`visit_date`) >=".$this->Esc($VisitStart);
		if($VisitEnd)
			$Where[] = "DATE(a.`visit_date`) <=".$this->Esc($VisitEnd);

		$Query = $this->Queries["membersForReport"][$Facility].$this->PrepareWhere($Where);
		
$this->Dump(__METHOD__.": ".__LINE__, $Query);
		
        return $this->Get($Query);
    }

    public function GetMembersForEmail($Facility, $Member)
    {
        return $this->Get($this->Queries["forEmail"][$Facility]."=".$Member.";", true);
    }




	public function GetDealerEmails($Brand, $Facility, $Dealer)
	{
		return $this->Get("SELECT
	`email` AS email
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`".$this->PrepareValue($Dealer)."
	AND `brand_id`=".$Brand."
	AND `facility_id`=".$Facility.";");
	}

	public function GetAllEmails($Brand, $Facility)
	{
		return $this->Get("SELECT
	`email` AS email
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`
WHERE `facility_id`=".$Facility."
	AND `brand_id`=".$Brand.";");
	}

    public function GetDealerAndUniversalEmail($Facility, $FormType, $Dealer, $Type)
    {
        $Where = array();
        $Where2 = array();
        $A = "`form_type_id`=".($FormType ? $FormType : 1);
        $Where[] = $A;
        $Where2[] = $A;
        $A = "`facility_id`=".$Facility;
        $Where[] = $A;
        $Where2[] = $A;
        $A = "`type` IN (".($Type ? 2 : 1).", 3)";
        $Where[] = $A;
        $Where2[] = $A;
		$Where2[] = "`category`<>1";
        if($Dealer)
            $Where[] = "`partners_division_id`".$this->PrepareValue($Dealer);

        $Query = "SELECT
	`email` AS email,
	3 AS sendAs
FROM `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_facilities_emails`".$this->PrepareWhere($Where)."
UNION
SELECT
	`email`,
	`send_as`
FROM `".DBS_UNIVERSAL_REFERENCES."`.`facilities_forms_universal_emails`".$this->PrepareWhere($Where2).";";
        return $this->Get($Query);
    }

    public function GetUniversalEmail($Facility, $FormType, $Type, $Category)
    {
        $Query = "SELECT
	`email`,
	`send_as` AS sendAs
FROM `".DBS_UNIVERSAL_REFERENCES."`.`facilities_forms_universal_emails`
WHERE `facility_id`=".$Facility."
	AND `form_type_id`=".$FormType."
    AND `type` IN (".($Type ? 2 : 1).", 3)
	AND `category`".$this->PrepareValue($Category).";";
        return $this->Get($Query);
    }



    public function GetEmails($Brand, $Facility, $Dealer = null)
    {
        $Query = "SELECT
	a.`partners_division_id` AS partnerId,
	a.`facility_id` AS actionId,
	a.`email` AS email,
	b.`name` AS partnerName
FROM `references`.`partners_divisions_facilities_emails` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`";
        $Where = array();
        $Where[] = "a.`facility_id`=".$Facility;
		$Where[] = "b.`brand_id`=".$Brand;
        if($Dealer)
            $Where[] = "a.`partners_division_id`".$this->PrepareValue($Dealer);

        return $this->Get($Query.$this->PrepareWhere($Where));
    }

    public function DeleteEmail($Facility, $Dealer, $Email)
    {
        return $this->Exec("DELETE FROM `references`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`=".$Dealer."
	AND `facility_id`=".$Facility."
	AND `email`=".$this->Esc($Email).";");
    }

    public function ClearEmail($Facility, $Dealer)
    {
        return $this->Exec("DELETE FROM `references`.`partners_divisions_facilities_emails`
WHERE `partners_division_id`=".$Dealer."
	AND `facility_id`=".$Facility.";");
    }

    public function AddEmail($Facility, $Dealer, $Email)
    {
        if($this->Count("SELECT
	COUNT(*) AS Cnt
FROM `references`.`partners_divisions_facilities_emails`
WHERE `facility_id`=".$Facility."
    AND `partners_division_id`".$this->PrepareValue($Dealer)."
    AND `email`=".$this->Esc($Email).";"))
            throw new dmtException("Already exists", 10);
            $this->Exec("INSERT INTO `references`.`partners_divisions_facilities_emails`
    (`partners_division_id`,
    `facility_id`,
    `email`)
VALUES
    (".$Dealer.",
    ".$Facility.",
    ".$this->Esc($Email).")");

        return $this->Get("SELECT
	a.`partners_division_id` AS partnerId,
	a.`facility_id` AS actionId,
	a.`email` AS email,
	b.`name` AS partnerName
FROM `references`.`partners_divisions_facilities_emails` AS a
LEFT JOIN `references`.`partners_divisions` AS b ON b.`partners_division_id`=a.`partners_division_id`
WHERE a.`facility_id`=".$Facility."
    AND a.`partners_division_id`".$this->PrepareValue($Dealer)."
    AND a.`email`=".$this->Esc($Email).";", true);
    }

    public function GetTemplates($Brand, $Facility, $FormType)
    {
        return $this->Get("SELECT
	`facility_id` AS facilityId,
	`form_type_id` AS formTypeId,
	`file_name` AS fileName,
    `dealer_send` AS dealerSend,
	`dealer_mail_template` AS dealerMailTemplate,
    `dealer_send_as_html` AS dealerAsHTML,
    `dealer_charset` AS dealerCharset,
	`dealer_mail_subject` AS dealerMailSubject,
	`dealer_mail_form` AS dealerMailForm,
    `dealer_from_name` AS dealerMailFormName,
    `user_send` AS userSend,
	`user_mail_template` AS userMailTemplate,
    `user_send_as_html` AS userAsHTML,
    `user_charset` AS userCharset,
	`user_mail_subject` AS userMailSubject,
	`user_mail_form` AS userMailForm,
    `user_from_name` AS userMailFormName,
    `tmr_send` AS tmrSend,
	`tmr_mail_template` AS tmrMailTemplate,
    `tmr_send_as_html` AS tmrAsHTML,
    `tmr_charset` AS tmrCharset,
	`tmr_mail_subject` AS tmrMailSubject,
	`tmr_mail_form` AS tmrMailForm,
    `tmr_from_name` AS tmrMailFormName,
	`tmr_attachment_mail_template` AS tmrAttachmentMailTemplate,
    `tmr_attachment_mail_name` AS tmrAttachmentMailTemplateName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`facilities_forms`
WHERE `facility_id`=".$Facility."
	AND `brand_id`=".$Brand."
	AND `form_type_id`=".$FormType.";", true);
    }



	public function GetContactingThemes($Brand, $Facility)
	{
		return $this->Get("SELECT
	`contactings_theme_id` AS contactingThemeId,
    `name` AS contactingThemeName,
    `description` AS contactingThemeDescription
FROM `references`.`facilities_forms_contactings_themes`
WHERE `brand_id`=".$Brand."
	AND `facilityId`=".$Facility."
ORDER BY `name`;");
	}


	public function GetCitiesForDealers($Action, $Model)
	{
		return $this->Get("SELECT DISTINCT
	c.`city_id` AS cityId,
	c.`name` AS cityName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS ot
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS pd ON pd.`partners_division_id`=ot.`partners_division_id`
LEFT JOIN `references`.`cities` AS c ON c.`city_id`=pd.`city_id`
WHERE pd.`status`=1
	AND ot.`action_id`=".$Action.($Model ? "
	AND ot.`model_id`=".$Model : "")."
	AND ot.`status`=1
ORDER BY cityName");
	}

	public function GetDealersForCity($Action, $Model, $City)
	{
		return $this->Get("SELECT DISTINCT
	pd.`partners_division_id` AS partnerId,
	pd.`name` AS partnerName
FROM `".DBS_REFERENCES."`.`custom_actions_partners_models` AS ot
LEFT JOIN `".DBS_REFERENCES."`.`partners_divisions` AS pd ON pd.`partners_division_id`=ot.`partners_division_id`
WHERE pd.`city_id`=".$City."
	AND pd.`status`=1
	AND ot.`action_id`=".$Action.($Model ? "
	AND ot.`model_id`=".$Model : "")."
	AND ot.`status`=1");
	}
}