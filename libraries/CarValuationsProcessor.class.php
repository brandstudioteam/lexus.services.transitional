<?php
define("CAR_VALUATIONS_WSDL_PATH",					PATH_TEMPLATES."RequestWWWCreateService.xml");
define("SERVICES_TRADEIN_ID",                       21);

define("FINANCE_CALCULATOR_URL",                    SITE_CURRENT == SITE_LEXUS ? "http://calc-lexus.prlist.ru/" : "http://www.toyota.ru/toyota-fs/calculator.tmex");

/*
Веб-сервис доступен по следующему адресу:
https://webrequest.toyota-motor.ru/MicrosoftDynamicsAXAif60/WebRequestExternalSite/xppservice.svc
https://webrequest.toyota-motor.ru/MicrosoftDynamicsAXAif60/WebRequestExternalSite/xppservice.svc

Для доступа к нему необходима авторизация.
Логин: tmr\AX12WebServicesExt
Пароль: gV1S6KNg19JH
 */

class CarValuationsProcessor extends Processor
{
	/**
	 *
	 * @var CarValuationsProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var CarValuationsData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return CarValuationsProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new CarValuationsData();
    }

	public function GetMembers($Dealer = null)
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TIEVALUATION_MEMBERS_MANAGEMENT, PERMISSIONS_TIEVALUATION_MEMBERS_READY), $Dealer);
        return $this->CData->GetMembers(SITE_CURRENT == SITE_LEXUS ? 2 : 1, $Scope);
    }

    public function GetMembersCSV($Dealer, $Model = null, $RegisterStart = null, $RegisterEnd = null, $TradeInModel = null)
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TO_USER_ACTIONS_MANAGEMENT, $Dealer);
        $R = $this->CData->GetMembersForReport(WS::Init()->BrandId, $Scope, $Model, $RegisterStart, $RegisterEnd, $TradeInModel);
        array_unshift(  $R,
                        array(
							"Дилерский центр",
                            "Имя",
                            "Телефон",
                            "Электронная почта",
                            "Дата отправки заявки",
                            "Модель автомобиля",
                            "Год выпуска",
                            "Пробег",
                            //"Описание",
                            "Количество приложенных фотографий",
                            "Модель Trafr-in",
                            "Статус заявки",
                            "Дата изменения статуса заявки",
                            "Цена минимальная",
							"Цена максимальная",
                            "Представитель дилера, изменивший статус заявки",
                            "Заявка получена с сайта дилера",
							"Источник трафика"
                        ));
        return $R;
    }
	
	public function GetAllMembersCSV()
    {
        $Scope = PermissionsProcessor::Init()->GetScopeAccess(PERMISSIONS_TIEVALUATION_ALL_MEMBERS_REPORT);
        $R = $this->CData->GetMembersForReport(WS::Init()->BrandId, $Scope, null, null, null, null, true);
        array_unshift(  $R,
                        array(
							"Дилерский центр",
                            "Имя",
                            "Телефон",
                            "Электронная почта",
                            "Дата отправки заявки",
                            "Модель автомобиля",
                            "Год выпуска",
                            "Пробег",
                            //"Описание",
                            "Количество приложенных фотографий",
                            "Модель Trafr-in",
                            "Статус заявки",
                            "Дата изменения статуса заявки",
                            "Цена минимальная",
							"Цена максимальная",
                            "Представитель дилера, изменивший статус заявки",
                            "Заявка получена с сайта дилера"
                        ));
        return $R;
    }

    public function AddCost($Member, $Cost, $CostMax)
    {
        $Data = $this->CData->GetMember($Member);
        PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TIEVALUATION_MEMBERS_MANAGEMENT, PERMISSIONS_TIEVALUATION_MEMBERS_STATUS_CHANGE), $Data["partnersDivisionId"]);
        $this->CData->AddCost($Member, $Cost, $CostMax, $_SERVER["REMOTE_ADDR"]);
        $Data = $this->CData->GetMember($Member);
        $this->AddEvent(SERVICES_TRADEIN_ID, 18, 2, $Data, $Data["partnersDivisionId"], User::Init()->GetAccount(), WS::Init()->ClientUID);
        return $Data;
    }

    public function SendMailWitchCost($Member)
    {
		$Data = $this->CData->GetMember($Member);
		if(!$Data["cost"])
			return;
        PermissionsProcessor::Init()->GetScopeAccess(array(PERMISSIONS_TIEVALUATION_MEMBERS_MANAGEMENT, PERMISSIONS_TIEVALUATION_MEMBERS_STATUS_CHANGE), $Data["partnersDivisionId"]);
        if($Data["calculator"])
        {
            $CalcURL = base64_decode($Data["calculator"]);
            $CalcURL = base64_encode($CalcURL."&ti=".$Data["cost"]);
        }
		else $CalcURL = null;

        if($Data["email"])
        {
            $M = new MailDriver(false, true);
            $M->AddAddress($Data["email"]);

            if(SITE_CURRENT==SITE_LEXUS)
            {
                $M->SetFrom('info@trade.lexus.ru', 'LEXUS Trade In');
                $M->Subject = "LEXUS Trade In: Предложение цены за автомобиль";
            }
            else
            {
                $M->SetFrom('info@trade.toyota.ru', 'Toyota Trade In');
                $M->Subject = "Toyota Trade In: Предложение цены за автомобиль";
            }
            $D = $this->CData->GetDealerData($Data["partnersDivisionId"]);
            $Message = '<h3>Уважаемый(ая), '.$Data["name"].'!</h3>
<p>Мы произвели предварительную оценку Вашего автомобиля '.$Data["carName"].' '.$Data["manufacturerYear"].' года выпуска</p>
<p>Его стоимость составит от '.$Data["cost"].' до '.$Data["costMax"].' рублей.</p>';
            if($Data["calculator"])
            {
                $CalcURL = FINANCE_CALCULATOR_URL."?c=".$CalcURL;
                $Message .= '<p>Перейдите по ссылке: <a href="'.$CalcURL.'"></a>'.$CalcURL.', продолжите свой кредитный расчет и получите специальное кредитной предложение на покупку нового автомобиля от ЗАО «Тойота Банк»!</p>
<p>Для получения точной стоимости автомобиля обратитесь в Ваш дилерский центр в часы его работы.</p><br />';
            }
            else
            {
                $Message .= '<div>Воспользуйтесь специальным кредитным предложением от "Тойота Банк" для покупки нового автомобиля Toyota.</div>
<div><a href="'.FINANCE_CALCULATOR_URL.'"></a>'.FINANCE_CALCULATOR_URL.'</div>';
            }
			
			$Message .= '<br /><p>С Уважением,<br />'
.(User::Init()->GetPosition() ? "<i>".User::Init()->GetPosition()."</i> " : "").User::Init()->GetName()."!<br />"
.(User::Init()->GetPhone() ? "Телефон: <b>".User::Init()->GetPhone()."</b>".(User::Init()->GetAdvancedPhone() ? " добавочный <b>".User::Init()->GetAdvancedPhone()."</b>" : "")."<br />" : "")
.(User::Init()->GetEmail() ? "E-mail: <b>".User::Init()->GetEmail()."</b><br />" : "")
.'<br />'.$D["partnerName"].'<br /><br />
Сайт: <b>'.$D["partnerSite"].'</b><br />
Телефон: <b>'.$D["partnerPhone"].'</b></p>';
			
            $M->MsgHTML($Message);
            $M->Send();

			$this->CData->ChangeStatus($Member, 3, $_SERVER["REMOTE_ADDR"]);
			$Data = $this->CData->GetMember($Member);
			$this->AddEvent(SERVICES_TRADEIN_ID, 18, 2, $Data, $Data["partnersDivisionId"], User::Init()->GetAccount(), WS::Init()->ClientUID);
        }
        return $Data;
    }
}