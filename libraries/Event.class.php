<?php
class Event extends Controller
{
	/**
	 *
	 * @var Event
	 */
	protected static $Inst;

	/**
	 *
	 * Инициализирует класс
	 * @return Event
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

	protected function Sets()
	{
		$this->Tpls		= array(
			"TplVars"		=> array(
                "eventId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				//Тип
				"eventType"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				//Статус
				"eventStatus"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				),
				"eventCategory"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"eventParent"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
				"eventName"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				"eventDescription"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_TEXT),
				),
				//Логин
				"users"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
                "timestamp"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED),
				),
			)
		);

		$this->Modes = array(
			//Создать событие
			"a"	=> array(
				"exec"			=> array("EventProcessor", "CreateEvent"),
				"Results"		=> array(
					"exceptions"		=>  array(1),
					"succes"			=> array(7, "RegisterSucTxt")
				),
				"TplVars"			=> array("eventType" => 1, "eventCategory" => 1, "eventParent" => 0, "eventName" => 1, "eventDescription" => 1, "users" => 0)
			),
			//Получить события
			"b"	=> array(
				"exec"				=> array("EventProcessor", "GetEvents"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
                "TplVars"			=> array("timestamp" => 0)
			),
			//Пометить событие как доставленное
			"c"	=> array(
				"exec"				=> array("EventProcessor", "ClosedEvent"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("eventId" => 2)
			),
			//Пометить событие как отправленное
			"d"	=> array(
				"exec"				=> array("EventProcessor", "DeleteRole"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
			//Пометить событие как отработанное
			"e"	=> array(
				"exec"				=> array("EventProcessor", "GetRolesAccess"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
			//Пометить событие как закрытое
			"f"	=> array(
				"exec"				=> array("EventProcessor", "SetRoleAccess"),
				"Results"			=> array(
					"exceptions"		=>  array(1,
						array(
							85001			=> "InvalidLoginOrPsw",
						)
					)
				),
				"TplVars"			=> array("roleId" => 1)
			),
		);
	}
}