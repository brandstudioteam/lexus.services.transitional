<?php
class AccountsData extends Data
{
	public function __construct()
	{
		parent::__construct();
	}

	public function CreateAccount($Type, $Email, $FName, $MName, $LName, $Login, $Partner, $Role, $Children, $ChildrenRoles)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_CREATE);

		$this->Begin();
		try {
			$Brand = WS::Init()->BrandId;
            //TODO: Удалить `partner_id` из таблицы пользователей после реализации объектно-иерархических прав
			$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users`
	(`users_type_id`,
	`brand_id`,
    `partner_id`,
	`partners_division_id`,
	`first_name`,
	`middle_name`,
	`last_name`,
	`email`,
	`create`)
VALUES
	(".$Type.",
	".$Brand.",
	".$this->CheckNull($Partner).",
    ".$this->CheckNull($Partner).",
	".$this->Esc($FName, true, false, true).",
	".$this->Esc($MName).",
	".$this->Esc($LName).",
	".$this->Esc($Email).",
	NOW());");

			$Account = $this->DB->GetLastID();

			$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
	(`user_id`,
	`brand_id`,
	`login`,
	`password`)
VALUES
	(".$Account.",
	".$Brand.",
	".$this->Esc($Login, true, false, true).",
	".$this->Esc('not set password').");");

            if($Role)
                $this->SaveChildrenRoles($Account, array($Partner), $Role, $Partner);

            if($Children && $ChildrenRoles)
                $this->SaveChildrenRoles($Account, $Children, $ChildrenRoles, $Partner);

			$this->Commit();
		}
		catch (dmtException $e)
		{
			$this->Rollback();
			throw new dmtException($e->getMessage(), $e->getCode(), true);
		}
		return $Account;
	}

	public function Save($Account, $Type, $Email, $FName, $MName, $LName, $Partner, $Login, $Role, $Children, $ChildrenRoles)
	{
		$A = array();
		$B = array();
		if($Type)
		{
			$A[] = "`users_type_id`=".$Type;
		}
		if($Partner)
		{
            //TODO: Проверить иерахию и при необходимости скорректировать.
			$A[] = "`partner_id`=".$Partner;
            $A[] = "`partners_division_id`=".$Partner;
		}
		if($FName)
		{
			$A[] = "`first_name`=".$this->Esc($FName);
		}
		if($MName)
		{
			$A[] = "`middle_name`=".$this->Esc($MName);
		}
		if($LName)
		{
			$A[] = "`last_name`=".$this->Esc($LName);
		}
		if($Email)
		{
			$A[] = "`email`=".$this->Esc($Email);
		}
		$this->Begin();
		try {
			if(sizeof($A))
			{
				if(sizeof($A))
					$this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`users`
SET ".implode(", ", $A)."
WHERE `user_id`=".$Account.";");
			}
			if($Login)
			{
				$this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
SET `login`=".$this->Esc($Login)."
WHERE `user_id`=".$Account.";");
			}
			if($Role || $Children && $ChildrenRoles)
				$this->DeleteAllChildrenRoles($Account);
			if($Role)
				$this->SaveChildrenRoles($Account, array($Partner), $Role, $Partner);
			if($Children && $ChildrenRoles)
				$this->SaveChildrenRoles($Account, $Children, $ChildrenRoles, $Partner);

			$this->Commit();
		}
		catch (dmtException $e)
		{
			$this->Rollback();
			throw new dmtException($e->getMessage(), $e->getCode(), true);
		}
	}

	public function SaveFIO($FName = null, $MName = null, $LName = null, $Position = null, $Phone = null, $AdvPhone = null)
	{
		$A = array();
		if($FName)
		{
			$A[] = "`first_name`=".$this->Esc($FName);
		}
		if($MName !== null)
		{
			$A[] = "`middle_name`=".$this->Esc($MName);
		}
		if($LName)
		{
			$A[] = "`last_name`=".$this->Esc($LName);
		}
		if($Position !== null)
		{
			$A[] = "`position`=".$this->Esc($Position);
		}
		if($Phone !== null)
		{
			$A[] = "`phone`=".$this->Esc($Phone);
		}
		if($AdvPhone !== null)
		{
			$A[] = "`advanced_phone`=".$this->Esc($AdvPhone);
		}

		if(sizeof($A))
		{
			$this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`users`
SET ".implode(", ", $A)."
WHERE `user_id`=".DB_VARS_ACCOUNT_CURRENT.";");
		}
	}

    public function DeleteAccount($Account)
    {
        try
        {
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
WHERE `user_id`=".$Account.";");
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_event_subscriptions`
WHERE `user_id`=".$Account.";");
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_events_users`
WHERE `user_id`=".$Account.";");
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_sessions`
WHERE `user_id`=".$Account.";");
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_roles`
WHERE `user_id`=".$Account.";");

            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions`
WHERE `user_id`=".$Account.";");
            $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
WHERE `user_id`=".$Account.";");

             $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `user_id`=".$Account.";");
            $this->Commit();
        }
        catch (dmtException $e)
        {
            $this->Rollback();
            throw new dmtException($e->getMessage(), $e->getCode(), true);
        }
    }

	public function SetRole($Account, $AccountType)
	{
		$this->Exec("");
	}



	public function AdminChangePassword($Account, $Password)
	{
		$this->Exec("UPDATE `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
SET `password`=".$this->Esc($Password)."
WHERE `user_id`=".$Account.";");
	}


	public function ChangePassword($Password, $NewPassword)
	{

	}


	/**
	 * Регистрация пользователя
	 *
	 * @param string $Login - логин
	 * @param string $Email - адрес электронной почты
	 * @param string $Password - пароль в md5
	 * @param string $Gender - идентификатор пола
	 * @param integer $Lang - идентификатор языка
	 */
	public function Register($Type, $Name, $Login, $Passw, $Partner = null, $Email = null)
	{
		$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users`
	(`type`,
	`partner_id`,
	`name`,
	`login`,
	`password`,
	`create`,
	`email`)
VALUES
	(".$Type.",
	".$Partner.",
	".$this->Esc($Name, true, false, true).",
	".$this->Esc($Login, true, false, true).",
	".$this->Esc($Passw, true, false, true).",
	".$this->Esc($Email).");");

		return $this->DB->GetLastID();
	}

	public function GetUsersList($Type = null)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_VIEW);
		return $this->Get("SELECT
	`user_id` AS userId,
	`users_type_id` AS userTypeId,
	`partner_id` AS partnerId,
	`partners_division_id` AS partnerDivisionId,
	`first_name` AS userFirstName,
	`middle_name` AS userMiddleName,
	`last_name` AS userLastName,
	`email` AS userEmail,
	`create` AS userCreateDate,
	`phone` AS userPhone,
	`advanced_phone` AS userAdvPhone,
	`position` AS userPosition,
	CONCAT_WS('', `first_name`, ' ', `middle_name`, ' ', `last_name`) AS userName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `users_type_id`<>1".($Type ? "
	AND `users_type_id`=".$Type : "")."
ORDER BY `first_name`");
	}

	public function GetUsersListF($Brand, $Partners, $Type = null)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_VIEW);
		$Query = "SELECT
	u.`user_id` AS userId,
	u.`users_type_id` AS userTypeId,
    u.`partner_id` AS partnerId,
    u.`partners_division_id` AS partnerDivisionId,
	u.`first_name` AS userFirstName,
	u.`middle_name` AS userMiddleName,
	u.`last_name` AS userLastName,
	u.`email` AS userEmail,
	u.`create` AS userCreateDate,
	u.`phone` AS userPhone,
	u.`advanced_phone` AS userAdvPhone,
	u.`position` AS userPosition,
	CONCAT_WS('', u.`first_name`, ' ', u.`middle_name`, ' ', u.`last_name`) AS userName,
	ut.`name` AS userTypeName,
	p.`name` AS partnerDivisionName,
	CONCAT_WS('', p.`name`, IF(p.`name` IS NULL, '', CONCAT_WS('', ' (', p.`type_name`, ')'))) AS userPartnerFullName,
    ua.`login` AS userLogin
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS u
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_types` AS ut ON ut.`users_type_id`=u.`users_type_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_f` AS p ON p.`partners_division_id`=u.`partners_division_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_auth` AS ua ON ua.`user_id`=u.`user_id`
WHERE u.`brand_id`=".$Brand."
	AND u.`partners_division_id`".$this->PrepareValue($Partners)."
	AND u.`users_type_id`<>1".($Type ? "
	AND u.`users_type_id`=".$Type : "")."
ORDER BY u.`first_name`";
		$R = $this->Get($Query);
        return $R;
	}


	public function GetUsersListForScope($Type = null)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_VIEW);
		$Query = "SELECT
	u.`user_id` AS userId,
	u.`users_type_id` AS userTypeId,
    u.`partner_id` AS partnerId,
    u.`partners_division_id` AS partnerDivisionId,
	u.`first_name` AS userFirstName,
	u.`middle_name` AS userMiddleName,
	u.`last_name` AS userLastName,
	u.`email` AS userEmail,
	u.`phone` AS userPhone,
	u.`advanced_phone` AS userAdvPhone,
	u.`position` AS userPosition,
	u.`create` AS userCreateDate,
	CONCAT_WS('', u.`first_name`, ' ', u.`middle_name`, ' ', u.`last_name`) AS userName,
	ut.`name` AS userTypeName,
	p.`name` AS partnerDivisionName,
	CONCAT_WS('', p.`name`, IF(p.`name` IS NULL, '', CONCAT_WS('', ' (', p.`type_name`, ')'))) AS userPartnerFullName,
    ua.`login` AS userLogin
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS u
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_types` AS ut ON ut.`users_type_id`=u.`users_type_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_f` AS p ON p.`partners_division_id`=u.`partners_division_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_auth` AS ua ON ua.`user_id`=u.`user_id`
WHERE u.`users_type_id`<>1".($Type ? "
	AND u.`users_type_id`=".$Type : "")."
ORDER BY u.`first_name`";
		$R = $this->Get($Query);
        return $R;
	}


	public function GetUserData($User)
	{
        //PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_VIEW);
		$Query = "SELECT
	u.`user_id` AS userId,
	u.`users_type_id` AS userTypeId,
    u.`partner_id` AS partnerId,
	u.`partners_division_id` AS partnerDivisionId,
    u.`partners_division_id` AS Id,
	u.`first_name` AS userFirstName,
	u.`middle_name` AS userMiddleName,
	u.`last_name` AS userLastName,
	u.`email` AS userEmail,
	u.`create` AS userCreateDate,
	u.`phone` AS userPhone,
	u.`advanced_phone` AS userAdvPhone,
	u.`position` AS userPosition,
	CONCAT_WS('', u.`first_name`, ' ', u.`middle_name`, ' ', u.`last_name`) AS userName,
	ut.`name` AS userTypeName,
	p.`name` AS partnerDivisionName,
	CONCAT_WS('', p.`name`, IF(p.`name` IS NULL, '', CONCAT_WS('', ' (', p.`type_name`, ')'))) AS userPartnerFullName,
    ua.`login` AS userLogin
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS u
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_types` AS ut ON ut.`users_type_id`=u.`users_type_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_f` AS p ON p.`partners_division_id`=u.`partners_division_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_auth` AS ua ON ua.`user_id`=u.`user_id`
WHERE u.`user_id`=".$User.";";

		$R = $this->Get($Query, true);
		return $R;
	}

	public function GetUser($User)
	{
        //PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_VIEW);
		return $this->Get("SELECT
	u.`user_id` AS userId,
	u.`users_type_id` AS userTypeId,
	u.`partner_id` AS partnerId,
	u.`first_name` AS userFirstName,
	u.`middle_name` AS userMiddleName,
	u.`last_name` AS userLastName,
	u.`email` AS userEmail,
	u.`create` AS userCreateDate,
	u.`phone` AS userPhone,
	u.`advanced_phone` AS userAdvPhone,
	u.`position` AS userPosition,
	ua.`login` AS userLogin
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS u
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`users_auth` AS ua ON ua.`user_id`=u.`user_id`
WHERE u.`user_id`=".$User.";", true);
	}

	public function DeleteRolesForAccount($User)
	{
        //PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_ROLES_LINK);
		$this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_roles`
WHERE `user_id`=".$User.";");
	}

	/**
	 *
	 * Удаляет роль от пользователя
	 * @param integer $User - Идентификатор пользователя
	 * @param integer $Role - Идентификатор роли
	 */
	public function DeleteRole($User, $Role)
	{
        //PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_ROLES_LINK);
		$this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_roles`
WHERE `user_id`=".$User."
	AND `role_id`=".$Role.";");
	}


	public function GetRoles($Account, $Partners)
	{
		if($Partners)
			$Query = "SELECT DISTINCT
	a.`user_id` AS userId,
	a.`role_id` AS roleId
FROM `references`.`users_partners_divisions_roles` AS a
LEFT JOIN `references`.`users` AS b ON b.`user_id`=a.`user_id`
WHERE b.`partners_division_id`".$this->PrepareValue($Partners)."
	AND a.`partners_division_id`=b.`partners_division_id`;";
		elseif($Account)
			$Query = "SELECT DISTINCT
	ur.`user_id` AS userId,
	ur.`role_id` AS roleId,
	r.`roles_type_id` AS roleTypeId,
	r.`name` AS roleName,
	r.`descriptioin` AS roleDescription,
	rt.`name` AS roleTypeName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_roles` AS ur
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles` AS r ON r.`role_id`=ur.`role_id`
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`roles_types` AS rt ON rt.`roles_type_id`=r.`roles_type_id`
WHERE ur.`user_id`".$this->PrepareValue($Account).";";
		return $this->Get($Query);
	}

	public function RoleAdd($Account, $Role)
	{
		$A = array();
		foreach($Role as $v)
		{
			$A[] = "(".$Account.",
	".$v.")";
		}
		if(sizeof($A))
			$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_roles`
	(`user_id`,
	`role_id`)
VALUES ".implode(", ", $A).";");
	}

	public function RoleDelete($Account, $Role)
	{
		//PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_ROLES_LINK);

		$this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_roles`
WHERE `user_id`=".$Account."
	AND `role_id`=".$Role.";");
	}

	public function CheckLogin($User, $Login)
	{
		$R = $this->Count("SELECT
	`user_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
WHERE `login`=".$this->Esc($Login, true, false, true)."
	AND `brand_id`=".WS::Init()->BrandId.";");
		if($R && $R != $User)
			throw new dmtException("Login is exists");
	}

	public function CheckEmail($User, $Email)
	{
		$R = $this->Count("SELECT
	`user_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `email`=".$this->Esc($Email, true, false, true)."
	AND `brand_id`=".WS::Init()->BrandId.";");
		if($R && $R != $User)
			throw new dmtException("Email is exists");
	}

    /**
	 * Проверяет существование адреса электронной почты в базе данных
	 *
	 * @param string $Email - адрес электронной почты
	 * @return bool true, если адрес электронной почт существует, или false, если не существует или его не удалось определить
	 */
	public function IsEmailExists($Email)
	{
		return $this->Check("SELECT
	COUNT(*) AS Exs
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `email`=".$this->Esc($Email, true, false, true)."
	AND `brand_id`=".WS::Init()->BrandId." LIMIT 1;");
	}

	/**
	 * Проверяет существование логина (имени учетной записи) пользователя
	 *
	 * @param string $Login - логин (имя учетной записи) пользователя
	 * @return bool true, если логина (учетная запись) пользователя существует, или false, если не существует или ее не удалось определить
	 */
	public function IsLoginExists($Login)
	{
		return $this->Check("SELECT
	COUNT(*) AS Exs
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
WHERE `login`=".$this->Esc($Login, true, false, true)."
	AND `brand_id`=".WS::Init()->BrandId." LIMIT 1;");
	}

	public function GetPartnerForUser($Account)
	{
		return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `user_id`=".$Account.";");
	}


    public function GetEmailFromAccount($Account)
    {
        return $this->Count("SELECT
	`email` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `user_id`=".$Account.";");
    }

    public function Restore($Account, $Hash, $Code)
    {
        $this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`restored_password`
	(`user_id`,
	`hash`,
	`create_date`,
    `code`)
VALUES
	(".$Account.",
    ".$this->Esc($Hash).",
    NOW(),
    ".$this->Esc($Code).")
ON DUPLICATE KEY UPDATE
    `hash`=VALUES(`hash`),
    `code`=VALUES(`code`),
    `create_date`=VALUES(create_date);");
    }


    public function GetCurrentData()
    {
        return $this->Get("SELECT
	`first_name` AS userFirstName,
	`middle_name` AS userMiddleName,
	`last_name` AS userLastName,
	`users_type_id` AS userTypeId,
	`partner_id` AS partnerId,
	`email` AS userEmail,
	`phone` AS userPhone,
	`advanced_phone` AS userAdvPhone,
	`position` AS userPosition
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `user_id`=".DB_VARS_ACCOUNT_CURRENT.";", true);
    }


    public function SaveChildrenRoles($Account, $Partners, $Roles, $Parent)
    {
        if(!is_array($Partners) && !is_empty($Partners))
            throw new dmtException("Partners is error", 10);
		$A = array();
		if(!is_empty($Partners))
		{
			foreach($Partners as $v)
			{
				$this->CheckChild($Parent, $v);
				foreach($Roles as $r)
				{
					$A[] = "(".$Account.", ".$v.", ".$r.")";
				}
			}

			if(sizeof($A))
				$this->Exec("INSERT IGNORE INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
	(`user_id`,
	`partners_division_id`,
	`role_id`)
VALUES ".implode(", ", $A).";");
		}
    }

    public function DeleteAllChildrenRoles($Account)
    {
		$R = $this->Exec("DELETE FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_partners_divisions_roles`
WHERE `user_id`=".$Account.";");
    }


    protected function CheckChild($Parent, $Child)
    {
		if($Parent != $Child && !$this->Count("SELECT
		COUNT(`partners_division_id`) AS Cnt,
		`partners_division_id` AS pid
	FROM `references`.`partners_divisions_parent`
	WHERE `partners_division_id`=".$Child."
		AND `parent_partners_division_id`=".$Parent.";"))
			throw new dmtException("Partner is not child", 11);
/*
        $Query = "SELECT
	SUM(c.`cnt`) AS Cnt
FROM (
	SELECT
		COUNT(b.`partners_division_id`) AS cnt,
		a.`partners_division_id` AS pid
	FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS a
	LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`partners_divisions_parent` AS b ON b.`parent_partners_division_id`=a.`partners_division_id`
	WHERE a.`user_id`=".$Account."
		AND b.`partners_division_id`=".$Child."
	UNION
	SELECT
		IF(a.`partners_division_id`=".$Child.", 1, 0),
		a.`partners_division_id`
	FROM `".DBS_UNIVERSAL_REFERENCES."`.`users` AS a
	WHERE a.`user_id`=".$Account.") AS c";
        if(!$this->Count($Query))
            throw new dmtException("Partner is not child", 11);
 */
    }



    public function CheckEmailForUpdate($Brand, $Email, $Account)
    {
        $R = $this->Count("SELECT
	`user_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users`
WHERE `email`=".$this->Esc($Email)."
	AND `brand_id`=".$Brand.";");

        if($R && $R != $Account)
            throw new dmtException("EmailIsExists", 1);
        return $Email;
    }

    public function CheckLoginForUpdate($Brand, $Login, $Account)
    {
        $R = $this->Count("SELECT
	`user_id` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
WHERE `login`=".$this->Esc($Login)."
	AND `brand_id`=".$Brand.";");
        if($R && $R != $Account)
            throw new dmtException("LoginIsExists", 2);
        return $Login;
    }

	public function GetAllEmailsForFacilities($Facilities)
	{
		return $this->Get("SELECT
	`partners_division_id` AS partnersDivisionId,
    `email` AS email
FROM `toyota_references`.`partners_divisions_facilities_emails`
WHERE `facility_id`=".$Facilities.";");
	}


	public function GetNewDealer($Dealer)
	{
		 return $this->Count("SELECT
	`partners_division_id` AS Cnt
FROM `references`.`partners_divisions`
WHERE `old_id`=".$Dealer."
    AND `brand_id`=".(SITE_CURRENT == SITE_LEXUS ? 2 : 1).";");
	}

	public function CreateAccountFromEmail($Type, $Email, $Partner, $Role)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_USERS_CREATE);
		/*
		if($this->IsEmailExists($Email))
			throw new dmtException("Email is exists. ".$Email);
		 */
		$Account = $this->Count("SELECT
	`user_id` AS Cnt
FROM `references`.`users`
WHERE `email`=".$this->Esc($Email).";");
		$this->Begin();
		try {
			if(!$Account)
			{
				//TODO: Удалить `partner_id` из таблицы пользователей после реализации объектно-иерархических прав
				$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users`
	(`users_type_id`,
    `partner_id`,
	`partners_division_id`,
	`first_name`,
	`email`,
	`create`)
VALUES
	(".$Type.",
	".$this->CheckNull($Partner).",
    ".$this->CheckNull($Partner).",
	".$this->Esc($Email).",
	".$this->Esc($Email).",
	NOW());");

				$Account = $this->DB->GetLastID();

				$this->Exec("INSERT INTO `".DBS_UNIVERSAL_REFERENCES."`.`users_auth`
	(`user_id`,
	`login`,
	`password`)
VALUES
	(".$Account.",
	".$this->Esc($Email, true, false, true).",
	".$this->Esc('not set password').");");
			}

            if($Role)
            {
                //TODO: Удалить строку после реализации объектно-иерархических прав
				if(!is_array($Role))
					$Role = array($Role);
                $this->SaveChildrenRoles($Account, array($Partner), $Role, $Partner);
            }
			$this->Commit();
		}
		catch (dmtException $e)
		{
			$this->Rollback();
			throw new dmtException($e->getMessage(), $e->getCode(), true);
		}
		return $Account;
	}



	public function GetUsersChildrenParents($Scope)
	{
		return $this->Get("SELECT DISTINCT
	a.`user_id` AS userId,
	a.`partners_division_id` AS partnerDivisionId
FROM `references`.`users_partners_divisions_roles` AS a
LEFT JOIN `references`.`users` AS b ON b.`user_id`=a.`user_id`
WHERE b.`partners_division_id`".$this->PrepareValue($Scope).";");
	}


	public function GetUsersChildrenRoles($Scope)
	{
		return $this->Get("SELECT DISTINCT
	a.`user_id` AS userId,
	a.`role_id` AS roleId
FROM `references`.`users_partners_divisions_roles` AS a
LEFT JOIN `references`.`users` AS b ON b.`user_id`=a.`user_id`
WHERE b.`partners_division_id`".$this->PrepareValue($Scope)."
	AND a.`partners_division_id`<>b.`partners_division_id`;");
	}
}