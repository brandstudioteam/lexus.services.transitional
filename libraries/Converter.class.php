<?php
class Converter extends BaseStatic
{
    static public function ArrayToCSV($Array, $Delimiter = null, $Quotes = null, $CharSet = null, $LineEnd = null)
    {
        $R = "";
        if(is_array($Array))
        {
            if($Delimiter == null)
                $Delimiter = ";";
            if($Quotes == null)
                $Quotes = '"';
            if($LineEnd == null)
                $LineEnd = '\r\n';
GetDebug()->Dump(__METHOD__.": ".__LINE__, $Array);
            foreach($Array as $k => $v)
            {
//GetDebug()->Dump(__METHOD__.": ".__LINE__, $k, $v);
                foreach($v as $r => $f)
                {
                    //$R .= $Quotes.$f.$Quotes.$Delimiter;//$Quotes.addslashes($f).$Quotes.$Delimiter;
					$f = html_entity_decode($f);
					
					$f = preg_replace(array('/(?:\r|\n)/', '/"/',  '/;/'), array(' ', "'", ","), $f);
                    $R .= $f.$Delimiter;//$Quotes.addslashes($f).$Quotes.$Delimiter;
                }
                //$R .= $LineEnd;
                $R .= "
";
            }
            if($CharSet && mb_strtolower($CharSet) != CHARSET)
            {
                $R = iconv($CharSet, CHARSET.'//IGNORE', $R);
            }
        }
        return $R;
    }
}