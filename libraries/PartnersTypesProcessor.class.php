<?php
class PartnersTypesProcessor extends Processor
{
	/**
	 *
	 * @var PartnersTypesProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * @var PartnersTypesData
	 */
	protected $CData;

	/**
	 *
	 * Инициализирует класс
	 *
	 * @return PartnersTypesProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new PartnersTypesData();
	}

	/**
	 *
	 * Создает тип партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function CreatePartnerType($Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_MANAGEMENT, User::Init()->GetAccount());
		$R = $this->CData->CreatePartnerType($Name, $Description);
		return $R;
	}

	/**
	 *
	 * Сохраняет изменения типа партнера
	 * @param integer $PartnerType - Идентификатор типа партнера
	 * @param string $Name - Наименование
	 * @param string $Description - Описание
	 */
	public function SavePartnerType($PartnerType, $Name, $Description)
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_MANAGEMENT, User::Init()->GetAccount());
		$this->CData->SavePartnerType($PartnerType, $Name, $Description);
	}

	/**
	 *
	 * Возвращает список типов партнеров
	 */
	public function GetPartnersTypeList()
	{
		PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_PARTNERS_TYPE_READY, User::Init()->GetAccount());

		return $this->CData->GetPartnersTypeList();
	}
}