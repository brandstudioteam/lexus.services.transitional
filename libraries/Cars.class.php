<?php
class Cars extends Controller
{
	/**
	 * 
	 * @var Cars
	 */
	protected static $Inst;
	
	/**
	 * 
	 * Инициализирует класс
	 * @return Cars
	 */
    public static function Init()
    {
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
    }

    protected function Sets()
    {
    	$this->Tpls		= array(
			"TplVars"		=> array(
				//Идентификатор
				"manufacturerId"		=> array(
					"filter"	=> array(FILTER_TYPE_TYPE, FILTER_TT_INTEGER_UNSIGNED)
				)
			)
		);
		$this->Modes = array(
			//Получить список производителей автомобилей
			"a"	=> array(
				"exec"		=> array("CarsProcessor", "GetManufacturersList")
			),
			//Получить список автомобилей для конкретного производителя
			"b"	=> array(
				"exec"			=> array("CarsProcessor", "GetCarsForManufacturer"),
				"TplVars"		=> array("manufacturerId" => 0)
			)
		);
    }
}