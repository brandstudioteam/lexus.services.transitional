<?php
class RolesProcessor extends Processor
{
	/**
	 *
	 * @var RolesProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var RolesData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return RolesProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new RolesData();
	}


	/**
	 *
	 * Получить список ролей для заданного типа
	 */
	public function GetRolesForType($Type)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_READ);
		return $this->CData->GetRolesForType($Type);
	}

	/**
	 *
	 * Получить список ролей
	 */
	public function GetRoles($Role = null)
	{
        PermissionsProcessor::Init()->CheckAccess(array(
                                                        PERMISSIONS_ROLES_READ,
                                                        PERMISSIONS_USERS_VIEW,
                                                        PERMISSIONS_USERS_CREATE,
                                                    ));
		return $this->CData->GetRoles($Role);
	}

	/**
	 *
	 * Создать роль
	 * @param integer $Type - Тип роли
	 * @param string $Name - Название роли
	 * @param string $Desc - Описание роли
	 */
	public function CreateRole($Type, $Name, $Desc, $Permissions)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_CREATE);
        $Role = $this->CData->CreateRole($Type, $Name, $Desc, $Permissions);
		return $this->CData->GetRoles($Role);
	}

	/**
	 *
	 * Изменить данные роли
	 * @param integer $Role - Идентификатор роли
	 * @param string $Name - Название роли
	 * @param string $Desc - Описание роли
	 */
	public function SaveRole($Role, $Name, $Desc, $Permissions)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_EDIT);
		$this->CData->SaveRole($Role, $Name, $Desc, $Permissions);
        return $this->CData->GetRoles($Role);
	}

	/**
	 *
	 * Удалить роль
	 * @param integer $Role - Идентификатор роли
	 */
	public function DeleteRole($Role)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_DELETE);
		$this->CData->DeleteRole($Role);
	}

	/**
	 *
	 * Получить права доступа к роли
	 * @param integer $Role - Идентификатор роли
	 */
	public function GetRolesAccess($Role)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_READ);
		return $this->CData->GetRolesAccess($Role);
	}

	/**
	 *
	 * Установить права доступа к роли
	 * @param integer $Role - Идентификатор роли
	 * @param array $Access - Массив прав доступа
	 */
	public function SetRoleAccess($Role, $Access)
	{
        PermissionsProcessor::Init()->CheckAccess(array(PERMISSIONS_ROLES_CREATE, PERMISSIONS_ROLES_EDIT));
		$this->CData->SetRoleAccess($Role, $Access);
	}


	/**
	 *
	 * Получить права доступа к сервисам, привязанные к роли
	 * @param integer $Role - Идентификатор роли
	 */
	public function GetRoleServicePermissions($Role)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_READ);
		$R = $this->CData->GetRoleServicePermissions($Role);
$this->Dump(__METHOD__, $R);
		return $R;
	}

	public function GetBasicAccessForServices($Role)
	{
		return $this->CData->GetBasicAccessForServices($Role);
	}

	/**
	 *
	 * Получить права доступа, привязанные к роли для заданного сервиса
	 * @param integer $Role - Идентификатор роли
	 * @param integer $Service - Идентификатор сервиса
	 */
	public function GetRolePermissionsForService($Role, $Service)
	{
		return $this->CData->GetRolePermissionsForService($Role, $Service);
	}

	/**
	 *
	 * Получить права доступа, привязанные к роли
	 * @param integer $Role - Идентификатор роли
	 * @param integer $Service - Идентификатор сервиса
	 */
	public function GetRolePermissions($Role)
	{
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ROLES_READ);
		return $this->CData->GetRolePermissions($Role);
	}

	/**
	 *
	 * Установить права доступа, привязанные к роли
	 * @param integer $Role - Идентификатор роли
	 * @param array $Permissions - Массив прав доступа
	 */
	public function SaveRolePermissions($Role, $Permissions)
	{
		$this->CData->SaveRolePermissions($Role, $Permissions);
	}

	public function DeleteRolePermissionsForService($Role, $Service)
	{
		$this->CData->DeleteRolePermissionsForService($Role, $Service);
	}

	/**
	 *
	 * Получить список пользователей, к которым привязана данная роль
	 * @param integer $Role - Идентификатор роли
	 */
	public function GetUsersForRole($Role)
	{
		return $this->CData->GetUsersForRole($Role);
	}

	public function GetRolesTypes()
	{
		return $this->CData->GetRolesTypes(User::Init()->GetType());
	}

    public function GetRolesTypesUsersTypes()
    {
        return $this->CData->GetRolesTypesUsersTypes(User::Init()->GetType());
    }
}