<?php
class Processor extends Base
{
	protected function __construct()
	{
		parent::__construct();
	}

	protected function Check()
	{
		if(!self::$Inst) throw new dmtException("Class is not inicialized");
	}

    protected function PrepareArray($Array, $FieldName = null, $NotNull = null)
    {
        if(is_array($Array))
        {
            $R = array();
			if($FieldName)
			{
				foreach($Array as $v)
				{
					if($NotNull)
					{
						if($v[$FieldName] !== null) $R[] = $v[$FieldName];
					}
					else $R[] = $v[$FieldName];
				}
			}
			else
			{
				foreach($Array as $v)
				{
					$tmp = reset($v);
					if($NotNull)
					{
						if($tmp !== null) $R[] = $tmp;
					}
					else $R[] = $tmp;
				}
			}
        }
        else $R = $Array;
        return $R;
    }

    protected function AddEvent($Service, $Type, $Category, $Detail, $Dealer, $ExclUser = null, $ExclCUID = null)
    {
        EventProcessor::Init()->CreateEvent($Service, $Type, $Category, $Detail, $Dealer, $ExclUser, $ExclCUID);
    }
}