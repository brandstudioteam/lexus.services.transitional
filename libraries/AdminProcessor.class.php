<?php
class AdminProcessor extends Processor
{
	/**
	 *
	 * @var AdminProcessor
	 */
	protected static $Inst = false;

	/**
	 *
	 * Класс данных
	 * @var AdminData
	 */
	private $CData;


	/**
	 *
	 * Инициализирует класс
	 *
	 * @return AdminProcessor
	 */
	public static function Init()
	{
		if(!self::$Inst) self::$Inst = new self();
		return self::$Inst;
	}

	protected function __construct()
	{
		parent::__construct();
		$this->CData = new AdminData();
	}

    public function GetServicesDataForAdmin()
    {
        PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ADMIN_ACCESS);
        return array(
            "services"      => $this->CData->GetServicesForAdmin(),
            "actions"       => $this->CData->GetServicesActionsForAdmin(WS::Init()->GetBrandId())
        );
    }

    public function GetServiceSets($Service)
    {
        //PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ADMIN_ACCESS);
        $File = PATH_SERVICES."/s".$Service.".txt";
        if(!file_exists($File))
            throw new dmtException("Sets for service ".$Service." is not found. File = '".$File."'", 2);
        WS::Init()->SendType = SEND_TYPE_TEXT;
        return file_get_contents($File);
    }

    public function GetServiceTemplate($Tempalete)
    {
        //PermissionsProcessor::Init()->CheckAccess(PERMISSIONS_ADMIN_ACCESS);
        $File = PATH_SERVICES."/".$Tempalete;
        if(!file_exists($File))
            throw new dmtException("Template ".$Tempalete." is not found", 2);
        WS::Init()->SendType = SEND_TYPE_TEXT;
        return file_get_contents($File);
    }

	public function GetSession()
	{
		return User::Init()->GetSessionId();
	}
}