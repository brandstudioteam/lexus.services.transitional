<?php
define("SITE_LEXUS",							"lexus");
define("SITE_TOYOTA",							"toyota");

if(!defined("SITE_CURRENT"))
    define("SITE_CURRENT",							preg_match('/lexus/ui', $_SERVER["HTTP_HOST"]) ? SITE_LEXUS : SITE_TOYOTA);

define("BRAND_TOYOTA",                          1);
define("BRAND_LEXUS",                           2);

/**
 *
 * Константы кодировок
 */
define("CHARSET", 								"utf-8");

/**
 *  Константы путей
 */
/**
 *
 * Путь к начальной папке
 * @var string Путь
 */
define("PATH_DOCROOT", 						$_SERVER['DOCUMENT_ROOT']);

/**
 * Путь к библиотекам php
 * @var string Путь
 */
define("PATH_LIBS", 						PATH_ROOT."libraries/");

/**
 * Пути к файловым репозиторям объектов интерфейса
 */

/**
 * Путь к директории скриптов js
 * @var string Путь
 */
define("PATH_SCRIPTES", 					PATH_STATIC."js/");

/**
 * Путь к директории файлов таблиц стилей
 * @var string Путь
 */
define("PATH_CSS", 							PATH_STATIC."css/");


/**
 * Путь к директории изображений
 * @var string Путь
 */
define("PATH_IMAGES", 						PATH_STATIC."img/");

if(SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_DEVELOPMENT)
	define("PATH_SERVER_STATICS", 				"/work/www/dev/toyota/statics/www/");
else define("PATH_SERVER_STATICS", 				"/work/www/toyota/content.toyota.ru/www/");

/**
 * Путь к директории изображений
 * @var string Путь
 */
define("PATH_NULL_PIXEL_IMAGE", 			PATH_STATIC."img/1-1.gif");

define("PATH_PAGES",						PATH_ROOT."pages/");

define("PATH_TEMPLATES",					SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_DEVELOPMENT ? PATH_ROOT."templates/dev/" : PATH_ROOT."templates/");

define("PATH_SERVICES",						PATH_ROOT."services/");

define("PATH_TEMPLATES_MAIL",				PATH_ROOT."templates/tpls/");

define("PATH_IMPORTED_FILES",				PATH_ROOT."imported/");

define("PATH_CAR_VALAUATION_UPLOAD",		PATH_ROOT."car_val_uploaded/");

define("PATH_PARTNERS_MAP_IMAGE",			PATH_SERVER_STATICS."i/");

define("PATH_MODELS_IMAGES",				PATH_STATIC."img/cars/");

define("PATH_SERVICES_UPLOAD_FILE",			PATH_ROOT."_tmp_upload_/");

define('PHPEXCEL_ROOT',						PATH_LIBS."third_party/");

/**
 * URLs
 */

/**
 * Базовый URLs
 */
define("URL_BASE", 							$_SERVER["HTTP_HOST"]."/z/");
define("URL_BASE_FULL",						(isset($_SERVER["HTTPS"]) ? "https://" : "http://").URL_BASE);

/**
 * Основной URL
 */
define("URL_CURRENT", 						$_SERVER["HTTP_HOST"]);

define("URL_PATH_IMAGE",					"/img/");

define("URL_SCRIPTS",						"/js/");
define("URL_SCRIPTS_FULL",					$_SERVER["HTTP_HOST"].URL_SCRIPTS);

define("URL_SYS_FONTS",						"/mobile/fonts/");
define("URL_SYS_FONTS_FULL",				$_SERVER["HTTP_HOST"].URL_SYS_FONTS);

define("URL_CSS",							"/css/");
define("URL_CSS_FULL",						$_SERVER["HTTP_HOST"].URL_CSS);

define("URL_IMAGE",							"/img/");
define("URL_IMAGE_FULL",					$_SERVER["HTTP_HOST"].URL_IMAGE);

define("URL_MODELS_IMAGE",					URL_IMAGE."cars/");
define("URL_MODELS_IMAGE_FULL",				$_SERVER["HTTP_HOST"].URL_MODELS_IMAGE);

define("URL_AUTORIZATION",					URL_BASE_FULL."/auth/");


if(SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_DEVELOPMENT)
	define("URL_SERVER_STATICS",				"statics.lexus.dev.bstd.ru/");
else define("URL_SERVER_STATICS",				"content.toyota.ru/");

define("URL_SERVER_STATICS_FULL",				(isset($_SERVER["HTTPS"]) ? "https://" : "http://").URL_SERVER_STATICS);

define("URL_PARTNERS_MAP_IMAGE",			URL_SERVER_STATICS_FULL."i/");

/**
 *
 * Константы инструментария отладки
 */

/**
 * Разрешение использвания объекта Debug
 */
define("DEBUG_ENABLE",							true);

/**
 * Записывать отчеты в файл
 */
define("DEBUG_REPORT_FILE",					1);

/**
 * Выводить отчеты в консоль клиентского интерфейса
 */
define("DEBUG_REPORT_JSCONSOLE",			2);

/**
 * Записывать отчеты в файл и выводить в консоль клиентского интерфейса
 */
define("DEBUG_REPORT_BOTH",					3);
/**
 * Максимальный размер файла отчета
 */
define("DEBUG_FILE_SIZE_MAX",				10485760);

/**
 * Текущий режим вывода отчетов
 */
//define("DEBUG_MODE",						DEBUG_REPORT_BOTH);//DEBUG_REPORT_JSCONSOLE);
define("DEBUG_MODE",     					DEBUG_REPORT_FILE);

/**
 * Внешний класс форматирования информации о трассировке
 */
define("DEBUG_EXTERNAL_FORMATER_TRACE", 	"");

/**
 * Внешний класс формитирования информации об исключении
 */
define("DEBUG_EXTERNAL_FORMATER_EXCEPTION", "");

define("DEBUG_FILES_PATH",					PATH_ROOT."debug/");

/**
 * Файл, в который записывается отчет, с путем к нему
 */
define("DEBUG_FILE",						DEBUG_FILES_PATH."debug.log");

define("DEBUG_FILE_MODE_SESSION", 			1);
define("DEBUG_FILE_MODE_SITE", 				2);
define("DEBUG_FILE_MODE_CUSTOM", 			3);

define("DEBUG_FILE_MODE", 					DEBUG_FILE_MODE_SITE);



/**
 * Константы управления подключением к внешним серверам
 */

/**
 * Подключаться к хосту через прокси-сервер по-умолчанию
 */
define("ISPROXY",							false);

if(ISPROXY)
{
	/**
	 * ip-адрес прокси-сервера
	 */
	define("PROXY_IP",						"");

	/**
	 * порт подключения к прокси-серверу
	 */
	define("PROXY_PORT",					"");

	/**
	 * логин для аутентификации на прокси-сервере
	 */
	define("PROXY_LOGIN",					"");

	/**
	 * пароль для аутентификации на прокси-сервере
	 */
	define("PROXY_PASSWORD",				"");
}


/**
 * Константы, определяющие настройки протоколов клиента и сервера
 */

/**
 *
 * Максимальное время межуду обновлениями клиента (подтверждением активности - пингами)
 * @var int секунды
 */
define("CLIENT_PING_MAX",					60);

/**
 *
 * Минимальное время межуду обновлениями клиента (подтверждением активности - пингами)
 * @var int секунды
 */
define("CLIENT_PING_MIN",					1);


/**
 * константы управления протоколами
 */
define("PROTOCOL_ENABLE_SSL", 				false);


/**
 * Константы, конфигурирующие работу сессий
 */

/**
 * Сессия, хранимая в файле
 */
define("SESSION_TYPE_FILE",					1);

/**
 * Сессия, хранимая в Базе Данных
 */
define("SESSION_TYPE_DATABASE",				2);

/**
 * Текущий тип сессии
 */
define("SESSION_TYPE",						SESSION_TYPE_FILE);

/**
 * Время жизни сессии
 */
define("SESSION_TIME_DEFAULT",				60*60*5); //60*30

define("SESSIONS_LIFETIME", 				3600);

/**
 * Название пользовательской сессии
 */
define("SESSION_NAME_USER",					"umc");


/**
 * Если не определены явно имена сессий, использовать название сессии по-умолчанию
 */
define("SESSION_NAME_DEFAULT",				false);

if(SESSION_TYPE == SESSION_TYPE_DATABASE)
{
	/**
	 * Название таблицы в базе данных для хранения сессий
	 */
	define("SESSION_TABLENAME", 			"sys_sessions");

	/**
	 * Название поля идентификатора записи
	 */
	define("SESSION_FIELDS_ID",				"sid");

	/**
	 * Название поля сохраненных данных
	 */
	define("SESSION_FIELDS_DATA",			"data");

	/**
	 * Название поля идентификатора пользователя
	 */
	define("SESSION_FIELDS_USER",			"user_id");

	/**
	 * Название поля идентификатора пользователя
	 */
	define("SESSION_FIELDS_TIMEATAMP",		"timestamp");
}

define("SESSIOIN_SECTION_USERS",			"UserData");
define("SESSIOIN_SECTION_SERVICES",			"Services");
define("SESSIOIN_SECTION_CONNECTED",		"Connect");

/*
if(SYSTEM_LOCATION_CURRENT == SYSTEM_LOCATION_DEVELOPMENT)
{
	try {
		session_save_path(PATH_ROOT."tmp/");
	}
	catch(dmtException $e)
	{
		if(!file_exists(PATH_ROOT."tmp/"))
		{
			mkdir(PATH_ROOT."tmp/");
			session_save_path(PATH_ROOT."tmp/");
		}
	}
}
*/

define("PASSWORD_PREFIX",							'h2PHfKRjnrh60NfEB1YuMQOClU9KYR17'); //SITE_CURRENT == SITE_LEXUS ? 'h2PHfKRjnrh60NfEB1YuMQOClU9KYR17' : 'M3GePhQ9VaLXdZqv3d9Odc0mSzoCwNKl');

define("SERVICES_ADDRESS_SUBSCRIPTIONS",			"http://subscribe.lexus.ru/z/");

define("TOYOTA_MOTORS_RUSSIA_ID",											108);



/**
 * Всякие-разные
 */
if(SITE_CURRENT == SITE_LEXUS)
{
	define("SMSGATE_SENDER_NAME",			"Lexus");
	define("SYSTEM_EMAIL_NOREPLY",			'info@lexusmasterclass.ru');
	define("SYSTEM_EMAIL_NOREPLY_NAME",		'Lexus Master Class');
}
else
{
	define("SMSGATE_SENDER_NAME",			"Toyota");
	define("SYSTEM_EMAIL_NOREPLY",			'info@toyota.ru');
	define("SYSTEM_EMAIL_NOREPLY_NAME",		'Toyota Motors');
}




define("TEMPLATES_REGISTER_USER",			PATH_TEMPLATES_MAIL."userRegisterEmail.html");






require_once 'DataBases.constants.php';
require_once 'BasicObjects.constants.php';
require_once 'Controllers.constants.php';
require_once 'AccountsConnectsConstants.php';
require_once 'Permissions.constants.php';
require_once 'Events.constants.php';
require_once 'MailConstants.class.php';