<?php
class CarsData extends Data
{
	public function GetManufacturersList()
	{
		return $this->Get("SELECT
	`manufacturer_id` AS `id`,
	`name` AS `title`
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_manufacturers`
WHERE `status`=1;");
	}


	public function GetManufacturersList2()
	{
		return $this->Get("SELECT
	`manufacturer_id` AS manufacturerId,
	`name` AS manufacturerName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_manufacturers`
WHERE `status`=1;");
	}

	public function GetCarsForManufacturer($Manufacturer)
	{
		return $this->Get("SELECT
	`model_id` AS `id`,
	`manufacturer_id` AS manufacturerId,
	`name` AS `title`
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_models`".($Manufacturer ? "
WHERE `manufacturer_id`=".$Manufacturer : "").";");
	}

	public function GetCarsForManufacturer2($Manufacturer)
	{
		return $this->Get("SELECT
	`model_id` AS carModelId,
	-- `manufacturer_id` AS manufacturerId,
	`name` AS carModelName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_models`
WHERE `manufacturer_id`=".$Manufacturer.";");
	}

	public function GetCarsF()
	{
		return $this->Get("SELECT
	`model_id` AS carModel,
	CONCAT_WS(' ', af.`name`, am.`name`) AS carName
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_models` AS am
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`automobile_manufacturers` AS af ON af.`manufacturer_id`=am.`manufacturer_id`
WHERE af.`status`=1;");
	}

	public function GetCarFullName($Car)
	{
		return $this->Count("SELECT
	CONCAT_WS(' ', af.`name`, am.`name`) AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_models` AS am
LEFT JOIN `".DBS_UNIVERSAL_REFERENCES."`.`automobile_manufacturers` AS af ON af.`manufacturer_id`=am.`manufacturer_id`
WHERE `model_id`=".$Car."
	AND af.`status`=1;");
	}

	public function GetCarName($Car)
	{
		return $this->Count("SELECT
	`name` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_models`
WHERE `model_id`=".$Car.";");
	}

	public function GetManufacturerName($Manufacturer)
	{
		return $this->Count("SELECT
	`name` AS Cnt
FROM `".DBS_UNIVERSAL_REFERENCES."`.`automobile_manufacturers`
WHERE `manufacturer_id`=".$Manufacturer.";");
	}
}