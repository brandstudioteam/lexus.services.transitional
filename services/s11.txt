{
	service:				11,
	links: {
		4: {
			dataModels: ["dealers"]
		},
		7: {
			dataModels: ["models"]
		}
	},
	init: {
		inclideCSS: [
			["http://content.toyota.ru/css/s11.css"]
		],
		messages: [
			["modelId", "Идентификатор модели", 152],
			["modelName", "Название модели", 152],
			["modelUrl", "URL страницы", 152],
			["save", "Сохранить", 152],
			["DataSaveSuccess", "Данные успешно сохранены", 152],
			["importModelsFromFile", "Загрузить из файла", 152],
			["importFromFileSucces", "Данные из файла загружены", 152],
			["modelFamilyImage", "Изображение", 152],
			["modelFamilyPageUrl", "Специализарованная страница", 152],
			["modelFamilyOrder", "Порядок сортировки", 152],
			["modelFamilyComment", "Отображаемый комментарий", 152],
			["memberVisitTimeCall", "Желаемое время для звонка", 152]
		]
	},
	modelsDefs: {
		testDrivesMembers: {
			type:								"base",
			fieldsDef: {
				memberId: {
					title:						"memberId",
					dataType:					"int"
				},
				firstName: {
					title:						"firstName",
					dataType:					"firstName"
				},
				middleName: {
					title:						"middleName",
					dataType:					"firstName"
				},
				lastName:  {
					title:						"lastName",
					dataType:					"lastName"
				},
				gender:  {
					title:						"gender",
					dataType:					"int"
				},
				genderName:  {
					title:						"genderName",
					dataType:					"string"
				},
				age:  {
					title:						"age",
					dataType:					"int"
				},
				email:  {
					title:						"email",
					dataType:					"email"
				},
				phone:  {
					title:						"phone",
					dataType:					"phone"
				},
				isOwner: {
					title:						"isOwner",
					dataType:					"int"
				},
				fds: {
					title:						"fds",
					dataType:					"int"
				},
				memberRegisterU:  {
					title:						"registerU",
					dataType:					"datetime"
				},
				memberRegister:  {
					title:						"register",
					dataType:					"datetime"
				},
				trafficSourceId:  {
					title:						"trafficSourceId",
					dataType:					"int"
				},
				trafficSourceName:  {
					title:						"trafficSourceName",
					dataType:					"string"
				},
				memberVisitDate:  {
					title:						"visitDate",
					dataType:					"date"
				},
				memberVisitTime:  {
					title:						"memberVisitTimeCall",
					dataType:					"time"
				},
				modelName:  {
					title:						"modelName",
					dataType:					"string"
				},
				modelAlias:  {
					title:						"modelAlias",
					dataType:					"string"
				},
				partnersDivisionName:  {
					title:						"dealer",
					dataType:					"string"
				},
				RCode:  {
					title:						"rCode",
					dataType:					"string"
				}
			},
			keyFields: [
				"memberId"
			],
			loadType:						"preload",
			source: {
				url:							"/admin/facilities/forms/",
				controller:						"ai",
				action:							"g",
				arguments:				[
					{
						type:					"value",
						name:					"facilityId",
						value:					50
					}
				]
			},
			actions: [
				{
					name:					"getFile",
					title:					"Отчет о заявках",
					category:				"collection",
					type:					"getFile",
					server: {
						url:					"/admin/facilities/forms/csv/",
						arguments:				[
							{
								type:					"exec",
								method:					"_currentView.filters.getFiltersValues"
							},
							{
								type:					"value",
								name:					"facilityId",
								value:					50
							}
						]
					}
				}
			]
		},
		testDrivesDealersModels: {
			type:								"matrix",
			fieldsDef: {
				modelId: {
					title:						"modelId",
					dataType:					"int"
				},
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				partnerDivisionModelStatus:  {
					title:						"partnerDivisionModelStatus",
					dataType:					"int"
				}
			},
			matrix:					{
				rowSource:				{
					data:					"dealers",
					rowField:				"partnerDivisionId",
					dataField:				"partnerDivisionId",
					titleField:				"partnerDivisionName"
				},
				columnSource:				{
					data:					"models",
					colField:				"modelId",
					dataField:				"modelId",
					titleField:				"modelName"
				},
				valueField:				{
					field:					"partnerDivisionModelStatus",
					edidable:				{
						mode:					"always",
						editedElement:			"checkbox",
						elementStateField:		"partnerDivisionModelStatus",
						elementName:			"partnerDivisionModelStatusAll"
					}
				},
				predefinedFields:		{
					facilityId:				50
				},
				useKeyFields: [
					"facilityId",
					"partnerDivisionId"
				]
			},
			keyFields: [
				"facilityId",
				"modelId",
				"partnerDivisionId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/facilities/",
				controller:						"ai",
				action:							"e",
				arguments:				{
					facilityId:				{
						type:					"value",
						data:					50
					},
					partnerDivisionModelStatus:				{
						type:					"value",
						data:					[1, 0]
					}
				}
			},
			actions: [
				{
					name:					"edidableTable",
					title:					"save",
					category:				"collection",
					type:					"edidableTable",
					view:					"testDrivesDealersModelTables",
					server: {
						url:					"/admin/facilities/",
						controller:				"m",
						action:					"d",
						arguments:				[{
							type:					"values",
							method:					"_currentView.filters.getFiltersValues"
						}]
					}
				}
			]
		},

		testDrivesDealersModelsCount: {
			type:								"matrix",
			fieldsDef: {
				modelId: {
					title:						"modelId",
					dataType:					"int"
				},
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				partnerDivisionModelCount:  {
					title:						"partnerDivisionModelCount",
					dataType:					"int"
				}
			},
			matrix:					{
				rowSource:				{
					data:					"dealers",
					rowField:				"partnerDivisionId",
					dataField:				"partnerDivisionId",
					titleField:				"partnerDivisionName"
				},
				columnSource:				{
					data:					"models",
					colField:				"modelId",
					dataField:				"modelId",
					titleField:				"modelName"
				},
				valueField:				{
					field:					"partnerDivisionModelCount",
					defaultValue:			0,
					edidable:				{
						mode:					"always",
						editedElement:			"string",
						//elementStateField:		"partnerDivisionModelCount",
						elementName:			"partnerDivisionModelCountAll"
					}
				},
				predefinedFields:		{
					facilityId:				50
				},
				useKeyFields: [
					"facilityId",
					"partnerDivisionId"
				]
			},
			keyFields: [
				"facilityId",
				"modelId",
				"partnerDivisionId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/facilities/",
				controller:						"ai",
				action:							"e0",
				arguments:				{
					facilityId:				{
						type:					"value",
						data:					50
					},
					partnerDivisionModelStatus:				{
						type:					"value",
						data:					[1, 0]
					}
				}
			},
			actions: [
				{
					name:					"edidableTable",
					title:					"save",
					category:				"collection",
					type:					"edidableTable",
					view:					"testDrivesDealersModelCountTables",
					server: {
						url:					"/admin/facilities/",
						controller:				"m",
						action:					"d0",
						arguments:				[{
							type:					"values",
							method:					"_currentView.filters.getFiltersValues"
						}]
					}
				}
			]
		},
		testDrivesModelsFamilies: {
			type:								"matrix",
			fieldsDef: {
				modelFamilyId: {
					title:						"modelFamilyId",
					dataType:					"int"
				},
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				modelFamilyStatus: {
					title:						"modelFamilyStatus",
					dataType:					"int"
				},
				modelFamilyImage:  {
					title:						"modelFamilyImage",
					dataType:					"image"
				},
				modelFamilyPageUrl:  {
					title:						"modelFamilyPageUrl",
					dataType:					"url"
				},
				modelFamilyOrder:  {
					title:						"modelFamilyOrder",
					dataType:					"int"
				},
				modelFamilyComment:  {
					title:						"modelFamilyComment",
					dataType:					"text"
				}
			},
			matrix:					{
				rowSource:				{
					data:					"modelsFamilies",
					rowField:				"modelFamilyId",
					dataField:				"modelFamilyId",
					titleField:				"modelFamilyName"
				},
				columnFields:			[
					"modelFamilyStatus",
					"modelFamilyImage",
					"modelFamilyPageUrl",
					"modelFamilyOrder",
					"modelFamilyComment"
				],
				valueField:				{
					field:					"modelFamilyStatus"
				},
				predefinedFields:		{
					facilityId:				50
				},
				useKeyFields: [
					"modelFamilyId"
				]
			},
			keyFields: [
				"facilityId",
				"modelFamilyId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/models/",
				controller:						"l",
				action:							"c",
				arguments:				{
					facilityId:				{
						type:					"value",
						data:					50
					}
				}
			},
			actions: [
				{
					name:					"edit",
					title:					"edit",
					category:				"object",
					type:					"form",
					view:					"testDrivesModelsFamiliesEdit",
					server: {
						url:					"/admin/models/",
						controller:				"l",
						action:					"d"
					}
				}
			]
		},

		testDrivesModels: {
			type:								"matrix",
			fieldsDef: {
				modelId: {
					title:						"modelId",
					dataType:					"int"
				},
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				facilityModelStatus: {
					title:						"modelStatus",
					dataType:					"int"
				},
				facilityModelImage:  {
					title:						"modelFamilyImage",
					dataType:					"image"
				},
				facilityModelPage:  {
					title:						"modelFamilyPageUrl",
					dataType:					"url"
				},
				facilityModelOrder:  {
					title:						"modelFamilyOrder",
					dataType:					"int"
				},
				facilityModelComment:  {
					title:						"modelFamilyComment",
					dataType:					"text"
				}
			},
			matrix:					{
				rowSource:				{
					data:					"models",
					rowField:				"modelId",
					dataField:				"modelId",
					titleField:				"modelName"
				},
				columnFields:			[
					"facilityModelStatus",
					"facilityModelImage",
					"facilityModelPage",
					"facilityModelOrder",
					"facilityModelComment"
				],
				valueField:				{
					field:					"modelStatus"
				},
				predefinedFields:		{
					facilityId:				50
				},
				useKeyFields: [
					"modelId"
				]
			},
			keyFields: [
				"facilityId",
				"modelId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/models/",
				controller:						"l",
				action:							"e",
				arguments:				{
					facilityId:				{
						type:					"value",
						data:					50
					}
				}
			},
			actions: [
				{
					name:					"edit",
					title:					"edit",
					category:				"object",
					type:					"form",
					view:					"testDrivesModelsEdit",
					server: {
						url:					"/admin/models/",
						controller:				"l",
						action:					"g"
					}
				}
			]
		}
	},
	ViewsDefs: {
		testDrivesMemberCollections: {
			category:				"collection",
			data:					"testDrivesMembers",
			viewType:				"asTable",
			fields:					[
				{
					field:				"partnersDivisionName",
					ordered:			1
				},
				{
					field:				"modelName",
					ordered:			1
				},
				{
					field:				"firstName",
					ordered:			1
				},
				{
					field:				"lastName",
					ordered:			1
				},
				{
					field:				"genderName",
					ordered:			1
				},
				{
					field:				"age",
					ordered:			1
				},
				{
					field:				"email",
					ordered:			1
				},
				{
					field:				"phone",
					ordered:			1
				},
				{
					field:				"memberVisitTime",
					ordered:			1
				},
				{
					field:				"fds",
					ordered:			1
				},
				{
					field:				"memberRegister",
					ordered:			1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"partnersDivisionId",
						fieldTitle:		"partnersDivisionName",
						title:			"Дилерский центр",
						type:			"select",
						idField:		"partnersDivisionId",
						titleField:		"partnersDivisionName",
						dataType:		"int",
						order:			{
							fieldName:	"partnersDivisionName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"modelId",
						fieldTitle:		"modelName",
						title:			"Модель",
						type:			"select",
						idField:		"modelId",
						titleField:		"modelName",
						order:			{
							fieldName:	"modelName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"memberRegisterU",
						fieldTitle:		"memberRegisterU",
						title:			"Дата регистрации, от",
						idField:		"memberRegisterU",
						titleField:		"memberRegisterU",
						type:			"Datepicker",
						range:			"min",		//min, max, both
						outRange:		0,
						convert:		1
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100
			}
		},
		testDrivesDealersModelTables: {
			category:				"collection",
			viewType:				"asTable",
			data:					"testDrivesDealersModels",
			filters:	{
				type:			"static",
				fields: []
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
				cellMode:		"edidable",
				buildMode:		"matrix",
				edidableMode:	"always"
			}
		},
		testDrivesDealersModelCountTables: {
			category:				"collection",
			viewType:				"asTable",
			data:					"testDrivesDealersModelsCount",
			filters:	{
				type:			"static",
				fields: []
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
				cellMode:		"edidable",
				buildMode:		"matrix",
				edidableMode:	"always"
			}
		},
		testDrivesModelsFamiliesTables: {
			category:				"collection",
			viewType:				"asTable",
			data:					"testDrivesModelsFamilies",
			filters:	{
				type:			"static",
				fields: []
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
				buildMode:		"matrix"
			}
		},
		testDrivesModelsFamiliesEdit: 	{
			category:				"object",
			data:					"testDrivesModelsFamilies",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s11a17edit.html",
				elementSelector: 		"#s11a17edit_form"
			},
			fields:					[
				{
					field:		"modelFamilyId"
				},
				{
					field:		"facilityId"
				},
				{
					field:		"modelFamilyStatus"
				},
				{
					field:		"modelFamilyImage"
				},
				{
					field:		"modelFamilyPageUrl"
				},
				{
					field:		"modelFamilyOrder"
				},
				{
					field:		"modelFamilyComment"
				}
			],
			params: {
				mode: 2
			}
		},
		testDrivesModelsTables: {
			category:				"collection",
			viewType:				"asTable",
			data:					"testDrivesModels",
			filters:	{
				type:			"static",
				fields: []
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
				buildMode:		"matrix"
			}
		},
		testDrivesModelsEdit: 	{
			category:				"object",
			data:					"testDrivesModels",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s11a18edit.html",
				elementSelector: 		"#s11a18edit_form"
			},
			fields:					[
				{
					field:		"modelId"
				},
				{
					field:		"facilityId"
				},
				{
					field:		"modelStatus"
				},
				{
					field:		"modelImage"
				},
				{
					field:		"modelPageUrl"
				},
				{
					field:		"modelOrder"
				},
				{
					field:		"modelComment"
				}
			],
			params: {
				mode: 2
			}
		},
		testDrivesDealersModelImport:	{
			category:				"object",
			data:					"testDrivesDealersModels",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:				"s11a14upload.html",
				elementSelector: 	"#s11a14upload_form"
			},
			params: {
				mode:				1,
				resultActionsType:	"reloadData"
			}
		}

	},

	UsersEvents:	{

	},

	ActionsDefs: {
		4: {
			action:					4,
			views:					["testDrivesMemberCollections"],
			defaultView:			"testDrivesMemberCollections"
		},
		14: {
			action:					14,
			views:					["testDrivesDealersModelTables"],
			defaultView:			"testDrivesDealersModelTables"
		},
		17: {
			action:					17,
			views:					["testDrivesModelsFamiliesTables"],
			defaultView:			"testDrivesModelsFamiliesTables"
		},
		18: {
			action:					18,
			views:					["testDrivesModelsTables"],
			defaultView:			"testDrivesModelsTables"
		},
		20: {
			action:					20,
			views:					["testDrivesDealersModelCountTables"],
			defaultView:			"testDrivesDealersModelCountTables"
		}
	}
}