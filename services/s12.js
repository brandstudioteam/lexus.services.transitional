var a = {
	service:				12,
	links: {},
	init: {
		messages: [
			["campaignId", "Идентификатор кампании", 152],
			["campaignStatusId", "Идентификатор статуса кампании", 152],
			["campaignName", "Название кампании", 152],
			["campaignDescription", "Описание кампании", 152],
			["campaignUseCars", "Использовать разделение квот по моделям", 152],
			["campaignUseAgency", "Участвуют агентства", 152],
			["campaignUsePreregistred", "Используется предварительная регистрация", 152],
			["campaignUseTMR", "Участвует ТМР", 152],
			["campaignTMRGetQuotesFromDealers", "Участники от ТМР используют квоты дилеров", 152],
			["campaignTMRRegistredByDealers", "Участники от ТМР записываются к дилерам", 152],
			["campaignStatusName", "Статус кампании", 152]
		],
		"inclideCSS": [
			["http://content.toyota.ru/css/s23.css"]
		]
	},
	modelsDefs: {
		TDCampaign: {
			fieldsDef: {
				campaignId: {
					title:						"campaignId",
					dataType:					"int"
				},
				campaignStatusId: {
					title:						"campaignStatusId",
					dataType:					"int"
				},
				campaignName: {
					title:						"campaignName",
					dataType:					"string"
				},
				campaignDescription: {
					title:						"campaignDescription",
					dataType:					"text"
				},
				campaignUseCars: {
					title:						"campaignUseCars",
					dataType:					"boolean"
				},
				campaignUseAgency: {
					title:						"campaignUseAgency",
					dataType:					"boolean"
				},
				campaignUsePreregistred: {
					title:						"campaignUsePreregistred",
					dataType:					"boolean"
				},
				campaignUseTMR: {
					title:						"campaignUseTMR",
					dataType:					"boolean"
				},
				campaignTMRGetQuotesFromDealers: {
					title:						"campaignTMRGetQuotesFromDealers",
					dataType:					"boolean"
				},
				campaignTMRRegistredByDealers: {
					title:						"campaignTMRRegistredByDealers",
					dataType:					"boolean"
				},
				campaignStatusName: {
					title:						"campaignStatusName",
					dataType:					"year"
				}
			},
			keyFields: [
									"campaignId"
			],
			source:	{
				url:					{
					path:					"bstdSystem.Sets.TDCampaigns.Base.url"
				},
				controller:				"",
				action:					"d"
			},
			loadType:				"preload",
			actions: [
				{
					name:					"view",
					title:					"Посмотреть",
					category:				"object",
					type:					"view",
					view:					"tradeInEvalMemberAddCost",
					server: {
						url:					"/admin/tradein/evaluation/",
						controller:				"s",
						action:					"g"
					}
				},
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"toDealersActionsAdd",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"s"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить запись<br />{#campaignName#}"
											},
											{
												type:	"value",
												value:	"Удаление записи"
											},
											{
												type:	"currentAction"
											}
										]
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"a2"
					}
				}
			]
		},
		TDActions: {
			fieldsDef: {
				actionId: {
					title:						"TDactionId",
					dataType:					"int"
				},
				campaignStatus: {
					title:						"campaignStatus",
					dataType:					"int"
				},
				actionStart: {
					title:						"TDactionStart",
					dataType:					"time"
				},
				actionEnd: {
					title:						"TDactionEnd",
					dataType:					"time"
				},
				actionDate: {
					title:						"TDactionDate",
					dataType:					"date"
				},
				actionName: {
					title:						"TDactionName",
					dataType:					"string"
				},
				actionDescription: {
					title:						"TDactionDescription",
					dataType:					"text"
				},
				actionVenue: {
					title:						"actionVenue",
					dataType:					"boolean"
				},
				actionEmailText: {
					title:						"actionEmailText",
					dataType:					"boolean"
				}
			},
			keyFields: [
									"actionId"
			],
			source:	{
				url:					{
					path:					"bstdSystem.Sets.TDCampaigns.Base.url"
				},
				controller:				"",
				action:					"j"
			},
			loadType:				"preload",
			actions: [
				{
					name:					"view",
					title:					"Посмотреть",
					category:				"object",
					type:					"view",
					view:					"toMemberView",
					execMethod:				"selected",
					customAction:			"editUsersAction",
					buttonTemplate:			'<a class="edit_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" href="javascript:void(0);">'
												+ '<span class="ui-button-icon-primary ui-icon ui-icon-pencil"></span>'
												+ '<span class="ui-button-text"> Редактировать</span>'
											+ '</a>',
					exec: {
						url:					"http://content.toyota.ru/js/bstd/execs/s23a2n.js",
						className:				"s23a2_exec",
						method:					"memberView"
					}
				},
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"toDealersActionsAdd",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"s"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить запись<br />{#campaignName#}"
											},
											{
												type:	"value",
												value:	"Удаление записи"
											},
											{
												type:	"currentAction"
											}
										]
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"a2"
					}
				}
			]
		},
	},





	ViewsDefs: {
		TDCampaignCollections: {
			category:				"collection",
			data:					"toMember",
			viewType:				"asTable",
			fields:					[
				{
					field:	"campaignName",
					ordered:	1
				},
				{
					field:	"campaignDescription",
					ordered:	1
				},
				{
					field:	"campaignStatusName",
					ordered:	1
				},
				{
					field:	"campaignUseCars",
					ordered:	1
				},
				{
					field:	"campaignUseAgency",
					ordered:	1
				},
				{
					field:	"campaignUsePreregistred",
					ordered:	1
				},
				{
					field:	"campaignUseTMR",
					ordered:	1
				},
				{
					field:	"campaignTMRGetQuotesFromDealers",
					ordered:	1
				},
				{
					field:	"campaignTMRRegistredByDealers",
					ordered:	1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"campaignStatusId",
						fieldTitle:		"campaignStatusName",
						//title:			"Дилерский центр",
						type:			"select",
						idField:		"campaignStatusId",
						titleField:		"campaignStatusName",
						dataType:		"int",
						order:			{
							fieldName:	"campaignStatusName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100
			}
		},
	},

	UsersEvents:	{

	},



	ActionsDefs: {
		15: {
			action:					15,
			views:					["TDCampaignCollections"],
			defaultView:			"TDCampaignCollections",
		},
	}
}