{
	service:				6,
	init: {
		messages: [
			["parents", "Вышестоящие подразделения", 152],
			["parentPartnerDivisionId", "Идентификатор вышестоящие подразделения", 152]
		]
	},

	modelsDefs: {
		otherModelsSimple: {
			type:								"base",
			fieldsDef: {
				carModel: {
					title:						"carModel",
					dataType:					"int"
				},
				carName: {
					title:						"carName",
					dataType:					"string"
				}
			},
			keyFields: [
							"carModel"
			],
			source: {
				url:							"/admin/cars/",
				controller:						"c",
				action:							"b1",
			}
		}
	}
}