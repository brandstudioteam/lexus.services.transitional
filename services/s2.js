a = {
	service:				2,
	links: {
		4: {
			dataModels: ["partners"]
		}
	},
	init: {
		messages: [
			["roleId", "Идентификатор роли", 152],
			["roleTypeId", "Идентификатор типа роли", 152],
			["roleName", "Роль", 152],
			["roleDescription", "Описание", 152],
			["roleTypeName", "Тип роли", 152],
			["permissions", "Права", 152],
			["permissionName", "Право", 152]
		]
	},
	modelsDefs: {
		roles: {
			fieldsDef: {
				roleId:{
					title:						"roleId",
					dataType:					"int"
				},
				roleTypeId:{
					title:						"roleTypeId",
					dataType:					"int"
				},
				roleName:{
					title:						"roleName",
					dataType:					"string"
				},
				roleDescription:{
					title:						"roleDescription",
					dataType:					"text"
				},
				roleTypeName:{
					title:						"roleTypeName",
					dataType:					"string"
				},
				permissions:{
					title:						"permissions",
					dataType:					"collection"
				}
			},
			keyFields: [
				"roleId"
			],
			source:	{
				url:							"/admin/roles/",
				controller:						"j",
				action:							"a"
			},
			loadType:				"preload"
		},
		rolesPermissions: {
			fieldsDef: {
				roleId:{
					title:						"roleId",
					dataType:					"int"
				},
				permissionId:{
					title:						"permissionId",
					dataType:					"int"
				}
			},
			keyFields: [
				"roleId",
				"permissionId"
			],
			source:	{
				url:							"/admin/roles/",
				controller:						"j",
				action:							"e"
			},
			loadType:				"preload"
		},
		rolesTypes: {
			fieldsDef: {
				roleTypeId:{
					title:						"roleTypeId",
					dataType:					"int"
				},
				roleTypeName:{
					title:						"roleTypeName",
					dataType:					"string"
				},
				roleTypeDescription:{
					title:						"roleTypeDescription",
					dataType:					"text"
				}
			},
			keyFields: [
				"roleTypeId"
			],
			source:	{
				url:							"/admin/roles/",
				controller:						"j",
				action:							"p"
			},
			loadType:				"preload"
		},
		rolesTypesUsersTypes: {
			fieldsDef: {
				roleTypeId:{
					title:						"roleTypeId",
					dataType:					"int"
				},
				userTypeId:{
					title:						"userTypeId",
					dataType:					"int"
				}
			},
			keyFields: [
				"roleTypeId",
				"userTypeId"
			],
			source:	{
				url:							"/admin/roles/",
				controller:						"j",
				action:							"o"
			},
			loadType:				"preload"
		}
	},
	ViewsDefs: {
		rolesCollection: {
			category:				"collection",
			data:					"roles",
			viewType:				"asTable",
			fields:		[
				{
					field:	"roleName",
					ordered:	1
				},
				{
					field:	"roleDescription",
					ordered:	1
				},
				{
					field:	"roleTypeName",
					ordered:	1
				},
				{
					field:	"permissions",
					ordered:	1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"roleTypeId",
						fieldTitle:		"roleTypeName",
						title:			"roleTypeName",
						type:			"select",
						idField:		"roleTypeId",
						titleField:		"roleTypeName",
						dataType:		"int",
						order:			{
							fieldName:	"roleTypeName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100
			}
		}
	},

	UsersEvents:	{

	},



	ActionsDefs: {
		default: {
			action:					"default",
			views:					["rolesCollection"],
			defaultView:			"rolesCollection"
		}
	}
}