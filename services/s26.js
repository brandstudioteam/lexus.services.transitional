a = {
	service:				26,
	init: {
		inclideCSS: [
			["http://content.toyota.ru/css/s11.css"]
		],
		messages: [
			["modelId", "Идентификатор модели", 152],
			["modelName", "Название модели", 152],
			["userName", "Сотрудник", 152],
			["date", "Дата", 152],
			["timeStart", "Начало", 152],
			["timeEnd", "Окончание", 152],
			["memberName", "Пользователь", 152],
			["memberPhone", "Телефон", 152],
			["testdriveMileage", "Пробег", 152],
			["factualTimeStart", "Фактическое время начала", 152],
			["factualTimeEnd", "Фактическое время окончания", 152],
			["currentStatusName", "Статус", 152],
			["commentCount", "Количество комментариев", 152],
			["clear", "Очистить", 152]
		]
	},
	modelsDefs: {
		schedulerUsers: {
			type:								"base",
			fieldsDef: {
				recordId: {
					title:						"recordId",
					dataType:					"int"
				},
				userId: {
					title:						"userId",
					dataType:					"int"
				},
				timeCellId: {
					title:						"timeCellId",
					dataType:					"int"
				},
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				firstName:  {
					title:						"memberName",
					dataType:					"fio"
				},
				timeCellState:  {
					title:						"timeCellState",
					dataType:					"int"
				},
				timeCellStatus:  {
					title:						"timeCellStatus",
					dataType:					"string"
				},
				factualTimeStart:  {
					title:						"factualTimeStart",
					dataType:					"time"
				},
				factualTimeEnd:  {
					title:						"factualTimeEnd",
					dataType:					"time"
				},
				timeStart:  {
					title:						"timeStart",
					dataType:					"time"
				},
				timeEnd: {
					title:						"timeEnd",
					dataType:					"time"
				},
				date:  {
					title:						"date",
					dataType:					"date"
				},
				dateU:  {
					title:						"dateU",
					dataType:					"date"
				},
				duration:  {
					title:						"duration",
					dataType:					"int"
				},
				commentCount:  {
					title:						"commentCount",
					dataType:					"int"
				},
				phone:  {
					title:						"memberPhone",
					dataType:					"phone"
				},
				memberRegister:  {
					title:						"register",
					dataType:					"datetime"
				},
				currentStatus:  {
					title:						"currentStatus",
					dataType:					"int"
				},
				modelName:  {
					title:						"modelName",
					dataType:					"string"
				},
				mileage:  {
					title:						"testdriveMileage",
					dataType:					"int"
				},
				currentStatusName:  {
					title:						"currentStatusName",
					dataType:					"string"
				},
				userName:  {
					title:						"userName",
					dataType:					"string"
				}
			},
			keyFields: [
				"recordId"
			],
			loadType:						"preload",
			source: {
				url:							"/admin/scheduler/",
				controller:						"ak",
				action:							"a0",
				arguments:				[
					{
						type:					"value",
						name:					"facilityId",
						value:					50
					}
				]
			},
			actions: [
				{
					/*
					condition:				{
						permission: {
							service:						"8",
							value:							"2"
						}
					},
					*/
					name:					"view",
					title:					"Записать",
					category:				"collection",
					type:					"view",
					view:					"schedulerUsersEdit",
					execMethod:				"button",
					customAction:			"s26_editMember",
					/*
					buttonTemplate:			'<a class="edit_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" href="javascript:void(0);">'
												+ '<span class="ui-button-icon-primary ui-icon ui-icon-pencil"></span>'
												+ '<span class="ui-button-text"> Редактировать</span>'
											+ '</a>',
					*/
					exec: {
						url:					"http://content.toyota.ru/js/bstd/execs/s26aDef.js",
						className:				"s26aDef_exec",
						method:					"createNew"
					}
				},
				{
					/*
					condition:				{
						permission: {
							service:						"8",
							value:							"2"
						}
					},
					*/
					name:					"edit",
					title:					"Изменить",
					category:				"object",
					type:					"view",
					view:					"schedulerUsersEdit",
					execMethod:				"button",
					customAction:			"s26_editMember",
					/*
					buttonTemplate:			'<a class="edit_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" href="javascript:void(0);">'
												+ '<span class="ui-button-icon-primary ui-icon ui-icon-pencil"></span>'
												+ '<span class="ui-button-text"> Редактировать</span>'
											+ '</a>',
					*/
					exec: {
						url:					"http://content.toyota.ru/js/bstd/execs/s26aDef.js",
						className:				"s26aDef_exec",
						method:					"edit"
					}
				},
				{
					/*
					condition:				{
						permission: {
							service:						"8",
							value:							"2"
						}
					},
					*/
					name:					"clear",
					title:					"clear",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите очистить запись с пользователем <b>{#firstName#}<b>"
											},
											{
												type:	"value",
												value:	"Очистка записи"
											},
											{
												type:	"currentAction",
											}
										]
					},
					server: {
						url:							"/admin/scheduler/",
						controller:						"ak",
						action:							"d"
					}
				}
			]
		},
		schedulerTestDrivesAvailableModels: {
			type:								"basic",
			fieldsDef: {
				modelId: {
					title:						"modelId",
					dataType:					"int"
				},
				modelName: {
					title:						"modelName",
					dataType:					"string"
				}
			},
			keyFields: [
				"modelId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/scheduler/",
				controller:						"ak",
				action:							"e",
				arguments:				{
					facilityId:				{
						type:					"value",
						data:					50
					}
				}
			}
		}
	},
	ViewsDefs: {
		schedulerUsers: {
			category:				"collection",
			data:					"schedulerUsers",
			viewType:				"asTable",
			fields:					[
				{
					field:				"userName",
					ordered:			1
				},
				{
					field:				"date",
					ordered:			1
				},
				{
					field:				"timeStart",
					ordered:			1
				},
				{
					field:				"timeEnd",
					ordered:			1
				},
				{
					field:				"firstName",
					ordered:			1
				},
				{
					field:				"phone",
					ordered:			1
				},
				{
					field:				"modelName",
					ordered:			1
				},
				{
					field:				"mileage",
					ordered:			1
				},
				{
					field:				"factualTimeStart",
					ordered:			1
				},
				{
					field:				"factualTimeEnd",
					ordered:			1
				},
				{
					field:				"currentStatusName",
					ordered:			1
				},
				{
					field:				"commentCount",
					ordered:			1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"userId",
						fieldTitle:		"userName",
						title:			"Сотрудник",
						type:			"select",
						idField:		"userId",
						titleField:		"userName",
						dataType:		"int",
						order:			{
							fieldName:	"userName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"modelId",
						fieldTitle:		"modelName",
						title:			"Модель",
						type:			"select",
						idField:		"modelId",
						titleField:		"modelName",
						order:			{
							fieldName:	"modelName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"dateU",
						fieldTitle:		"dateU",
						title:			"Дата, от",
						idField:		"dateU",
						titleField:		"dateU",
						type:			"Datepicker",
						range:			"min",		//min, max, both
						outRange:		0,
						convert:		1
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100
			}
		},
		schedulerUsersEdit: {
			category:				"object",
			data:					"schedulerUsers",
			viewType:				"panel",
			fields:					[
/*
a.`record_id` AS recordId,
a.`user_id` AS userId,
a.`cell_id` AS timeCellId,
a.`partners_division_id` AS partnerDivisionId,
a.`facility_id` AS facilityId,
a.`state` AS timeCellState,
a.`status_id` AS timeCellStatus,
a.`factual_time_start` AS factualTimeStart,
a.`factual_time_end` AS factualTimeEnd,
a.`duration` AS duration,
a.`comments_count` AS commentCount,
DATE_FORMAT(b.`date`, '%d.%m.%Y') AS `date`,
b.`date` AS dateU,
b.`time_start` AS timeStart,
b.`time_end` AS timeEnd,
c.`member_id` AS memberId,
c.`model_id` AS modelId,
c.`first_name` AS firstName,
c.`phone` AS phone,
c.`register` AS register,
c.`mileage` AS mileage,
c.`current_status_id` AS currentStatus,
c.`comments_count` AS commentCount,
d.`name` AS modelName
  */

				{
					field:				"partnerDivisionId",
					ordered:			1
				},
				{
					field:				"timeCellId",
					ordered:			1
				},
				{
					field:				"recordId",
					ordered:			1
				},
				{
					field:				"userId",
					ordered:			1
				},
				{
					field:				"dateU"
				},



				{
					field:				"userName",
					ordered:			1
				},
				{
					field:				"date",
					ordered:			1
				},
				{
					field:				"timeStart",
					ordered:			1
				},
				{
					field:				"timeEnd",
					ordered:			1
				},
				{
					field:				"firstName",
					ordered:			1
				},
				{
					field:				"phone",
					ordered:			1
				},
				{
					field:				"modelName",
					ordered:			1
				},
				{
					field:				"mileage",
					ordered:			1
				},
				{
					field:				"factualTimeStart",
					ordered:			1
				},
				{
					field:				"factualTimeEnd",
					ordered:			1
				},
				{
					field:				"currentStatusName",
					ordered:			1
				},
				{
					field:				"commentCount",
					ordered:			1
				}
			]
		}
	},

	UsersEvents:	{

	},

	ActionsDefs: {
		default: {
			action:					4,
			views:					["schedulerUsers"],
			defaultView:			"schedulerUsers"
		}
	}
}