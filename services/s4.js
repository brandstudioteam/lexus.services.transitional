a = {
	service:				4,
	links: {
		5: {
			dataModels: ["regions", "cities"]
		},
		16: {
			dataModels: ["facilities"]
		}
	},
	init: {
		messages: [
			["parents", "Вышестоящие подразделения", 152],
			["parentPartnerDivisionId", "Идентификатор вышестоящие подразделения", 152],
			["partnerDivisionId", "Идентификатор подразделения партнера", 152],
			["partnerId", "Идентификатор партнера", 152],
			["lastName", "Фамилия", 152],
			["firstName", "Имя", 152],
			["middleName", "Отчество", 152],
			["email", "Электронная почта", 152],
			["phone", "Телефон", 152],
			["visitDate", "Планируемая дата визита", 152],
			["visitTime", "Планируемое время визита", 152],
			["vin", "Код VIN", 152],
			["manufacturedYear", "Год выпуска", 152],
			["modelId", "Идентификатор модели", 152],
			["regNumber", "Регистрационный номер", 152],
			["toServiceTypeId", "Идентификатор типа обслуживания", 152],
			["partnerId", "Идентификатор дилера", 152],
			["isOwner", "С сайта дилера", 152],
			["toUserActionName", "Во время прохождения ТО", 152],
			["registred", "Дата регистрации", 152],
			["statusId", "Идентификатор состояния заявки", 152],
			["statusName", "Состояние заявки", 152],
			["childrenPartners", "Подчиненные подразделения", 152],
			["mapImage", "Изображение карты", 152]
		]
	},

	modelsDefs: {
		partners: {
			type:								"base",
			fieldsDef: {
				partnerDivisionId: {
					title:						"partnerId",
					dataType:					"int"
				},
				partnerId: {
					title:						"partnerId",
					dataType:					"int"
				},
				partnerTypeId: {
					title:						"partnerTypeId",
					dataType:					"int"
				},
				partnerDivisionName: {
					title:						"partnerDivision",
					dataType:					"string"
				},
				partnerDivisionSite: {
					title:						"site",
					dataType:					"string"
				},
				cityId: {
					title:						"email",
					dataType:					"email"
				},
				gLn: {
					title:						"latitude",
					dataType:					"float"
				},
				gLg: {
					title:						"longitude",
					dataType:					"float"
				},
				partnerDivisionAddress: {
					title:						"address",
					dataType:					"string"
				},
				partnerDivisionPhone: {
					title:						"phone",
					dataType:					"phone"
				},
				partnerDivisionRCode: {
					title:						"RCode",
					dataType:					"string"
				},
				partnerDivisionStatus: {
					title:						"statusName",
					dataType:					"int"
				},
				partnerDivisionStatusName: {
					title:						"statusName",
					dataType:					"int"
				},
				federalDistrictId: {
					title:						"federalDistrict",
					dataType:					"string"
				},
				federalDistrictName: {
					title:						"federalDistrict",
					dataType:					"string"
				},
				partnerTypeName: {
					title:						"partnerType",
					dataType:					"string"
				},
				regionId: {
					title:						"regionId",
					dataType:					"string"
				},
				regionName: {
					title:						"region",
					dataType:					"string"
				},
				cityName: {
					title:						"city",
					dataType:					"string"
				},
				childrenPartners:	{
					title:						"childrenPartners",
					dataType:					"collection"
				},
				mapImage:	{
					title:						"mapImage",
					dataType:					"image"
				},
			},
			loadType:				"preload",
			keyFields: [
				"partnerDivisionId"
			],
			source: {
				url:							"/admin/partners/",
				controller:						"c",
				action:							"m"
			},
			actions: [
				{
					name:					"getFile",
					title:					"Выгрузить в csv-файл",
					category:				"collection",
					type:					"getFile",
					server: {
						url:					"/admin/partners/csv/",
						arguments:				[{
							type:					"exec",
							method:					"_currentView.filters.getFiltersValues"
						}]
					}
				},
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"partnersCreate",
					server: {
						url:					"/admin/partners/",
						controller:				"c",
						action:					"a"
					},
					result:	{
						message:				"Партнер {#partnerDivisionName#} добавлен",
						action:					"update",
						updateDate:				[
							"partners"
						]
					}
				},
				{
					name:					"edit",
					title:					"edit",
					category:				"object",
					type:					"form",
					view:					"partnersEdit",
					server: {
						url:					"/admin/partners/",
						controller:				"c",
						action:					"b"
					},
					result:	{
						message:				"Данные партнера {#partnerDivisionName#} сохранены",
						action:					"update",
						updateDate:				[
							"partners",
							"partnersHierarchy"
						]
					}
				}
			]
		},

		dealers: {
			type:								"prepared",
			import: {
				data:							"partners",
				filter:							{
					partnerTypeId:					{
						filter:							[1,6],
						dataType:						"int",
						by:								"or"
					}
				}
			},
			loadType:				"preload"
		},

		partnersHierarchy: {
			type:								"related",
			loadType:				"preload",
			fieldsDef: {
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				parentPartnerDivisionId: {
					title:						"parentPartnerDivisionId",
					dataType:					"int"
				}
			},
			keyFields: [
				"partnerDivisionId",
				"parentPartnerDivisionId"
			],
			source: {
				url:							"/admin/partners/",
				controller:						"c",
				action:							"n"
			}
		},

		slavePartnersTypes: {
			type:						"related",
			loadType:					"preload",
			fieldsDef: {
				partnerTypeId: {
					title:						"partnerTypeId",
					dataType:					"int"
				},
				slavePartnerTypeId: {
					title:						"slavePartnerTypeId",
					dataType:					"int"
				}
			},
			keyFields: [
				"partnerTypeId",
				"slavePartnerTypeId"
			],
			source: {
				url:							"/admin/partners/",
				controller:						"c",
				action:							"k"
			}
		},

		partnersTypes: {
			type:						"related",
			loadType:					"preload",
			fieldsDef: {
				partnerTypeId: {
					title:						"partnerTypeId",
					dataType:					"int"
				},
				partnerTypeName: {
					title:						"partnerTypeName",
					dataType:					"string"
				},
				partnerTypeDescription: {
					title:						"partnerTypeDescription",
					dataType:					"text"
				}
			},
			keyFields: [
				"partnerTypeId",
				"slavePartnerTypeId"
			],
			source: {
				url:							"/admin/partners/",
				controller:						"c",
				action:							"g"
			}
		},
		
		dealersFacilities: {
			type:								"matrix",
			fieldsDef: {
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				partnerDivisionFacilityStatus:  {
					title:						"modelId",
					dataType:					"int"
				}
			},
			matrix:					{
				rowSource:				{
					data:					"dealers",
					rowField:				"partnerDivisionId",
					dataField:				"partnerDivisionId",
					titleField:				"partnerDivisionName"
				},
				columnSource:				{
					data:					"facilities",
					colField:				"facilityId",
					dataField:				"facilityId",
					titleField:				"facilityName"
				},
				valueField:				{
					field:					"partnerDivisionFacilityStatus",
					edidable:				{
						mode:					"always",
						editedElement:			"checkbox",
						elementStateField:		"partnerDivisionFacilityStatus",
						elementName:			"partnerDivisionFacilityStatusAll"
					}
				},
				/*
				predefinedFields:		{
					facilityId:				50
				},
				*/
				useKeyFields: [
					"partnerDivisionId"
				]
			},
			keyFields: [
				"facilityId",
				"partnerDivisionId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/facilities/",
				controller:						"m",
				action:							"g",
				arguments:				{
					partnerDivisionModelStatus:				{
						type:					"value",
						data:					[1, 0]
					}
				}
			},
			actions: [
				{
					name:					"edidableTable",
					title:					"save",
					category:				"collection",
					type:					"edidableTable",
					view:					"facilitiesDealersCollection",
					server: {
						url:					"/admin/facilities/",
						controller:				"m",
						action:					"f"
					}
				}
			]
		}
	},




	ViewsDefs: {
		partnersCollection: {
			category:				"collection",
			data:					"partners",
			viewType:				"asTable",
			fields:		[
				{
					field:	"partnerDivisionName",
					ordered:	1
				},
				{
					field:	"partnerTypeName",
					ordered:	1
				},
				{
					field:	"partnerDivisionStatusName",
					ordered:	1
				},
				{
					field:	"federalDistrictName",
					ordered:	1
				},
				{
					field:	"regionName",
					ordered:	1
				},
				{
					field:	"cityName",
					ordered:	1
				},
				{
					field:	"partnerDivisionAddress"
				},
				{
					field:	"gLn",
					ordered:	1
				},
				{
					field:	"gLg",
					ordered:	1
				},
				{
					field:	"partnerDivisionSite",
					ordered:	1
				},
				{
					field:	"partnerDivisionPhone"
				},

				{
					field:	"partnerDivisionRCode",
					ordered:	1
				},
				{
					field:	"mapImage"
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"federalDistrictId",
						fieldTitle:		"federalDistrictName",
						title:			"federalDistrictName",
						type:			"select",
						idField:		"federalDistrictId",
						titleField:		"federalDistrictName",
						order:			{
							fieldName:	"federalDistrictName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"cityId",
						fieldTitle:		"cityName",
						title:			"cityName",
						type:			"select",
						idField:		"cityId",
						titleField:		"cityName",
						order:			{
							fieldName:	"cityName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"partnerTypeId",
						fieldTitle:		"partnerTypeName",
						title:			"partnerTypeName",
						type:			"select",
						idField:		"partnerTypeId",
						titleField:		"partnerTypeName",
						order:			{
							fieldName:	"partnerTypeName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"partnerDivisionStatus",
						fieldTitle:		"partnerDivisionStatusName",
						title:			"statusName",
						type:			"select",
						idField:		"partnerDivisionStatus",
						titleField:		"partnerDivisionStatusName",
						order:			{
							fieldName:	"partnerDivisionStatusName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100
			}
		},
		partnersEdit:	{
			category:				"object",
			data:					"partners",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s4a1e.html",
				elementSelector:		"#s4a1e_partnersEdit"
			},
			fields:					[
				{
					field:				"partnerDivisionId"
				},
				{
					field:				"partnerDivisionName"
				},
				{
					field:				"partnerDivisionSite"
				},
				{
					field:	"mapImage"
				},
				{
					field:				"partnerTypeId",
					type:				"select",
					data:				"partnersTypes",
					idFields:			"partnerTypeId",
					titleFields:		"partnerTypeName"
				},
				{
					field:				"partnerId",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"slavePartnersTypes",
						relFields:				"partnerTypeId",
						relFieldsId:			"partnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"partnerTypeId",
							relFieldsId:			"partnerTypeId",
							field:					"slavePartnerTypeId"
						}
					}
				},
				{
					field:				"partnerDivisionAddress",
					ordered:			1
				},
				{
					field:				"gLn",
					ordered:			1
				},
				{
					field:				"gLg",
					ordered:			1
				},
				{
					field:				"partnerDivisionPhone",
					ordered:			1
				},
				{
					field:				"partnerDivisionRCode",
					ordered:			1
				},

				{
					field:				"childrenPartners",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"slavePartnersTypes",
						relFields:				"slavePartnerTypeId",
						relFieldsId:			"slavePartnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"partnerTypeId",
							relFieldsId:			"partnerTypeId",
							field:					"partnerTypeId"
						}
					},
					currentValues:		{
						data:					"partnersHierarchy",
						field:					"partnerDivisionId",
						relFields:				"partnerDivisionId",
						relate:				{
							data:					"_current",
							relFields:				"partnerDivisionId",
							relFieldsId:			"partnerDivisionId",
							field:					"parentPartnerDivisionId"
						}
					}
				}
			],
			params: {
				mode: 2
			}
		},

		partnersCreate:	{
			category:				"object",
			data:					"partners",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s4a1c.html",
				elementSelector:		"#s4a1c_partnersCreate"
			},
			fields:					[
				{
					field:				"partnerDivisionId"
				},
				{
					field:				"partnerDivisionName"
				},
				{
					field:				"partnerDivisionSite"
				},
				{
					field:				"partnerTypeId",
					type:				"select",
					data:				"partnersTypes",
					idFields:			"partnerTypeId",
					titleFields:		"partnerTypeName"
				},
				{
					field:				"mapImage"
				},
				{
					field:				"partnerId",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"slavePartnersTypes",
						relFields:				"partnerTypeId",
						relFieldsId:			"partnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"partnerTypeId",
							relFieldsId:			"partnerTypeId",
							field:					"slavePartnerTypeId"
						}
					}
				},

				{
					field:				"regionId",
					type:				"select",
					data:				"regions",
					idFields:			"regionId",
					titleFields:		"regionName"
				},
				{
					field:				"cityId",
					type:				"select",
					data:				"cities",
					idFields:			"cityId",
					titleFields:		"cityName",
					relate:				{
						data:					"_current",
						relFields:				"regionId",
						relFieldsId:			"regionId",
						field:					"regionId"
					}
				},

				{
					field:				"partnerDivisionAddress",
					ordered:			1
				},
				{
					field:				"gLn",
					ordered:			1
				},
				{
					field:				"gLg",
					ordered:			1
				},
				{
					field:				"partnerDivisionPhone",
					ordered:			1
				},
				{
					field:				"partnerDivisionRCode",
					ordered:			1
				},

				{
					field:				"childrenPartners",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"slavePartnersTypes",
						relFields:				"slavePartnerTypeId",
						relFieldsId:			"slavePartnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"partnerTypeId",
							relFieldsId:			"partnerTypeId",
							field:					"partnerTypeId"
						}
					}
				}
			],
			params: {
				mode: 1
			}
		},

		childrenPartnersEdit:	{
			category:				"object",
			data:					"partners",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s4a1childrenPartnersEdit.html",
				elementSelector:		"#s4a1_childrenPartnersEdit"
			},
			fields:					[
				{
					field:				"partnerDivisionId"
				},
				{
					field:				"childrenPartners",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"slavePartnersTypes",
						relFields:				"slavePartnerTypeId",
						relFieldsId:			"slavePartnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"partnerTypeId",
							relFieldsId:			"partnerTypeId",
							field:					"partnerTypeId"
						}
					},
					currentValues:		{
						data:					"partnersHierarchy",
						field:					"partnerDivisionId",
						relFields:				"partnerDivisionId",
						relate:				{
							data:					"_current",
							relFields:				"partnerDivisionId",
							relFieldsId:			"partnerDivisionId",
							field:					"parentPartnerDivisionId"
						}
					}
				}
			],
			params: {
				mode: 2
			}
		},
		dealersFacilitiesTables: {
			category:				"collection",
			viewType:				"asTable",
			data:					"dealersFacilities",
			filters:	{
				type:			"static",
				fields: []
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
				cellMode:		"edidable",
				buildMode:		"matrix",
				edidableMode:	"always"
			}
		}
	},

	UsersEvents: {},

	ActionsDefs: {
		21: {
			action:					21,
			views:					["partnersCollection"],
			defaultView:			"partnersCollection"
		},
		22: {
			action:					22,
			views:					["dealersFacilitiesTables"],
			defaultView:			"dealersFacilitiesTables"
		}
	}
}