a = {
	service:				23,
	links: {
		4: {
			dataModels: ["partners"]
		}
	},
	init: {
		messages: [
			["userActionId", "Идентификатор пользовательского действия", 152],
			["lastName", "Фамилия", 152],
			["firstName", "Имя", 152],
			["middleName", "Отчество", 152],
			["email", "Электронная почта", 152],
			["phone", "Телефон", 152],
			["visitDate", "Планируемая дата визита", 152],
			["visitTime", "Планируемое время визита", 152],
			["vin", "Код VIN", 152],
			["manufacturedYear", "Год выпуска", 152],
			["curretnModelId", "Идентификатор модели пользователя", 152],
			["regNumber", "Регистрационный номер", 152],
			["toServiceTypeId", "Идентификатор типа обслуживания", 152],
			["partnerId", "Идентификатор дилера", 152],
			["isOwner", "С сайта дилера", 152],
			["toUserActionName", "Во время прохождения ТО", 152],
			["registred", "Дата регистрации", 152],
			["statusId", "Идентификатор состояния заявки", 152],
			["statusName", "Состояние заявки", 152],
			["models", "Автомобили", 152],
			["dealersActionName", "Название", 152],
			["dealersActionStatusName", "Состояние", 152],
			["dealersActionMileageMin", "Минимальный пробег", 152],
			["dealersActionMileageMax", "Максимальный пробег", 152],
			["dealersActionYearMin", "Минимальный год производства", 152],
			["dealersActionYearMax", "Максимальный год производства", 152],
			["dealersActionStart", "Дата начала", 152],
			["dealersActionEnd", "Дата завершения", 152],
			["dealersActionCreate", "Дата создания", 152],
			["partnerName", "Дилер", 152],
			["usersActionName", "Действие клиента", 152],
			["file", "Файл", 152],
			["ErrFileFLimit", "Превышен максимальный размер файла", 152],
			["referrer", "Получено с сайта", 152],
			["TOSpatialCapmaign", "Специальные сервисные кампании", 152],
			["serviceTypeName", "Тип обслуживания", 152],
			["advancedInfo", "Дополнительная информация", 152]
		],
		"inclideCSS": [
			["http://content.toyota.ru/css/s23.css"]
		]
	},
	modelsDefs: {
		toMember: {
			fieldsDef: {
				memberId: {
					title:						"id",
					dataType:					"int"
				},
				usersActionId: {
					title:						"userActionId",
					dataType:					"int"
				},
				lastName: {
					title:						"lastName",
					dataType:					"lastName"
				},
				firstName: {
					title:						"firstName",
					dataType:					"firstName"
				},
				middleName: {
					title:						"middleName",
					dataType:					"firstName"
				},
				advancedInfo: {
					title:						"advancedInfo",
					dataType:					"text"
				},
				email: {
					title:						"email",
					dataType:					"email"
				},
				phone: {
					title:						"phone",
					dataType:					"phone"
				},
				visitDate: {
					title:						"visitDate",
					dataType:					"date"
				},
				visitTime: {
					title:						"visitTime",
					dataType:					"time"
				},
				vin: {
					title:						"vin",
					dataType:					"vin"
				},
				year: {
					name:						"year",
					title:						"manufacturedYear",
					dataType:					"year"
				},
				modelId: {
					name:						"modelId",
					title:						"Идентификатор модели",
					dataType:					"int"
				},
				modelName: {
					name:						"modelName",
					title:						"Модель",
					dataType:					"string"
				},
				regNumber: {
					title:						"regNumber",
					dataType:					"regNumber"
				},
				serviceTypeId: {
					title:						"Идентификатор типа обслуживания",
					dataType:					"int"
				},
				serviceTypeName: {
					title:						"serviceTypeName",
					dataType:					"string"
				},
				partnerId: {
					title:						"partnerId",
					dataType:					"int"
				},
				partnerName: {
					title:						"Дилер",
					dataType:					"string"
				},
				isOwner: {
					title:						"isOwner",
					dataType:					"bool"
				},
				userActionName: {
					title:						"toUserActionName",
					dataType:					"string"
				},
				registred: {
					title:						"registred",
					dataType:					"datetime"
				},
				statusId: {
					title:						"statusId",
					dataType:					"int"
				},
				statusName: {
					title:						"Состояние заявки",
					dataType:					"string"
				},
				visitDateU: {
					title:						"visitDate",
					dataType:					"date"
				},
				registredU: {
					title:						"registred",
					dataType:					"datetime"
				},
				commentsCount: {
					title:						"Комментариев дилера",
					dataType:					"int"
				},
				pollingAnswerComment: {
					title:						"Дата визита",
					dataType:					"string"
				},
				pollingAnswerCreate: {
					title:						"Дата ответа клиента",
					dataType:					"date"
				},
				pollingAnswerName: {
					title:						"Ответ клиента",
					dataType:					"string"
				},
				pollingAnswer: {
					title:						"Ответ клиента",
					dataType:					"string"
				},
				isToCampaigns: {
					title:						"TOSpatialCapmaign",
					dataType:					"string"
				},
				referrer: {
					title:						"referrer",
					dataType:					"string"
				},
			},
			keyFields: [
									"memberId"
			],
			source:	{
				url:					"/admin/techservice/",
				controller:				"v",
				action:					"l",
			},
			loadType:				"preload",
			actions: [
				{
					name:					"getFile",
					title:					"Отчет о заявках",
					category:				"collection",
					type:					"getFile",
					server: {
						url:					"/admin/techservice/members/csv/",
						arguments:				[{
							type:					"exec",
							method:					"_currentView.filters.getFiltersValues"
						}]
					}
				},
				{
					name:					"view",
					title:					"Посмотреть",
					category:				"object",
					type:					"view",
					view:					"toMemberView",
					execMethod:				"selected",
					customAction:			"editUsersAction",
					buttonTemplate:			'<a class="edit_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" href="javascript:void(0);">'
												+ '<span class="ui-button-icon-primary ui-icon ui-icon-pencil"></span>'
												+ '<span class="ui-button-text"> Редактировать</span>'
											+ '</a>',
					exec: {
						url:					"http://content.toyota.ru/js/bstd/execs/s23a2n.js",
						className:				"s23a2_exec",
						method:					"memberView"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить запись<br />{#usersActionName#}"
											},
											{
												type:	"value",
												value:	"Удаление записи"
											},
											{
												type:	"currentAction",
											}
										]
					},
					conditions: {
						permission: {
							service:						23,
							value:							16384
						}
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"a6"
					}
				}
			]
		},
		
		s23_statuses: {
			fieldsDef: {
				statusId: {
					title:						"id",
					dataType:					"int"
				},
				statusName: {
					title:						"s23_statuses_statusName",
					dataType:					"string"
				},
				statusDescription: {
					title:						"s23_statuses_statusDescription",
					dataType:					"text"
				}
			},
			keyFields: [
				"statusId"
			],
			source:	{
				url:						"/admin/techservice/",
				controller:					"v",
				action:						"n"
			},
			loadType:				"preload"
		},
		
		
		toPolling: {
			commentsCount: {
				name:						"commentsCount",
				title:						"Комментариев дилера",
				dataType:					"",
				validator:					"name",
				myltiValues:				0,
				required:					1,
				edidable:					1,
				isString:					1
			},
			pollingAnswerComment: {
				name:						"pollingAnswerComment",
				title:						"Дата визита",
				dataType:					"",
				validator:					"name",
				myltiValues:				0,
				required:					1,
				edidable:					1,
				isString:					1
			},
			pollingAnswerCreate: {
				name:						"pollingAnswerCreate",
				title:						"Дата визита",
				dataType:					"",
				validator:					"name",
				myltiValues:				0,
				required:					1,
				edidable:					1,
				isString:					1
			},
			pollingAnswerName: {
				name:						"pollingAnswerName",
				title:						"Дата визита",
				dataType:					"",
				validator:					"name",
				myltiValues:				0,
				required:					1,
				edidable:					1,
				isString:					1
			},
			pollingAnswer: {
				name:						"pollingAnswer",
				title:						"Ответ клиента",
				dataType:					"",
				validator:					"name",
				myltiValues:				0,
				required:					1,
				edidable:					1,
				isString:					1
			}
		},
		
		
		
		toDealersActionsModels: {
			fieldsDef: {
				dealersActionId: {
					title:						"id",
					dataType:					"int"
				},
				modelId: {
					title:						"dealersActionName",
					dataType:					"int"
				},
				modelName: {
					title:						"modelName",
					dataType:					"string"
				}
			},
			keyFields: [
				"dealersActionId",
				"modelId"
			],
			source:	{
				url:					"/admin/techservice/",
				controller:				"v",
				action:					"v"
			},
			loadType:				"preload"
		},
		
		toDealersActions: {
			fieldsDef: {
				dealersActionId: {
					title:						"id",
					dataType:					"int"
				},
				dealersActionName: {
					title:						"dealersActionName",
					dataType:					"int"
				},
				partnersDivisionId: {
					title:						"partnerId",
					dataType:					"int"
				},
				brandId: {
					title:						"brandId",
					dataType:					"int"
				},
				dealersActionStatus: {
					title:						"dealersActionStatus",
					dataType:					"int"
				},
				dealersActionStatusName: {
					title:						"dealersActionStatusName",
					dataType:					"string"
				},
				dealersActionFileName: {
					title:						"dealersActionFileName",
					dataType:					"string"
				},
				dealersActionMileageMin: {
					title:						"dealersActionMileageMin",
					dataType:					"mileage"
				},
				dealersActionMileageMax: {
					title:						"dealersActionMileageMax",
					dataType:					"mileage"
				},
				dealersActionYearMin: {
					title:						"dealersActionYearMin",
					dataType:					"year"
				},
				dealersActionYearMax: {
					title:						"dealersActionYearMax",
					dataType:					"year"
				},
				dealersActionCreate: {
					title:						"dealersActionCreate",
					dataType:					"dateTime"
				},
				dealersActionStart: {
					title:						"dealersActionStart",
					dataType:					"date"
				},
				dealersActionEnd: {
					title:						"dealersActionEnd",
					dataType:					"date"
				},
				partnersDivisionName: {
					title:						"partnerName",
					dataType:					"string"
				},
				dealersActionOriginFileName: {
					title:						"file",
					dataType:					"string"
				},
				dealersActionModels:	 {
					title:						"models",
					dataType:					"collection",
					relate:						{
						data:						"toDealersActionsModels",
						relFields:					"dealersActionId",
						relFieldsId:				"modelId",
						field:						"dealersActionId",
						substitute:					"modelName",
						loadType:					"preload"
					}
				},
			},
			keyFields: [
				"dealersActionId"
			],
			source:	{
				url:					"/admin/techservice/",
				controller:				"v",
				action:					"u",
			},
			loadType:				"preload",
			actions: [
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"toDealersActionsAdd",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"s"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить запись<br />{#usersActionName#}"
											},
											{
												type:	"value",
												value:	"Удаление записи"
											},
											{
												type:	"currentAction",
											}
										]
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"w"
					}
				}
			]
		},
		
		toModels: {
			type:								"base",
			fieldsDef: {
				modelId: {
					title:						"modelId",
					dataType:					"int"
				},
				modelName: {
					title:						"modelName",
					dataType:					"string"
				},
				brandId: {
					title:						"brandId",
					dataType:					"int"
				}
			},
			keyFields: [
				"modelId"
			],
			loadType:						"preload",
			source: {
				url:							"/admin/techservice/",
				controller:						"v",
				action:							"x",
			}
		},
		
		toEmails: {
			type:								"base",
			fieldsDef: {
				partnerDivisionId: {
					title:						"partnerId",
					dataType:					"int"
				},
				partnerDivisionEmail: {
					title:						"email",
					dataType:					"email"
				},
				partnerDivisionName: {
					title:						"partnerName",
					dataType:					"string"
				},
			},
			keyFields: [
				"partnerDivisionId",
				"partnerDivisionEmail"
			],
			loadType:						"preload",
			source: {
				url:							"/admin/techservice/",
				controller:						"v",
				action:							"y",
			},
			actions: [
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"toEmailAdd",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"z"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить запись<br />{#partnerDivisionEmail#}"
											},
											{
												type:	"value",
												value:	"Удаление записи"
											},
											{
												type:	"currentAction",
											}
										]
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"a1"
					}
				}
			]
		},
		
		toUsersAction: {
			type:								"base",
			fieldsDef: {
				usersActionId: {
					title:						"usersActionId",
					dataType:					"int"
				},
				partnerId: {
					title:						"partnerId",
					dataType:					"int"
				},
				usersActionName: {
					title:						"usersActionName",
					dataType:					"string"
				},
				usersActionDescription: {
					title:						"usersActionDescription",
					dataType:					"text"
				},
				partnerName: {
					title:						"partnerName",
					dataType:					"string"
				},
			},
			keyFields: [
				"usersActionId"
			],
			loadType:						"preload",
			source: {
				url:							"/admin/techservice/",
				controller:						"v",
				action:							"e3",
			},
			actions: [
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"toUsersActionAdd",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"h"
					}
				},
				{
					name:					"edit",
					title:					"edit",
					category:				"object",
					type:					"form",
					view:					"toUsersActionEdit",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"i"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить запись<br />{#usersActionName#} для дилера {#partnerName#}"
											},
											{
												type:	"value",
												value:	"Удаление записи"
											},
											{
												type:	"currentAction",
											}
										]
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"j"
					}
				}
			]
		},
		toSpatialCampaigns:	{
			type:								"base",
			fieldsDef: {
				scCampaignId: {
					title:						"id",
					dataType:					"int"
				},
				scCampaignExtId: {
					title:						"scCampaignExtId",
					dataType:					"int"
				},
				brandId: {
					title:						"brandId",
					dataType:					"int"
				},
				scCampaignName: {
					title:						"name",
					dataType:					"string"
				},
				scCampaignDescription: {
					title:						"description",
					dataType:					"text"
				},
				scCampaignStatus: {
					title:						"statusId",
					dataType:					"int"
				},
				scCampaignStatusName: {
					title:						"statusName",
					dataType:					"string"
				},
			},
			keyFields: [
				"scCampaignId"
			],
			loadType:						"preload",
			source: {
				url:							"/admin/techservice/",
				controller:						"v",
				action:							"a5",
			},
			actions: [
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"toSpatialCampaignsAdd",
					server: {
						url:					"/admin/techservice/",
						controller:				"v",
						action:					"a3"
					}
				},
				{
					name:					"remove",
					title:					"delete",
					category:				"object",
					beforeAction:	{
						method:			"bstdConfirm",
						arguments:		[
											{
												type:	"template",
												value:	"Вы действительно хотите удалить кампанию<br />{#scCampaignName#}?"
											},
											{
												type:	"value",
												value:	"Удаление кампании"
											},
											{
												type:	"currentAction",
											}
										]
					},
					server: {
						url:							"/admin/techservice/",
						controller:						"v",
						action:							"a5"
					}
				}
			]
		},
	},
		
		
		
		
		
	ViewsDefs: {
		toMemberCollections: {
			category:				"collection",
			data:					"toMember",
			viewType:				"asTable",
			fields:					[
				{
					field:	"commentsCount",
					ordered:	1
				},
				{
					field:	"isToCampaigns",
					ordered:	1
				},
				{
					field:	"statusName",
					ordered:	1
				},
				{
					field:	"partnerName",
					ordered:	1
				},
				{
					field:	"lastName",
					ordered:	1
				},
				{
					field:	"firstName",
					ordered:	1
				},
				{
					field:	"middleName",
					ordered:	1
				},
				{
					field:	"email",
					ordered:	1
				},
				{
					field:	"phone",
					ordered:	1
				},
				{
					field:	"visitDate",
					ordered:	1
				},
				{
					field:	"visitTime",
					ordered:	1
				},
				{
					field:	"vin",
					ordered:	0
				},
				{
					field:	"year",
					ordered:	1
				},
				{
					field:	"modelName",
					ordered:	1
				},
				{
					field:	"regNumber",
					ordered:	1
				},
				{
					field:	"serviceTypeName",
					ordered:	1
				},
				{
					field:	"advancedInfo",
					ordered:	0
				},
				{
					field:	"isOwner",
					ordered:	1
				},
				{
					field:	"userActionName",
					ordered:	1
				},
				{
					field:	"registred",
					ordered:	1
				},
				{
					field:	"pollingAnswer",
					ordered:	0
				},
				{
					field:	"referrer",
					ordered:	1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"partnerId",
						fieldTitle:		"partnerName",
						title:			"Дилерский центр",
						type:			"select",
						idField:		"partnerId",
						titleField:		"partnerName",
						dataType:		"int",
						order:			{
							fieldName:	"partnerName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"modelId",
						fieldTitle:		"modelName",
						title:			"Модель",
						type:			"select",
						idField:		"modelId",
						titleField:		"modelName",
						order:			{
							fieldName:	"modelName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"registred",
						fieldTitle:		"registred",
						title:			"Дата регистрации, от",
						idField:		"registred",
						titleField:		"registred",
						type:			"Datepicker",
						range:			"min",		//min, max, both
						idField:		"registred",
						outRange:		0,
						convert:		1
					},
					{
						field:			"statusId",
						fieldTitle:		"statusName",
						title:			"Состояние заявки",
						type:			"select",
						idField:		"statusId",
						titleField:		"statusName",
						dataType:		"int",
						order:			{
							fieldName:	"statusName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
			},
		},
		toMemberView: {
			category:				"object",
			data:					"toMember",
			viewType:				"panel",
			fields:					[
				{
					field:			"usersActionId",
					substitute:		1
				},
				{
					field:			"lastName"
				},
				{
					field:			"firstName"
				},
				{
					field:			"middleName"
				},
				{
					field:			"email"
				},
				{
					field:			"phone"
				},
				{
					field:			"visitDate"
				},
				{
					field:			"visitTime"
				},
				{
					field:			"advancedInfo"
				},
				{
					field:			"vin"
				},
				{
					field:			"year"
				},
				{
					field:			"modelId",
					substitute:		1
				},
				{
					field:			"regNumber"
				},
				{
					field:			"serviceTypeId",
					substitute:		1
				},
				{
					field:			"partnerId",
					substitute:		1
				},
				{
					field:			"isOwner"
				},
				{
					field:			"userActionName"
				},
				{
					field:			"statusId",
					substitute:		1
				},
				{
					field:			"visitDateU"
				},
				{
					field:			"registredU"
				},
				{
					field:			"isOwner"
				}
			]
		},
		toDealersActionsAdd:	{
			category:				"object",
			data:					"toDealersActions",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:				"s23a8.html",
				elementSelector: 	"#s23a8_dealersActionsForm"
			},
			fields:					[
				{
					field:	"dealersActionName",
					ordered:	1
				},
				{
					field:	"dealersActionStatusName",
					ordered:	1
				},
				{
					field:	"dealersActionMileageMin",
					ordered:	1
				},
				{
					field:	"dealersActionMileageMax",
					ordered:	1
				},
				{
					field:	"dealersActionYearMin",
					ordered:	1
				},
				{
					field:	"dealersActionYearMax",
					ordered:	1
				},
				{
					field:	"dealersActionStart",
					ordered:	1
				},
				{
					field:	"dealersActionEnd",
					ordered:	1
				},
				{
					field:	"dealersActionCreate",
					ordered:	1
				},
				{
					field:	"partnersDivisionName",
					ordered:	1
				},
				{
					field:	"dealersActionModels",
					ordered:	1,
					data:	"toModels"
				}
			],
			params: {
				mode: 1
			}
		},
		toDealersAction: {
			category:				"object",
			data:					"toDealersActions",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:				"s23a9.html",
				elementSelector: 	"#s23a9_dealersActionsForm"
			},
			fields:					[
				{
					field:	"dealersActionName",
					ordered:	1
				},
				{
					field:	"dealersActionStatusName",
					ordered:	1
				},
				{
					field:	"dealersActionMileageMin",
					ordered:	1
				},
				{
					field:	"dealersActionMileageMax",
					ordered:	1
				},
				{
					field:	"dealersActionYearMin",
					ordered:	1
				},
				{
					field:	"dealersActionYearMax",
					ordered:	1
				},
				{
					field:	"dealersActionStart",
					ordered:	1
				},
				{
					field:	"dealersActionEnd",
					ordered:	1
				},
				{
					field:	"dealersActionCreate",
					ordered:	1
				},
				{
					field:	"partnersDivisionName",
					ordered:	1
				},
				{
					field:	"dealersActionModels",
					ordered:	1,
					data:	"toModels"
				}
			]
		},
		toSpatialCampaignsAdd:	{
			category:				"object",
			data:					"toSpatialCampaigns",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s23a19.html",
				elementSelector: 		"#s23a19_addTOSpatialCampaigns"
			},
			fields:					[
				{
					field:	"scCampaignId",
					ordered:	1
				},
				{
					field:	"scCampaignName",
					ordered:	1
				},
				{
					field:	"scCampaignDescription",
					ordered:	1
				}
			],
			params: {
				mode: 1
			}
		},
		toSpatialCampaignsCollection: {
			category:				"collection",
			data:					"toSpatialCampaigns",
			viewType:				"asTable",
			fields:					[
				{
					field:	"scCampaignExtId",
					ordered:	1
				},
				{
					field:	"scCampaignName",
					ordered:	1
				},
				{
					field:	"scCampaignDescription",
					ordered:	1
				},
				{
					field:	"scCampaignStatusName",
					ordered:	1
				}
			],
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
			}
		},


		toDealersActionsCollections: {
			category:				"collection",
			data:					"toDealersActions",
			viewType:				"asTable",
			fields:					[
				{
					field:	"dealersActionName",
					ordered:	1
				},
				{
					field:	"dealersActionStatusName",
					ordered:	1
				},
				{
					field:	"dealersActionModels",
					ordered:	1
				},
				{
					field:	"dealersActionMileageMin",
					ordered:	1
				},
				{
					field:	"dealersActionMileageMax",
					ordered:	1
				},
				{
					field:	"dealersActionYearMin",
					ordered:	1
				},
				{
					field:	"dealersActionYearMax",
					ordered:	1
				},
				{
					field:	"dealersActionStart",
					ordered:	1
				},
				{
					field:	"dealersActionEnd",
					ordered:	1
				},
				{
					field:	"dealersActionCreate",
					ordered:	1
				},
				{
					field:	"partnersDivisionName",
					ordered:	1
				},
				{
					field:	"dealersActionOriginFileName",
					ordered:	0,
					tempalte:	"<a target=\"_blank\" href=\"/to_dealers_actions_files/{#dealersActionFileName#}\">{#dealersActionOriginFileName#}</a>"
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"partnersDivisionId",
						fieldTitle:		"partnersDivisionName",
						title:			"Дилерский центр",
						type:			"select",
						idField:		"partnersDivisionId",
						titleField:		"partnersDivisionName",
						dataType:		"int",
						order:			{
							fieldName:	"partnersDivisionName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
			},
		},
		
		toEmailAdd:	{
			category:				"object",
			data:					"toEmails",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s23a9.html",
				elementSelector: 		"#s23a9_addTOEmails"
			},
			fields:					[
				{
					field:				"partnerDivisionId",
					ordered:			"1",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName"
				},
				{
					field:	"partnerDivisionEmail",
					ordered:	1
				}
			],
			params: {
				mode: 1
			}
		},
		toEmail: {
			category:				"collection",
			data:					"toEmails",
			viewType:				"asTable",
			fields:					[
				{
					field:	"partnerDivisionName",
					ordered:	1
				},
				{
					field:	"partnerDivisionEmail",
					ordered:	1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"partnerDivisionId",
						fieldTitle:		"partnerDivisionName",
						title:			"Дилерский центр",
						type:			"select",
						idField:		"partnerDivisionId",
						titleField:		"partnerDivisionName",
						dataType:		"int",
						order:			{
							fieldName:	"partnerDivisionName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
			}
		},
		
		
		toUsersActionCollections:  {
			category:				"collection",
			data:					"toUsersAction",
			viewType:				"asTable",
			fields:					[
				{
					field:	"partnerName",
					ordered:	1
				},
				{
					field:	"usersActionName",
					ordered:	1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"partnerId",
						fieldTitle:		"partnerName",
						title:			"Дилерский центр",
						type:			"select",
						idField:		"partnerId",
						titleField:		"partnerName",
						dataType:		"int",
						order:			{
							fieldName:	"partnerName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
			},
		},
		toUsersActionAdd:	{
			category:				"object",
			data:					"toUsersAction",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s23a1.html",
				elementSelector: 		"#s23a1_usersActionsForm"
			},
			fields:					[
				{
					field:				"partnerId",
					ordered:			"1",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName"
				},
				{
					field:	"usersActionName",
					ordered:	1
				}
			],
			params: {
				mode: 1
			}
		},
		toUsersActionEdit:	{
			category:				"object",
			data:					"toUsersAction",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s23a1e.html",
				elementSelector: 		"#s23a1e_usersActionsForm"
			},
			fields:					[
				{
					field:				"partnerId",
					ordered:			"1",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName"
				},
				{
					field:	"usersActionName",
					ordered:	1
				},
				{
					field:	"usersActionId",
					ordered:	1
				}
			],
			params: {
				mode: 2
			}
		},
	},

	UsersEvents:	{
		"1": {
			template:				"<div class=\"service-alias\">TO</div><div class=\"event-name\">Заявка на ТО</div><div class=\"event-detail\">Клиент: {#firstName#} {#middleName#} {#lastName#}</div><div class=\"event-detail\">Дата: {#visitDate#}, время: {#visitTime#}</div><div class=\"event-detail\">Модель: {#modelName#}</div>",
			data:					"toMember",
			actionType:				"add",
			view:					"toMemberCollections"
		},
		"2": {
			template:				"<div class=\"service-alias\">TO</div><div class=\"event-name\">Изменен статус заявки</div><div class=\"event-detail\">Клиент: {#firstName#} {#middleName#} {#lastName#}</div><div class=\"event-detail\">Дата изменения: {#statusChangeDate#}</div><div class=\"event-detail\">Статус изменил: {#userFirstName#} {#userLastName#}</div>",
			data:					"toMember",
			actionType:				"edit",
			view:					"toMemberCollections"
		},
		"3": {
			template:				"<div class=\"service-alias\">TO</div><div class=\"event-name\">Получен отзыв клиента</div><div class=\"event-detail\">Клиент: {#firstName#} {#middleName#} {#lastName#}</div><div class=\"event-detail\">Дата: {#statusChangeDate#}</div>",
			view:					"toMemberCollections",
			actionType:				"edit",
			view:					"toMemberCollections"
		},
		"4": {
			data:					"toDealersActions",
			actionType:				"add"
		},
		"5": {
			data:					"toDealersActions",
			actionType:				"edit"
		},
		"6": {
			data:					"toDealersActions",
			actionType:				"delete"
		},
		"7": {
			data:					"toDealersActions",
			actionType:				"delete"
		},
		"10": {
			data:					"toMember",
			actionType:				"edit"
		},
		"11": {
			data:					"toUsersAction",
			actionType:				"add"
		},
		"12": {
			data:					"toUsersAction",
			actionType:				"edit"
		},
		"13": {
			data:					"toUsersAction",
			actionType:				"delete"
		},
		"14": {
			data:					"toEmails",
			actionType:				"add"
		},
		"15": {
			data:					"toEmails",
			actionType:				"edit"
		},
		"16": {
			data:					"toEmails",
			actionType:				"delete"
		}
	},
	
	
	
	ActionsDefs: {
		1: {
			action:					1,
			views:					["toUsersActionCollections"],
			defaultView:			"toUsersActionCollections",
		},
		2: {
			action:					2,
			views:					["toMemberCollections"],
			defaultView:			"toMemberCollections"
		},
		8: {
			action:					8,
			views:					["toDealersActionsCollections"],
			defaultView:			"toDealersActionsCollections",
		},
		9: {
			action:					9,
			views:					["toEmail"],
			defaultView:			"toEmail"
		},
		19: {
			action:					19,
			views:					["toSpatialCampaignsCollection"],
			defaultView:			"toSpatialCampaignsCollection"
		},
	}
}