a = {
	service:				16,
	init: {
		messages: [
			["facilityId", "Идентификатор возможноти", 152],
			["facilityName", "Возможность", 152],
			["facilityDescription", "Описание", 152],
			["save", "Сохранить", 152],
			["DataSaveSuccess", "Данные успешно сохранены", 152]
		]
	},
	modelsDefs: {
		facilities: {
			fieldsDef: {
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				facilityName: {
					title:						"facilityName",
					dataType:					"string"
				},
				facilityDescription: {
					title:						"facilityDescription",
					dataType:					"int"
				}
			},
			keyFields: [
									"facilityId",
									"partnerDivisionId",
									"formType"
			],
			source:	{
				url:					"/admin/facilities/",
				//controller:				"ai",
				action:					"a0"
			},
			loadType:				"preload"
			/*
			actions: [
				{
					name:					"edit",
					title:					"edit",
					category:				"object",
					type:					"form",
					view:					"facilitiesFormsDealersSetsEdit",
					server: {
						url:					"/admin/facilities/forms/",
						controller:				"ai",
						action:					"c"
					}
				},
			]
			*/
		},
		
		/*
		facilitiesDealers: {
			type:								"matrix",
			fieldsDef: {
				facilityId: {
					title:						"facilityId",
					dataType:					"int"
				},
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				partnerDivisionFacilityStatus:  {
					title:						"modelId",
					dataType:					"int"
				}
			},
			matrix:					{
				rowSource:				{
					data:					"dealers",
					rowField:				"partnerDivisionId",
					dataField:				"partnerDivisionId",
					titleField:				"partnerDivisionName"
				},
				columnSource:				{
					data:					"facilities",
					colField:				"facilityId",
					dataField:				"facilityId",
					titleField:				"facilityName"
				},
				valueField:				{
					field:					"partnerDivisionModelStatus",
					edidable:				{
						mode:					"always",
						editedElement:			"checkbox",
						elementStateField:		"partnerDivisionFacilityStatus",
						elementName:			"partnerDivisionFacilityStatusAll"
					}
				},

				predefinedFields:		{
					facilityId:				50
				},

				useKeyFields: [
					"facilityId",
					"partnerDivisionId"
				],
			},
			keyFields: [
				"facilityId",
				"partnerDivisionId"
			],
			loadType:				"preload",
			source: {
				url:							"/admin/facilities/",
				controller:						"m",
				action:							"g",
				arguments:				{
					partnerDivisionModelStatus:				{
						type:					"value",
						data:					[1, 0]
					}
				}
			},
			actions: [
				{
					name:					"edidableTable",
					title:					"save",
					category:				"collection",
					type:					"edidableTable",
					view:					"facilitiesDealersCollection",
					server: {
						url:					"/admin/facilities/",
						controller:				"m",
						action:					"f"
					}
				}
			]
		}
*/
	},

	ViewsDefs: {
		facilitiesDealersCollection: {
			category:				"collection",
			viewType:				"asTable",
			data:					"facilitiesDealers",
			filters:	{
				type:			"static",
				fields: []
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100,
				cellMode:		"edidable",
				buildMode:		"matrix",
				edidableMode:	"always"
			}
		}
	},
/*
	UsersEvents:	{
		"22": {
			template: "",
			data: "toMember",
			actionType: "edit"
		}
	},
*/
	ActionsDefs: {
		16: {
			action:					16,
			views:					["facilitiesDealersCollection"],
			defaultView:			"facilitiesDealersCollection"
		},
		"default": {
			action:					16,
			views:					["facilitiesDealersCollection"],
			defaultView:			"facilitiesDealersCollection"
		}
	}
}