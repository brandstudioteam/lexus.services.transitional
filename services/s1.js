var a = {
	service:				1,
	links: {
		4: {
			dataModels: [
				"partners",
				"partnersHierarchy"
			]
		},
		2: {
			dataModels: [
				"roles",
				"rolesTypesUsersTypes"
			]
		}
	},
	init: {
		messages: [
			["userLastName", "Фамилия", 152],
			["userFirstName", "Имя", 152],
			["userMiddleName", "Отчество", 152],
			["email", "Электронная почта", 152],
			["phone", "Телефон", 152],
			["registred", "Дата регистрации", 152],
			["userTypeId", "Идентификатор типа пользователя", 152],
			["userTypeName", "Тип пользователя", 152],
			["partnerDivisionId", "Идентификатор партнера", 152],
			["partnerDivisionName", "Партнер", 152],
			["Login", "Имя учетной записи (логин)", 152],
			["userRoles", "Роли пользователя", 152],
			["childrenPartners", "Подчиненные партнеры", 152],
			["childrenPartnersRoles", "Роли для управления подчиненнными партнерами", 152]
		],
		"inclideCSS": [
			["http://content.toyota.ru/css/s23.css"]
		]
	},
	modelsDefs: {
		users:	{
			fieldsDef: {
				userId: {
					title:						"Идентификатор",
					dataType:					"int"
				},
				userTypeId: {
					title:						"userTypeId",
					dataType:					"int"
				},
				userTypeName: {
					title:						"userTypeName",
					dataType:					"string"
				},
				userFirstName: {
					title:						"userFirstName",
					dataType:					"name"
				},
				userMiddleName: {
					title:						"userMiddleName",
					dataType:					"name"
				},
				userLastName: {
					title:						"userLastName",
					dataType:					"lastName"
				},
				userEmail:{
					title:						"email",
					dataType:					"email"
				},
				partnerDivisionId: {
					title:						"partnerDivisionId",
					dataType:					"int"
				},
				partnerDivisionName: {
					title:						"partnerDivisionName",
					dataType:					"string"
				},
				userCreateDate: {
					title:						"registred",
					dataType:					"datetime"
				},
				userLogin: {
					title:						"Login",
					dataType:					"string"
				},
				roleId:	{
					title:						"userRoles",
					dataType:					"collection"
				},
				children:	{
					title:						"childrenPartners",
					dataType:					"collection"
				},
				childrenRoles:	{
					title:						"childrenPartnersRoles",
					dataType:					"collection"
				}
			},
			keyFields: [
				"userId"
			],
			source:	{
				url:							"/admin/users/",
				controller:						"a",
				action:							"f"
			},
			loadType:				"preload",
			actions: [
				{
					name:					"add",
					title:					"add",
					category:				"collection",
					type:					"form",
					view:					"usersAdd",
					server: {
						url:					"/admin/users/",
						controller:				"a",
						action:					"a"
					},
					result:	{
						message:				"Пользователь {#userFirstName#} {#userLastName#} добавлен",
						action:					"update",
						updateDate:				[
							"usersRoles",
							"userChildrenPartners",
							"childrenPartnersRoles"
						]
					}
				},
				{
					name:					"edit",
					title:					"edit",
					category:				"object",
					type:					"form",
					view:					"usersEdit",
					server: {
						url:					"/admin/users/",
						controller:				"a",
						action:					"j"
					},
					result:	{
						message:				"Данные пользователя {#userFirstName#} {#userLastName#} сохранены",
						action:					"update",
						updateDate:				[
							"usersRoles",
							"userChildrenPartners",
							"childrenPartnersRoles"
						]
					}
				}
			]
		},

		usersTypes: {
			fieldsDef: {
				userTypeId:{
					title:						"userTypeId",
					dataType:					"int"
				},
				userTypeName:{
					title:						"userTypeName",
					dataType:					"string"
				},
				userTypeDescription:{
					title:						"userTypeDescription",
					dataType:					"text"
				}
			},
			keyFields: [
				"userId"
			],
			source:	{
				url:							"/admin/users/types/",
				controller:						"aa",
				action:							"a"
			},
			loadType:				"preload"
		},
		usersTypesPartnersTypes: {
			fieldsDef: {
				userTypeId:{
					title:						"userTypeId",
					dataType:					"int"
				},
				partnerTypeId:{
					title:						"partnerTypeId",
					dataType:					"int"
				}
			},
			keyFields: [
				"userTypeId",
				"partnerTypeId"
			],
			source:	{
				url:							"/admin/users/types/",
				controller:						"aa",
				action:							"f"
			},
			loadType:				"preload"
		},
		usersRoles: {
			fieldsDef: {
				userId:{
					title:						"userTypeId",
					dataType:					"int"
				},
				roleId:{
					title:						"userTypeId",
					dataType:					"int"
				}
			},
			keyFields: [
				"userId",
				"roleId"
			],
			source:	{
				url:							"/admin/users/",
				controller:						"a",
				action:							"m"
			},
			loadType:				"preload"
		},

		userChildrenPartners: {
			fieldsDef: {
				userId:{
					title:						"userTypeId",
					dataType:					"int"
				},
				partnerDivisionId:{
					title:						"userTypeId",
					dataType:					"int"
				}
			},
			keyFields: [
				"userId",
				"partnerDivisionId"
			],
			source:	{
				url:							"/admin/users/",
				controller:						"a",
				action:							"p"
			},
			loadType:				"preload"
		},

		childrenPartnersRoles: {
			fieldsDef: {
				userId:{
					title:						"userTypeId",
					dataType:					"int"
				},
				roleId:{
					title:						"userTypeId",
					dataType:					"int"
				}
			},
			keyFields: [
				"userId",
				"roleId"
			],
			source:	{
				url:							"/admin/users/",
				controller:						"a",
				action:							"q"
			},
			loadType:				"preload"
		}
	},





	ViewsDefs: {
		usersCollection: {
			category:				"collection",
			data:					"users",
			viewType:				"asTable",
			fields:		[
				{
					field:	"userFirstName",
					ordered:	1
				},
				{
					field:	"userMiddleName",
					ordered:	1
				},
				{
					field:	"userLastName",
					ordered:	1
				},
				{
					field:	"userTypeName",
					ordered:	1
				},
				{
					field:	"partnerDivisionName",
					ordered:	1
				},
				{
					field:	"userEmail",
					ordered:	1
				},
				{
					field:	"userLogin",
					ordered:	1
				},
				{
					field:	"userCreateDate",
					ordered:	1
				}
			],
			filters:	{
				type:			"static",
				fields: [
					{
						field:			"partnerDivisionId",
						fieldTitle:		"partnerDivisionName",
						title:			"partnerDivisionName",
						type:			"select",
						idField:		"partnerDivisionId",
						titleField:		"partnerDivisionName",
						dataType:		"int",
						order:			{
							fieldName:	"partnerDivisionName",
							direct:		1,
							dataType:	"string"
						}
					},
					{
						field:			"userTypeId",
						fieldTitle:		"userTypeName",
						title:			"userTypeName",
						type:			"select",
						idField:		"userTypeId",
						titleField:		"userTypeName",
						order:			{
							fieldName:	"userTypeName",
							direct:		1,
							dataType:	"string"
						}
					}
				]
			},
			params: {
				isPager:		1,
				isMultyOrder:	1,
				viewRowsCount:	100
			}
		},

		usersAdd:	{
			category:				"object",
			data:					"users",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s1a1.html",
				elementSelector:		"#s1a1_usersFormAdd"
			},
			fields:					[
				{
					field:				"userTypeId",
					type:				"select",
					data:				"usersTypes",
					idFields:			"userTypeId",
					titleFields:		"userTypeName"
				},
				{
					field:				"partnerDivisionId",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"usersTypesPartnersTypes",
						relFields:				"partnerTypeId",
						relFieldsId:			"partnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"userTypeId",
							relFieldsId:			"userTypeId",
							field:					"userTypeId"
						}
					}
				},
				{
					field:				"userFirstName"
				},
				{
					field:				"userMiddleName",
					ordered:			1
				},
				{
					field:				"userLastName",
					ordered:			1
				},
				{
					field:				"userEmail",
					ordered:			1
				},
				{
					field:				"userLogin",
					ordered:			1
				},
				{
					field:				"roleId",
					type:				"select",
					data:				"roles",
					idFields:			"roleId",
					titleFields:		"roleName",
					relate:				{
						data:					"rolesTypesUsersTypes",
						relFields:				"roleTypeId",
						relFieldsId:			"roleTypeId",
						field:					"roleTypeId",
						relate:				{
							data:					"_current",
							relFields:				"userTypeId",
							relFieldsId:			"userTypeId",
							field:					"userTypeId"
						}
					}
				},
				{
					field:				"children",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"partnersHierarchy",
						relFields:				"partnerDivisionId",
						relFieldsId:			"partnerDivisionId",
						field:					"partnerDivisionId",
						relate:				{
							data:					"_current",
							relFields:				"partnerDivisionId",
							relFieldsId:			"partnerDivisionId",
							field:					"parentPartnerDivisionId"
						}
					}
				},
				{
					field:				"childrenRoles",
					type:				"select",
					data:				"roles",
					idFields:			"roleId",
					titleFields:		"roleName",
					conditions:	{
						enable:	{},
						visible:	{
							relateField:	"userTypeId",
							value:			[4,6,7,8,9]
						}
					},
					relate:				{
						data:					"rolesTypesUsersTypes",
						relFields:				"roleTypeId",
						relFieldsId:			"roleTypeId",
						field:					"roleTypeId",
						relate:				{
							data:					"_current",
							relFields:				"userTypeId",
							relFieldsId:			"userTypeId",
							field:					"userTypeId"
						}
					}
				}
			],
			params: {
				mode: 1
			}
		},
		usersEdit:	{
			category:				"object",
			data:					"users",
			viewType:				"form",
			close:					"destroy",
			open:					"window",
			template:	{
				load:					"s1a1e.html",
				elementSelector:		"#s1a1_usersFormEdit"
			},
			fields:					[
				{
					field:				"userId"
				},
				{
					field:				"userTypeId",
					type:				"select",
					data:				"usersTypes",
					idFields:			"userTypeId",
					titleFields:		"userTypeName"
				},
				{
					field:				"partnerDivisionId",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"usersTypesPartnersTypes",
						relFields:				"partnerTypeId",
						relFieldsId:			"partnerTypeId",
						field:					"partnerTypeId",
						relate:				{
							data:					"_current",
							relFields:				"userTypeId",
							relFieldsId:			"userTypeId",
							field:					"userTypeId"
						}
					}
				},
				{
					field:				"userFirstName"
				},
				{
					field:				"userMiddleName",
					ordered:			1
				},
				{
					field:				"userLastName",
					ordered:			1
				},
				{
					field:				"userEmail",
					ordered:			1
				},
				{
					field:				"userLogin",
					ordered:			1
				},
				{
					field:				"roleId",
					type:				"select",
					data:				"roles",
					idFields:			"roleId",
					titleFields:		"roleName",
					relate:				{
						data:					"rolesTypesUsersTypes",
						relFields:				"roleTypeId",
						relFieldsId:			"roleTypeId",
						field:					"roleTypeId",
						relate:				{
							data:					"_current",
							relFields:				"userTypeId",
							relFieldsId:			"userTypeId",
							field:					"userTypeId"
						}
					},
					currentValues:		{
						data:					"usersRoles",
						field:					"roleId",
						relFields:				"userId",
						relate:				{
							data:					"_current",
							relFields:				"userId",
							relFieldsId:			"userId",
							field:					"userId"
						}
					}
				},
				{
					field:				"children",
					type:				"select",
					data:				"partners",
					idFields:			"partnerDivisionId",
					titleFields:		"partnerDivisionName",
					relate:				{
						data:					"partnersHierarchy",
						relFields:				"partnerDivisionId",
						relFieldsId:			"partnerDivisionId",
						field:					"partnerDivisionId",
						relate:				{
							data:					"_current",
							relFields:				"partnerDivisionId",
							relFieldsId:			"partnerDivisionId",
							field:					"parentPartnerDivisionId"
						}
					},
					currentValues:		{
						data:					"userChildrenPartners",
						field:					"partnerDivisionId",
						relFields:				"userId",
						relate:				{
							data:					"_current",
							relFields:				"userId",
							relFieldsId:			"userId",
							field:					"userId"
						}
					}
				},
				{
					field:				"childrenRoles",
					type:				"select",
					data:				"roles",
					idFields:			"roleId",
					titleFields:		"roleName",
					relate:				{
						data:					"rolesTypesUsersTypes",
						relFields:				"roleTypeId",
						relFieldsId:			"roleTypeId",
						field:					"roleTypeId",
						relate:				{
							data:					"_current",
							relFields:				"userTypeId",
							relFieldsId:			"userTypeId",
							field:					"userTypeId"
						}
					},
					currentValues:		{
						data:					"childrenPartnersRoles",
						field:					"roleId",
						relFields:				"userId",
						relate:				{
							data:					"_current",
							relFields:				"userId",
							relFieldsId:			"userId",
							field:					"userId"
						}
					}
				}
			],
			params: {
				mode: 2
			}
		}
	},

	UsersEvents: {

	},

	ActionsDefs: {
		default: {
			action:					"default",
			views:					["usersCollection"],
			defaultView:			"usersCollection"
		}
	}
}